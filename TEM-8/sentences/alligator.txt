However, the researchers realized that the alligator gar has a series of joints that allow it to expand the skull while closing its jaws. This ancient fish-crocodile mashup snared its prey using a key adaptation |Kate Baggaley |February 3, 2021 |Popular-Science 

Ally replied, perhaps too intuitively as a Floridian, “I don’t know anyone who has eaten someone’s face or wrestled an alligator.” Date Lab: Mariah Carey. Alligators. Urology. Is this any way to start a romance? |Damona Hoffman |January 21, 2021 |Washington Post 

The scientists in Louisiana chose alligators, Dixon says, because these animals have been found recently in both freshwater and saltwater environments. Whales get a second life as deep-sea buffets |Stephen Ornes |October 15, 2020 |Science News For Students 

The scientists wondered how alligator falls fit into a larger food web. Whales get a second life as deep-sea buffets |Stephen Ornes |October 15, 2020 |Science News For Students 

They prefer living in saltwater and are more aggressive than alligators. Let’s learn about alligators and crocodiles |Bethany Brookshire |September 29, 2020 |Science News For Students 

In another study, children saw a puppet show where a mouse was eaten by an alligator. Why Are Millennials Unfriending Organized Religion? |Vlad Chituc |November 9, 2014 |DAILY BEAST 

No matter what was on that flag—a white buttercup, a corn muffin, the skeleton of an alligator, who knows? We've Got Bigger Problems Than a Confederate Flag |John McWhorter |August 28, 2014 |DAILY BEAST 

I changed the beach ball to an inflatable alligator and started writing the story to find out what would happen next. Real Thrills And High Art In A Poignant Page-Turner Of A Novel |Susan Cheever |May 16, 2014 |DAILY BEAST 

You can even purchase a “Canned Exotic Meat Gift Set,” which includes rattlesnake, alligator, elk, and buffalo. Camel in a Can and 6 More Weird Canned Meats | |January 5, 2014 |DAILY BEAST 

He can cry alligator tears, condemn the turncoats, appease the Tea Partiers base and remain speaker. To Be or Not To Be…A Loser: Boehner’s Hamlet Moment |Joe McLean |October 4, 2013 |DAILY BEAST 

An alligator was observed swimming about, but very few fish were noticed. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

We rowed on, now and then knocking a young alligator on the nose as he popped his ugly head out of the water to have a look at us. In the Wilds of Florida |W.H.G. Kingston 

For a long time he had set his heart on catching a cayman, a kind of alligator that is found in the rivers of Guiana. The Animal Story Book |Various 

Once a tall crane stalked into view among the sedges; once an unseen alligator shook the silence with his deep, hollow roaring. In Search of the Unknown |Robert W. Chambers 

The alligator tried to follow her, but the shaft of the paddle caught among some tree trunks and stuck. The Adventures of Louis de Rougemont |Louis de Rougemont