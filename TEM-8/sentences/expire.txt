The $300 weekly enhanced unemployment benefit replaces the $600 enhanced benefit that expired the week ending July 25. These are the states that accepted Trump’s offer for $300 enhanced unemployment benefits |Lance Lambert |August 18, 2020 |Fortune 

He remembers watching birds expire in midair as they flew from one side of the plant to the other. Unmade in America |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

No one is getting that $600 payment right now, though, since it expired at the end of July and Congress is still deadlocked over whether to extend it. Yes, Unemployment Fell. But The Recovery Seems To Be Slowing Down. |Neil Paine (neil.paine@fivethirtyeight.com) |August 7, 2020 |FiveThirtyEight 

America and Russia updated that treaty when it was to expire in 2009. Butterfly Effect: The Next Nuclear Race Is Starting |Charu Kasturi |August 6, 2020 |Ozy 

Expanded unemployment benefits keeping the economy turning over expire on July 31, one-time checks to families are long spent, and there is still no coordinated federal plan to test for the coronavirus. Republicans fret over coronavirus lawsuits amid economic collapse |Tim Fernholz |July 22, 2020 |Quartz 

Higher courts, including the Supreme Court had refused to intercede, and the stay was to expire tonight. The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

Last year, it let an unemployment extension for the long-term jobless expire during the holidays. To GOP Congress, as Usual, It’s Welfare on the Chopping Block |Monica Potts |December 25, 2014 |DAILY BEAST 

The temporary reduction of Social Security payroll taxes was allowed to expire in early 2013. The Battle of the Deficit Bulge Has Been Won |Daniel Gross |October 6, 2014 |DAILY BEAST 

Those negotiations are set to expire at the end of November. Iran Orders Elite Troops: Lay Off U.S. Forces in Iraq |Eli Lake |October 6, 2014 |DAILY BEAST 

First, they let the stimulus boost expire, which that meant an average family of three receiving benefits lost $29 per month. Congress Unites to Screw the Hungry |Monica Potts |September 8, 2014 |DAILY BEAST 

When a lease is about to expire a difficult question sometimes arises, what can the tenant take away with him? Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

He begged me to follow him: “I may die under the knife, and I should wish, in that case, to expire in your arms.” My Ten Years' Imprisonment |Silvio Pellico 

The act which was passed at that time for imposing a tax upon income will shortly expire. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

The conquered remain on the battlefield, nearly broken in two, and feebly waving their paws, till they slowly expire in agonies. The Animal Story Book |Various 

The colonists looked anxiously to 1764 when the odious act would expire by limitation. A short history of Rhode Island |George Washington Greene