Flimflam felt better in the mouth than swindle, and rubberneck was a more agreeable verb than crane. From ‘Scientist’ to ‘Spam,’ the Surprisingly Playful Origins of English Words |Ralph Keyes |April 1, 2021 |Time 

An illegal stock tip is not the same thing as a swindle; but $68 million buys a lot of basketballs and BB guns. What Tolstoy Teaches Us About Insider Trading |Liesl Schillinger |June 2, 2013 |DAILY BEAST 

Another way of looking at it: How many Bernard Madoffs would it take to swindle the US taxpayer out of $1.2 trillion? The Wall Street Journal's Trillion-Dollar Error |Christopher Buckley |January 14, 2009 |DAILY BEAST 

I know where it claims to be, and I know it is just one big swindle from beginning to end. The Sunbridge Girls at Six Star Ranch |Eleanor H. (Eleanor Hodgman) Porter 

He said it was a bad swindle and he knew many others who had lost their money, too, which I thought would please you. The Sunbridge Girls at Six Star Ranch |Eleanor H. (Eleanor Hodgman) Porter 

Bois l'Hery's horses were unsound, Schwalbach's gallery was a swindle, Moessard's articles a recognised blackmail. The Nabob |Alphonse Daudet 

The whole thing was a cleverly-planned swindle, and unless you can get the bonds back you'll be out the money. The Rover Boys on the Farm |Arthur M. Winfield (AKA Edward Stratemeyer) 

That is true, too,—although I somehow think Merrick is the prime mover in this swindle. The Rover Boys on the Farm |Arthur M. Winfield (AKA Edward Stratemeyer)