One of their earliest products was a milkshake machine called the Cyclone Drink Mixer, and many of their kitchen tools were based on the company’s own inventions, including a high-speed fractional motor. Best toaster: Get perfectly golden slices every time |PopSci Commerce Team |February 10, 2021 |Popular-Science 

Robinhood is known for fractional shares, but it temporarily blocked people buying fractional shares of Gamestop. 5 ways Robinhood’s rushed UX changes exacerbated the GameStop crisis |Steve O'Hear |February 1, 2021 |TechCrunch 

The proposal reportedly includes $2,000 stimulus checks, which would buy you a decent fractional share of Bitcoin. Why Stimulus 3.0 + Impeachment 2.0 adds up to very little for investors |Bernhard Warner |January 14, 2021 |Fortune 

Tenev, meanwhile, notes that buying fractional shares of pricey stocks like Google and Amazon is particularly appealing to this generation. Robinhood’s next adventure: Stealing market share from the rich |Jeff |December 3, 2020 |Fortune 

So to create their algorithm, Oveis Gharan, Saberi and Singh defined a random process that picks a tree connecting all the cities, so that the probability that a given edge is in the tree equals that edge’s fraction in the best fractional route. Computer Scientists Break Traveling Salesperson Record |Erica Klarreich |October 8, 2020 |Quanta Magazine 

Royalty rates drop to the alarmingly fractional in this new “non-physical” arena. Van Dyke Parks on How Songwriters Are Getting Screwed in the Digital Age |Van Dyke Parks |June 4, 2014 |DAILY BEAST 

However, one nascent winner has been the rise of crowdsourced fractional labor. Is Crowdsourced Labor the Future of Middle Class Employment? |Sarah Kunst |March 26, 2014 |DAILY BEAST 

This camouflage ability underscores a broader issue with fractional regulation of a complex industry. Jamie Dimon’s Hubris Unshakable as JPMorgan Reelects Him to Top Two Posts |Nomi Prins |May 16, 2012 |DAILY BEAST 

The difference is due to the less minute calculation of fractional quantities in the latter case. Elements of Agricultural Chemistry |Thomas Anderson 

There was not a lie in this honest girl—not a fractional part of a lie—from her toes to her head. A Forest Hearth: A Romance of Indiana in the Thirties |Charles Major 

By the process of fractional distillation mineral oils of all grades can be obtained. Aviation Engines |Victor Wilfred Pag 

In the fractional second before the others could understand and react Dalgetty was moving. The Sensitive Man |Poul William Anderson 

The yard-land was only a fractional holding, incomplete for purposes of ploughing without co-operation. The English Village Community |Frederic Seebohm