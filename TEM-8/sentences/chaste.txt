None of that happened and Jéiser continued to do his thing, despite my advice to remain chaste. Locked up in the Land of Liberty: Part II |Michael K. Lavers |July 14, 2021 |Washington Blade 

As we see the marriage market unfold in all its patriarchal glory, Whistledown tells us that the “titled, chaste and innocent” daughters of the Ton have been preparing for marriage “since birth.” ‘Bridgerton’ dropped several clues about Lady Whistledown’s identity. Did they add up? |Bethonie Butler |January 6, 2021 |Washington Post 

Novelty aside, the real question is whether these avowedly chaste men of the cloth are listening. The Vatican's Same-Sex Synod: The Bishops Hear About Reality. Do They Listen? |Barbie Latza Nadeau |October 12, 2014 |DAILY BEAST 

Unlike the chaste boys club devotees of ‘Dungeons and Losers’ (oops, ‘Dragons’) of yore, women play the new games and like them. All the Grown-Up Hipsters Playing Kids’ Games |Daniel Genis |June 29, 2014 |DAILY BEAST 

This stood in contrast to the chaste 19th century, when marriage, rather than romance, was the main topic of interest. What the Sex Lives of the Founding Fathers Reveal About Us |Eric Herschthal |February 21, 2014 |DAILY BEAST 

There is a sense that Bennett is the real deal: he is the authentic Zionist, the chaste politician, the man with a plan. Why Is Bennett So Popular With Young Israelis? |Elisheva Goldberg |December 26, 2012 |DAILY BEAST 

She has no problem chugging a beer with dad, all the while staying chaste. Clint Eastwood’s New Movie ‘Trouble With the Curve’ & Why He’s a Republican Icon |Ramin Setoodeh |September 21, 2012 |DAILY BEAST 

She may be as chaste as unsunned snow, she is certainly as cold: but for warm, inspiring virtue! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

She sunk into Mr. Bumbles arms; and that gentleman, in his agitation, imprinted a passionate kiss upon her chaste nose. Oliver Twist, Vol. II (of 3) |Charles Dickens 

The elegant roof was supported on three rows of red sandstone pillars, adorned with chaste gilding and stucco-work. The Red Year |Louis Tracy 

Then, he could actually use a sextant, and his way of working out his latitude was chaste and picturesque. The Chequers |James Runciman 

Mr. Forten is a gentleman of fine education, a pure, chaste poet, and attends to farming for the love of nature. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany