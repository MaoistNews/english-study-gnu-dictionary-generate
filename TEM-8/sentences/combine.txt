In moments like these—at the graveside, in the hospital room, around the dinner table, riding the combine—cynicism rings hollow. Political cynicism has given way to love in Christian America |jakemeth |October 28, 2020 |Fortune 

Teams take great pains to try to quantify prospects’ general athleticism each year at the combine in Indianapolis. Who’s The Best Quarterback From The Much-Hyped 2018 Draft Class? |Josh Hermsmeyer |October 27, 2020 |FiveThirtyEight 

In addition to smart harvesting combines, the company is also developing technology using cameras to inspect individual plants and weeds in a field to modulate the amount of fertilizer and pesticides applied based on need. A.I. gets down in the dirt as precision agriculture takes off |Aaron Pressman |October 5, 2020 |Fortune 

It takes the burden of having to manage a lot of different things on the combine from the operator. A.I. gets down in the dirt as precision agriculture takes off |Aaron Pressman |October 5, 2020 |Fortune 

It’s like we’ve gone through a field after the combine has been through, and we are trying to find a kernel here and there. These undecided voters watched the debate hoping for clarity. What they got was a useless mess. |Marc Fisher, Christine Spolar, Amy B Wang |September 30, 2020 |Washington Post 

This is a testament to the fundamental human—and American—desire to combine place and possibility. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

Combine the beans and onion sauce in a 9x9-inch casserole dish and bake for 20 to 25 minutes. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 

Combine the cold butter and flour in the bowl of a food processor. Make ‘The Chew’s’ Carla Hall’s Pumpkin Pecan Pie |Carla Hall |December 26, 2014 |DAILY BEAST 

Add chocolate and butter to the bowl and melt, stirring to combine. Carla Hall’s Christmas Day Treat: Rum Balls |Carla Hall |December 25, 2014 |DAILY BEAST 

Then they would go to a hotel afterwards and combine the parts they had remembered in one sketch. The Big Business of Fashion Counterfeits |Lizzie Crocker |December 24, 2014 |DAILY BEAST 

Those stains which are dissolved in methyl-alcohol combine fixation with the staining process. A Manual of Clinical Diagnosis |James Campbell Todd 

They combine the fixing with the staining process, and stain differentially every normal and abnormal structure in the blood. A Manual of Clinical Diagnosis |James Campbell Todd 

The manufacturers of these pipes claim for them that they combine the strength of steel with the lightness of paper. Asbestos |Robert H. Jones 

Lets combine a flying machine with an iceboat and beat out everybody on the lake this winter! The Girls of Central High on the Stage |Gertrude W. Morrison 

Wisdom and experience combine in suggesting to all parents that they should guide their children, and not be governed by them. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al.