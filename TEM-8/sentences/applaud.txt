“This is an example of the progress that can be achieved when you collaborate and dedicate resources to improving the quality of water,” said Ohio governor Mike DeWine last year, applauding the EPA’s decision to declare fish in the river safe to eat. 51 Years Later, the Cuyahoga River Burns Again |Wes Siler |August 28, 2020 |Outside Online 

To which the farmers and ranchers listening to his speech applauded very mightily. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

Further, while TikTok should be applauded for exposing its algorithm, its competitors are likely to scoff at its suggestion that “all companies should disclose their algorithms, moderation policies, and data flows to regulators.” Social Shorts: TikTok’s future, Quora lead-gen ads, Facebook’s India plans |Ginny Marvin |August 3, 2020 |Search Engine Land 

We try to show all sides to the story’ — lots of people are trying to do that … I applaud what they try to get at, but I just don’t see that as a defendable position. Six months in, News Corp’s ‘Knewz’ aggregator has big ambitions |Lara O'Reilly |July 16, 2020 |Digiday 

Protecting their funding is a noble intention, and the Legislature should be applauded for it. New State Budget Will Do Irreparable Harm to Schools |Alec Patton |June 29, 2020 |Voice of San Diego 

It seems backwards to applaud what is already necessary, what has already been clear for a long time. The Unbearable Whiteness of Protesting |Rawiya Kameir, Judnick Mayard |December 10, 2014 |DAILY BEAST 

Far beyond his well-earned lucre, this is the reason to applaud Radcliffe the most. Daniel Radcliffe: I’m Richer Than One Direction |Tim Teeman |October 24, 2014 |DAILY BEAST 

They were up on their feet so often to applaud the Texas senator that his speech was practically an aerobics class. Paul, Cruz Duel at ‘Values Voter’ Event |Olivia Nuzzi |September 26, 2014 |DAILY BEAST 

I applaud Paul Ryan and the CBC members for their willingness to engage while respectfully listening to what each had to say. Paul Ryan Opens a Door to the Congressional Black Congress |Ron Christie |May 2, 2014 |DAILY BEAST 

If it was The View, someone off to the side would be motioning for the audience to applaud. It’s Not Just the Vaccines. Jenny McCarthy’s New Book Offers More ‘Lessons’ |Tim Teeman |April 28, 2014 |DAILY BEAST 

A small contingent of the members hurried off to applaud the successful comic opera of the hour. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

I think so, at least, by the agitation they are in and by the self-satisfied air with which they applaud their success. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

When June's fortune is finished all applaud her, as if she had performed some feat of skill, and then Mr. Morrell seizes Wy. Mushroom Town |Oliver Onions 

All eyes are bent in eager gaze to catch the first glimpse of the new Hamlet—all hands are ready to applaud. The Mirror of Literature, Amusement, and Instruction, No. 366 |Various 

He himself was sufficient audience to himself, ready to applaud and condemn with equal exaggeration of feeling. Sinister Street, vol. 1 |Compton Mackenzie