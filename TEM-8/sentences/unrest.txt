From political unrest to the global pandemic, different forces have added complexity to international travel. If you’re a solo parent traveling internationally with your kids, be ready for this question |Gina Rich |February 4, 2021 |Washington Post 

How BT performs against Vodafone, for example, matters less to investors than how industry in general is adapting to universal, global risks such as climate change, pandemics, social and political unrest. The Public Trusts Businesses Over Government to Solve Our Problems. But We Need Both to Try |Colin Mayer |February 3, 2021 |Time 

Last summer, amidst civil unrest and political polarization, many advertisers joined in a temporary boycott of Facebook. ‘Consumers expect brands to act’: Why defining voice and values has become crucial for marketers amid crises |Kimeko McCoy |January 25, 2021 |Digiday 

They had booked a hotel room in downtown Washington for Tuesday night to ensure access to the Mall but canceled it after the short-lived insurrection to avoid any potential unrest. Inaugurations are highly anticipated events in D.C. Now people must watch from their couches. |Emily Davies |January 20, 2021 |Washington Post 

“These efforts, including our open lines of communication with law enforcement, will continue through the inauguration and will adapt as needed if circumstances change in real-time,” it adds, preparing for the possibility of more unrest. How Twitter is handling the 2021 US presidential transition |Natasha Lomas |January 15, 2021 |TechCrunch 

But by Wednesday evening there was little in the way of organized protests or random unrest in the area. St. Louis Shooting Is the Anti-Ferguson |Justin Glawe |December 25, 2014 |DAILY BEAST 

Police Commissioner Andrew Scipione asked residents to remain calm, amid fears of unrest. Jihadi Siege in Sydney Ends in Gunfight |Courtney Subramanian, Lennox Samuels, Chris Allbritton |December 15, 2014 |DAILY BEAST 

Thereafter, the 1960s swelled with political zeal and social unrest. A History of Paris in 150 Photographs |Sarah Moroz |December 14, 2014 |DAILY BEAST 

One thing this new unrest shares with the Second Intifada, also known as the al-Aqsa Intifada, is the flashpoint. Intifada 3.0: Growing Unrest and a Plot to Kill an Israeli Minister |Creede Newton |November 21, 2014 |DAILY BEAST 

We saw how such provocative actions only inflamed passions and escalated the unrest. Awaiting the Grand Jury, Dread in Ferguson and America |Gene Robinson |November 16, 2014 |DAILY BEAST 

There were other causes entering into this unrest besides his yearning desire to win Ramona for his wife. Ramona |Helen Hunt Jackson 

One should be perfectly happy here—so peaceful, so beautiful, so far removed from the unrest and turmoil of the world. The Courier of the Ozarks |Byron A. Dunn 

But a fever of unrest consumed her, and finally she went downstairs, took her hat from its hook, and swung out of the house. Summer |Edith Wharton 

He reached the town in a dour mood of unrest, spite of the promise of wealth he carried in his pocket. Cabin Fever |B. M. Bower 

The story of the intended mutiny was well founded, and was only one phase of the general feeling of unrest throughout Alabama. The Supplies for the Confederate Army |Caleb Huse