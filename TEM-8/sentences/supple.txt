If you don’t have tension, your vocal cords come together in ways that are supple and gentle. We’re More of Ourselves When We’re in Tune with Others - Issue 104: Harmony |Kevin Berger |July 21, 2021 |Nautilus 

Shoes in this price point have uppers made of less supple materials throughout, and single-dial closure systems or some other setup instead of a double-dial closure. How Much Should You Spend on Cycling Shoes? |Joe Lindsey |May 21, 2021 |Outside Online 

I spent days in it—falling asleep in it, waking and spending the day in it, and falling asleep in it again—without complaint, a result of its medium-light weight and incredibly supple feel. Our Favorite Do-It-All Hoodies |Joe Jackson |April 5, 2021 |Outside Online 

Finally, the 80 percent cotton, 20 percent polyester weave was supple against my skin and after a few washes felt even more so. Our Favorite Do-It-All Hoodies |Joe Jackson |April 5, 2021 |Outside Online 

You just go so fast, gliding along the smooth track, a full-body choreography with hips burning, shoulders supple with each flick of the wrists, easy on the knees, hard on the heart. Cross-Country Skiers Have Ruined My Dog Walks |Sundog |April 3, 2021 |Outside Online 

But that wine, a balanced blend of supple fruit, focused acidity and sweet spice, was my wake-up call. The Pleasures of America’s Oldest Vines |Jordan Salcito |February 22, 2014 |DAILY BEAST 

Loose silk pants, cozy ponchos, and supple leggings were all a part of the equation. The Missoni Show Was Full of Bathrobes |Misty White Sidell |February 24, 2013 |DAILY BEAST 

There is a diversity of voices within Hamas, some more supple than others. Bring Hamas to the Table |Roi Ben-Yehuda |September 4, 2010 |DAILY BEAST 

They are still comparatively supple, and any misplaced pinnæ may be re-arranged without any difficulty. How to Know the Ferns |S. Leonard Bastin 

He did not say good night until she had become supple to his gentle, seductive entreaties. The Awakening and Selected Short Stories |Kate Chopin 

With a quick and supple movement he was beside her, stretching his length upon the ground in the shadow of the mountain. Bella Donna |Robert Hichens 

I had put my arms about her waist, and I felt her supple body weigh lightly on my clasped hands. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

His neck was long, more supple and active, he kept moving his head in an unnatural watchfulness like a wild animal's. Valley of the Croen |Lee Tarbell