Previous studies of endosymbiosis have shown that there can be complementary gene losses between hosts and endosymbionts, creating a metabolic interdependence. How Two Became One: Origins of a Mysterious Symbiosis Found |Viviane Callier |September 9, 2020 |Quanta Magazine 

The algorithms behind social media giants are designed to funnel viewers through a vortex of complementary interests — basically, to keep feeding them content they like in order to maintain interest and engagement. The American Fringes Go Mainstream |Nick Fouriezos |September 6, 2020 |Ozy 

In an increasingly complex and interdependent world, much more can be accomplished by working with partners who have different, yet complementary, skills. COVID-19 has spurred rapid transformation in health care. Let’s make sure it stays that way |jakemeth |August 20, 2020 |Fortune 

Grocery advertisers should adopt an “always-on” strategy by cross-promoting products on both similar and complementary keywords. How to prepare your e-commerce ad strategy for an uncertain Q4 |Sponsored Content: Pacvue |August 17, 2020 |Search Engine Land 

To Eric von Hippel’s point about the complementary relationship between home innovators and firms, Dana Lewis and her co-inventors have licensed their algorithm to healthcare companies to use in their devices. Honey, I Grew the Economy (Ep. 399) |Stephen J. Dubner |December 5, 2019 |Freakonomics 

To an extent, such ambitions are complementary, yet Liana is not interested in “extreme biography.” A Novel About a Novelist ‘Like’ Naipaul |Edward Platt |November 6, 2014 |DAILY BEAST 

He added that “the method is not a better method, but complementary to other methods.” It’s Official: Religion Doesn’t Make You More Moral |Elizabeth Picciuto |September 23, 2014 |DAILY BEAST 

If galaxy mapping is like doing a population map, the complementary study is like a demographic survey. SAMI Is Like Google Earth for the Universe |Matthew R. Francis |July 27, 2014 |DAILY BEAST 

According to Sig Gissler, who administered the awards, the two series of stories “were complementary.” Guardian and WaPo Share Pulitzer: Snowden Hails Victory for “More Accountable Democracy” |David Freedlander |April 14, 2014 |DAILY BEAST 

Between them, the four of them being so complementary, they managed to appeal to almost everyone. What It Was Like to Watch the Beatles Become the Beatles—Nik Cohn Remembers |Nik Cohn |February 9, 2014 |DAILY BEAST 

He placed it in prominence, instinctively or designedly, rejecting the merely complementary parts. Charles Baudelaire, His Life |Thophile Gautier 

This has occurred with regard to agricultural investments, which have awaited a complementary organic legislation. Social Comptabilism, Cheque and Clearing Service &amp; Proposed Law |Ernest Solvay 

Every color makes objects near it take on the antagonistic or complementary color. The Science of Human Nature |William Henry Pyle 

Each one of a pair enhances the effect of its complementary when the two colors are brought close together. The Science of Human Nature |William Henry Pyle 

If colored disks not complementary are mixed by rotation on a motor, they produce an intermediate color. The Science of Human Nature |William Henry Pyle