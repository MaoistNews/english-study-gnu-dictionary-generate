His right hand had only one small sore upon it, and no tumour discovered itself in the corresponding axilla. An Inquiry into the Causes and Effects of the Variolae Vaccinae |Edward Jenner 

Severe injury to the brain or the pressure due to the presence of a brain tumour, may also be a cause of mania. Essays In Pastoral Medicine |Austin Malley 

Meantime, even before the liberation of Solera and Fortini, Maroncelli was ill with a bad tumour upon his knee. My Ten Years' Imprisonment |Silvio Pellico 

Also an encysted tumour containing matter of a curdy appearance. The New Gresham Encyclopedia. Vol. 1 Part 2 |Various 

Hang a root of vervain around the neck in order to cause the disappearance of a tumour: as the plant dries up, so will the tumour. The Psychological Origin and the Nature of Religion |James H. Leuba