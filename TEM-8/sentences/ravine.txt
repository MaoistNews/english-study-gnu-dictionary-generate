There was a huge park a few blocks from your house, built around a series of wooded ravines and gullies that flattened themselves into picnic grounds in the lower elevations, the grass full of fireflies at dusk. Dark spaces on the map |Katie McLean |December 18, 2020 |MIT Technology Review 

To get here, you’ll have to ditch your car and hike 150 feet along a ravine and over a bridge. 7 Tranquil Airbnbs Near Winter-Friendly National Parks |Megan Michelson |December 11, 2020 |Outside Online 

As you see in the video, she comes jolting toward me and then kind of dips down into a ravine to my left. "Cougar Guy" Tells the Story Behind His Viral Video |Luke Whelan |October 16, 2020 |Outside Online 

By July, a sticky, shimmering stream of crude and brine oozed through the steep ravine. Oil Companies Are Profiting From Illegal Spills. And California Lets Them. |by Janet Wilson, The Desert Sun, and Lylla Younes, ProPublica |September 18, 2020 |ProPublica 

He watched oil spill into the ravine there, month after month, before CalGEM issued the fine against Chevron. Oil Companies Are Profiting From Illegal Spills. And California Lets Them. |by Janet Wilson, The Desert Sun, and Lylla Younes, ProPublica |September 18, 2020 |ProPublica 

Whose fault was it anyways that truck three got stuck in the ravine? A Man to Believe In: Eulogy for Marine Master Sergeant Aaron Torian |Elliot Ackerman |March 5, 2014 |DAILY BEAST 

We got our trucks stuck in flooded ravine beds, almost losing one to swiftly rising waters. A Man to Believe In: Eulogy for Marine Master Sergeant Aaron Torian |Elliot Ackerman |March 5, 2014 |DAILY BEAST 

The remains of her body were found in a ravine almost a year later. Washington, D.C., Law Seeks to Limit Late-Night Jail Releases |Chris Opfer |May 14, 2012 |DAILY BEAST 

The bones told a different story: she was executed along with five or six men in a ravine near Vlasenica. Survivor of the Genocide |Lauren Comiteau |March 5, 2010 |DAILY BEAST 

Hasan says he's heard the ravine was used as a garbage dump by the locals for years; few remains have survived. Survivor of the Genocide |Lauren Comiteau |March 5, 2010 |DAILY BEAST 

The cantonment was split into two sections by an irregular ravine, or nullah, running east and west. The Red Year |Louis Tracy 

Fully two miles away, on the south side of the ravine, were the sepoy lines, and another group of isolated bungalows. The Red Year |Louis Tracy 

Just at that moment the trail dipped into a rocky ravine and climbed a steep bank on the opposite side. Motor Matt's "Century" Run |Stanley R. Matthews 

There was no water in the ravine, but the rocks were jagged and sharp, and they had to use much care to save their tires. Motor Matt's "Century" Run |Stanley R. Matthews 

In two places the ravine became so narrow, that the bed of the stream occupied its whole extent. A Woman's Journey Round the World |Ida Pfeiffer