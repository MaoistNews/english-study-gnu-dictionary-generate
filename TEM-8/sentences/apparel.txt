Many apparel companies have successfully moved and mitigated the risks to other low-end alternative options to China. COVID proves that companies need to reduce their dependence on China |matthewheimer |September 11, 2020 |Fortune 

All kinds of businesses – be it food and beverage, apparel and footwear, beauty, automotive, travel, or hospitality all have been severely impacted. How businesses can use YouTube to tackle the COVID-19 business crisis |Catherrine Garcia |September 7, 2020 |Search Engine Watch 

For industries that aren’t seeing growth, such as apparel, Q3 is the time to employ defensive advertising strategies to maintain customer loyalty. How to prepare your e-commerce ad strategy for an uncertain Q4 |Sponsored Content: Pacvue |August 17, 2020 |Search Engine Land 

Online retailers may also see much higher than average returns as consumers traditionally shopping for apparel or gift items in-store move to more online shopping. How to prepare your e-commerce ad strategy for an uncertain Q4 |Sponsored Content: Pacvue |August 17, 2020 |Search Engine Land 

Meanwhile, the work-from-home trend has caused a big hit to the apparel and beauty industry as less people are spending on new clothes and cosmetics when they aren’t going out. How to prepare your e-commerce ad strategy for an uncertain Q4 |Sponsored Content: Pacvue |August 17, 2020 |Search Engine Land 

As a test, Sun shipped over a container stuffed with apparel made in his home province. 'Made in China' Now Being Made in Africa |Brendon Hong |August 23, 2014 |DAILY BEAST 

The American Apparel adverts are deemed tacky and offensive because they dramatize, brazenly, the sex-drenched time we live in. In Defense of American Apparel: Why a Hitched-Up Skirt Barely Conceals a Profound Social Confusion |Tim Teeman |August 8, 2014 |DAILY BEAST 

Allan Mayer, American Apparel's co-chair, seemingly laughed at Charney's plea, labeling him "a dreamer." American Apparel’s Dov Charney Speaks for First Time Since Firing |The Fashion Beast Team |June 24, 2014 |DAILY BEAST 

Ipanema has long been the hotbed of beach apparel, launching looks like the tanga and the g-string. The Girl From Ipanema Is Not Alone: Rio’s Famous Beach Is A Rich, Cultural Kaleidoscope |Brandon Presser |June 23, 2014 |DAILY BEAST 

Charney was ousted as CEO of American Apparel, which has been struggling in recent years. Revenge on the Pervs: Why the Tide Is Finally Turning Against Dov Charney and Terry Richardson |Amanda Marcotte |June 20, 2014 |DAILY BEAST 

After all, here was a babe equipped to face the exigencies of a censorious world; in looks and apparel a credit to any father. The Joyous Adventures of Aristide Pujol |William J. Locke 

Wearing apparel, furniture, jewelry, even legal expenses incurred in regaining her conjugal rights have been included. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Some half-decayed pieces of cloth and portions of wearing apparel were still lying in it. A Woman's Journey Round the World |Ida Pfeiffer 

She wouldnt have had her mother know how really thin her apparel seemed for anything in the world. The Girls of Central High on the Stage |Gertrude W. Morrison 

Shortly there appeared a youngish man, constructed by nature to adorn wearing apparel. Scattergood Baines |Clarence Budington Kelland