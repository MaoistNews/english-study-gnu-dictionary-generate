Fourteen years this woman had spent with Rigondeaux before he escaped. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

You can find fourteen of these copper creations, all initially containing 3,900 liters of liquid apiece, on the Macallan estate. When It Comes to Great Whisky, The Size of Your Still Matters | |December 9, 2014 |DAILY BEAST 

Fourteen years on, the wooden stairs and ceiling are still charred, and the walls are studded with clusters of bullet holes. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

The first machine, the 914—so called because it could copy on paper nine inches by fourteen inches—was a complex, clunky beast. Pioneers in Printing |The Daily Beast |October 21, 2014 |DAILY BEAST 

Mooney quickly inflated his life raft, sent out an SOS signal and drifted for fourteen days before he was rescued. Victor Mooney’s Epic Adventure for His Dead Brother |Justin Jones |October 19, 2014 |DAILY BEAST 

Fourteen genera, representing about 19 species, of Mallophaga are reported for 20 different species of bird hosts. Summer Birds From the Yucatan Peninsula |Erwin E. Klaas 

Bonaparte took possession of Venice, boasting an independence of fourteen centuries. The Every Day Book of History and Chronology |Joel Munsell 

For fourteen years Massna served in the Royal Italians, but at last he retired in disgust. Napoleon's Marshals |R. P. Dunn-Pattison 

A mosquito becomes dangerous in eight to fourteen days after it bites a malarious person, and remains so throughout its life. A Manual of Clinical Diagnosis |James Campbell Todd 

I need fourteen wire ropes, all pulling in different directions, to hold me steady. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling