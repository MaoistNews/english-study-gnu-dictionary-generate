Nearly 500 Olympic volunteers withdrew, and one poll found fewer than 7 percent of respondents thought Mori was qualified to continue in his role. Japan Olympics chief who said women talk too much will resign over remarks, reports say |Simon Denyer, Julia Mio Inuma |February 11, 2021 |Washington Post 

Many respondents were spooked by the idea of allowing others access to their brain’s inner workings. People are concerned about tech tinkering with our minds |Laura Sanders |February 11, 2021 |Science News For Students 

The idea of allowing companies, or governments, or even health care workers access to the brain’s inner workings spooked many respondents. Can privacy coexist with technology that reads and changes brain activity? |Laura Sanders |February 11, 2021 |Science News 

In this experiment, people were not particularly uncomfortable asking sensitive questions, and the respondents’ comfort levels were fairly similar across the three conditions. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

Eighty-four percent of respondents said they were satisfied. A brutal, isolating year leads to baffling battles between good and evil |Philip Bump |February 4, 2021 |Washington Post 

The judge did rule that one respondent crossed the line by implying that Im Tirtzu espouses Nazi race theory. The 'Defenders of Zionism' Lose Their Case |Gershom Gorenberg |September 9, 2013 |DAILY BEAST 

But rarely does it cover the respondent in even short-term glory. Marco Rubio And The State of the Union Response Speech Curse |Michelle Cottle |February 12, 2013 |DAILY BEAST 

The most obvious: no matter how charismatic a respondent may be, he or she is not the president. Marco Rubio And The State of the Union Response Speech Curse |Michelle Cottle |February 12, 2013 |DAILY BEAST 

The more promising the respondent, the higher the expectations and the greater the potential for disappointment. Marco Rubio And The State of the Union Response Speech Curse |Michelle Cottle |February 12, 2013 |DAILY BEAST 

The younger the respondent, the lower the support for whaling. I’ll Have the Whale, Please: Japan’s Unsustainable Whale Hunts |Jake Adelstein, Nathalie-Kyoko Stucky |February 5, 2013 |DAILY BEAST 

It really raises the question whether a man who has wrongly been named as co-respondent is in honour bound to marry the defendant. When Winter Comes to Main Street |Grant Martin Overton 

See also the allusion in Sophistês (to the appearance of the younger Sokrates as respondent), p. 218 B. Plato and the Other Companions of Sokrates, 3rd ed. Volume III (of 4) |George Grote 

He depends upon the acquiescence of the respondent for every step taken in advance. Plato and the Other Companions of Sokrates, 3rd ed. Volume III (of 4) |George Grote 

That the term defendant is, when alone, synonymous with respondent. How to Catalogue a Library |Henry B. (Henry Benjamin) Wheatley 

Mr. Evans and Mr. Palmer were for the appellants, and Mr. Hawkins and Mr. Bulwer for the respondent. Norfolk Annals |Charles Mackie