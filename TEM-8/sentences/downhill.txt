Landslides — the sudden failure of a slope, sending a rush of rocks and sediment downhill — can be triggered by anything from an earthquake to an intense deluge of rain. Three things to know about the disastrous flood in India |Carolyn Gramling |February 9, 2021 |Science News 

Let’s hope I don’t need to fish for downhill metaphors to describe the markets this week. Bitcoin, stocks and crude take off as the markets brace for a wave of stimulus checks |Bernhard Warner |February 8, 2021 |Fortune 

Then we’re running downhill on the vaccines because the pipeline gets better and better. Why Opening Restaurants Is Exactly What the Coronavirus Wants Us to Do |Caroline Chen |February 6, 2021 |ProPublica 

Turning downhill a bit more frequently on pick and rolls and leveraging his improved rim-finishing and free-throw shooting would be a good place to start. Jaylen Brown Is Using The Midrange Shot To Reach A Different Level |Yaron Weitzman |January 29, 2021 |FiveThirtyEight 

As fun as it is to climb on, in general, rando race gear isn’t all that fun to ski downhill, even inbounds. The Backcountry Ski Sales Boom Is Upon Us |Marc Peruzzi |January 22, 2021 |Outside Online 

Pat Robertson finished second in the 1988 Iowa caucus, and it was all downhill from there. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

There would be occasional periods of promise, but once the 1950s came it was a steady, painful downhill slide. Football Great Bob Suffridge Wanders Through the End Zone of Life |Paul Hemphill |September 6, 2014 |DAILY BEAST 

After determining that the bee pollen and mushroom broth were inedible, the “detox” quickly went downhill. We Were Gwyneth’s GOOP Guinea Pigs |Erin Cunningham, Olivia Nuzzi |March 30, 2014 |DAILY BEAST 

When downhill racer Julia Mancuso did it, she wore the American flag like a cape. What’s a Key to Victory in Sochi? Coming So Close to Defeat. |Kevin Bleyer |February 23, 2014 |DAILY BEAST 

Downhill track sports like luge are technology battles, as exciting as a NASCAR qualifying day. The Stench of Sochi |Alex Berenson |February 12, 2014 |DAILY BEAST 

He built splendid residences at Downhill and Ballyscullion, which he adorned with rare works of art. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

Though the course was downhill to the Mertz Glacier, the sledge required a good deal of pulling owing to the wet runners. The Home of the Blizzard |Douglas Mawson 

In the afternoon, Dr. Mawson's party forged ahead, the dogs romping along on a downhill grade. The Home of the Blizzard |Douglas Mawson 

For two miles we were going downhill at a running pace and then the slope became suddenly steeper and the sledge overtook me. The Home of the Blizzard |Douglas Mawson 

Once, in a preoccupied moment, he let go of the little vehicle and it started downhill, gaining speed rapidly. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine