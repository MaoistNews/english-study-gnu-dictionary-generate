Yet her writing is magnificent, thrilling, pleasurable, and psychologically profound. Highsmith at 100: Literary legacy marred by racism |Kathi Wolfe |February 10, 2021 |Washington Blade 

He had so many burdens on his back that day and responded in such a profound way that changed not just that Super Bowl, but it changed the game. Doug Williams’s MVP performance still ranks among the greatest in Super Bowl history |Scott Allen |February 5, 2021 |Washington Post 

It was almost this ritualistic thing where I was with this man through all these profound final moments of his existence on the planet. How to Be Better at Death (Ep. 450) |Maria Konnikova |February 4, 2021 |Freakonomics 

The post Australia’s showdown with Google has profound implications for domestic businesses and other digital platforms appeared first on Search Engine Land. Australia’s showdown with Google has profound implications for domestic businesses and other digital platforms |George Nguyen |February 2, 2021 |Search Engine Land 

It feels like you have been awakened in a very profound way. Boris Kodjoe: Teach Racial History |Eugene Robinson |February 2, 2021 |Ozy 

Great American leaders have long contributed profound thoughts of tremendous consequence to the public discourse. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

But throughout the series so far, its style has also had a profound story of its own to tell. What Downton’s Fashion Really Means |Katie Baker |January 2, 2015 |DAILY BEAST 

This does not seem like a profound bit of dramaturgy on my part, and he agrees with it. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

It might have been asking too much for Philip Roth to provide it, but the need was profound. American Dreams: How Bush Shaped Our Reading of Roth’s ‘The Plot Against America’ |Nathaniel Rich |November 23, 2014 |DAILY BEAST 

And I had something deep and profound to say about drug legalization too, but the bong went out. The GOP Senate: A New Utopia Dawns |P. J. O’Rourke |November 8, 2014 |DAILY BEAST 

"I most humbly thank your lordship," replied the butler with an air of profound gratitude, as he chuckled in his sleeve. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Never before in human experience had such a display of kindly feeling and profound regret been witnessed in similar circumstances. The Giant of the North |R.M. Ballantyne 

But he is not aware of the profound extent to which his own opinions have been affected by the changing times. The Unsolved Riddle of Social Justice |Stephen Leacock 

Struck with surprise, the dead silence of profound awe, for an instant stilled the whole assembly. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Mr Cutbill, the managing partner of the London house, received him with profound respect and pleasure. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various