To begin with, there’s Ursula, a flamboyantly campy villain who lives to be over the top. Toward a queer Disney canon |Emily VanDerWerff |September 4, 2020 |Vox 

He has avoided labeling any actual villains he would blame for San Diego’s problems. Politics Report: Bry vs. Transit |Andrew Keatts and Scott Lewis |August 29, 2020 |Voice of San Diego 

Instead, Halliburton claims, health administrators treated him like a villain for calling. Confirmed Nursing Home Complaints Plummet During Pandemic |Jared Whitlock |August 25, 2020 |Voice of San Diego 

For years, publishers have singled out the duopoly — Google and Facebook — as the industry’s mega villain characters, hoovering up the majority of the digital advertising pie and leaving behind just the crumbs for everyone else to play with. Publishers could soon have more leverage to force Apple to relax its ‘my way or the highway’ approach |Lara O'Reilly |August 25, 2020 |Digiday 

China’s accelerating AI innovation deserves the world’s full attention, but it is unhelpful to reduce all the many developments into a simplistic narrative about China as a threat or a villain. China and AI: What the World Can Learn and What It Should Be Wary of |Hessy Elliott |July 3, 2020 |Singularity Hub 

I could complain about how, two out of eight episodes in, Agent Carter is in no hurry to introduce its real villain. Marvel’s ‘Agent Carter’ Stomps on the Patriarchy |Melissa Leon |January 7, 2015 |DAILY BEAST 

When I play a villain, I always try to make sure they believe what they are doing is right. After The Fall: Introducing The Anti-Villain |Rich Goldstein |December 21, 2014 |DAILY BEAST 

I say a lot that in the story of racism in America nobody wants to be the villain. The Unbearable Whiteness of Protesting |Rawiya Kameir, Judnick Mayard |December 10, 2014 |DAILY BEAST 

Bill Cosby, it seems, can only be seen in two registers: sainted family man of a much-loved sitcom, or fallen, tarnished villain. Newsflash: Bill Cosby Is Not Cliff Huxtable |Tim Teeman |November 20, 2014 |DAILY BEAST 

And if there was a villain on whom to pin this whole struggle—it would be our inner demons. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

"So that is Jim Poindexter, the bloody villain," muttered the boy between his set teeth, and nervously fingering his revolver. The Courier of the Ozarks |Byron A. Dunn 

The villain Longcluse, and the whole fabric of his machinations, may be dashed in pieces by a word. Checkmate |Joseph Sheridan Le Fanu 

"You infernal villain, if you don't surrender, I'll blow your brains out," hissed his lordship. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

That black-hearted villain, Hidayut Khan, wanted more than his share of plunder on many occasions, and was refused it. Confessions of a Thug |Philip Meadows Taylor 

But Sir G. Downing would not be answered so: though all the world takes notice of him for a most ungrateful villain for his pains. Diary of Samuel Pepys, Complete |Samuel Pepys