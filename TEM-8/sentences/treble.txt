The sound profile skewed toward a brighter treble than equivalent headsets I’ve tested. Skullcandy Hesh headphones review: An everyday overachiever |Andrew Waite |August 20, 2021 |Popular-Science 

Paavo Nurmi was ready for the same treble as Hassan in 1924, and more. As Sifan Hassan Attempts Olympic Triple, a Look at Historic Multi-Medalists |jversteegh |August 4, 2021 |Outside Online 

That treble would let Hassan step on to a very special podium, with Paavo Nurmi, Emil Zátopek, and other multi-victory giants whose golden shadows still shimmer over the Tokyo track. As Sifan Hassan Attempts Olympic Triple, a Look at Historic Multi-Medalists |jversteegh |August 4, 2021 |Outside Online 

On the app, you can control the sound mix using a single slider that scales from bass-heavy to treble-focused. CX True Wireless review: Basic Sennheiser earbuds that sound anything but |Tony Ware |July 21, 2021 |Popular-Science 

The B652 Air set adds a ribbon tweeter for a more accurate treble definition. The best bookshelf speakers fill your room with sound, not clutter |Tony Ware |July 19, 2021 |Popular-Science 

She looked through his list of potential song titles and came across one called “Treble Bass.” ‘All About That Bass’ Singer Meghan Trainor On Haters and Her Polarizing (and Unlikely) No. 1 Hit |Marlow Stern |October 7, 2014 |DAILY BEAST 

At the time of his death, Fleming had pulled off a rare treble in English literature. James Bond is Back But is He Any Good without Fleming? |Robert McCrum |October 5, 2013 |DAILY BEAST 

In treble, second and fourth, the first change is a dodge behind; and the second time the treble leads, there's a double Bob. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

The Seven-score and four on the six middle Bells, the treble leading, and the tenor lying behind every change, makes good Musick. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

Sir Peter Edlin, it seems, has been doing treble the amount of work for a two-third's salary. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

The simplest form was the Doublette sounding the 15th and 22nd (the double and treble octave) of the note struck. The Recent Revolution in Organ Building |George Laing Miller 

Thou knittest togider the mene sowle of treble kinde, moeving alle thinges'; &c. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer