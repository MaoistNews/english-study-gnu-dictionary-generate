While there’s a lot of chatter around a possible surge in coronavirus cases come fall, economists note there may also be chilling headwinds for the labor market and small businesses in the next few months. Is the economy about to head backwards? |Anne Sraders |August 21, 2020 |Fortune 

The other is a carbon tax, a term that often sends chills through free market advocates. Environment Report: One Way to Force Companies to Emit Less Carbon |MacKenzie Elmer |August 10, 2020 |Voice of San Diego 

When chilled, a warmer system cooled off in less time than it took a cooler system to reach the same low temperature. A new experiment hints at how hot water can freeze faster than cold |Emily Conover |August 7, 2020 |Science News 

I’d known Jim since grade school, and he did try to chill out when I asked him to, but he simply couldn’t control himself. When Jesus Freaks Go Bad |Eugene Robinson |August 6, 2020 |Ozy 

Side effects to the coronavirus vaccine include pain at the injection site, fatigue, headache, muscle aches, chills and feeling feverish. COVID-19 vaccines by Oxford, CanSino and Pfizer all trigger immune responses |Tina Hesman Saey |July 21, 2020 |Science News 

It was an attempt to combat a growing chill on free speech in Turkey while placing his newspaper at the center of the debate. Turkey Arrests Journalists in Crackdown |Jesse Rosenfeld |December 14, 2014 |DAILY BEAST 

Alice wore a black nylon rain jacket that looked as if it was ill prepared to deal with the coming chill. The Wildly Peaceful, Human, Almost Boring, Ultimately Great New York City Protests for Eric Garner |Mike Barnicle |December 8, 2014 |DAILY BEAST 

If you prefer them chewy in the middle and crisp outside, chill the balls of dough. Make These Barefoot Contessa Salty Oatmeal Chocolate Chunk Cookies |Ina Garten |November 28, 2014 |DAILY BEAST 

Standing in the chill breeze of autumn, I knew something had passed between us. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

What he—and his friend holding the camera—heard in response was enough to chill them to the bone. Dumpster Politicians, Jeter Tributes, and More Viral Videos |Jack Holmes |September 21, 2014 |DAILY BEAST 

Presently he began to shiver so, with some sort of a chill, that I took off my coat and wrapped it round him. The Boarded-Up House |Augusta Huiell Seaman 

From the day of that terrible chill in the snow-storm, she had never been quite well, Ramona thought. Ramona |Helen Hunt Jackson 

Though she was warmly wrapped in a soft rug of silvery fur, a chill crept into her heart. Rosemary in Search of a Father |C. N. Williamson 

As she walked along the chill promenade she looked with discreet curiosity at every woman she met, to see her condition. Hilda Lessways |Arnold Bennett 

A chill, sinister feeling crept over me, but I kept my gaze fixed steadily in the same direction. Uncanny Tales |Various