It is pressed deep inside, then more is fetched to mash on top. Dr. Mike’s Makes the Best Ice Cream on Earth |Jane & Michael Stern |July 27, 2014 |DAILY BEAST 

Orson Welles (1965) His Chimes at Midnight was a mash-up of the Shakespeare plays in which Sir John Falstaff appears. Shakespeare’s Movie Magic |Marina Watts, Malcolm Jones |April 24, 2014 |DAILY BEAST 

Because this is my book,” Kibbe writes, he decided to “mash up” the conversations into an “imaginary gab fest. Why The Tea Party Won’t Go Away And More Wisdom From Matt Kibbe |Michael Signer |April 23, 2014 |DAILY BEAST 

It is made, as the label narrates “with traditional mash hopping and without wort boiling.” Wine Snobs, There’s a Beer for You |Jordan Salcito |April 5, 2014 |DAILY BEAST 

First, it must come from a mash bill that contains at least 51 percent corn. Hillbilly Heaven: The History of Small-Batch Bourbon |Dane Huckelbridge |March 29, 2014 |DAILY BEAST 

The red cow ha' calved, an' no one here to see 'un, an' mother had to carry her a hot mash hersel'. The World Before Them |Susanna Moodie 

Then throw away the bees and lay the stings gently but firmly on a mash composed of the breasts of five Buff Orpington cockerels. Punch, or the London Charivari, Vol. 152, May 16, 1917. |Various 

Why, if it wasn't for the fact that I'm feeling particularly happy to-night, I'd mash your mouth for that. An Arkansas Planter |Opie Percival Read 

Horses that ought to be having a mash between their ribs make riders despond. Lord Ormont and his Aminta, Complete |George Meredith 

"Yes, and I'd like to know how you come to mash my mouth so dod-rottedly," said Sneak, in well-affected ill nature. Wild Western Scenes |John Beauchamp Jones