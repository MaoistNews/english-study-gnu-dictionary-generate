Although corporations can opt in to become a PBC, there is no obligation on them to do so and they need the support of their shareholders. 50 years later, Milton Friedman’s shareholder doctrine is dead |jakemeth |September 13, 2020 |Fortune 

Right now, 71 percent of all non-agricultural part-time workers fit the latter category, and one of the biggest noneconomic reasons that people look for or accept part-time work is child care obligations. The Easy Part Of The Economic Recovery Might Be Over |Amelia Thomson-DeVeaux |September 4, 2020 |FiveThirtyEight 

Each of us has an obligation to befriend people who are different from us and invite them into our homes. We won’t have a true economic recovery until we tackle the racial wealth gap |matthewheimer |September 1, 2020 |Fortune 

Earlier in lockdown, when people were always available — because life outside the home was essentially banned — there were new, complicated obligations to be virtually present. ‘It can take on a panopticon effect’: Slack’s presenteeism problem grows with no end in sight for remote work |Lucinda Southern |August 28, 2020 |Digiday 

By spreading out the payments over many years, he could keep his tax obligations low. How the City Came to Lease a Lemon |Lisa Halverstadt and Jesse Marx |August 10, 2020 |Voice of San Diego 

Obviously, the first obligation of all liberal democratic governments is to enforce the rule of law. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

It is the obligation of citizens and journalists as well as governments. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

“It is our Islamic obligation to pledge allegiance to the Islamic State and give it our Islamic fealty,” he said. ISIS Targets Afghanistan Just as the U.S. Quits |Sami Yousafzai, Christopher Dickey |December 19, 2014 |DAILY BEAST 

Even the best of us can hurt the people who come to us for care when we forget that our foremost obligation is to them. Why So Many Surgeons Are Psychos |Russell Saunders |December 17, 2014 |DAILY BEAST 

This government obligation is limited by practical considerations of safety and security. The GOP’s Hidden Ban on Prison Abortions |Harold Pollack |December 13, 2014 |DAILY BEAST 

There is an implied obligation on the hirer's part to use the car only for the purpose and in the manner for which it was hired. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

With this political subjection one is reluctant to associate a more sordid kind of obligation. King Robert the Bruce |A. F. Murison 

The swearing of an oath always brings under obligation to God, and therefore always includes the making of a vow. The Ordinance of Covenanting |John Cunningham 

If he carries these gratuitously his obligation is still less, nevertheless he must even then take some care of them. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Sometimes a moral obligation to pay money is a good consideration for a promising to pay it. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles