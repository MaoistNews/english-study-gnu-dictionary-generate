Here in Alaska, many moose hunters frequent old burned timber stands, and these are littered with dead small trees and debris that can damage an ATV pretty easily if you’re not careful. Six ways to make your ATV even more rugged |By Tyler Freel/Outdoor Life |September 17, 2020 |Popular-Science 

His bill died after the timber industry opposed defunding the institute. Oregon Gov. Kate Brown Calls for Audit After Our Reporting on a State Institute That Lobbied for the Timber Industry |by Tony Schick, OPB, and Rob Davis, The Oregonian/OregonLive |September 2, 2020 |ProPublica 

It also would reinstate timber severance taxes, which are based on the value of the trees that are logged. Oregon Gov. Kate Brown Calls for Audit After Our Reporting on a State Institute That Lobbied for the Timber Industry |by Tony Schick, OPB, and Rob Davis, The Oregonian/OregonLive |September 2, 2020 |ProPublica 

In a story published in June, the three news organizations found that the state’s largest timber companies received billions of dollars in tax cuts since the 1990s. Oregon Gov. Kate Brown Calls for Audit After Our Reporting on a State Institute That Lobbied for the Timber Industry |by Tony Schick, OPB, and Rob Davis, The Oregonian/OregonLive |September 2, 2020 |ProPublica 

At the same time a surge in home renovations and do-it-yourself home projects during the pandemic—just look at the earnings of Home Depot and Lowe’s—and rebounding home construction have increased the demand for timber. ‘Severe lumber shortage’ is adding $14,000 to the cost of a new home |Lance Lambert |August 31, 2020 |Fortune 

“The Americans were a tool, used by the Safis in the Pech to rid them of their competition in the timber trade,” Zalwar Khan said. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

Despite the trade officially being banned, he explains, timber was still locally harvested and sold. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

With a lot of forethought, he wields an 8-pound maul through timber. Brooklyn’s Booming Firewood Industry |Dale Eisinger |July 8, 2014 |DAILY BEAST 

Though the company imports its wood from a timber supplier, they cut and process it. Brooklyn’s Booming Firewood Industry |Dale Eisinger |July 8, 2014 |DAILY BEAST 

One outfitter found a camp in timber—a Nichols camp, with a fresh three-rock campfire. The Ballad of Johnny France |Richard Ben Cramer |January 12, 2014 |DAILY BEAST 

The moment the bait was touched, down would come the heavy timber—smash—on the tiger's head. Our Little Korean Cousin |H. Lee M. Pike 

I pulled up and glanced about, but the clumps of scrubby timber were just plentiful enough to cut off a clear view of the flat. Raw Gold |Bertrand W. Sinclair 

The case was fixed over the engine-shaft on two beams of timber from wall to wall. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

It is like the eating of a smothered fire into rotten timber in that it is noiseless and without haste. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

There was then a great deal of old timber about the place and a long avenue of oaks, besides three large cherry orchards. Birds of Guernsey (1879) |Cecil Smith