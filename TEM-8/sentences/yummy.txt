In the 1980s, she showed how a fungal network searches for food and then re-forms itself after it finds something yummy. Secret forest fungi partner with plants — and help the climate |Kathryn Hulick |December 9, 2021 |Science News For Students 

Two in each specially-marked box of Cap’n Crunch cerealKAYEIn the 70s, kids like me got up super early to watch Saturday morning cartoons laced with advertising for sugary cereal and snacks loaded with yummy empty-calories. Kill Your Algorithm: Listen to episode two of the podcast featuring tales from a more fearsome FTC |Kate Kaye |October 28, 2021 |Digiday 

As the warming water slowed its growth, less yummy kelp drifted into the crevices of the reefs where hungry sea urchins typically lurk. Urchin takeover underlies California’s vanishing kelp forests |Anushree Dave |May 7, 2021 |Science News For Students 

The plastic’s recipe also offers the microbes ingredients they find yummy. Here’s how to make flip-flops biodegradable |Kathiann Kowalski |December 8, 2020 |Science News For Students 

No dumpsters full of “yummy restaurant food” means short-term starvation, he explains. The ‘ratpocalypse’ isn’t nigh, according to service call data |Bethany Brookshire |July 14, 2020 |Science News 

In recent years, away from the screen, Hurley has reinvented herself as a yummy mummy. Happy 20th Birthday, Liz Hurley’s Safety-Pin Dress |Tim Teeman |December 12, 2014 |DAILY BEAST 

Plus, sushi is yummy, and you can't blame a singer who's spent a decade shopping at Hot Topic for wanting to switch up her look. Avril Lavigne’s Dumb ‘Hello Kitty’ Video Is Rife with Cultural Appropriation |Amy Zimmerman |April 25, 2014 |DAILY BEAST 

Yummy guarantees delivery within 30 minutes of ordering with a $3.99 delivery charge, a feat Amazon cannot match. Get the Door. It’s … Amazon? |CNBC |July 26, 2013 |DAILY BEAST 

Democrats found it difficult to choose in 2008 because both their options seemed to them so delectably yummy. Still Waiting for Mr. Right |David Frum |February 27, 2012 |DAILY BEAST 

Well-known for their included breakfast at yummy Terra Bistro, this lodge is all the good stuff sans the excess nonsense. Vail, Colorado: Skier's Paradise |Jolie Hunt |January 15, 2011 |DAILY BEAST