Rival brigades often bare-knuckle brawled each other in the streets outside of burning buildings for the right to put out the fire and secure the lucre that came with it. New York’s First Crime Boss: Captain Isaiah Rynders |Mark Lawrence Schrad |September 10, 2021 |The Daily Beast 

Decades later, during the Persian Gulf War, he commanded a brigade of the 1st Armored Division. Montgomery Meigs, who led U.S. Army in Europe, dies at 76 |Bart Barnes |July 13, 2021 |Washington Post 

Assuming the battle won, the brigade to which the 71st was attached was granted permission to withdraw and rest. Civil War cannons that may have been at Battle of Bull Run get new home |Michael Ruane |June 16, 2021 |Washington Post 

The chorizo con huevo features crumbly pork from Toluca Que Rico in Baltimore, whose heat is muted, though not extinguished, by a water brigade of ingredients, including scrambled eggs, avocado, Oaxaca cheese and chipotle mayo. The 25 best sandwiches in the D.C. area |Tim Carman |June 16, 2021 |Washington Post 

I am thankful for the flexibility and adaptability of the brigade and our entire team here on the yard and in the local community as we navigate this challenging period, especially the hotels for their responsiveness and hospitality. Naval Academy moves nearly 200 midshipmen to hotels after rise in coronavirus cases |Lauren Lumpkin |March 4, 2021 |Washington Post 

Simultaneously, a brigade of mercenaries and Congolese soldiers would seal off the city and expel the guerrillas. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

Stasio would join the Fourth Brigade, Second Infantry Division, “the Raiders,” and deploy to Iraq. How the NSA Became a Killing Machine |Shane Harris |November 9, 2014 |DAILY BEAST 

The staff sergeant said that when he was in the Givati Brigade in 2007 or so, it was “openly secular.” The Ghosts of Gaza: Israel’s Soldier Suicides |Creede Newton |October 28, 2014 |DAILY BEAST 

But the Givati Brigade drew particular attention because of its alleged responsibility for widespread civilian casualties. The Ghosts of Gaza: Israel’s Soldier Suicides |Creede Newton |October 28, 2014 |DAILY BEAST 

His fixer, who Barfi said was affiliated with the Islamist Tawhid brigade, was set free 15 days later. Obama Administration and Sotloff Family Battle Over Blame for Journalist’s Kidnapping |Josh Rogin |September 22, 2014 |DAILY BEAST 

Even New Zealand Brigade which has been only recently engaged lost heavily and is to some extent demoralised. Gallipoli Diary, Volume I |Ian Hamilton 

Soon afterwards Cox, commanding the 29th Indian Brigade, came on board to make his salaam. Gallipoli Diary, Volume I |Ian Hamilton 

I decided after anxious searching of heart to help the French by taking over some portion of their line with the Naval Brigade. Gallipoli Diary, Volume I |Ian Hamilton 

Coming back in the evening to the ship we watched the Manchester Brigade disembarking. Gallipoli Diary, Volume I |Ian Hamilton 

Again visited Headquarters 29th Division, and afterwards walked through the trenches of the 87th Brigade. Gallipoli Diary, Volume I |Ian Hamilton