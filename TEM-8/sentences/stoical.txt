He faced his loss with stoical fortitude, as I believe he would have confronted any disaster that life could bring. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

His gravity, his intellectual life, his almost stoical philosophy impressed her imagination and captivated her understanding. Madame Roland, Makers of History |John S. C. Abbott 

He studied Madame Roland with even more of stoical apathy than another man would study a book which he admires. Madame Roland, Makers of History |John S. C. Abbott 

He remembered her strange, almost stoical calm, in such contrast with her wild agitation now. They Looked and Loved |Mrs. Alex McVeigh Miller 

He had seemed at first glance a stoical person; but his deep-set, brown eyes were bright with good humor. A Hoosier Chronicle |Meredith Nicholson