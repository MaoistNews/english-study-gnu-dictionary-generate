Even then, I could see her making a plausible case that you both understood the terms of your arrangement differently, or that she was desperate and contributed as much as she could. I can’t trust my new co-worker, and I have a list of reasons. How do I tell management? |Karla Miller |February 11, 2021 |Washington Post 

Today the Fairness Doctrine is even less plausible as a ready-made solution for solving modern media problems. The Fairness Doctrine won’t solve our problems — but it can foster needed debate |Victor Pickard |February 4, 2021 |Washington Post 

That gave him his 15th major title and his first since 2008, putting Jack Nicklaus’s record of 18 within plausible reach. Tiger Woods undergoes back surgery again and is sidelined for at least several weeks |Des Bieler |January 20, 2021 |Washington Post 

Of course, “biologically plausible” is not a standard likely to inspire a fantastic degree of public confidence. The U.K. is delaying the second dose of COVID vaccines to 12 weeks. Is that even safe? |Katherine Dunn |January 12, 2021 |Fortune 

Based on the energy required to make the clouds so big and so hot, there are two plausible sources. Galaxy-Size Bubbles Discovered Towering Over the Milky Way |Charlie Wood |January 6, 2021 |Quanta Magazine 

This may be the case—but it is not remotely plausible evidence that this attack was therefore orchestrated by North Korea. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

The notion that Ebola might be a sexually transmitted disease remains plausible if unproven. Did One Liberian Prostitute Give Ebola to Eight Soldiers? |Kent Sepkowitz |October 7, 2014 |DAILY BEAST 

And the more disconnected we become from the era of the Civil War, the more abstract and plausible the idea of secession becomes. America’s Slumbering Secession Obsession |James Poulos |September 23, 2014 |DAILY BEAST 

At the same time, they said it was plausible that pot could be disrupting brain development in teenagers. Fact-Checking the Sunday Shows: July 27 |PunditFact.com |July 27, 2014 |DAILY BEAST 

Even when plausible deniability crumbles, the brainwashed paste it back together again. This 1979 Novel Predicted Putin’s Invasion Of Crimea |Michael Weiss |May 18, 2014 |DAILY BEAST 

And our surroundings at that particular moment were not the most favorable to coherent thought or plausible theory-building. Raw Gold |Bertrand W. Sinclair 

It is well known that these declarations of science are mere speculations, plausible indeed, but nowhere proven to be true. Gospel Philosophy |J. H. Ward 

The old folks discussed it, and hope made it seem more and more plausible to them. Scattergood Baines |Clarence Budington Kelland 

It really did not look plausible that he would come out in the drizzle to see if Foster's car was safely locked in for the night. Cabin Fever |B. M. Bower 

His inventive faculties and his plausible eloquence were no more; and he seemed to have sunk into second childhood. The History of England from the Accession of James II. |Thomas Babington Macaulay