Newman again starred in the film, which this time bore only the faintest resemblance to the novel. ‘The Queen’s Gambit’ is a bestseller, but its author, Walter Tevis, was hardly a one-hit wonder |Michael Dirda |February 3, 2021 |Washington Post 

McConnell’s relationship to the extremists in his party bears a striking resemblance to Douglas’s. Mitch McConnell has repeated Stephen A. Douglas’s biggest mistake |Lauren Haumesser |January 12, 2021 |Washington Post 

Today’s food system bears little resemblance to the one of just a couple of generations ago. Blessed are the hungry? Not yet |Katie McLean |December 18, 2020 |MIT Technology Review 

They finished with one completion and two interceptions in a game that bore scant resemblance to NFL football. What to know from NFL Week 12: Tyreek Hill and Derrick Henry took over as the coronavirus loomed |Adam Kilgore |November 30, 2020 |Washington Post 

The team they will put on the field Thursday night will bear little resemblance to the one that beat the Packers in January. Ravaged by injuries and coronavirus issues, the 49ers had little chance against the Packers |Mark Maske |November 6, 2020 |Washington Post 

He does not admit it, but Miyazaki likely sees the resemblance between himself and Horikoshi as well. Anime King Hayao Miyazaki’s Cursed Dreams |Melissa Leon |December 2, 2014 |DAILY BEAST 

The eventual deal, approved by Law 840 in June 2013, bore little resemblance to the original. China’s Nicaragua Canal Could Spark a New Central America Revolution |Nina Lakhani |November 30, 2014 |DAILY BEAST 

In 2010, Jake Holmes sued over “Dazed and Confused,” claiming it bore a strong resemblance to his own song of the same name. ‘No Stairway, Denied!’ Led Zeppelin Lawsuit Winds on Down the Road |Keith Phipps |October 22, 2014 |DAILY BEAST 

Rachel starts dating a guy named Russ, and would you believe that he bears a striking resemblance to Ross? 15 Times ‘Friends’ Was Really, Really Weird |Kevin Fallon |September 18, 2014 |DAILY BEAST 

The second portrait, examined by the Mona Lisa Foundation, bears a striking resemblance to the famous portrait. The Life of Lisa Gherardini del Giocondo, the (Most Likely) Real 'Mona Lisa' |Justin Jones |August 9, 2014 |DAILY BEAST 

The vision of dreams is the resemblance of one thing to another: as when a man's likeness is before the face of a man. The Bible, Douay-Rheims Version |Various 

Eve, too, lovely as she is, seems to bear no likelihood of resemblance to Milton's superb mother of mankind. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

We revelled in its resemblance, or its fancied resemblance to the famous old hostelry kept by old John Willet. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

It is interesting to notice a resemblance between this huge bird and our English wild duck or plover. Hunting the Lions |R.M. Ballantyne 

She was fair as a lily, with bright golden hair, and bore no resemblance to this dark-eyed, black-browed wench. The World Before Them |Susanna Moodie