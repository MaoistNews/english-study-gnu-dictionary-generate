In September of last year, Delish partnered with Enlightened, a keto-friendly ice cream company, to launch two flavors. Hearst turns to first-party data to look for new brand licensing categories |Erika Wheless |February 10, 2021 |Digiday 

Authorities in Moscow are offering free ice cream just to get people to come in and take their shot. Why South Africa stopped using the AstraZeneca COVID-19 vaccine |Rahul Rao |February 8, 2021 |Popular-Science 

Serve with a dollop of softly whipped cream and berries or a scoop of vanilla ice cream. If loving a piping hot chocolate lava cake is wrong, I don’t want to be right |Becky Krystal |February 4, 2021 |Washington Post 

A dietitian I was working with suggested, reasonably, that I loosen those self-imposed rules and focus on eating whatever I actually wanted to eat that wouldn’t do harm, even if that was a pint of ice cream. Six Months of Soft Food Set Me Free |Meghan McCarron |February 2, 2021 |Eater 

We are in a very fortunate position to have a lot of eyes on us at any given moment, and we’re across the street from the beach, and people come pick up ice cream and go down to the beach, or pick up food. How One Instagram Post Inspired Diners From Around the World to Donate Restaurant Meals |Amanda Kludt |January 27, 2021 |Eater 

Lalo said he reported the kidnapping to his ICE handlers, which was confirmed by a former federal agent familiar with the case. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

But Huckabee (akin to Elizabeth Warren on the left) is like an ice cream sundae. The Devil in Mike Huckabee |Dean Obeidallah |January 6, 2015 |DAILY BEAST 

Just who is crazy enough to go swimming when the pond across the street has a layer of ice across the top? Diving Into 2015 With Polar Bear Plunge Extremists |James Joiner |January 1, 2015 |DAILY BEAST 

Crew members had to cut through the ice on the streets to get shots. Speed Read: The Juiciest Bits From the History of ‘Purple Rain’ |Jennie Yabroff |January 1, 2015 |DAILY BEAST 

Serve with the warm sauce and your choice of ice cream, whipped cream, or yogurt. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

He leant against the wall of his refuge, notwithstanding this boast, and licked the ice to moisten his parched lips. The Giant of the North |R.M. Ballantyne 

The left heel followed like lightning, and the right paw also slipped, letting the bear again fall heavily on the ice below. The Giant of the North |R.M. Ballantyne 

A long stretch of smooth ice followed, over which he glided with ever-increasing speed. The Giant of the North |R.M. Ballantyne 

Profiting by this, Benjy quietly moved away round a colossal buttress of the berg, and took refuge in an ice-cave. The Giant of the North |R.M. Ballantyne 

The smile was still on his lips when his head drooped on a piece of ice, and he sank into a deep slumber. The Giant of the North |R.M. Ballantyne