On a recent call from a suburb of Chicago, where she’s been living alone throughout the pandemic, she sounded defeated. My mother and her friends couldn’t get coronavirus vaccine appointments, so they turned to a stranger for help. He’s 13. |Greg Harris |February 12, 2021 |Washington Post 

After a recent defeat to Bayern Munich, he made a point of seeking out superstar Robert Lewandowski and requesting his jersey. Matthew Hoppe was a little-known American soccer player — until he reached the Bundesliga |Steven Goff |February 11, 2021 |Washington Post 

So we asked Science News readers their views about recent neurotechnology advances. Can privacy coexist with technology that reads and changes brain activity? |Laura Sanders |February 11, 2021 |Science News 

The recent state commission meeting previewed how some of the translation issues might cause problems. African Communities Warn Language Issues Could Shut Them Out of Redistricting |Maya Srikrishnan |February 10, 2021 |Voice of San Diego 

A recent GitHub study shows that open-source project creation is up 25% since April of last year. The rise of the activist developer |Walter Thompson |February 9, 2021 |TechCrunch 

In fact, in a recent study of their users internationally, it was the lowest priority for most. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

The comedian responded to the deadly attack on a French satirical magazine by renewing his recent criticisms of the Islamic faith. Bill Maher: Hundreds of Millions of Muslims Support Attack on ‘Charlie Hebdo’ |Lloyd Grove |January 8, 2015 |DAILY BEAST 

The most recent issue contains detailed instructions for building car bombs, and the magazine frequently draws up hit-lists. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

Spouting off against police online has become criminalized in recent weeks. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

The most recent activity had a high point of 3.6 on the Richter Scale. 26 Earthquakes Later, Fracking’s Smoking Gun Is in Texas |James Joiner |January 7, 2015 |DAILY BEAST 

They are unquestionably penitent now; but then, you know, they have the recollection of very recent suffering fresh upon them. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

The recent concessions made by the Spanish Government have been seen by the Philippine public. The Philippine Islands |John Foreman 

Recent events have done much to introduce Korea and its people to the world at large. Our Little Korean Cousin |H. Lee M. Pike 

Besides, that inspection did not pertain to me, since I was neither the oldest nor the most recent auditor. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

At this stock-taking the number unaccounted-for is twenty-two, several of which are quite recent accessions to the Library. Report of the Chief Librarian for the Year 1924-25 |General Assembly Library (New Zealand)