Neuralink, in particular, aims to build an incredibly powerful brain-machine interface, a device with the power to handle lots of data that can be inserted in a relatively simple surgery. Elon Musk is one step closer to connecting a computer to your brain |Rebecca Heilweil |August 28, 2020 |Vox 

The damage was so severe that he needed immediate surgeries to keep him alive. Misery in Minsk |Eugene Robinson |August 18, 2020 |Ozy 

Ben Simmons is going to miss at least several weeks after having surgery on his knee. The Suns Are On Fire, The Sixers Are Reeling, And Other Lessons From The NBA Bubble |Chris Herring (chris.herring@fivethirtyeight.com) |August 13, 2020 |FiveThirtyEight 

As India’s lockdown lifts, elective surgeries are getting back on the track. India’s overburdened hospitals could actually benefit from more elective surgeries |Ananya Bhattacharya |August 10, 2020 |Quartz 

The other significant addition was Bojan Bogdanović — Utah’s second-leading scorer — and he’s now out for the playoffs thanks to wrist surgery. Can Donovan Mitchell Reach His Potential In The Bubble? |Michael Pina |August 3, 2020 |FiveThirtyEight 

When the father arrived at the hospital, he was told that Andrew Dossi was in surgery, but the wounds were not life-threatening. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

He was also a charismatic, telegenic speaker with a face improved by plastic surgery several years earlier. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

“As far as we were aware, and as far as the surgeon was aware, the surgery was a go,” Shaheen sighs. The Insurance Company Promised a Gender Reassignment. Then They Made a Mistake. |James Joiner |December 29, 2014 |DAILY BEAST 

Internet chatter rose to a deafening roar as speculation began about what—plastic surgery? Butts, Brawls, and Bill Cosby: The Biggest Celebrity Scandals of 2014 |Kevin Fallon |December 27, 2014 |DAILY BEAST 

As I sign the forms to be admitted to have surgery the next day, I ask my husband the date. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

Insult and outrage seemed to have given that bodily vigour to Ripperda, which medicine and surgery had taken no pains to restore. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

He went at his job with a handy adroitness which was almost scientific, it was so much like surgery, like dissection. Overland |John William De Forest 

I have no doubt that unpretending woman knows more about surgery than all the men doctors in New York city. Overland |John William De Forest 

Moreover, as Dr. Lorien had stated, and as Leonard had found by actual experience, he was skilled in medicine and surgery. The Devil-Tree of El Dorado |Frank Aubrey 

Medicine and surgery—well, there's a huge program of compulsory sterilization, and another one of eugenic marriage-control. Hunter Patrol |Henry Beam Piper and John J. McGuire