Have you looked around the American Dental Association website for an explanation of how fluoridation actually works? Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Added to drinking water at concentrations of around one part per million, fluoride ions stick to dental plaque. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Then Ziegler tosses the buff LaBeouf around like a rag doll. Sia and Shia LaBeouf’s Pedophilia Nontroversy Over ‘Elastic Heart’ |Marlow Stern |January 9, 2015 |DAILY BEAST 

In straight relationships with an age gap, words like ‘gold-digger’ and ‘trophy wife’ get thrown around. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

Toomey glides around the room like a Brazilian capoeira dancer. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

Descending the Alps to the east or south into Piedmont, a new world lies around and before you. Glances at Europe |Horace Greeley 

Davy looked around and saw an old man coming toward them across the lawn. Davy and The Goblin |Charles E. Carryl 

This, however, did not apply to the waters lying directly around the Poloe and Flatland groups. The Giant of the North |R.M. Ballantyne 

At present, Louis was too self-absorbed by the struggles within him, to look deep into what was passing around him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

While they were doing this, he assembled the officers around him, and the meaning of our night march was explained to us. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various