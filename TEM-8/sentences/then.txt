So then-President George H.W. Bush and other prominent Republicans endorsed Treen in the House runoff. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

MIAMI — Fidel Castro seized power in January 1959 after waging a guerilla war against then-dictator Fulgencio Batista. Cuba Is A Kleptocracy, Not Communist |Romina Ruiz-Goiriena |December 19, 2014 |DAILY BEAST 

I gave statements to then-Captain Michael Sheffield on several different occasions. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

The certification, which lasts three years, was renewed by then-Defense Secretary Leon Panetta in 2012. The Detainee Abuse Photos Obama Didn’t Want You To See |Noah Shachtman, Tim Mak |December 15, 2014 |DAILY BEAST 

We won an unprecedented pardon from then-President Karzai, and Gulnaz was freed. We’re Not Done in Afghanistan |Kimberley Motley |December 5, 2014 |DAILY BEAST 

England alone-then a comparatively weak and insignificant country-stood out against this overwhelming combination. Is Ulster Right? |Anonymous 

Do we not see that it is highly requisite that a sweet girl should be now-and-then drawn aside by him? Clarissa, Volume 6 (of 9) |Samuel Richardson 

Have you now related all the conversation you had at that time with then-Vice President Johnson? Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy 

And what conversations, if any, did you have with then-Vice President Johnson? Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy 

Wouldst find the fair lady Fate chooses for you?Then search ye this line of wee pin-pricks clear through. Suppers |Paul Pierce