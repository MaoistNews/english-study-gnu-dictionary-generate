Every year, we catalogue the best columns of the year here at The Daily Beast. The Best Columns of 2014 |John Avlon, Errol Louis |December 31, 2014 |DAILY BEAST 

It was like a constant assault, an almost stupefying catalogue of mindless racial insult and injury. How Martin Luther King Jr. Influenced Sam Cooke’s ‘A Change Is Gonna Come’ |Peter Guralnick |December 28, 2014 |DAILY BEAST 

Catalogue, published by Flammarion (2014), includes photographs from the Magnum Photos archives and text by Éric Hazan. A History of Paris in 150 Photographs |Sarah Moroz |December 14, 2014 |DAILY BEAST 

The accompanying 184 page catalogue includes 154 photos, of which 150 have not previously been published. The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 

Meanwhile CBS announced a similar deal this year that will offer their catalogue of shows online for a monthly fee. Binge Watching is the New Bonding Time |The Daily Beast |December 10, 2014 |DAILY BEAST 

Within a hundred yards I met my two friends, Varnhorst and Guiscard, and poured out my whole catalogue of wrongs at once. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

This insect appears to be a Chrysolopus in M. Dejean's Catalogue. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

He published a catalogue of all the works known to have been printed from the invention of the art of printing to the year 1536. The Every Day Book of History and Chronology |Joel Munsell 

Bud set him down on the bunk, gave him a mail-order catalogue to look at, and went out again into the storm. Cabin Fever |B. M. Bower 

Is this long catalogue of proofs of such a nature as to inspire us with great confidence in the hidden views of the Divinity? Superstition In All Ages (1732) |Jean Meslier