A pliable EVA midsole makes for easy walking and pedaling, and the lugs excel on ice. The Best Winter Cycling Kit of 2022 |jversteegh |October 26, 2021 |Outside Online 

Testers found the horizontal lugs in the natural rubber outsole provided solid grip in slushy snow. The Best Men’s Après Kit of 2022 |jversteegh |October 26, 2021 |Outside Online 

Send your fully rigid alpine boots to them and they’ll kit them out with tech fittings in the toe lugs so that you can put in a short tour. Our Ski-Test Director’s Go-To Backcountry Kit |agintzler |October 14, 2021 |Outside Online 

Oh, and make sure your shoes are up to the task—aggressive lugs and plenty of cushioning are key. How to Up Your Trail-Running Game |kklein |July 21, 2021 |Outside Online 

The binding is compatible with regular alpine boots, GripWalk boots with tech fittings, and touring-specific boots with toe lugs. Marker’s New Touring Binding Does (Almost) Everything |Marc Peruzzi |February 22, 2021 |Outside Online 

Darren was the kind of man who'd lug a sofa bed up three flights of stairs without a whisper of complaint. Let Us Now Praise Famous Rednecks and Their Unjustly Unsung Kin |Allison Glock |August 23, 2014 |DAILY BEAST 

I mean, what an incredibly annoying gift to have to lug home. Prince George Given Giant Toy Wombat |Tom Sykes |April 16, 2014 |DAILY BEAST 

No bulky helmet to lug around or uncomfortable shell to ruin your hairstyle. Helmet Haute Couture: The Invisible Helmet Revolutionizing Bike Safety |Nina Strochlic |December 19, 2013 |DAILY BEAST 

The macho guys could lug up buckets of water, which are sorely needed to flush toilets. Make the New York City Marathon a Help-a-Thon |Michael Daly |November 3, 2012 |DAILY BEAST 

The clip played with the belabored simplicity of a silent movie—one lug and two thugs, brandished gun and chain-snatch. To Shoot or Not to Shoot |Edward Conlon |April 14, 2011 |DAILY BEAST 

In this case a lug L is cast upon the block, forming, indeed, a portion of said block. The Recent Revolution in Organ Building |George Laing Miller 

After getting well away from the beach he hoisted a small lug-sail, and stood out to sea. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

It takes a full day to lug enough water up to the canyon levels to wash out a pan of gravel. David Lannarck, Midget |George S. Harney 

They would say, 'We shall get our land for potato-ground at 1/2 d. a lug, instead of paying 3d. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

We want to lug this h-yer money up stairs and count it before everybody—then ther' ain't noth'n suspicious. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens)