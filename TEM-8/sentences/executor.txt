Then, a few years before the Civil War broke out, he became the executor of his father-in-law’s estate, which included nearly 200 enslaved people. Let’s get real about Robert E. Lee and slavery |Gillian Brockell |September 10, 2021 |Washington Post 

Of course, moderators have long existed in the offline world—lawmakers, police, and the courts can all be understood to be the executors of a kind of moderation power. The Capitol Attack, Impeachment and GameStop Make it Clear: 2021 Is Shaping Up to Be the Year of the Moderator |Alex Fitzpatrick |January 29, 2021 |Time 

If I had to sum up the people that work at Rocket Lab, they’re just pure executors. Peter Beck on Rocket Lab’s expanding orbit |Devin Coldewey |December 18, 2020 |TechCrunch 

There’s always a partnership of a visionary and an executor, of a visionary and an operator who work in concert to advance something greater. Simon Sinek Says: Go Forth and Be Fantastic! |Eugene Robinson |October 9, 2020 |Ozy 

At that point an executor will be in charge of activating the service. New Services Immortalize Tweets |Nina Strochlic |March 1, 2013 |DAILY BEAST 

In September, during a hike in Franklin Park, Chasen told Smilgis that there was a new will and that she was still co-executor. New Clue in Chasen Murder |A. L. Bardach |December 6, 2010 |DAILY BEAST 

If the lessee die, his executor or administrator can assign the remainder of his term. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Or, if the land has been given to a devisee, he can require the executor or administrator to pay the mortgage. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

If dead or insane, the oath must be made by his executor, administrator, or other representative. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The hard-hearted executor of the law was brought within the influence of her enchantment. Madame Roland, Makers of History |John S. C. Abbott 

An executor would be admitted to give evidence of the validity of a will, which he could not do at present. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan