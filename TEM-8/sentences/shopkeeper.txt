Her father was a shopkeeper, at times selling deli food, at times dry goods or clothing. Gladys Stern, who led Georgetown Day School, dies at 104 |Bart Barnes |November 18, 2021 |Washington Post 

Here there are artists, athletes, teachers, shopkeepers and laborers who only want to live a decent life in one of the oldest and greatest civilizations on earth. Iran Is Too Often Viewed Through the Lens of Our Rulers. Try Looking Through the Eyes of Our People |Tara Kangarlou |June 18, 2021 |Time 

“We’ll find out who the winners are,” the shopkeeper says, “when they quietly move away.” Powerball mystery: Someone in this tiny town won $731 million. Now everyone wants a piece of it. |Marc Fisher |June 17, 2021 |Washington Post 

Gold Spa never attracted much attention from its neighbors, shopkeepers said. A nationwide horror: Witnesses, police paint a picture of a murderous rampage that took 8 lives |Tim Craig, Mark Berman, Hannah Knowles, Marc Fisher |March 18, 2021 |Washington Post 

Mannar took a car to a local market and strolled around, polling the shopkeepers selling salt on where they got it. One man’s crusade to end a global scourge with better salt |Katie McLean |December 18, 2020 |MIT Technology Review 

“We were inside and heard a noise much louder than usual,” said one shopkeeper, who did not want to be identified. MH17 Missile Can't Hide From These Internet Sleuths |Eliot Higgins |August 2, 2014 |DAILY BEAST 

The shopkeeper unlocked a door for us and we went down a narrow spiral staircase to a communal hole in the wall. I Heard About the Latest Crazed Shooter While I Watched the World Cup with Guys He Almost Killed |Daniel Genis |July 1, 2014 |DAILY BEAST 

Then Sheriff Onstad got a call: a country shopkeeper had found in his till a check from Nichols. The Ballad of Johnny France |Richard Ben Cramer |January 12, 2014 |DAILY BEAST 

I have been a shopkeeper since I was 21, I have worked in a shop since I was 15. 5 Questions for Paul Smith |Tom Sykes |February 17, 2013 |DAILY BEAST 

“We get airstrikes every two or three days, depending on the weather,” says Abdul, a shopkeeper. Syria’s Dying Revolution |Jamie Dettmer |December 20, 2012 |DAILY BEAST 

The shopkeeper and his wife, drawn two ways by pity and self-interest, began by lulling their consciences with words. An Episode Under the Terror |Honore de Balzac 

That class is the mercantile, or rather shopkeeper class; and with them the money power is all powerful. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various 

I go to leave an empty basket at the door, and the lantern that the Shopkeeper set in the hand of the pedlar. When Valmond Came to Pontiac, Complete |Gilbert Parker 

And now a shopkeeper has filled his window with royal Stuart tartans, and I am instantly a Jacobite. Penelope's Experiences in Scotland |Kate Douglas Wiggin 

It is delightful to hear them talk,—so different from an English shopkeeper. Paul Patoff |F. Marion Crawford