Breaking was still an activity that could be done by tossing a piece of cardboard on a sidewalk and letting the music take hold, but for many, it had graduated from the sidewalks to dance studios. How break dancing made the leap from ’80s pop culture to the Olympic stage |Rick Maese |February 9, 2021 |Washington Post 

The company said it plans to donate as many as 20,000 kits, which include a cardboard base, a 20-inch box fan and a 20-by-20-by-4 air filter. Ford’s next pandemic mission: Clear N95 masks and low-cost air filters |Hannah Denham |February 9, 2021 |Washington Post 

The stadium appeared to be full because of 30,000 cardboard cutouts. America suffered an unnecessary loss during the Super Bowl |Michele Norris |February 8, 2021 |Washington Post 

We are, in fact, not too old to join in coloring pictures, make up funny stories or helping turn cardboard boxes into secret forts. Christopher Plummer’s Captain von Trapp is the father I want to be |Bill McQuillen |February 8, 2021 |Washington Post 

The pandemic was inescapable, represented in the stands with the cardboard cutout fans, and it was visualized in the halftime show, with the dancers’ plague doctor-esque masks. The 8 best Super Bowl commercials, from an ‘Edward Scissorhands’ sequel to Michael B. Jordan’s Alexa |Sonia Rao, Maura Judkis |February 8, 2021 |Washington Post 

Host and guest pop and lock on cardboard—the only proper surface for breakdancing—and in no way did they have dancing doubles. Breakdancing Brad Pitt, Chainsaw Massacre Prank, and More Viral Videos |The Daily Beast Video |October 19, 2014 |DAILY BEAST 

A heavy trowel is used to retrieve a mass of your chosen kind of ice cream from its tub and put it in the cardboard container. Dr. Mike’s Makes the Best Ice Cream on Earth |Jane & Michael Stern |July 27, 2014 |DAILY BEAST 

From an old cardboard box, Li Wen drew a clear glass bottle filled with a dark brown liquid. China Is Brewing Wine From Tiger Bones |Brendon Hong |July 22, 2014 |DAILY BEAST 

Back at the Coop, food is presented on heavy cardboard plates. Charlottesville Is Swimming in Finger Lickin’ Gas Station Fried Chicken |Jane & Michael Stern |May 26, 2014 |DAILY BEAST 

By the end of the day, six more bodies were found, each wrapped in clothing and blankets, each in its own cardboard box. Utah’s Murderer Mom Is a Monster but She’s Not the First |Steve Miller |April 16, 2014 |DAILY BEAST 

He further proposes to use it in the form of a solid cardboard as a roofing material for light structures. Asbestos |Robert H. Jones 

The application is made in a paper machine, the pulp being allowed to flow over the cardboard. Asbestos |Robert H. Jones 

On the stall are baby dolls with bodies made of grey cardboard, smiling after the manner of idols, monstrous and serene as they. Marguerite |Anatole France 

In doing so, to his dismay, he upset a couple of old cardboard boxes filled with letters, and they fell with some clatter. Marriage la mode |Mrs. Humphry Ward 

It is true that it might have lain under the setting of the frame, hidden beneath the protecting cardboard mat. Urania |Camille Flammarion