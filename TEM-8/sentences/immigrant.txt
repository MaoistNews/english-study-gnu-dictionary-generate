Pauline Binam has come forward after a whistleblower complaint on hysterectomies of immigrant detainees in Georgia. A woman in ICE detention says her fallopian tube was removed without her consent |Nicole Narea |September 17, 2020 |Vox 

Last Friday, lawyers filed a habeas petition in federal court asking that the woman be freed on supervised release and held in an immigrant shelter in El Paso. ICE Deported a Woman Who Accused Guards of Sexual Assault While the Feds Were Still Investigating the Incident |by Lomi Kriel |September 15, 2020 |ProPublica 

Of the eight teams in the Eastern Midwest League from Ohio, Kentucky, Indiana and Michigan, immigrants last year made up 34 percent of the playing roster, though those four states have an average per capita immigrant population of just 5 percent. Can Small-Town America Survive Pandemic’s Hit to Minor League Baseball? |Charu Kasturi |September 14, 2020 |Ozy 

I was assigned to read the book a couple of years after arriving as an immigrant kid from the Philippines. Book recommendations from Fortune’s 40 under 40 in government and policy |Rachel King |September 10, 2020 |Fortune 

Born in Manhattan in 1917 to a pair of poor Jewish immigrants from Russia, Sara Finkelstein was a real 20th-century thinkfluencer right from the start. Your car is probably full of spiders |PopSci Staff |September 2, 2020 |Popular-Science 

He was born in an apartment above the grocery store owned by his immigrant parents in South Jamaica, Queens. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 

None of these writers set out to write an “immigrant novel,” or to make political statements. The 2014 Novel of the Year |Nathaniel Rich |December 29, 2014 |DAILY BEAST 

He was treated like an immigrant, working for minimum wage, missing his family and having to move on from his musical career. Cuban Hip-Hop Was Born in Alamar |Daniel Levin |December 26, 2014 |DAILY BEAST 

Over the years, this country has offered many of its immigrant groups a remarkable opportunity for reinvention. COEXIST’s Bonehead Bumper-Sticker Politics |Michael Schulson |December 21, 2014 |DAILY BEAST 

Former President Nicolas Sarkozy used it to cultivate right-wing anti-immigrant voters. Abu Dhabi Stabbing: Why Law Enforcement Hates The Niqab & Burqa |Christopher Dickey |December 3, 2014 |DAILY BEAST 

We assimilate anything white so quickly it is a wonder an immigrant remembers the native way of pronouncing his own name. Ancestors |Gertrude Atherton 

The old characteristics must needs hang about the newly-arrived immigrant. American Sketches |Charles Whibley 

So between boss and immigrant grows up a relation like that between a feudal lord and his vassals. The Old World in the New |Edward Alsworth Ross 

In return for the boss's help and protection, the immigrant gives regularly his vote. The Old World in the New |Edward Alsworth Ross 

If the immigrant is neither debauched nor misled, but votes his opinions, is he then an element of strength to us? The Old World in the New |Edward Alsworth Ross