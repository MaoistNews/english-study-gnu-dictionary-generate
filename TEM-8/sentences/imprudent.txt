In her memo, Elliott emphasized that the settlement “would not put an end to an ugly chapter in its history stemming from highly imprudent decisions made during the prior mayoral administration.” What’s In the City’s Proposed 101 Ash Settlement |Lisa Halverstadt |June 25, 2022 |Voice of San Diego 

I now realize that this discussion was imprudent for many factors, including the age of the students, my demeanor and insensitivity. Tensions in the U.S. Catholic Church over abortion, race and politics come to a head in a Maryland classroom |Brittany Mayes |May 28, 2021 |Washington Post 

Marine One’s crew was saying that bad visibility could make it imprudent to chopper to the cemetery. Did Trump call US war dead “losers” and “suckers”? The controversy, explained. |Alex Ward |September 4, 2020 |Vox 

It is nevertheless imprudent to fight illegal immigration with laws targeting otherwise law-abiding residents with arrest. Immigration Policy Gone Loco |Conor Friedersdorf |April 17, 2010 |DAILY BEAST 

Can there be any doubt that it is imprudent to kill alleged traitors without even bothering to convict them? Assassinating Americans |Conor Friedersdorf |February 7, 2010 |DAILY BEAST 

The parents were well to do, and in due time forgave the imprudent match. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Bluebeard, you know, had a whole museum of them—as that imprudent little last wife of his found out to her cost. A Selection from the Works of Frederick Locker |Frederick Locker 

His life and conduct subsequently was extremely imprudent and reprehensible. The Every Day Book of History and Chronology |Joel Munsell 

The cubicula were also defaced, their symmetry injured, and their construction endangered by similar imprudent excavations. The Catacombs of Rome |William Henry Withrow 

The story is characteristic of the gallant but imprudent man who played so great a part in Irish history. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell