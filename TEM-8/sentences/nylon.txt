Made in Vermont from rugged goat leather, and hand-stitched with thick nylon thread to prevent seam tears, they’re the most durable work gloves I’ve ever tested. My Favorite Winter Gloves for Various Activities |Jakob Schiller |February 3, 2021 |Outside Online 

Made from a soft, lightweight combination of nylon and spandex, it offers UPF 50 protection for sunny days when I want a little extra shielding from the sun between sets. The Surf Gear I Rely on from Women-Owned Companies |Rebecca Parsons |January 30, 2021 |Outside Online 

Instead of traditional sidearms, the company uses a thin nylon strap to hold them on your face, like a permanent version of sunglasses straps that you can tighten. Ombraz's Armless Sunglasses Are a Mixed Bag |Graham Averill |January 22, 2021 |Outside Online 

This chemical carried the acid out of the nylon thread as it dried. Bye-bye batteries? Power a phone with fabric or a beacon with sound |Kathryn Hulick |January 6, 2021 |Science News For Students 

This chair is just big enough for two people, with dual beverage holders on either arm, and it’s made from tough nylon fabric that’s sure to withstand years of abuse. The Most Coveted Gear of 2020, According to Our Editors |Will Taylor, Jeremy Rellosa, Ariella Gintzler, and Maren Larsen |December 9, 2020 |Outside Online 

Alice wore a black nylon rain jacket that looked as if it was ill prepared to deal with the coming chill. The Wildly Peaceful, Human, Almost Boring, Ultimately Great New York City Protests for Eric Garner |Mike Barnicle |December 8, 2014 |DAILY BEAST 

They are unapologetically uncool, the men in white nylon jumpsuits and the women in “artsy” drop earrings. Sneer and Clothing in Miami: Inside The $3 Billion Woodstock of Contemporary Art |Jay Michaelson |December 6, 2014 |DAILY BEAST 

Four of them carried a thick black nylon body bag, two to a side, and loaded it into the middle of the hull. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

The mother unties the palm staves from the mattress, then takes the nylon ropes and ties the mattress to the boat. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

Other techniques included laser-cut laces, braiding, and high-gloss liquid fabrics made from silk and nylon. Iris Van Herpen Spring/Summer 2014: Sonic Youth |Liza Foreman |October 1, 2013 |DAILY BEAST 

Conservatively dressed—matching coat and trousers of orange nylon tweed—royal blue half-brim bowler—carrying a blue brief case. The Penal Cluster |Ivar Jorgensen (AKA Randall Garrett) 

From his pocket he took a long strip of cloth and some of the tough nylon cords from the net. Stalemate |Basil Eugene Wells 

I gave her a nylon nightgown and a little nylon coat that went on and she was sitting and touching it. Warren Commission (9 of 26): Hearings Vol. IX (of 15) |The President's Commission on the Assassination of President Kennedy 

He was firmly attached to the ship by a long nylon rope which he hooked to rings set on the outer shell. The Secret of the Ninth Planet |Donald Allen Wollheim 

Then he hastily drew off his left gauntlet and the thin nylon glove that was the inner protection of his suit. The Secret of the Ninth Planet |Donald Allen Wollheim