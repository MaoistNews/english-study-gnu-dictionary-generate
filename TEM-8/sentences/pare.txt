Closures of the Mall would not alter plans for the inauguration ceremony, which had already been pared back because of the coronavirus pandemic, said a senior official with the committee planning the event. Inauguration Day closures expected to include the Mall, major bridges |Emily Davies, Justin Jouvenal |January 15, 2021 |Washington Post 

The company first set out to build an electric car in 2014, hiring hundreds of hardware engineers for the effort before rapidly paring it back around 2016 to focus on the self-driving car system. Apple’s self-driving electric car is at least half a decade away |Verne Kopytoff |January 7, 2021 |Fortune 

By the end of this weekend, the playoff field will have been pared to eight teams. The NFL made it to the playoffs despite the coronavirus. But plenty of obstacles remain. |Mark Maske |January 7, 2021 |Washington Post 

The largest cryptocurrency reached an all-time high of $28,365 on Sunday before paring some of the advance, according to a composite of prices compiled by Bloomberg. With $30,000 Bitcoin in sight, analysts wonder how high it could go |Bernhard Warner |December 28, 2020 |Fortune 

We quickly pared that back to the things that were working, and email was at the top of the list. Letterhead wants to be the Shopify of email newsletters |Eric Eldon |December 23, 2020 |TechCrunch 

In retirement, Frank is consciously trying to pare down and rein in. Richard Ford’s Artful Survivalist Guide: The Return of Frank Bascombe |Tom LeClair |November 4, 2014 |DAILY BEAST 

After food stamp usage hit record-breaking numbers in 2013, Congress tried to pare back the benefit. The People Vs. the Bank of Walmart |James Poulos |October 1, 2014 |DAILY BEAST 

Those measures, combined with continued sustained growth and increasing taxes, have helped pare the deficit. A Debt Deal Won’t Save Us |Daniel Gross |October 15, 2013 |DAILY BEAST 

Jessica Pare serenaded Don Draper on Mad Men but the fascination with her rendition of “Zou Bisou Bisou” is still a bit baffling. CFDA Honors Behind-the-Scenes Fashion Stars and Understated Style |Robin Givhan |June 5, 2012 |DAILY BEAST 

How have we let social interaction pare us down to fewer than 140 keystrokes? Let's Stop the Twitter Madness |Lee Woodruff |February 28, 2009 |DAILY BEAST 

Ambroise Pare, standing in a corner, caught a glance which the duke cast upon him, and immediately advanced. Catherine de' Medici |Honore de Balzac 

No sooner was he in the open street than Ruggiero took his arm and asked by what means Ambroise Pare proposed to save the king. Catherine de' Medici |Honore de Balzac 

"Your reign is over, messieurs," said Catherine to the Guises, seeing from Pare's face that there was no longer any hope. Catherine de' Medici |Honore de Balzac 

Pare your pineapple; cut it in small pieces, and leave out the core. A Poetical Cook-Book |Maria J. Moss 

The old lawyer sat down, took a penknife from a drawer, and throwing himself back in his chair, began to pare his nails. The Mynns' Mystery |George Manville Fenn