For something with a little more grip, Kitchen-pro’s chainmail comes wrapped around a silicone insert. Metal scrubbers to keep your cast iron skillet in pristine condition |PopSci Commerce Team |September 23, 2020 |Popular-Science 

With practical removable laundry bag inserts or collapsible options, there are ways to improve function and style. Attractive laundry hampers that make your dirty clothes look a little better |PopSci Commerce Team |September 16, 2020 |Popular-Science 

While excipients are listed on packaging or package insert for over-the-counter and prescription drugs, this information can be difficult to find. The ‘inactive' ingredients in your pills could harm you |By Yelena Ionova/The Conversation |September 15, 2020 |Popular-Science 

The latter includes the game case and any manuals or inserts that would typically come included with the title. Pick up The Last of Us Part II for $10 off—or take $30 off a used copy |Ars Staff |September 3, 2020 |Ars Technica 

Outfits like the Manufacturing Solutions Center are also uniquely qualified to develop a new generation of higher-performance cloth masks, or ones that use small filter inserts to stretch scarce materials further. Unmade in America |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

Mixner would insert it via an IV and hold his friend until the drip was completed. Gay Activist David Mixner: I Mercy Killed 8 People |Tim Teeman |October 29, 2014 |DAILY BEAST 

These tend to arrive when Shuler tempers his impulse to insert himself into the action. The Twisted History of the Noose |Kevin Canfield |August 27, 2014 |DAILY BEAST 

Klinsmann may also insert Aron Johannsson at some point if the Americans need added juice up front. USA vs. Germany World Cup Primer: Everything You Need to Know About the Epic Showdown |Marlow Stern |June 26, 2014 |DAILY BEAST 

The state likely will lead in the countervailing lawsuits when (insert a horrid disease here) is found to be linked to e-puffing. Big Tobacco, Not MRSA, Is the Real Problem With E-Cigarettes |Kent Sepkowitz |May 20, 2014 |DAILY BEAST 

Insert your own cognitive disabilities from head-butting in the House of Representatives joke. Up to a Point: The U.S. Government’s Minimum Wage Is $430 Million Per Hour |P. J. O’Rourke |March 21, 2014 |DAILY BEAST 

All insert yis (or yes) before parde; which spoils both sense and metre. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

All insert white after Was, which spoils metre and story (see l. 948). Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

All badly insert pure (dissyllabic) before flat; but smothe has two syllables. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

At the head they insert a bamboo knot, with its point well sharpened into two edges. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Those chiefs generally insert gold in the teeth, which is so well fitted that it does not hinder their talking or eating at all. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various