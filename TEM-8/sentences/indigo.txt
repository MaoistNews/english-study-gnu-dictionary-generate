The canvas comes in multiple colors—like indigo, stone, and hand-dyed turmeric—so you can match this sauna kit with your current wall color or room’s aesthetic. Best portable saunas of 2022 |Hannah Singleton |June 7, 2022 |Popular-Science 

To give denim its blue hue, the researchers add indigo powder to a hydrogel containing a small amount of nanocellulose. Scientists find a ‘greener’ way to make jeans blue |Shi En Kim |November 3, 2021 |Science News For Students 

Denin absorbed more indigo in one pass with the new dye than it would have picked up after being dipped in the traditional vat of dye eight times. Scientists find a ‘greener’ way to make jeans blue |Shi En Kim |November 3, 2021 |Science News For Students 

Textile makers must treat indigo with harsh chemicals to make it soluble. Scientists find a ‘greener’ way to make jeans blue |Shi En Kim |November 3, 2021 |Science News For Students 

Your happiness as a reader will depend on how open you are to insights that recognize no coincidences, some of them from the crystal-indigo-rainbow file, as well as proposed, though not explained, secret-knowledge theories. The tale of a bass player, sonic epiphanies and a quest to save ‘real music’ |Ben Ratliff |February 12, 2021 |Washington Post 

With Mood Indigo, I had a girlfriend who got really sick in America and I had to pay for the hospital bills. Michel Gondry on ‘Mood Indigo,’ Kanye West, and the 10th Anniversary of ‘Eternal Sunshine’ |Marlow Stern |July 20, 2014 |DAILY BEAST 

Both Eternal Sunshine and Mood Indigo concern a relationship unraveling. Michel Gondry on ‘Mood Indigo,’ Kanye West, and the 10th Anniversary of ‘Eternal Sunshine’ |Marlow Stern |July 20, 2014 |DAILY BEAST 

“With Mood Indigo, I really let myself be as me as I could be,” Gondry tells The Daily Beast. Michel Gondry on ‘Mood Indigo,’ Kanye West, and the 10th Anniversary of ‘Eternal Sunshine’ |Marlow Stern |July 20, 2014 |DAILY BEAST 

The jeans mysteriously came in a spray can, and were offered in two washes: “Indigo,” and “Bright Light.” American Eagle's Spray-On 'Skinny Skinny' Jeans Are, Thankfully, A Joke |Isabel Wilkinson |March 22, 2013 |DAILY BEAST 

He hangs around with the supposedly psychic Indigo Children who inspired The Last Airbender. This Week’s Hot Reads: November 6, 2012 |Mythili Rao |November 7, 2012 |DAILY BEAST 

The indigo plant is herbaceous, and from one to three feet high, with delicate bluish-green leaves. A Woman's Journey Round the World |Ida Pfeiffer 

Next rinse the veil through two cold waters, tinging the last with indigo. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

But the foot-gin appeared on the scene, and indigo went down before cotton. Lippincott's Magazine of Popular Literature and Science |Various 

Some fifty years later, a planter's daughter tried to raise indigo. The Story of the Thirteen Colonies |H. A. (Hlne Adeline) Guerber 

After several failures, she succeeded in doing so, and indigo was raised in Carolina until the time came when cotton paid better. The Story of the Thirteen Colonies |H. A. (Hlne Adeline) Guerber