Be careful that none of the company are left to mope alone from being unacquainted with other guests. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Girls are just like cats; they all like to mope around the register or the steam radiator in cold weather. The Girls of Central High on the Stage |Gertrude W. Morrison 

He would put the gun back into its case again and mope in his library for days afterward. Gallegher and Other Stories |Richard Harding Davis 

"I'm tired o' doin' nowt but mope i' th' house," Liz fretted. That Lass O' Lowrie's |Frances Hodgson Burnett 

She could go out and move about and bestir herself, whereas in Manchester Square she could only sit and mope at home. The Prime Minister |Anthony Trollope