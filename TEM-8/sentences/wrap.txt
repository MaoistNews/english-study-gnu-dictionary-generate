After the July sessions wrapped, coaches continued meeting with smaller groups of students from the cheer team at private gyms for extra compensation. School Sports Became ‘Clubs’ Amid the Pandemic – Now Two Coaches Are Out |Ashly McGlone |September 17, 2020 |Voice of San Diego 

None of this data is original — it’s all public — but putting these charts in one place can help us wrap our minds around the many different ways that questions about responsibility for climate change can be phrased. What’s causing climate change, in 10 charts |David Roberts |September 11, 2020 |Vox 

The extent of the program had remained under wraps until early September, when new information revealed that participation had not been as limited as the government initially suggested. China has quietly vaccinated more than 100,000 people for Covid-19 before completing safety trials |Lili Pike |September 11, 2020 |Vox 

A 4-0 thrashing of Leicester City at King Power stadium on Boxing Day all but wrapped it up for the Reds, who dropped only 2 points in their first 27 games in 2019-20. Will Liverpool Run Away With The Premier League Again, Or Can Manchester City Take The Title Back? |Terrence Doyle |September 10, 2020 |FiveThirtyEight 

Walker is nimble, perceptive and skilled enough to punish defenses in too many ways for them to wrap their arms around. Give Boston’s Kemba Walker A Double Pick And Watch Him Work |Michael Pina |August 31, 2020 |FiveThirtyEight 

Cover with plastic wrap and allow the dates to soften, about 15 minutes. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

It can be hard to wrap your head around the problems facing the continent because they might seem ancient to us. Silicon Valley Sets Its Sights on Africa |Christian Borys |December 22, 2014 |DAILY BEAST 

He can't wrap his head around the idea that people may want to talk about painful events. The Right's Rape Trolls vs. Lena Dunham |Emily Shire |December 10, 2014 |DAILY BEAST 

Creepy thing to wrap up in festive paper and a bow and give to a newborn baby, yeah? Keep Christmas Commercialized! |P. J. O’Rourke |December 6, 2014 |DAILY BEAST 

Available at La Boîte SHOLDIT Clutch Wrap Purse, $70 We can all agree the dorky passport holders and money bags have got to go. The Daily Beast’s 2014 Holiday Gift Guide: For the Anthony Bourdain in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

First of all, wrap a portion of damp newspaper round the roots, and then tie up with dry paper. How to Know the Ferns |S. Leonard Bastin 

When that time comes they wrap it in their blankets, and fasten buyos and other things about the waist for the journey. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

She put on a warm wrap and seated herself at the dressing-table, studying her face critically. Dope |Sax Rohmer 

Mildred Wallace, scrutinizing the program, merely drew her wrap closer about her shoulders and sat more erect. The Fifth String |John Philip Sousa 

Rashid retired to wrap up the purchase, and with it a second and smaller package was slipped into the customer's hand. Dope |Sax Rohmer