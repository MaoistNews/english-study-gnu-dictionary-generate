The Fairness Doctrine has often been conflated with the “equal time” rule for political candidates. The Fairness Doctrine won’t solve our problems — but it can foster needed debate |Victor Pickard |February 4, 2021 |Washington Post 

Video game-related media has conflated the people buying GameStop stock with gamers. The GameStop stock situation isn’t about populism. It’s about whether the market is ‘real.’ |Mikhail Klimentov |February 1, 2021 |Washington Post 

In a technology-driven world, people tend to conflate adaptability with technological change, especially when it comes to navigating adverse climates and places. Scientist findings in Tanzania show how ancient humans used tools 2 million years ago |Julio Mercader Florin |January 17, 2021 |Quartz 

Gersberg, the microbiologist at SDSU, warned not to conflate sewage with toxic industrial chemicals. New Snapshot of What’s in the Tijuana River Is as Gross as You’d Expect |MacKenzie Elmer |November 2, 2020 |Voice of San Diego 

According to Shoebridge, Australia’s future approach to trade with China will seek to conflate economic and political goals into a single policy, rather than treating them as separate tracks. China is inching toward another trade war |eamonbarrett |September 27, 2020 |Fortune 

With Entourage and Vincent Chase, do you feel like audiences and producers tend to conflate you with the character? Adrian Grenier Talks the Economy, the ‘Entourage’ Movie, and the HBO Series’ Alleged ‘Misogyny’ |Marlow Stern |October 28, 2014 |DAILY BEAST 

I still conflate safer sex with respect for the gay community and the lessons we learned from the AIDS crisis. Will This Pill Kill ‘Safe Sex’? |Russell Saunders |May 16, 2014 |DAILY BEAST 

Now it looks as if the Israeli strategy of trying to get us to conflate the issues has gained traction. Obama Admin Confirms: We May Free Israeli Spy to Save Peace Talks |Josh Rogin |April 1, 2014 |DAILY BEAST 

Christie will need to assure the party about his own integrity, and his tendency to conflate government with his own self. Election Night 2013: The Center Speaks |Lloyd Green |November 6, 2013 |DAILY BEAST 

And I think [Gould] was guilty of using a poetic language to conflate those three kinds of episodic changes. Rediscovering Richard Dawkins: An Interview |J.P. O’Malley |September 23, 2013 |DAILY BEAST