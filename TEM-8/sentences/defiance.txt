Cuomo warned the state would withhold funding from any school that remained open in defiance of the shutdown. New York’s block-by-block lockdowns are curbing covid-19. But residents aren’t pleased. |Ben Guarino |November 8, 2020 |Washington Post 

If there were ballots that were left behind by the Postal Service in defiance of the court order, they’re going to be found. For the Postal Service, a Frantic Election Day Turns to Finger-Pointing the Day After |by Maryam Jameel and Ryan McCarthy |November 5, 2020 |ProPublica 

Despite prodigious yo-yoing due to scrapes with securities regulators, missed deadlines, and defiance of pandemic-driven health laws, Tesla market capitalization is about $400 billion. Does Elon Musk even need a PR department? |Adam Lashinsky |October 8, 2020 |Fortune 

Those who operate in defiance of the state rules could face fines, possibly the loss of their license. Sacramento Report: State Audit Knocks San Diego Agency’s Use of Vehicle Fees |Voice of San Diego |July 17, 2020 |Voice of San Diego 

In 2014, California banned school districts from using willful defiance as a reason to suspend young students in kindergarten through third grade, and from expelling any K-12 student for willful defiance alone. School Leaders Can’t Suspend the Discipline Discussion Any Longer |Will Huntsberry |July 16, 2020 |Voice of San Diego 

Or you may not have many—or any—friends, recasting your social exclusion as brave defiance of social norms. The Refuseniks Hiding From ‘Happy New Year’ |Lizzie Crocker |December 31, 2014 |DAILY BEAST 

Another 10 slaves threw themselves overboard in a display of defiance at the inhumanity. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

In defiance, I held my ticket above my head, which triggered the spitting and chants of “How Dare You!” Inside the Metropolitan Opera’s Insane Year |Shawn E. Milnes |November 23, 2014 |DAILY BEAST 

She had just shot her arrow at an electric force field, an apparently society-changing act of defiance. 'Mockingjay—Part 1’ Is the Most Violent ‘Hunger Games’ Yet |Kevin Fallon |November 20, 2014 |DAILY BEAST 

The economy in Aleppo barely exists, and the attitude of those who remain hovers somewhere between defiance and defeat. Local Truces Are Syria’s Sad Little Pieces of Peace |Joshua Hersh |November 18, 2014 |DAILY BEAST 

Two of the artillerymen who had not been injured came to his assistance, and again the gun was thundering forth its defiance. The Courier of the Ozarks |Byron A. Dunn 

Others now rushed to the rescue, the artillery men came back, and once more the guns were thundering their defiance. The Courier of the Ozarks |Byron A. Dunn 

It would have been a sort of review—in the face of the city of Dublin, in open defiance of all order and government. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

From its strongly fortified position Havana set the buccaneers at defiance, and sometimes saved the whole island from ruin. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Madame had risen hastily, fear and defiance blending in her face, and she had at once commanded mademoiselle's withdrawal. St. Martin's Summer |Rafael Sabatini