Accusations flew that she was ripping off investors, and Carlson revealed she was a convicted swindler who’d changed her gender. Politics Report: BIA’s Big Decision |Scott Lewis and Andrew Keatts |March 6, 2021 |Voice of San Diego 

The Cleveland Browns waited 18 years just to have fate swindle them. The Cleveland Browns were an NFL feel-good story. Then the coronavirus got jealous. |Jerry Brewer |January 6, 2021 |Washington Post 

The most ridiculous character in Pay Any Price may be Dennis Montgomery, who is described as an inveterate gambler and swindler. Speed Read: James Risen Indicts The War On Terror’s Costly Follies |William O’Connor |October 14, 2014 |DAILY BEAST 

This kind of combat reached its apotheosis when the guest was an insurance swindler called Emile Savundra. ‘A Fiery Tribune’ |Clive Irving |September 1, 2013 |DAILY BEAST 

From the start, we see him as he is: a despot and a swindler, a Dallas blue-blood with FBI ties, fleeing a violent past. This Week’s Hot Reads, July 15, 2013 |Sarah Stodola, Jen Vafidis, Damaris Colhoun |July 15, 2013 |DAILY BEAST 

Top Producerby Norb Vonnegut A financial thriller about a Bernie Madoff-like swindler. This Week's Hot Reads |The Daily Beast |September 21, 2009 |DAILY BEAST 

I will turn and face you,” the 71-year swindler in a Savile Row-tailored charcoal-gray suit said flatly: “I am sorry. The Madoff Message |Allan Dodds Frank |June 29, 2009 |DAILY BEAST 

Does the old swindler think to persuade me that C. F. Garman is in want of cash? Skipper Worse |Alexander Lange Kielland 

Figgered a swindler wouldn't never suspect nobody of swindlin' him with one of his own tricks. Scattergood Baines |Clarence Budington Kelland 

The man who brought these calamities on his country was not a mere visionary or a mere swindler. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Algernon Deuceace, you don't want a father to tell you that you are a swindler and a spendthrift! Memoirs of Mr. Charles J. Yellowplush |William Makepeace Thackeray 

Evidently the swindler had dropped from the roof to the upper landing of the fire escape. The Rover Boys on the Farm |Arthur M. Winfield (AKA Edward Stratemeyer)