They had feared ISIS fighters and their Sunni allies would unleash a massacre of the predominantly Shiite townsfolk. This Is How You Fight ISIS |Jamie Dettmer |June 19, 2014 |DAILY BEAST 

On the outskirts of Slovyansk, a hundred kilometers away, frustration is building among some of the townsfolk. Inside East Ukraine’s Make-Believe Republics |Jamie Dettmer |May 15, 2014 |DAILY BEAST 

Vanderkool makes a string of arrests that stun border patrol compatriots and townsfolk. Drugs and Terror on the Other Border |John Douglas Marshall |June 24, 2009 |DAILY BEAST 

Then did the townsfolk flee, though many prayed for mercy, and mercy did all receive who gave themselves up. The Sagas of Olaf Tryggvason and of Harald The Tyrant (Harald Haardraade) |Snorri Sturluson 

The mountaineers, indeed, suffered less than the townsfolk as being more accustomed than they to conditions of trek and battle. The Cradle of Mankind |W.A. Wigram 

The people of St. Sampson, except a few rich families among the townsfolk, are also a population of quarriers and carpenters. Toilers of the Sea |Victor Hugo 

The chatter of the townsfolk crept into my ears between the hoof-beats, and made me sick and dizzy. The Yeoman Adventurer |George W. Gough 

An inn-yard, with soldiery around and townsfolk gaping through doors and windows, is no place for a council of war. The Yeoman Adventurer |George W. Gough