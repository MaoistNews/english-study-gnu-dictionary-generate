Rothschild is all-too-aware of that when it comes to space, being frugal about resources is paramount. NASA’s Trippy Thought: Build Space Homes Out of Mushrooms |Nick Hilden |November 16, 2021 |The Daily Beast 

The former software engineer had retired six years before at the tender age of 30, thanks to the savings he’d accrued through the extremely frugal lifestyle he and his wife at the time had adhered to, as well as some smart investing. Embrace Your Inner Dirtbag |jversteegh |July 13, 2021 |Outside Online 

She and her husband, both of whom are extremely frugal, make our home their annual, and only, vacation. Ask Amy: In-laws’ vaccination status is dealbreaker |Amy Dickinson |May 30, 2021 |Washington Post 

The Patriots of old — teams that had the benefit of the best quarterback in NFL history on the roster — were typically frugal spenders in free agency. The Patriots Have Opened Their Free Agent Checkbooks. But Will It Help Them Win? |Josh Hermsmeyer |March 22, 2021 |FiveThirtyEight 

Be as frugal as you can, call on favors, fundraise, apply for funding. How to Suck Less at … Starting a Theater Company |Eugene Robinson |March 22, 2021 |Ozy 

Even a frugal firm like Wal-Mart is enlarging its Silicon Valley presence. Battle of the Upstarts: Houston vs. San Francisco Bay |Joel Kotkin |October 5, 2014 |DAILY BEAST 

Much of the praise Edwards earned for his Godzilla adaptation stemmed from his frugal use of the monster. The Cult of Boba Fett: The ‘Star Wars’ Bounty Hunter’s Spin-Off |Rich Goldstein |June 5, 2014 |DAILY BEAST 

It could be that is exactly what frugal Pope Francis has in mind for the Bishop of Bling. Germany’s Bishop of Bling |Barbie Latza Nadeau |October 16, 2013 |DAILY BEAST 

Grey played Christine, a high-class escort in New York City whose clients have become more frugal due to the financial crisis. Hollywood’s Favorite Ex–Porn Star: A Chat With Sasha Grey |Marlow Stern |February 15, 2013 |DAILY BEAST 

“My grandmother didn't die poor, but the majority of what she did was very frugal,” he says to me. Marcus Samuelsson Talks New Memoir: ‘Yes, Chef’ |Jacob Bernstein |July 2, 2012 |DAILY BEAST 

He was of frugal and temperate habits, a wiry man at the height of his physical powers, with lean flanks and a deep chest. Uncanny Tales |Various 

"He'll do until the doctor comes to-morrow," said Seth, as he presently found Barrington at the frugal meal. The Light That Lures |Percy Brebner 

They are, however, happy on account of their simple manners and frugal way of life. The Geography of Strabo, Volume III (of 3) |Strabo 

Truphemy not knowing him, he was pointed out partaking of a frugal breakfast with the family. Fox's Book of Martyrs |John Foxe 

Both were frugal men, and they now decided to invest all their funds in the scheme, which promised to make or break them. Adrift on the Pacific |Edward S. Ellis