He went up to a park, in Inwood, northern Manhattan, and the garbage pails are all full and there’s papers flying around and there are these park employees just sitting there. How Does New York City Keep Reinventing Itself? (Bonus) |Kurt Andersen |March 21, 2021 |Freakonomics 

My nostrils have smelt the horrors of the (cloth) diaper pail. Why Men Shouldn’t Wait To Have Kids |Conor P. Williams |March 8, 2014 |DAILY BEAST 

Homer uses metaphor—‘it was so crowded, it was like bees swarming over a pail of milk.’ Denis O’Hare Talks About One-Man Show “An Iliad” |Janice Kaplan |March 25, 2012 |DAILY BEAST 

Let us try to set aside—preferably forever—the image of the ever-immaculate Romney crouched bare-assed over a pail. Mitt Romney's 'Missionary in France' Tale Won't Make Us Forget He's Rich |Michelle Cottle |December 15, 2011 |DAILY BEAST 

But it was hard to be the party of Wall Street and the party of the lunch-pail at the same time. Why Obama Can Spend, Spend, Spend |Peter Beinart |December 24, 2008 |DAILY BEAST 

At the farm-gate they met Dorothy, fresh and blooming as a rose, with a pail in each hand foaming to the brim with milk. The World Before Them |Susanna Moodie 

She kicked at Jehosophat and over went the pail of milk which his father had almost full. Seven O'Clock Stories |Robert Gordon Anderson 

Sometimes a milk-pail is represented near a lamb, or hanging on a crook by its side, or even resting on its back. The Catacombs of Rome |William Henry Withrow 

He listened carefully before, standing outside in the cold, he poured over his head and shoulders a pail of cold water. The Amazing Interlude |Mary Roberts Rinehart 

The root thus broken up is rubbed about in a great pail, with water slowly added. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson