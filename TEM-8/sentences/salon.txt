Young’s salon is back open but limited to half its usual capacity. Courts may reconsider temporary coronavirus restrictions as pandemic drags on |Anne Gearan, Karin Brulliard |September 16, 2020 |Washington Post 

The numbers grew alarming and Newsom abruptly shut restaurants and their bars again, gyms, salons and churches back down. Team Reopen: 2, Schools: 0 |Scott Lewis |August 31, 2020 |Voice of San Diego 

There are crowdfunding campaigns to help locals, but hair salons are also chipping in — turns out human hair is a great material for soaking up oil. Sunday Magazine: A World in Need |Daniel Malloy |August 16, 2020 |Ozy 

“In new guidance issued Monday, after conflicting messages from government entities last week, the state clarified that salons could operate outdoors,” the Los Angeles Times reports. Morning Report: Smart Streetlights Are Now Accessible Only to Police |Voice of San Diego |July 21, 2020 |Voice of San Diego 

Due to yesterday’s amended order from your office, all local barbershops and hair salons face imminent closure and many of these establishments will not survive a second mandatory shutdown. Sacramento Report: State Audit Knocks San Diego Agency’s Use of Vehicle Fees |Voice of San Diego |July 17, 2020 |Voice of San Diego 

It upsets me because I used to really, and still do sometimes, love the articles Salon writes. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

The other Twitter topic you are well known for is the topic of Salon. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

Mary Elizabeth Williams of Salon labels the show a “crass stunt” on a “bottom-feeding vortex of sadness network.” Your Husband Is Definitely Gay: TLC’s Painful Portrait of Mormonism |Samantha Allen |January 1, 2015 |DAILY BEAST 

She and her sister went into business together in 1997, opening Curve Salon after a career in media. Goodbye To A Natural Hair Guru: Miss Jessie's Cofounder Titi Branch Dead At 45 |Danielle Belton |December 16, 2014 |DAILY BEAST 

The idea to invest in their own hair company came from Miko after seeing how clients at their salon responded to her natural hair. Goodbye To A Natural Hair Guru: Miss Jessie's Cofounder Titi Branch Dead At 45 |Danielle Belton |December 16, 2014 |DAILY BEAST 

The sun was shining when they arrived at Salon, the gayest, the most coquettish, the most laughing little town in Provence. The Joyous Adventures of Aristide Pujol |William J. Locke 

The "Pensierosa" and a little girl were at the Paris Salon in 1894, and were much admired. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

I introduced him into the salon, and Mademoiselle, after conversing a little while with Madame, consented to receive him. Confidence |Henry James 

This was shown at the Paris Salon, 1889, and missed the gold medal by two votes. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

On entering the salon, they found several groups already assembled. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various