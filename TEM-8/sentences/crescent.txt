If partly to mostly cloudy skies allow a glimpse, check out the sliver of new crescent moon on the western horizon with Mars and Venus in conjunction. D.C.-area forecast: Scattered late-day storms, then a more pleasant start to the weekend |A. Camden Walker |July 9, 2021 |Washington Post 

The sliver of a new crescent moon on the western horizon with Mars and Venus in conjunction is likely to be obscured early Sunday evening but has a better chance to be seen Monday evening. Updated forecast: Flash flood watch extended into District due to downpours from Elsa |David Streit, Jason Samenow |July 8, 2021 |Washington Post 

For this week’s Classic, let’s take a closer look at a waxing crescent moon. Can You Crack The Case Of The Crescent Moon? |Zach Wissner-Gross (riddlercolumn@gmail.com) |April 16, 2021 |FiveThirtyEight 

The industry has its critics, such as Prince William residents concerned that data centers will spoil the beauty of its western rural crescent. Silver lining: Pandemic triggers booms in D.C. region’s biotech, data center industries |Robert McCartney |April 12, 2021 |Washington Post 

The crescent-shaped pastries called shekerbura, filled with crushed hazelnuts, sugar and cardamom, look more like something to admire as art than ravage with fingers and teeth. This new Azerbaijani bakery offers stories as good as its pastries |Tom Sietsema |February 26, 2021 |Washington Post 

Leaving the OR that night, I looked up the clear sky, at the flocks of white seagulls and a sliver of crescent moon. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

Tipitina's in the warm blue fog, squatting beneath a crescent moon so sharp and clean you could shave a wild hog with it. The Stacks: The Neville Brothers Stake Their Claim as Bards of the Bayou |John Ed Bradley |April 27, 2014 |DAILY BEAST 

More Red Cross and Red Crescent volunteers have been killed in this conflict than in any other since records have been kept. The Penny-Ante Syria Talks |Christopher Dickey |January 27, 2014 |DAILY BEAST 

The head of Iran's Red Crescent rescue corps, Mahmoud Mozafar, said communications to the stricken areas have been cut. At Least 34 Dead in Pakistan After 7.8 Magnitude Earthquake |Justin Green |April 16, 2013 |DAILY BEAST 

The Syrian Arab Red Crescent recently estimated that 2.5 million people have been displaced within the country. Under Siege, Damascus Gets Desperate for Food and Fuel |Mikel Ayestaran |December 14, 2012 |DAILY BEAST 

The valve-seat H has formed on its lower surface two crescent shaped long and narrow slits. The Recent Revolution in Organ Building |George Laing Miller 

From her windows could be seen the crescent of the river, the masts of ships and the big chimneys of the Mississippi steamers. The Awakening and Selected Short Stories |Kate Chopin 

It floated out upon the night, over the housetops, the crescent of the river, losing itself in the silence of the upper air. The Awakening and Selected Short Stories |Kate Chopin 

On either horn of the crescent by which the pebbles are imported into the pocket we find the largest fragments. Outlines of the Earth's History |Nathaniel Southgate Shaler 

They are ranged around a crescent-shaped table formed of cushions, and wear festive crowns upon their heads. The Catacombs of Rome |William Henry Withrow