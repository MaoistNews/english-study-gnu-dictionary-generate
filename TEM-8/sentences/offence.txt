Breaching the quarantine order is a criminal offence and offenders are subject to a maximum fine of HK $25,000 and imprisonment for six months. I Saw Firsthand What It Takes to Keep COVID Out of Hong Kong. It Felt Like a Different Planet. |by Caroline Chen |January 6, 2022 |ProPublica 

The nineteenth charge was, “For transporting us beyond seas to be tried for pretended offences.” America's Last King Wasn't the Crazy Tyrant You've Been Led to Believe |Andrew Roberts |November 9, 2021 |Time 

They also included an agreement not to subject him to a highly restrictive form of solitary confinement, provided he did not commit another offence. What to Know About Julian Assange’s Extradition Appeal |Eloise Barry / London |October 29, 2021 |Time 

On Monday, Downing Street said it was first made aware of “a potential offence relating to child abuse imagery” on February 12. British Prime Minister’s Child Porn Adviser Arrested Over Child Porn |Tim Teeman |March 4, 2014 |DAILY BEAST 

“We never meant to be offensive, but we apologize if any offence was caused,” they told the Sun. British Co-Eds Dress as 9/11 in a Costume Contest—and Win |Nico Hines |November 6, 2013 |DAILY BEAST 

Is it really an offence against democracy for a government to enforce its own commitments upon its own MPs? Stephen Harper is Respecting Voter Wishes by Not Reopening the Abortion Debate |David Frum |April 6, 2013 |DAILY BEAST 

It is the first offence of which Mr Taylor has ever been accused. Harry's Day in Court Over Stolen Mobile Moves Closer |Tom Sykes |February 29, 2012 |DAILY BEAST 

This, of course, amounted to the criminal offence of aiding and abetting sex with minors. Prosecute the Pope |Geoffrey Robertson |April 1, 2010 |DAILY BEAST 

Turn away from sin and order thy hands aright, and cleanse thy heart from all offence. The Bible, Douay-Rheims Version |Various 

Without any known cause of offence, a tacit acknowledgement of mutual dislike was shewn by Louis and de Patinos. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

A little quarrelling or fighting, or playing at cards, was apparently no offence. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

No more admirable illustration can be found of the truth that the essence of defence lies in a vigorous local offence. Napoleon's Marshals |R. P. Dunn-Pattison 

Still, I mean no offence when I put tea in the same category with Tobacco. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.