Lawson felt she could do this more effectively if she had the title “rabbi,” and she wanted to raise awareness of racial and ethnic diversity in the Jewish community. Making Reconstructing Judaism work for all |Kathi Wolfe |February 18, 2021 |Washington Blade 

Lown was born Boruch Latz on June 7, 1921, in Utena, Lithuania, where a grandfather was a rabbi. Bernard Lown, physician who rallied doctors against nuclear war, dies at 99 |Emily Langer |February 18, 2021 |Washington Post 

Lasry explained that his wife’s uncle, a rabbi at a Milwaukee senior-living center, had called her about having extra, unused doses of the vaccine. Milwaukee Bucks executive announces run for U.S. Senate in Wisconsin |Matt Bonesteel |February 17, 2021 |Washington Post 

“There are times we will look at someone eating alone in a restaurant and we feel like we can’t go on,” says Susan Silverman, a rabbi in Israel and one of three older sisters. Sarah Silverman just wants to make things right |Geoff Edgers |February 4, 2021 |Washington Post 

No rabbi has ever been an official chaplain, a paid job that comes with an office and staff. Praying for the politicians: A new book recounts the history of rabbis in the Capitol |John Kelly |December 9, 2020 |Washington Post 

One of my most important mentors was a brilliant and eccentric rabbi from Bethesda, Maryland. Fixing a Dysfunctional Family: Congress |Gene Robinson |November 9, 2014 |DAILY BEAST 

Freundel is also rabbi of the prominent Kesher Israel synagogue in Washington. Accusations Pile Up on Top D.C. Rabbi Barry Freundel |Steven I. Weiss |October 15, 2014 |DAILY BEAST 

His office let Rabbi Yehuda Kolko get away without jail time or registering as a sex offender. The Orthodox Sex Abuse Crackdown That Wasn’t |Emily Shire |October 7, 2014 |DAILY BEAST 

Well, if it's Carrie and her spy-rabbi Saul, courtesy of the inimitable Mandy Patinkin, the answer is: Yes, we still are. ‘Homeland’ Season 4: A Stripped-Down and Surprisingly Badass Return to Form |Marlow Stern |September 30, 2014 |DAILY BEAST 

He remembered one day when Rabbi Bodenheimer came outside during recess. This 'Holy Guy'—and Grandfather of 100—Is Accused of Sexually Abusing a Student |Batya Ungar-Sargon |September 9, 2014 |DAILY BEAST 

And Peter calling to remembrance saith unto him, "Rabbi, behold the fig tree which thou cursedst is withered away." His Last Week |William E. Barton 

Rabbi Gamaliel ordered his servant Tobi to bring something good from the market, and he brought a tongue. Hebraic Literature; Translations from the Talmud, Midrashim and Kabbala |Various 

The Rabbi Effendi had approached the river from a different direction, and that some years before. The Cradle of Mankind |W.A. Wigram 

Rabbi Mr. Wigram had needed some trifling repair to his boots, and had accordingly sent them overnight to a cobbler. The Cradle of Mankind |W.A. Wigram 

Again was Rabbi Jochanan filled with wonder, but he said naught, and they proceeded on their journey. Hebraic Literature; Translations from the Talmud, Midrashim and Kabbala |Various