The brass finish lends a warm vibe to your fireside, while keeping your wood neatly in place. Fall Decor You Need in Your Life Right Now |Korin Miller |September 30, 2021 |The Daily Beast 

King urged the president to talk about integration in moral terms, rather than purely as a political issue, and suggested that he revive FDR’s “fireside chats” to explain civil rights to the American people. How Robert F. Kennedy Shaped His Brother's Response to Civil Rights |Patricia Sullivan |August 11, 2021 |Time 

Officials also asked nonprofits, faith organizations and other community partners to spread the word and are exploring hosting “fireside chats” to inform and answer questions. With evictions set to begin next month, hundreds of millions in Washington-area rental aid remains unspent |Anu Narayanswamy, Jonathan O'Connell, Marissa Lang, Kyle Swenson |June 4, 2021 |Washington Post 

Walensky said during a “fireside chat” webinar Tuesday she expects the current infection numbers to continue their recent decline. CDC says coronavirus could be under control this summer in U.S. if people get vaccinated and are careful |Joel Achenbach, Lena H. Sun |May 5, 2021 |Washington Post 

The perfect fireside dessert is an adult take on a childhood classic. 6 Creative Ways to Enjoy Whiskey While Camping |Outside Editors |April 1, 2021 |Outside Online 

His Oval Office radio addresses were famously labeled “Fireside Chats” and he called listening citizens “my friends.” FDR: King of All Media |John Avlon |September 2, 2014 |DAILY BEAST 

His fireside chats brought his reassuring “radiogenic” voice to millions of Americans still suffering from the Great Depression. When Mars Attacked 75 Years Ago—And Everyone Believed It |Marc Wortman |October 29, 2013 |DAILY BEAST 

Are there robes and fireside chats with goblets of sherry, that sort of thing?No, ha, nothing quite so effete. Ali Smith: How I Write |Noah Charney |January 23, 2013 |DAILY BEAST 

In his mid-40s the author of so many happy fireside scenes grew tired of his wife. Charles Dickens’s Unhappy Children |Michael Gorra |December 2, 2012 |DAILY BEAST 

Knowing he needed to sell the New Deal to the American people, Franklin Roosevelt created the dramatic fireside chats. Republicans Embrace Theatrics—It’s Time for Barack Obama to Do the Same |Sally Kohn |July 20, 2012 |DAILY BEAST 

The quiet comfort and heartfelt warmth of an English fireside must be felt to be appreciated. Glances at Europe |Horace Greeley 

Little Colonel comes cautiously into the room, hugging the wall till he is back at the fireside. The Soldier of the Valley |Nelson Lloyd 

But Gwynne concealed the promptings of vanity and took one of the chairs at the fireside, asking permission to light his pipe. Ancestors |Gertrude Atherton 

Well said, my man; but remember 'tis easier talking by one's own fireside than doing when the trial comes. The Daisy Chain |Charlotte Yonge 

In its inception it was a mere attempt to write pleasing, popular verse of a better kind in the dialect of the fireside. Frdric Mistral |Charles Alfred Downer