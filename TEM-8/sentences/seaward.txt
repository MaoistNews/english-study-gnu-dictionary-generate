On passing the cape two reefs were seen to seaward, which had previously escaped our notice. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

They stood out of the harbour and headed down the coast of the island, which extended seaward thus for some four miles. The Rival Campers Afloat |Ruel Perley Smith 

It was three stories high, the top story opened to seaward, with its lanterns and tin reflectors. The Belted Seas |Arthur Colton 

At four o'clock I was out upon the quay, straining my eyes seaward for any sign of smoke, but could see nothing. The Czar's Spy |William Le Queux 

It was different with the poor fellows on the Lighthouse, eighteen miles to seaward of us, to the south-west. Wandering Heath |Sir Arthur Thomas Quiller-Couch