There are several other reasons, including that oftentimes when there is an option of sending one child to school, families will send the boy. Malala Yousafzai tells the business community: Education is the best way to guard against future crises |Michal Lev-Ram, writer |September 15, 2020 |Fortune 

When an imam refused to speak with a girl, she sent a boy from her organization in her place. The Teenager Breaking Up Child Marriages, Door to Door |Pallabi Munsi |September 2, 2020 |Ozy 

She didn’t mind that Charlie didn’t play sports as the other boys did. ‘A Star is Bored’ a delicious work of fiction |Kathi Wolfe |August 13, 2020 |Washington Blade 

Once, while volunteering in a first-grade classroom, I even accidentally kissed the wrong boy because the back of his head looked like my son’s. Online Learning Is Here to Stay |Sara-Ellen Amster |August 7, 2020 |Voice of San Diego 

His condition improved more lastingly only after his physician, Ogden Bruton, discovered that the boy had almost no gamma globulins in his blood. Our Genes May Explain Severity of COVID-19 and Other Infections |Monique Brouillette |July 27, 2020 |Quanta Magazine 

In the 90s, it kept gay men out of leadership roles in the Boy Scouts of America. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

This is Bey and Nicki at their most lyrically masochistic, and boy, is it a treat. The 14 Best Songs of 2014: Bobby Shmurda, Future Islands, Drake, and More |Marlow Stern |December 31, 2014 |DAILY BEAST 

Interviews in Serial (including ones from Adnan) do acknowledge that Jay was known as a resident bad boy at Woodlawn High School. The Deal With Serial’s Jay? He’s Pissed Off, Mucks Up Our Timeline |Emily Shire |December 31, 2014 |DAILY BEAST 

The former Virginia governor was once the golden boy of the GOP. 2014 Was a Delectably Good Year for Sleaze |Patricia Murphy |December 30, 2014 |DAILY BEAST 

A sepia photo shows him as a young boy, head in his hands, with a large book open at a bar table. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

A little boy of four was moved to passionate grief at the sight of a dead dog taken from a pond. Children's Ways |James Sully 

You see, I'd always thought of him as the boy whom Great-aunt Lucia described having seen. The Boarded-Up House |Augusta Huiell Seaman 

Poor Squinty ran and tried to hide under the straw, for he knew the boy was talking about him. Squinty the Comical Pig |Richard Barnum 

A look of passion came into the face of the watching boy, and again he fingered his revolver. The Courier of the Ozarks |Byron A. Dunn 

Several times after this the boy and his sisters came to look down into the pig pen. Squinty the Comical Pig |Richard Barnum