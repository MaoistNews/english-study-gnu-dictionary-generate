Some are funny and light, and others attempt to speak for a world that’s become mired in grief. How Hallmark is handling pandemic Valentine’s Day |Luke Winkie |February 12, 2021 |Vox 

As a culture, we don’t seem to recognize the loss of intimacy or closeness in a relationship as legitimate grief. Politics and conspiracy theories are fracturing relationships. Here’s how to grieve those broken bonds. |Jeff Schrum |February 11, 2021 |Washington Post 

Pitched from that home, he becomes home unto himself—home at the speed of light but slower than the speed of grief. In Science Fiction, We Are Never Home - Issue 95: Escape |Steve Erickson |February 10, 2021 |Nautilus 

As Katz makes her way through the world, carrying grief and shame and secrets with her all the while, readers will hope for an easing of her burdens. For a mother forced to give up her child, decades of grief, shame and secrets |Ellen McCarthy |February 5, 2021 |Washington Post 

Flipping between the voices of the family members, Hobson depicts the lingering effects of trauma, and the way grief informs memory and love. Here Are the 14 New Books You Should Read in February |Annabel Gutterman |February 2, 2021 |Time 

Though tissues are present and tears are not uncommon, the Dinner Parties are distinctly not grief counseling or group therapy. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

It warps them and yet makes them, and horrifies them both as it does so—just as grief does. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

Energy is sucked from them, the world around them becomes impossible—the Babadook of grief and loss exerts its force everywhere. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

The grief in this house is extreme of course; this is a horror movie, after all. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

Amelia says some truly terrible things to Sam, supposedly inhabited by the Babadook but really consumed in grief. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

A little boy of four was moved to passionate grief at the sight of a dead dog taken from a pond. Children's Ways |James Sully 

They wanted Papa and Mamma, gone to Bombay beyond the seas, and their grief while it lasted was without remedy. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Then I hesitated no longer, but turned away and left her alone with her grief; it was not for me to comfort her. Raw Gold |Bertrand W. Sinclair 

Then waves of grief broke over her, and she sobbed convulsively; but still she shed no tears. Ramona |Helen Hunt Jackson 

In a statuesque attitude, she sat, like Marius on the ruins of Carthage, or Patience on a monument smiling at grief. The Pit Town Coronet, Volume I (of 3) |Charles James Wills