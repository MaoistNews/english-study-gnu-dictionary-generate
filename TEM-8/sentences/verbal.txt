Most members of the San Diego City Council boycotted Tuesday’s closed session hearing in protest of City Attorney Mara Elliott’s decision to provide verbal rather than written legal reports after someone leaked information last week to NBC 7. Morning Report: The Seditious Language Law’s Origin Story |Voice of San Diego |August 5, 2020 |Voice of San Diego 

The review board report ultimately concluded that Browder’s actions were reasonable only if he’d shouted verbal commands to Nehad prior to shooting him. Police Review Board Was Denied Docs, Interviews in 2015 Shooting Review |Sara Libby |July 22, 2020 |Voice of San Diego 

Hernandez told investigators she reported being fearful of “verbal attack.” Accusations Flew, Then National School District Official Got Paid to Resign |Ashly McGlone |July 20, 2020 |Voice of San Diego 

When staff finally released him –after everyone else in his building – he got into a verbal dispute with an officer who was working on the floor that night. Donovan Inmates Plead for Protection, Allege Retaliation Following Lawsuit |Maya Srikrishnan |July 2, 2020 |Voice of San Diego 

Do you know how when we grew up, students would call themselves, proudly, verbal kids or math kids, so you could get an 800 on the verbal section even though you didn’t like numbers and you never had to encounter them. America’s Math Curriculum Doesn’t Add Up (Ep. 391) |Steven D. Levitt |October 3, 2019 |Freakonomics 

These were cops who had worked the protests and suffered the accompanying verbal taunts and abuse. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

And then there was the unrelenting verbal abuse of cops simply because they are cops. Protesters Slimed This Good Samaritan Cop |Michael Daly |December 16, 2014 |DAILY BEAST 

In movies, that language, visual and verbal, has yet to be mastered. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

He reiterated the statements “I am not a politician” and “I am not a political advisor” so often that it seemed like a verbal tic. Obamacare Architect: I Wanted to Sound Smart |Ben Jacobs |December 9, 2014 |DAILY BEAST 

Because of the verbal abuse and death threats coming my way, these women seemed to identify with me. During Advent, Lots of Waiting, But Not Enough Hope |Gene Robinson |December 7, 2014 |DAILY BEAST 

These are few and verbal, but momentous, and were not made without consultation of many critical authorities and versions. Solomon and Solomonic Literature |Moncure Daniel Conway 

A verbal narrative has of course in itself nothing similar to the scenes and events of which it tells. Children's Ways |James Sully 

These brilliant results were arrived at after much clamour and argument and imposing procès verbal. The Joyous Adventures of Aristide Pujol |William J. Locke 

I resolved to investigate the matter, as it was only verbal, so that it might not become public. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

When thus acting his authority may be either verbal, or written, or may be shown by ratification. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles