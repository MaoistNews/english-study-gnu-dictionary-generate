Messages from across the Atlantic used to come by boat, she says, then came copper cables to relay telegraph dispatches in the 1840s. Materials of the last century shaped modern life, but at a price |Carolyn Wilke |January 28, 2022 |Science News 

In exchange for allowing it to provide universal telephone service, the US government required AT&T to stay out of other communication businesses, first by selling its telegraph subsidiary and later by steering clear of computing. Where computing might go next |Margaret O’Mara |October 27, 2021 |MIT Technology Review 

In May 1861, federal agents descended on Northern telegraph offices and seized transcribed messages in bulk. Modern capitalism is inseparable from surveillance |Josh Lauer, Kenneth Lipartito |September 22, 2021 |Washington Post 

The tech fight dates to the days of telegraph lines, undersea cables and “wireless” — that is, radio. Hackers thrive on the weak link in cybersecurity: People |Gershom Gorenberg |February 1, 2021 |Washington Post 

The Daily Telegraph's Lisa Armstrong called the show a "stupendously vacuous enterprise." Kanye West and Kim Kardashian’s Balmain Campaign: High Fashion Meets Low Culture |Amy Zimmerman |December 23, 2014 |DAILY BEAST 

Obama said, through laughter, according to an eyewitness report of the meeting in The Telegraph. When Your Royalty Met Ours: Kate Meets Bey Courtside |Tom Sykes |December 9, 2014 |DAILY BEAST 

Mr Obama said, through laughter, according to an eyewitness report of the meeting in The Telegraph. Kate Middleton and Prince William's $2m Dinner |Tom Sykes |December 8, 2014 |DAILY BEAST 

The Telegraph reports that he is fluent in Swahili and a keen zoologist. How A British Aristocrat Used Big Game Hunter’s Sperm To Get Pregnant Without His Permission |Tom Sykes |December 2, 2014 |DAILY BEAST 

“Unlike Turkey or Egypt, we have no art-historical tradition,” he told The Telegraph in 2002. The Mysterious Death of the Art World’s Favorite Sheikh |Lizzie Crocker |November 13, 2014 |DAILY BEAST 

In the telegraph office a young signaler was sending a thrilling message to Umballa, Lahore and the north. The Red Year |Louis Tracy 

As there were no telegraph lines, another way had to be provided by which messages might be quickly sent. Our Little Korean Cousin |H. Lee M. Pike 

Flocks of birds seemed to sing through the air, striking against the telegraph wires. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward 

Suppose he should receive an acceptance by letter or telegraph but deny it, and insist that no contract had been made. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

He proceeded to the tall telegraph pole and swarmed quickly up it. The Campfire Girls of Roselawn |Margaret Penrose