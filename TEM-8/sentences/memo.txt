“Their performance on this small project shows that some claims may have been inflated due to lack of experience with this type of work,” the memo states. New Engineering Report Finds Privately Built Border Wall Will Fail |by Jeremy Schwartz and Perla Trevizo |September 2, 2020 |ProPublica 

The shift will go into effect September 1, according to the memo. GM shifts Corvette engineering team to its electric and autonomous vehicle programs |Kirsten Korosec |August 28, 2020 |TechCrunch 

Under this new role, Piatek will work across the organization on future EV programs, according to the memo. GM shifts Corvette engineering team to its electric and autonomous vehicle programs |Kirsten Korosec |August 28, 2020 |TechCrunch 

The new WarnerMedia CEO sent a memo to employees declaring the going to direct to consumer through streaming is the future of the TV business. Shut out of Fire TV and Roku, Peacock is the latest example of the arrival power moves to streaming |Tim Peterson |July 15, 2020 |Digiday 

SDPD posts a memo to its website outlining its procedures for accessing the devices. Years Into Smart Streetlights Program, Council Will Write Surveillance Rules |Jesse Marx |July 9, 2020 |Voice of San Diego 

Ironically, unlike Dukakis and apparently Paul, Bill Clinton and Al Gore had each “read the memo” back in the day. GOP Won’t Forgive Rand for Cop Critique |Lloyd Green |December 23, 2014 |DAILY BEAST 

Memo to all Republican contenders: The GOP is now the home of white working- and middle-class voters. Bush, Christie, Romney: Who’ll Be the GOP Class Warrior? |Lloyd Green |December 15, 2014 |DAILY BEAST 

At just three pages long, the memo leaves more questions than it answers. Tribes to U.S. Government: Take Your Weed and Shove It |Abby Haglage |December 13, 2014 |DAILY BEAST 

According to the memo, Miller then asked what the statute of limitations in Illinois was and to define what the allegations meant. Chicago Priests Raped and Pillaged for 50 Years |Barbie Latza Nadeau |November 7, 2014 |DAILY BEAST 

The military did not publish a memo outlining Ebola protection guidelines until Oct. 16. U.S. Soldiers Get Just Four Hours of Ebola Training |Tim Mak |October 17, 2014 |DAILY BEAST 

"I'll probably get word on it by the time someone has it all organized into a nice, official memo," Flannery said. Victory |Lester del Rey 

Moreover, the suggestion of holding the Assembly responsible is to be found as early as in the memo. Encyclopaedia Britannica, 11th Edition, Volume 17, Slice 6 |Various 

Where dollars are concerned it is good sense to trust to a written memo., and not to any mental memo. Dollars and Sense |Col. Wm. C. Hunter 

He had no respect whatever for the Pass Memo., his central and sole idea being to push along with the elimination of the Bosch. Punch, or the London Charivari, Volume 150, February 2, 1916 |Various 

Cam dictated a memo to his pocket recorder forbidding MAB girls to observe the current abbreviated fashions. Telempathy |Vance Simonds