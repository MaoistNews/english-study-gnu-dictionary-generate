I feel anger, too, that BIPOCs will continue to die because of the legacies of medical racism, that children in Pakistan will die of polio despite an effective vaccine. How the CIA’s fake vaccine program in Pakistan helped fuel the anti-vax movement |Hala Iqbal |February 1, 2021 |Vox 

Most of these outbreaks — which have been responsible for more polio cases in the last few years than the remaining type of wild poliovirus — are linked to vaccine virus type 2. A new polio vaccine joins the fight to vanquish the paralyzing disease |Aimee Cunningham |January 8, 2021 |Science News 

McConnell, 78 and a polio survivor, encouraged Americans to continue wearing masks and practice social distancing as currently recommended by federal health officials. Pelosi receives shot of coronavirus vaccine from the Capitol’s top doctor |Paul Kane, John Wagner |December 18, 2020 |Washington Post 

In Herat, they hoped to get Saidgul vaccinated against polio, since clinics in their district had shut down without warning due to the Covid-19 pandemic. The Covid-19 pandemic is driving new cases of polio in Afghanistan |Ruchi Kumar |December 8, 2020 |Quartz 

When a vaccine becomes available, this polio group will likely help out as well. Buying A Coronavirus Vaccine For Everyone On Earth, Storing And Shipping It, And Giving It Safely Will All Be Hard And Expensive |LGBTQ-Editor |November 14, 2020 |No Straight News 

Last year, a polio outbreak in Deir ez-Zor raised concerns throughout the region about the spread of an epidemic. U.S. Humanitarian Aid Going to ISIS |Jamie Dettmer |October 20, 2014 |DAILY BEAST 

Then Cutter Laboratories in Berkeley, California, made a bad batch of vaccine, and 40,000 children were sickened with polio. How Presidents Handle Pandemics |Eleanor Clift |October 16, 2014 |DAILY BEAST 

After she battled polio and learned to walk again, the doctors told her she would be a cripple her entire life. Uzo Aduba: My Road to ‘Orange Is the New Black’ |Uzoamaka Aduba |August 4, 2014 |DAILY BEAST 

Fifty-eight children took part in a trial into polio and diptheria vaccines in December 1960. Irish Care Home Scandal Grows As Children Revealed To Be Used As Vaccine Guinea Pigs |Tom Sykes |June 10, 2014 |DAILY BEAST 

Either way, part of the tragedy and poignancy of polio is its preferential spread to babies and toddlers. Who’s to Blame for Global Polio Emergency? |Kent Sepkowitz |May 6, 2014 |DAILY BEAST 

So far, none of the contagious infections except polio and the common cold had made the jump. Badge of Infamy |Lester del Rey 

Wood up thar, you Polio—hang on to the safety valve—guess she'll crawl off on her paddles. The Octoroon |Dion Boucicault 

Nagtakihud siya tungud sa pulyu, He limps because he had polio. A Dictionary of Cebuano Visayan |John U. Wolff 

Nalúlid siya kay gitakbúyag pulyu, He was crippled after his bout with polio. A Dictionary of Cebuano Visayan |John U. Wolff 

She walked with a barely noticeable limp—polio in childhood, Mann recalled from the record. The Trial of Callista Blake |Edgar Pangborn