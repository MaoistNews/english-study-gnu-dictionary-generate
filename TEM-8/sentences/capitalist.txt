For any SPAC investors or venture capitalists worried that they’re now left out of the EV plane investment bonanza, take heart! With a reported deal in the wings for Joby Aviation, electric aircraft soars to $10B business |Jonathan Shieber |February 12, 2021 |TechCrunch 

Startups as a whole raised 13% more from venture capitalists in 2020 compared to 2019, for an annual total of $150 billion, according to PitchBook data. Female founders’ share of venture capital funding shrank to 2.2% in 2020 |Emma Hinchliffe |February 8, 2021 |Fortune 

The capitalist system, she says, depends on you believing that deception. Why You Don't Feel as Fulfilled From Your Job as You Think You Should |Eliana Dockterman |January 25, 2021 |Time 

Provocatively titled Seed Investments in Insurrection, his argument was that venture capitalists needed to wrestle with their impact on democracy. Tech is having a reckoning. Tech investors? Not so much. |Eileen Guo |January 24, 2021 |MIT Technology Review 

In October, the startup announced a $3 million seed round from multiple venture capitalists. The Boom of the Burnout Business |Charu Kasturi |January 22, 2021 |Ozy 

Nothing in it was meant to change the basic operations of the capitalist economy or to intervene aggressively in class relations. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

Beck is in the same position as any post-industrial capitalist entrepreneur. Glenn Beck Is Now Selling Hipster Clothes. Really. |Ana Marie Cox |December 20, 2014 |DAILY BEAST 

At home, Lobanov's favorite books were those that showed the aircraft of capitalist countries like the US, Britain, and France. The World's Most Beautiful Boat—Yours for Half a Billion Dollars |Tim Teeman |October 19, 2014 |DAILY BEAST 

Capitalist-led industrial growth shifted the proportion of the population living in cities. In the Future We'll All Be Renters: America's Disappearing Middle Class |Joel Kotkin |August 10, 2014 |DAILY BEAST 

Calling capitalist ownership parasitism is an extreme position, perhaps, but one common enough among Occupiers. Occupying the Throne: Justine Tunney, Neoreactionaries, and the New 1% |Arthur Chu |August 1, 2014 |DAILY BEAST 

Properly applied, the capitalist and the employer of labor need have nothing to fear from it. The Unsolved Riddle of Social Justice |Stephen Leacock 

The good will of that concern was, however, purchased by M. Ducroquet (a capitalist), who entrusted him with its management. The Recent Revolution in Organ Building |George Laing Miller 

More than one capitalist put down his name for thirty thousand pounds. The History of England from the Accession of James II. |Thomas Babington Macaulay 

The suffering of individuals, of large masses, indeed, is unavoidable under capitalist conditions. Prison Memoirs of an Anarchist |Alexander Berkman 

How boldly he acted during the Haymarket tragedy—publicly advised the use of violence to avenge the capitalist conspiracy. Prison Memoirs of an Anarchist |Alexander Berkman