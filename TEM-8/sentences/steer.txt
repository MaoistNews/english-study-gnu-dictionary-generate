The Mustang Mach-E does not currently have an ability to steer itself, but that is a feature that will be added in the future, according to Ford. Ford’s electric Mustang Mach-E is an important leap into the future |Dan Carney |February 12, 2021 |Popular-Science 

All-wheel steeringOn a typical car, only the front wheels turn when you steer. Audi’s e-Tron GT charges up fast, but turns even faster |Stan Horaczek |February 10, 2021 |Popular-Science 

He was sentenced in 2019 to three months in prison for accepting payment to steer players toward agents and financial advisers, part of the fallout of the seemingly forever-ago FBI investigation into college hoops corruption. NCAA tournament bracketology: Oklahoma State will probably make it despite its postseason ban |Patrick Stevens |January 26, 2021 |Washington Post 

We might well wish that covid-19 dashboards functioned as both protective barriers and a way to see the information we need to steer ourselves clear of trouble. Covid-19 dashboards are vital, yet flawed, sources of public information |Jacqueline Wernimont |January 26, 2021 |Washington Post 

It’s Nash’s job to steer Irving toward more ball movement and to make sure that Durant and Harden don’t become bystanders. With Kyrie Irving back, rookie coach Steve Nash has to figure out how to get his stars aligned |Ben Golliver |January 21, 2021 |Washington Post 

And Epstein continues to steer money toward universities to advance scientific research. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

This is the Mexico that U.S. college students would be wise to steer clear of on spring break. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

A successful trend-maker might be able to steer a conversation, but virality remains extremely difficult to predict. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

In first person, Grand Theft Auto lets you be the kind of criminal you want to be, rather than just steer one. I Felt Like Showering After the First-Person Sex in ‘Grand Theft Auto’ |Alec Kubas-Meyer |November 22, 2014 |DAILY BEAST 

I have just been the man in the middle, trying to make sure that we steer the right course. Dan Malloy Is Progressives’ Dream Governor. So Why Isn’t He Winning? |David Freedlander |October 30, 2014 |DAILY BEAST 

Then, with one accord, they all rose and began to steer their way around the furniture toward the hall, Goliath following. The Boarded-Up House |Augusta Huiell Seaman 

Knowing by experience that he would soon be up to it, he used his pole with all his might, hoping to steer clear of it. The Giant of the North |R.M. Ballantyne 

Well, to steer a middle course between my duty to my force and my loyalty to K. is not so simple as it might seem. Gallipoli Diary, Volume I |Ian Hamilton 

Looks like good stock, that calf does; 's if 't might make a nice steer, but 'twon't never be a cow to give milk. Dorothy at Skyrie |Evelyn Raymond 

With his hat he signalled his brother to steer for the General Price, and on the two rams rushed, the Queen slightly ahead. The Courier of the Ozarks |Byron A. Dunn