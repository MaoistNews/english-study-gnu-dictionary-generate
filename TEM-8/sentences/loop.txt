To tally the possibilities implied by a loop, theorists must turn to a summing operation known as an integral. The Mathematical Structure of Particle Collisions Comes Into View |Charlie Wood |August 20, 2020 |Quanta Magazine 

The McAfee researchers say their goal is ultimately to demonstrate the inherent vulnerabilities in these AI systems and make clear that human beings must stay in the loop. The hack that could make face recognition think someone else is you |Karen Hao |August 5, 2020 |MIT Technology Review 

As you have now navigated the ins and outs of the forecast, there’s still one more thing that may get the customer stuck in a loop. How to craft a winning SEO proposal and avoid getting a silent ‘No’ |Sponsored Content: SEOmonitor |August 3, 2020 |Search Engine Land 

In contrast, the new prediction method is rooted in the intricacies of how and when the sun’s tangled loops of magnetic fields rearrange themselves, in a process known as magnetic reconnection, releasing bursts of energy that mark solar flares. The physics of solar flares could help scientists predict imminent outbursts |Emily Conover |July 30, 2020 |Science News 

How exactly that would happen is far from clear, so for the time being it seems like it might be a good idea to keep humans in the loop for most AI decision-making. AI Behaving Badly: New Model Could Help AI Make More Ethical Choices |Edd Gent |July 6, 2020 |Singularity Hub 

Just being able to be in the loop when something is happening, it just works. Anastasia Ashley, Surfer-Cum-Model, Rides The Viral Internet Wave |James Joiner |December 23, 2014 |DAILY BEAST 

The outré character is sure to throw even the most ardent fans of the Golden Globe winner for a loop. Michael C. Hall on Going Drag for ‘Hedwig and the Angry Inch’ and Exorcising ‘Dexter’ |Marlow Stern |December 4, 2014 |DAILY BEAST 

From the roof of the barn is a long loop of rope, through this the turkey is suspended by its legs. Confessions of a Turkey Killer |Tom Sykes |November 26, 2014 |DAILY BEAST 

I am coming from Venice [Film Festival], so I am a little bit out of the time loop. Al Pacino Does What He Wants to Do: 'The Humbling,' Scorsese, and That 'Scarface' Remake |Alex Suskind |September 9, 2014 |DAILY BEAST 

The system could operate on a closed loop, recycling its water and harnessing the power of the sun. Why Architects Dream Big—and Crazy |Nate Berg |August 23, 2014 |DAILY BEAST 

Finish off with a button and loop, and flute the frill on each side over the finger to make it set. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

The hawser was made into a loop around his body and the other end was tied around the mother. Kari the Elephant |Dhan Gopal Mukerji 

This time we made a double loop around him, and also made him hold on to the rope around the tree with his trunk. Kari the Elephant |Dhan Gopal Mukerji 

A small loop, slipped over the point of the lower stick, held the noose in position. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

A small nail or button—anything larger than the loop in the wire—should be attached to the kite string a few feet from the kite. The Boy Mechanic, Book 2 |Various