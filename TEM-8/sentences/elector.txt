Pennsylvania, Wisconsin and the rest of the states certified their electors. The Secret History of the Shadow Campaign That Saved the 2020 Election |Molly Ball |February 4, 2021 |Time 

The Constitution, he wrote, “leaves it to the legislature exclusively to define the method” for appointing electors. Sen. Hawley has been condemned. His bad legal arguments should be stamped out, too. |Daniel Epps, Alan Trammell |January 20, 2021 |Washington Post 

He objected to electors from Arizona and Pennsylvania in votes after the Capitol had been secured. The crowd outside the Capitol last week included Rep. Bob Good’s district director |Meagan Flynn |January 15, 2021 |Washington Post 

I’m leading the fight to reject key electors from key states unless there is an emergency audit of the election results. Democrats and Republicans unite in making the Capitol riots a fundraising issue |Nicole Goodkind |January 8, 2021 |Fortune 

Wisconsin’s votes were counted because Congress had agreed that an “Act of God” had prevented the electors from reaching the state capital of Madison on time. A blizzard, a disputed electoral vote count and the 1887 law tying Pence’s hands |Ronald G. Shafer |January 6, 2021 |Washington Post 

It was decided that the Elector and his family should leave Bonn. Beethoven in Love: The Woman Who Captivated the Young Composer |John Suchet |January 26, 2014 |DAILY BEAST 

All of the elector-Cardinals were appointed by John Paul II and Benedict XVI. Who Will Replace Pope Benedict XVI? |Justin Green |February 11, 2013 |DAILY BEAST 

The first thought which occurred to William was that it might be possible to put the Elector of Bavaria in his son's place. The History of England from the Accession of James II. |Thomas Babington Macaulay 

On receiving this ultimatum, the magistrates asked for time to communicate with the Elector and the King of England. Belgium |George W. T. (George William Thomson) Omond 

A loyal gallant Elector this, it must be owned; capable withal of doing signal damage if we irritated him too far! History Of Friedrich II. of Prussia, Vol. I. (of XXI.) |Thomas Carlyle 

To the huge joy of Elector Friedrich and his Court, almost the very nation thinking itself glad. History Of Friedrich II. of Prussia, Vol. I. (of XXI.) |Thomas Carlyle 

No other Elector held them both, for nearly a hundred years; nor then, except as it were for a moment. History Of Friedrich II. of Prussia, Vol. III. (of XXI.) |Thomas Carlyle