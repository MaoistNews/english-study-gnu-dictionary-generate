What I more and more felt, as the trauma deepened, was that while my body survived, the self that I had once been had lost its life. Larry McMurtry, award-winning novelist who pierced myths of his native Texas, dies at 84 |Joe Holley |March 26, 2021 |Washington Post 

Instead of resembling a plastic canister, the Nest Audio speaker is sleeker, akin to a 7-inch pill draped in felt. Here’s what Google’s new Nest Audio speakers are like |jonathanvanian2015 |October 5, 2020 |Fortune 

He felt his body grow limp (like one of those high-speed films of a flower wilting). Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

The EPA felt that the State Department had not looked carefully enough at the impact of the pipeline if oil prices fell. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

Whatever frustrations or disappointments he felt about politics never surfaced. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 

I—I never felt that way about the presidency, as you—as you probably know. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 

It was one of the few things that felt familiar to him after being away from the outside world since 1975. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

She was flushed and felt intoxicated with the sound of her own voice and the unaccustomed taste of candor. The Awakening and Selected Short Stories |Kate Chopin 

Genoa has but recently and partially felt the new impulse, yet even here the march of improvement is visible. Glances at Europe |Horace Greeley 

All felt strangely as if something evil had crept into their lives, and their excitement was great. The Homesteader |Oscar Micheaux 

He made me think of an old time magician more than anything, and I felt that with a touch of his wand he could transform us all. Music-Study in Germany |Amy Fay 

Her feet felt rooted to the floor in the wonder and doubt of this strange occurrence. The Bondboy |George W. (George Washington) Ogden