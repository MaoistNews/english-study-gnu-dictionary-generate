Instead, he spread the Overview Effect message, even though the response for years was crickets. Can Astronaut Thinking Heal America at Last? |Leslie dela Vega |January 28, 2021 |Ozy 

Salim Abdool Karim was at a cricket match on December 26, Boxing Day, when he made the mistake of looking at his email. We could know soon whether vaccines work against a scary new coronavirus variant |Antonio Regalado |January 23, 2021 |MIT Technology Review 

He creates melodies to the rhythms of crickets, katydids, and a whole orchestra of bugs. The Hills Are Alive with the Sounds of Bugs |Outside Editors |January 22, 2021 |Outside Online 

Some male crickets make their own megaphones by cutting wing-sized holes into the center of leaves. Small, quiet crickets turn leaves into megaphones to blare their mating call |Jonathan Lambert |December 16, 2020 |Science News 

One pound of crickets provides three times the protein, as well as more iron and nutrients, than a pound of beef. Why We Should Eat Crickets. And Other Bug Ideas - Facts So Romantic |Mary Ellen Hannibal |October 2, 2020 |Nautilus 

Cricket is a sport enjoyed by hundreds of millions around the globe, mainly in former British colonies. The Story of the World’s Greatest Cricket Player |William O’Connor |December 24, 2014 |DAILY BEAST 

His life, as he himself admits, is all cricket from an early age. The Story of the World’s Greatest Cricket Player |William O’Connor |December 24, 2014 |DAILY BEAST 

He experienced a rapid rise, only beginning to play cricket competitively at age 11. The Story of the World’s Greatest Cricket Player |William O’Connor |December 24, 2014 |DAILY BEAST 

Three kids play cricket among the crude gravestones in a cemetery that is the largest in the province. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

But the language about public schools and cricket bewildered audiences and Frank Rich gave it a stinking review. Bring ‘Another Country’ to Broadway: Why a Hit British Classic Needs Its New York Moment |Tom Teodorczuk |June 2, 2014 |DAILY BEAST 

A cricket-match was in progress, but the bowling and batting were extremely wild, thanks to The Warren strong beer. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He was a manly young fellow, a sportsman and renowned at cricket, and she was amiable and pretty, a little blonde beauty. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

So still was the place that the caged cricket hanging from the eaves of Um's distant room beat time like an elfin metronome. The Dragon Painter |Mary McNeil Fenollosa 

In one such startled interval of waking her caged cricket had given out its plaintive cry. The Dragon Painter |Mary McNeil Fenollosa 

Somewhere near her feet the cricket gave out an importunate chirp. The Dragon Painter |Mary McNeil Fenollosa