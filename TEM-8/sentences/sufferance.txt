It is a story about who gets to go where, who gets to exist safely in public, and who is only there on sufferance. We love you. You’re very special. Go home. |Alexandra Petri |January 7, 2021 |Washington Post 

Having no tenure, and his future career being at the sufferance of her political enemies, objectively he lacked independence. Is Julia Tymoshenko Europe’s Aung San Suu Kyi? |Geoffrey Robertson |October 23, 2012 |DAILY BEAST 

It was after Amadeo had thrown down his crown, exclaiming, "A son of Savoy does not wear a crown on sufferance!" Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

Thus he sat, dejection and despair stamped on his homely face; haughty, yet a suppliant; a king, yet only by sufferance. The Life of Napoleon Bonaparte |William Milligan Sloane 

It was hard to realize that he could see Blent now only by another's will or sufferance. Tristram of Blent |Anthony Hope 

He glanced with sufferance at the window, which offered a close-range view of a whitewashed wall. The Regent |E. Arnold Bennett 

All these cottages and allotments have only been held on sufferance, on good behaviour, and hence they have failed. The Hills and the Vale |Richard Jefferies