After losing his sight to smallpox in 1759 at the age of 2, John Gough developed a heightened sense of touch. The Shape-Shifting Squeeze Coolers |Marcus Woo |August 24, 2020 |Quanta Magazine 

Consistency of posting is also key to staying Instagram-relevant, hence the need to be visible and active to remain in users’ sight. How to optimize for the Instagram algorithm in 2020 |Julia Miashkova |August 19, 2020 |Search Engine Watch 

Amid the maelstrom of negative news about the dangers of misinformation and the exploitation of private data, it’s easy to lose sight of how technology might help. Want to Fix Your Country? Czech This Box |Dan Peleschuk |August 13, 2020 |Ozy 

Use the right mix of short-tail, long-tail, geo-targeting, and LSI keywords to help your business become a frequent sight on the first page of Google for industry-specific and local searches. What Google says about nofollow, sponsored, and UGC links in 2020: Does it affect your SEO rankings? |Joseph Dyson |July 24, 2020 |Search Engine Watch 

Last week, you set your sights on breaking baseball records, albeit in a shortened season. Are You A Pinball Wizard? |Zach Wissner-Gross |July 24, 2020 |FiveThirtyEight 

There was the obvious sight gags of Valerie not realizing who everyone was clapping for, when the party was clapping for her. ‘The Comeback’ Finale: Give Lisa Kudrow All of the Awards |Kevin Fallon |December 29, 2014 |DAILY BEAST 

The gathering of the thousands of cops had been a soul-stirring sight. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

Thanks to the Atlanta case, they can now see another in plain sight. A Gift to the Jihadis: The Unseen Airport Security Threat |Clive Irving |December 27, 2014 |DAILY BEAST 

I wonder if the seasoned salesman can spot the billionaires on sight. Sneer and Clothing in Miami: Inside The $3 Billion Woodstock of Contemporary Art |Jay Michaelson |December 6, 2014 |DAILY BEAST 

In the afternoon, about a thousand people marched in protest through the largest Prague square, with police nowhere in sight. How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

And he was gone, and out of sight on the swift galloping Benito, before Father Gaspara bethought himself. Ramona |Helen Hunt Jackson 

A little boy of four was moved to passionate grief at the sight of a dead dog taken from a pond. Children's Ways |James Sully 

But at the instant I caught a sight of my counterfeit presentment in a shop window, and veiled my haughty crest. God and my Neighbour |Robert Blatchford 

The Vine is a universal favorite, and rarely out of view; while it often seems to cover half the ground in sight. Glances at Europe |Horace Greeley 

At the sight, Felipe flung himself on his knees before her; he kissed the aged hands as they lay trembling in her lap. Ramona |Helen Hunt Jackson