The rest plan to restructure their debts, close money-losing stores, and continue operation. Why bankruptcy is rarely the end for retailers in dire straits |Alexandra Ossola |September 17, 2020 |Quartz 

“There is so much debt production and debt monetization,” Dalio said. Ray Dalio issues stark warning about U.S. dollar’s future as global reserve currency |Claire Zillman, reporter |September 16, 2020 |Fortune 

The cost of the combined wars will probably surpass $7 trillion by 2056, when interest on the debt is considered, almost four decades from now. Trump’s ABC News town hall: Four Pinocchios, over and over again |Glenn Kessler |September 16, 2020 |Washington Post 

They also like precious metals and debt of just about any flavor. Is M&A back? Investors hope so, and that’s lifting global stocks |Bernhard Warner |September 15, 2020 |Fortune 

Local banks, meanwhile, keep securitizing their mortgage debt, sloughing off their own liabilities. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

Big discounts and cheap credit keep them coming back for more; and keep millions in perpetual debt. Christmas Is the New Subprime |Doug McIntyre |December 9, 2014 |DAILY BEAST 

Some of the concern over student debt is likely driven by the startling headline numbers. The Student Loan Crisis That Isn’t About Kids at Harvard |Monica Potts |November 30, 2014 |DAILY BEAST 

These low-income students take on debt and are also the least likely to finish. The Student Loan Crisis That Isn’t About Kids at Harvard |Monica Potts |November 30, 2014 |DAILY BEAST 

By 2013, the company owed the lender about $35,000 per month—solely in interest payments—and imploded in debt. Patients Screwed in Spine Surgery ‘Scam’ |The Center for Investigative Reporting |November 3, 2014 |DAILY BEAST 

“So let history record that Iran owes an invaluable debt to al Qaeda,” he added. Iran Says It’s Under Attack by ISIS |Jassem Al Salami |October 9, 2014 |DAILY BEAST 

It is painful to add, that the latter years of his life were passed in prison, where he was confined for debt. The Every Day Book of History and Chronology |Joel Munsell 

It sent gold to Paris as fast as it could be shipped and insured, and so seems to have liquidated its debt. Readings in Money and Banking |Chester Arthur Phillips 

This rascal was owed a debt for the indignity he had offered the sahib in the village, and now he was paid in full. The Red Year |Louis Tracy 

In conclusion, I must distress my friend J. M. Barrie (who gave me a first chance) by acknowledging my great debt to him. First Plays |A. A. Milne 

I owe you a large debt of gratitude, which I want to work out—so do not talk of sending me away. The World Before Them |Susanna Moodie