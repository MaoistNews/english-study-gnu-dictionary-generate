The Spencers are a very hot headed, impetuous family with a lot of swashbuckling drama. Tina Brown Breaks Down the Biggest Royal Family Revelations From Her New Book |Charlie Campbell / London |April 25, 2022 |Time 

According to the Madman Theory in international relations, a leader who is seen as impetuous, even unhinged, can coerce an adversary into concessions. It’s Not Irrational to Party Like It’s 1999 - Issue 108: Change |Steven Pinker |November 17, 2021 |Nautilus 

In Lowery’s film, Gawain is young, impetuous, prone to carousing, and ashamed of how little of his life has been spent on bold and brave exploits. The Green Knight is glorious and a little baffling. Let’s untangle it. |Alissa Wilkinson |July 30, 2021 |Vox 

You see where you were more impetuous, where you might have overreacted to certain things. All Eyes on Anjelica Huston: The Legendary Actress on Love, Abuse, and Jack Nicholson |Alex Suskind |November 10, 2014 |DAILY BEAST 

The old Kaiser Franz Joseph, faithful and hardworking, was the obverse of the feckless and impetuous German kaiser. Before the Fall: What Did the World Look Like in 1913? |Jacob Heilbrunn |June 9, 2013 |DAILY BEAST 

Zakir is a fearless and impetuous fighter, a former Guantanamo prisoner who earned a reputation for brutality on the battlefield. Afghanistan: Will the Taliban Destroy Itself? |Sami Yousafzai |December 17, 2012 |DAILY BEAST 

Referring to his numerous divorces, he writes, "I was young, impetuous, and lonely." The Daily Beast Recommends |The Daily Beast |May 19, 2009 |DAILY BEAST 

It was a kind of incredible performance, half on earth and half in the air: it rushed with such impetuous momentum. The Wave |Algernon Blackwood 

"You have been placed under arrest by order of the Ministry," replied Bézard, speaking in his quick, impetuous way. The Doctor of Pimlico |William Le Queux 

Oh, those Camford conversations—how impetuous, how interesting, how thoroughly hearty and unconventional they were! Julian Home |Dean Frederic W. Farrar 

A rush of impetuous words followed by the collapse of his father's form upon the pillow showed that the examination was over. The Circular Study |Anna Katharine Green 

It was an exciting moment as that great bulk came on, its tons of sodden wood backed by the impetuous forces of the torrent. Gold-Seeking on the Dalton Trail |Arthur R. Thompson