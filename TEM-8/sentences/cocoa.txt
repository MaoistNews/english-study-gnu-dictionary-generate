Flavors of dark fruits yield to mushrooms and cocoa on a sweet-ripe finish with soft tannins. Snag the equivalent of four bottles of flavorful red wine in this $30 box |Dave McIntyre |March 5, 2021 |Washington Post 

Negranza points out the House of Angostura’s recently-released cocoa bitters—made with cacao nibs sourced from Trinidad and Tobago, where the company is based—as a solid standby. How to melt, mix, and balance chocolate in cocktails |By Dan Q. Dao/Saveur |March 4, 2021 |Popular-Science 

While most would-be home buyers are busy trimming trees and decking the halls, you can grab a cup of hot cocoa and experience all the benefits of house hunting during the holidays from your living room. Benefits of house hunting during the holidays |Khalil Alexander El-Ghoul |December 6, 2020 |Washington Blade 

No one disputes how bad conditions are on those farms, and the Supreme Court briefs filed by the cocoa workers’ lawyers make for chilling reading—a tale about horrific abuse of children, for the benefit of global commerce. The Supreme Court will decide: Can Big Chocolate be blamed for child slavery? |Vivienne Walt |November 26, 2020 |Fortune 

On Tuesday, another years-in-the-making conflict will play out at the Supreme Court, when the nine justices finally consider whether Nestlé and Cargill are responsible for the use of child slavery on cocoa farms in West Africa. The Supreme Court will decide: Can Big Chocolate be blamed for child slavery? |Vivienne Walt |November 26, 2020 |Fortune 

He was a beautiful child, sweet natured, affectionate, with cocoa-colored skin and a thousand-watt smile. The Cost: What Stop and Frisk Does to a Young Man’s Soul |Rilla Askew |May 21, 2014 |DAILY BEAST 

After the blaze was extinguished, a retired cop came over with hot cocoa. Eight Year Old Rides to a Blaze With His Firefighter Grandfather |Michael Daly |January 19, 2014 |DAILY BEAST 

Talese, the New Journalism maestro, would send Lipsyte on cocoa runs. On the Peninsula |Bryan Curtis |April 25, 2011 |DAILY BEAST 

Sweeten the cocoa with the sugar, adding more or less according to your taste. How Top Chefs Stay Thin |Rachel Syme |December 15, 2009 |DAILY BEAST 

It goes beautifully with dark chocolate, making for an enticingly spicy version of hot cocoa. How Top Chefs Stay Thin |Rachel Syme |December 15, 2009 |DAILY BEAST 

She opened the two boxes they had brought and set the vacuum bottle of hot cocoa on the bench. The Campfire Girls of Roselawn |Margaret Penrose 

For each guest there was a cocoa-nut shell, half-filled with miti, a sourish beverage extracted from the cocoa-palm. A Woman's Journey Round the World |Ida Pfeiffer 

We had each of us a fresh cocoa-nut with a hole bored in it, containing at least a pint of clear, sweet-tasting water. A Woman's Journey Round the World |Ida Pfeiffer 

Presently, this indulgence is extended to "dry coffee or cocoa," if preferred, and the men also are allowed to receive it. English Poor Law Policy |Sidney Webb 

Now she was going back, expecting to pick up a cargo of rubber and cocoa and what not, along the West Coast. The Belted Seas |Arthur Colton