The front of the Capitol is divided into terraces linked by stairs, and Glover first positioned officers on the middle terrace. How battered D.C. police made a stand against the Capitol mob |Peter Hermann |January 15, 2021 |Washington Post 

Beyond my pod, I can p-set with my friends outdoors on a terrace, and it’s a major upgrade over our usual p-set Zooms. A final fall on campus |Katie McLean |December 18, 2020 |MIT Technology Review 

In this regimented fall, Blank laments the loss of community rituals — the pep rallies with the Bucky the Badger mascot, and the afternoon crowds that would soak in sun and the sweeping vistas of Lake Mendota from the terrace of Memorial Union. College students hit the road after an eerie pandemic semester. Will the virus go home with them? |Nick Anderson, Susan Svrluga |November 22, 2020 |Washington Post 

Because the terraces are open on all sides, they allow air to flow across them naturally. Changing climates can take cooling tips from warm regions |Sharon Oosthoek |October 8, 2020 |Science News For Students 

They could do what one guy did and run a marathon on his terrace during lockdown. Canceled Races Aren’t Stopping Endurance Athletes From Setting Wild New Records |Anna Wiederkehr (anna.wiederkehr@abc.com) |July 20, 2020 |FiveThirtyEight 

“They would bring them to the terrace and say, ‘You pay or you are his next meal,’” Trapuzzano explains. Days of Mafia Mayhem Are Wracking Italy Once Again |Barbie Latza Nadeau |November 22, 2014 |DAILY BEAST 

The apartment is quarantined but a terrace door was left open so the dog could go outside “to do his business.” The Dog is Dead—And We’ll Never Know if He Had Ebola |Barbie Latza Nadeau |October 9, 2014 |DAILY BEAST 

On a New York terrace, there is a dinner dance one balmy summer evening. Adam Hochschild on Keeping Company With His Dying Father |Adam Hochschild |June 14, 2014 |DAILY BEAST 

We thought of walking on the park terrace along the North River. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

“Step into the Void” is the newest attraction on the uppermost terrace of Aiguille du Midi. A New Installation in the French Alps Allows Visitors to Walk Off the Highest Mountain Peak |Justin Jones |December 21, 2013 |DAILY BEAST 

The villa and its terrace were built of white stone, but a large portion of the walls was covered with ivy. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

In the middle of the warmest room is a round terrace-like elevation, called Gobek-tosh. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

We resided at Derby in a terrace on the outskirt of the town, much to my dislike, for monotonous rows of houses I have ever hated. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Before he could say a word she had crossed a little terrace, disappeared through a French window, and vanished into the villa. Bella Donna |Robert Hichens 

She had just begun to think that she had better try the Casino, when down the steps from the upper terrace came three figures. Rosemary in Search of a Father |C. N. Williamson