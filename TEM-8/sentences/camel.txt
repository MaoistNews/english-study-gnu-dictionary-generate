Wernery and Eriksen call those they’ve found in the camels “polybezoars.” Camels have been dying after mistaking plastic for food |Asher Jones |January 15, 2021 |Science News For Students 

Before long, they spotted a camel skeleton and began to dig through sand and bones. Plastic waste forms huge, deadly masses in camel guts |Asher Jones |December 15, 2020 |Science News 

With a stomach full of plastic, camels don’t eat because they don’t feel hungry, and they starve to death. Plastic waste forms huge, deadly masses in camel guts |Asher Jones |December 15, 2020 |Science News 

If all goes well, the camels will make antibodies to both viruses. When physicians and veterinarians team up, all species benefit |Liz Devitt |November 5, 2020 |Science News For Students 

Getting young camels to make antibodies to the virus would protect people. When physicians and veterinarians team up, all species benefit |Liz Devitt |November 5, 2020 |Science News For Students 

The “clairvoyant camel” has picked Argentina over Germany in the World Cup final. The Amazing Tale of Paul the Psychic Octopus: Germany’s World Cup Soothsayer |Emily Shire |July 12, 2014 |DAILY BEAST 

In short: It is so convenient that it makes a taxi seem about as high-tech as a camel. Inside Uber’s Political War Machine |Olivia Nuzzi |June 30, 2014 |DAILY BEAST 

There was something rather theatrical—surreal even—about walking into camp accompanied by a camel caravan later that evening. On Foot in the High Atlas Mountains of Morocco |Joanna Eede |January 22, 2014 |DAILY BEAST 

Later, I fell asleep to the sound of Lhoucine gently coaxing his camel and faint ululating from a distant stone cottage. On Foot in the High Atlas Mountains of Morocco |Joanna Eede |January 22, 2014 |DAILY BEAST 

Joe would often appear looking suave or cool in various brightly colored situations - as cool as a man with a camel face can look. Big Tobacco’s Biggest Lies |Gideon Resnick |January 20, 2014 |DAILY BEAST 

He and I went through many distant places in India with camel caravans, carrying loads of silver and gold, spices and fruits. Kari the Elephant |Dhan Gopal Mukerji 

I followed his advice, allowed my luggage to be carried, and patiently mounted my camel. A Woman's Journey Round the World |Ida Pfeiffer 

With the soft tuft of camel hair he blurred against the peak pale, luminous vapor of new cloud. The Dragon Painter |Mary McNeil Fenollosa 

And the same John had his garment of camel's hair, and a leathern girdle about his loins: and his meat was locusts and wild honey. The Bible, Douay-Rheims Version |Various 

Junglee was close behind the camel leading my pony, and the others in the rear, but all in their places. Confessions of a Thug |Philip Meadows Taylor