We know more than ever about how students reason, process information and solve problems. Why hasn’t digital learning lived up to its promise? |Walter Thompson |September 17, 2020 |TechCrunch 

I always think about what my favorite creators are building in their homes and what problems they are trying to solve. Kerby Jean-Raymond Launch ‘Your Friends In New York’ |Nandi Howard |September 11, 2020 |Essence.com 

Whoever’s fault it is, we either all chip in to solve it or we all suffer. What’s causing climate change, in 10 charts |David Roberts |September 11, 2020 |Vox 

I was only looking for a generic term that helps me to solve my most immediate problem, which is to create a logo. Inbound marketing for brand awareness: Four up-to-date ways to do it |Ali Faagba |September 11, 2020 |Search Engine Watch 

She wants them to think about different ways to solve a problem. A secret of science: Mistakes boost understanding |Rachel Kehoe |September 10, 2020 |Science News For Students 

But one extra trick would instantly solve the problem of crashes that occur over water. Red Tape and Black Boxes: Why We Keep ‘Losing’ Airliners in 2014 |Clive Irving |December 29, 2014 |DAILY BEAST 

This is the first study of its kind in Turkey and raises the possibility that a private solution could solve a public problem. The Women Battling an Islamist Strongman |Christina Asquith |December 22, 2014 |DAILY BEAST 

To solve the problem, we need to study a lot more comets and meteorites. Are Comets the Origin of Earth’s Oceans? |Matthew R. Francis |December 14, 2014 |DAILY BEAST 

Is there anyone who thinks the urgent problem we need to solve in Washington, D.C. is how to allow more spending on campaigns? Just What We Needed: More Campaign Spending |Mark McKinnon |December 8, 2014 |DAILY BEAST 

“I doubt you can solve range and the need for a large magazine with the same missile,” he said. Pentagon Worries That Russia Can Now Outshoot U.S. Stealth Jets |Dave Majumdar |December 4, 2014 |DAILY BEAST 

To solve this one I stopped on the tavern steps, leaned against a pillar, and gazed through the dozing village. The Soldier of the Valley |Nelson Lloyd 

But, though a capital sabreur, he was evidently not made to solve questions in diplomacy. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

In the preceding chapter we have endeavoured to solve the question what are the qualities that constitute good tone. Violins and Violin Makers |Joseph Pearce 

The meetings were held secretly behind closed doors because of the grave problems which the convention had to solve. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

To solve this problem, is to know the remedy; and to know it, is but necessary, in order successfully to apply it. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany