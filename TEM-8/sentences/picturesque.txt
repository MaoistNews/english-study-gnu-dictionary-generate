Such picturesque qualities showed where we were, but not where we were headed. Monday was a summer landmark: The last sunrise before 6:30 a.m. |Martin Weil |August 24, 2021 |Washington Post 

Together they hike trails that pass through ancient ruins, overlooking picturesque bays where cruise ships drop their guests for lunch. Climate Change Won't Kill Tourism, But the Industry Is in for a Painful Reckoning |Ciara Nugent |August 18, 2021 |Time 

They’re rustic but come stocked with real beds, stoves, and picturesque cocktail porches. Add These 5 Backcountry Lodges to Your Adventure Bucket List |elessard |August 12, 2021 |Outside Online 

This part of the state also happens to offer some of its most picturesque cycling—and a hearty dose of intriguing Texas history. Texas’s Best Riding for Every Kind of Cyclist |elessard |July 27, 2021 |Outside Online 

The goal, Marasco said, is that picturesque Hillsboro, a one-time mill town, would become a destination — not only a stop for tourists visiting breweries and wineries nearby. As infrastructure debate lingers, $34 million and a new sidewalk are bringing life to a small Virginia town |Luz Lazo |July 23, 2021 |Washington Post 

Chefchaouen, shut off from the world for centuries, is almost absurdly picturesque. Morocco's Secret All-Blue City |Nina Strochlic |August 28, 2014 |DAILY BEAST 

One Street Museum is dedicated to the picturesque old Kiev street called Andriyivskyy Descent, on which it is situated. The Ukrainian Face Collector Launches an Exhibition in Kiev |Nina Strochlic |August 21, 2014 |DAILY BEAST 

Later, we hiked up a picturesque trail to the Dovbush rocks, which are a sort of local Stonehenge. For Ukrainians on Holiday, the Carpathians Are the New Crimea |Vijai Maheshwari |July 14, 2014 |DAILY BEAST 

But the area is also an unexplored region of picturesque villages and surprising flavors. The Road to Cinco de Mayo |Tania Lara |May 5, 2014 |DAILY BEAST 

His social snapshots reveal the unhappy repercussions of tyranny and poverty in a picturesque Africa. Saatchi Resurrects Ancient Pangaea with Show Featuring South American and African Artists |Chloë Ashby |April 4, 2014 |DAILY BEAST 

Many adults assume that a child can look at a landscape as they look at it, taking in the whole picturesque effect. Children's Ways |James Sully 

It was a large, beautiful room, rich and picturesque in the soft, dim light which the maid had turned low. The Awakening and Selected Short Stories |Kate Chopin 

It is true that they wanted the picturesque splendour of ancient warfare. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

There is nothing picturesque in those old towns, for they were dead before they were civilized. Ancestors |Gertrude Atherton 

It lies in a large and picturesque hollow, surrounded by mountains at an elevation of 3,200 feet above the level of the sea. A Woman's Journey Round the World |Ida Pfeiffer