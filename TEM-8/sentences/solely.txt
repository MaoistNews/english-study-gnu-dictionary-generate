Both sources said Garrison will report solely to the Pentagon chief. The Pentagon is taking a major step to deal with its diversity problems |Alex Ward |February 12, 2021 |Vox 

“Pausing all aspects of game day entertainment is solely based on transforming the fan experience in connection with the rebrand,” Wright said. Former cheerleaders settle with Washington Football Team as program’s future is in doubt |Beth Reinhard |February 10, 2021 |Washington Post 

Birds were singing, and I once again wished I could recognize more birds solely by the sounds they make. When you walk a dog, your mind is as likely to wander as the dog is |John Kelly |February 10, 2021 |Washington Post 

TikTok is well aware it’s no longer a service used solely by bubbly teens doing dance challenges. TikTok takes on the mess that is misinformation |Danielle Abril |February 4, 2021 |Fortune 

Instead of relying solely on human input, they utilize marketing automation tools and adjust their strategies according to analytics data. Five ways to use VPN in digital marketing |Juta Gurinaviciute |February 1, 2021 |Search Engine Watch 

Instead, he is cruelly jailed solely for the peaceful expression of his beliefs. Behind Bars for the Holidays: 11 Political Prisoners We Want to See Free In 2015 |Movements.Org |December 25, 2014 |DAILY BEAST 

Winners are solely responsible for any and all federal, state, and local taxes and/or fees that may be incurred. The Daily Beast Company LLC The New Alphas Sweepstakes Official Rules | |December 9, 2014 |DAILY BEAST 

Winners will be solely responsible for any and all local, state, and federal taxes, and/or fees that may be incurred. The Daily Beast Company LLC The New Alphas Sweepstakes Official Rules | |December 9, 2014 |DAILY BEAST 

Half a century ago the family tree was consigned solely to paper. Why Every Home Needs a Drone This Holiday |Charlie Gilbert |December 8, 2014 |DAILY BEAST 

As a result, American and British prisoners are almost solely reliant on dangerous military efforts to free them. Did U.S. Policy Get Luke Somers Killed? |Shane Harris |December 6, 2014 |DAILY BEAST 

Solely over one man therein thou hast quite absolute control. Pearls of Thought |Maturin M. Ballou 

Fiddle-cases seem to have been used almost solely for travelling purposes. Antonio Stradivari |Horace William Petherick 

The American casualties that day, due solely to the morning skirmishes, amounted to four killed and thirty wounded. The Philippine Islands |John Foreman 

Our solicitude was now required solely for Joe, whose head was too deeply buried to be exhumed with so much facility. The Book of Anecdotes and Budget of Fun; |Various 

It is wrong to imagine that the obligation comes solely from the will of those who vow. The Ordinance of Covenanting |John Cunningham