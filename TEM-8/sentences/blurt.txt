But as soon as I see her it seems ridiculous to blurt out a speech like that the first thing. Out of the Hurly-Burly |Charles Heber Clark 

I should just blurt it out and then flee like what's-a-name—the messenger that came to Jehu. Averil |Rosa Nouchette Carey 

A man would blurt it out, and then I would know where I am at. The Copy-Cat and Other Stories |Mary E. Wilkins Freeman 

Demoralized, unstrung, they would blurt out the truth and so convict themselves. The Third Degree |Charles Klein and Arthur Hornblow 

Sometimes he thought that it would be more endurable to blurt out the truth and go into banishment. Shadows of Flames |Amelie Rives