When I started out in aesthetics skin care was referred to as “lotions and potions,” the latter making it seem magical or unreal. The skin care brand striving to make medical-grade topicals both more luxe and accessible |Rachel King |November 29, 2020 |Fortune 

Retro ballparks like the Rangers’ previous home were widely hailed as improvements — both in aesthetics and fan friendliness — over multi-purpose stadiums, many of which were built in the 1960s and 1970s. MLB’s Newest Ballpark Is A Shift Away From Retro-Era Stadiums |Travis Sawchik |July 16, 2020 |FiveThirtyEight 

Some of the pre-eminent innovators at the intersection of art and coding are based at the Aesthetics and Computation Group at MIT. Frickin’ Laser Beams Run by Eyeballs: The Next Art Revolution Is Here |Nico Hines |July 7, 2014 |DAILY BEAST 

We often talk about religion in terms of commitment and ideology, but the aesthetics and experience matter, too. Is American Christianity Becoming a Workout Cult? |Michael Schulson |April 27, 2014 |DAILY BEAST 

Think aesthetics as politics, and academic credentials as peerage. ‘Downton Abbey’ Democrats May Cost their Party the Senate |Lloyd Green |March 24, 2014 |DAILY BEAST 

It was a tense matchup that made up in suspense for what it lacked in aesthetics. Seahawks-Broncos and 7 Other Thrilling Super Bowl Matchups |Ben Jacobs |February 6, 2014 |DAILY BEAST 

Though the designers have markedly different aesthetics, both articulate how intrinsic personalization is to the process. The New Queens of Haute Couture |Sarah Moroz |January 27, 2014 |DAILY BEAST 

Culture is desirable; but the welfare of nations is based on morals rather than on aesthetics. Beacon Lights of History, Volume II |John Lord 

Many of the books necessary to a first-hand study of the subject are cited in the article Aesthetics. Encyclopaedia Britannica, 11th Edition, Volume 10, Slice 3 |Various 

Two more reflections appear completely to meet the objection that art or aesthetics is not entitled to the name of science. The Aesthetical Essays |Friedrich Schiller 

The following year, 1795, appeared his most important contribution to aesthetics, in his Aesthetical Letters. The Aesthetical Essays |Friedrich Schiller 

He held forth on the love of ornament—the cottage parlour moved him thereto—and its connection with aesthetics. Sons and Lovers |David Herbert Lawrence