So with the sovereign internet it does remain to be seen whether this is something that Russia is actually able to pull off. Podcast: How Russia’s everything company works with the Kremlin |Anthony Green |September 30, 2020 |MIT Technology Review 

Drescher’s job, she notes, has typically entailed “a healthy amount of travel around the world” for face-to-face meetings with the pension funds, sovereign wealth funds, and other investors making up Apollo’s client base. Apollo’s Stephanie Drescher on running a global investment firm from her living room |reymashayekhi |September 19, 2020 |Fortune 

We built it but they did the hacking—and they are sovereign nations. Inside NSO, Israel’s billion-dollar spyware giant |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Norway’s sovereign wealth fund, the world’s biggest, lost $21 billion in the first half of the year as a rebound in stock markets wasn’t enough to erase its record decline earlier this year. The world’s largest wealth fund has lost $21 billion so far this year |kdunn6 |August 18, 2020 |Fortune 

Jim then became a sovereign citizen, just like Timothy McVeigh, driving without registration or license plates. When Jesus Freaks Go Bad |Eugene Robinson |August 6, 2020 |Ozy 

So where does this leave the millions of Palestinians—like my relatives—who dream of self-determination and a sovereign state? In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

Paudert is sure to acknowledge that sovereign citizens have the same rights as all Americans and that they are not all dangerous. Sovereign Citizens Are America’s Top Cop-Killers |Caitlin Dickson |November 25, 2014 |DAILY BEAST 

But the group has been paying close attention to the sovereign citizens for a while. Sovereign Citizens Are America’s Top Cop-Killers |Caitlin Dickson |November 25, 2014 |DAILY BEAST 

Often, the most common opportunity for police officers to encounter a sovereign citizen is during a traffic stop. Sovereign Citizens Are America’s Top Cop-Killers |Caitlin Dickson |November 25, 2014 |DAILY BEAST 

The Reservation is sovereign Indian land, and the grizzly is a sacred animal to these tribes. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

Progress: an old term for the travelling of the sovereign to different parts of his country. Gulliver's Travels |Jonathan Swift 

The lower class were idle and lazy, and willing to serve any sovereign who appealed to them by ostentation. Napoleon's Marshals |R. P. Dunn-Pattison 

Moderate salaries prevailed, but the sovereign was worth much more then than now, while wants were fewer. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

After his death she was proclaimed sovereign empress of all the Russias, and showed herself worthy of her high fortune. The Every Day Book of History and Chronology |Joel Munsell 

He trusted that what might be done in this matter be most expedient for the service of the king our sovereign. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various