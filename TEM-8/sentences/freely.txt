There is an inherent friction in having an authoritarian government running in parallel with an open, freely accessible internet. Uganda’s election is a key moment for the splinternet in Africa |Odanga Madung |February 10, 2021 |Quartz 

Through it all, the gloves need to allow your fingers to move freely while helping to maintain a solid grip on your welding torch. Welding gloves to keep you safe and comfortable on the job |PopSci Commerce Team |February 9, 2021 |Popular-Science 

The next stage of our economy recovering is being able to walk the Earth freely. Uncertainty surrounding economy holds mortgage rates in place |Kathy Orton |February 4, 2021 |Washington Post 

The idea is that by layering data about how people use land and view animals onto existing maps of the physical landscape, conservation managers could better identify land where animals could move freely. A new mapping method could help humans and wildlife coexist |Philip Kiefer |February 4, 2021 |Popular-Science 

This means that trees cannot take up water through their roots even if it is freely available in the soil. Fish blood could hold the answer to safer de-icing solutions during snowstorms |By Monika Bleszynski/The Conversation |February 1, 2021 |Popular-Science 

And it must make sure that the platform of debate where we can freely exchange ideas is safe and sound. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

“But I could breathe freely only when the plane took off,” she told me. Russians Plot Exiled Government in Kiev |Anna Nemtsova |December 16, 2014 |DAILY BEAST 

They seem to belong to us, and then they freely go—behavior very uncharacteristic of a shadow or a shoe. Owning Up to Possession’s Downside |Samantha Harvey |December 14, 2014 |DAILY BEAST 

She freely admits that she was using both to self-medicate after she lost her insurance. States Slap Pregnant Women With Harsher Jail Sentences |Emily Shire |December 12, 2014 |DAILY BEAST 

The very nature of going “viral” is that it requires the content to be instantly, freely shareable. Death of the Author by Viral Infection: In Defense of Taylor Swift, Digital Doomsayer |Arthur Chu |December 3, 2014 |DAILY BEAST 

That which is called nasality is caused by the failure of the tone to reach freely the anterior cavities of the nares. Expressive Voice Culture |Jessie Eldridge Southwick 

And although we gabbled freely enough, MacRae avoided all mention of the persons of whom I most wished to hear. Raw Gold |Bertrand W. Sinclair 

In such conditions many kinds which do not flourish very freely in the open garden, grow into handsome specimens. How to Know the Ferns |S. Leonard Bastin 

Captains Spotstroke and Pool were equally careful; the rest of those present drank freely. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Civilisation is built up of the "heresies" of men who thought freely and spoke bravely. God and my Neighbour |Robert Blatchford