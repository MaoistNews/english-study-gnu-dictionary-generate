Rather, they house enough of the raw ingredients to create water when heated. Earth’s building blocks may have had far more water than previously thought |Christopher Crockett |August 27, 2020 |Science News 

Some of the elements on a printed circuit board, essentially the brain of a computer, are raw materials whose supply is at risk. We’re Using Microbes to Clean Up Toxic Electronic Waste. Here’s How |Sebastien Farnaud |August 20, 2020 |Singularity Hub 

It was less a joke than a vote of confidence that in an age of extreme building — the Eiffel Tower, the Brooklyn Bridge and more — anything was possible if you just had enough raw material and the chutzpah to defy known limitations. The Story Behind the Eiffel Tower’s Forgotten Competitors |Fiona Zublin |August 10, 2020 |Ozy 

It’s the same reaction that powers all stars, and trying to corral that kind of raw power and turn it into something we can use effectively is a challenge scientists have been struggling with for decades. Construction of the World’s Biggest Nuclear Fusion Plant Just Started in France |Edd Gent |August 3, 2020 |Singularity Hub 

This why testimonial link building is seen as a great way to harness this raw strength of positive experiences by customers. Testimonial link building: Using real experiences for success |Rameez Ghayas Usmani |July 15, 2020 |Search Engine Watch 

Raw eel seemed to be popular during and after the Middle Ages. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

The script would be used as more than just raw material, but would need to be fudged. Michael C. Hall on Going Drag for ‘Hedwig and the Angry Inch’ and Exorcising ‘Dexter’ |Marlow Stern |December 4, 2014 |DAILY BEAST 

The raw materials— tin, tantalum, tungsten and gold—were dubbed “conflict minerals.” Aaron Rodgers Takes Aim at Congo’s ‘Blood Minerals’ War |John Prendergast |December 3, 2014 |DAILY BEAST 

But there was a lot of raw violence in the humor of the time. The Magazine That Made—and Unmade—Politicians |Anthony Haden-Guest |November 2, 2014 |DAILY BEAST 

But the raw believability of, say, Magnum photography has been done for. The Magazine That Made—and Unmade—Politicians |Anthony Haden-Guest |November 2, 2014 |DAILY BEAST 

It makes out of the savage raw material which is our basal mental stuff, a citizen. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

It had come on to rain, and the raw dampness mingled itself with the dusky uproar of the Strand. Confidence |Henry James 

The evening was cold and raw and so dark that it was almost impossible to distinguish people on the badly lighted little platform. Uncanny Tales |Various 

This problem was solved by a native coming along driving a raw-boned horse before a rickety wagon. The Courier of the Ozarks |Byron A. Dunn 

The comparison of the cost of production, therefore, with the value of the raw material, shows a very large margin of profit. Asbestos |Robert H. Jones