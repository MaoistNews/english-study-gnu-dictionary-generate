An economist might say, “Well, we should be using fiscal instruments — taxes, transfers — to redistribute.” Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

If nothing is done to reverse this massive wealth transfer, we risk losing our independent businesses for good. How we can save small business from coronavirus-induced extinction |matthewheimer |September 10, 2020 |Fortune 

Bayern Munich and Spain midfielder Thiago Alcántara has been linked with a move to Merseyside, while Georginio Wijnaldum has been linked with a move away from Liverpool to Barcelona, but neither transfer has come to pass yet. Will Liverpool Run Away With The Premier League Again, Or Can Manchester City Take The Title Back? |Terrence Doyle |September 10, 2020 |FiveThirtyEight 

The same problem applies whatever legal mechanism companies are using for those transfers. Time is running out for Big Tech’s monetization of Europeans’ personal data |David Meyer |September 10, 2020 |Fortune 

Electric vehicles wouldn’t be possible without cobalt, a mineral used in rechargeable batteries to store and transfer power. Can Tesla help solve one of the thorniest ethical problems with electric vehicles? |Tim McDonnell |September 10, 2020 |Quartz 

Parents who want to transfer custody of a child to someone other than a relative must seek permission from a judge. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 

“I ran for my life,” said Tenayo, who is a home attendant for an autistic resident, but wants to transfer because of the crime. Anger at The Cop Killer - And The Police |M.L. Nestel |December 21, 2014 |DAILY BEAST 

He suggested I needed mental help, and offered to help me transfer to another college. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

All the junkies try to transfer to them, for the abundance of morphine. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

Jasmin helps her transfer in and out of her wheelchair, get dressed, and bathe. Care Providers Fight for $15 and a Union |Jasmin Almodovar, Shirley Thompson |December 5, 2014 |DAILY BEAST 

He, Bastien-Lepage, painter of the soil, found himself unable to transfer to canvas the enchantment of that land of fairy tale! Bastien Lepage |Fr. Crastre 

Giles; aluminium and its bronze in 1864; the transfer process in 1856 by Tearne and Richmond. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

It is immaterial to whom the transfer is made if the purpose be to prefer one creditor to another. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Another act of bankruptcy is to convey, transfer, conceal or remove property with the intention to defraud creditors. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

As additional methods facilitating in some cases the transfer of stops must be named the "double touch" and the "pizzicato touch." The Recent Revolution in Organ Building |George Laing Miller