A policewoman was shot dead this morning while law enforcement searched for the Charlie Lebdo killers. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

Acting as a moral policewoman, she busted the couples on camera—on a Pakistani TV channel called Samaa. When the Killer Is a Mother: The Tragedy of Three Murdered Teens in Canada |Asra Q. Nomani |February 1, 2012 |DAILY BEAST 

One cell is suspected of killing 10 people since 2000, nine immigrants and a policewoman. Extending the German Ban on ‘Mein Kampf’ Does More Harm Than Good |Andrew Nagorski |January 29, 2012 |DAILY BEAST 

Murdoch praised the courage and poise of a young policewoman who arrived first on the scene with a male colleague. Australia’s Collar-Bomb Hoax |David Leser |August 4, 2011 |DAILY BEAST 

"I was even with my husband one time when a policewoman gave me a warning about bad hijab," she says. Iran’s Hardline Fashion Police |Babak Dehghanpisheh |June 24, 2011 |DAILY BEAST 

Elisaveta felt uncomfortably unclean after she had passed through the policewoman's paws. The Created Legend |Feodor Sologub 

Young man, if this were a performance, you would be dealt with by our aesthetic policewoman. The Harlequinade |Dion Clayton Calthrop 

She pulled up the anchor, but this time the policewoman did the rowing, and she rowed well. Missing at Marshlands |Cleo Garis 

Policewoman McLine is the only one that I could say that was definitely there. Warren Commission (12 of 26): Hearings Vol. XII (of 15) |The President's Commission on the Assassination of President Kennedy 

The Policewoman reported that she was given every courtesy and assistance by both police and railway officials. Women and War Work |Helen Fraser