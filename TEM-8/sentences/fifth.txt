A fifth bill to help the FTC and its new chairThe fifth bill in the House anti-tech antitrust package would increase filing fees for mergers. Cheat Sheet: How new antitrust bills could force more data access from Facebook and Google (and stop them from favoring their own services) |Kate Kaye |June 17, 2021 |Digiday 

It was month 13 of virtual learning, and Alejandro Diasgranados once again had to keep his 19 fifth-graders engaged on their computers. ‘My students are brilliant:’ Meet the D.C. teacher of the year and one of the four finalists for the top national honor |Perry Stein |May 6, 2021 |Washington Post 

Soon a fifth-grader joined, kicking a soccer ball as it skipped across the field. ‘Equity hubs’ give families struggling financially a chance at pandemic pods |Donna St. George |December 25, 2020 |Washington Post 

She could not have afforded standard child-care costs — about $1,300 a month per child — and she found that her second-grader and fifth-grader liked the staff, along with the structure and schedule, she said. ‘Equity hubs’ give families struggling financially a chance at pandemic pods |Donna St. George |December 25, 2020 |Washington Post 

In China, it’s quite normal for us to deploy 5G, not in big cell towers but on lampposts…So every second or third or fifth lamppost has 5G solutions built on them, and that’s because the government understands that you need to share infrastructure. ‘Work to your strength’: Huawei’s CTO weighs in on U.S. efforts to build a Huawei alternative |Veta Chan |August 20, 2020 |Fortune 

But on Thursday Boxer triggered a Golden State political earthquake, announcing that she would not seek a fifth term in 2016. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

He branded it a fifth-column invasion into popular culture, normalizing radical, even communist ambitions. Glenn Beck Is Now Selling Hipster Clothes. Really. |Ana Marie Cox |December 20, 2014 |DAILY BEAST 

Insult to injury, its $43 million gross was less than one-fifth of what Ted took in. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

As late as the fifth century, powerful aristocratic women took charge of the commemoration of the dead in Rome. First Anglican Woman Bishop A Return to Christian Roots |Candida Moss |December 18, 2014 |DAILY BEAST 

Each two-hour episode will build upon itself to tell a story that takes place between the third and fifth seasons of the show. ‘Game of Thrones’ Interactive FanFiction: Whoops, My Friend Was Speared in the Throat |Alec Kubas-Meyer |December 13, 2014 |DAILY BEAST 

These Rules (leaving out the Tenor) serves for five bells; and leaving out the fifth and Tenor, they serve for four bells. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

After about the forty-fifth year it becomes gradually less; after seventy-five years it is about one-half the amount given. A Manual of Clinical Diagnosis |James Campbell Todd 

Ordinarily the diazo appears a little earlier than the Widal reaction—about the fourth or fifth day—but it may be delayed. A Manual of Clinical Diagnosis |James Campbell Todd 

In the next two days he re-wrote the twenty thousand, and on the fifth day he tore it into shreds and threw it to the winds. The Homesteader |Oscar Micheaux 

A fifth by the sheer hazard of a lucky "deal" acquires a fortune without work at all. The Unsolved Riddle of Social Justice |Stephen Leacock