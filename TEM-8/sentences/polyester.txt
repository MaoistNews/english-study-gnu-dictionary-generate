Avoid polyester, rayon, and acrylic fabrics, and instead, turn your attention to the benefits of cotton. Best weighted blanket: Sleep like a baby with our comfy bedding picks |PopSci Commerce Team |February 12, 2021 |Popular-Science 

The leather is made from Swedish elkskin and the gloves are lined in a soft polyester knit. Best winter gloves: Our picks for touch screen gloves, ski gloves, and more |PopSci Commerce Team |February 2, 2021 |Popular-Science 

Note, however, that not all polyester fleece linings are created equal. The Best Fleece-Lined Pants for the Outdoors |Wes Siler |January 15, 2021 |Outside Online 

It’s made out of soft, lightweight polyester and features classic Zelda characters and and game icons. Stylish and sophisticated scarves that everyone will love |PopSci Commerce Team |January 13, 2021 |Popular-Science 

Some pillows are advertised as being made of synthetic down, which means they’re filled with materials such as polyester. The best pillow: Sleep better in any position |Carsen Joenk |December 11, 2020 |Popular-Science 

The Centers for Disease Control note that it is used to “make polyester compounds, and as a base for deicing solutions.” Europeans Recall Fireball Whiskey Over a Sweetener Also Used in Antifreeze |Tim Mak |October 28, 2014 |DAILY BEAST 

He re-invents himself: he goes from ill-fitting polyester cotton blends to silk shirts and stylish leather jackets. The ‘American Hustle’ Style Guide |Erin Cunningham |February 14, 2014 |DAILY BEAST 

It was a cashmere year for movies, so why are we cheering polyester? ‘American Hustle’ Is Overrated |Kevin Fallon |January 28, 2014 |DAILY BEAST 

She was 17 and dressed as a flight attendant—think cheap polyester and way too little fabric. The 15 Most Cringeworthy Bits From Leandra Medine’s New Memoir |Emilia Petrarca |September 10, 2013 |DAILY BEAST 

The translucent green polyester has been stretched into door handles, moldings, and even a telephone. The Best Things to See at Frieze Art Fair NY 2013 |Isabel Wilkinson |May 10, 2013 |DAILY BEAST 

I said, "Well, adipic acid is a known polyester ingredient." The Professional Approach |Charles Leonard Harness