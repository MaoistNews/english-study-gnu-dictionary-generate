The soft-shell backing blocks chilly winds, silicone grips improve control of my handlebars, and they work well with touchscreens. The Gear Our Editors Loved in January |The Editors |February 3, 2021 |Outside Online 

The S-200 is equipped with three independent braking systems — a regenerative rear brake, front and rear drum brakes — and turn signals located on handlebars and near the rear wheel. Spin bets its scooter future on 3 wheels and remote-control tech |Kirsten Korosec |January 27, 2021 |TechCrunch 

Other factors that may increase or decrease the size of an exercise bike include handlebars and onboard screens, all of which factor into whether or not a particular model is appropriate for your space. Best exercise bike: Get your miles in without leaving the house |PopSci Commerce Team |December 19, 2020 |Popular-Science 

I’ve attached the Zeus to just about every GoPro mount I own, including my helmet and handlebar attachments. The Gear Our Editors Loved in November |The Editors |December 4, 2020 |Outside Online 

Tilt down the handlebars for an aggressive forward-leaning posture. The 100 greatest innovations of 2020 |Popular Science Staff |December 2, 2020 |Popular-Science 

Peter pushed the carriage aimlessly about for a little while, never letting go of the handlebar. The Boy Grew Older |Heywood Broun 

The hand car did run loggily at first; but with four hardy Scouts on each handlebar, it slowly gained headway. The Boy Scouts of Lakeville High |Leslie W. Quirk 

It could be operated by a foot lever on the right side of the machine and also by a grip lever in the left handlebar. Bert Wilson's Twin Cylinder Racer |J. W. Duffield 

Not to be outdone I twisted Lizzie's right handlebar grip as far as it would go, and like a bolt from the blue we darted ahead. Across America by Motor-cycle |C. K. Shepherd 

The handlebar was wrenched out of my hands and I was thrown with great force over it and on to the bank at the side. Across America by Motor-cycle |C. K. Shepherd