Eden, the office management platform that has raised nearly $70 million since inception, is today making a tweak to its nomenclature to reflect its push into SaaS tools. Eden office management platform rebrands to Eden Workplace |Jordan Crook |January 12, 2021 |TechCrunch 

By one count, there have been more than 25 different threshold definitions published in the literature using various criteria and nomenclatures. What Does Your “Threshold” Really Mean? |Alex Hutchinson |December 2, 2020 |Outside Online 

We can use language in the nomenclature around targets and still hit a goal. Should corporate diversity aim for ‘targets’ or ‘quotas’? |Aric Jenkins |September 29, 2020 |Fortune 

There’s a whole nomenclature around targets directly correlated to what we deliver and produce on a day-to-day basis. Should corporate diversity aim for ‘targets’ or ‘quotas’? |Aric Jenkins |September 29, 2020 |Fortune 

It should be noted that a penis might be placed in all three, but what happens once it’s placed is what determines the nomenclature. Sex Is Dead: Discuss |Eugene Robinson |August 10, 2020 |Ozy 

So Rhoxane could be the “queen” that local nomenclature has linked to the site. Is This Alexander the Great’s Tomb—or His Wife’s? |James Romm |December 12, 2014 |DAILY BEAST 

The world had become a different place and I found myself unequipped to interpret a whole new landmine-strewn nomenclature. What Should I Call the Man I Love? |Dushka Zapata |November 18, 2014 |DAILY BEAST 

Of course, the jokes, the nomenclature, the people doing the creepy but supposed to be funny stares, are almost always men. Women, It's Time to Reclaim Our Breasts |Emily Shire |September 9, 2014 |DAILY BEAST 

The term itself contains misleading nomenclature—the individual does not have to be a surgeon. Meet Obama’s Controversial Surgeon General Nominee |Soumitra R. Eachempati, MD |February 14, 2014 |DAILY BEAST 

But because they wanted something friendlier, they came up with the nomenclature of ‘screen name.’ We're All Still Secretly Using Our 1990s AOL Screen Names. Why? |Andrew Romano |January 23, 2014 |DAILY BEAST 

The nomenclature by position is an essential part of the scheme of Keys proposed by Ptolemy. The Modes of Ancient Greek Music |David Binning Monro 

Among the apt terms which he applied in all his nomenclature only one, Cape Tribulation, bears witness to the risks which he ran. Yachting Vol. 2 |Various. 

Similar specializations are of frequent occurrence in the history even of scientific nomenclature. A System of Logic: Ratiocinative and Inductive |John Stuart Mill 

On this principle is founded the admirable binary nomenclature of botany and zoology. A System of Logic: Ratiocinative and Inductive |John Stuart Mill 

This was done in the mineralogical nomenclature proposed by Professor Mohs. A System of Logic: Ratiocinative and Inductive |John Stuart Mill