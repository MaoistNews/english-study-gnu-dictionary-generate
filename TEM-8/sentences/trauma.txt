I’m sure there was some level of trauma, but the weekend, I testified on a Friday, Monday was a holiday, Columbus Day, the following Monday. Can Anita Hill Forgive Joe Biden … and Work With Him? |Pallabi Munsi |September 14, 2020 |Ozy 

“The trauma in skeletal males is often considered evidence of participation in warfare or violence,” Broehl says. Women like Mulan didn’t need to go to war in disguise |Bethany Brookshire |September 4, 2020 |Science News For Students 

The adopted kids appeared to have gotten over the impacts of their earlier trauma. Puberty may reboot the brain and behaviors |Esther Landhuis |August 27, 2020 |Science News For Students 

Research shows that these stress responses develop abnormally in kids who face trauma early in life — such as abuse or neglect. Explainer: What is puberty? |Esther Landhuis |August 27, 2020 |Science News For Students 

Nayirah recalled the trauma of watching Iraqi troops storm into Al Adan Hospital in Kuwait City, where she volunteered as a nurse. The Great Lie of the First Gulf War |Mat Nashed |August 17, 2020 |Ozy 

For them, the trauma of assault can be compounded by a lack of institutional support, and even disciplinary action. Jameis Winston Cleared of Rape Like Every Other College Sports Star |Robert Silverman |December 22, 2014 |DAILY BEAST 

Question them, and you are colluding in exacerbating the awful effects of their trauma. What the U-VA Rape Case Tells Us About a Victim Culture Gone Mad |Lizzie Crocker |December 6, 2014 |DAILY BEAST 

Does he give in to the trauma or does he embrace all of the lessons he has learned? The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

The 54-year-old trauma doctor and father of three is suffering from heart disease. In the Battle for Kobani, ISIS Falls Back. But for How Long? |Jamie Dettmer |October 20, 2014 |DAILY BEAST 

It doesn't make you a better person because you endured the indignity and trauma of it. On Her Own Terms: Why Brittany Maynard Has Chosen to Die |Gene Robinson |October 12, 2014 |DAILY BEAST 

And the papers of Braulinski of the old University of Warsaw on the fear trauma which he termed a birthmark of civilization. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Originally, he endeavoured to reawaken the memory of the sexual trauma by means of the induction of profound hypnosis. The Sexual Life of the Child |Albert Moll 

This perception was enough to make me sceptical about the whole trauma-theory. Collected Papers on Analytical Psychology |C. G. Jung 

Not infrequently, the result of a trauma, division of the tendo Achillis occurs. Lameness of the Horse |John Victor Lacroix 

The ideal operation, therefore, is to make an artificial pupil with the least amount of trauma to the ciliary body. A System of Operative Surgery, Volume IV (of 4) |Various