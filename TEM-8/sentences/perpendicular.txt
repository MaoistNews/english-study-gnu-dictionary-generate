Put your feet on the socks, and start in a standard push-up position, with your hands on the yoga mat, your body perpendicular to the mat. Jessie Diggins's Killer 8-Minute Core Workout |Hayden Carpenter |October 30, 2020 |Outside Online 

To start, stand on the hill perpendicular to the slope, with your uphill edges dug in. 6 Easy Ways to Be a Better Skier |Heather Hansman |October 1, 2020 |Outside Online 

Even if you’ve mastered fall-line skiing elsewhere, when you get into steep terrain, your instinct might still be to move sideways, swinging your whole body around with each turn and finishing with your skis perpendicular to the slope. 6 Easy Ways to Be a Better Skier |Heather Hansman |October 1, 2020 |Outside Online 

Aggressively working 550 cord perpendicular across a zip tie will weaken the plastic until it eventually breaks. This essential survival tool can save your life 10 different ways |By Tim MacWelch/Outdoor Life |September 15, 2020 |Popular-Science 

Let the line from the origin to A and the origin to B, 2 perpendicular radii of a circle centered at the origin. America’s Math Curriculum Doesn’t Add Up (Ep. 391) |Steven D. Levitt |October 3, 2019 |Freakonomics 

In the parish churches, many of which are of great interest, the predominant styles are Decorated and Perpendicular. Encyclopaedia Britannica, 11th Edition, Volume 3, Slice 4 |Various 

Among the Perpendicular additions to the church last named may be noted a very beautiful oaken rood-screen. Encyclopaedia Britannica, 11th Edition, Volume 3, Slice 4 |Various 

The water-piston is 10 inches in diameter, drawing and forcing 35 feet perpendicular, equal beam. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Le Bouton was dominated by a perpendicular rock two thousand feet high. Napoleon's Marshals |R. P. Dunn-Pattison 

They were at the foot of one of the narrow almost perpendicular blocks that rose between Pine and California streets. Ancestors |Gertrude Atherton