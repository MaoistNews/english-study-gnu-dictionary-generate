I’ll never forget the one summer I went sightseeing in a dress on vacation. This Anti-Chafe Stick Is the Product I Didn’t Know I Needed |Kristen Garaffo |July 30, 2021 |The Daily Beast 

For Randy Sullivan, that meant another day — his fifth in a row — of flying sightseeing tours and charters. In Alaska, Commercial Aviation Is a Lifeline. The State Is Also Home to a Growing Share of the Country’s Deadly Crashes. |by Zoë Sobel, KUCB, and Agnel Philip, ProPublica |June 16, 2021 |ProPublica 

Osaka can turn to Instagram and TikTok to push the brands that help make her a fortune but also to show her working out with her boyfriend or sightseeing in Rome. Athletes are human, not cyborgs, and Naomi Osaka’s exit should make that clear |Barry Svrluga |May 31, 2021 |Washington Post 

It’s important to feel safe, especially when it’s time to rest after a packed day of sightseeing or adventuring. Portable door locks can give travelers extra comfort and protection. Here’s how to choose one. |Gina Rich |May 20, 2021 |Washington Post 

The crew will be doing more than sightseeing in their three days aloft. Meet the Inspiration4 Team, the World's First Non-Professional Astronaut Space Crew |Jeffrey Kluger |April 23, 2021 |Time 

Nearby, an Italian businessman worked a deal, and a visiting couple unwound in the romantic setting after a day of sightseeing. The Classic Hotel Bar Checks Back In |Kayleigh Kulp |July 26, 2014 |DAILY BEAST 

They passed a sightseeing bus from which tourists were taking photos of an unexpected sight. She Is Trayvon Martin |Michael Daly |July 15, 2013 |DAILY BEAST 

At college, when I was sightseeing among white guys and we all pointed out the females we thought looked good. I Want a Bigger Butt |Stanley Crouch |December 1, 2009 |DAILY BEAST 

He was exceedingly kind and facilitated our sightseeing in the great city during our stay. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

There were warm robes and blankets in the Belding auto and in the sightseeing machine that Mr. Purcell had sent. The Girls of Central High on the Stage |Gertrude W. Morrison 

I had now reached my fourth objective and felt the importance of more haste and less leisure and sightseeing. Ocean to Ocean on Horseback |Willard Glazier 

Ralph was weary of sightseeing and seldom took an interest in viewing things alone. The Camp Fire Girls at the End of the Trail |Margaret Vandercook 

And thus in an amiable frame of mind the party returned to their boarding-house, pleased with their sightseeing. Amy in Acadia |Helen Leah Reed