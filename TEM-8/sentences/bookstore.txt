Eight of these 10 selections will already be on bookstore shelves by the time you read this, which means you’ll be ready to spend the entire month reading. 10 books to read in February |Bethanne Patrick |February 2, 2021 |Washington Post 

We’d go to a special bookstore for science and philosophy books. A Prodigy Who Cracked Open the Cosmos |Claudia Dreifus |January 12, 2021 |Quanta Magazine 

Shortly after I came to Washington in the 1970s, I wandered into a bookstore and happened upon a shopworn copy of “The Letters of Oscar Wilde,” edited by Rupert Hart-Davis. How did all these books get into my house? There’s a method to what seems like madness. |Michael Dirda |January 6, 2021 |Washington Post 

Busboys and Poets, a combined restaurant, bar, bookstore, and coffee shop with several locations around the city, offers several options for LGBTQ-specific merchandise. Shop local this season and make a difference |Parker Purifoy |December 17, 2020 |Washington Blade 

Just call up a local bookstore near your giftee’s address and have them put aside a copy of one of the books below. The 2020 Sweat Science Holiday Book List |Alex Hutchinson |December 16, 2020 |Outside Online 

What it did not resemble was any other bookstore in the nation. The Bookstore That Bewitched Mick Jagger, John Lennon, and Greta Garbo |Felice Picano |December 16, 2014 |DAILY BEAST 

The bookstore was opened as a way of presenting Italian books and culture to Manhattanites. The Bookstore That Bewitched Mick Jagger, John Lennon, and Greta Garbo |Felice Picano |December 16, 2014 |DAILY BEAST 

The Rizzoli in New York City was no ordinary bookstore in its seventies heyday. The Bookstore That Bewitched Mick Jagger, John Lennon, and Greta Garbo |Felice Picano |December 16, 2014 |DAILY BEAST 

Anti-Korean publications are sold at every bookstore in Japan. Japan’s Nasty Nazi-ish Elections |Jake Adelstein |December 12, 2014 |DAILY BEAST 

But now I see them as the giant vampire squid, the enemy of the independent bookstore. Amazon Won’t Kill the Indie Bookstore |Bill Morris |July 30, 2014 |DAILY BEAST 

Mr. Johnson, the bookstore man, sold more Bibles the next month after the revival than he had in the whole previous year. Around Old Bethany |Robert Lee Berry 

Tempted into Gaine's bookstore by the display of volumes, he chanced upon a friend who called him by name. Literary New York |Charles Hemstreet 

Bakka is the oldest science fiction bookstore in the world, and it made me the mutant I am today. Little Brother |Cory Doctorow 

Powell's is the largest bookstore in the world, an endless, multi-storey universe of papery smells and towering shelves. Little Brother |Cory Doctorow 

They have tons of events for kids and one of the most inviting atmospheres I've ever experienced at a bookstore. Little Brother |Cory Doctorow