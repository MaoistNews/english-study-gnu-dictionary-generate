If you do, you can skip the initial 20-minute simmer and add the kale when you add the broth. This vegan soup gathers greens, grains and peanut butter in one spicy bowl |Aaron Hutcherson |January 7, 2021 |Washington Post 

Add enough water to cover and bring to a boil over high heat, then lower the heat to a simmer and cook, skimming off and discarding any foam that rises to the top, until the chicken breasts are firm to the touch, about 25 minutes. An aromatic chicken soup to restore the body and revive the spirit |Olga Massov |January 7, 2021 |Washington Post 

Bring the soup to a brisk simmer and cook for 5 minutes, then turn off the heat. This briny oyster soup brings the smells and tastes of Louisiana’s coast to your table |Dayana Sarkisova |December 17, 2020 |Washington Post 

When the spices are fragrant, add the water and bring the mixture to a simmer. 4 Hot Cocktail Recipes to Save Your Winter Social Life |AC Shilton |December 17, 2020 |Outside Online 

Nine months into the worst pandemic the world has seen in 90 years, deep in a recession that shows no signs of ebbing, we are witnessing multiple hot spots on the simmer simultaneously. Is War Next for 2020? |Charu Kasturi |September 21, 2020 |Ozy 

Add the butter, chicken stock, salt, bay leaf, thyme, and tarragon and bring to a simmer. Daniel Boulud Reveals His 4 Favorite Recipes From His New Cookbook |Daniel Boulud |October 15, 2013 |DAILY BEAST 

Add the vinegar, bring to a simmer, then stir in the chicken stock. Daniel Boulud Reveals His 4 Favorite Recipes From His New Cookbook |Daniel Boulud |October 15, 2013 |DAILY BEAST 

In a small saucepan, combine the yogurt, cream, and vadouvan spice and bring to a simmer. Daniel Boulud Reveals His 4 Favorite Recipes From His New Cookbook |Daniel Boulud |October 15, 2013 |DAILY BEAST 

Add the remaining tomatoes, cover, and simmer for another 20 minutes, or until the chicken is cooked through. Daniel Boulud Reveals His 4 Favorite Recipes From His New Cookbook |Daniel Boulud |October 15, 2013 |DAILY BEAST 

Bring to a simmer, then reduce the heat to just below a simmer and cook until the shallots are very tender, about 20 minutes. Daniel Boulud Reveals His 4 Favorite Recipes From His New Cookbook |Daniel Boulud |October 15, 2013 |DAILY BEAST 

Well, we'll simmer down when the turn comes, and though I'm piling up dollars, I'll be thankful. Winston of the Prairie |Harold Bindloss 

Allow the mixture to simmer in a hot oven and serve with fresh asparagus cut before breakfast. Punch, or the London Charivari, Vol. 152, May 16, 1917. |Various 

Keep it boiling till the chicken is done, which would be in about an hour's time after it begins to simmer. Dressed Game and Poultry la Mode |Harriet A. de Salis 

Cut up the duck or teal, and put it into the sauce, and let it simmer for a long time—the duck having been previously roasted. Dressed Game and Poultry la Mode |Harriet A. de Salis 

Reduce and simmer till it is thick, then pass through a sieve, and take it off the fire before it boils. Dressed Game and Poultry la Mode |Harriet A. de Salis