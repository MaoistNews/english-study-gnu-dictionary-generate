Some of my teammates sat in the nearby parking lot running their bike chains through rags to clean the grease. How Biking Across America Formed an Unlikely Friendship |Raffi Joe Wartanian |October 8, 2020 |Outside Online 

Levels of ammonia, oils and greases, phosphorus, nitrates, coliform bacteria and several substances associated with the presence of wastewater exceeded limits set in both countries. Border Report: New State Law Requires an Action Plan for the Tijuana River |Maya Srikrishnan |October 5, 2020 |Voice of San Diego 

When the pot has cooled, wipe out any remaining grease with a paper towel and burn that. A Family-Friendly Camp Meal That Everyone Can Help With |Wes Siler |October 1, 2020 |Outside Online 

Again, this step requires a lot of elbow grease, so bear that in mind. Testimonial link building: Using real experiences for success |Rameez Ghayas Usmani |July 15, 2020 |Search Engine Watch 

Those individuals the doctors could not attend to dressed their wounds with generous amounts of vaseline and axle grease smeared onto their limbs. Fire on the Bay: 115 Years Ago This Month, a Deadly Explosion Rocked a Navy Ship |Randy Dotinga |July 14, 2020 |Voice of San Diego 

Preheat oven to 350°F. Grease and flour 6, 1/2-cup ramekins and set aside. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

It has faded from pixelated gray to rusted ochre, fringed on the edges with black sweat grease. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

But at least Obama does seem to genuinely loathe the sucking up required to grease the wheels. Is Big Money Politics an Overblown Evil? |David Freedlander |August 2, 2014 |DAILY BEAST 

No longer swimming in the dough, means less money for the adult industry to grease the partnership wheels. Too Hot for Google: Why The Internet Giant Is Scared of Porn |Aurora Snow |July 12, 2014 |DAILY BEAST 

The owners can grease a lot of palms with revenues like that. Al-Sisi’s Egypt Is Worse For Gays Than The Muslim Brotherhood |Bel Trew |June 28, 2014 |DAILY BEAST 

At the back, near a stand that racked a number of grease guns, he saw a second telephone fixed to the wall. Hooded Detective, Volume III No. 2, January, 1942 |Various 

From the canvas wrapping George took out and tossed on the counter a square packet wrapped in grease-paper. Dope |Sax Rohmer 

A lack of processed candles and kerosene oil left only grease and wax to be utilized for making handmade candles. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Bill looked at his watch and suggested that they eat first before they got all over grease by monkeying with the rear end. Cabin Fever |B. M. Bower 

He found much lost motion and four broken teeth, and he was grease to his eyebrows—in other words, he was happy. Cabin Fever |B. M. Bower