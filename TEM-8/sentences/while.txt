Panda-Patrice hybrid plays board games with five young girls whilst rapping “Get me broccoli/While I play Monopoly.” The Most Offensive Lyrics and WTF Moments From ‘Chinese Food’ |Culture Team |October 15, 2013 |DAILY BEAST 

I fervently believe that the government should cut down on texting-while-driving if it possibly can. Even Good Laws Sometimes Don't Work |Megan McArdle |October 31, 2012 |DAILY BEAST 

At first borrowing from an online lender was just a once-in-a-while habit that started in early 2009. The Perils of Borrowing Online |Gary Rivlin |October 12, 2011 |DAILY BEAST 

With the more abundant material now available, such an appraisal would be worth-while. A Synopsis of the American Bats of the Genus Pipistrellus |Walter W. Dalquest 

The youthful pair start beneath the smile of a blue sky, flecked with milk-while clouds merely to heighten the effect. The Petty Troubles of Married Life, Complete |Honore de Balzac 

She seemed one of the most worth‑while girls I had ever met. The Fire People |Ray Cummings 

Why, on the campus now, the really worth-while girls rave over her. Marjorie Dean College Freshman |Pauline Lester 

“It is as though the bearer had run for a paternoster-while, and then leaped the river,” Dick observed. The Works of Robert Louis Stevenson - Swanston Edition Vol. 8 (of 25) |Robert Louis Stevenson