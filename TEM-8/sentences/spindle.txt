Around that time, the muscle’s golgi tendon organs—spindles of neurons that sit on the muscle fibers—kick in and inhibit muscle contractions, allowing your muscles to relax and lengthening the stretching you can do. Why can’t we all touch our toes? |Claire Maldarelli |September 30, 2021 |Popular-Science 

Muscles contain special sensory receptor cells called muscle spindles. You can injure yourself stretching—but it’s not easy |empire |July 8, 2021 |Popular-Science 

The author, an art curator and educator who has developed curriculums on animals, speculates that hippo empathy may derive from their brains being well-stocked with spindle neurons, which are associated in many species with social emotions. Eye-opening acts of empathy in the animal kingdom |Richard Schiffman |April 9, 2021 |Washington Post 

Hyman became interested in understanding the formation of mitotic spindles during his graduate studies in the Laboratory of Molecular Biology at the University of Cambridge in the 1980s. A Newfound Source of Cellular Order in the Chemistry of Life |Viviane Callier |January 7, 2021 |Quanta Magazine 

Along the way, a succession of other rapacious characters flock to the Spindle Gallery. The Art World's Devil Wears Prada |James Reginato |May 13, 2010 |DAILY BEAST 

At the center of the action—and the guessing game of who inspired each character—is über-dealer Art Spindle (Danny Huston). The Art World's Devil Wears Prada |James Reginato |May 13, 2010 |DAILY BEAST 

No sooner had she picked up the spindle, than she pricked her hand with it, and fell swooning. The Story Of The Duchess Of Cicogne And Of Monsieur De Boulingrin |Anatole France 

Behind the bride were carried the distaff and spindle, emblems of domestic life. The Private Life of the Romans |Harold Whetstone Johnston 

A very convenient method of keeping shipping tags at hand is to slip them on a desk spindle. The Boy Mechanic, Book 2 |Various 

In the thick shade of a fig-tree, by a running spring, sat a blind woman unrolling the last gold and silver thread from a spindle. Laboulaye's Fairy Book |Various 

He lifted a spindle-legged table in the air and went on talking. Patchwork |Anna Balmer Myers