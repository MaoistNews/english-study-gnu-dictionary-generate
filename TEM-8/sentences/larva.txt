The larvae are also protected by the bark of the tree, meaning that it might need to be even colder to knock them out. Does a cold winter mean fewer bugs in the summer? |Philip Kiefer |February 25, 2021 |Popular-Science 

Meanwhile, skinks — which made up 28 percent of the host animals captured — had 92 percent of the larvae and 98 percent of the nymphs. The animals that ticks bite in the U.S. South can impact Lyme disease spread |Aimee Cunningham |February 5, 2021 |Science News 

All larvae have mouthparts, though some can be very simple in structure, and are adapted for chewing or sucking at foods that range from plants to flesh. Evolution made mosquitos into stealthy, sensitive vampires |Erica McAlister |October 15, 2020 |Popular-Science 

This is very uncommon for flies, where the adults and larvae often live in very different environments and feed on very different food. Evolution made mosquitos into stealthy, sensitive vampires |Erica McAlister |October 15, 2020 |Popular-Science 

If so, any impacts might have had to wait until those larvae grew into adults. Pesticides can have long-term impact on bumblebee learning |Alison Pearce Stevens |May 18, 2020 |Science News For Students 

When summer comes, adult beetles attack and larva feed in the cambium layer, girdling the trees and sealing their doom. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

The entire larva is black and the segments of the body possess numerous tubercles bearing setae. Journal of Entomology and Zoology, March 1917 |Various 

As soon as it hatches the larva attacks the cricket in the belly at the chosen spot where the egg has been layed. The Natural Philosophy of Love |Remy de Gourmont 

The larva of the hemerolicus feeds also on the aphides, and deposits its eggs on the leaves of such plants as are beset with them. The Book of Curiosities |I. Platts 

What a shelter for the larva of this Pompilus: the warm retreat and downy hammock of the Segestria! More Hunting Wasps |J. Henri Fabre 

No doubt the food for her family, the larva of which I possess the empty skin, now an unrecognizable shred. More Hunting Wasps |J. Henri Fabre