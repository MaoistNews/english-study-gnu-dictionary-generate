Time limits were placed on investigations, the FBI in Washington had to approve informers, and the attorney general had to approve mail interceptions and “other invasive investigative methods,” Medsger wrote. The FBI break-in that exposed J. Edgar Hoover’s misdeeds to be honored with historical marker |Tom Jackman |September 1, 2021 |Washington Post 

The witnesses against him may include Vincente Zambada, brother of the informer-hating Serafin. Is Mexico's Kim Kardashian-Lookalike Assassin for Real? |Michael Daly |June 10, 2014 |DAILY BEAST 

Serafin posted numerous identifiable photos of himself on Facebook, as well as pictures of a disemboweled informer. Is Mexico's Kim Kardashian-Lookalike Assassin for Real? |Michael Daly |June 10, 2014 |DAILY BEAST 

An inquiry headed by the Police Ombudsman, a sort of referee figure, came down against the informer allegation. Sinn Fein Boss Gerry Adams Wanted This Murder Bust |Ed Moloney |May 1, 2014 |DAILY BEAST 

Other rabbis denounced the open condemnation of community pedophiles, labeling Rosenberg an “informer” against the Jewish people. Rebel Rabbi Exposes Child Molesters |Moral Courage |January 27, 2014 |DAILY BEAST 

He started as a KGB informer in the 1930s in London, while working as a London Times correspondent. Second ‘Underwear Bomber,’ Kim Philby, and Other Notorious Double Agents |The Daily Beast |May 10, 2012 |DAILY BEAST 

Dale, an indefatigable informer, was consumed by vermin, and died a miserable spectacle. Fox's Book of Martyrs |John Foxe 

Kurt then suddenly understood that his impudent small sister had probably been the informer and he did not know what to answer. Maezli |Johanna Spyri 

The informer, blank-faced and stern, noted the crime and informed the police. The Status Civilization |Robert Sheckley 

One morning the informer was found by the jailer hanging to the bars of his window. Chambers's Journal of Popular Literature, Science, and Art |Various 

How comes it that he is not produced here to tell your Lordships who was his informer, and what he knows of the transaction? The Works of the Right Honourable Edmund Burke, Vol. XI. (of 12) |Edmund Burke