The rest of the family sometimes balked at their squabbles, which Farr said drove their relatives “justifiably nuts.” Obituary for Kansas covid-19 victim slams anti-maskers who ‘refuse to wear a piece of cloth on their face to protect one another’ |Katie Shepherd |December 4, 2020 |Washington Post 

Shaver’s songs evoked dusty back roads, rural isolation, bar fights, knock-down drag-out marital squabbles and money lost at poker games, as well as the near-destitution of his youth and his own lofty ambitions. Billy Joe Shaver, singer-songwriter who inspired outlaw country, dies at 81 |Terence McArdle |October 29, 2020 |Washington Post 

“The meeting devolved into a partisan squabble,” The Post’s Greg Miller, Ellen Nakashima and Adam Entous reported the following year. Four years ago today, the U.S. saw the wildest hour in presidential election history |Philip Bump |October 7, 2020 |Washington Post 

Bees did, we presume, during a biochemical squabble with viruses in a damp corner of their shared history. The Fungal Evangelist Who Would Save the Bees - Issue 90: Something Green |Merlin Sheldrake |September 23, 2020 |Nautilus 

The squabble eventually devolved into a barely choreographed slapstick fight where Colbert, O’Brien, and Jon Stewart seemed to be locked in a battle for the ages. One Good Thing: Stephen Colbert is looser, funnier, and angrier in quarantine |Emily VanDerWerff |September 4, 2020 |Vox 

The squabble was also immortalized in this incredibly awkward family portrait. Solange Smacks Jay Z, Legolas Slaps Bieber, and the Biggest Celebrity Feuds of the Year |Amy Zimmerman |December 24, 2014 |DAILY BEAST 

At the time, the renamed corner caused a slight international squabble and demands for its removal. The Nigerian Women Who Fight for Democracy |Nina Strochlic |October 1, 2014 |DAILY BEAST 

The crowd that accumulated to watch the squabble reportedly applauded and cheered as Bieber fled the scene. An Unlikely Hero Blooms in Ibiza: Orlando Bloom Sort of Punches Justin Bieber |Amy Zimmerman |July 30, 2014 |DAILY BEAST 

But this scholarly squabble quickly grew ugly with the discovery of Kennewick Man in 1996. Incontrovertible Evidence Proves the First Americans Came From Asia |Doug Peacock |March 27, 2014 |DAILY BEAST 

Refaeli got involved in the squabble herself with a pair of near-identical tweets written in Hebrew. Bar Refaeli In Trouble With Israeli Army |Claire Stern |March 21, 2013 |DAILY BEAST 

We fix it up and agree to try over ag'in, and then, fust thing we know, we're right into the middle of another squabble. Scattergood Baines |Clarence Budington Kelland 

The principle would be valuable in many a squabble of corporate employer and hosts of servants in the modern time. Education: How Old The New |James J. Walsh 

Twice the big steamer stopped her engines and drifted until the squabble ahead of her seemed to have been settled. Blow The Man Down |Holman Day 

Fult Cawsler hes done moved hyar from over on Squabble Creek, an' opened a resteraw. The Code of the Mountains |Charles Neville Buck 

The hogs evidently thought it feed time, for they rushed forward and began to squabble over the voided matter. Lincolniana |Andrew Adderup