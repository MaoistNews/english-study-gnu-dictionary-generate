The principal articles of manufacture have long been sailcloth, cordage, linen and fishing-nets. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

“There,” he said, “you may sleep there for to-night,” and he pointed to a great heap of sailcloth beside the mast. Eric, or Little by Little |Frederic W. Farrar 

He was of middle age, black as anthracite coal, bald-headed, (p. 226) and was dressed in pants and coat made of old sailcloth. The Boys of '61 |Charles Carleton Coffin. 

He was dressed in pants made of old sailcloth, and the tattered cast-off blouse of a Union soldier. The Boys of '61 |Charles Carleton Coffin. 

The horse is a wooden circle, with a dress of blackened sailcloth, a horse's head, and a prominent tail. The Cornwall Coast |Arthur L. Salmon