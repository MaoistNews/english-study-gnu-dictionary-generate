When the Nats extended him for $245 million for seven years, I understood and agreed. MLB teams can buy hope easier than championships |Thomas M. Boswell |February 12, 2021 |Washington Post 

As a storyteller, Yeun says he is drawn to those who are “unseen,” and extends the notion of life in the gap to include the character. For Steven Yeun, making a film about Midwestern immigrants wasn’t a political statement. It was an ‘exercise in humanity.’ |Sonia Rao |February 12, 2021 |Washington Post 

Another daughter, Christina Farmer, recalled getting a message one morning that convinced her, for a short while, that her father was extending his trip in the Indiana countryside. An Indiana man never returned from a hunting trip. Strange texts and emojis led police to his son. |Katie Shepherd |February 12, 2021 |Washington Post 

It’s possible that light snow will extend into areas inside the Beltway for a time this evening. Snow expected tonight, mainly south of D.C., before possible ice on Saturday |Jason Samenow, Wes Junker |February 11, 2021 |Washington Post 

They extended the lead like three times on us when we cut the lead to six, seven, eight points. The Wizards’ defense again lets them down in loss at home to Raptors |Ava Wallace |February 11, 2021 |Washington Post 

At some point, show creator Mark Burnett made the diabolical decision to extend the show to 120 minutes. Donald Trump Fires Woman For Not Calling Bill Cosby |Jack Holmes, The Daily Beast Video |January 5, 2015 |DAILY BEAST 

We would like to extend our sincere sympathies to the family and friends of those on board QZ8501. Wreckage, Bodies of AirAsia Crash Found |Lennox Samuels |December 30, 2014 |DAILY BEAST 

In order to extend their legal residence in the United States, they had to obtain other visas. ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 

But would they be willing to extend their welcome to series that are streamed outside of Netflix? 15 Enraging Golden Globe TV Snubs and Surprises: Amy Poehler, 'Mad Men' & More |Kevin Fallon |December 11, 2014 |DAILY BEAST 

Valerie isn't going anywhere, and her work will extend through those she has cultivated and inspired. The Valerie Jarrett I Know: How She Saved the Obama Campaign and Why She’s Indispensable |Joshua DuBois |November 18, 2014 |DAILY BEAST 

This impulse to extend rule appears more plainly in many of the little ceremonial observances of the child. Children's Ways |James Sully 

I presume this path does not extend many miles without meeting impediments. Glances at Europe |Horace Greeley 

Not only do children thus of themselves extend the scope of our commands, they show a disposition to make rules for themselves. Children's Ways |James Sully 

I doubt if the modern community can afford to continue it; it certainly cannot afford to extend it very widely. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

This is especially the case in what are known as the Reed and Haydon properties which extend over about 200 acres. Asbestos |Robert H. Jones