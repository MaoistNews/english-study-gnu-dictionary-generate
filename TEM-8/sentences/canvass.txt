Since then, Arizona Senate President Karen Fann has suggested that it's worth revisiting the canvass idea. The Trailer: Whatever happened to Medicare-for-all? |David Weigel |July 22, 2021 |Washington Post 

Rothko signed on to paint another in his series of “rooms,” massive canvasses sized to fit specific spaces, as he’d done for the Four Seasons restaurant in New York’s Seagram Building. How These Rothkos Were Restored Without Touching the Canvas |Adam Rogers |May 30, 2021 |The Daily Beast 

One of the foremost artists in the Hudson River School of landscape painting, he was known for his highly detailed, epic canvasses favoring sunsets and waterfalls. The Hudson Valley’s outdoor art parks make for an alluring pandemic destination |Nevin Martell |April 23, 2021 |Washington Post 

Two ballots were excluded from the initial canvass because a machine wouldn’t let the voters cast them through curbside voting. A Democrat Is Challenging the Election Result in Her District. Republicans Are Seizing the Moment |Lissandra Villa |March 26, 2021 |Time 

The canvass period ended with both gaining votes since two weeks ago, when the races were called and Democrats began planning for their new majority. The Trailer: What's next for the Republican Party |David Weigel |January 19, 2021 |Washington Post 

There is no purpose in asking people to walk the neighborhood to canvass for someone who would support the status quo. Tea Party Takes on Boehner |Patricia Murphy |February 19, 2014 |DAILY BEAST 

Two blocks down Seventh Avenue, he came to Smiling Pizza, where he was scheduled to kick off a neighborhood campaign canvass. Bill De Blasio’s Retro Values Are Back in Fashion |Michael Daly |September 30, 2013 |DAILY BEAST 

The night sky has always been an amazing canvass for the human imagination. Goodnight Moon, Goodnight America |Noah Kristula-Green |May 23, 2012 |DAILY BEAST 

The software allows users to make phone calls, register to vote, and canvass neighborhoods with a few simple instructions. Midterm Scramble for Youth Vote |Dayo Olopade |October 3, 2010 |DAILY BEAST 

Or, a better preventive is a canvass or leather cap to protect the neck entirely from the storm. Domestic Animals |Richard L. Allen 

My sentiments are neither divine oracles nor theological opinions which it is not permitted to canvass. Letters To Eugenia |Paul Henri Thiry Holbach 

In the canvass of 1896 Mr. McKinley announced that he would make no electioneering tour. Great Men and Famous Women. Vol. 4 of 8 |Various 

But Mr Ferguson was not a sailor, or he would have known that it is the custom to reduce the grace in proportion with the canvass. Newton Forster |Captain Frederick Marryat 

Canvass of the local trucking industry brought to light the conveyor of that elegant article of furniture. Average Jones |Samuel Hopkins Adams