While most media-literate people can distinguish between a real news publisher and a fake one with some scrutiny, the bots that crawl through web pages or the algorithms that decide what to surface in newsfeeds don’t take those steps. WTF is trust.txt? |Max Willens |February 19, 2021 |Digiday 

Of course, writing systems are thousands of years old, found in ancient Sumer, China, and Egypt, but in most literate societies only a small fraction of people ever learned to read, rarely more than 10 percent. Martin Luther Rewired Your Brain - Issue 96: Rewired |Joseph Henrich |February 17, 2021 |Nautilus 

It’s very important that children be literate by third grade. What Cindy Marten’s Rise Tells Us About the State of Education Politics |Will Huntsberry |February 16, 2021 |Voice of San Diego 

One study of climate change sceptics, for example, found that the most scientifically literate people in the group were most likely to strongly endorse climate scepticism. ‘I Won The Election’ – How Powerful People Use Lousy Lies To Twist Reality |LGBTQ-Editor |November 25, 2020 |No Straight News 

The intention is to make the public and policymakers WUI literate and provide science and tools that could lead to the creation of cost-effective solutions, so we don’t keep repeating the same tragic, expensive mistakes. California Will Keep Burning. But Housing Policy Is Making It Worse. |by Elizabeth Weil and Mollie Simon |October 2, 2020 |ProPublica 

I am not the most financially literate person (I would be hard-pressed to articulate the term “junk bond”). Can Self-Help Books Really Make a New You? |Lizzie Crocker |December 29, 2014 |DAILY BEAST 

Pointing out that Nick Denton writes and speaks like a literate adult and not like a 14-year-old in remedial English. Rage Against GamerGate’s Hate Machine: What I Got For Speaking Up |Arthur Chu |November 17, 2014 |DAILY BEAST 

The trick for directors is to make it as surprising and shocking for such a gore-literate audience. Sex, Blood and Maroon 5: Pop Culture’s Wounds Run Deep |Lizzie Crocker |October 3, 2014 |DAILY BEAST 

The Keep America Safe website was at least a globally literate and coherent representation of international security issues. The Cheneys’ Permanent War |Heather Hurlburt |June 18, 2014 |DAILY BEAST 

Penguin India wet itself, and entered into an agreement with this semi-literate goon. Pulp Nonfiction: India’s Shameful Failure to Defend Historian of Hinduism |Tunku Varadarajan |February 13, 2014 |DAILY BEAST 

Flaubert is in six volumes, four or five of which every literate man must at one time or another assault. Instigations |Ezra Pound 

Lincoln, they knew, favored the extension of suffrage only to literate Negroes and to those who had served in the military forces. Susan B. Anthony |Alma Lutz 

The opinion was advanced that the evening of the day he landed his arrival was known in every literate home in New York. Turns about Town |Robert Cortes Holliday 

I doubt if there is a single literate person in the world to-day who would apply the word “wicked” to Shelley. The Art of Letters |Robert Lynd 

There they competed on alternate forums with literate gardeners and stuttering horticultural amateurs. Greener Than You Think |Ward Moore