I’m going to do what I can to change, to lobby and use my voice on behalf of some of these changes. Tom Colicchio Hopes (and Fears) COVID-19 Will Change the Restaurant Industry |Pallabi Munsi |September 16, 2020 |Ozy 

The increasing usage of voice search and voice-enabled devices also offers an opportunity for consumer brands to make it easier than ever for customers to find their products. How to drive digital innovation necessary during the pandemic |Nick Chasinov |September 16, 2020 |Search Engine Watch 

Growing up in Texas, Priya Tahim felt like she didn’t have a voice. A mental health crisis brewing among South Asian immigrants in the West needs serious attention |Amanat Khullar |September 15, 2020 |Quartz 

Honoring our grandparents is important and it’s great to have your voice be a part of this conversation. Debbie Allen’s Grandmother Love Doubled |Joi-Marie McKenzie |September 11, 2020 |Essence.com 

In Hurston’s book, power is shown through who is given a voice. Book recommendations from Fortune’s 40 under 40 in government and policy |Rachel King |September 10, 2020 |Fortune 

“Jeffrey wanted me to tell you that you looked so pretty,” the female voice said into my disbelieving ear. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

As he drove me back to the logging road, Frank told me about the area in his deep voice. The 7-Year-Old Plane Crash Survivor’s Brutal Journey Through the Woods |James Higdon |January 7, 2015 |DAILY BEAST 

When he does, here is a gentleness in his voice, a reflective and lovely quality that no movie he has been in has ever captured. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The Millennial Action Project (MAP) seeks to engage young people in politics and give them more of a voice in governing. When Will We See a #Millennial Congress? |Linda Killian |December 26, 2014 |DAILY BEAST 

“He is borrowing my voice to tell you this story,” she told the crowd. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

Other things being equal, the volume of voice used measures the value that the mind puts upon the thought. Expressive Voice Culture |Jessie Eldridge Southwick 

She was flushed and felt intoxicated with the sound of her own voice and the unaccustomed taste of candor. The Awakening and Selected Short Stories |Kate Chopin 

I called out several times, as loud as I could raise my voice, but all to no purpose. Gulliver's Travels |Jonathan Swift 

The Princess still kept her eyes fixed on Louis, while, in a suppressed and unsteady voice, she answered her governess. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

For this use of the voice in the special service of will-power, or propelling force, it is necessary first to test its freedom. Expressive Voice Culture |Jessie Eldridge Southwick