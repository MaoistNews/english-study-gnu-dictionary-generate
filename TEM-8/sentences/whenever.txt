I am fortunate that I have never been deathly ill, but whenever I have the stomach flu, I most certainly feel like I am dying. Why My Norovirus Panic Makes Me Sick |Lizzie Crocker |January 5, 2015 |DAILY BEAST 

For now, the Egyptian government has issued a statement saying that Clooney is free to enter Egypt “whenever she wants.” Amal Clooney vs. Egypt’s Courts |Christopher Dickey |January 4, 2015 |DAILY BEAST 

Weirich said whenever she saw Fox, she was wearing something too tight. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

According to the deacon, Williams made countless house calls and hospital visits whenever he could. Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 

This is just the way we consume TV now: our favorite shows, whenever we want them, wherever we want them. New Innovations Let You Watch TV Anywhere You Go | |December 8, 2014 |DAILY BEAST 

The real experience has a magnetism of its own and will win above mere technicality whenever it has the opportunity. Expressive Voice Culture |Jessie Eldridge Southwick 

I knew he was pleased, because whenever he corrected me he would say, "Nein, Kindchen" in such a gentle way! Music-Study in Germany |Amy Fay 

Is it true that whenever we are about to do an ill or unjust deed a shadow of the fruits it will bring comes over us as a warning? Elster's Folly |Mrs. Henry Wood 

Whenever he visited the garden he told everybody that he should never come there again because Grandfather Mole was too greedy. The Tale of Grandfather Mole |Arthur Scott Bailey 

There were machine guns here which wiped out the landing parties whenever they tried to get ashore North of the present line. Gallipoli Diary, Volume I |Ian Hamilton