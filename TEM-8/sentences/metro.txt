The largest threat facing the 4,000 coyotes in the Chicago metro area is vehicle collisions. Four wild animals that are thriving in cities |By Ryan Chelius/Outdoor Life |February 9, 2021 |Popular-Science 

That meant a bright sky and temperatures in the high 60s and people strewn and sunbathing along the beach out by Saint Petersburg on the west edge of the metro area, about 45 minutes from Raymond James Stadium. Super Bowl highlights: Bucs celebrate championship, Tom Brady wins MVP |Des Bieler, Mark Maske, Chuck Culpepper |February 8, 2021 |Washington Post 

Here are some scenes that Washington Post photographers have captured from around the metro region, with more snow on the way. After 2 to 4 inches of snow, an icy night ahead in D.C. area. Snow showers possible Monday. |Jason Samenow, Dan Stillman, Andrew Freedman |February 1, 2021 |Washington Post 

On Capital Weather Gang’s winter storm impact scale, this storm rates at a Category 3 “significant” winter storm for immediate metro area, primarily due to the amount of snow projected, cold temperatures prior to the storm, and its duration. Winter storm to blanket D.C. area in moderate snow Sunday, and more might come Monday |Jason Samenow, Wes Junker |January 31, 2021 |Washington Post 

That spike in population put the Atlanta metro area fourth in growth nationwide, behind Houston, Dallas, and Phoenix. Georgia went blue. Can Democrats make it happen elsewhere? |Ella Nilsen |January 15, 2021 |Vox 

They go to Paris, but never leave the underground metro station, where they stalk the metro mall shops. ‘Girlhood’: Coming of Age in France’s Projects |Molly Hannon |November 25, 2014 |DAILY BEAST 

There will be an issue of the New York Times and magazines, an iPhone and Kindle, metro cards and subway maintenance signs. New York’s Century-Old Time Capsule Is a Dud |Justin Jones |October 8, 2014 |DAILY BEAST 

Looting at the Metro store was another headache among many, he said. The Rot Among the Rebels in Ukraine |Anna Nemtsova |May 31, 2014 |DAILY BEAST 

On Wednesday, however, Martin's people set the record straight, telling The Metro that he and Chung “are definitely not dating.” Solange Knowles’s Met Gala Clutch ‘Worth Fighting For’; Nicolas Ghesquiere Brings Top Talent to First Louis Vuitton Campaign |The Fashion Beast Team |May 14, 2014 |DAILY BEAST 

We kept that a secret so well, apart from The Metro or some newspaper in the U.K. that did an article on it. ‘Game of Thrones’ Star Maisie Williams, aka Arya Stark, on Her Big Premiere Episode ‘Two Swords’ |Marlow Stern |April 7, 2014 |DAILY BEAST 

Aristoteles de regimine principumGwydo de excidio Troianorumidem in metro. Henry the Sixth |John Blacman 

In the metro-entry at the top of the stairs they went through a security check station manned by six blaster-armed police guards. The Cartels Jungle |Irving E. Cox, Jr. 

A subway is always a tube, or the underground, or the Metro. The American Language |Henry L. Mencken 

"There's a new picture at the Metro," he said as quietly as he could. No Strings Attached |Lester del Rey 

I went down into the Metro and in time arrived at the station. A "Y Girl in France |Katherine Shortall