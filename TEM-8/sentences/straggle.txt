Survivors with minor injuries straggled out with blood splattered on their clothes. Islamic State claims mosque blast in Kunduz, Afghanistan, that killed nearly 50, injured dozens |Sudarsan Raghavan, Ellen Francis |October 8, 2021 |Washington Post 

The humpbacks would slap their tails to keep the prey inside that sphere, and pick off the straggling fish nearer to the outside. A rare humpback whale ‘megapod’ was spotted snacking off the Australian coast |Hannah Seo |September 14, 2021 |Popular-Science 

Better to straggle along on Zoom, seeing one another’s faces, than mask up for eight hours or more. Require the vaccine. It’s time to stop coddling the reckless. |Ruth Marcus |July 30, 2021 |Washington Post 

One by one, the boys straggle out to the cars, again looking sleepy and hung-over. Stacks: Hitting the Note with the Allman Brothers Band |Grover Lewis |March 15, 2014 |DAILY BEAST 

It was plain that the Indians were dogging our steps day and night, and the men were warned not to straggle. A Virginia Scout |Hugh Pendexter 

The Rebel army had made slower marches, and the soldiers could not straggle; they were in an enemy's country. The Boys of '61 |Charles Carleton Coffin. 

Soon groups of natives began to straggle up, not in regular formation this time. A Frontier Mystery |Bertram Mitford 

The agile and powerful Zulu, however, was half up in a moment, and the straggle became a hand-to-hand one. The Luck of Gerard Ridgeley |Bertram Mitford 

He could either possess his soul in patience until the mounted contingent began to straggle back, or risk another rock-fall. The Terms of Surrender |Louis Tracy