Campaigns have also been forced to reconsider their conception of Pennsylvania’s political geography. Why Pennsylvania Could Decide The 2020 Election |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |September 15, 2020 |FiveThirtyEight 

Compare our conception of werewolves, vampires, and zombies. Believing in Monsters: David Livingstone Smith on the Subhuman - Facts So Romantic |Eric Schwitzgebel |September 11, 2020 |Nautilus 

Two Conceptions of RealityThese challenges are rooted in a conception of reality which we have gotten so used to that we’re not even aware that there’s an alternative. The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 

There’s another conception of reality which I call the “container view.” The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 

Another incredible conception from da Vinci, the Armored Car is surely the forerunner to the contemporary military tanks. 14 Exceptional Weapon Systems from History That Were Ahead of their Time |Dattatreya Mandal |March 26, 2020 |Realm of History 

What it endangers is a narrow conception of Russian power, understood through the eyes of its dictatorial leader. Oliver Stone’s Latest Dictator Suckup |James Kirchick |January 5, 2015 |DAILY BEAST 

Spar has a new book titled The Baby Business: How Money, Science and Politics Drive the Commerce of Conception. Want Blue Eyes With That Baby?: The Strange New World of Human Reproduction |Eleanor Clift |November 24, 2014 |DAILY BEAST 

He completely disrupts not only the conception of architecture—but also the fabrication, the mise en oeuvre of architecture. Frank Gehry Is Architecture’s Mad Genius |Sarah Moroz |October 27, 2014 |DAILY BEAST 

In fact, the airplane and the movie were more or less simultaneous in conception (the movie opened in 1968). The Sexy Dream of the 747 |Clive Irving |October 26, 2014 |DAILY BEAST 

North Dakota has a less subtle constitutional amendment on the ballot stating that life begins at conception. America’s Most Important (and Wackiest) Referendums This November |Ben Jacobs |October 22, 2014 |DAILY BEAST 

Adequate conception of the extent, the variety, the excellence of the works of Art here heaped together is impossible. Glances at Europe |Horace Greeley 

The conception of the relation of this institution with them as co-operative makes headway slowly. Readings in Money and Banking |Chester Arthur Phillips 

The myth of "Boreas and Orithyia," though faulty perhaps in technique, is good in conception and arrangement. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

From that hour dated a new and sterner conception of the task that lay before him and every other Briton in the country. The Red Year |Louis Tracy 

In fact, he says, with great candour and courage, that the early Bible conception of God is one which we cannot now accept. God and my Neighbour |Robert Blatchford