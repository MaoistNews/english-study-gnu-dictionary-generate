Before that, the secretary himself, Raffensperger, met with von Spakovsky in the summer of 2019 at Raffensperger’s request. No Democrats Allowed: A Conservative Lawyer Holds Secret Voter Fraud Meetings With State Election Officials |by Mike Spies, Jake Pearson and Jessica Huseman |September 15, 2020 |ProPublica 

National Association of Secretaries of StateEvery state makes its own voting rules, so it makes sense that the secretaries of state, whose offices handle elections, would have banded together to help people vote. It’s time to check your voter registration—here’s how |John Kennedy |September 10, 2020 |Popular-Science 

The secretary of state said this would take a couple of weeks to resolve. As the U.K.’s coronavirus testing system struggles, the health secretary blames too many ‘inappropriate’ tests |David Meyer |September 9, 2020 |Fortune 

Included in the ban are ads purchased by election officials — secretaries of state and boards of elections — who use Facebook to inform voters about how voting will work. Facebook’s Political Ad Ban Also Threatens Ability to Spread Accurate Information on How to Vote |by Jeremy B. Merrill for ProPublica |September 4, 2020 |ProPublica 

She previously served as the assistant press secretary and director of women’s media at the Democratic National Committee. Donald Trump has failed to protect me and other students from COVID-19 |jakemeth |September 3, 2020 |Fortune 

And compare, as noted up top, to Secretary Clinton, who spent years quietly pushing a modernized Cuba policy. Rubio’s Embargo Anger Plays to the Past |Michael Tomasky |December 19, 2014 |DAILY BEAST 

The certification, which lasts three years, was renewed by then-Defense Secretary Leon Panetta in 2012. The Detainee Abuse Photos Obama Didn’t Want You To See |Noah Shachtman, Tim Mak |December 15, 2014 |DAILY BEAST 

Even Defense Secretary Gates, at least for a time, was open to the notion. The Detainee Abuse Photos Obama Didn’t Want You To See |Noah Shachtman, Tim Mak |December 15, 2014 |DAILY BEAST 

In order to withhold the photographs, the secretary of defense must certify that photographs could cause harm to Americans. The Detainee Abuse Photos Obama Didn’t Want You To See |Noah Shachtman, Tim Mak |December 15, 2014 |DAILY BEAST 

ALEC echoed the ideology of Charles Wilson, the first Defense Secretary in the Eisenhower administration. The Left’s Answer to ALEC |Ben Jacobs |December 15, 2014 |DAILY BEAST 

He desired his secretary to go to the devil, but, thinking better of it, he recalled him as he reached the door. St. Martin's Summer |Rafael Sabatini 

At his desk sat his secretary, who had been a witness of the interview, lost in wonder almost as great as the Seneschal's own. St. Martin's Summer |Rafael Sabatini 

I should pay a capable secretary like you—knowing several languages and all that—say forty dollars a week. Rosemary in Search of a Father |C. N. Williamson 

Probably his Private Secretary, considering you a new man, will have failed to furnish the necessary information. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

And here let me point out for your future guidance the importance of having a private secretary thoroughly up to his work. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various