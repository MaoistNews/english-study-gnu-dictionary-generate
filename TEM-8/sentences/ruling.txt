Brendan Johnson, a lawyer who represents South Dakotans for Better Marijuana Laws, the group that put the measure on the ballot, told the Sioux Falls Argus Leader that he planned to appeal the ruling to state Supreme Court. South Dakota voters said yes to legalizing marijuana. But a judge ruled it’s unconstitutional. |Teo Armus |February 9, 2021 |Washington Post 

Included within the decision, was a ruling that for all new Android devices, Google must offer users a choice in their default search engine. The future of Google and what it means for search |Pete Eckersley |February 5, 2021 |Search Engine Watch 

This weekend, Xiaomi posted a response to the ruling, which came in the form of legal complaints to the US Defense and Treasury Departments. Xiaomi denies ties to China’s military, calls US ruling “unconstitutional” |Ron Amadeo |February 1, 2021 |Ars Technica 

Based on the ruling, two schools — Coolidge High and Watkins Elementary — will probably have to delay their reopenings and schedule new walk-throughs. D.C. public schools planned to reopen Monday but weather will keep classes virtual, mayor says |Perry Stein |February 1, 2021 |Washington Post 

The state’s 4th District Court of Appeal on Friday overturned a San Diego Superior Court judge’s ruling that had allowed restaurants to open for in-person dining in defiance of the state’s stay-at-home order. Morning Report: School District Showed Employees How to Permanently Delete Emails |Voice of San Diego |January 25, 2021 |Voice of San Diego 

The WHO has agreed to meet with Yang and a number of Chinese NGOs to discuss a broader ruling on the practice. China’s Electroshock Gay-Conversion Case |Nina Strochlic |December 19, 2014 |DAILY BEAST 

That ruling is binding law in the United States, no matter what the former vice president says. Fact-Checking the Sunday Shows: Dec. 14 |PunditFact.com |December 14, 2014 |DAILY BEAST 

The constitutional problem with this ruling, experts say, is that it places an extra burden on women for being pregnant. States Slap Pregnant Women With Harsher Jail Sentences |Emily Shire |December 12, 2014 |DAILY BEAST 

“It is well established that a fetus is not a ‘person’; rather it is a sui generis organism,” the ruling stated. Court Says Fetal Alcohol Syndrome Isn’t a Crime |Elizabeth Picciuto |December 9, 2014 |DAILY BEAST 

That ruling was affirmed in a final judgment by the Afghan courts, which the Afghan Supreme Court confirmed in 2013. Special Forces’ $77M ‘Hustler’ Hits Back |Kevin Maurer |December 8, 2014 |DAILY BEAST 

And ruling over the present people, and by the strength of wisdom instructing the people in most holy words. The Bible, Douay-Rheims Version |Various 

Not only did she fully intend Hartledon House to be her home, but she meant to be its one ruling power. Elster's Folly |Mrs. Henry Wood 

In the political sphere he was the soul of the insurgent movement, the ruling power behind the presidency of Aguinaldo. The Philippine Islands |John Foreman 

The ruling passion of the brother-in-law was a stern and acrimonious party spirit. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Jessie recalled all the strange stories she had heard of the ruling house of Asturia, of its intrigues and fiery conspiracies. The Weight of the Crown |Fred M. White