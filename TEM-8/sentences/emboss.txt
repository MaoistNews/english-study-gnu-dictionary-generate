Of course, top grain leather is the most expensive, and we have suede, die cut, embossed, patent, and a variety of other techniques used to change the look of a hide. Leather and lace in your home decor |Valerie Blake |January 15, 2022 |Washington Blade 

The words “Die Heilige Schrift der Israeliten” — the Holy Scriptures of the Israelites — were embossed on the front. A family hid their Bible in an attic as Nazis invaded. Almost 80 years later, it was reunited with the family’s heirs. |Nicole Asbury |August 24, 2021 |Washington Post 

When we see someone else live our dream first, we extend a gold-embossed invitation to that kind of disappointment, a FOMO we will never live down. You Don’t Need to Watch Hiking Videos to Hike |Grayson Haver Currin |June 15, 2021 |Outside Online 

In our earlier days did we not emboss our bosoms with the daffodils, and shake them almost unto shedding with our transport? Imaginary Conversations and Poems |Walter Savage Landor 

When he was preparing his magnificent vase for the Exhibition, he was advised to emboss it with the royal arms of England. Harper's New Monthly Magazine, Vol IV. No. XX. January, 1852. |Various 

But there is a way by which almost anyone may emboss stationery at home with one's own design at no expense whatever. The Boy Mechanic, Book 2 |Various 

Steel dies for flower-shapes have a cutting edge, so that they can stamp out and emboss in one action. The New Gresham Encyclopedia |Various 

It is cover'd with Dutch emboss'd paper, almost totally gilt. Experiments and Observations on Electricity made at Philadelphia in America |Benjamin Franklin