The roof, doors, and windows will be sourced locally, and the whole process can be completed in less than a week, another major advantage over traditional building methods. The World’s First 3D Printed School Will Be Built in Madagascar |Vanessa Bates Ramirez |February 26, 2021 |Singularity Hub 

She met her first husband and writing partner Gerry Goffin, a chemistry major while they were students at Queens College. Fifty years later, ‘Tapestry’s’ hope and optimism still resonates |Tanya Pearson |February 26, 2021 |Washington Post 

CHth1zkHj8WOne of the ways that Toyota is able to create vehicles with such strong reputations for reliability is by reusing major components across multiple vehicles. How to Modify Your Tacoma the Right Way |Wes Siler |February 25, 2021 |Outside Online 

The newspapers reported that Thomas “Shaky Tom” Anderson and Jimmy “Kid Riviera” Williams were major players in the policy racket. The Murder Chicago Didn’t Want to Solve |by Mick Dumke |February 25, 2021 |ProPublica 

Another former Democratic lawmaker who now leads a major Texas city similarly tried and failed to pass legislation that would bring greater accountability to the state. “Power Companies Get Exactly What They Want”: How Texas Repeatedly Failed to Protect Its Power Grid Against Extreme Weather |by Jeremy Schwartz, Kiah Collier and Vianna Davila |February 22, 2021 |ProPublica 

To put it rather uncharitably, the USPHS practiced a major dental experiment on a city full of unconsenting subjects. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Other major news outlets made the same decision, hiding behind a misplaced sense of multicultural sensitivity. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

Iraq may have been an irregular fight, but it had major moments. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

This is the Mexico that the U.S. Chamber of Commerce, and most major U.S. corporations, are eager to call amigo. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

Those who have watched anti-gay groups closely suggest that there will be two major strategic shifts in their strategy. ‘Only God’ Can Stop Gay Marriage |Tim Mak |January 6, 2015 |DAILY BEAST 

The major-general kept him well informed of every movement of the enemy, and pointed out the dangerous isolation of Davout. Napoleon's Marshals |R. P. Dunn-Pattison 

Thereon the major-general took on himself to nominate Prince Eugne as Murat's successor. Napoleon's Marshals |R. P. Dunn-Pattison 

Major Abbott and his brother officers, trying to keep their men loyal, stood fast and listened to the distant turmoil in the city. The Red Year |Louis Tracy 

Frulein Fichtner was the young lady who was going to play his concerto in A major at the concert that evening. Music-Study in Germany |Amy Fay 

He was a good judge of men, that eagle-faced major; he knew that the slightest move with hostile intent would mean a smoking gun. Raw Gold |Bertrand W. Sinclair