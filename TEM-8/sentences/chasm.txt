The Wizards lost, 119-97, to the Hornets after falling into yet another first-half hole — they have felt more like chasms lately — this one a 21-point deficit. Wizards’ defense falters again in a 119-97 loss to the Hornets |Ava Wallace |February 7, 2021 |Washington Post 

The chasm in the party between its base and its elected leaders in Washington can be traced back to the cracks that emerged about a decade ago. The Republican Party tries to figure out the path forward |Philip Bump |February 2, 2021 |Washington Post 

Some of them appear to have erupted from volcanic pits or chasms within the last few million years, perhaps even within the last few tens of thousands of years. Rumbles on Mars Raise Hopes of Underground Magma Flows |Robin George Andrews |February 1, 2021 |Quanta Magazine 

Allen and Cummings crossed lines that in America today increasingly resemble a chasm. In neighboring Georgia counties, election revealed a growing divide that mirrors the nation |Haisten Willis, Griff Witte |November 30, 2020 |Washington Post 

A chasm had opened between me and my skin, as though I were fumbling around in a too-big pair of gloves. Zeyn Joukhadar’s ‘The Thirty Names of Night’ is a poetic portrait of a trans man’s search for a rare bird — and his own identity |Carol Memmott |November 23, 2020 |Washington Post 

Over the next few years, a chasm would open up between the Party and the KGB, culminating with the failed coup in August 1991. How the Fall of the Berlin Wall Radicalized Putin |Masha Gessen |November 9, 2014 |DAILY BEAST 

I was writing a cover story for ‬Newsweek‪ about the chasm between white and black understandings of the Martin case. Maya Angelou Knew How To Inspire As A Writer, Teacher, and Great Human Being |Joshua DuBois |May 28, 2014 |DAILY BEAST 

The result in all three cases is a chasm between image and performance that magnifies the narrative of dashed expectations. Even the Most Powerful Man in the World Is at the Mercy of the IT Guy |Jill Lawrence |March 5, 2014 |DAILY BEAST 

Inching towards the opposing positions will never bridge the yawning chasm between them. The Reality Behind Kerry's Optimism |Matthew Kalman |July 1, 2013 |DAILY BEAST 

The vast chasm between these two groups and regular Republicans is something that Republican lawmakers can't easily bridge. Republicans and Entitlements |Michael Tomasky |March 1, 2013 |DAILY BEAST 

At a distance of four miles from the colony, a waterfall foams down a chasm which it has worn away for itself. A Woman's Journey Round the World |Ida Pfeiffer 

The procession, preceded by Bob on his feathered steed, passed through a chasm overgrown with brambles. Honey-Bee |Anatole France 

In most cases the roofs over these sea caves fall in, so that the structure is known as a chasm. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Two miles to the east the San Juan burst out of a defile of sandstone, and a mile to the west it disappeared in a similar chasm. Overland |John William De Forest 

He was in a chasm, twenty-five hundred feet below the average surface of the earth, the floor of which was a swift river. Overland |John William De Forest