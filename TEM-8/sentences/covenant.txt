Our hosts were joined by Alain Stephens of The Trace, who gave a great lesson on ghost guns, and Cristina Kim of KPBS, who has reported on racially restrictive housing covenants that remain in public records. Morning Report: The Street Vending Predicament |Voice of San Diego |November 29, 2021 |Voice of San Diego 

If you look back at the deeds for Levittown and other places, you’ll find that there are covenants in them requiring the owner of a new Levittown home to mow their lawn, their yard once a week. How Stupid Is Our Obsession With Lawns? (Ep. 289 Rebroadcast) |Stephen J. Dubner |July 1, 2021 |Freakonomics 

Toptal says three additional former employees in non-executive roles breached express covenants not to compete in their agreements with Toptal. Freelancer marketplace Toptal sues Andela and ex-employees, alleging theft of trade secrets |Tage Kene-Okafor |June 11, 2021 |TechCrunch 

The SBA also considered whether it should revive an old pilot program used to fund underserved businesses via nonprofit intermediaries, or impose covenants that would bar distribution of profits until the loan had been repaid. The Government Is Here to Help Small Businesses — Unless They’re Cooperatives |by Lydia DePillis |June 7, 2021 |ProPublica 

Racial covenants effectively maintained residential segregation without the use of explicit racial zoning. 'Haunted Countries Deserve Haunted Stories.' How America's History of Racial Housing Discrimination Inspired Amazon's New Horror Series THEM |Megan McCluskey |April 9, 2021 |Time 

Fed-up doctors want that too—and many have begun to reclaim the covenant between doctor and patient. The Health-Care System Is So Broken, It’s Time for Doctors to Strike |Daniela Drake |April 29, 2014 |DAILY BEAST 

“The Covenant—which should have been nominated,” he says with a laugh. ‘Lone Survivor’ Taylor Kitsch’s Journey From Homelessness to Hollywood Stardom |Marlow Stern |December 18, 2013 |DAILY BEAST 

The sacred covenant of travel dictates: always chase the new. I Can’t Shake Hawaii: An Ode to Returning to Places You’ve Been Before |Debra A. Klein |October 7, 2013 |DAILY BEAST 

But MLB has a duty at least to try to live up to its contract with the players and its covenant with the public. Major League Baseball Is Right to Punish the Biogenesis Cheats |Michael Brendan Dougherty |June 6, 2013 |DAILY BEAST 

It has the right to regard the threat that Hamas poses as an annihilating one, given the emphatic language of its covenant. Is Israel Immoral to Retaliate Against Gaza? |Thane Rosenbaum |November 24, 2012 |DAILY BEAST 

The scene is the covenant made between the two first persons of the Trinity on Mount Moriah. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

The solemn league and covenant burned by the common hangman at London, and afterwards throughout the country. The Every Day Book of History and Chronology |Joel Munsell 

The term covenant designating their relation to him as a people is not figuratively applied to it. The Ordinance of Covenanting |John Cunningham 

Besides having parties,—one essential of a covenant in its proper acceptation, this relation with God has conditions. The Ordinance of Covenanting |John Cunningham 

Covenanting in civil life is the exercise of entering into a covenant engagement, or of renewing it. The Ordinance of Covenanting |John Cunningham