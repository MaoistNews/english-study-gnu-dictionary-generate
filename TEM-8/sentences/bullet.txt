The dot formation in the weekday slots borrows from the ultra-popular bullet journal method, but handles much of the work for you. Desk calendars to organize your life |PopSci Commerce Team |September 10, 2020 |Popular-Science 

Taiwan’s compact size means that adventure is never more than two hours away by bullet train from any major city. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

While recognizing monthly fluctuations can be useful, period tracking isn’t a silver bullet. Everything You Need to Know About Period Tracking |Christine Yu |September 6, 2020 |Outside Online 

Whatever his intentions, the bullets were in the chamber long before Rittenhouse was born. The Rise of American Militias, From Timothy McVeigh to Kyle Rittenhouse |Nick Fouriezos |September 6, 2020 |Ozy 

AB 66 would have reined in the use of rubber bullets and other non-lethal weapons on protesters. Sacramento Report: 3 Takeaways From a Wild Legislative Session |Sara Libby |September 4, 2020 |Voice of San Diego 

Merabet had already been immobilized by a bullet to the groin. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 

The incident still might have sparked trouble because that lone bullet proved fatal for a man who was black. Synagogue Slay: When Cops Have to Kill |Michael Daly |December 10, 2014 |DAILY BEAST 

He survived, Risner says, but was left permanently injured by a bullet to his spine. Drug Smuggler Sues U.S. Over Dog Bite |Caitlin Dickson |December 10, 2014 |DAILY BEAST 

Not only did a cherished character get a bullet to the brain, but things are only going to get worse on The Walking Dead. ‘Walking Dead’ Showrunner Scott Gimple Teases ‘Darker, Weirder’ Times Ahead |Melissa Leon |December 2, 2014 |DAILY BEAST 

Her mother, pregnant at the time of the killing, was hit in the shoulder by a bullet from the same gun that killed her son. Drawing on the Memories of Syrian Women |Masha Hamilton |November 26, 2014 |DAILY BEAST 

He had not the least idea what wadding was, and his notion of a bullet was a dockyard cannon-ball bigger than his own head. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Next moment Tom Brown sent a bullet straight into his heart, and his tail made a splendid flourish as he fell off his pedestal! Hunting the Lions |R.M. Ballantyne 

Once even a blue bean (a bullet) made sad work with my head, and my fist has got a deuce of a smashing. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

In the meantime, whilst Captain Roman was running towards a house he was shot dead by a bullet in his breast. The Philippine Islands |John Foreman 

I could almost afford to be shot for the pleasure of putting a bullet through the black heart of Jennison. The Courier of the Ozarks |Byron A. Dunn