Costs would need to drop to around a sixth of this level for the company to make a profit. The World’s Largest Direct Air Capture Plant Is Now Pulling CO2 From the Air in Iceland |Vanessa Bates Ramirez |September 9, 2021 |Singularity Hub 

Months had passed since the sixth-grader decided he wanted to die, and now the day that he hoped would be his last had come. As school shootings surge, a sixth-grader tucks his dad’s gun in his backpack |John Woodrow Cox, Steven Rich |June 24, 2021 |Washington Post 

The Englishman rebounded to make par on the par-3 sixth and maintains a one-shot lead over Marc Leishman and Will Zalatoris for now. Hideki Matsuyama’s sublime day at the Masters gives him a four-shot lead and a shot at history |Chuck Culpepper, Scott Allen |April 11, 2021 |Washington Post 

“You don’t have to fight them to wear masks,” said Snew, who is in her first year teaching Bear Valley sixth-graders English and social studies. One North County School Managed to Open and (Mostly) Stay Open |Ashly McGlone |March 29, 2021 |Voice of San Diego 

He most recently coached the Nashville Predators, who fired him in the middle of his sixth season last January. What you may have missed in the Caps’ offseason, and what experts predict for this season |Scott Allen |January 14, 2021 |Washington Post 

This “Sixth Migration” of massive human migration to Texas is the larger story of the book, and it is a significant story. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

Rick would cut together five years worth of work, add the sixth, then recut six years worth of work, add the seventh, and so on. Coffee Talk with Ethan Hawke: On ‘Boyhood,’ Jennifer Lawrence, and Bill Clinton’s Urinal Exchange |Marlow Stern |December 27, 2014 |DAILY BEAST 

Turkey has had more than a decade of economic boom, and is now the sixth-most-visited tourist destination in the world. The Women Battling an Islamist Strongman |Christina Asquith |December 22, 2014 |DAILY BEAST 

We are currently in the sixth, which is predicted to be the worst, and, to make matters worse, we are the cause. The Best Big Ideas Of 2014 |William O’Connor |December 11, 2014 |DAILY BEAST 

Instead of our sixth iPhone we suddenly start thinking about our first Jitterbug. Christmas Is the New Subprime |Doug McIntyre |December 9, 2014 |DAILY BEAST 

Under the one-sixth they appear as slender, highly refractive fibers with double contour and, often, curled or split ends. A Manual of Clinical Diagnosis |James Campbell Todd 

The two-thirds objective should be used as a finder, while the one-sixth is reserved for examining details. A Manual of Clinical Diagnosis |James Campbell Todd 

They are easily seen with the one-sixth objective in the routine microscopic examination. A Manual of Clinical Diagnosis |James Campbell Todd 

The sixth line is in these words: “Keeping time, time, time, in a sort of Runic rhyme.” Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Who can explain the sixth sense that warns a night-herder of a stampede a moment before the herd jumps off the bed-ground? Raw Gold |Bertrand W. Sinclair