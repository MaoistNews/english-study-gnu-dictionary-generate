After swarming through barricades, rioters forced their way inside. How to fight online hate before it leads to violence |Kathiann Kowalski |February 4, 2021 |Science News For Students 

In 1971 he helped organize the May Day demonstrations in Washington, where protesters — thousands of whom were arrested — erected barricades throughout the District to voice their opposition to the war. Rennie Davis, ‘Chicago Seven’ activist and leader of New Left, dies at 80 |Emily Langer |February 4, 2021 |Washington Post 

Police blocked access to the court with metal barricades, and dozens of riot police lined the streets as mounted units patrolled the area. Russian opposition leader Navalny ordered jailed, calls on supporters to keep pressure on Putin |Isabelle Khurshudyan, Robyn Dixon |February 2, 2021 |Washington Post 

They should be stopped before they can do that — stopped where they are now — not at barricades that wall off our elected representatives from the people they serve. The danger of right-wing mobs is real. Fencing at the U.S. Capitol won’t help. |Philip Kennicott |February 1, 2021 |Washington Post 

Farmers used tractors to tear down police barricades, and videos of police attacking protesting farmers circulated on social media. Why Twitter Blocked Accounts Linked to Farmers' Protests in India—Only to Reverse Course |Billy Perrigo |February 1, 2021 |Time 

Sarah stood by the police barricade with her 12-year-old sister, Mary, and their mother, Rada. Synagogue Slay: When Cops Have to Kill |Michael Daly |December 10, 2014 |DAILY BEAST 

A white police officer standing amid the crowd inside the barricade got his laughs a moment later. ‘They Let Him Off?’ Scenes from NYC in Disbelief |Jacob Siegel |December 4, 2014 |DAILY BEAST 

(Rioters) were building a barricade across Winchester Street and looking for material. Frat Culture Clashes With Riot Police at Keene, N.H., Pumpkin Festival |Melanie Plenda |October 19, 2014 |DAILY BEAST 

In front of the City Hall building hundreds of tires have been piled up to form a barricade that is manned by yet more masked men. Pro-Russian Protestors in Ukraine Dream of Soviet Glory Days |David Patrikarakos |April 8, 2014 |DAILY BEAST 

They made a barricade of metal junk and acted as human shields to stop the train proceeding. Soccer Hooligans Prep Ukraine for Putin |Jamie Dettmer |March 20, 2014 |DAILY BEAST 

Across the middle of the cage a stout barricade has been erected, and behind the barricade sits the Master, pale but defiant. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

The besiegers forced the advance barricade, burned the drawbridge, and fired the gate. King Robert the Bruce |A. F. Murison 

Then she issued her commands to the men, and fiercely she bade them pull down that barricade and take the dog alive. St. Martin's Summer |Rafael Sabatini 

He would go alone if he must; no barricade of unearthly beasts could hold him from the great adventure. Astounding Stories, May, 1931 |Various 

First there was to be seen the city itself, nestled beyond its barricade of levees. The Sunbridge Girls at Six Star Ranch |Eleanor H. (Eleanor Hodgman) Porter