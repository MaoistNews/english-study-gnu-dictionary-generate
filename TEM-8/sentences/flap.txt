You can wear them as mittens when it’s excruciatingly cold, and if you need to use your fingers, one tuck of a flap or pull of a zipper can turn them into fingerless gloves. Best mittens: Keep your hands cozy |PopSci Commerce Team |February 8, 2021 |Popular-Science 

That’s right, we’re talking ear flaps also known as trapper hats. Best winter hats: Comfortable hats to keep you warm |Carsen Joenk |January 20, 2021 |Popular-Science 

“It sounds like a sharp thunderclap,” said Darden, who published more than 50 papers on high-lift wing design in supersonic flow, flap design and sonic boom prediction and minimization. The NASA Engineer Who’s a Mathematician at Heart |Susan D'Agostino |January 19, 2021 |Quanta Magazine 

Beanies and hats with ear flaps always ride up, especially under my helmet. The Gear Our Editors Loved in December |The Editors |January 6, 2021 |Outside Online 

We can see in the videos released by SpaceX that the flaps are under good control. SpaceX Starship Prototype Exploded, but It’s Still a Giant Leap Towards Mars |Hugh Hunt |December 17, 2020 |Singularity Hub 

My editor called and said, “Do a column on this Lena Dunham flap!” Up to a Point: They Made Me Write About Lena Dunham |P. J. O’Rourke |December 13, 2014 |DAILY BEAST 

Yet, after flipping through Not That Kind of Girl, I do begin to understand what “this Lena Dunham flap” is about. Up to a Point: They Made Me Write About Lena Dunham |P. J. O’Rourke |December 13, 2014 |DAILY BEAST 

If you flap your arms hard enough, a pair of vast feathered wings appears to grow out from your shoulders. Frickin’ Laser Beams Run by Eyeballs: The Next Art Revolution Is Here |Nico Hines |July 7, 2014 |DAILY BEAST 

Take my former boss, Mitt Romney, and the flap over a Jeep plant in Ohio. When Campaign Spin Becomes Fact |Stuart Stevens |March 21, 2014 |DAILY BEAST 

Songs that the Hyades shall sing, Where flap the tatters of the King, Must die unheard in Dim Carcosa. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

He shifted across to the mouth of the tent and raised the flap, fastening it against the pole so that he could see out. Three More John Silence Stories |Algernon Blackwood 

"That's the wind rising," whispered the clergyman, and pulled the flap open as far as it would go. Three More John Silence Stories |Algernon Blackwood 

And as I watched the canvas shake and heard it boom and flap I heartily welcomed it. Three More John Silence Stories |Algernon Blackwood 

The flap was wide open and any ordinary animal would have been out and away without the least trouble. Three More John Silence Stories |Algernon Blackwood 

Mother thinks a dash-churn, stand and flap the dasher straight up and down till your arms and legs give out, is the best kind. Dorothy at Skyrie |Evelyn Raymond