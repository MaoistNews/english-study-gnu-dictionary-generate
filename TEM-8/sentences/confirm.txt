Barry Schwartz“Is This New” unofficial Google Liaison After announcing passage ranking, formerly known as passage indexing, last October, Google has officially confirmed that it began to roll out passage indexing in the US English search results. Google goes dark theme and passage ranking sees the light: Friday’s daily brief |Barry Schwartz |February 12, 2021 |Search Engine Land 

Investigators relied on videos from inside the Capitol that circulated on social media to confirm his presence. Grand jury beefs up charges against Olympian Klete Keller related to Capitol riot |Rick Maese |February 11, 2021 |Washington Post 

Another group confirmed that emissions from eastern China have diminished since 2018 by analyzing air samples from Hateruma, Japan, and Gosan, South Korea. A drop in CFC emissions puts the hole in the ozone layer back on track to closing |Maria Temming |February 10, 2021 |Science News 

Asked Monday morning whether he’d spoken with his son since the crash, the head coach said that he had and confirmed that Britt Reid had undergone an unspecified surgery. As police continue probe of Britt Reid crash, 5-year-old remains in critical condition |Rick Maese |February 9, 2021 |Washington Post 

We are still investigating the incident, however at this time we can confirm that — to our best knowledge — the compromised systems did not contain any personal data of our players or users of our services. CD Projekt hit by ransomware attack, refuses to pay ransom |Zack Whittaker |February 9, 2021 |TechCrunch 

DNA tests were used to confirm Albert's status as father in both cases, following protracted legal battles. Princess Charlene Gives Birth To Twins Gabriella and Jacques |Tom Sykes |December 10, 2014 |DAILY BEAST 

Prosecutor Alessandro Leopizzi wasted no time in repeating the transcript to ask Schettino to confirm his words. The Costa Concordia’s Randy Reckless Captain Takes the Stand |Barbie Latza Nadeau |December 2, 2014 |DAILY BEAST 

Furthermore, data confirm a connection between education level and police behavior. Are College Educated Police Safer? |Keli Goff |December 1, 2014 |DAILY BEAST 

Blood spatters on the pavement seem to confirm that he was moving toward Wilson when the instantly fatal shot was fired. 90 Seconds of Fury in Ferguson Are the Key to Making Peace in America |Michael Daly |November 26, 2014 |DAILY BEAST 

Just three Republicans out of 44 voted to confirm Hagel, himself a former GOP senator. The Coming Battle for a New SecDef |Michael Tomasky |November 25, 2014 |DAILY BEAST 

When the oath is given to confirm an assertion, it is sworn in confirmation of a covenant with God. The Ordinance of Covenanting |John Cunningham 

In later periods still, the history of African travelers, confirm all the former accounts concerning the industry of the people. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

But it is at least deserving of notice, that the very few apparent exceptions to this rule evidently tend to confirm it. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Chet touched a button, and a white light flashed to confirm his report that all was clear. Astounding Stories, May, 1931 |Various 

To confirm the evidence of these gentlemen two or three sailors were put into the witness box. The History of England from the Accession of James II. |Thomas Babington Macaulay