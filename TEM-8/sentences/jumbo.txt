On jumbo screens at each end of the hall, we watch our votes being tabulated — arrived at collectively, one per table. Ann Dowd, the twisted Aunt Lydia of ‘Handmaid’s Tale,’ beguiles in a new take on Ibsen’s ‘Enemy of the People’ |Peter Marks |July 1, 2021 |Washington Post 

Such declarations are par for the course here, and their mixture of mumbo-jumbo and outright certainty is enough to make one frequently chuckle. The Real-Life ‘Demonic’ Murder That Inspired ‘The Conjuring’ |Nick Schager |June 11, 2021 |The Daily Beast 

Cleaned of any shell, jumbo lump crab is first dredged in a mix of eggs, mayonnaise, lemon juice and hot sauce. Get your Maryland crab fix at the Point Crab House, along with ace service and a water view |Tom Sietsema |May 14, 2021 |Washington Post 

“Hi Mommy, Y in the sky,” he says, expecting me to respond “T in the sky”—clearly mumbo-jumbo to outsiders, though it makes perfect sense to me. The toll pandemic isolation took on me and my autistic son |Lisa Wolfson |May 1, 2021 |Quartz 

All the market segments covered by our subindexes increased over the month, notably government and jumbo indexes. Fixed mortgage rates upward march halted as they fall for the first time in 7 weeks |Kathy Orton |April 8, 2021 |Washington Post 

As the driver bios appeared on the jumbo screen, I flashed a toothy grin after noticing that two of them were women. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

Please go to BobQ2014.com for details and all that attorney mumbo-jumbo. ‘Blow Your Balls Off’ Senate Candidate Wants to Give Away a Free Car |Olivia Nuzzi |April 18, 2014 |DAILY BEAST 

They're always trying to upsell you on that jumbo popcorn bucket at the movies. ‘Catching Fire’ Review: Bigger, More Polished, and Just Another Popcorn Flick |Kevin Fallon |November 14, 2013 |DAILY BEAST 

This was appropriate because just like her jumbo soda, her twenty minute speech was filled with empty calories. Sarah Palin's Big Gulp of a Speech |Ben Jacobs |March 16, 2013 |DAILY BEAST 

It's really twisted and sick mumbo-jumbo to try to pass this off as part of God's plan. Akin's Permanence, Huckabee's Sick Mumbo-Jumbo |Michael Tomasky |August 21, 2012 |DAILY BEAST 

He had leaped very smartly to this point of vantage, nevertheless he found Jumbo there before him, chattering worse than ever! Hunting the Lions |R.M. Ballantyne 

Had his legs been longer, Fred Greenwood would have pronounced him the equal of Jumbo himself. Two Boys in Wyoming |Edward S. Ellis 

Thereupon he asked me, even as you did some time ago, what I meant by Mumbo Jumbo? Lavengro |George Borrow 

Me Jumbo—come to lay breakfast, and cappen say you hab what you like ask for, especially someting nice for de young lady. The Missing Ship |W. H. G. Kingston 

They kept up the jumbo sail, as the main jib is called; they reefed the foresail down to its smallest compass. Blow The Man Down |Holman Day