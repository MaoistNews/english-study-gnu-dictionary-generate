This year, the brand’s designers took a hacksaw to the once-traditional nose and tail, adding a more angular, geometric look. Runner-Up Review: The Snowboards That Almost Made Our 2022 Winter Buyer’s Guide |agintzler |October 26, 2021 |Outside Online 

They dress in clothing from the flophouse lost-and-found and are groomed with a hacksaw and gravel rake. Up to a Point: They Made Me Write About Lena Dunham |P. J. O’Rourke |December 13, 2014 |DAILY BEAST 

But Switzer, one of college football's finest hacksaw butchers, still knew a good cut of meat. The Greatest Football Player That Never Was |Buzz Bissinger |February 10, 2011 |DAILY BEAST 

In an emergency an ordinary hacksaw blade may be made to serve very acceptably as a paper perforator. The Boy Mechanic, Book 2 |Various 

Still one might be nicked with a hacksaw and left to break with the shock when the next log ran down the slide. The Girl From Keller's |Harold Bindloss 

The rudder-post should be a piece of brass rod so thick that it can be split with a hacksaw. Boys' Book of Model Boats |Raymond Francis Yates 

The hub is clamped between two boards placed in the vise, and a hacksaw is used to cut a slot in the hub. Boys' Book of Model Boats |Raymond Francis Yates 

With a hacksaw this is cut off in a sloping direction with an angle to correspond with the slope in the bottom of the dry-dock. Boys' Book of Model Boats |Raymond Francis Yates