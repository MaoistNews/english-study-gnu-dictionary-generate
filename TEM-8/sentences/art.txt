The smaller notebook options would make great travel art journals. Notable notebooks for writing and drawing |PopSci Commerce Team |September 17, 2020 |Popular-Science 

On some level, it’s an indication that “art is reflecting the times,” Renz says. ‘Antebellum’ tackles the past head on in an effort to ‘move forward’ |radmarya |September 17, 2020 |Fortune 

This week, Folger Theatre is premiering its virtual project “Encores,” an initiative to help provide more online content for the community while most arts institutions remain closed during the pandemic. D.C. theater scene adapts with films, concerts, and more |Patrick Folliard |September 17, 2020 |Washington Blade 

I learned to love the art of the search, the stories emerging from decomposing seats, revealing lost glasses and keys. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

This social art is generally lost during months of drinking at home, unless you have company, but the pandemic had forbidden even that possibility, and weeknight drinking alone became an alarming habit. The Last Bar Standing? |Eugene Robinson |September 15, 2020 |Ozy 

The pulps brought new readers to serious fiction, making it less intimidating with alluring art and low prices. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 

Cold War fears could be manipulated through misleading art to attract readers to daunting material. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 

In “Steal This Episode,” the filmmaker denounces Homer Simpson as an “enemy of art.” Here’s the Lost Judd Apatow ‘Simpsons’ Episode, Penned by Judd Apatow |Asawin Suebsaeng |January 6, 2015 |DAILY BEAST 

But those watching Selma were judging a work of cinematic art. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

I spent time yesterday listening to the music you made, and looking at the art you created. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

Woman is mistress of the art of completely embittering the life of the person on whom she depends. Pearls of Thought |Maturin M. Ballou 

Many of these have been seen in the Corcoran Art Gallery and in other public exhibitions. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Adequate conception of the extent, the variety, the excellence of the works of Art here heaped together is impossible. Glances at Europe |Horace Greeley 

So it commands the other sciences in all the wonderful and hidden things of nature and art (pp. 510-511). The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

While residing in Brussels these two artists began to collect works of art for what is now known as the Mesdag Museum. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement