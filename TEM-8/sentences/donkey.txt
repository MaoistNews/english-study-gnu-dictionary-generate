If memory serves, donkey dung doesn’t broadcast itself with anything like the strength of horse dung. Readers react to the history of plate tectonics, pandas rolling in poop and more |Science News Staff |February 21, 2021 |Science News 

These are your site’s biggest donkeys, the pages with the highest number of impressions but deliver lower than expected CTR for their ranking position. Five hacks to enhance your organic CTR and rankings in SERPs |Karl Tablante |January 27, 2021 |Search Engine Watch 

My husband wrote a note apologizing that the camel and donkey mates were missing. The pandemic gave me the time to finally clean out my shameful attic. Here’s what I learned. |Jura Koncius |January 21, 2021 |Washington Post 

Cars, buses, truck—even motorcycles, bicycles and donkeys—may be required to get the vaccine to rural areas. Airlines are facing ‘mission of the century’ in shipping COVID vaccines |McKenna Moore |December 1, 2020 |Fortune 

Sixteen contenders — mostly dogs, with one donkey, a rooster and a cat in the mix — fought hard for the title of mayor. After a tight race, a French bulldog was elected mayor of this small Kentucky town |Sydney Page |November 9, 2020 |Washington Post 

Nearby a family of Turkish Kurds busied themselves in their fields piling vegetables onto a donkey-drawn cart. Impotent U.S. Airstrikes, Passive Turks and an ISIS Triumph |Jamie Dettmer |October 3, 2014 |DAILY BEAST 

In the second instance, it is a man and his wife pleading from atop a donkey. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

He patrolled on foot, and Improvised Explosive Devices (IEDs) filled the donkey paths that crisscrossed the wadis and hills. How a Thumb-Sized Gauge Is Revolutionizing Traumatic Brain Injuries |Brian Castner |March 23, 2014 |DAILY BEAST 

For the donkeys who have long felt that Donkey Ball should not be real, 2014 earns an A-. How Is 2014 Going? An Early Review |Kelly Williams Brown |January 4, 2014 |DAILY BEAST 

He was painting the Sistine Chapel, and he was angry at one of the bishops or cardinals, so he painted him in with donkey ears. Interview: T Bone Burnett, the Coen Brothers’ Music Guru |Andrew Romano |December 13, 2013 |DAILY BEAST 

His donkey stumbled—it was natural enough, seeing that the reins hung loose and his feet had somehow left the stirrups. The Wave |Algernon Blackwood 

His donkey had gone lame, he abandoned it to the boys behind, he climbed in to drive with Lettice. The Wave |Algernon Blackwood 

Ibrahim stopped his song to sigh, and struck his donkey lightly under the right ear, causing it to turn sharply to the left. Bella Donna |Robert Hichens 

And now Ibrahim struck his donkey again, and they went on rapidly towards the Libyan mountains. Bella Donna |Robert Hichens 

Could all these people read her mind and follow the track of her distastes and desires, even the dragomans and the donkey-boys? Bella Donna |Robert Hichens