It was originally slated for release in late 2019, said three people familiar with the process, and was delayed in part by the coronavirus pandemic. Erased From the Trump Administration’s Draft of a Key Foreign Aid Policy: Any Mention of LGBT People |by Yeganeh Torbati |August 20, 2020 |ProPublica 

Municipal elections are slated to take place later this year, and electoral courts could use the new law to investigate political parties for improper accumulation and use of data, according to Zanatta of Data Privacy Brasil. Brazil is sliding into techno-authoritarianism |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Lawmakers have known for months that the payment was slated to expire at the end of July, but that deadline came and went. What Economists Fear Will Happen Without More Unemployment Aid |Amelia Thomson-DeVeaux |August 11, 2020 |FiveThirtyEight 

The Heat were set to face a below-average strength of schedule if the regular season had continued apace, but they will now face the league’s second-toughest seeding-game slate. Who’s Who In The NBA Bubble: The Potential Party-Crashers |Jared Dubin |July 23, 2020 |FiveThirtyEight 

A largely new slate of MTS board members is pushing for reform after years of public complaints about the agency’s aggressive ticketing. The Police Reform Push Comes for MTS |Lisa Halverstadt and Jesse Marx |June 25, 2020 |Voice of San Diego 

In any case, I welcome the conversation as part of the review of the upcoming slate that we're doing tomorrow. Inside Sony’s ‘Pineapple Express 2 Drama’: Leaked Emails Reveal Fight Over Stoner Comedy Sequel |William Boot |December 21, 2014 |DAILY BEAST 

Instead of wallowing in comedy exile, Slate was earning a book deal. The Curious Little Shell That Restarted Jenny Slate’s Career |Luke Hopping |December 15, 2014 |DAILY BEAST 

So with the doors of late night closed to her, Slate had to scale down her ambitions to raise her profile. The Curious Little Shell That Restarted Jenny Slate’s Career |Luke Hopping |December 15, 2014 |DAILY BEAST 

Less than a minute into her big break, Slate let slip a highly audible F-bomb instead of the scripted “freaking.” The Curious Little Shell That Restarted Jenny Slate’s Career |Luke Hopping |December 15, 2014 |DAILY BEAST 

Will you work with Jenny Slate, John Daly, John Mulaney, etc. moving forward? The Zany Shades of Nick Kroll |Abby Haglage |December 15, 2014 |DAILY BEAST 

The tower has four clock faces, pinnacles at the angles, and a steep slate roof and is 120 feet high. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Slate slabs were originally tried for sleepers on the Birmingham and London line. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

John Tubbs was one day doing his sums, when little Sam Jones pushed against him; and down went the slate with a horrid clatter. The Nursery, July 1873, Vol. XIV. No. 1 |Various 

Dinah had left her slate on 10 a chair, and dropped her algebra on the carpet, at the sound of Norahs voice below the window. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

Why send her a picture of a slate-colored cow when a herd of Durhams pastures every day right under her eye? The Soldier of the Valley |Nelson Lloyd