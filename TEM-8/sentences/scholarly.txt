Her work is dedicated to encouraging the application of scholarly knowledge to important societal problems. How to Build Trust in Covid-19 Vaccines - Issue 93: Forerunners |Ramanan Laxminarayan, Susan Fitzpatrick, & Simon Levin |December 9, 2020 |Nautilus 

The group also began a crowdsourced “syllabus” covering scholarly and creative writings addressing all things pandemic. A COVID-19 time capsule captures pandemic moments for future researchers |Sujata Gupta |December 2, 2020 |Science News 

A year ago, my daily reading was largely determined by what curators considered to be important and relevant, catalogue essays and scholarly articles related to upcoming exhibitions. Transformed by crisis, arts criticism may never be the same. And that’s a good thing. |Philip Kennicott |November 29, 2020 |Washington Post 

Her mother completed high school and encouraged Ginsburg to be independent and scholarly. Ruth Bader Ginsburg’s passing feels like millions of women have lost their bubbie |jakemeth |September 22, 2020 |Fortune 

Calabi and Yau would have had more right than anyone to interject and suggest something else, but as Yau tells it, they were both proud and happy to watch their names spread together through scholarly publications and through popular culture. Why Mathematicians Should Stop Naming Things After Each Other - Issue 89: The Dark Side |Laura Ball |September 2, 2020 |Nautilus 

In the meantime, much of the book is already available online, and scholarly criticism has already started to trickle in. Jesus Christ, Baby Daddy? |Candida Moss |November 12, 2014 |DAILY BEAST 

Most notably written about by Dr. Gillian Frank in this scholarly retrospective. Of Gamers, Gates, and Disco Demolition: The Roots of Reactionary Rage |Arthur Chu |October 16, 2014 |DAILY BEAST 

For thousands of years, people have had scholarly debates about what constitutes beauty. Hello, ‘Gorgeous’: Grit and Glamour In San Francisco |Emily Wilson |June 20, 2014 |DAILY BEAST 

The released photos of Kim Jong-un as a scholarly, military-minded youth reinforce this ideology. Such a Sweet Little Dictator: Kim Jong-un and North Korea’s Child Cult |Scott Bixby |April 24, 2014 |DAILY BEAST 

Yet, once again, the broader scholarly community has been less enthusiastic about the discovery. The ‘Gospel of Jesus’s Wife’ is Still as Big a Mystery as Ever |Candida Moss |April 13, 2014 |DAILY BEAST 

His scholarly and linguistic attainments and his varied travels, fitted him well for the task. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

He had scholarly tastes and business ability in pretty equal parts. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Poetry was scarce but many scholarly articles, often historically inclined, were written. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Men of refinement and high culture adopted it rather as an article of scholarly adornment. A Cursory History of Swearing |Julian Sharman 

This is the best account of the treasures of the buried city that has appeared in English, at once interesting and scholarly. The Private Life of the Romans |Harold Whetstone Johnston