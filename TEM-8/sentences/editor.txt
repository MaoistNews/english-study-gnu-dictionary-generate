The other piece came this morning from Iain Morris, news editor at telecoms trade outlet Light Reading, who noted that early deployers of 5G in East Asia are still waiting to see results. In the 5G race, the prize remains unclear |David Meyer |August 20, 2020 |Fortune 

Their ringleader was the magazine editor and writer Dan Okrent. The Economics of Sports Gambling (Ep. 388 Rebroadcast) |Stephen J. Dubner |August 20, 2020 |Freakonomics 

This week on Deep Tech, she joins our editor-in-chief, Gideon Lichfield, to discuss how consumers’ private data is protected in the world’s largest surveillance state. Podcast: Want consumer privacy? Try China |Michael Reilly |August 19, 2020 |MIT Technology Review 

My editors are forever asking me to take the long Twitter threads I write and turn them into articles here at FiveThirtyEight. Our Election Forecast Didn’t Say What I Thought It Would |Nate Silver (nrsilver@fivethirtyeight.com) |August 17, 2020 |FiveThirtyEight 

Hall and other editors have insisted, however, that these endorsements are not the Union-Tribune’s views. Politics Report: Who Will Get the Midway Rose? |Scott Lewis and Andrew Keatts |August 15, 2020 |Voice of San Diego 

Vicky Ward was a contributing editor to Vanity Fair for 11 years. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

Her name was Courtney, and she was a fashion editor for magazines like Photoplay, Screenland, Silver Screen. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

“James Woods refuses to toe the Hollyweird line,” Twitchy managing editor Lori Ziganto told The Daily Beast in an email. How James Woods Became Obama’s Biggest Twitter Troll |Asawin Suebsaeng |December 31, 2014 |DAILY BEAST 

The film had been with George Tomasini, the editor, and Hitch hadn't seen it in ten days. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

My editor called and said, “Do a column on this Lena Dunham flap!” Up to a Point: They Made Me Write About Lena Dunham |P. J. O’Rourke |December 13, 2014 |DAILY BEAST 

He was the successor of Dr. Franklin as editor, and entered upon the business in 1763. The Every Day Book of History and Chronology |Joel Munsell 

Thomas Barnes, principal editor of the Times newspaper, died in London, aged 56. The Every Day Book of History and Chronology |Joel Munsell 

In the Railway Official Gazette was a column devoted to short reviews of new books which were sent to the editor. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

He is celebrated as the editor of Shakspeare, and published several biographies. The Every Day Book of History and Chronology |Joel Munsell 

For an account of their missionary work in Canada, see Editor's Introduction, vol. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various