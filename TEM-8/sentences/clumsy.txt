Equipped with foreign DNA, the little guys nevertheless seemed totally normal, nosing around the lab and running everywhere without obvious clumsy stumbles. How Scientists Grew Human Muscles in Pig Embryos, and Why It Matters for Organ Transplants |Shelly Fan |April 6, 2021 |Singularity Hub 

The colonizers spun this through their clumsy tongues and came up with “carel” as a sweeping term for all the food the Indigenous people ate. The Violent Rise of Curry |Shaan Merchant |March 26, 2021 |Ozy 

Each of Liverpool’s first two goals received a Leeds reply within 10 minutes, but a clumsy lunge by Rodrigo Moreno gave Liverpool’s Mohamed Salah the opportunity to convert the winning penalty shot. Leeds United Is Scoring (And Getting Scored On) Like Nobody Else In The Premier League |Nicholas Som |March 10, 2021 |FiveThirtyEight 

Reynolds and Mani are very good at playing hapless and clumsy with endearing rapport, and their general helplessness comes off not as a stale joke but as a concerning hint of things to come. 'Save Yourselves!' Is a Surprisingly Fun Disaster Movie |Erin Berger |October 19, 2020 |Outside Online 

Here’s a practical guide to finding the glass out there for you, whether you’re frugal, clumsy, or just running out of kitchen space. Sommeliers Pick the Best Wine Glasses for Every Scenario |Priya Krishna |October 16, 2020 |Eater 

He must be pretending to be something, striking some kind of clumsy tribal note. For a President Today, Talkin' Down Is Speaking American |John McWhorter |August 7, 2014 |DAILY BEAST 

But Silva, hapless Silva, got his merely for tangling with the Colombian goalkeeper in a clumsy melee of limbs. Brazil and Colombia Bring the Ugly Game |Tunku Varadarajan |July 4, 2014 |DAILY BEAST 

Season one of OITNB chronicled her clumsy, fumbling attempts to get her legs under her so she could run for safety. ‘Orange Is the New Black’ Season Two Is More Bingeworthy Than the First |Kevin Fallon |May 16, 2014 |DAILY BEAST 

Ignore the clumsy comparisons of the Syria War and the Balkans War. No One Understands Syria, But Everyone Is Choosing Sides |Michael Moynihan |September 4, 2013 |DAILY BEAST 

Rather, after weeks of clumsy diplomacy, they consciously led their nations into battle. The Utterly Pointless First World War |Michael F. Bishop |May 22, 2013 |DAILY BEAST 

Scientists tell us that from the point of view of optics the human eye is a clumsy instrument poorly contrived for its work. The Unsolved Riddle of Social Justice |Stephen Leacock 

With which magnanimous sentiment he turned on his clumsy heel, and entered his apartment again. Checkmate |Joseph Sheridan Le Fanu 

They merely used such instruments as fate offered, however trivial, however clumsy. The Wave |Algernon Blackwood 

He loses sight of the supreme fact that after all, in its own poor, clumsy fashion, the machine does work. The Unsolved Riddle of Social Justice |Stephen Leacock 

When the owner can afford it, an ample supply of cushions and shawls makes the clumsy vehicle more comfortable for its occupant. Our Little Korean Cousin |H. Lee M. Pike