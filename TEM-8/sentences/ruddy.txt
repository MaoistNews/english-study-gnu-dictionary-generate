It was a regular feature for us to be scared out of our minds, because the hill was a layer of ice and it was ruddy. U.S. Ski Jumping Is Looking For More Friends In High Places |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |February 18, 2022 |FiveThirtyEight 

His complexion was ruddy, his fair skin burnt from time in the sun. Watching ISIS Come to Power Again |Elliot Ackerman |September 7, 2014 |DAILY BEAST 

“Some years ago I introduced Dick to former President Bill Clinton,” Ruddy wrote. Scaife v. Clinton and the Dangers of Demonization |John Avlon |July 7, 2014 |DAILY BEAST 

One of the boys, a stocky ruddy-faced blond teen, vanished after he and Middleton got drunk on rubbing alcohol. The Bone Collectors Get to Work at Florida’s Dozier School |Christine Pelisek |September 6, 2013 |DAILY BEAST 

Yet now, Obama has need for more—much more—than a jolly, ruddy dude who serves as a racial palliative. Hillary in 2012 |Tunku Varadarajan |August 12, 2010 |DAILY BEAST 

By July 23, Karadzic looked like himself, albeit a bit thinner, his skin shallow compared to its wartime ruddy glow. The Rebel at The Hague |Ellie Tzortzi |July 22, 2009 |DAILY BEAST 

Tom himself was burly, ruddy, broad, and rather above middle size. Hunting the Lions |R.M. Ballantyne 

He was a big-bodied, big-hearted, ruddy-faced, farmerlike man of fifty or so; and the service was proud of him. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

He was a big man, and looked bigger than he was; good-looking too; ruddy, portly, well-dressed and formal. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

He stepped hastily back, his cheeks, before so fresh and ruddy, were now blanched with a deadly pallor. The World Before Them |Susanna Moodie 

A cheerful fire was roaring up the great chimney, and she was literally basking in the warmth the ruddy blaze diffused around. The World Before Them |Susanna Moodie