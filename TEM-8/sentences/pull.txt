Finally, the gravitational pull of the whole Milky Way galaxy can lure away some stars. Milky Way’s tidal forces are shredding a nearby star cluster |Ken Croswell |August 18, 2020 |Science News For Students 

Stellar gatherings such as the Hyades, known as open star clusters, are born with hundreds or thousands of stars that are held close to one another by their mutual gravitational pull. The star cluster closest to Earth is in its death throes |Ken Croswell |July 24, 2020 |Science News 

As vehicle sales cooled over in recent years, EVs have resisted the pull of gravity, selling roughly a million new units every six months since 2018. Electric cars got crushed in 2020, but next year could be their best |Michael J. Coren |July 16, 2020 |Quartz 

The rider lies on a bed, head pointing toward the center of the carousel, which spins to exert a horizontal centri­fugal force out toward the feet that’s as strong as the downward pull of gravity. What will astronauts need to survive the dangerous journey to Mars? |Maria Temming |July 15, 2020 |Science News 

Those waves, called tidal waves, are created by the gravitational pull of the sun or moon. Scientists Say: Tsunami |Carolyn Wilke |June 15, 2020 |Science News For Students 

It is a spy series at its core, but you guys never really pull from the headlines. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Just how many fake nodes would be needed in order to pull off a successful Sybil attack against Tor is not known. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 

Botala remembers that the rebels would pull into the island, loot what they could, and then take the haul back to Stanleyville. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

All it took was a good idea, and OK Go had one—and the drive to pull it off. OK Go Is Helping Redefine the Music Video For the Internet Age |Lauren Schwartzberg |December 15, 2014 |DAILY BEAST 

And we do mean drunken—in the keep your kids at home, pull the shades kind of drunken. Before the Bros, SantaCon Was as an Anti-Corporate Protest |David Freedlander |December 12, 2014 |DAILY BEAST 

Strange to say, the silken cord yielded to the first pull, as if nothing had been wrong with it at all! The Giant of the North |R.M. Ballantyne 

Never grasp a Fern plant from above and try to pull it away, as this will be almost sure to result in damage. How to Know the Ferns |S. Leonard Bastin 

"I ordered you not to come," said Aspinall: "I can still pull a trigger, Sir," replied the man. Gallipoli Diary, Volume I |Ian Hamilton 

This harmless image of a fierce beast Yung Pak would pull about the floor with a string by the hour. Our Little Korean Cousin |H. Lee M. Pike 

To pull through such a siege, the old settlers usually did much better than the new. The Homesteader |Oscar Micheaux