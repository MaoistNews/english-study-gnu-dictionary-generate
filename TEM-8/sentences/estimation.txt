Based on Watanabe and colleagues’ estimations, Baikal seals may be getting about 20 percent of their daily calorie requirements just from amphipods. Using comb-shaped teeth, Baikal seals feed on tiny crustaceans like whales do |Jake Buehler |December 11, 2020 |Science News 

By his estimation, Mike Tyson shed about 100 pounds for his nostalgia-filled return to the boxing ring Saturday night. Mike Tyson fought for the first time in 15 years. So much had changed. |Gene Wang |November 29, 2020 |Washington Post 

If intensity estimations are correct, it looks to have made landfall as a major Category 3 hurricane. PM Update: Gusty showers overnight. Clear but cooler on Monday. |Greg Porter |November 22, 2020 |Washington Post 

The team made new estimations of the reptiles’ weight, wing shape and wingspan, and then simulated how those features might translate into flapping, gliding or launching. Bat-winged dinosaurs were clumsy fliers |Carolyn Gramling |October 22, 2020 |Science News 

Tomi Lahren, the 28-year-old South Dakota native who is the host of Final Thoughts on Fox Nation, is equal parts telegenic, mouthy and, in her estimation at least, “relentless.” Go Behind the Tomi Lahren Interview on ‘The Carlos Watson Show’ |Eugene Robinson |August 31, 2020 |Ozy 

John le Carré is notorious for taking the establishment for which he once toiled as a spy at its lowest estimation. Britain’s KGB Sugar Daddy |Michael Weiss |March 7, 2014 |DAILY BEAST 

As for the estimation of Snowden among the Russian opposition, you must realize what his journey looks like in our eyes. Snowden’s Revelations Shouldn’t Distract from Putin’s Brutal Rule |Garry Kasparov |January 29, 2014 |DAILY BEAST 

But in my estimation nothing really seems too off the charts. You Should Have Been There: Dispatches From Miami Art Basel |Anthony Haden-Guest |December 8, 2013 |DAILY BEAST 

And my estimation of that accomplishment grew and grew the more I put pen to paper. ‘Dear Mr. Watterson’ Celebrates the Glory of Calvin and Hobbes on Film |Malcolm Jones |November 16, 2013 |DAILY BEAST 

So he deserves some punishment, but he has already received (in my estimation) excessive amounts of punishment. Ron Paul Hits Reddit |The Daily Beast |August 22, 2013 |DAILY BEAST 

An estimation of the solids, therefore, furnishes an important clue to the functional efficiency of the kidneys. A Manual of Clinical Diagnosis |James Campbell Todd 

Quantitative estimation does not furnish much of definite clinical value. A Manual of Clinical Diagnosis |James Campbell Todd 

Quantitative estimation of the total sulphates yields little of clinical value. A Manual of Clinical Diagnosis |James Campbell Todd 

We will now mention a fact which in the estimation of all true lovers of these fine violins is to be greatly regretted. Violins and Violin Makers |Joseph Pearce 

Madame Torvestad was in the habit of writing many letters, which were held in much estimation by the Brethren around. Skipper Worse |Alexander Lange Kielland