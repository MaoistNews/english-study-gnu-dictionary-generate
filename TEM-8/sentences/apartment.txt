Police also reportedly mistook Taylor’s car, which was parked outside her apartment that night, as the drug dealer’s car, according to the lawsuit. The policing reforms in the Breonna Taylor settlement, explained |Fabiola Cineas |September 17, 2020 |Vox 

On top of the cost of filters, lower-end homes or apartments may have more leaks around windows and doors that let in contaminants. Wildfire smoke and COVID-19 are a one-two punch for indoor air quality across the U.S. |dzanemorris |September 17, 2020 |Fortune 

The collapsible option makes it great for space conscious rooms or in smaller apartments and dorms. Attractive laundry hampers that make your dirty clothes look a little better |PopSci Commerce Team |September 16, 2020 |Popular-Science 

A “target area” could be a park, an apartment complex or the bulk of the entire city. While We’re Rethinking Policing, It’s Time to End Gang Injunctions |Jamie Wilson |September 15, 2020 |Voice of San Diego 

It’s not easy working from home with kids, or being alone in your apartment. Are you ready to start traveling for work again? TripActions’ CEO is banking on it |Michal Lev-Ram, writer |September 15, 2020 |Fortune 

He was born in an apartment above the grocery store owned by his immigrant parents in South Jamaica, Queens. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 

“Bodegas, private residences, apartment buildings, you name it,” the investigator says. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

At 24, I slipped on the ice outside of my Michigan apartment. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

Magazines are the only thing in my apartment that qualify as clutter. I’m a Digital Hoarder |Lizzie Crocker |December 17, 2014 |DAILY BEAST 

And there are a few nice things buried beneath the rubble that I could use in my apartment. I’m a Digital Hoarder |Lizzie Crocker |December 17, 2014 |DAILY BEAST 

To the invitation to precede him she readily responded, and, with a bow to the Seneschal, she began to walk across the apartment. St. Martin's Summer |Rafael Sabatini 

The others stood silent till they heard the outer door of the apartment close behind him. Confidence |Henry James 

It was a large square apartment, very lofty and very naked-looking. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

With which magnanimous sentiment he turned on his clumsy heel, and entered his apartment again. Checkmate |Joseph Sheridan Le Fanu 

In a few minutes he was alone, in a magnificent apartment, where every tranquillizing luxury invited to repose. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter