If primary producers captured one percent of the incipient solar energy, then those were the chips on the table to get passed around. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

We don’t actually come together at the same table until the script is complete. ‘Antebellum’ explores truths of our ugly past |Brian T. Carney |September 16, 2020 |Washington Blade 

Net-neutrality advocates took that as a victory, but it is only now that the CJEU has confirmed that zero rating is off the table. The EU’s top court just closed a major loophole in Europe’s net-neutrality rules |David Meyer |September 15, 2020 |Fortune 

I think we have a resolution that will allow us to process next week and put protein on America’s table. Emails Show the Meatpacking Industry Drafted an Executive Order to Keep Plants Open |by Michael Grabell and Bernice Yeung |September 14, 2020 |ProPublica 

Our tables ended up being next to each other’s, so we kept talking. Making Poly Less of a Pain in the Ass |Eugene Robinson |September 14, 2020 |Ozy 

He gets up and goes over to their table and introduces himself, and he says, ‘Hello, I’m Oliver Reed. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Joel Osteen wants to talk about muting your cell phone at the dinner table. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

A sepia photo shows him as a young boy, head in his hands, with a large book open at a bar table. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

That means any response that could result in physical damage inside North Korea is off the table. Cyberwar on North Korea Could Be Illegal |Shane Harris |December 23, 2014 |DAILY BEAST 

A table creaking under the weight of a Christmas banquet, a classic celebration of binge eating and drinking. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

Old Mrs. Wurzel and the buxom but not too well-favoured heiress of the house of Grains were at the head of the table. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

A small book, bound in full purple calf, lay half hidden in a nest of fine tissue paper on the dressing-table. Hilda Lessways |Arnold Bennett 

There were at least a dozen ladies seated round the big table at the Parsonage. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Miss Smith immediately rises from the table, puts up her dear little mouth to her papa to be kissed. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

He sighed as he laid the papers on the table; for he thought the task would be a harder one than even his own immolation. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter