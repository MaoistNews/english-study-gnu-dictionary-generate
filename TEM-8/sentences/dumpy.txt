I find it hard to put myself in the head of a non-book-reader for this, but so far it’s a bit lore-dumpy, and too many of the characters have yet to come into focus. Andrew and Lee dissect The Wheel of Time’s television premiere |Andrew Cunningham |November 19, 2021 |Ars Technica 

Lupoi, on the other hand, wore a dumpy black coat and generally looked like a Brooklyn schlub. Mafia’s Cocaine-in-a-Can Bust |Michael Daly |February 12, 2014 |DAILY BEAST 

Every study ever performed has shown that the fit and lean outlive the dumpy and diffident every time. Cool It on the CrossFit: What’s Rhabdomyolysis? |Kent Sepkowitz |October 11, 2013 |DAILY BEAST 

After more than two decades of merchandising her looks, Elizabeth was anxious about appearing dumpy. Liz Taylor's Secret Life |William J. Mann |October 19, 2009 |DAILY BEAST 

Behold a dumpy, comfortable British paterfamilias in a light flannel suit and a faded sun hat. God and my Neighbour |Robert Blatchford 

With his dumpy figure in the long coat, and his round face under the tall hat, the little man was irresistible. The Shepherd of the Hills |Harold Bell Wright 

"'Tis a matter of taste, but I couldn't fancy corpses as furniture," observed the Dumpy Philosopher. A Drake by George! |John Trevena 

The chain kinked time and again as he groped with the undermost hand for the openings; his dumpy, pudgy form writhed grotesquely. The Escape of Mr. Trimm |Irvin S. Cobb 

A dumpy, rosy-faced woman was Mrs Jeal, with a pair of extremely wicked black eyes which snapped fire when she was angered. The Pagan's Cup |Fergus Hume