After a while, you start to realize that they were just tools used by the leaders and the Politburo. Putin’s Hockey Pal Tells All: Slava Fetisov on ‘Red Army,’ Soviet Nostalgia, and What Drives Putin |Marlow Stern |October 9, 2014 |DAILY BEAST 

The reason that our computers are built with a politburo architecture is that it is efficient at doing very boring tasks. Can Robots Fall in Love, and Why Would They? |Jimmy So |December 31, 2013 |DAILY BEAST 

Zhou, a staunch supporter of Bo, was rumored to have designated Bo as his successor inside the Politburo Standing Committee. China’s Corruption Purge: The Fall of Zhou Yongkang |Wenguang Huang, Ho Pin |December 12, 2013 |DAILY BEAST 

These leaders, and others who questioned the Politburo's massive gamble are now in a position to gloat over being right. Hamas-Egypt Tensions Take Toll On Gaza |Hussein Ibish |July 25, 2013 |DAILY BEAST 

Four of the seven Politburo Standing Committee members, including Xi, are princelings. China’s Risky Path, From Revolution to War |Cheng Li |January 20, 2013 |DAILY BEAST 

The Politburo set the general policy guidelines and directives. Area Handbook for Albania |Eugene K. Keefe 

The once-powerful Politburo and its sub-committees became obsolete. The Ambassador |Samuel Kimball Merwin 

Family connections played a key role in the composition of the Politburo in 1970. Area Handbook for Albania |Eugene K. Keefe 

Similar family relationships existed between the other Politburo members. Area Handbook for Albania |Eugene K. Keefe 

The commission's function was to elaborate the Politburo's directives on reforming the school system. Area Handbook for Albania |Eugene K. Keefe