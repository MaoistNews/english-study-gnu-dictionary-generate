Customers nonetheless took to Twitter to vent about the latest delays, and mock Robinhood’s performance. Robinhood app stumbles amid surge in Apple, Tesla trading volume |Jeff |September 1, 2020 |Fortune 

It was transformed into a “mock marketplace… with everything from Andy Warhol shopping bags screen printed with Campbell’s Soup cans, to Tom Wesselman plastic turkeys to illustrate the ideals of the American consumption,” she says. Summer Is Forever With This Corn-on-the-Cob Chair |Emma Orlow |August 28, 2020 |Eater 

Four months after NASA chose three teams as finalists, one of the teams, led by Jeff Bezos’ rocket company Blue Origin, on Thursday delivered a 40-foot tall mock-up lander to the space agency for testing. Here’s what NASA’s next moon lander may look like |Aaron Pressman |August 21, 2020 |Fortune 

Locals wound up crowding the park and shouting obscenities and mocking officials. Morning Report: Rural Districts Still Scrambling to Prepare for Online Learning |Voice of San Diego |August 12, 2020 |Voice of San Diego 

Thompson mocked up multiple versions and tested how people interpreted them with help from the Science News staff plus some college roommates. Data visualizations turn numbers into a story |Nancy Shute |August 3, 2020 |Science News 

She jumps on his back, mock-choking him and covering his eyes. Sia and Shia LaBeouf’s Pedophilia Nontroversy Over ‘Elastic Heart’ |Marlow Stern |January 9, 2015 |DAILY BEAST 

Satirists are reliant ultimately on the very establishment they mock. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

He's so white he's almost mock-white, and so are his jerky, long-necked, mechanical-man movements. The Stacks: Pauline Kael's Talking Heads Obsession |Pauline Kael |November 22, 2014 |DAILY BEAST 

Arab standup comics, and even an Iraqi TV show, regularly mock ISIS mercilessly. Middle East Goes Monty Python on ISIS |Dean Obeidallah |October 29, 2014 |DAILY BEAST 

Dogs were used in the interrogations, and the accused were subjected to mock executions. The Myth of the Central Park Five |Edward Conlon |October 19, 2014 |DAILY BEAST 

For others life is but a foolish leisure with mock activities and mimic avocations to mask its uselessness. The Unsolved Riddle of Social Justice |Stephen Leacock 

Now men laughed at him, pointed to him with their fingers, and made their children mock and hoot the penniless insolvent. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Gone, too, is the hamlet of Garratt, whose mock elections of a Mayor caused such convivial excitement a century ago. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Even the mock hero, the good young man who tries to raise himself, has something comic in him. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Dispense with ornaments altogether rather than wear mock jewelry. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley