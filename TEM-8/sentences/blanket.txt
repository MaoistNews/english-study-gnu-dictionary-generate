Among the 160 items, you’ll get a CPR mask, metal tweezer, eye wash, emergency blanket, knuckle adhesive bandages, and plenty of cotton tip applicators. The best first aid kits for staying safe and prepared |PopSci Commerce Team |September 4, 2020 |Popular-Science 

Whereas something like Starlink seeks to blanket the planet with many more satellites orbiting at a closer distance, satellites in higher orbits have a larger communications footprint, meaning each can cover more of the Earth. Satellite mega-constellations risk ruining astronomy forever |Neel Patel |September 2, 2020 |MIT Technology Review 

In recent years, artists have begun asserting clauses that exclude the use of songs at political rallies from the blanket licensing regimes. Trump and ‘Hallelujah’: Why it’s so hard to stop campaigns from playing songs, even when artists object |Jeff |August 31, 2020 |Fortune 

After running on an extreme sleep deficit for several pandemic months in a row, I decided to give in to the weighted blanket craze and did hours of research on which one was the right fit for me. The best things I bought in August |Rachel Schallom |August 30, 2020 |Fortune 

So, a blanket statement of just quarantine might not be enough to keep potential carriers in lockdown. The CDC’s new COVID-19 testing guidelines could make the pandemic worse |Sara Kiley Watson |August 27, 2020 |Popular-Science 

Aside from a blanket ban, social media platforms like Twitter, Facebook, and Reddit are nearly impossible to control. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

Have you noticed there are some people who would love to put a big wet blanket on all of this? Kirk Cameron Saves Christmas from Abominable Killjoys (Other Christians) |Brandy Zadrozny |November 14, 2014 |DAILY BEAST 

“He would have preferred to have been given a bed rather than a blanket on the floor,” the judge said. What It’s Like to Be Snatched by the Delta Force |Michael Daly |October 9, 2014 |DAILY BEAST 

The correct procedure in that case is obvious: you cover her with a blanket and leave her alone. ‘The Fappening’ Perpetuators Have a J.Law Come-to-Jesus Moment and ‘Cower With Shame’ |Marlow Stern |October 8, 2014 |DAILY BEAST 

However, the act that gives blanket immunity seems only to apply to licensed dealers. Victim’s Parents Sue Merchants of the Aurora Massacre |Cliff Schecter |September 17, 2014 |DAILY BEAST 

He swerved as he passed it, and, looking, saw that it was a bundle wrapped in a striped blanket. The Joyous Adventures of Aristide Pujol |William J. Locke 

And, old ink pot, tuck a horse blanket under my chin, and rub me down with brickbats while I feed! The Book of Anecdotes and Budget of Fun; |Various 

They entered a house where an apparently sick man sat cowering in a corner, wrapped in a blanket. The Courier of the Ozarks |Byron A. Dunn 

On these occasions he was wrapped in an old blanket ingrained with snuff. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Oliver retained his stool by the fire; and Barney, wrapped in a blanket, stretched himself on the floor, close outside the fender. Oliver Twist, Vol. II (of 3) |Charles Dickens