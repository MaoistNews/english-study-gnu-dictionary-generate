Much of the effort to fight the blazes has come from local villagers, who have taken the work of defending surrounding hay fields—essential for their livelihoods raising horses and cattle—into their own hands. Villagers in Siberia, Facing Wildfires and a Warming Climate, Battle to Protect their Homes |Alejandro de la Garza |August 26, 2021 |Time 

Players feed the bugsnax — half-bug, half-snack creatures — to the villagers, or Grumpuses, whose body parts turn into snacks once they’re fed. What brands can learn from LGBTQ+ storytelling in gaming |Google |August 13, 2021 |Digiday 

Threatened by local villagers and challenged by unpredictable weather and dangerous rock and ice falls, the climbers must decide whether to give up or commit fully to the quest. The Best Climbing Movies and Where to Watch Them |syadron |July 31, 2021 |Outside Online 

The villagers had assumed they would still be able to take trees for lumber, harvest their medicinal plants or sell other bits from the forest during cash emergencies. The first step in using trees to slow climate change: Protect the trees we have |Susan Milius |July 13, 2021 |Science News 

It was the villagers rising to the defense of the boy who cried wolf by pointing out that, in fact, wolves do exist. Tucker Carlson’s NSA claims are the latest example of the right wing’s rhetorical bait-and-switch |Philip Bump |July 8, 2021 |Washington Post 

So, they randomly select a poor villager and strap a bucket of rats against his chest. Game of Thrones’ 8 Most Gruesome Deaths: From The Mountain’s Exploding Head Kill to Rat Torture |Marlow Stern |June 4, 2014 |DAILY BEAST 

Despite the look of plenty, it had not been a good one, according to one villager. A Little Too Off the Beaten Path in Burma |Katya Cengel |June 2, 2014 |DAILY BEAST 

“The Taliban hate our small village of 60 homes and families,” says Sabray villager Muhammad Jan, 45. The Afghan Village That Saved Navy SEAL Marcus Luttrell |Sami Yousafzai, Ron Moreau |November 8, 2013 |DAILY BEAST 

Min is slated to be charged with the mass knifing, which injured 22 students and an 84-year-old villager. China’s Doomsday Crackdown |Melinda Liu |December 18, 2012 |DAILY BEAST 

Local media reported that suspect Min Yingjun was a local villager suffering from a mental disorder. Not Just Sandy Hook: China’s Terrifying Knife Attacks |Melinda Liu |December 15, 2012 |DAILY BEAST 

A traveller, stopped on his way by a torrent, asks a villager on the opposite bank to show him the ford: "Go to the right!" A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

It was at this point that some young villager called, in profuse compliment: "Three cheers for the Prince!" When Valmond Came to Pontiac, Complete |Gilbert Parker 

How many events had occurred since he last walked along this path, and received a friendly bow and smile from every villager. File No. 113 |Emile Gaboriau 

My clumsy villager's scales, on which potatoes may be weighed to within a kilogramme or so, do not permit of this precision. More Hunting Wasps |J. Henri Fabre 

"Perizzite," in fact, means "villager," and the word is a descriptive title rather than the name of a people or a race. Early Israel and the Surrounding Nations |Archibald Sayce