Those close to him believe he would be fine with having a vote on another package before they leave town, even if it’s smaller than the plan Pelosi wants. Trump moves closer to Pelosi in economic aid talks, and House speaker must decide next move |Rachael Bade, Erica Werner |September 17, 2020 |Washington Post 

The Bedminster club temporarily closed down operations on March 17, after Murphy imposed new restrictions on businesses and social gatherings because of the pandemic. Trump’s businesses charged Secret Service more than $1.1 million, including for rooms in club shuttered for pandemic |David Fahrenthold, Josh Dawsey |September 17, 2020 |Washington Post 

The results were closer among Republicans, 41 percent of whom said the theory didn’t hurt the country and 50 percent of whom said it did. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

If I didn’t close it, I think you’d have 2 million deaths instead of having the 185,000 — 190,000. The problem with Trump’s ‘herd mentality’ line isn’t the verbal flub. It’s the mass death. |Philip Bump |September 16, 2020 |Washington Post 

Along with the entire automaker sector, Honda’s business felt the hit from the coronavirus crisis early on as global lockdowns caused factories to close and kept consumers away from dealerships. ‘A credible voice’: Why Honda is doubling down on esports |Lara O'Reilly |September 16, 2020 |Digiday 

Hopefully not overly close, but we talk about it in the episode how similar it is. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

The two strengthened ties over the years and now Krauss considers Epstein a “close” and “considerate” friend. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

Ney said McDonnell needs to “keep a stiff lip” and stay in close contact with family members. Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 

I got to work on all of it—and Epstein kept close tabs on me. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

So in that sense we have gotten close to the families that have lost loved ones, be it from one side or the other. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

Squinty could look out, but the slats were as close together as those in a chicken coop, and the little pig could not get out. Squinty the Comical Pig |Richard Barnum 

On the upper part of the stem the whorls are very close together, but they are more widely separated at the lower portion. How to Know the Ferns |S. Leonard Bastin 

It was close upon twelve o'clock, and the "Rooms" had been open to the public for two hours. Rosemary in Search of a Father |C. N. Williamson 

In the close relation and affection of these last days, the sense of alienation and antagonism faded from both their hearts. Ramona |Helen Hunt Jackson 

He passed the latter part of his life in poverty, and towards the close of it, was confined in a madhouse. The Every Day Book of History and Chronology |Joel Munsell