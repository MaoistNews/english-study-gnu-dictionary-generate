While the company hasn’t become a verb akin to Google, its name has made its way into financial vernacular. The problem with VC-backed founders who say they don’t care about getting rich |Lucinda Shen |December 3, 2020 |Fortune 

“Our brand is now widely used as a verb for bike taxi and 30 minute deliveries, and the fresh capital allows us to expand our network to solidify our leading position,” he said. Prosus Ventures leads $13 million investment in Pakistan’s ride-hailing giant Bykea |Manish Singh |September 30, 2020 |TechCrunch 

A verb marking whether the speaker has good evidence or not-so-good evidence for the thing they’re talking about is called an evidential. Talking Is Throwing Fictional Worlds at One Another - Issue 89: The Dark Side |Kevin Berger |September 9, 2020 |Nautilus 

You find that same pattern—things that get attached to the verb—in language after language. Talking Is Throwing Fictional Worlds at One Another - Issue 89: The Dark Side |Kevin Berger |September 9, 2020 |Nautilus 

When you look at other languages, you can see that same piece of structure, the thing that conveys the same meaning, further away from the verb. Talking Is Throwing Fictional Worlds at One Another - Issue 89: The Dark Side |Kevin Berger |September 9, 2020 |Nautilus 

Thus the adoption of any particular verb is a matter of taste, not a question of absolute correctness. Go Ahead, End With a Preposition: Grammar Rules We All Can Live With |Nick Romeo |November 3, 2014 |DAILY BEAST 

The term “gestation,” for instance, is derived from the Latin verb gestāre, used to describe a mammal carrying a burden. The Artificial Womb Will Change Feminism Forever |Samantha Allen |August 12, 2014 |DAILY BEAST 

As with any emergent technology where an action is involved, the brand becomes the verb. The Zen of Yo |Dale Eisinger |August 2, 2014 |DAILY BEAST 

The verb shovel is not a figure of speech; a garden shovel actually is used to serve the oysters. A Briny, South Carolina Oyster Shack |Jane & Michael Stern |March 23, 2014 |DAILY BEAST 

Their Dutch nickname, putterje, comes from the verb putten, meaning to draw water from a well. Face to Face With ‘The Goldfinch,’ the Painting from Donna Tartt’s Novel |Malcolm Jones |December 1, 2013 |DAILY BEAST 

The verb (—) in the Hebrew, when connected with the name of God in different other passages, has the same import. The Ordinance of Covenanting |John Cunningham 

Here ends Chaucer's portion of the translation, in the middle of an incomplete sentence, without any verb. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Observe that the word Christus has no verb following it; it is practically an objective case, governed by thanke in l. 168. ' Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Both in the present passage and in the Pardoner's Prologue the verb to erme is used with the same sb., viz. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Ethel could not help saying, "How did you find out the meaning of that word, Tom, if you didn't look out the verb?" The Daisy Chain |Charlotte Yonge