Desplat was, as he puts it, “a rather good flautist—like Henry Mancini was, actually.” Meet Alexandre Desplat, Hollywood’s Master Composer |Andrew Romano |February 11, 2014 |DAILY BEAST 

He was a good flautist, and composed several operas for Schikaneder's company, which he joined as a vocalist in 1784. Life Of Mozart, Vol. 3 (of 3) |Otto Jahn 

It pleased the young men musically-inclined and bohemian by profession to patronise the flautist, whom they declared marvellous. Aaron's Rod |D. H. Lawrence 

James Paisible, flautist and composer, who set this charming song to music, was born about 1656. The Works of Aphra Behn, Volume VI |Aphra Behn 

Jim rocked through the crowd, in his tall hat, looking for the flautist. Aaron's Rod |D. H. Lawrence 

He might have been a flautist, and he played with a catching lilt, a luxurious abandon that was an incarnation of Nature. Ghosts I Have Seen |Violet Tweedale