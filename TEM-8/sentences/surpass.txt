Monday marked Abe’s 2,799th straight day in office, making him Japan’s longest-serving Prime Minister and surpassing the record set by his great-uncle, Eisaku Sato. The legacy Shinzo Abe, Japan’s longest-serving Prime Minister, will leave after resigning |claychandler |August 28, 2020 |Fortune 

Matthias Schmidt, an independent auto analyst in Berlin, says registrations for NEVs in West Europe—a region covering 18 countries—have actually surpassed registrations in China so far this year. Tesla loses ground in Europe despite a local surge in EV purchases post-lockdown |eamonbarrett |August 28, 2020 |Fortune 

Argentina’s infections have surpassed 350,000 with the death toll climbing to more than 7,000. These countries aren’t waiting for a U.S., China, or U.K. COVID vaccine |Claire Zillman, reporter |August 26, 2020 |Fortune 

Generally, these employers set a threshold for how much they choose to pay out based on projected costs, and stop-loss insurance covers the claims when the threshold is surpassed. Alphabet’s Verily plans to use big data to help employers predict health insurance costs |Rachel Schallom |August 25, 2020 |Fortune 

Tesla market value has surpassed retail behemoth Walmart after an astounding surge in the electric-vehicle maker’s shares over the past five months. Tesla’s soaring stock pushes the company’s market value past Walmart’s |Verne Kopytoff |August 20, 2020 |Fortune 

This is why 2015 is projected to be the first year where gun deaths surpass traffic fatalities. How the Gun Nuts Try to Excuse Away the Santa Barbara Slaughter—and Why They’re All Wrong |Cliff Schecter |May 27, 2014 |DAILY BEAST 

Aim to surpass your funding goal by 100 percent in the early days of your campaign. How to Win at Crowdfunding |Bianca Goodloe |April 27, 2014 |DAILY BEAST 

Obama officials have to do a far better job of helping all Syrians see that this nightmarish future could surpass the Assad past. Face the Assad Reality In Syria |Frank G. Wisner, Leslie H. Gelb |January 26, 2014 |DAILY BEAST 

Home ownership reached record highs that have been difficult to surpass. The Revolt Against the Masses and the Roots of Modern Liberalism |Fred Siegel |January 26, 2014 |DAILY BEAST 

Adjusted for inflation, that would equal or surpass the oil shocks of 1973 and 1979. The End of Iran’s Oil Shield |Christopher Dickey |November 12, 2013 |DAILY BEAST 

In the tear-stained story of humanity there has never been aught to surpass the thrilling record of Cawnpore. The Red Year |Louis Tracy 

Woollen rags have been largely employed as a manure for hops, and are believed to surpass every other substance for that crop. Elements of Agricultural Chemistry |Thomas Anderson 

But these ripe and classic works will surpass everything we have heard since Glinka. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

There are many Mahomedans among the inhabitants, who surpass the Hindoos in industry and perseverance. A Woman's Journey Round the World |Ida Pfeiffer 

As a breed, they are excellent milkers; though some families of the Short Horns surpass others in this quality. Domestic Animals |Richard L. Allen