The basic trope — the last man on the ground — recalls an emotionally resonant idea of responsibility and even chivalry. The viral photo of the last soldier in Afghanistan is powerful — and that’s why it’s deceptive |Philip Kennicott |August 31, 2021 |Washington Post 

In the poem, Gawain is already a beloved and respected member of the Round Table, noted for his chivalry. The Green Knight is glorious and a little baffling. Let’s untangle it. |Alissa Wilkinson |July 30, 2021 |Vox 

After he arrives at the castle in the poem, Gawain undergoes a kind of testing of his chivalry and his virtue. The Green Knight is glorious and a little baffling. Let’s untangle it. |Alissa Wilkinson |July 30, 2021 |Vox 

Unfortunately, we only had the one fancy mattress between the two of us, and chivalry compelled me to claim her old backpacking Therm-a-Rest, a solid inch and a half below her perch on the luxurious Paco Pad. Don’t Compromise on Your Camp Mattress |mlarsen |July 13, 2021 |Outside Online 

I have never met her, and I am inclined to laud her chivalry. Pulp Nonfiction: India’s Shameful Failure to Defend Historian of Hinduism |Tunku Varadarajan |February 13, 2014 |DAILY BEAST 

“There's also the argument that traditional acts of chivalry are frowned upon as ‘suspicious,’” she writes. A New Survey Suggests Chivalry May Not Be Dead...It's Just Women Who Are Doing It |Erin Cunningham |December 23, 2013 |DAILY BEAST 

Turns out chivalry may not be dead—we just might be looking for it in the wrong places, or rather, from the wrong people. A New Survey Suggests Chivalry May Not Be Dead...It's Just Women Who Are Doing It |Erin Cunningham |December 23, 2013 |DAILY BEAST 

It may just reassure our faith that chivalry isn't completely over after all. A New Survey Suggests Chivalry May Not Be Dead...It's Just Women Who Are Doing It |Erin Cunningham |December 23, 2013 |DAILY BEAST 

This is traditional for Royal Spouses who are not themselves entitled to surround their Arms with an order of chivalry. William and Kate's New Conjugal Coat of Arms |Tom Sykes |September 27, 2013 |DAILY BEAST 

For in these times of chivalry the best perish first, and in order to live long one must be a monk. Honey-Bee |Anatole France 

It was a contest for extirpation, and ere it ended the flower of the English and the chivalry of the Indians were laid low. The Every Day Book of History and Chronology |Joel Munsell 

From feelings of chivalry or other reasons it is not in the nature of the male to inform on the female. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

The spirit of feudalism and of the old chivalry had all but departed, but had left a vacuum which was not yet supplied. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton 

My idea was to attract the chivalry and nobility, and make them useful and keep them out of mischief. A Connecticut Yankee in King Arthur's Court, Complete |Mark Twain (Samuel Clemens)