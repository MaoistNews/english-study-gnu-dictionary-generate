The world is not perfect, there are issues from racism to sexism and global inequality. Malala Yousafzai tells the business community: Education is the best way to guard against future crises |Michal Lev-Ram, writer |September 15, 2020 |Fortune 

The sexism inherent in how these cases are pursued by law enforcement comes through loud and clear. “People want to believe”: How Love Fraud builds an absorbing docuseries around a romantic con man |Alissa Wilkinson |September 4, 2020 |Vox 

Policies must be updated to reflect their lives and to address systemic racism and sexism. Millennials and Gen Zers need better childcare. Can either Biden or Trump deliver? |matthewheimer |August 29, 2020 |Fortune 

This question is thorny because in answering it one almost necessarily informs issues related to sexism, discrimination, and inequity. No, Animals Do Not Have Genders - Facts So Romantic |Cailin O'Connor |August 26, 2020 |Nautilus 

Some biases, such as racism and sexism, can even impact someone’s physical health and increase signs of depression. Let’s learn about bias |Bethany Brookshire |June 3, 2020 |Science News For Students 

Its biggest asset, of course, is the steely Atwell, who never asks you to feel sorry for Carter despite all the sexism around her. Marvel’s ‘Agent Carter’ Stomps on the Patriarchy |Melissa Leon |January 7, 2015 |DAILY BEAST 

The rule of law, you see, buckles, bends and sometimes crumbles under the weight of racism, sexism, and classism. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 

Attacked on Twitter and by outraged columnists for promoting sexism, Taylor issued a tearful public apology. Feminism Has Gone Too Far |Lizzie Crocker |November 21, 2014 |DAILY BEAST 

The most interesting personal information Grandin shared during the AMA was the sexism she faced during the start of her career. The Most Inspiring Bits of Temple Grandin’s Reddit AMA |Emily Shire |November 18, 2014 |DAILY BEAST 

In fact, she says the sexism she faced was far more of an obstacle than her autism in the workplace. The Most Inspiring Bits of Temple Grandin’s Reddit AMA |Emily Shire |November 18, 2014 |DAILY BEAST