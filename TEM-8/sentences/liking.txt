I would gladly have returned the item and replaced it with something of his liking. Miss Manners: She’s ‘best’ at giving gifts but stinks at receiving them |Judith Martin, Nicholas Martin, Jacobina Martin |February 4, 2021 |Washington Post 

The corporate titans seem to believe that the only way to get a democracy to their liking is to eliminate all threats to the Democratic Party’s unified control of government. Rupert Murdoch blasts ‘wave of censorship’ and ‘woke orthodoxy’ |Erik Wemple |January 26, 2021 |Washington Post 

After navigating to SurferSEO’s Content Editor, you plug in the query that you want to gather intel on, change the settings to your liking, and hit “Create a query”. Five SEO tips that capture holiday attention and boost sales |Houston Barnett-Gearhart |December 8, 2020 |Search Engine Watch 

Lighting — both natural and artificial — is hugely important to how we feel throughout the day, and being able to customize the lights to your exact likings is one of the huge plusses of working from home. Gift Guide: 7 great gifts for anyone working from home |Brian Heater |November 23, 2020 |TechCrunch 

While both parties support items like a second round of $1,200 stimulus checks, enhanced unemployment benefits, and more aid for small businesses, each side has held back from making a deal in hopes of getting a package more to their liking. Stimulus update: How the election will shape the next aid package |Lance Lambert |November 3, 2020 |Fortune 

And with stand-ups, I remember liking George Carlin and Steve Martin. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

“Most people are focused on the holidays anyway,” she continued, before adding something about people liking Cuban sandwiches. How Will Cuba Play In Peoria? |Eleanor Clift |December 21, 2014 |DAILY BEAST 

Actually for Conte, who has a passionate aversion to labeling, that may be a bit too much categorization for his liking. Viral Video Pioneers: How Pomplamoose is Turning YouTube Stardom Into a Sustainable Profession |Oliver Jones |October 27, 2014 |DAILY BEAST 

You see, Nolan had been suspended from work hours before the crime for comments about “not liking white people.” Megyn Kelly’s Really Scary Muslim |Dean Obeidallah |October 5, 2014 |DAILY BEAST 

Her T-shirts, which hang on the walls, were—I am told—originally hung too low for her liking, and too unevenly. The Cult of Blondie: Debbie Harry’s Very Special New York Picture Show |Tim Teeman |October 1, 2014 |DAILY BEAST 

Liking for a single colour is a considerably smaller display of mind than an appreciation of the relation of two colours. Children's Ways |James Sully 

Who could have believed that only a fortnight ago these same figures were clean as new pins; smart and well-liking! Gallipoli Diary, Volume I |Ian Hamilton 

I did not perceive that they had any great liking to them at first, neither did they seem to admire anything that we had. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Well, I am glad, for no more can I. I can't think of her liking for papa and baby and all of us to be left to ourselves. The Daisy Chain |Charlotte Yonge 

His tie with her was slight, her husband, a clergyman, little to his liking; he had not been near them for several years. The Wave |Algernon Blackwood