Each cabinet holds A4 Hanging Files, and come equipped with key lock systems. Great filing cabinets for your home office |PopSci Commerce Team |September 17, 2020 |Popular-Science 

The moment when cybersecurity experts would have to worry about quantum computer-equipped hackers seemed a long way off—at least a quarter century by some estimates—and there were far more pressing threats. Quantum computers threaten to end digital security. Here’s what’s being done about it |Jeremy Kahn |September 11, 2020 |Fortune 

This tube is equipped with three EVA foam seating pads, neoprene handles, and a “kwik-connect” system for an easy hook-up. The best boating tubes |PopSci Commerce Team |September 3, 2020 |Popular-Science 

The answer is to fund their training and make sure that they are equipped with the things that they need to be able to handle situations in a better way. Full Transcript: Tomi Lahren on ‘The Carlos Watson Show’ |Daniel Malloy |August 31, 2020 |Ozy 

If we hope to change this cycle, we must equip ourselves with the tools to do so. Why some senior officers are making it harder for police departments to fight racism |matthewheimer |August 26, 2020 |Fortune 

In September, Congress authorized the train-and-equip mission in Syria as part of a larger spending package. U.S. Hasn’t Even Started Training Rebel Army to Fight ISIS |Tim Mak |November 25, 2014 |DAILY BEAST 

Congress authorized the train-and-equip mission in mid-September, but two months later, recruitment has not even begun. U.S. Hasn’t Even Started Training Rebel Army to Fight ISIS |Tim Mak |November 25, 2014 |DAILY BEAST 

Irritated members of Congress say that the authorization of the train-and-equip mission is merely about optics. U.S. Hasn’t Even Started Training Rebel Army to Fight ISIS |Tim Mak |November 25, 2014 |DAILY BEAST 

Officials and experts acknowledge that the plan to train and equip a new rebel army from scratch will take years. No Syrian Rebels Allowed at ISIS War Conference |Josh Rogin |October 14, 2014 |DAILY BEAST 

He then called on Congress to authorize a program to train and equip 5,000 rebels per year in Saudi Arabia, which they did. Exclusive: America’s Allies Almost Bombed in Syrian Airstrikes |Josh Rogin |September 30, 2014 |DAILY BEAST 

To equip a dull, respectable person with wings would be but to make a parody of an angel. The Pocket R.L.S. |Robert Louis Stevenson 

If you fail at some point, then arms enough to equip a brigade of Mexican rebels may cross the river to-night. Uncle Sam's Boys as Lieutenants |H. Irving Hancock 

We have plenty of money saved to equip you, and maintain you well for a year or so; but after that you may require more. The Children of the New Forest |Captain Marryat 

Some, however, remained behind, to equip and despatch reinforcements as men continued to arrive asking to be enrolled. The Devil-Tree of El Dorado |Frank Aubrey 

It took years to train a legionnaire and a lot of money to equip an army and keep it in the field. Security |Poul William Anderson