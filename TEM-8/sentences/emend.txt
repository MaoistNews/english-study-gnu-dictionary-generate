Partly with the help of Toup, we may emend this corrupt passage as follows: , , . On the Sublime |Longinus 

To emend the Vulgate by the Hebrew and Greek is exactly what the heretics seek to do. A History of the Inquisition of Spain; vol. 4 |Henry Charles Lea 

Second, all critics have agreed to condemn the digression in which Theobald advertised his ability to emend Greek texts. Preface to the Works of Shakespeare (1734) |Lewis Theobald 

Some of these are trivial slips that a scribe copying B might emend on his own initiative, or perhaps by a lucky mistake. A Sixth-Century Fragment of the Letters of Pliny the Younger |Elias Avery Lowe and Edward Kennard Rand 

The alteration is very slight, affecting only one letter, and may be due to error in transcription or to mere desire to emend. The Expositor's Bible: The Psalms, Vol. 2 |Alexander Maclaren