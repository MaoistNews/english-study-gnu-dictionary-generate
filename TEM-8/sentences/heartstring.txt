Say what you will, though, he never tried to tug at our heartstrings by making Marlowe an orphan. Can Pulp Win the Booker? |Allen Barra |September 7, 2011 |DAILY BEAST 

Sometimes I see tabloid stuff online and it tugs at my heartstrings. Sandra Bullock's Ex Jesse James Opens Up |Marlow Stern |May 5, 2011 |DAILY BEAST 

She saw that her duty was before her; she must nerve herself and face it, though it tore her heartstrings. Love's Pilgrimage |Upton Sinclair 

Three or four horrible suggestions arose in succession to jerk at her heartstrings. Overland |John William De Forest 

The thoughts of this orphan boy clung to her heartstrings with a fondness for which she herself was unable to account. The Abbot |Sir Walter Scott 

The appeal which touched the heartstrings of all persons served a double purpose. Harper's Pictorial Library of the World War, Volume XII |Various 

It meant the tearing out of his very heartstrings which had found root at a woman's feet. "Unto Caesar" |Baroness Emmuska Orczy