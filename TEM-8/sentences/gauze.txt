Treated by an officer carrying the quick-clot gauze, the man survived a wound to the abdomen. Thousands of bullets have been fired in this D.C. neighborhood. Fear is part of everyday life. |Peter Hermann, John D. Harden |July 23, 2021 |Washington Post 

The group described its new chitin-based gauze in the January 2021 issue of ACS Applied Bio Materials. Bandages made from crab shells speed healing |Silke Schmidt |April 14, 2021 |Science News For Students 

Fattening the fabric between two hot steel sheets left it looking like the gauze people have long used as a wound dressing, or bandage. Bandages made from crab shells speed healing |Silke Schmidt |April 14, 2021 |Science News For Students 

These researchers wondered if making a gauze out of it would speed wound healing better than the traditional cellulose-based gauze does. Bandages made from crab shells speed healing |Silke Schmidt |April 14, 2021 |Science News For Students 

For their new gauze, the researchers ground up the shells of crabs, shrimp and lobsters. Bandages made from crab shells speed healing |Silke Schmidt |April 14, 2021 |Science News For Students 

The more we try to look through the gauze, the more it all begins to look like gauze. A Novel Nearly Impossible to Review |Nicholas Mancusi |December 28, 2014 |DAILY BEAST 

It will end up shriveled up, dried up, dead; rolled up in dirty gauze and tossed into a wastebasket, quickly forgotten. No One Ever Loses to Cancer |Dushka Zapata |October 8, 2014 |DAILY BEAST 

Another bold piece of color is a simple red dress made of silk gauze worn by one Monica Maurice for her wedding to Arthur Jackson. Here Comes the Bride…In Flaming Red: Two Centuries of Colorful Wedding Dresses |Liza Foreman |May 7, 2014 |DAILY BEAST 

Using tape and gauze, he then tried to plug the holes in that leg. Ty Carter Awarded Medal of Honor |David Eisler, Jake Tapper |August 31, 2013 |DAILY BEAST 

Thinking fondly of some gauze-filtered yesteryear where manufacturing jobs abounded and kids played outside more is one thing. Eat Like a Caveman? The Trouble With Paleo Living |Robert Herritt |March 21, 2013 |DAILY BEAST 

Madame Ratignolle, more careful of her complexion, had twined a gauze veil about her head. The Awakening and Selected Short Stories |Kate Chopin 

The wine spilled over Arobin's legs and some of it trickled down upon Mrs. Highcamp's black gauze gown. The Awakening and Selected Short Stories |Kate Chopin 

The only one who did justice to it was the countess-dowager—in a black gauze dress and white crêpe turban. Elster's Folly |Mrs. Henry Wood 

Titania herself appears in the transparent robe of silver gauze. Charles Baudelaire, His Life |Thophile Gautier 

In the dance in Scene VI she used a long black gauze scarf and a white one. Fifty Contemporary One-Act Plays |Various