If you’re hoping to tackle some other hairy areas, look for a model that comes with a nose, ear, or eyebrow trimmer to help you achieve your final look. The best beard trimmer: Shape your facial hair with ease |Carsen Joenk |January 19, 2021 |Popular-Science 

Any predator foolish enough to bite a Lophiomys imhausi gets a hairy mouthful of bitter toxins that human poachers use on arrows for hunting big game. Rats with poisonous hairdos live surprisingly sociable private lives |Susan Milius |January 12, 2021 |Science News 

Though 2020 was dominated by big, hairy societal change, science and technology took significant steps forward. These Were Our Favorite Tech Stories From Around the Web in 2020 |Singularity Hub Staff |January 2, 2021 |Singularity Hub 

The old man with the bad cough, hairy knuckles, and gray eyes. Fiction: Quiet earth philosophy |Katie McLean |October 21, 2020 |MIT Technology Review 

He dumped the contents onto a hairy monster, and the monster’s hair grew and looked beautiful. This scientist wants to know how racial discrimination gets ‘under the skin’ |Esther Landhuis |March 18, 2020 |Science News For Students 

His play The Hairy Ape, the agent noted, “could easily lend itself to radical propaganda.” Was the Unabomber a Eugene O’Neill Fan? |Robert M. Dowling |November 6, 2014 |DAILY BEAST 

Indeed, one female student told ASU News that cultivating a hairy existence was a “life-changing experience.” Meet the Professor of Hairy Studies |Lizzie Crocker |July 9, 2014 |DAILY BEAST 

In one mortuary, she observes the very hairy body of a dead fascist, “the nearest human thing I have ever seen to a gorilla.” Death Became Her: Molly Lefebure’s Wartime Years of Murder and Suicide |Tim Teeman |April 2, 2014 |DAILY BEAST 

As The Guardian noted, Cameron Diaz was telling adorable anecdotes about forcibly grooming a hairy friend just under a year ago. Madonna Took a Selfie of Her Hairy Armpit. So What? |Amy Zimmerman |March 21, 2014 |DAILY BEAST 

The Express reports that Harry's cousin Zara has dubbed him "Prince Hairy" according to a royal insider quoted by the Express. Prince Harry Ordered to Shave His Polar Beard |Tom Sykes |January 6, 2014 |DAILY BEAST 

This animal was something like a little boy, only, instead of clothes, he was covered with hairy fur. Squinty the Comical Pig |Richard Barnum 

The hairy animal, with the long tail, came straight for the bush behind which Squinty was hiding, and crawled through. Squinty the Comical Pig |Richard Barnum 

He had a loose hairy benignant face with a humorous but penetrating eye and the usual domelike brow. Ancestors |Gertrude Atherton 

They bore my friends away on their hairy-naked shoulders, and I stood too shocked to say a word. Valley of the Croen |Lee Tarbell 

Instead of pulling Mrs. Yoop's hair, he perched on her shoulder and smoothed her soft cheek with his hairy paw. The Tin Woodman of Oz |L. Frank Baum