“They treated us like kings and queens,” said one member who participated in the set visit. The case against caring about the Golden Globes |Alissa Wilkinson |February 26, 2021 |Vox 

These pale wrinkly little rodents, indigenous to East Africa, live in underground colonies with rigid roles and elaborate social hierarchies under the stewardship of a queen. Like humans, naked mole-rats have regional accents |Rahul Rao |February 25, 2021 |Popular-Science 

Once a new queen emerges, the colony’s sound again starts to match. Unique dialects help naked mole-rats tell friends from foes |Jonathan Lambert |February 24, 2021 |Science News For Students 

Alas, the queen has never liked the concept of “one foot in, one foot out” for working royals. Prince Harry and Meghan lose their patronages, won’t return as ‘working royals’ |William Booth, Karla Adam |February 19, 2021 |Washington Post 

The latest match pits Serena Williams, the queen of tennis, against Naomi Osaka, the youngster who is her heir apparent. Serena Williams and Naomi Osaka have a brief but memorable history |Cindy Boren |February 17, 2021 |Washington Post 

The reigning queen of hip-hop treads the boards in her teen years. Nicki Minaj: High School Actress |Alex Chancey, The Daily Beast Video |December 30, 2014 |DAILY BEAST 

The last time there was a raid of this scale was in 2001, when 52 men were arrested on Queen Boat, a floating disco on the Nile. Sisi Is Persecuting, Prosecuting, and Publicly Shaming Egypt’s Gays |Bel Trew |December 30, 2014 |DAILY BEAST 

When they invade new territory, populations are low, and the queen has limited mate options. Mongooses, Meerkats, and Ants, Oh My! Why Some Animals Keep Mating All in the Family |Helen Thompson |December 29, 2014 |DAILY BEAST 

Even the queen saw fit to honor him with the Order of the British Empire at Buckingham Palace in 2008. The Greatest Rock Voice of All Time Belonged to Joe Cocker |Ted Gioia |December 23, 2014 |DAILY BEAST 

A palace insider however insisted to the Daily Beast today that the Queen was not about to abdicate. Could The Queen Abdicate on Christmas Day? |Tom Sykes |December 17, 2014 |DAILY BEAST 

All along the highways and by-paths of our literature we encounter much that pertains to this "queen of plants." Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

A fancy came into my head that I would entertain the king and queen with an English tune upon this instrument. Gulliver's Travels |Jonathan Swift 

Q was a Queen, who wore a silk slip; R was a Robber, and wanted a whip. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

One of her humours was to unite the son of her minister, with a niece of the widowed Queen of Saint Germain's. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

As night began to settle down over the land, the Queen Elizabeth seemed to feel the time had come to give full vent to her wrath. Gallipoli Diary, Volume I |Ian Hamilton