But, in terms of our ability to fortify against those impending risks, it’s a system that’s not very adaptable to climate change. Environment Report: The High Cost of Getting Rid of Water |MacKenzie Elmer |February 8, 2021 |Voice of San Diego 

After your paddle, stroll over to the Jordan Pond House, where you can soak up the view once more, this time while fortifying yourself on creamy lobster stew and popovers. The Ultimate Acadia National Park Travel Guide |Virginia M. Wright |February 8, 2021 |Outside Online 

He thought they were safe, that the Capitol doors and windows were fortified to withstand blows and bullets. How battered D.C. police made a stand against the Capitol mob |Peter Hermann |January 15, 2021 |Washington Post 

In the United States, it’s best known, and well-loved, as a soup of greens, tiny pasta and meatballs, flavorful and fortifying. 3 nourishing soup recipes to revive you |Aaron Hutcherson |January 7, 2021 |Washington Post 

Some formulas are already fortified with proteins and carbohydrates derived synthetically or from cow’s milk. Startups are racing to reproduce breast milk in the lab |Katie McLean |December 18, 2020 |MIT Technology Review 

Does Israel offer up any facts to fortify his incendiary charge? Cynical Race-Baiting Will Fail to Save the Democrats |Ron Christie |April 14, 2014 |DAILY BEAST 

The freedom that comes with not having to seek votes can sometimes fortify the spine. Washington’s Other Car Crash: Obama vs. the Boehner Rule |Michael Tomasky |October 4, 2013 |DAILY BEAST 

And volunteers are working around the clock at Fort Tilden beach in the Rockaways to fortify sand dunes. Hurricane Season Is Starting, and Projections Are Higher Than Average |Eliza Shapiro |May 29, 2013 |DAILY BEAST 

She says she got involved volunteering not only to clean up the beach but to help fortify it. Superstorm Who? Sandy’s Hard-Hit Beach Towns Reopen for Business |Eliza Shapiro, Josh Dzieza |May 25, 2013 |DAILY BEAST 

So Romney reversed course again, vowing “to fortify his communications and messaging team by adding seasoned operatives.” Ignore the Pundits, Mitt, They’ll Ruin Your Presidential Campaign |Matt Latimer |July 8, 2012 |DAILY BEAST 

Young Richard, never loath to fortify himself, proved amenable enough to the stiffly laced Canary that his friend set before him. Mistress Wilding |Rafael Sabatini 

That is the most trying part of the day, even for those who have managed to fortify themselves with a good meal. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Gage began to fortify Boston Neck and brought in some guns which might otherwise have been seized by the people. The Political History of England - Vol. X. |William Hunt 

She saw not this glorious vision, to inspire and fortify her against the possible decay or downfall of her power. Select Speeches of Daniel Webster |Daniel Webster 

He had exhausted and stripped himself in constructing and completing it; he could neither fortify nor add to it. Toilers of the Sea |Victor Hugo