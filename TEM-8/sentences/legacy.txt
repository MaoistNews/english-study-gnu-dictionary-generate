All three actions directly threaten the legacy of Carl Stokes and that river fire back in 1969. 51 Years Later, the Cuyahoga River Burns Again |Wes Siler |August 28, 2020 |Outside Online 

Martinez told me the monitoring commitment is a huge victory for the workers, many of whom have since left Voyant but now “leave behind a legacy for their co-workers who remain.” Temp Workers Fight Back Against Alleged Sexual Harassment and Say They Face Retaliation for Doing So |by Melissa Sanchez |August 28, 2020 |ProPublica 

Initially seen as a mere caretaker for the iconic franchise that Jobs built before his 2011 death, Cook has forged his own distinctive legacy. Apple CEO Tim Cook is fulfilling another Steve Jobs vision |Rachel Schallom |August 24, 2020 |Fortune 

Part of that is because young shoppers do not care about legacy or the amount of time that brands have been around. ‘The new definition of luxury’: Highsnobiety unpacks how the landscape of high-end fashion has tilted toward accessibility |Kayleigh Barber |August 24, 2020 |Digiday 

Let’s hope we can renew those lost American conventions through this coming electoral season, honoring our legacy and ensuring our nation’s future as a unified people. Departure from convention—mom, baseball, the postal worker, and patriotism |jakemeth |August 19, 2020 |Fortune 

Unfortunately, this is more about protecting the legacy of a ‘great man.’ Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

I don't know why or who's doing it, but it's the legacy…and it's a legacy that is so important to the culture. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

The TVA, a federally owned and chartered electric power provider, is a New Deal legacy just like Social Security. Steve Scalise Shows There’s a Fine Line Between Confederate & Southern |Lloyd Green |January 2, 2015 |DAILY BEAST 

Reconcile is a rapper from Houston, a city with a rich hip-hop legacy. Down With the King: Christianity Isn’t Hiding in Rap’s Closet |Stereo Williams |December 28, 2014 |DAILY BEAST 

With the midterm elections safely in the rearview mirror, Obama is on legacy patrol. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

"These must be the legacy to our children," was the reply, in a grave and almost contrite tone. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Each gave to the abbé some legacy of affection to be conveyed to loved ones who were to be left behind. Madame Roland, Makers of History |John S. C. Abbott 

Thus, should a person mentioned as legatee die before the testator, the legacy would be invalid. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The primitive church, indeed, treasured up these memories of moral heroism as her most precious legacy to after times. The Catacombs of Rome |William Henry Withrow 

He had another legacy to make over to him, a large iron case fastened with three iron locks. Black Diamonds |Mr Jkai