At least two Massachusetts districts have eliminated elementary PE altogether this year, according to the president of the state’s SHAPE chapter. Kids are shooting hoops with rolled up socks, but pandemic physical education is not canceled |Kelly Field |February 12, 2021 |Washington Post 

Despite the need, Pfizer is the only manufacturer whose pediatric vaccine trials are far enough along to potentially have data on elementary-school age children by the end of the summer. Fauci: Vaccines for Kids as Young as First Graders Could Be Authorized by September |by Caroline Chen |February 11, 2021 |ProPublica 

In part of the city where demand was high for in-person learning, elementary students may be reporting to classrooms for only a few hours in the morning, with another group coming in the afternoon. After months of planning, protests and false starts, D.C. students and teachers head to classes for first time in nearly a year |Perry Stein, Lauren Lumpkin, Joe Heim, Laura Meckler |February 2, 2021 |Washington Post 

This elementary observation has deep connections to many areas of mathematics and yet is simple enough to be taught to kindergartners. Topology 101: The Hole Truth |David S. Richeson |January 26, 2021 |Quanta Magazine 

In the late 1960s, Ralph Dotinga – the son of Dutch immigrant dairy farmers – was a 6-foot-6 teacher at a Chula Vista elementary school. The Long Goodbye: How COVID-19 Took My Dad’s Life |Randy Dotinga |January 11, 2021 |Voice of San Diego 

Although Huckabee's condescending tone - like that of an elementary school history teacher - makes it difficult to take seriously. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

And then I did teachers all throughout elementary school and junior high for my friends. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Supporters pointed to math and literacy gains, while critics noted that those improvements disappeared in elementary school. Can the U.S. Government Go Moneyball? |Peter Orszag, Jim Nussle |December 23, 2014 |DAILY BEAST 

A small-town Ohio elementary school teacher just racked up 25 charges on an otherwise clean record for allegedly raping her son. Ohio Elementary School Teacher Charged With Raping Her Son |Nina Strochlic |November 11, 2014 |DAILY BEAST 

Her lawyer said he thought she had been fired from her job, as a first-grade teacher at Cridersville Elementary. Ohio Elementary School Teacher Charged With Raping Her Son |Nina Strochlic |November 11, 2014 |DAILY BEAST 

He had no conception of the use of the other arms of the service, and never gained even the most elementary knowledge of strategy. Napoleon's Marshals |R. P. Dunn-Pattison 

Have cabled a very elementary question: "Could not the Japanese bombs be copied in England?" Gallipoli Diary, Volume I |Ian Hamilton 

With them he is obliged to submit his bills to the elementary rules of arithmetic, and be careful that two and two make only four. Friend Mac Donald |Max O'Rell 

As time passed, this fund expanded considerably and was used to improve elementary education. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

It is used in journals, memoirs, biographies, and many elementary histories. English: Composition and Literature |W. F. (William Franklin) Webster