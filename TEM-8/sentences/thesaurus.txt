A thesaurus is a handy thing, but sometimes seemingly tiny differences in meaning can actually have a huge impact. Earliest known burial in Africa is that of a small, fragile child |Kiona N. Smith |May 6, 2021 |Ars Technica 

Instead, day after day I turn to all the wonderful dictionaries, thesauruses, and weird twists on them that are out there on the internet. Become a better writer with these online tools |Harry Guinness |October 20, 2020 |Popular-Science 

Had Palin scoured a thesaurus, she could not have come up with a more inflammatory phrase. Palin Goes Nuclear With 'Blood Libel' Speech |Howard Kurtz |January 12, 2011 |DAILY BEAST 

Hickes' transcript of the Calendar (Thesaurus, I, 203) shows an average of one error in every six lines. Beowulf |R. W. Chambers 

Thus the ancient Runic inscriptions, as we gather from Hickes's Thesaurus, are in the form of a knot. The Medicine-Men of the Apache. (1892 N 09 / 1887-1888 (pages 443-604)) |John G. Bourke 

The new thought of a treasury of merits (thesaurus meritorum) introduced further changes. Encyclopaedia Britannica, 11th Edition, Volume 17, Slice 2 |Various 

The work, however, on which his fame as a scholar is most surely based is the Thesaurus Graecae linguae. Encyclopaedia Britannica, 11th Edition, Volume 9, Slice 7 |Various 

John Starke himself, with his Thesaurus of Horror, never penned anything so deliciously frightful. The History of Burke and Hare |George Mac Gregor