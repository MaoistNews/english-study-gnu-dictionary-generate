Most of her colleagues and supervisors were middle-aged men. She reported sexual harassment by a former supervisor — and was fired soon after |Samantha Schmidt |February 8, 2021 |Washington Post 

The Pfizer-BioNTech vaccine is still approved for use only in those aged 16 years or older, and the Moderna vaccine is only for adults. Why aren’t kids getting vaccinated? |Niall Firth |February 8, 2021 |MIT Technology Review 

We know that our customers are 65% to 75% female, 25% to 35% male, and span a variety of age demographics from young millennials to middle-aged adults. Changing the plant-based food industry one spoon at a time |Rachel King |February 7, 2021 |Fortune 

Published a quarter century later, Tevis’s final novel, “The Color of Money,” revisits the now middle-aged Felson as he cues up against a new, younger generation of pool players. ‘The Queen’s Gambit’ is a bestseller, but its author, Walter Tevis, was hardly a one-hit wonder |Michael Dirda |February 3, 2021 |Washington Post 

As Miss Pittman, Tyson shows us a robust spirit seeking to push the boundaries of a frail, aged body. Cicely Tyson Didn't Just Open Doors—She Opened Whole Worlds |Stephanie Zacharek |January 29, 2021 |Time 

But when she returned to Selma in 1962 to care for her aged mother, she lost that right. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

At 10 past 5, a middle-aged white man climbed the stairs out of the City Hall subway. NYC’s Garner Protesters vs. Pro-Cop Protesters |Jacob Siegel |December 20, 2014 |DAILY BEAST 

Its beautifully aged wooden exterior houses traditional floor seating and beautiful gardens typical of the area. Inside The World’s 10 Oldest Restaurants |Justin Jones |December 20, 2014 |DAILY BEAST 

He alleges that a third boy, aged 10 or 11, was deliberately hit by a car and killed by a member of the pedophile network in 1979. Victim: I Watched British MPs Rape and Murder Young Boys |Nico Hines |December 18, 2014 |DAILY BEAST 

Can someone aged seven or 11 become a member of al Qaeda and the Taliban? Pakistani School Killers Want to Strike the U.S. |Sami Yousafzai, Christopher Dickey |December 17, 2014 |DAILY BEAST 

The aged woman made no reply; her eyes still studied Ramona's face, and she still held her hand. Ramona |Helen Hunt Jackson 

At the sight, Felipe flung himself on his knees before her; he kissed the aged hands as they lay trembling in her lap. Ramona |Helen Hunt Jackson 

A little boy aged two years and four months was deprived of a pencil from Thursday to Sunday for scribbling on the wall-paper. Children's Ways |James Sully 

Henry Rowley Bishop, a noted English music composer, died, aged 68. The Every Day Book of History and Chronology |Joel Munsell 

John Wilson, a celebrated landscape and marine painter, died at Folkstone, aged 81. The Every Day Book of History and Chronology |Joel Munsell