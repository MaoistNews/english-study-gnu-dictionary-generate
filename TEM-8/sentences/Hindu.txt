In one episode, a young Muslim man wins a neighbourhood cricket match against his Hindu opponents. Netflix v Modi and the battle for Indian cinema’s soul |Konstantin Kakaes |March 24, 2021 |MIT Technology Review 

Narendra Modi—is also the figurehead of right-wing Hindu nationalism. The Farmers' Protests Are a Turning Point for India's Democracy—and the World Can No Longer Ignore That |Simran Jeet Singh |February 12, 2021 |Time 

One of the men forced his way onto the stage and accused the stand-up, who is Muslim, of hurting Hindu sentiments. How An Indian Stand Up Comic Found Himself Arrested for a Joke He Didn't Tell |Sonia Faleiro |February 10, 2021 |Time 

I asked Swami Tattwamayananda, the head of the Vedanta Society of Northern California in San Francisco and one of the world’s leading authorities on Hindu ritual and scripture, how he felt about Americans like me saying namaste. Why ‘Namaste’ Has Become The Perfect Pandemic Greeting |LGBTQ-Editor |September 30, 2020 |No Straight News 

The timing of Apple’s direct-to-consumer entry in India comes right before India’s Hindu festival season, which is an especially lucrative time for mobile phone companies. Apple finally starts selling directly to customers in the world’s fastest-growing smartphone market |Grady McGregor |September 23, 2020 |Fortune 

By which I mean, there are some Jews and the occasional Hindu or Muslim. In Florida, ’Tis The Season for Satan |Jay Michaelson |December 7, 2014 |DAILY BEAST 

Buddhist and Hindu literature is rich with stories of disciples finally learning to surrender in this way. Is India’s Fallen ‘God-Man’ So Different From a Megachurch Pastor? |Jay Michaelson |November 21, 2014 |DAILY BEAST 

And while guru literally means “teacher,” in Hindu and Buddhist contexts, it often means much more. Is India’s Fallen ‘God-Man’ So Different From a Megachurch Pastor? |Jay Michaelson |November 21, 2014 |DAILY BEAST 

There are Egyptian influences and an imitation Hindu temple. The Postman Who Built a Palace in France…by Hand |Nina Strochlic |November 20, 2014 |DAILY BEAST 

The attendees throw colored powder around, not unlike the Hindu festival of Holi. Viral Video of the Day: Other Uses for GoPro |Alex Chancey |August 26, 2014 |DAILY BEAST 

We can do as we like with Hindu and Mussalman so long as we leave their respective religions untouched. The Red Year |Louis Tracy 

The Hindu walks with a great deal of poise, in fact, very much like an elephant, but he also has the agility of the panther. Kari the Elephant |Dhan Gopal Mukerji 

The highest idea of the Hindu, as of the Buddhist, is to pass out into a sort of painless existence of nothingness. Round the Wonderful World |G. E. Mitton 

In Penloe is seen the interior life of the Hindu combined with the best practical thought of the West. A California Girl |Edward Eldridge 

The Chinese and Hindu millions who would have starved to death, have been fed, and that's why they're with us to-day. The Iron Puddler |James J. Davis