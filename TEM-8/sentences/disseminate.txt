The long-time leader of Europe’s biggest economy was short on comforting words, saying that beating the disease hangs on the uncertain pace of developing and disseminating a vaccine—a process that could take 12 months or more. ‘Things will become more difficult:’ Merkel tries to sell debt-averse Germany on her ambitious COVID spending plan |Bernhard Warner |August 28, 2020 |Fortune 

Some of the insights shared by our dozens of panelists helped to explain trends or disseminate practical advice, both of which will be crucial to marketers as we move past these first six months of marketing disruption. Replay: Live with Search Engine Land season wrap-up—COVID and marketing disruption |George Nguyen |August 24, 2020 |Search Engine Land 

The idea of “self-disseminating” vaccines has floated through epidemiological circles for decades, conceived mainly as a tool for protecting the health of wildlife. Can Vaccines for Wildlife Prevent Human Pandemics? |Rodrigo Pérez Ortega |August 24, 2020 |Quanta Magazine 

Because health care — unlike the education system, the first three years of life really don’t have any infrastructure in which to disseminate programs. Policymaking Is Not a Science (Yet) (Ep. 405) |Stephen J. Dubner |February 13, 2020 |Freakonomics 

It is legal in the United States to create and disseminate a medical device without FDA approval as long as no money is exchanged. Honey, I Grew the Economy (Ep. 399) |Stephen J. Dubner |December 5, 2019 |Freakonomics 

Part of the apparent panic stems from the different ways countries disseminate their information. Europe’s Hidden Ebola Cases |Barbie Latza Nadeau |October 15, 2014 |DAILY BEAST 

Doctors have a vast platform to investigate, tabulate, and disseminate just how miserable they are. Study Says Doctors More Burned Out Than Others, But It’s Not Really a Malady |Kent Sepkowitz |August 23, 2012 |DAILY BEAST 

Most of the time, however, journalists disseminate boringly constructed articles or reports. The Honorable Press Baron |Steve Weinberg |March 19, 2010 |DAILY BEAST 

Each of these incidents incited the miffed woman to disseminate mild hearsay about my sexual orientation or general oddness. Grant Stoddard: Men Have It Worse |Grant Stoddard |March 17, 2009 |DAILY BEAST 

I would rather keep all this in camera than disseminate it to the teeming raptors of the Internet. Norman Mailer vs. Everyone |Norman Mailer |February 27, 2009 |DAILY BEAST 

With him painting was an instrument to disseminate the inventions of his poetic-satiric humour; it was a form of speech to him. The History of Modern Painting, Volume 1 (of 4) |Richard Muther 

He had imbibed an opinion that it was his duty to disseminate the truths of the gospel among the unbelieving nations. Wieland; or The Transformation |Charles Brockden Brown 

As they came to enjoy and disseminate their religion, they had no motive to irritate or disturb the aboriginal inhabitants. Great Events in the History of North and South America |Charles A. Goodrich 

The work of its ministers is not to discover and promulgate truths, but to invent and disseminate falsehoods. The Bible |John E. Remsburg 

Now it is very unpleasant to find that your news is untrue, when you have been at great pains to disseminate it. Phineas Finn |Anthony Trollope