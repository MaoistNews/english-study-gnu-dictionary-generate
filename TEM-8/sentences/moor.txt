Even as the vast majority of these settlers were born right here in the USA, Sundog speculates that their attachment to turf is some sort of emotional inheritance from the Motherland of moors and meadows. Should I Move to the Southwest, Even Though There’s a Drought? |mskenazy |September 1, 2021 |Outside Online 

“This one stood out like a sore thumb and it threw me for a loop,” Moor said. How the Police Bank Millions Through Their Union Contracts |by Andrew Ford, Asbury Park Press, and Agnes Chang, Jeff Kao and Agnel Philip, ProPublica |February 8, 2021 |ProPublica 

She was absolved of the charge because a portrait of a Moor hung above her bed. The Strange Science Behind Virgin Births |Candida Moss |January 22, 2014 |DAILY BEAST 

Estevanico, or “Esteban the Moor,” arrived on the continent in 1534. Slavery As ‘Innovation’ and Other Provocative Ideas: What I Learned From Henry Louis Gates’s ‘Many Rivers to Cross’ |Jamelle Bouie |October 22, 2013 |DAILY BEAST 

Battle of Shipton moor; prince Henry dispersed the 8,000 insurgents under Scroop, by seizing the persons of their leaders. The Every Day Book of History and Chronology |Joel Munsell 

A faint column of smoke curled up into the still air, and as he spoke the lower rim of the setting sun met the edge of the moor. Uncanny Tales |Various 

It ended in a broad open moor, stony; and full of damp boggy hollows, forlorn and desolate under the autumn sky. The Daisy Chain |Charlotte Yonge 

The girl looked round the ragged moor, brooding in the twilight, and half hesitated. Uncanny Tales |Various 

"I've been mooning about the moor all the afternoon and lost myself twice," she explained between frank mouthfuls. Uncanny Tales |Various