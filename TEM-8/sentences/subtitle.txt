This time, the puritan robot overlords that run the Play Store briefly decided that listing support for common subtitle files is enough to get your app banned. Google Play bans video app for standard “.ass” subtitle support |Ron Amadeo |January 26, 2021 |Ars Technica 

The developer says they "immediately filed an appeal" and today, the app is back up with the ASS subtitle listing still in the description. Google Play bans video app for standard “.ass” subtitle support |Ron Amadeo |January 26, 2021 |Ars Technica 

The subtitle of my article on Kurt Gödel’s infamous 1931 incompleteness theorems states that the theorems “destroyed the search for a mathematical theory of everything.” Our Favorite Comments of the Year |Thomas Lin |December 23, 2020 |Quanta Magazine 

I asked Glaeser to justify each of the claims in his book’s subtitle, that cities make us richer, smarter, greener, healthier, and happier. Is New York City Over? (Ep. 434) |Stephen J. Dubner |October 8, 2020 |Freakonomics 

With more users requiring subtitles, the technology behind them is constantly getting better. Learn how to add closed captions to video calls, Netflix, and more |David Nield |August 28, 2020 |Popular-Science 

He answers in the subtitle: “Probably not—and government should stop bribing people to stay there.” The Rustbelt Roars Back From the Dead |Joel Kotkin, Richey Piiparinen |December 7, 2014 |DAILY BEAST 

The book is not, as the subtitle maternally suggests, about “Protecting the Heart of Christmas.” Sarah Palin Serves Up a Healthy Serving of Venom in Her Christmas Book |Michelle Cottle |November 16, 2013 |DAILY BEAST 

Its subtitle: Why the Clustering of Like-Minded America is Tearing Us Apart. Colorado’s Strange Secession Vote |Michael Tomasky |November 5, 2013 |DAILY BEAST 

The disconnect points ironically to the subtitle of this book and the concept of liberalism. Anne Frank’s Amsterdam |Russell Shorto |October 12, 2013 |DAILY BEAST 

The subtitle says it all:  How Affirmative Action Hurts Students Its Intended to Help and Why Universities Won't Admit It. Affirmative Action: Who Does it Help, Who Does it Hurt? |Megan McArdle |June 24, 2013 |DAILY BEAST 

If he look more closely, he will find a subtitle, “An Old Story,” but this confuses him still more. Browning and the Dramatic Monologue |S. S. Curry 

In his subtitle Beughem makes clear what he intended to include in the Bibliotheca bibliothecarum. A History of Bibliographies of Bibliographies |Archer Taylor 

As his subtitle indicates, he has included many books that are not bibliographies. A History of Bibliographies of Bibliographies |Archer Taylor 

It is pragmatism as method which is emphasized, I take it, in the subtitle, "a new name for some old ways of thinking." Essays in Experimental Logic |John Dewey 

The dateline of each letter, which is right justified in the original, is here presented as a subtitle to each header. The Life and Letters of Lafcadio Hearn, Volume 1 |Elizabeth Bisland