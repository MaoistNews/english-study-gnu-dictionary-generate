In deep water, where relatively few animals live, the feast may last for years. Whales get a second life as deep-sea buffets |Stephen Ornes |October 15, 2020 |Science News For Students 

Here are 10 great films you can stream about feasts, food, family, and what cooking and eating teach us about being human. 10 delicious food movies |Alissa Wilkinson |September 25, 2020 |Vox 

A feast of comedies, dramas, documentaries, and more to stream at home. 10 delicious food movies |Alissa Wilkinson |September 25, 2020 |Vox 

One day, she wins the lottery, and instead of using the money to finally go back home, she uses it to prepare a lavish feast in honor of the sect’s founder. 10 delicious food movies |Alissa Wilkinson |September 25, 2020 |Vox 

Eager as ad buyers were for major sports like the NBA and NFL to return to TV, many were anxious about how the volume of sports on TV going from famine to feast would affect viewership. ‘Significant under-delivery’: TV advertisers grapple with glut of live sports affecting viewership |Tim Peterson |September 24, 2020 |Digiday 

It was known as the feast of Akitu, and it was celebrated in April. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

The mythic origin of the feast was the creation of the world by the god Marduk. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

Given the somewhat macabre origins of the feast, many of the celebrations were designed to placate the gods. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

Feast your eyes on the ‘top-grain leather,’ ‘original’ design, gilded pages. Rand Paul’s Many Leather-Bound Books |Olivia Nuzzi |November 27, 2014 |DAILY BEAST 

Then feast your ears on this 1969 Bill Cosby routine about drugging and seducing women. The Bill Cosby Controversy Stages of Grief |Amy Zimmerman |November 18, 2014 |DAILY BEAST 

In both cases the decision was made at a feast, and in favour of the one who “loved much.” Solomon and Solomonic Literature |Moncure Daniel Conway 

They were just about to celebrate tabagie, or a solemn feast, over his last farewell. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

In the spring of 1880 she went again to Paris, only to "feast on things artistic." Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Death comes in, the bread at the feast turns black, the hound falls down—and so on. The Wave |Algernon Blackwood 

But strangest of all the dishes at the Tagal's feast was one prepared from a kind of beetle. Alila, Our Little Philippine Cousin |Mary Hazelton Wade