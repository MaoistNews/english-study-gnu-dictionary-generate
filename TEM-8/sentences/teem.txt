As Facebook teems with posts — and ads — about politics in the home stretch of this presidential election season, advertisers have begun to ask their media agencies whether they should be taking a break from the platform. ‘Ready to spend’: Publishers are angling for election-wary advertisers’ Facebook budgets |Max Willens |October 2, 2020 |Digiday 

So, to make my daily three-mile walk more interesting, I started to reimagine the world around me as a reef teeming with alien life. 11 Great Microadventures You Can Do Now |The Editors |October 1, 2020 |Outside Online 

Soils teeming with wriggling worms just a few weeks ago now hold far fewer. Invasive jumping worms damage U.S. soil and threaten forests |Megan Sever |September 29, 2020 |Science News 

The original zone at the posterior pole, which Blochmann had seen, teems with bacteria. How Two Became One: Origins of a Mysterious Symbiosis Found |Viviane Callier |September 9, 2020 |Quanta Magazine 

In lyrical, engaging writing, Stewart Johnson, a planetary scientist, chronicles how our perception of Mars has swung from a world teeming with life, to definitely dead and boring, and back again over and over since the invention of telescopes. Two new books explore Mars — and what it means to be human |Lisa Grossman |July 15, 2020 |Science News 

Prep school faculties teem with Wasps who majored in English or history, as brokerage houses do with Wasps who majored in finance. The Last of the Wasps |Tad Friend |September 27, 2009 |DAILY BEAST 

If the past eight months were full of international thrills, the next ones are more likely to teem with spills. Obama's Next Six Crises |Leslie H. Gelb |September 11, 2009 |DAILY BEAST 

Observe how the papers teem with the misery of the lower classes in England, yet this affects not the West India philanthropist. Newton Forster |Captain Frederick Marryat 

Tears availing nothing, Elizabeth's quick brain began to teem with plans for John's escape. Revolutionary Reader |Sophie Lee Foster 

The newspapers in most cases teem with scandals which absorb the thoughts or arouse the passions. The War Upon Religion |Rev. Francis A. Cunningham 

Shining by reflected light, its pages literally teem with interesting anecdotes of many sorts. The Pagan's Cup |Fergus Hume 

The volumes of the Health of Towns Report teem with instances of the mischief of insufficient ventilation. The Claims of Labour |Arthur Helps