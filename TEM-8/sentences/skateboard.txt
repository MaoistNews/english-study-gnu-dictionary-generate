Children younger than 10 years old saw the biggest increase in injuries from skateboards and scooters. Pandemic led to fewer sports injuries, more firework and power-tool accidents |Todd Frankel |March 4, 2021 |Washington Post 

This time, all it took was one man, a skateboard, and a very large bottle of cranberry juice. How TikTok is changing the music industry |Stacey Anderson |February 21, 2021 |Quartz 

Consider the viral Ocean Spray-sipping TikToker on a skateboard with Fleetwood Mac “Dreams” playing in the background. ‘A regular drum beat of content’: How brands like Chobani are using TikTok to reach new audiences |Kimeko McCoy |January 29, 2021 |Digiday 

So, he jumped on his skateboard as one does, to take himself there. A TikTok star’s ‘Dreams’ come true |Ellen McGirt |October 7, 2020 |Fortune 

Huber, who was carrying a skateboard, was shot in the chest after apparently trying to wrest the gun from Rittenhouse, the complaint said. Prosecutors charge teen in fatal shooting of two protestors and wounding of a third in Kenosha |kdunn6 |August 28, 2020 |Fortune 

Look no further than Marbel, the record-setting, streamlined electric skateboard that weighs less than 10lbs. Fish on Wheels, Digital Pet Babysitters, and More of the Summer’s Best Kickstarters |Charlotte Lytton |May 29, 2014 |DAILY BEAST 

When a skateboard enthusiast turned her painting skills to making boards from scratch, the result was a work of art. Lauren Andino, Skater Chick Turned Entrepreneur, Makes Old-school Boards From Scratch |Nina Strochlic |May 8, 2014 |DAILY BEAST 

They rollerblade, skateboard, perform beatbox and hip-hop, and do parkour. If You Build It, They Will Skate |Maysoon Zayid |July 17, 2013 |DAILY BEAST 

“Growing up I had a funky skateboard and I loved watching Dolly Parton movies,” said Latifah. Queen Latifah Talks About the ‘Steel Magnolias’ Remake and the Importance of Diversity |Allison Samuels |October 7, 2012 |DAILY BEAST 

Nicole Miller turned to pop art, skateboard culture, and street style for her inspiration. The Oscar de la Renta Effect |Robin Givhan |September 13, 2011 |DAILY BEAST