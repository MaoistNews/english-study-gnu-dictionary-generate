To do this, it transforms itself into an old peasant, a fellow Buddhist or a beautiful young woman. The action-packed saga ‘Monkey King: Journey to the West’ gets a modern take |Michael Dirda |March 3, 2021 |Washington Post 

It falls squarely in the cibo povero, or peasant food, category. Bitcoin bombs lower, touching the dreaded $30,000 range |Bernhard Warner |January 22, 2021 |Fortune 

Some toilet-oriented regurgitation was involved, followed by comical sweating, like you see in “The Wizard of Id” comic strip from a peasant before he is hanged. Gene Weingarten: This hot sauce is a killer |Gene Weingarten |January 21, 2021 |Washington Post 

His coordinators weren’t being snared away from him every other year by desperate peasants such as Texas. Nick Saban continues a stretch unmatched in the history of college football |Chuck Culpepper |January 11, 2021 |Washington Post 

This playful memorial recognizes the role of service workers and the taste for imported peasant cuisine. In the galleries: Perspectives on blending culture and identity |Mark Jenkins |December 11, 2020 |Washington Post 

He was a large man, totally bald, with the rough hands of a peasant. In Chile, Poetry Outlives the Dictators |Jay Parini |October 27, 2014 |DAILY BEAST 

After wandering at haphazard some little way I met a peasant in a sleigh. Book Bag: Beguiling if Unlikely Travel Books |Sean Wilsey |September 4, 2014 |DAILY BEAST 

Some “new men” from peasant and artisan backgrounds rose, but many others became part of an impoverished proletariat. In the Future We'll All Be Renters: America's Disappearing Middle Class |Joel Kotkin |August 10, 2014 |DAILY BEAST 

Pretty well by Russian standards—a free peasant was known as a smerd, meaning “stinker.” Russian History Is on Our Side: Putin Will Surely Screw Himself |P. J. O’Rourke |May 11, 2014 |DAILY BEAST 

Entertaining used to require intelligence or a measure of wit or, at least, peasant cunning. Welcome to Showbiz Sharia Law |P. J. O’Rourke |May 4, 2014 |DAILY BEAST 

But the observation he thoughtlessly uttered in French seemed to excite the peasant's attention. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

She was the daughter of a peasant of Livonia, married a Swedish dragoon, who was killed on the same day in battle. The Every Day Book of History and Chronology |Joel Munsell 

In a springtime landscape a young peasant girl is seated beneath a tree, looking before her over a sunlit plain. Bastien Lepage |Fr. Crastre 

The king smiled, and remembering his past pleasures, ordered a thousand crowns to the peasant. The Book of Anecdotes and Budget of Fun; |Various 

He was the fourth son of a peasant proprietor of Lectourne, a little town on the slopes of the Pyrenees. Napoleon's Marshals |R. P. Dunn-Pattison