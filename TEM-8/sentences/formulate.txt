Decision making is about formulating alternative plans of action, considering their consequences, and choosing between them. What a study of video games can tell us about being better decision makers |Walter Frick |September 6, 2020 |Quartz 

It’s possible, the third employee speculated, that the lack of a strategy being communicated could be the result of their company’s top leaders formulating an overall strategy that has yet to trickle down. ‘Feels very much lip service’: Media employees agitate over companies’ inaction following diversity and inclusion pledges |Tim Peterson |August 31, 2020 |Digiday 

And, as the Democrats’ platform and policy plans are formulated, the Sanders wing of the party has continued to play this role. Biden Had To Fight For The Presidential Nomination. But Most VPs Have To. |Julia Azari |August 20, 2020 |FiveThirtyEight 

The idea was formulated at the tail end of the Cold War as a multinational collaboration, but design work didn’t properly start until the turn of the millennium, and its parent organization wasn’t launched until 2007. Construction of the World’s Biggest Nuclear Fusion Plant Just Started in France |Edd Gent |August 3, 2020 |Singularity Hub 

We have since engaged in detailed conversations with Facebook and are pleased to see their new commitments based on a four-point action plan formulated by the Global Alliance for Responsible Media. As the Facebook boycott ends, brand advertisers are split on what happens next with their marketing budgets |Seb Joseph |August 3, 2020 |Digiday 

There were wiki pages where users could jointly formulate and debate the rules. You Can Look It Up: The Wikipedia Story |Walter Isaacson |October 19, 2014 |DAILY BEAST 

After reading the script for this episode, did you formulate any theories about why the Terminans do what they do? Andrew Lincoln Wants Rick to End With Johnny Cash and the Sunset |Melissa Leon |October 14, 2014 |DAILY BEAST 

He asked Vice President Joe Biden to help formulate ways to curb gun violence. Hollywood, Shootings, and ‘2 Guns’: When Is Stylized Violence Obscene? |Michael Daly |July 30, 2013 |DAILY BEAST 

It works with members of Congress and the Obama administration to formulate center-left progressive policies. Intramural War of Words Raises Question of Who Loves Israel More |Linda Killian |January 6, 2012 |DAILY BEAST 

So yes, there was a change in that we applied those algorithms, or the results there-of, to formulate the "who"/"what" response. Behind the Scenes With the Team That Built Jeopardy's Watson |The Daily Beast |February 23, 2011 |DAILY BEAST 

He attempted to formulate some plans in his mind, and after a time it occurred to him that he should go back West to Gregory. The Homesteader |Oscar Micheaux 

He even began to formulate plans by which he could lure the unsuspecting Peter Levine into telling what he knew. The Outdoor Girls in the Saddle |Laura Lee Hope 

Bristow did not have time or need to formulate an excuse for his presence before Mr. Fulton rushed up the steps to meet Maria. The Winning Clue |James Hay, Jr. 

It is hardly possible of course from a distance of 57 miles to formulate an accurate idea of a mountain's shape. Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury 

He toiled in the daytime to formulate his thoughts for the evening. Wayside Courtships |Hamlin Garland