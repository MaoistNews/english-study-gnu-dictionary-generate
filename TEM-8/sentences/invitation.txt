These are not prescriptions, I replied, but invitations to experiment. The new wine rules: Drink what you like with what you want to eat |Dave McIntyre |February 12, 2021 |Washington Post 

Snow, rather than rainwater, filled the hollows when I saw Yang’s pieces, so I saw no birds taking up the invitation. Sculpture parks are a great way to see art during a pandemic. Here’s why some are better than others. |Sebastian Smee |February 11, 2021 |Washington Post 

In short, it has been a challenge trying to figure out the system, including how policies compare between state and county, and especially how Montgomery decides who gets a coveted email invitation to make an appointment. Getting a coronavirus vaccine appointment fills me with despair. Every search ends in frustration, and I’m at a low point of a long, dark year. |Marlene Cimons |February 5, 2021 |Washington Post 

Residents must register to request an appointment, and strong demand means “wait times between submitting a request form and receiving an invitation to schedule could be significant,” officials said. Maryland Democrats call for ‘course correction’ amid regionwide frustration over vaccine rollout |Erin Cox, Rebecca Tan, Antonio Olivo |February 3, 2021 |Washington Post 

Unlike sites such as Facebook, Telegram is a messaging app where users can create large, invitation-only and encrypted chat groups, and it allows users to remain anonymous by hiding their phone numbers from one another. “This Is War”: Inside the Secret Chat Where Far-Right Extremists Devised Their Post-Capitol Plans |by Logan Jaffe and Jack Gillum |January 28, 2021 |ProPublica 

You were there at my invitation to discuss issues with your constituents. GOP Boss Gets Help From ‘White Hate’ Pal |Tim Mak |December 30, 2014 |DAILY BEAST 

Letting humans use their common sense is not an invitation to anarchy. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

And I was lucky enough to receive an invitation to stay at Easter Elchies House, the spiritual home at The Macallan. A Whisky Connoisseur Remembers That First Sip of The Macallan | |December 10, 2014 |DAILY BEAST 

Admission is free, but by invitation only, and advance RSVP is required. Quorum: Global LGBT Voices | |December 10, 2014 |DAILY BEAST 

“Lyova, you know I have a funny feeling about this invitation,” he told Levon Atovmyan, a close friend and fellow composer. When Stalin Met Lady Macbeth |Brian Moynahan |November 9, 2014 |DAILY BEAST 

Twice a year the formal invitation was sent out by the old nobleman to his only son, and to his two nephews. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

To the invitation to precede him she readily responded, and, with a bow to the Seneschal, she began to walk across the apartment. St. Martin's Summer |Rafael Sabatini 

It was considerably to Justice Haggard's astonishment that he heard of the invitation to his son and his son's wife. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Aristide glowingly accepted the invitation and cast a look of triumph around the café. The Joyous Adventures of Aristide Pujol |William J. Locke 

A heavy rain had fallen in the afternoon, and he lingered in her company at her invitation and encouragement. The Homesteader |Oscar Micheaux