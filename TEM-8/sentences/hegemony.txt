Leading analysts such as Harvard’s Graham Allison argue that the vast majority of historical struggles among states for global hegemony have ended in devastating wars. As the U.S. and China continue to posture, the key will be Taiwan |Clyde Prestowitz |October 29, 2021 |Washington Post 

In reply, Colby notes that all those conflicts arose before the nuclear age and points out that the Soviet Union and America conducted a largely peaceful struggle for global hegemony precisely because they knew all-out war would destroy them both. As the U.S. and China continue to posture, the key will be Taiwan |Clyde Prestowitz |October 29, 2021 |Washington Post 

Back in the countercultural heyday of the 1960s, the molecular biologist Gunther Stent suggested that this process would happen through “global hegemony of beat attitudes.” Drugs, Robots, and the Pursuit of Pleasure: Why Experts Are Worried About AIs Becoming Addicts |Thomas Moynihan |September 17, 2021 |Singularity Hub 

On the other hand, if they are conscious, we should welcome the prospect of their future hegemony. If Aliens Exist, Here’s How We’ll Find Them - Issue 97: Wonder |Martin Rees & Mario Livio |February 24, 2021 |Nautilus 

Washington must not permit China to gain regional hegemony over its neighbors nor allow it to establish exclusionary or discriminatory trade blocs. Defeating today’s top threats requires rethinking our idea of national security |Melvyn Leffler |January 26, 2021 |Washington Post 

People sent her bits of information as a way for them to resist the hegemony of the cartels. She Tweeted Against the Mexican Cartels. They Tweeted Her Murder. |Jason McGahan |October 21, 2014 |DAILY BEAST 

The BRICS Bank looks, for all its founding rhetoric, like a platform for Chinese hegemony instead. John Kerry Just Visited. But Should We Just Forget About India? |Tunku Varadarajan |August 3, 2014 |DAILY BEAST 

The schism in Wisconsin was the first crack in the Republican Party's hegemony. The GOP’s Last Identity Crisis Remade U.S. Politics |Michael Wolraich |July 24, 2014 |DAILY BEAST 

The voting-rights issue in Mississippi was about control of the political machinery and about the “tradition” of white hegemony. When the Right to Vote Wasn’t a Right |Scott Porch |June 23, 2014 |DAILY BEAST 

After Japan invaded the Korean Peninsula in 1905, the conquerors sought to co-opt local pride to reinforce Japanese hegemony. Such a Sweet Little Dictator: Kim Jong-un and North Korea’s Child Cult |Scott Bixby |April 24, 2014 |DAILY BEAST 

Why he appropriated for Italy the revolutionary hegemony, he would have found it difficult to give a convincing reason. The Life of Mazzini |Bolton King 

In reality it was far more, because it gave the hegemony of continental Europe to Prussia. The Life of Napoleon Bonaparte |William Milligan Sloane 

The Athenian “hegemony” in its earlier and later phases had an important financial side. Encyclopaedia Britannica, 11th Edition, Volume 10, Slice 3 |Various 

And it seemed that only a short ladder lay between the preparation room and the Anglo-Saxon hegemony of the globe. The Longest Journey |E. M. Forster 

She aimed at overthrowing the present status quo in the Balkans, and establishing her own hegemony there. The Story of the Great War, Volume I (of 8) |Various