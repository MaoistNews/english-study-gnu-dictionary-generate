If there are five justices who wish to overrule decisions like those of Atkins, Roper, and Miller, Jones potentially presents them with a way to do it. 7 big cases the Supreme Court will hear in its new term, explained |Ian Millhiser |October 1, 2020 |Vox 

One report said Leach’s interview for the Tennessee job “went very well,” and another indicated that then-athletic director John Currie was prepared to hire Leach but was overruled by the administration. Is The SEC Ready For The Air Raid? Mike Leach Sure Thinks So. |Jake Lourim |September 30, 2020 |FiveThirtyEight 

If FDA career scientists get overruled by political appointees, that would also be a major sign of political pressure. How to Tell a Political Stunt From a Real Vaccine |by Caroline Chen, Isaac Arnsdorf and Ryan Gabrielson |September 26, 2020 |ProPublica 

Let me give me you the three reasons why we think it should be overruled, and it was a very nice generous thing to do. ‘Skim milk’ marriages not enough: Ginsburg remembered as LGBTQ ally |Chris Johnson |September 21, 2020 |Washington Blade 

When they failed, thanks to a maneuver granted by a recent state law that allows a few large cities that represent a majority of the county’s population to overrule the rest of the board, they walked out. Politics Report: What Comes Next for Sports Arena |Scott Lewis and Andrew Keatts |July 11, 2020 |Voice of San Diego 

He tried to contact a Health Republic doctor to overrule the nurse and get approval for payment. My Insurance Company Killed Me, Despite Obamacare |Malcolm MacDougall |November 24, 2014 |DAILY BEAST 

First, he maintains that enactment of such a law “would overrule a historic Supreme Court decision.” Democracy Demands a Journalist-Source Shield Law |Geoffrey R. Stone |April 15, 2014 |DAILY BEAST 

If all the mild mutineers could be induced to cling together, they could easily overrule Howe and his party. Down the Rhine |Oliver Optic 

I think it right to state the facts, because it shows how wonderfully God's grace can overrule. Gathering Jewels |James Knowles and Matilda Darroch Knowles 

Hope they'll overrule it—make a lot of difference to me if they don't. Lyre and Lancet |F. Anstey 

It will, of course, overrule any conflicting view of the Provost-Marshal-General, if there be such. The Papers And Writings Of Abraham Lincoln, Volume Six |Abraham Lincoln 

We know the court that made it has often overruled its own decisions, and we shall do what we can to have it to overrule this. The Papers And Writings Of Abraham Lincoln, Volume Two |Abraham Lincoln