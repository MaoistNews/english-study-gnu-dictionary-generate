Temperatures across the barren, windy highlands routinely dip below –30° Celsius, and the grass that typically sustains the rabbitlike mammals becomes dry and brittle. Pikas survive winter using a slower metabolism and, at times, yak poop |Jonathan Lambert |July 19, 2021 |Science News 

With the help of nonprofit groups, the community planted hundreds of trees, turning some of the barren hillsides green. Mixing trees and crops can help both farmers and the climate |Jonathan Lambert |July 14, 2021 |Science News 

For example, if you select a grey leaning toward a blue hue you might get prospective buyers to question the brightness of a space as blue tones are often found to be “cold” or barren. Tips to properly prep your home to sell for the most |Justin Noble |June 24, 2021 |Washington Blade 

It was once thought the solar system was probably a barren wasteland apart from Earth. The best places to find extraterrestrial life in our solar system, ranked |Neel V. Patel |June 16, 2021 |MIT Technology Review 

In the present, out there in Australia’s dinosaur country, you might find yourself staring across a barren plain imagining what other secrets this world of long-lost giants will reveal. Lizard of Oz: Australia’s Biggest Dinosaur Discovered |Scott Hocknull and Rochelle Lawrence, The Conversation |June 7, 2021 |The Daily Beast 

First, though, he has to be shocked into recognizing the barren waste of his spiritual life – by spirits. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

He'd kept the few offices at the front of the bungalow, now oddly barren. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Without it in the atmosphere, the Earth would be a barren, frozen wasteland. Extreme Weather? Blame the End Times |Jay Michaelson |November 28, 2014 |DAILY BEAST 

Ibrahim Hijazi walked me through his barren house, emptied ahead of the demolition. In Jerusalem Home Demolitions, the Biblical Justice of Revenge |Creede Newton |November 25, 2014 |DAILY BEAST 

Some parts were arid, nearly barren, others green and fertile. ‘The Harness Maker’s Dream:’ The Unlikely Ranch King of Texas |Nick Kotz |September 20, 2014 |DAILY BEAST 

Nothing, however, can save us but a union, which would turn our barren hills into fruitful valleys. The Book of Anecdotes and Budget of Fun; |Various 

The cost of extraction varies in different localities, depending mainly on the mass of barren rock to be encountered and removed. Asbestos |Robert H. Jones 

There the quantity of barren overlying rock and earth is enormous, and detracts immensely from the value of the mines. Asbestos |Robert H. Jones 

The coast is sandy, and from M. Peron's description, barren and unprofitable. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

After a barren discussion she held out her hand, large and generous like herself. The Joyous Adventures of Aristide Pujol |William J. Locke