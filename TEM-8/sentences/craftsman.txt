Each Grovemade monitor stand is made in the USA by dedicated craftsmen. Best computer monitor stand: One trusty product to support every screen in your home office |Irena Collaku |July 13, 2021 |Popular-Science 

Where once a studio might have handed off their “Superman” movie to a reliable craftsman like Richard Donner — who died Monday at 91 — professionalism and competence were no longer enough. Comic books have taken over the movies. Must they take our indie auteurs, too? |Ann Hornaday |July 8, 2021 |Washington Post 

Groups overseeing restoration work in Patan Durbar Square, another group of temples and shrines 30 minutes south of Kathmandu city center, are even allowing the public to walk near the craftsmen as they go about their work. How to Save the Structures We Love Most |Stephen Starr |June 28, 2021 |Ozy 

Today, in the shadow of a conflict that’s taken an estimated half a million lives, craftsmen are working to restore these unique water features. Fun: A River Runs Through It |Sean Culligan |June 4, 2021 |Ozy 

Until the second industrial revolution in the late-19th century, production was largely in the hands of skilled craftsmen who fashioned goods at their own pace. Employers’ new tools to surveil and monitor workers are historically rooted |Saima Akhtar |May 6, 2021 |Washington Post 

It gave him, writes Sykes, “the ability to become his own craftsman.” The Many Lives of Artist David Hockney |William O’Connor |November 23, 2014 |DAILY BEAST 

There are 17 processes herein, which collectively take four years for a craftsman to master. Behind the Wheel of the Bespoke Bentley |Zoe Settle |October 27, 2014 |DAILY BEAST 

A professional craftsman was recruited to build the rowboat. Victor Mooney’s Epic Adventure for His Dead Brother |Justin Jones |October 19, 2014 |DAILY BEAST 

The artist, instead of a mere craftsman, was beginning to be revered as something quite different. The Original Selfies |William O’Connor |April 15, 2014 |DAILY BEAST 

People take for granted how skilled he was as a craftsman, the simplicity of the writing, and how naturally it came. Joe McGinniss: The Hide of an Alligator, And Then Some |Lloyd Grove |March 12, 2014 |DAILY BEAST 

A fine piece of woodwork is often spoiled by the amateur craftsman when starting a cut with a saw. The Boy Mechanic, Book 2 |Various 

"You're supposed to be our combination inventor-craftsman," he said to George. Space Prison |Tom Godwin 

The reporter did something else which marked him as a craftsman. From Place to Place |Irvin S. Cobb 

Perhaps the Homeric chief may have sometimes been a craftsman like the heroes of the Sagas, great sword-smiths. Homer and His Age |Andrew Lang 

A more cunning craftsman than myself has told us that the less we track human life the more cheerily we shall speak of it. Tony Butler |Charles James Lever