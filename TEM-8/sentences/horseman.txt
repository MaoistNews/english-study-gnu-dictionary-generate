Contacted via its Facebook page and through English, the stable’s horsemen did not respond to multiple requests for comment. A horse track with no rules |Gus Garcia-Roberts |August 5, 2022 |Washington Post 

Our popular notions about the Cossacks tended to portray them as the energetic and rambunctious horsemen of the lower steppe from Russia and Ukraine. Cossacks: The Remarkable Military Settlers of Russia and Ukraine |Dattatreya Mandal |June 16, 2022 |Realm of History 

Together, they point to this horseman having been a warrior. The earliest known pants are surprisingly modern — and comfy |Bruce Bower |March 21, 2022 |Science News For Students 

A bronze horseman pointed toward the direction of the threat. Ancient robots were objects of fantasy and fun |E. R. Truitt/MIT Press Reader |November 30, 2021 |Popular-Science 

Guccio opened his first shop in Florence in 1921, offering fine leather luggage and accessories, as well as leather goods for horsemen. The Outrageous True Story Behind House of Gucci |Cady Lang |November 24, 2021 |Time 

In retrospect, 2009 and 2010 were halcyon days in the Middle East, now that we seem just one horseman short of an apocalypse. The Myth of the Central Park Five |Edward Conlon |October 19, 2014 |DAILY BEAST 

The horseman, aka Abraham, is actually passably cute, with a rocking bod and apparently steady source of income. Naked Ben Franklin Christens the Campy Return of ‘Sleepy Hollow’ |Amy Zimmerman |September 23, 2014 |DAILY BEAST 

A mischievous cross between Sunset Boulevard and Adult Swim, BoJack Horseman boasts an exciting cast of supporting characters. 'BoJack Horseman': The Debauched Tales of a Drunken, Groupie-Sexing D-List Horse, Hits Netflix |Marlow Stern |August 22, 2014 |DAILY BEAST 

In the pilot, Brooks proves himself to be the lamest cop of all time by agreeing to lead the headless horseman to his head. ‘Sleepy Hollow’ Is TV’s Craziest, Most Over-the-Top New Show ... And You Should Watch It |Amy Zimmerman |October 8, 2013 |DAILY BEAST 

Naturally, the headless horseman is really the first horseman of the apocalypse. ‘Sleepy Hollow’ Is TV’s Craziest, Most Over-the-Top New Show ... And You Should Watch It |Amy Zimmerman |October 8, 2013 |DAILY BEAST 

And Elam took the quiver, the chariot of the horseman, and the shield was taken down from the wall. The Bible, Douay-Rheims Version |Various 

Sometimes a horseman may succeed in killing him by cutting across his undeviating course. Hunting the Lions |R.M. Ballantyne 

This post admirably suited Grouchy, who was a horseman by nature and a cavalry soldier by instinct. Napoleon's Marshals |R. P. Dunn-Pattison 

"Um, that's better," admitted Grace, while the girls craned their necks for a better view of the horseman. The Outdoor Girls in the Saddle |Laura Lee Hope 

Just after they had swung once more into the road near the ranch, they met a horseman who proved to be Bill Talpers. Mystery Ranch |Arthur Chapman