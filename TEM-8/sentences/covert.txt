Set atop the dried-up bed of Groom Lake in the Nevada desert, the now-infamous spot made for good runways, and was remote enough to keep prying eyes off covert Cold War projects. A CIA spyplane crashed outside Area 51 a half-century ago. This explorer found it. |Sarah Scoles |January 5, 2021 |Popular-Science 

Military dogs and their handlers play vital roles, particularly in covert operations. This new harness lets military dogs parachute safely and with style |Christina Mackenzie |January 4, 2021 |Popular-Science 

While Project Veritas had previously disseminated covert recordings of CNN’s daily meeting, in this video O’Keefe himself could be seen dialing in to a private CNN call — apparently without the knowledge or consent of participants. Project Veritas’s James O’Keefe crashed a private CNN teleconference. CNN says he may have broken the law. |Jeremy Barr |December 3, 2020 |Washington Post 

When Bill Sampson pleads with Margo to contain her “paranoic outbursts” and “paranoic tantrums,” he’s channeling the sense that the American psyche is at risk of being torn apart by anxiety over covert invasion. ‘All About Eve’ at 70 |Tom Joudrey |September 25, 2020 |Washington Blade 

The GRU, he says, is also constantly creating “digital” equivalents of traditionally “analog” dirty tricks, making covert operations that used to involve agents or officials tougher to track. Spy Wars: The Hidden Foe America Must Defeat to Save Its Democracy |Charu Kasturi |September 13, 2020 |Ozy 

The Kulahu strike was part of a widening covert war being waged by Iran inside Pakistan. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

The remote controlled flying craft has gone from covert military ops to a communal backyard hobby. Why Every Home Needs a Drone This Holiday |Charlie Gilbert |December 8, 2014 |DAILY BEAST 

He and his followers have become really good at keeping their communications covert. ISIS Keeps Getting Better at Dodging U.S. Spies |Shane Harris, Noah Shachtman |November 14, 2014 |DAILY BEAST 

PARIS, France—What could be more cynical than a covert operation? The CIA’s Wrong: Arming Rebels Works |Christopher Dickey |October 19, 2014 |DAILY BEAST 

Syrian rebels have overtaken a joint Russian-Syrian secret facility that they claim was a covert intelligence collection base. Syrian Rebels Seize Russian Spy Station Near Israeli Border |Josh Rogin, Eli Lake |October 7, 2014 |DAILY BEAST 

She saw a covert smile on his wrinkled face, while his wife pushed her former inquiry. The World Before Them |Susanna Moodie 

To-day they wore light covert coats over their canvas and rubber. Ancestors |Gertrude Atherton 

A thick wood skirted the meadow-land in another direction; but they could not have gained that covert for the same reason. Oliver Twist, Vol. II (of 3) |Charles Dickens 

Fetherston raised his eyes and shot a covert glance at her—a glance of distinct suspicion. The Doctor of Pimlico |William Le Queux 

Their deeper recesses were given up to owls and bats, and nearer the entrance the prowling fox or jackal found a covert. The Catacombs of Rome |William Henry Withrow