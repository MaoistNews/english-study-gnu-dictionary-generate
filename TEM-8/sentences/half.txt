Eric Garcetti succeeded Villaraigosa and has received high marks in his first year and a half on the job. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

My dad was a sailor, and all through my childhood he was away half of the time at sea, and to an extent I have a similar job. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

As a result, training squadrons—called Formal Training Units (FTU)—are being staffed with less than half the people they need. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

Travel Noire fellows earned about a half million travel miles in 2014. ‘We Out Here’: Inside the New Black Travel Movement |Charlise Ferguson |January 4, 2015 |DAILY BEAST 

Murders in the City of Angels have fallen by about half in the last 10 years: no small feat for such a big city. America’s 2014 Murder Capital |Brandy Zadrozny |January 3, 2015 |DAILY BEAST 

It is the principal waste-product of metabolism, and constitutes about one-half of all the solids excreted—about 30 gm. A Manual of Clinical Diagnosis |James Campbell Todd 

A small book, bound in full purple calf, lay half hidden in a nest of fine tissue paper on the dressing-table. Hilda Lessways |Arnold Bennett 

Hilda, trembling at the door, more than half expected Mr. Orgreave to say: "You mean, she's invited herself." Hilda Lessways |Arnold Bennett 

All changes are to be Rang either by walking them (as the term is) or else Whole-pulls, or Half-pulls. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

The Vine is a universal favorite, and rarely out of view; while it often seems to cover half the ground in sight. Glances at Europe |Horace Greeley