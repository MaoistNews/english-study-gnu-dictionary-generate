This roommate comedy, which aired for seven seasons on Fox, is funny, sweet and once featured a cameo by Prince. Nostalgic for good, old-fashioned network TV? Here are 15 shows you can stream, from ‘The Office’ to ‘Cheers’ |Bethonie Butler |January 15, 2021 |Washington Post 

Stefanski will take the reins back from cameo head coach and usual special teams coordinator Mike Priefer, a native Clevelander who owns the most unusual 1-0 record in NFL history. Browns shake off their history, secure first playoff win since 1995 |Adam Kilgore, Des Bieler |January 11, 2021 |Washington Post 

Though the insider musical-theater jokes are fun — Priscilla Lopez, for instance, makes a cameo to send up her own performance in the original “A Chorus Line” — more attention must be paid to the basics of plot development. Judy and Mickey would have loved the TikTok ‘Ratatouille’ — 2021’s answer to ‘Let’s put on a show!’ |Peter Marks |January 2, 2021 |Washington Post 

There’s humor and drama and headaches galore, not to mention celebrity cameos and more than one trip to the Titanic. A journey to the bottom of the oceans — all five of them |Lucinda Robb |December 18, 2020 |Washington Post 

His rebooted life has included acting on the big screen, most notably a memorable cameo in “The Hangover,” and on Broadway in “Undisputed Truth,” a one-man production starring Tyson that his wife wrote and Spike Lee directed. Mike Tyson fought for the first time in 15 years. So much had changed. |Gene Wang |November 29, 2020 |Washington Post 

How did you wrangle Nicole Kidman for her hilarious cameo in the movie? Stephen Merchant Talks ‘Hello Ladies’ movie, the Nicole Kidman Cameo, and Legacy of ‘The Office’ |Marlow Stern |November 22, 2014 |DAILY BEAST 

Tarantino would later cast Chiba in a cameo as legendary sword maker Hattori Hanzo in Kill Bill: Vol. The Secrets of ‘Pulp Fiction’: 20 Things You Didn’t Know About the Movie on Its 20th Anniversary |Marlow Stern |October 19, 2014 |DAILY BEAST 

[Laughs] I was down there watching him for a while—I did a cameo with him at the end of the film—and he was very funny. R.L. Stine’s Secret to Scaring the Crap Out of Kids |Kevin Fallon |October 3, 2014 |DAILY BEAST 

I went to Siena, which was cast as Perugia, for a cameo role as a television journalist in the film. What It's Like to Watch Kate Beckinsale Play You in a Movie |Barbie Latza Nadeau |September 3, 2014 |DAILY BEAST 

The film stars Michael Parks, Justin Long, Haley Joel Osment, and features a cameo by Johnny Depp. Watch the Best Footage From Comic-Con ‘14: ‘Interstellar,’ ‘Ant-Man,’ ‘Batman v Superman,’ More |Marlow Stern |July 27, 2014 |DAILY BEAST 

It is a cameo head of Elizabeth, cut in a sardonyx, and set in a gold ring, enamelled at the back. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon 

Among the monkish loot at St. Albans was an ancient cameo herewith reproduced. Archaic England |Harold Bayley 

The intaglio work on this side is not equal to that in cameo, on the other. Scarabs |Isaac Myer 

His arms were about the lithe figure, drawing her close when he became aware of the clean-carven cameo face so near him. Where the Pavement Ends |John Russell 

A cameo dropped in one place, a clay figure of Minerva set up in another, completed the picture. The Long Night |Stanley Weyman