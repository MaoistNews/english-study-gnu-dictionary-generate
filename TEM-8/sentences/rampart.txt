I do think, however, that having won core ramparts for our civil rights, they had to find something else to do and screaming at straight people, and at cis people seems to be the new mode. Andrew Sullivan doesn’t care what you think |Chris Johnson |September 22, 2021 |Washington Blade 

In fact the Berlin Wall was called the Anti-Fascist Rampart, as I recall. How The Cold War Endgame Played Out In The Rubble Of The Berlin Wall |William O’Connor |November 9, 2014 |DAILY BEAST 

The department has not changed since the Rampart and Rodney King days. Rogue L.A. Cop’s Facebook Manifesto: ‘You Will Now Live the Life of Prey’ |The Daily Beast |February 8, 2013 |DAILY BEAST 

And, Denzel Washington and Woody Harrelson as corrupt characters in, respectively, Safe House and Rampart. Flick Picks: Denzel Washington as a Baddie, Rachel McAdams in ‘The Vow’ |Ramin Setoodeh, Peter Travers |February 10, 2012 |DAILY BEAST 

In Rampart, Woody Harrelson plays a corrupt, violent LAPD officer. Woody Harrelson on ‘Rampart’ |Richard Rushfield |January 13, 2012 |DAILY BEAST 

Editor's Note: An earlier version of this story misstated the official release date of Rampart. Woody Harrelson on ‘Rampart’ |Richard Rushfield |January 13, 2012 |DAILY BEAST 

And I will make a circle round about thee, and I will cast up a rampart against thee, and raise up bulwarks to besiege thee. The Bible, Douay-Rheims Version |Various 

A white flag waved on the rampart, and the drums of the garrison beat the chamade. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Gerona had at one time been a fortress, but it was now simply covered with a feeble rampart. Napoleon's Marshals |R. P. Dunn-Pattison 

In 207 onwards, Severus built a new wall along the line of Hadrian's rampart. The Towns of Roman Britain |James Oliver Bevan 

They reached the foot of the rampart, and by the stairway of a tower that was left unguarded, they mounted onto the curtain-wall. The Merrie Tales Of Jacques Tournebroche |Anatole France