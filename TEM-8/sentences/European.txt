There are reasons that European countries tend to avoid fluoride. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

We do see that a few European countries have them on the books: Germany, Poland, Italy, Ireland, a couple more. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

In front of this strange structure are two blank-faced, well-dressed models showing off the latest in European minimalism. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

But outside of a few European countries and Quebec, this leave is usually two weeks or less and usually unpaid. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

Those who served abroad were treated with suspicion that they had been infected by European diplomacy. U.S. Embassies Have Always Been for Sale |William O’Connor |January 2, 2015 |DAILY BEAST 

We can readily see how this might have been, from numerous experiments made with both American and European varieties. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

European tobacco is lacking in flavor and is less powerful than the tobacco of America. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

These shipments in the past have been financed through credits drawn on European centers. Readings in Money and Banking |Chester Arthur Phillips 

The flower stems on the American varieties are much longer than those of European tobaccos and also larger. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Hence it was that he found in Great Britain an implacable enemy ever stirring up against him European coalitions. Napoleon's Marshals |R. P. Dunn-Pattison