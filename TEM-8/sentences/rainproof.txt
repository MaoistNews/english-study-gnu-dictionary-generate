Just behind the barn he built me a lean-to shed, 253 about seventy-five feet long, open toward the east, and shingled rainproof. The Idyl of Twin Fires |Walter Prichard Eaton 

We were hurried away to remove our wet rainproof coats and to dry our hats and faces in the brilliant sunshine. South African Memories |Lady Sarah Wilson 

Silk and woolen materials for umbrella making, raincoats, etc., are expected to be rainproof. Textiles |William H. Dooley 

Cravenette—Cloths treated and finished before weaving by an improved process which renders them rainproof. Textiles and Clothing |Kate Heintz Watson 

No one would cavil at that term as applied to Whitman—yet one must not forget the “rainproof coat.” The Vagabond in Literature |Arthur Rickett