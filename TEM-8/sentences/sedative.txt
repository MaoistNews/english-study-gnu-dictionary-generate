You can tell them that the procedure is generally quick and, barring a pinch when the sedative is given, is pain-free. How at-home euthanasia can provide comfort to pets and owners |Kathryn Streeter |May 7, 2021 |Washington Post 

Hospitals in Brazil are reportedly running out of sedatives. As the U.S. Moves Toward Post-Pandemic Life, COVID-19 Is Still Devastating the World—Especially India |Jamie Ducharme |April 26, 2021 |Time 

They placed him in a chokehold and medical personnel who arrived later injected him with a heavy dose of a sedative. The Verdict on Derek Chauvin: A New Ending to a Familiar Tale |Milton Coleman |April 26, 2021 |TruthBeTold.news 

Women threw themselves from the roof, shot themselves in the head, filled themselves with sedatives and hanged themselves from the curtain rods. The hotel that nurtured ambitious women and their New York dreams |Mary Jo Murphy |March 26, 2021 |Washington Post 

That could have been a sign of use of illegal fentanyl, a fast-acting sedative and powerful painkiller that produces feelings of euphoria. At the heart of Derek Chauvin’s trial is this question: What killed George Floyd? |Lenny Bernstein, Holly Bailey |March 11, 2021 |Washington Post 

Just as Palmer, taken in sixty-second doses, seems relaxed, so, measured over hours, he seems in need of a sedative. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

At 6.23 p.m., a doctor administered the first drug, which corrections officials identified as the sedative midazolam. Lethal Injection Leads to the Most Botched Executions |Austin Sarat, Robert Henry Weaver, Heather Richard |April 30, 2014 |DAILY BEAST 

Ohio used a mix of midazolam, a sedative, with hydromorphone, a powerful narcotic. What Happens to the Death Penalty When Lethal Injection Isn’t Quick and Painless? |Andrew Cohen |January 21, 2014 |DAILY BEAST 

Only one thing seemed to calm his wanderlust: “I find an interesting book the only sedative,” he said. Top 10 Misbehaving Literary Rogues |Andrew Shaffer |February 7, 2013 |DAILY BEAST 

They gave a sedative to Methos, the alpha-male wolf, because he seemed particularly anxious. At New Jersey’s Turtle Back Zoo, Humans Slept Alongside the Pythons |Winston Ross |November 5, 2012 |DAILY BEAST 

Dorian was glad to take the sedative that promised oblivion from vexing thoughts. They Looked and Loved |Mrs. Alex McVeigh Miller 

It is no new propensity of animal nature, to find pleasure from the combination of a stimulant, and a sedative. The Mirror of Literature, Amusement, and Instruction, No. 358 |Various 

Pas-Avena is a widely advertised nerve sedative and hypnotic. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

And added, "Indeed, what can we do for sorrow except give the body a sedative?" Mary Gray |Katharine Tynan 

I administer some orthodox verbal sedative, and change the subject. Mystic London: |Charles Maurice Davies