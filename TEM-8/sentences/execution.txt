That has an impact on both media planning and creative execution. ‘2020 has been the year of contingency plans’: The new norms of marketing |Kristina Monllos |September 14, 2020 |Digiday 

“I’m looking forward to accelerating execution of the strategic vision for the future of Walgreens,” Standley said in a statement. Walgreens hires John Standley, former Rite Aid CEO, to run U.S. unit |Lee Clifford |August 31, 2020 |Fortune 

Broad cultural contexts are rarer and rarer, and to harness these contexts takes a lot of thoughtfulness and a high degree of successful execution. Multicultural audiences are making nuanced media choices |Vevo |August 25, 2020 |Digiday 

Eventually, the system may be tapped to help judge execution scores, too. A robot referee can really keep its ‘eye’ on the ball |Kathryn Hulick |August 20, 2020 |Science News For Students 

While these campaigns will have the tone of The Times, the execution is different, all publishers now, for instance, have to use programs like Zoom while remote working. ‘We can be agile and evolve’: News UK is quickly growing a 7-figure incremental revenue stream from social video |Lucinda Southern |August 5, 2020 |Digiday 

Recall how Clinton returned to Arkansas from the campaign trail to preside over the execution of a mentally disabled man. President Cuomo Would’ve Been a Lion |Jonathan Alter |January 2, 2015 |DAILY BEAST 

The execution of two police officers in cold blood has shocked the city and driven a deeper wedge between the cops and the mayor. Two Cops ‘Assassinated’ in Brooklyn |Michael Daly |December 21, 2014 |DAILY BEAST 

Their intentions may be good, but their execution and insight are lousy. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

Heavily armed Simbas had already arrived at the missionary house and were lining up families in the backyard for execution. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

If they were well thought through, with a clear plan of execution, she was in, and ready to go to the mat. The Valerie Jarrett I Know: How She Saved the Obama Campaign and Why She’s Indispensable |Joshua DuBois |November 18, 2014 |DAILY BEAST 

This alone could hinder the execution of his appointment, for in other things he has excellent qualifications for the dignity. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

But it greatly equalizes and strengthens the fingers, and makes your execution smooth and elegant. Music-Study in Germany |Amy Fay 

Ripperda's attention was next directed to put his plan of escape, in train for execution. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Important orders for new books are now in course of execution, the volumes being due early in this year's session. Report of the Chief Librarian for the Year 1924-25 |General Assembly Library (New Zealand) 

Many persons impede their execution by not keeping the thumb independent enough of the rest of the hand. Music-Study in Germany |Amy Fay