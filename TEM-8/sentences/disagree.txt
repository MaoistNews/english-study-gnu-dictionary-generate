People disagree whether Facebook’s role should involve moderating this sort of content on its platform or to what extent any of this should be protected as “free speech.” Facebook tries to clean up Groups with new policies |Sarah Perez |September 17, 2020 |TechCrunch 

It seems that physical server location is important, but not the only thing to take into consideration…and shockingly, courts disagreed on the outcome. This decade’s most important marketing question: What data rights do advertisers possess? |Kirk Williams |September 17, 2020 |Search Engine Land 

We can have these knock-down drag-out creative fights when we disagree. ‘Antebellum’ explores truths of our ugly past |Brian T. Carney |September 16, 2020 |Washington Blade 

In fact, political scientists disagree about whether TV ads even have a significant effect on elections at all! Trump Has Lost His Edge In TV Advertising |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |September 16, 2020 |FiveThirtyEight 

They might even disagree that a particular website or company is in their industry, but you have data on your side. SEO proposals: Particular challenges and how to avoid getting a silent no |SEOmonitor |September 10, 2020 |Search Engine Watch 

The vast majority of New Yorkers seem to disagree, wanting the Santas gone for good. Before the Bros, SantaCon Was as an Anti-Corporate Protest |David Freedlander |December 12, 2014 |DAILY BEAST 

Many Muslims may disagree with my view, or interpret Islam in a more moderate way, but I cannot accept this religion myself. What It’s Like to Be an Atheist in Palestine |Waleed al-Husseini, Movements.Org |December 8, 2014 |DAILY BEAST 

You and I disagree about the solution to this problem, of course, but we agree that there is a problem. Dear Evangelicals: You’re Being Had |Jay Michaelson |November 30, 2014 |DAILY BEAST 

But by and large, McCain and Kaine didn't so much disagree as lament different topics. Politics End In Halifax As Democratic and GOP Senators Seek Common Ground on National Security |Tim Mak |November 22, 2014 |DAILY BEAST 

The Syrian-American residents of one Pennsylvania city disagree—strongly. Welcome to Assadville, USA |Christopher Moraff |November 11, 2014 |DAILY BEAST 

Of course not,” said Wilkins, “proverbial philosophy asserts and requires that doctors should disagree. Hunting the Lions |R.M. Ballantyne 

I—I begin to understand why he dislikes that miller and his money business, for he makes you disagree so. Dorothy at Skyrie |Evelyn Raymond 

Who is it that you are sure will agree with Miss Adelaide, if any one indeed could be found to disagree with her? Lippincott's Magazine of Popular Literature and Science |Various 

"You've bragged that you'll fight any man who dares disagree with you," Haggar said loudly. Space Prison |Tom Godwin 

Seeing her betters disagree, little Fly had taken her turn at pouting. Prudy Keeping House |Sophie May