So he decided to draw a creature that is not only mammoth and imposing, but also unwaveringly destructive. How cartoonists are skewering Facebook after the latest revelations |Michael Cavna |October 28, 2021 |Washington Post 

Fernandez needed 2 hours 21 minutes and all the tactics at her disposal for her three-set victory over Sabalenka, who boasts a more powerful game, more experience and a more imposing physique, nearly a half-foot taller. Teens Leylah Fernandez, Emma Raducanu storm into U.S. Open final on grit and belief |Liz Clarke |September 10, 2021 |Washington Post 

Submission is set in a France seven years from now that is dominated by a Muslim president intent on imposing Islamic law. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

He added, “We cannot have a society in which some dictator someplace can start imposing censorship here in the United States.” Exclusive: Sony Emails Reveal Studio Head Wants Idris Elba For the Next James Bond |William Boot |December 19, 2014 |DAILY BEAST 

Pyongyang has given the Obama administration no choice but to retaliate now by imposing sanctions or even an embargo. U.S. Should Make North Korea Pay for Sony Hack |Gordon G. Chang |December 18, 2014 |DAILY BEAST 

The floor-to-ceiling Texas flag, the single most imposing feature of the room, began to rise. In Texas, Cruz, Perry Crow Over GOP Rout |Tim Mak |November 5, 2014 |DAILY BEAST 

What he fails to realize is that he is imposing the same urban title on his family in retaliation. ‘black-ish’ Keeps It Real about the Invisible Black Man |Judnick Mayard |September 24, 2014 |DAILY BEAST 

I was rather awed by his imposing appearance, and advanced timidly to the doors, which were of glass, and pulled the bell. Music-Study in Germany |Amy Fay 

In the center of the river line stood the imposing red sandstone palace of Bahadur Shah, last of the Moguls. The Red Year |Louis Tracy 

The law went into operation in England imposing a tax on wearing hair powder. The Every Day Book of History and Chronology |Joel Munsell 

These brilliant results were arrived at after much clamour and argument and imposing procès verbal. The Joyous Adventures of Aristide Pujol |William J. Locke 

The huge engine, the wonderful carriages, the imposing guard, the busy porters and the bustling station. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow