Washington, Oregon, and California also rely on inmates to bolster their firefighting efforts. “Unprecedented”: What’s behind the California, Oregon, and Washington wildfires |Umair Irfan |September 11, 2020 |Vox 

As of Thursday, there were 129 active coronavirus cases among inmates and 140 people had recovered. Morning Report: Downtown Jail Outbreak Goes From Bad to Worse |Voice of San Diego |September 11, 2020 |Voice of San Diego 

A Bureau of Prisons spokesman told VOSD that the downtown prison has been performing targeted coronavirus testing and has tried to limit inmate moves between facilities. Morning Report: Downtown Jail Outbreak Goes From Bad to Worse |Voice of San Diego |September 11, 2020 |Voice of San Diego 

Werner told Morris she believed the Sheriff’s Department wasn’t doing enough to protect staff and inmates from the coronavirus. Longtime Sheriff’s Employee Contradicts Official Account of Jail Death |Kelly Davis |September 3, 2020 |Voice of San Diego 

During previous wildfires, California has used state prison inmates who volunteer for reduced sentences and a small amounts of money. Photos of California’s fires reveal massive destruction across the state |Danielle Abril |August 21, 2020 |Fortune 

In none of the weddings Joplin has performed did the inmate have more than five years to serve. Saying Yes to the Dress—Behind Bars |Caitlin Dickson |December 8, 2014 |DAILY BEAST 

I spent four years in a prison where each handicapped convict was issued an underpaid inmate assistant. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

The woman had accompanied a friend who was visiting her boyfriend when she met the inmate she would marry. Saying Yes to the Dress—Behind Bars |Caitlin Dickson |December 8, 2014 |DAILY BEAST 

The last federal position he held was Inmate--he served more than eight years for his inventive approach to acquiring money. Election Day In The Big Sleazy |Jeff Greenfield |November 2, 2014 |DAILY BEAST 

However, if an inmate cannot find a job, he should expect to sleep outside with other homeless inmates. Cocaine, Politicians and Wives: Inside the World’s Most Bizarre Prison |Jason Batansky |October 12, 2014 |DAILY BEAST 

I grieve that one of the most promising of them is now an inmate in my cabin, in a very delicate state of health. Journal of a Voyage to Brazil |Maria Graham 

He could not bear the thought of her unhappiness, and yet, at any sacrifice, Tatsu must be kept an inmate of their home. The Dragon Painter |Mary McNeil Fenollosa 

The officers make the alternative quickly apparent to the new inmate: to protest against injustice is unavailing and dangerous. Prison Memoirs of an Anarchist |Alexander Berkman 

These last gave the name and country of many an unhappy inmate, with the date of the fatal day of their captivity. My Ten Years' Imprisonment |Silvio Pellico 

In no case can any inmate claim it as a right, and it is not to be given merely on account of household work. English Poor Law Policy |Sidney Webb