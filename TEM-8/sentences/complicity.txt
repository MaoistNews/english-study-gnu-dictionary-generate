Justice Minister Gustavo Villatoro suggested that the depth and complexity of the secret burial site pointed to the complicity of more than one person. Eight Pits Full of Murdered Women Found in Ex Cop’s Backyard in El Salvador |Barbie Latza Nadeau |May 21, 2021 |The Daily Beast 

A submission to, and eventual complicity with racism normalized a dynamic that should not exist. If You're Surprised by the Anti-Asian Violence in Atlanta, You Haven't Been Listening. It's Time to Hear Our Voices |Eric Nam |March 19, 2021 |Time 

You wrote on Twitter that when people have the power to help but sit idly by, then their silence is complicity. 'This Is A Human Issue.' Daniel Dae Kim on Coming Together to Combat Bigotry |Cady Lang |March 19, 2021 |Time 

As for Spears, it is a very good thing that we are finally talking about the objectification, infantilization, gaslighting, ridicule and invasions of privacy she’s suffered—and about our own complicity in that suffering. From Britney to Buffy, We're Suddenly Rethinking Postfeminist Pop Culture—and Nothing Could Be Healthier |Judy Berman |March 2, 2021 |Time 

Its supporters, inter alia, pressure American companies to end their complicity with Israeli apartheid by ceasing operations on occupied land. Anti-BDS laws an affront to free speech |Khelil Bouarrouj |February 19, 2021 |Washington Blade 

The documents also highlight the apparent complicity by secular law enforcement in keeping some of these offenders out of jail. How Sicko Priests Got Away With It |Barbie Latza Nadeau |November 16, 2014 |DAILY BEAST 

And we have an amazing complicity, an amazing respect for each other. Diane von Furstenberg: How I Learned to Love My Wrap Dress |Lizzie Crocker |October 27, 2014 |DAILY BEAST 

We were never asked to confront our own complicity as sponsors of the game. How the Media Failed to Nail the NFL |Steve Almond |October 19, 2014 |DAILY BEAST 

What sports pundits rarely bother to do is confront themselves, or their audiences, about their complicity in this pattern. Hey NFL Fans: Ray Rice Isn’t the Problem. You Are. |Steve Almond |July 24, 2014 |DAILY BEAST 

It is a site devoted to ending all privacy and putting everything in public view, with our complicity and cooperation. You Can’t Have Facebook’s Best Privacy Policy |Dennis Kneale |May 23, 2014 |DAILY BEAST 

Neither had the slightest suspicion of the lawyers complicity in the events of the night before. The Woman Gives |Owen Johnson 

Already, however, had returning shame made everybody unwilling to avow his complicity in the crime. History of the Rise of the Huguenots |Henry Baird 

Perhaps he preferred doubt to shocking certainty, as if he could thus escape the remorse attendant upon criminal complicity. File No. 113 |Emile Gaboriau 

Hinde, too, is accused of complicity, and both are taken in chains through the town. The History of Yiddish Literature in the Nineteenth Century |Leo Wiener 

Accordingly, it was asserted that the arrested Sinn Feiners had been guilty of complicity in a German plot. The Evolution of Sinn Fein |Robert Mitchell Henry