A study linked some of the levee and dike failures to earlier drought conditions, such as cracks forming because of exposure to heat and dryness. Heavy rains in drought-stricken states could be dangerous |Amir AghaKouchak/The Conversation |October 24, 2021 |Popular-Science 

By adopting a number of new tactics to staying informed, we can build a dike to keep out the flood of misinformation. How to resist and counter today’s flood of fake news |Alexandra Witze |September 16, 2021 |Science News For Students 

That standard works fine when you’re building a dike in areas where a flood won’t cause enormous damage, like an area of farmland. U.S. Civil Engineers Bent the Rules to Give New Orleans Extra Protection from Hurricanes. Those Adjustments Might Have Saved the City During Ida |Alejandro de la Garza |September 2, 2021 |Time 

In December 2008, four months after Andrea’s second relative died of leukemia, more than a billion gallons of coal ash slurry broke through a dike at the Tennessee Valley Authority’s Kingston Fossil Plant. The Coal Plant Next Door |by Max Blau for Georgia Health News |March 22, 2021 |ProPublica 

Without system-level support, individual decision-making is like the proverbial Dutch boy with his finger in the dike. The Year Of Choosing Dangerously |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |March 11, 2021 |FiveThirtyEight 

A whole population of 11 million with every iron in the fire doubling as a finger in a dike. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

The GOP, meanwhile, paints itself as sticking a finger in the dike of massive Obama spending. The GOP's Fiscal Time Bomb |Howard Kurtz |December 2, 2010 |DAILY BEAST 

It's a sort of finger in the dike approach with no clear vision, but maybe no one has a clear vision. A Man with Too Many Plans |The Daily Beast |February 10, 2009 |DAILY BEAST 

When riding or walking along upon such a dike on one side, down a long slope, they have a glimpse of water between the trees. Rollo in Holland |Jacob Abbott 

The dike was very regular in its form, and it was ornamented with two rows of trees along the top of it. Rollo in Holland |Jacob Abbott 

The dike was very broad, and the descent from it to the low land on each side was very gradual. Rollo in Holland |Jacob Abbott 

They had a delightful drive back, going as they came, on the top of the great sea dike. Rollo in Holland |Jacob Abbott 

The passengers that came in the ferry boat divided into two parties, as they came down the dike. Rollo in Holland |Jacob Abbott