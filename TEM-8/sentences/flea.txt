Sometimes there’s a flea infestation that’s too severe to not use the more common chemicals for. How to lower your dog or cat’s carbon pawprint |Sara Kiley Watson |July 29, 2021 |Popular-Science 

Scientists finally identified the disease as an infectious agent that time around, caused by a bacterium and spread to humans mostly via fleas. You could get the plague (but probably won’t) |empire |July 18, 2021 |Popular-Science 

The microscopic fleas can cling to fishing lines and survive in lake water at the base of your boat. A spiky flea could ruin Midwestern ecosystems and kill native fish |By Abi Cole/Outdoor Life |February 17, 2021 |Popular-Science 

Spiny water fleas feed on other smaller, native water fleas, which are a vital food sources for small fish and keep algae in check. A spiky flea could ruin Midwestern ecosystems and kill native fish |By Abi Cole/Outdoor Life |February 17, 2021 |Popular-Science 

Small fish will choke or puncture their organs if they try to consume the flea due to its long, sharp spine. A spiky flea could ruin Midwestern ecosystems and kill native fish |By Abi Cole/Outdoor Life |February 17, 2021 |Popular-Science 

And it all began with a young model rooting through Paris flea markets to find something that made her feel good. Tatiana Sorokko Is the Queen of Vintage Couture |Anthony Haden-Guest |October 8, 2014 |DAILY BEAST 

I grew up in New York and there was a Caldor that had a Sunday flea market. The TV Superhero Guru Behind ‘The Flash’ |Jason Lynch |October 6, 2014 |DAILY BEAST 

Six months after he arrived in Paris, he found another high-wheeled bicycle in a flea market and bought that too. Pryor Dodge's Two-Wheeled Obsession Is Now a Museum of Bike History |Anthony Haden-Guest |September 15, 2014 |DAILY BEAST 

A flea, Xenopsylla cheopis, from an infected little mammal—usually a rat—can hop from the dying rat onto a human and bite it. It’s Not Time to Worry About China’s Plague Just Yet |Kent Sepkowitz |July 23, 2014 |DAILY BEAST 

With its famous flea market and cute shops, Eastern Market has always been a popular stop on the weekend. A Local’s Guide to D.C. During the Holidays |William O’Connor |December 18, 2013 |DAILY BEAST 

On feeling the bite of the flea, thrust the part bitten immediately into boiling water. The Book of Anecdotes and Budget of Fun; |Various 

W'en Brer Rabbit year 'im comin' he bounce 'roun' in dar same ez a flea in a piller-case, but 't aint do no good. Nights With Uncle Remus |Joel Chandler Harris 

W'y, dat ar ole creetur aint never hurted a flea in all he born days—dat he aint,' sezee. Nights With Uncle Remus |Joel Chandler Harris 

"I guess that's the best rabbit dog anywhere around here," he said, as a flea-bitten cur trotted past. A Yankee from the West |Opie Read 

Old carpets, the sleeping places of cats or dogs or any dirty unswept corner may hold the eggs of the flea. A Civic Biology |George William Hunter