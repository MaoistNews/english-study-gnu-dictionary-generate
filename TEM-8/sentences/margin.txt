The poll finds Biden leads 57 percent to 41 percent among Minnesota likely voters, a 16-point margin that contrasts with Biden’s six-point edge in a Post-ABC poll conducted in Wisconsin over the same period. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

Baber, not the auditor, wrote the recommendations, and those that passed did so on thin margins, while others didn’t get enough support to go to the full board. Politics Report: Forged Footnote 15 |Scott Lewis and Andrew Keatts |September 12, 2020 |Voice of San Diego 

In some cases, this uptick was within the polls’ margins of error, but the overall upward trend was still pretty clear. Trump And Biden Both Got Small Convention Bounces. But Only Biden Got More Popular. |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |September 11, 2020 |FiveThirtyEight 

The number of wins divided by the total number of draws represents a simulated probability of a GOP win given the poll’s margin. The Forecast: The Methodology Behind Our 2020 Election Model |Daniel Malloy |September 10, 2020 |Ozy 

The Packers are favorites by a razor-thin margin after coming within a game of the Super Bowl last year, though they did little to improve around Aaron Rodgers while the 36-year-old still has good seasons left in the tank. What To Watch For In An Abnormally Normal 2020 NFL Season |Neil Paine (neil.paine@fivethirtyeight.com) |September 9, 2020 |FiveThirtyEight 

The citizens of Stevens Point defeated fluoridation by a healthy margin. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Because of the thinness of the air, there is a very tight margin between the correct and incorrect airspeeds, as little as 50 mph. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 

Sixty-seven Republicans voted against it, a margin in line with estimates of many conservatives from earlier in the day. ‘Cromnibus’ Passes, But Did Anyone Win? |Ben Jacobs |December 12, 2014 |DAILY BEAST 

Being in an indie band is running a never-ending, rewarding, scary, low-margin small business. How Much Money Does a Band Really Make on Tour? |Jack Conte |December 8, 2014 |DAILY BEAST 

We believe in Him by a landslide 74 percent to 26 percent margin. Up to a Point: Thanks to the Biggest Turkey, Uncle Sam |P. J. O’Rourke |November 27, 2014 |DAILY BEAST 

This paper was noted here and there on the margin, and had been obviously carefully read. Checkmate |Joseph Sheridan Le Fanu 

The comparison of the cost of production, therefore, with the value of the raw material, shows a very large margin of profit. Asbestos |Robert H. Jones 

Along the sea-margin of the tongue of land between the rivers Mersey and Dee, the sand has been thrown up in domes. Notes and Queries, Number 177, March 19, 1853 |Various 

He likens the walls to the page of a book, in which the glose, or commentary, was often written in the margin. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

On the other side was a narrow margin, and then a sheer wall of hills in exquisite verdure. Mrs. Falchion, Complete |Gilbert Parker