Other slightly more enterprising cafés will charge a couple of bucks for each one or ask for a donation to charity. Five cool ways to upcycle old coffee sacks |Harry Guinness |August 27, 2020 |Popular-Science 

The group’s first filings, for the 2017 tax year, show no donations to the nonprofit. Federal Prosecutors Have Steve Bannon’s Murky Nonprofit in Their Sights |by Yeganeh Torbati |August 24, 2020 |ProPublica 

In an email Wednesday to SANDAG board members, Mary Scyocurka, a conservative activist, said Ikhrata should be disciplined for his donation. One Race Could Make or Break Plans to Overhaul the Region’s Transportation System |Jesse Marx |August 13, 2020 |Voice of San Diego 

Eyal pointed to live kidney donation, a practice that carries risk and no physical benefit for the donor, but is widely accepted. Some Volunteers Want To Be Infected With Coronavirus To Help Find A Vaccine. But It Isn’t That Simple. |Kaleigh Rogers (kaleigh.rogers@fivethirtyeight.com) |August 6, 2020 |FiveThirtyEight 

His parents, Angela and KC Ahlers, continue to fight for his life and are asking for donations to help with the mounting medical bills to treat him. Family Told to Let Their Baby Die Instead of Raising Money For His Rare Disease |Health |November 27, 2019 |Health 

On Sunday Than Dar held the last of the funeral rites for her husband with a food donation ceremony at a monastery. Hope and Change? Burma Kills a Journalist Before Obama Arrives |Joshua Carroll |November 11, 2014 |DAILY BEAST 

This was before Cassidy entered politics, and the Republican congressman now calls that donation a “youthful indiscretion.” Mary Landrieu-Bill Cassidy Louisiana Senate Race Heads to a Runoff |Tim Mak |November 5, 2014 |DAILY BEAST 

Which is $30 billion less than the largest ever single donation. How Does Zuckerberg’s Ebola Pledge Measure Up To Other Bigwig Donations? |Nina Strochlic |October 14, 2014 |DAILY BEAST 

This is nothing like the million-dollar donation Maher made to the Obama campaign in 2012. Bill Maher Wants You to Meet—and Beat—Republican John Kline |Eleanor Clift |September 16, 2014 |DAILY BEAST 

The 1978 movie Coma explored deep-rooted fears about exploitation and medical advances in organ donation. ‘Persecuted’ Is the Christian Right’s Paranoid Wet Dream |Candida Moss |July 22, 2014 |DAILY BEAST 

Ando promised a generous donation in return for information concerning the Kano family. The Dragon Painter |Mary McNeil Fenollosa 

In the same year he presented the funds of the Society with £100, his last donation. Art in England |Dutton Cook 

Whatever may be our estimate of the merit or utility of a small donation, the most trifling addition is of some importance. Female Scripture Biographies, Vol. II |Francis Augustus Cox 

Mere pecuniary aid, or indeed any other form of donation, is after all a cheap description of charity. Female Scripture Biographies, Vol. II |Francis Augustus Cox 

Accordingly it requires that before attaining this delectable privilege, the “alien” must make a “donation” of ten dollars. The Modern Ku Klux Klan |Henry Peck Fry