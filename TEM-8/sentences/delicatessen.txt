Mintz owned several kosher delicatessens in New York and had a flourishing catering business, but his observance of Jewish dietary laws would not allow him to include dairy and meat in the same dish. David Mintz, inventor of Tofutti nondairy ice cream, dies at 89 |Matt Schudel |March 5, 2021 |Washington Post 

In four years, the rest of the family joined them, and it was arranged for the parents to run a delicatessen. Jack Steinberger, Nobel laureate in physics, dies at 99 |Martin Weil |December 17, 2020 |Washington Post 

The Mill Valley Market has grown and offers a deluxe delicatessen. Don Carpenter Was a Novelist Both Lacerating and Forgiving |Louis B. Jones |July 14, 2014 |DAILY BEAST 

They had a delicatessen first in Brooklyn, then in Queens, and they entered the country legally through Ellis Island. A College Degree Worth the Wait |Eleanor Clift |June 1, 2014 |DAILY BEAST 

The patronage of the bakery and the delicatessen shop should also be mentioned, and the waste of money involved. The Complete Club Book for Women |Caroline French Benton 

From a delicatessen store procure dill pickles and a nice Edam cheese. Suppers |Paul Pierce 

After several blocks the window of a delicatessen store showed him she was more composed, and he again offered her his arm. To Him That Hath |Leroy Scott 

Well, theres just father and Lonny and France and I, and mostly father brings home things from the delicatessen. Winona of the Camp Fire |Margaret Widdemer 

In the distance behind him was a milk wagon, a few pedestrians, a little thinly clad girl coming out of a delicatessen store. The "Genius" |Theodore Dreiser