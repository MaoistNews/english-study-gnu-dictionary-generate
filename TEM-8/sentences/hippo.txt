Lost on the TechCrunch editing floor from late yesterday is a post we wrote noting the sharp declines in the value of insurtech stocks ahead of the impending public debut of Hippo, another neo-insurance company. The technology selloff is getting to be somewhat material |Alex Wilhelm |March 5, 2021 |TechCrunch 

Justin Thomas could play with a hippo as his partner and not draw that many eyeballs. Golf faces a future without its irreplaceable face |Barry Svrluga |February 25, 2021 |Washington Post 

Now a study forecasts that the invasive hippo population will swell to almost 1,500 individuals by 2040. Invasion of the hippos: Colombia is running out of time to tackle Pablo Escobar’s wildest legacy |Sarah Kaplan |January 11, 2021 |Washington Post 

Gift shops in nearby Puerto Triunfo sell hippo keychains and T-shirts. Invasion of the hippos: Colombia is running out of time to tackle Pablo Escobar’s wildest legacy |Sarah Kaplan |January 11, 2021 |Washington Post 

By mimicking some of the behaviors of long-extinct animals like the region’s giant llama, hippos might actually be productive members of Colombian society. There’s no stopping this immortal jellyfish |PopSci Staff |December 9, 2020 |Popular-Science 

I'd never heard of a hippo attacking repeatedly like this, but he clearly wanted me dead. Swallowed by a Hippo |Justin Green |May 6, 2013 |DAILY BEAST 

I swam towards Evans, but the hippo struck again, dragging me back under the surface. Swallowed by a Hippo |Justin Green |May 6, 2013 |DAILY BEAST 

I called her “Hippo Girl,” and as I berated her I felt negative attention shift from me to her. My Strange Passage From Suspected School Shooter to Prom Queen |Gina Tron |January 28, 2013 |DAILY BEAST 

A moment on the noise: the vuvuzela can only be described as a large animal — like a cow or a hippo — in labor. Gal With a Suitcase |Jolie Hunt |July 10, 2010 |DAILY BEAST 

From time to time the slave-drivers would jog them along with a few lashes from a four-cornered "hippo" hide kiboko, or whip. In Africa |John T. McCutcheon 

This city is named from the ancient Hippo, out of whose ruins, a mile to the southward, it was largely built. Lippincott's Magazine of Popular Literature and Science, Volume 11, No. 24, March, 1873 |Various 

At a villa outside Hippo, St. Augustine passed three years in the company of eleven pious men. Bell's Cathedrals: The Cathedral Church of Carlisle |C. King Eley 

Later, Harrisson proposed to accompany me as far as the Hippo depot, bringing the dogs and providing a supporting party. The Home of the Blizzard |Douglas Mawson 

Here a depot of provisions and spare gear was made, sufficient to take us back to the Hippo. The Home of the Blizzard |Douglas Mawson