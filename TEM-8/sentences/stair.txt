The other day I bounded out of bed and down the stairs, and I realized that was something I hadn’t done in quite a few years. Want to Improve Your Running? Focus on Recovery. |Outside Editors |February 5, 2021 |Outside Online 

So, if we’re going into someone’s home, you can’t be alone and drop the body down the stairs. How to Be Better at Death (Ep. 450) |Maria Konnikova |February 4, 2021 |Freakonomics 

Boston Dynamics estimates it will take around 15 minutes to get you fully up to speed, but after a minute or two, I was able to send the robot up and down a flight of stairs at BD HQ. Soon Boston Dynamics’ Spot will be remotely opening doors anywhere |Brian Heater |February 2, 2021 |TechCrunch 

Those who complimented me for taking the stairs for my health had no idea that it was for my mental health, not my physical health. I’m autistic. I’m hoping I can wear a mask for the rest of my life. |Christine M. Condo |February 2, 2021 |Washington Post 

The type and layout of your house might have an impact—especially if you have stairs to clean when the size and weight of the device can come into play. Best cordless vacuum: Suck up debris without getting yourself tangled |Charlotte Marcus |January 19, 2021 |Popular-Science 

The remote control contains mode selections for standing, walking, sitting, and stair up and down modes that the user can select. The Bionic Exoskeleton Helping Paraplegics Walk |Dr. Anand Veeravagu, MD |June 29, 2014 |DAILY BEAST 

The smiling president immediately joked with the crowd, “I was so fired up, I missed a stair!” Power Tripping: King Juan Carlos I & More (Photos) |The Daily Beast |August 5, 2012 |DAILY BEAST 

The young lady, hearing his step, turned round and stood on the stair, confronting him fiercely. Checkmate |Joseph Sheridan Le Fanu 

He made no further remark as they descended the darker section of the stair, and she could think of nothing to say to him. Ancestors |Gertrude Atherton 

As Isabel walked carefully down the slippery stair she veiled her eyes to hide the wonder in them. Ancestors |Gertrude Atherton 

He's in the room now, only one away from this, next the stair head, and Vargers is put to keep the door in the same room. Checkmate |Joseph Sheridan Le Fanu 

As they there stand for a minute under the lamp, Mr. Longcluse, gazing at him sternly from the stair, caught his eye. Checkmate |Joseph Sheridan Le Fanu