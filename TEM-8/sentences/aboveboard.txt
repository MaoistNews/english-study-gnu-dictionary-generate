Google and Facebook have said little publicly in response to the new suit, and have maintained that Jedi Blue agreement was legal and aboveboard. These local newspapers say Facebook and Google are killing them. Now they’re fighting back. |Margaret Sullivan |February 4, 2021 |Washington Post 

Actually Kennedy knew nothing about the arrangement—and it was completely aboveboard. How Mitt Romney Can Win the First Debate With Obama |Robert Shrum |September 25, 2012 |DAILY BEAST 

"Since things are aboveboard—listen here," said Greenfield with sudden seriousness. Murder in Any Degree |Owen Johnson 

Quick tempers and excitable natures do not arouse mistrust, as they are at least "clear and aboveboard." The Fourth Estate, vol.1 |Armando Palacio Valds 

They all 'as that; dylies, weeklies, evenin's, Sundyes; but it's of no consequence—my voos are open and aboveboard. The Foundations (Fourth Series Plays) |John Galsworthy 

The underhand scheme ran counter to the aboveboard principles of the scout law which he had sworn to obey; of that he felt sure. A Scout of To-day |Isabel Hornibrook 

It is all aboveboard and it is all done by men of high honour and good character—I mean the Embassy staff. The Life and Letters of Walter H. Page, Volume II |Burton J. Hendrick