Because you sent it to my office at midnight and simultaneously released it to the press, I can unfortunately only conclude it is a political stunt. Pelosi and Mnuchin, once Washington’s Odd Couple, publicly disavow economic relief talks - and each other |Erica Werner |October 29, 2020 |Washington Post 

Before the show, which is in previews, viewers watch a short video in which they are instructed to assemble basic tools they’ll use as Suran performs his confounding stunts. With playhouses dark, interactive theater online is lighting things up |Peter Marks |October 29, 2020 |Washington Post 

Buzzard agrees, calling the formalization of perfectoid spaces a “gimmick” — the kind of early stunt that new technologies sometimes perform to demonstrate their worth. Building the Mathematical Library of the Future |Kevin Hartnett |October 1, 2020 |Quanta Magazine 

In a tweet Wednesday, Snider called the stunt “moronic,” and shared a video that was recorded by an upset customer inside the Target at Coral Ridge Mall in Fort Lauderdale. Twisted Sister’s Dee Snider does not approve of anti-maskers using ‘We’re Not Gonna Take It’ |radmarya |September 17, 2020 |Fortune 

He jumps on his bike, does a few stunts on the balcony, then rolls down the interior stairs. Vittorio Brumotti Serves Vigilante Justice on a Bike |Tom Vanderbilt |September 15, 2020 |Outside Online 

Mary Elizabeth Williams of Salon labels the show a “crass stunt” on a “bottom-feeding vortex of sadness network.” Your Husband Is Definitely Gay: TLC’s Painful Portrait of Mormonism |Samantha Allen |January 1, 2015 |DAILY BEAST 

Sometimes you have to fly stunt formations with a plane and another guy in a jetpack to really get your kicks. Daredevil in a Jetpack Flies Alongside A Plane |Jack Holmes, The Daily Beast Video |December 12, 2014 |DAILY BEAST 

She maintains that her mattress-carrying stunt is an “art piece,” not a protest. Is Columbia Failing Campus Rape Victims? |Lizzie Crocker |November 6, 2014 |DAILY BEAST 

Admittedly it would be called a stunt, but Obama needs something dramatic to rejuvenate his leadership. Can Obama and a Republican Senate Find Common Ground? |Eleanor Clift |November 4, 2014 |DAILY BEAST 

Sadly, Dell was killed five weeks after performing this stunt, fatally injured in a different performance. Dirtbike Flips over Stunt Plane |Alex Chancey, The Daily Beast Video |October 21, 2014 |DAILY BEAST 

Dedmon, here, was out of a job—thanks to you and your meddling—and the steering stunt offered good pay. The Wreckers |Francis Lynde 

He did the 'Great White Way' stunt—the restaurants, the roof gardens, a pretty actress at times, jewels and champagne. The Winning Clue |James Hay, Jr. 

Grayson's doing a stunt to-day that would have driven me mad with envy if I could have stopped to look on. Red Pepper Burns |Grace S. Richmond 

The stunt did more than earn the boys a large share of fame. Radio Boys Loyalty |Wayne Whipple 

"You try any stunt like that and I'll shoot you," Frank Nelsen promised. The Planet Strappers |Raymond Zinke Gallun