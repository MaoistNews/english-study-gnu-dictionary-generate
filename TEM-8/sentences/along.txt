So here I am in my requisite Lululemon pants, grunting along to an old hip-hop song at a most ungodly hour. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

But along with the cartoon funk is an all-too-real story of police brutality embodied by a horde of evil Pigs. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

While excoriating the IRS, Huckabee brings his readers along on a flashback to his youth. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

Term limits could be a prescription to speed change along. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

Chérif was arrested in Paris in January 2005 as he was about to board a plane to Damascus along with a man named Thamer Bouchnak. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

All along the highways and by-paths of our literature we encounter much that pertains to this "queen of plants." Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

First a shower of shells dropping all along the lower ridges and out over the surface of the Bay. Gallipoli Diary, Volume I |Ian Hamilton 

May looked along at the dimpled grace, And then at the saint-like, fair old face, “How funny!” Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

Two Battalions racing due North along the coast and foothills with levelled bayonets. Gallipoli Diary, Volume I |Ian Hamilton 

Presently there was a clattering of hoofs behind him, and Ribsy came galloping along the road, with nothing on him but his collar. Davy and The Goblin |Charles E. Carryl