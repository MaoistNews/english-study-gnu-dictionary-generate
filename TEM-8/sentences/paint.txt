If Embiid even steps foot in the paint, the nearest help defender slides over to deny an entry pass until the man guarding Embiid can steel himself enough to force Embiid off his spot. When Joel Embiid Posts Up, The Celtics Are Ready |Jared Dubin |August 21, 2020 |FiveThirtyEight 

They each finished the regular season ranked among the top 10 in the NBA in defensive rating and opponent points in the paint. What Can LeBron’s Previous Top-Seeded Teams Tell Us About This One? |Andres Waters |August 18, 2020 |FiveThirtyEight 

While on the water, the fishermen beat the canoe with banana leaves, applied body paint, blew on conch shells and chanted in synchrony. Why do we miss the rituals put on hold by the COVID-19 pandemic? |Sujata Gupta |August 14, 2020 |Science News 

Many of those products had little to no English language instruction and contained methylene chloride, commonly used for paint removal, which is banned for retail sale by the EPA because exposure can result in death. Environment Report: One Way to Force Companies to Emit Less Carbon |MacKenzie Elmer |August 10, 2020 |Voice of San Diego 

She’s more concerned with tracking, for instance, how many possessions the Dream can get Carter into the paint, as opposed to times down the floor when she’s limited to the perimeter. The Pace Of Play Has Never Been Faster In The WNBA |Howard Megdal |August 6, 2020 |FiveThirtyEight 

U.S. spies are worried the long-awaited Senate review will paint targets on their backs. CIA Offers New Security Checks for ‘Torture Report’ Spies |Shane Harris, Kimberly Dozier |December 9, 2014 |DAILY BEAST 

I want to paint what I feel,” he said to Theo, “and feel what I paint. Decoding Vincent Van Gogh’s Tempestuous, Fragile Mind |Nick Mafi |December 7, 2014 |DAILY BEAST 

The idea of being able to paint together was the direction we were all hinting toward anyway. Herbie Hancock Holds Forth |David Yaffe |November 8, 2014 |DAILY BEAST 

At this point, he became Tom Sawyer, letting his musical compatriots—and the folk tradition—help paint his musical fence. Digging the Gold in Dylan’s ‘Basement’ |Malcolm Jones |November 5, 2014 |DAILY BEAST 

During the day they would paint portraits of Spahn, using oil paint on small canvases that they had brought. Gay Talese on Charlie Manson’s Home on the Range |Gay Talese |October 31, 2014 |DAILY BEAST 

She apparently prefers to paint single figures of women and young girls, but her works include a variety of subjects. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

The general ensemble of war-paint and spotted ponies was enough for me; I didn't need to be told that it was my move. Raw Gold |Bertrand W. Sinclair 

He went into the nursery, unearthed the now-disused Noah's Ark, and sucked the paint off as many animals as remained. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

A knife would hurt, but Aunty Rosa had told him, a year ago, that if he sucked paint he would die. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

"Perhaps I shall be able to paint your picture some day," said Edna with a smile when they were seated. The Awakening and Selected Short Stories |Kate Chopin