For Rohit, foregoing a lucrative career to introduce a martial art based on teachings of a scandalous godman in a town where sex is a taboo is quite daring. A New Martial Art Keeps Cult Leader’s Teachings Alive |Daniel Malloy |December 18, 2020 |Ozy 

Thus for Rohit, foregoing a lucrative career to introduce a martial art based on teachings of a scandalous godman in a town where sex is a taboo is quite daring. A New Martial Art Keeps Cult Leader’s Teachings Alive |Daniel Malloy |December 18, 2020 |Ozy 

It’s weird but since his departure, it seems everyone is looking for some scandalous thing to have happened there. Former Avalon singer on coming out, getting ousted and where he is today |Joey DiGuglielmo |October 20, 2020 |Washington Blade 

Virginity for young women was highly prized, so it was moving out before marriage that was scandalous, not staying home where they could be shielded from young men. Yes, More And More Young Adults Are Living With Their Parents – But Is That Necessarily Bad? |LGBTQ-Editor |October 17, 2020 |No Straight News 

It’s scandalous and absurd these agencies refuse to give correct guidance. This scientist made a Google Doc to educate the public about airborne coronavirus transmission |Niall Firth |October 2, 2020 |MIT Technology Review 

Sands was involved in a scandalous-for-the-time romance with the carpenter and there were rumors she was pregnant with his child. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

So why did the God of the Hebrew people choose such a scandalous setting for becoming human? Jesus Wasn’t Born Rich. Think About It. |Gene Robinson |December 25, 2014 |DAILY BEAST 

It is wild that something that would seem to be so scandalous would just disappear from the press. Inside the Lifetime Whitney Houston Movie’s Lesbian Lover Storyline |Kevin Fallon |December 16, 2014 |DAILY BEAST 

She was a celebrated bohemian, considered a scandalous woman. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Michaud did not come out of the closet in order to avoid some scandalous revelation. America’s First Post-Gay Governor |David Freedlander |October 24, 2014 |DAILY BEAST 

I remember, however, the contempt and disgust which awoke in me at the sight of this scandalous chaperoning. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

If another person attempts to open a conversation upon scandalous matters, check her. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

As to the "opportunities," Chloe coolly confessed to herself that she had made rather a scandalous use of them. Marriage la mode |Mrs. Humphry Ward 

De Gery had made a long detailed memorandum of these scandalous abuses, with proofs in support of it. The Nabob |Alphonse Daudet 

They have gone to the adulterous rendezvous celebrated in the scandalous verses of Lorenzo the Magnificent. The Medici Boots |Pearl Norton Swet