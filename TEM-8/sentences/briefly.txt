He explained the various vaccine providers in Illinois, and what strategies worked for each — such as the Twitter accounts and aggregator sites that had briefly flourished and then been shut down for security violations. My mother and her friends couldn’t get coronavirus vaccine appointments, so they turned to a stranger for help. He’s 13. |Greg Harris |February 12, 2021 |Washington Post 

Temperatures between 32 and 34 degrees mean little accumulation is occurring on road surfaces although they may briefly get slick during any moderate bursts as visibility drops. D.C.-area forecast: Snow and wintry mix taper off this morning, while ice concerns mount for Saturday |David Streit |February 11, 2021 |Washington Post 

While we briefly discussed our preference for inkjet printers for photos, you can go further by finding a printer that is specifically designed to deliver beautiful, high-quality images. Best all-in-one printer: Upgrade your home office with these multitasking machines |Carsen Joenk |February 8, 2021 |Popular-Science 

Along the way, I was the chef for the bar and the lounge, and then worked at the Joël Robuchon restaurant briefly. How French Baking Was Brought to Virginia |Eugene Robinson |February 5, 2021 |Ozy 

The Know-Nothings formed a political movement that briefly became a major political party in the United States in the mid-1800s. The GOP: the new know-nothing party |Aaron Blake |February 4, 2021 |Washington Post 

Lee and Coogan did briefly meet with the pope, with pictures to prove it, but no one at the Vatican officially screened the film. Pope Francis Has the Pleasure of Meeting Angelina Jolie for a Few Seconds |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

After the screening, Jolie, who says she renewed her faith in “the divine” during filming, met briefly with the pope. Pope Francis Has the Pleasure of Meeting Angelina Jolie for a Few Seconds |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

Scalise spoke briefly, adding little of substance, saying that the people back home know him best. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

Other footage shows him fleeing, keeping to a quick walk, jogging briefly, then walking again as he heads for a subway station. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

He was courteous, explained the legitimate reason we were briefly pulled over, and then let us continue on our way. Obama Is Right on Race. The Media Is Wrong. |Keli Goff |December 29, 2014 |DAILY BEAST 

To this the fat boy, considerably terrified, briefly responded, “Missis.” The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

There are many excellent makers, but we must content ourselves with briefly noticing the most prominent. Violins and Violin Makers |Joseph Pearce 

Garnache assured him very briefly, and none too politely that he did not intend to prove of any excessive amiability. St. Martin's Summer |Rafael Sabatini 

As that religion seems to be still very popular I will try to express it as briefly as I can. God and my Neighbour |Robert Blatchford 

Briefly—Joan's silk tent had been torn, and the girl was in a state bordering upon hysterics. Three More John Silence Stories |Algernon Blackwood