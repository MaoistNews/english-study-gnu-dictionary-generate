The royal couple then traveled on to the Welsh capital of Cardiff to watch a rugby match between Wales and Australia. Kate Middleton, the Preggers Fashion Princess |Tom Sykes |November 14, 2014 |DAILY BEAST 

Another was Greg Jacob, a pro rugby player for Gaelic Athletic Association. The X Factor of Sex Invades Britain: Rebecca More’s ‘Sex Tour’ Enrages UK Politicians |Marlow Stern |October 20, 2014 |DAILY BEAST 

The Haka, a traditional Maori dance, is best known as the pregame ritual of the New Zealand All Blacks rugby team. Cartoon Streetfights, Giant Mutant Spider Dogs, and More Viral Videos |Jack Holmes |September 7, 2014 |DAILY BEAST 

Some English teams were having none of it and formed leagues to play rugby from which the NFL real football evolved. Up To a Point: Oops, I Enjoyed Soccer |P. J. O’Rourke |July 13, 2014 |DAILY BEAST 

French rugby players provided similar knowing delight in their Dieux du Stade calendars. Prince Fielder’s Demi Moore Moment: World Loses It Over Athlete Without Six-Pack |Tim Teeman |July 10, 2014 |DAILY BEAST 

They sent a few years ago some young Tibetan boys to Rugby to be educated in different professions. Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury 

"Rugby, because a fellow I know is there," replied Colin, decisively. The Nicest Girl in the School |Angela Brazil 

"I shall go to Rugby too, when Colin does," announced Jamie confidently. The Nicest Girl in the School |Angela Brazil 

Here were born six of his nine children; the youngest three, besides one who died in infancy, were born at Rugby. The World's Greatest Books, Vol X |Various 

At Rugby he made it an essential part of the headmaster's office to preach a sermon every Sunday in the school chapel. The World's Greatest Books, Vol X |Various