In some cases, we checked town payroll records to see whether officers hired after 2010 were selling back unused sick time annually. How We Found Pricey Provisions in New Jersey Police Contracts |by Agnes Chang, Jeff Kao and Agnel Philip, ProPublica, and Andrew Ford, Asbury Park Press |February 8, 2021 |ProPublica 

Unions are backing a proposal that would provide $15 billion in payroll support to keep workers on the job through the end of September. Airlines warn employees they could be furloughed at the end of March |Lori Aratani |February 5, 2021 |Washington Post 

The report precedes Friday’s monthly jobs report, which is projected to show that private payrolls grew by 105,000 in January after declining by 95,000 in December. Minority-owned companies hampered by the coronavirus pandemic | |February 3, 2021 |Washington Post 

Sharing payroll data works even better as a means of economic incentive, some experts argue. Beyond GameStop: Are You Ready for an Economic Moonshot? |Charu Kasturi |January 31, 2021 |Ozy 

Long-term, most managers don’t make their teams perform better than their payrolls suggest they should. Will A New Manager Matter For Chelsea? |Chadwick Matlin (chadwick.matlin@fivethirtyeight.com) |January 29, 2021 |FiveThirtyEight 

Begich has courageously talked of eliminating the payroll tax cap of around $115,000. Hooray for Liberal Fear-Mongering! |Michael Tomasky |October 28, 2014 |DAILY BEAST 

“There is always at least one ISIS person on the payroll; they force people on us,” says an aid coordinator. U.S. Humanitarian Aid Going to ISIS |Jamie Dettmer |October 20, 2014 |DAILY BEAST 

The temporary reduction of Social Security payroll taxes was allowed to expire in early 2013. The Battle of the Deficit Bulge Has Been Won |Daniel Gross |October 6, 2014 |DAILY BEAST 

Even with several judges on her payroll, Mandelbaum was not able to squirm out of the charges. Meet 'The Queen of Thieves' Marm Mandelbaum, New York City's First Mob Boss |J. North Conway |September 7, 2014 |DAILY BEAST 

The team with the biggest payroll is almost always going to win in league games. Why Americans Should Love the World Cup |Sean Wilsey |June 12, 2014 |DAILY BEAST 

How the payroll cash was brought from up the line in an armored car to the bank before opening time in the morning. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Hit would interfere with his gittin' a bank opened and himself back on the payroll. David Lannarck, Midget |George S. Harney 

"Twelve hundred and thirty-one tens," he read from the payroll change slip before him. Astounding Stories of Super-Science February 1930 |Various 

Galileo was cut off the Standard Oil payroll, and forced to apply to a teachers' agency, that he might find employment. Little Journeys to the Homes of the Great - Volume 12 |Elbert Hubbard 

"Got it," he informed the payroll he had been trying to add for half an hour. The Pride of Palomar |Peter B. Kyne