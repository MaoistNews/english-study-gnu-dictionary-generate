Now that he’s finally gone there’s an opportunity to explore and investigate a lot more of what’s going on under the hood. Even with expansion into other categories, Crooked plans to keep things political |Max Willens |February 12, 2021 |Digiday 

Detectives are continuing to investigate leads and conduct interviews, police said. Fairfax County police identify victim of fatal shooting in Reston |Justin Jouvenal |February 11, 2021 |Washington Post 

When Umansky tried to find out whether the Police Department would investigate the cops’ actions, he discovered all the ways the NYPD is shielded from accountability. ProPublica’s “NYPD Files” Wins John Jay College/Harry Frank Guggenheim Award for Excellence in Criminal Justice Reporting |by ProPublica |February 11, 2021 |ProPublica 

Detectives remained at the scene investigating Wednesday evening. Police looking for suspect who fled scene of homicide Wednesday in Reston |Justin Jouvenal |February 10, 2021 |Washington Post 

When they reported the crimes, police sometimes failed to investigate or prosecutors declined to file charges. Two ProPublica Local Reporting Network Projects Named Finalists for Shadid Award for Journalism Ethics |by ProPublica |February 8, 2021 |ProPublica 

When there were disputes between students, Mecallari would leave one party with Ramos while he went to investigate. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

Starr stresses that universities are required under Title IX to investigate and intervene in sexual assault cases. Is Columbia Failing Campus Rape Victims? |Lizzie Crocker |November 6, 2014 |DAILY BEAST 

The campaign also said that they would be asking state and federal authorities to investigate. Grimes Campaign “Exploring Legal Options” Against McConnell |Ben Jacobs |October 31, 2014 |DAILY BEAST 

The National Transportation Safety Board also will investigate the crash, a spokesman told The Daily Beast. SpaceShipTwo Flew on Untested Rocket |Dave Majumdar, Clive Irving |October 31, 2014 |DAILY BEAST 

Vita activists have demanded that police investigate how the sea creatures appeared in Moscow and why. Activists: Moscow Sea Park Is ‘Torturing’ Its Orca Whales |Anna Nemtsova |October 27, 2014 |DAILY BEAST 

I resolved to investigate the matter, as it was only verbal, so that it might not become public. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Twenty baskets of it were obtained by the said miners to assay and investigate its nature, and determine what it might be. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

In 1901 he went to India to investigate for the Government the railways there, and to report upon them. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

It would be curious to investigate the feelings of princes on occasions so momentous to themselves and to their people. Journal of a Voyage to Brazil |Maria Graham 

He halted to investigate and find out what was wrong, and at that moment a startled cry came from Clip. Motor Matt's "Century" Run |Stanley R. Matthews