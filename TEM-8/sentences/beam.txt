The researchers formed positronium by colliding a beam of positrons with a target, where they met up with electrons. A measurement of positronium’s energy levels confounds scientists |Emily Conover |August 24, 2020 |Science News 

Droplets spewed from the person’s speech show up neon green in the laser beam, moving like tiny meteor showers. 4 reasons you shouldn’t trash your neck gaiter based on the new mask study |Jonathan Lambert |August 12, 2020 |Science News 

A laser beam hits the debris in orbit and bounces back to Earth, and ground crews can measure how long that takes to figure out where they are and where they are going, alerting you to possible collisions with other objects. How to cast a wider net for tracking space junk |Neel Patel |August 5, 2020 |MIT Technology Review 

Meanwhile, the beams going the opposite way have their wavelength compressed by their shorter path. A giant underground motion sensor in Germany tracks Earth’s wobbles |Maria Temming |July 17, 2020 |Science News 

But, like the cosmic rays, the beams of light have a marginal effect. Cosmic Rays May Explain Life’s Bias for Right-Handed DNA |Charlie Wood |June 29, 2020 |Quanta Magazine 

Back then, no one ever imagined needing to beam live video to ground troops from a fighter jet. Newest U.S. Stealth Fighter ‘10 Years Behind’ Older Jets |Dave Majumdar |December 26, 2014 |DAILY BEAST 

Conservative columnist George Will calls him a “cherubic 40-year-old…a human beam of sunshine.” With Ernst and Gardner, Republicans Think They’ve Found the Formula |Eleanor Clift |November 4, 2014 |DAILY BEAST 

Her door stands ajar, halving the room with a beam of light. After the Genocide, Rwanda’s Widows Aging Alone |Nina Strochlic |August 31, 2014 |DAILY BEAST 

Outside the lodge, running along its perimeter, was a small ditch lined by posts topped by a chest-high wooden beam. Spirit Tripping With Colombian Shamans |Chris Allbritton |August 24, 2014 |DAILY BEAST 

The pulses are from a beam of light produced by the intense magnetic field, which sweeps across Earth as the neutron star rotates. The Weirdest Object in the Universe |Matthew R. Francis |May 18, 2014 |DAILY BEAST 

The latter went immediately to look for his wife, and found her hidden in an attic, hanging to a beam. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Cast the beam from thine eye before noticing the mote in that of thy neighbour. Solomon and Solomonic Literature |Moncure Daniel Conway 

It was placed immediately over the shaft and pump-rods, requiring no engine-beam. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

A connecting rod worked a balance-beam, which worked the air-pump, feed-pump, and plug-rod for moving the valves. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

A large balance-beam was attached to the pump-rods, near the bottom cross-head. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick