In a later experiment, they showed that animals, when given the choice, actively avoided areas of their cages that, when entered, triggered the activation of the neurons. Why do you feel lonely? Neuroscience is starting to find answers. |Amy Nordrum |September 4, 2020 |MIT Technology Review 

Ten weeks later, each monkey was moved with its mother to an unfamiliar cage. Puberty can repair the brain’s stress responses after hardship early in life |Esther Landhuis |August 28, 2020 |Science News 

The team also analyzed blood levels of the stress hormone cortisol before and after the time spent in the new cage. Puberty can repair the brain’s stress responses after hardship early in life |Esther Landhuis |August 28, 2020 |Science News 

That blood had been collected before, during and after their time in the new cage. A bit of stress may help young people build resilience |Esther Landhuis |August 27, 2020 |Science News For Students 

Around the same time, the psychiatrist Cesar Agostini kept dogs in cages rigged with bells that jangled horribly whenever they tried to lie down and sleep, and in the 1920s researchers in Japan did something similar with cages studded with nails. Why Sleep Deprivation Kills |Veronique Greenwood |June 4, 2020 |Quanta Magazine 

But you wonder how even the sane keep from losing their minds when you step into a cell—or rather a cage—at Graterford. Here’s a Reform Even the Koch Brothers and George Soros Can Agree On |Tina Brown |November 10, 2014 |DAILY BEAST 

It is empty, the door swung open—perhaps the bird has already flown, or perhaps the cage awaits its next inhabitant. Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 

In fact, in 2000 a 9.0-rated Action Comics No.1 owned by Nicolas Cage was stolen from his house. The Holy Grail of Comic Books Hid in Plain Site at New York Comic Con |Sujay Kumar |October 14, 2014 |DAILY BEAST 

In the U.S, Allan Kaprow, an artist pupil of John Cage, came up with the word “Happening” in 1957. The Life and Art of Radical Provocateur—and Commune Leader—Otto Muehl |Anthony Haden-Guest |September 22, 2014 |DAILY BEAST 

When I first heard about the sport, I assumed that it was a “no holds barred” cage match where pretty much anything goes. Jesus Said Knock You Out: In ‘Fight Church’ Christians Beat Thy Neighbor |Bryan Storkel |September 16, 2014 |DAILY BEAST 

So Hettie put the chicken in a cage, with some wool to cover it, and fed it several times every day, till it came to know her. The Nursery, July 1873, Vol. XIV. No. 1 |Various 

The 'cage' was simply an arrangement for 'straiter custody,' though but rarely judged necessary in the case of ladies. King Robert the Bruce |A. F. Murison 

Across the middle of the cage a stout barricade has been erected, and behind the barricade sits the Master, pale but defiant. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

A long, portable cage had been put together on the stage during the intermission, and within it the ten pacing beasts. The Real Latin Quarter |F. Berkeley Smith 

The cage went down by the run into the boat, and with a crash fell asunder. The Floating Light of the Goodwin Sands |R.M. Ballantyne