Either way, he and others think the campfire flares are important. Close-up of the sun reveals ‘campfires’ |Lisa Grossman |August 28, 2020 |Science News For Students 

When applied to old data, the method anticipated several powerful flares, although it missed some as well. The physics of solar flares could help scientists predict imminent outbursts |Emily Conover |July 30, 2020 |Science News 

Together, the small but ubiquitous flares could be a source of energy to the corona that astronomers haven’t accounted for. The closest images of the sun ever taken reveal ‘campfire’ flares |Lisa Grossman |July 16, 2020 |Science News 

In the second scenario, which Metzger and his colleagues published last year, the flare of energy escapes the magnetosphere and travels a large distance — up to 1 million times the radius of the magnetar. A Surprise Discovery Points to the Source of Fast Radio Bursts |Shannon Hall |June 11, 2020 |Quanta Magazine 

Specific features of those electron–gas interactions give each type of aurora its unique flare. Newfound ‘dunes’ is among weirdest of northern lights |Maria Temming |March 9, 2020 |Science News For Students 

Both are the result of magnetic activity on the sun, but a solar flare has more energy than a CME. The Sun Is Pummeling Earth. Now What? Solar Storms for Dummies |Lizzie Crocker |September 12, 2014 |DAILY BEAST 

In past flare-ups—documented on open carry forums—the grocery chain has said that it will comply with state laws. Gun Control Group Moms Demand Action Asking Kroger to Ban Guns in Stores |Brandy Zadrozny |August 18, 2014 |DAILY BEAST 

In 1859, astronomer Richard Carrington observed a strong solar flare that was directed at the Earth. About That ‘World-Ending’ Solar Storm... |Nicole Gugliucci |July 28, 2014 |DAILY BEAST 

The principle is sound, but the effect is small enough that another source entirely could be responsible for the extra flare-up. Could ‘Star Wars’ Be Right About Habitable Moons? |Matthew R. Francis |May 4, 2014 |DAILY BEAST 

His words have a nationalistic flare, and his story is gripping. China Propagandizes Rape Of Nanjing Survivors |Brendon Hong |December 29, 2013 |DAILY BEAST 

The old Negro watched the approaching flare of the head-light as he ran on, with a grim, defiant eye. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward 

His chuckle stopped as lightning flare threw the shadow of a man across the ground at Joseph's feet. Hooded Detective, Volume III No. 2, January, 1942 |Various 

In order to keep the flare-lights burning all kinds of materials had been sacrificed. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

Jim agreed at once, handed over the care of his flare-lights to one of the men, and prepared for action. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

The furnishings are many hued, the cushions a flare of color, and the pictures fantastically futuristic. Fifty Contemporary One-Act Plays |Various