There are landers looking at the interior geology and measuring the planet’s seismic activity. We need to go to Venus as soon as possible |Neel Patel |September 16, 2020 |MIT Technology Review 

A coffee’s chemical quality “comes down to geology,” Ehleringer says. How does a crop’s environment shape a food’s smell and taste? |Carolyn Beans |September 10, 2020 |Science News 

While an elemental analysis can authenticate a product’s terroir, it does not suggest that geology shapes flavor. How does a crop’s environment shape a food’s smell and taste? |Carolyn Beans |September 10, 2020 |Science News 

Although it’s an asteroid, its geology is compelling and diverse. The 5 best places to explore in the solar system—besides Mars |Neel Patel |August 17, 2020 |MIT Technology Review 

As someone who studies Martian geology, I’m definitely looking forward to 2031 but am grateful I don’t have to wait 11 years to study rocks from Mars. Meteorites From Mars Contain Clues About the Red Planet’s Geology |Arya Udry |June 17, 2020 |Singularity Hub 

Geology is important to making great wine, and it is particularly respected by French vintners. Wine, Watch Out! These Ciders Are Just as Good |Jordan Salcito |July 19, 2014 |DAILY BEAST 

Your background in geology has a big influence on this book. The Man Who Made America: Simone Winchester Talks New Book |Eric Herschthal |October 17, 2013 |DAILY BEAST 

Geology, literally, as I say in this book, “underlies everything.” The Man Who Made America: Simone Winchester Talks New Book |Eric Herschthal |October 17, 2013 |DAILY BEAST 

He recommended I take the Oxford University entrance exam in geology, and I did fairly well. The Man Who Made America: Simone Winchester Talks New Book |Eric Herschthal |October 17, 2013 |DAILY BEAST 

The Daily Pic: Just when he hit it big with geology, the Californian turned to sociology. Llyn Foulkes Changes Tacks |Blake Gopnik |July 31, 2013 |DAILY BEAST 

Second, geology, which takes account of all those actions which in process of time have been developed in our own sphere. Outlines of the Earth's History |Nathaniel Southgate Shaler 

In geology there are half a dozen divisions relating to particular branches of that subject. Outlines of the Earth's History |Nathaniel Southgate Shaler 

On the basis of this understanding the science of geology, which had in a way been founded by the Greeks, was revived. Outlines of the Earth's History |Nathaniel Southgate Shaler 

No man who ever was in a quarry or gravel pit will say so, much less one who has the least smattering of chemistry or geology. Gospel Philosophy |J. H. Ward 

He was very successful in diffusing among the young a love for the study of mineralogy and geology. The Every Day Book of History and Chronology |Joel Munsell