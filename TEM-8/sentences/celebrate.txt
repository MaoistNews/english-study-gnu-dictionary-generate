The family visited the club in April to celebrate Passover, a period that overlaps with several of the largest Secret Service charges. Trump’s businesses charged Secret Service more than $1.1 million, including for rooms in club shuttered for pandemic |David Fahrenthold, Josh Dawsey |September 17, 2020 |Washington Post 

As the pandemic stretches into the North American autumn, some are determined to not let it ruin Halloween—even though celebrating the holiday might come with risk. Zoom parties, throwing candy: How Halloween might happen during Covid-19 |Alexandra Ossola |September 17, 2020 |Quartz 

Bernard Tyson, the celebrated former CEO of Kaiser Permanente who passed this past November, was one of the most influential health care leaders of his generation. Bernard Tyson’s profound impact continues even after his death—through a new social investment fund |cleaf2013 |September 16, 2020 |Fortune 

The Capital Pride Alliance is hosting its first completely virtual Pride Stride, a nationwide event to celebrate National Coming Out Day. D.C. fall calendar filled with virtual events |Steph Purifoy |September 16, 2020 |Washington Blade 

To celebrate LGBT History Month, HBO Max is releasing the four-part docuseries “Equal.” Fall TV season brings handful of queer shows |Brian T. Carney |September 16, 2020 |Washington Blade 

Rashad was there to celebrate the release of the Civil Rights drama Selma. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

That would truly be a milestone to celebrate—until you see what that record “diversity” actually means. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

How do you celebrate when happy occasions are colored by loss and absence? Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

He was told he could go back home to his house arrest to celebrate the New Year with his wife and their two children. Russia’s Rebel In Chief Escapes House Arrest |Anna Nemtsova |December 30, 2014 |DAILY BEAST 

To celebrate the year, here are the top 10 anti-science salvos of 2014. 2014: Revenge of the Creationists |Karl W. Giberson |December 27, 2014 |DAILY BEAST 

They were just about to celebrate tabagie, or a solemn feast, over his last farewell. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Besides, these are only a few intimate friends who have assembled to celebrate my daughter's fte-day. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

All were there to celebrate the birth of the New Year, and to "play the game," however chastened they might feel on the morrow. Ancestors |Gertrude Atherton 

As we are then about to celebrate the purification, we have written unto you: and you shall do well, if you keep the same days. The Bible, Douay-Rheims Version |Various 

Couldn't you smoke it now, and then we'd go out next week and celebrate your recovery. Punch, or the London Charivari, Vol. 147, November 4, 1914 |Various