One of the most important rooms in the temple is the shrine of Sheik Adi ibn Musafir. Fighting Back With Faith: Inside the Yezidis’ Iraqi Temple |Michael Luongo |August 21, 2014 |DAILY BEAST 

Yezidis believe Sheik Adi was the possible reincarnation of Tawuse Melek. Fighting Back With Faith: Inside the Yezidis’ Iraqi Temple |Michael Luongo |August 21, 2014 |DAILY BEAST 

“They are army, not militia,” the sheik repeated throughout our conversation. The Brewing Battle for Baghdad |Jacob Siegel |August 3, 2014 |DAILY BEAST 

The sheik seemed to keep his young fighter on hand for meetings with journalists. The Brewing Battle for Baghdad |Jacob Siegel |August 3, 2014 |DAILY BEAST 

Sheik Rogo used to say my mother was fair: she gave one son to Obama and the other to Osama. Death Squads in Kenya’s Shadow War on Shabaab Sympathizers |Margot Kiser |April 6, 2014 |DAILY BEAST 

The sheik halted before her, spinning upon one foot, yet keeping his place. God Wills It! |William Stearns Davis 

The sheik, panic-struck, came in and made submission, revealing the treachery of the ranee's paramour and adviser. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Coming still nearer the sheik whispered: ‘He has stolen my best mare and ten oxen.’ The Animal Story Book |Various 

Gérard declining both suggestions, the sheik was obliged to leave, as night was at hand, and the lions might appear at any moment. The Animal Story Book |Various 

The sheik immediately asked what it was, and desired a dose for himself and the other men present. Silver Chimes in Syria |W. S. Nelson