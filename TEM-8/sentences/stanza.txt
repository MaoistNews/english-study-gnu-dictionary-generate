You can see it if you substitute two words in the final stanza of John Donne’s most famous poem—one I’ve always thought of as a meditation of sorts, and a passage I suspect we will all recognize. No Species Is an Island - Issue 105: Whale Songs |Roger Payne |September 8, 2021 |Nautilus 

The short film featured the singer delivering stanzas from British-Somali poet Warsan Shire, and celebrity cameos from the likes of Zendaya and Serena Williams. How ‘Lemonade’ Shifted Beyoncé’s Art And Career |Brande Victorian |April 23, 2021 |Essence.com 

Finally, just before the tenth stanza, a crestfallen Alvarado whispered that he was done. Boxers, Be Brave and Quit Before Your Brain Turns to Mush |Gordon Marino |October 25, 2013 |DAILY BEAST 

And writers seem to get it a lot, the relationship between words and page and phrase and paragraph, or stanza. The Writer and the Potter: Edmund De Waal on his New York Debut |Iain Millar |September 12, 2013 |DAILY BEAST 

But on January 28, on a typed draft, the final stanza is crossed out and replaced, with no hope for spring remaining. Sylvia Plath’s Darkest Sea: What an Unveiled Draft Poem Reveals |Olivia Cole |May 3, 2013 |DAILY BEAST 

The final stanza was the most fitting tribute I could imagine for my father. The Girl I Will Miss at Commencement |Robert Bookman |May 22, 2009 |DAILY BEAST 

E-book use on the iPhone exploded, with over a million downloads of the Stanza application alone. Don't Write Off Books |Peter Osnos |April 7, 2009 |DAILY BEAST 

No further hints need be offered except perhaps in regard to the last ten lines of the last stanza. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

In order to shew the riming more clearly, I have 'set back' the 3rd, 6th, and 7th lines of each stanza. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Evidently this disclaimer is a pretended one; the preceding stanza and ll. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

This stanza reproduces in the sixth line the last word of the first, and in the seventh the last word of the fourth. Frdric Mistral |Charles Alfred Downer 

Like Mirèio, the poem is divided into twelve cantos, and the form of stanza employed is the same. Frdric Mistral |Charles Alfred Downer