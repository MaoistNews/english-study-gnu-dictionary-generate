The ensuing clatter would wake him, and he could rescue his inventive ideas before they were lost to the depths of sleep. How sleep may boost creativity |Laura Sanders |December 14, 2021 |Science News 

Suddenly the clatter of a B‑1 bomber overhead shook the earth on which Azeema was pacing. The Women Who Fought to Defend Their Homes Against ISIS |Gayle Tzemach Lemmon |February 22, 2021 |Time 

But timing is always unpredictable, and all the clatter around the film could have swallowed up I Am Abraham. Making Lincoln Sexy: Jerome Charyn’s Fictional President |Tom LeClair |March 6, 2014 |DAILY BEAST 

Fielding tossed the surgical knife back on his cart with a loud clatter. Dr. Scarpetta Heads to Psych Ward |Daily Beast Promotions |February 23, 2009 |DAILY BEAST 

The sewing-machine made a resounding clatter in the room; it was of a ponderous, by-gone make. The Awakening and Selected Short Stories |Kate Chopin 

The invitation was accepted; and Mr. Bellamy's grand carriage drew up immediately with splash and clatter to the door. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

She went up in the mornings to Madame Lebrun's room, braving the clatter of the old sewing-machine. The Awakening and Selected Short Stories |Kate Chopin 

John Tubbs was one day doing his sums, when little Sam Jones pushed against him; and down went the slate with a horrid clatter. The Nursery, July 1873, Vol. XIV. No. 1 |Various 

Gilbert made a great clatter with his knife and fork, to conceal the laugh that he could not repress. The World Before Them |Susanna Moodie