The campaign against North Korea’s military leadership also affected the standing of numerous others, notably Pak Jong Chon, shown in the picture still in uniform but with a somewhat less exalted military rank. New Kim Jong Un Purge Suggests North Korea Is in Deep Shit |Donald Kirk |July 9, 2021 |The Daily Beast 

At about the end of this month the man who holds probably the most exalted position in contemporary English letters is to arrive here for a visit. TIME and Legendary Bookstore Partner on New Cover Exhibit |D.W. Pine |June 1, 2021 |Time 

The result is a no-name defense much like the exalted 1972 unit that backstopped the Dolphins’ famous undefeated season. The Dolphins Are Making A Name For Themselves With Their No-Name Defense |Ty Schalter |December 17, 2020 |FiveThirtyEight 

Hitchcock's reputation before the Cahiers group had not been exalted. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

For liberals, the courts never quite occupied that exalted a place. Only Eight Years of President Hillary Can Take the Supreme Court Away From Conservatives |Michael Tomasky |June 30, 2014 |DAILY BEAST 

But this exalted place in the bar pantheon was not easily won. The Rise and Fall…and Rise Again of the Old-Fashioned |Allison McNearney |June 14, 2014 |DAILY BEAST 

Glynn West had been fifteen years old in 1948, the holder of an exalted position in the eyes of the rest of us. The Great Paul Hemphill Celebrates the Long Gone Birmingham Barons |Paul Hemphill |March 29, 2014 |DAILY BEAST 

He was about to prove anew that there is no more exalted position in the city than that of a cop on patrol. My Patrol With the NYPD’s Bill Bratton |Michael Daly |March 14, 2014 |DAILY BEAST 

It was not an exalted niche to fill in life, but at least she had learned to fill it to perfection, and her ambitions were modest. Ancestors |Gertrude Atherton 

Thou hast exalted my dwelling place upon the earth and I have prayed for death to pass away. The Bible, Douay-Rheims Version |Various 

This new exalted state was very marvellous; for while it lasted he welcomed all that was to come. The Wave |Algernon Blackwood 

In all letters which the son writes to his father he uses the most exalted titles and honourable phrases he can imagine. Our Little Korean Cousin |H. Lee M. Pike 

Power, and inherited influence, and exalted social position have a deadly insinuation. Ancestors |Gertrude Atherton