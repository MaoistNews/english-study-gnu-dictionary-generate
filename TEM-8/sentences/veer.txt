Almost immediately, my co-founder made it clear that my project management style was veering toward disaster. How to avoid being controlled by your digital tools |Michael J. Coren |February 7, 2021 |Quartz 

The pandemic's effect on sleepEpstein has found that pandemic sleep habits have veered in two directions — better and worse — which have had an effect on naps. Nap time is the new coffee break. Here’s how to make the most of it. |Galadriel Watson |February 1, 2021 |Washington Post 

Most notably, Miami-Dade County veered 22 points to the right in 2020 after drifting left in both 2012 and 2016. How The Frost Belt And Sun Belt Illustrate The Complexity Of America’s Urban-Rural Divide |Geoffrey Skelley (geoffrey.skelley@abc.com) |January 27, 2021 |FiveThirtyEight 

The studied elegance of his “less-is-more” aesthetic keeps his film from veering into the manipulative territory that could easily undermine the cumulative emotional power it works so carefully to build. Firth, Tucci face tragedy with restraint in ‘Supernova’ |John Paul King |January 26, 2021 |Washington Blade 

So unless the sponsor really veers off course with the acquisition target, the investors are likely to go along with the acquisition. WTF is a SPAC? |Tim Peterson |January 21, 2021 |Digiday 

Urban Outfitters has a track record of putting out products that veer into attention-grabbing, supposedly edgy territory. Who Designed Urban Outfitters's Bloody Kent State Shirt? They Won't Say |Asawin Suebsaeng |September 15, 2014 |DAILY BEAST 

“We were taught with Reefer Madness that it was a hard-core drug and we should veer away from it,” she says. Ganjapreneurs Line Up to Get Florida High |Abby Haglage |July 30, 2014 |DAILY BEAST 

The woman allegedly decided to hit her brakes suddenly and veer toward an exit, losing Tirico. World Cup Anchor Mike Tirico’s Bizarre History: Reports of Stalking and Sexual Harassment |Marlow Stern |July 1, 2014 |DAILY BEAST 

And yet Cinco de Mayo can veer so, so quickly into Cinco de Weirdly Racist Douchebaggery. How Not to Be Awful This Cinco de Mayo |Kelly Williams Brown |May 4, 2014 |DAILY BEAST 

It is also his tendency to occasionally veer off-script, as he did recently with his attacks on Bill Clinton and Monica Lewinsky. Can Rand Paul Make It Rain? |David Freedlander |March 12, 2014 |DAILY BEAST 

A moment later they were edging their way down the declivity of what once had been a railroad track, at last to veer. The White Desert |Courtney Ryley Cooper 

If it should veer to the east before the second frame could be constructed the peril would be great. Toilers of the Sea |Victor Hugo 

It requires another head than mine to veer round so often (changer si souvent de systame). History Of Friedrich II. of Prussia, Vol. VII. (of XXI.) |Thomas Carlyle 

In her preoccupation she let her fork veer away from her plate. The Opened Shutters |Clara Louise Burnham 

Jean Greb, seeing the peril, had chosen to climb above the steep portion on the west slope, rather than veer to the east. Rescue Dog of the High Pass |James Arthur Kjelgaard