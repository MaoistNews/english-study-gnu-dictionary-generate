Around the time of the event, Vogue published retrospective and historical content packages that were built out of previous galas, setting May 2020 up to be record-breaking month as well. How Vogue’s international approach to audience data helped it reach record readers |Kayleigh Barber |December 18, 2020 |Digiday 

Let her do it again this week with Style Invitational Week 1415, the first of our two second-chance contests, covering the first half of the ones we did since last year’s retrospective. Style Conversational Week 1415: Slaphappy seconds |Pat Myers |December 17, 2020 |Washington Post 

Musa Mayer, Guston’s daughter and head of the Guston Foundation, said that the decision represents real progress and that she looks forward to celebrating the retrospective when it opens. After intense criticism, Philip Guston exhibition rescheduled for 2022-24 |Peggy McGlone |November 5, 2020 |Washington Post 

Last year, the Bauhaus’s 100th anniversary was celebrated with a host of exhibits, events and retrospectives around the world—and inspired a flurry of thoughtful writing about the movement. Can Europe rebuild the Bauhaus? |claychandler |September 29, 2020 |Fortune 

Instead, retrospective evaluations correlated most closely to peak and end pain levels. How 2020 is like a colonoscopy |jakemeth |September 10, 2020 |Fortune 

In 1980, a retrospective of the artist at the Museum of Modern Art in New York captivated Hockney. The Many Lives of Artist David Hockney |William O’Connor |November 23, 2014 |DAILY BEAST 

To celebrate a retrospective box set, entitled Nothing Has Changed, Bowie has released a video for one of two new songs included. David Bowie Goes Big Band in New Music Video |Alex Chancey, The Daily Beast Video |November 14, 2014 |DAILY BEAST 

“When Tibor died we did a retrospective of MCo., and the lamp was the last thing you saw,” she says. The Singular Artist of New Yorkistan |Lizzie Crocker |November 14, 2014 |DAILY BEAST 

Frank Gehry is enjoying a major moment: his retrospective is in full swing in Paris and his latest creation opens today. Frank Gehry Is Architecture’s Mad Genius |Sarah Moroz |October 27, 2014 |DAILY BEAST 

Hopper (1882-1967), had his first major retrospective at the Whitney in 1964. The Stacks: Edward Hopper’s X-Ray Vision |Hilton Kramer |October 25, 2014 |DAILY BEAST 

But statutes which merely alter the procedure, if they are in themselves good statutes, ought to be retrospective. The History of England from the Accession of James II. |Thomas Babington Macaulay 

It is always to be remembered that retrospective legislation is bad in principle only when it affects the substantive law. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Statutes creating new crimes or increasing the punishment of old crimes ought in no case to be retrospective. The History of England from the Accession of James II. |Thomas Babington Macaulay 

But the bill against Duncombe really was, what the bill against Fenwick was not, objectionable as a retrospective bill. The History of England from the Accession of James II. |Thomas Babington Macaulay 

There was not a side nor retrospective glance to disturb the serenity of her large blue eyes. The Butterfly House |Mary E. Wilkins Freeman