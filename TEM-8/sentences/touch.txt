To let us know how your voting experience goes, here’s how to sign up and get in touch. Please Tell Us If You Have Any Trouble Voting This Year |by Rachel Glickhouse |September 8, 2020 |ProPublica 

We may not be able to hug each other, for fear of infection, but we can keep in touch digitally. Fortune’s 40 Under 40 honorees in tech defy the pandemic |rhhackettfortune |September 2, 2020 |Fortune 

A battery-powered system of fans stokes your fire with the touch of a button. 5 Portable Grills for All Your Outdoor Cooking Needs |Amy Marturana Winderl |September 2, 2020 |Outside Online 

This requires a data-driven approach, measuring gaps as well as progress while still employing the human touch overall. 3 ways to make sure corporate diversity and inclusion efforts have a lasting impact |matthewheimer |August 31, 2020 |Fortune 

That offer prompted the employee to get in touch with the FBI, which quickly began monitoring all of Kriuchkov’s actions. The FBI broke up a Russian hacker plot to extort millions from Tesla |Aaron Pressman |August 28, 2020 |Fortune 

Set a heatproof bowl over a pot of gently simmering water, making sure that the bowl does not touch the surface of the water. Carla Hall’s Christmas Day Treat: Rum Balls |Carla Hall |December 25, 2014 |DAILY BEAST 

“I had a small touch of prison a couple of times,” Mailer writes to Ehrlichman. Mailer’s Letters Pack a Punch and a Surprising Degree of Sweetness |Ronald K. Fried |December 14, 2014 |DAILY BEAST 

The quote appears on the bronze plaque the players touch before they take the field for home games. A West Point MVP Who Never Played a Down |Nicolaus Mills |December 13, 2014 |DAILY BEAST 

The former vice president is not known as a soft touch, but for a brief moment in 2011, he seemed to hold Clinton in high regard. Remember When Republicans Loved Hillary Clinton? |Ben Jacobs |December 1, 2014 |DAILY BEAST 

Too many designers, Nagrani reckons, stop thinking like entrepreneurs and lose touch with their customers. The Hot Designer Who Hates Fashion: VK Nagrani Triumphs His Own Way |Tom Teodorczuk |December 1, 2014 |DAILY BEAST 

The Goliath wouldn't answer; the Dublin said the force was coming off, and we could not get into touch with the soldiers at all. Gallipoli Diary, Volume I |Ian Hamilton 

He made me think of an old time magician more than anything, and I felt that with a touch of his wand he could transform us all. Music-Study in Germany |Amy Fay 

He thought they were now in touch with our troops at "X" but that they had been through some hard fighting to get there. Gallipoli Diary, Volume I |Ian Hamilton 

Thanks to Berthier's admirable system, Bonaparte was kept in touch with every part of his command. Napoleon's Marshals |R. P. Dunn-Pattison 

Tausig possessed this repose in a technical way, and his touch was marvellous; but he never drew the tears to your eyes. Music-Study in Germany |Amy Fay