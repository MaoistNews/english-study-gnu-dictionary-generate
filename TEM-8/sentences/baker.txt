Gratitude turned into an obligation to “repay the good that had come to me,” Baker says. Julien Baker questioned her faith. Music helped her embrace the uncertainty. |Sonia Rao |February 26, 2021 |Washington Post 

“We need to be prepared for offseason outbreaks and potentially large outbreaks,” Baker says. COVID-19 precautions may be reducing cases of flu and other respiratory infections |Jennifer Welsh |February 2, 2021 |Science News 

We should also be grateful for the bakers and cooks, whose risk of death rose more than 50 percent. Everyone should be wearing N95 masks now |Joseph G. Allen |January 26, 2021 |Washington Post 

Baker has been critical of the city’s decision not to let members of the public speak or be heard during City Council meetings – not just about this topic but during the entire pandemic. Chula Vista Police Chief Says She Didn’t Know Department Shared Data With Feds |Gustavo Solis |January 20, 2021 |Voice of San Diego 

“It seems like as it’s gotten colder and wetter, you’re seeing structures getting more elaborate,” Baker says. When it comes to COVID-19 risk, what counts as ‘outdoor’ dining? |Sara Kiley Watson |January 11, 2021 |Popular-Science 

The one we use was by a chappie called Theodore Baker, 1894. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

He becomes especially earnest when the conversation turns to his role as The Baker in Into the Woods. New ‘Late Late Show’ Host James Corden Would Like to Manage Your Expectations |Kevin Fallon |December 18, 2014 |DAILY BEAST 

It appeared that Baker would again have to face Clark in the forthcoming run-off. Honoring The Late John Doar, A Nearly Forgotten Hero Of The Civil Rights Era |Gary May |November 15, 2014 |DAILY BEAST 

Both state and federal law required that the disputed ballots be counted, which gave the victory to Wilson Baker. Honoring The Late John Doar, A Nearly Forgotten Hero Of The Civil Rights Era |Gary May |November 15, 2014 |DAILY BEAST 

Charlie Baker in Massachusetts and Charles Rauner in Illinois scored upset gubernatorial wins with pro-minimum messages. To Make Their Victory Durable, the GOP Must Fix the Minimum Wage |Dmitri Mehlhorn |November 6, 2014 |DAILY BEAST 

That Hicks—the damned —— —— —— he come t' Baker's as they hooked up t' leave the Spring. Raw Gold |Bertrand W. Sinclair 

Toward eight o'clock a pretty, capable-looking girl of twelve came out of the house and bought a loaf of bread at the baker's. The Box-Car Children |Gertrude Chandler Warner 

On the night when the children had crept so quietly away from the baker's wife, Jess had forgotten to take Benny's bear. The Box-Car Children |Gertrude Chandler Warner 

But I felt no anxiety: I knew the "silent baker" would prove deaf and dumb. Prison Memoirs of an Anarchist |Alexander Berkman 

Going ten paces up the street he saw the baker's shop where he supplied himself. Balsamo, The Magician |Alexander Dumas