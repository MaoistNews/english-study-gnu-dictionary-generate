My grandmother was a great cook and she only let me and my cousin Patricia help her. How French Baking Was Brought to Virginia |Eugene Robinson |February 5, 2021 |Ozy 

So today, when I finally got to hold the flute in my hands, I felt like it was my grandmother again. ‘FLUTE EMERGENCY’: A musician forgot his $22,000 instrument on a Chicago train. A homeless man found it. |Andrea Salcedo |February 5, 2021 |Washington Post 

One chief told me how his grandmother took his mother out to the wilderness for a year so that she would be safe. How is Alaska leading the nation in vaccinating residents? With boats, ferries, planes and snowmobiles. |Cathy Free |February 4, 2021 |Washington Post 

This was not the old-school, traditional funeral home that you think of with grandmother in the lilac suit, in polyester, with the silken sheets around her and all the flowers. How to Be Better at Death (Ep. 450) |Maria Konnikova |February 4, 2021 |Freakonomics 

Eighteen-year-old Stella, known as Lala to all but her grandmother Wilma, is about to give birth to her first child. ‘How the One-Armed Sister Sweeps Her House,’ by Cherie Jones, is a stunning debut |Hamilton Cain |February 2, 2021 |Washington Post 

In 2011, he was arrested while visiting his grandmother in Iran, charged with espionage, and sentenced to death. An American Marine in Iran’s Prisons Goes on Hunger Strike |IranWire |December 18, 2014 |DAILY BEAST 

Through her haircare line, named for her grandmother, Jessie Branch, Titi Branch was revolutionary. Goodbye To A Natural Hair Guru: Miss Jessie's Cofounder Titi Branch Dead At 45 |Danielle Belton |December 16, 2014 |DAILY BEAST 

Why is a straight grandmother the leading advocate for gays in Cameroon? The Straight Hero of Cameroon’s Gays |Jay Michaelson |December 10, 2014 |DAILY BEAST 

There is this trinity of female mourning: for your grandmother, your mother, and your unborn daughter. Meghan Daum On Tackling The Unspeakable Parts Of Life |David Yaffe |December 6, 2014 |DAILY BEAST 

The seven-year-old Detroit girl was sleeping on the couch as her grandmother sat next to her watching television. Worse Than Eric Garner: Cops Who Got Away With Killing Autistic Men and Little Girls |Emily Shire |December 4, 2014 |DAILY BEAST 

The year before they had spent part of the summer with their grandmother Pontellier in Iberville. The Awakening and Selected Short Stories |Kate Chopin 

Alila's grandmother prepared a quantity of betel before the new baby was born. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

That was a new idea to Hettie; and it puzzled her little brain for a minute: then she laughed out, "Shall I be their grandmother?" The Nursery, July 1873, Vol. XIV. No. 1 |Various 

The narrow individualism of the nineteenth century refused to recognize the social duty of supporting somebody else's grandmother. The Unsolved Riddle of Social Justice |Stephen Leacock 

At her bosom she wore a great brooch, containing intertwined locks of a grandfather and grandmother long since defunct. The Joyous Adventures of Aristide Pujol |William J. Locke