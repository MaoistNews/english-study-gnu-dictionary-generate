Yet these questions feel a lot like coercion by government officials, an incursion into the cultural promise of the First Amendment. Hey, Democrats: Hands off Fox News’s cable carriers |Erik Wemple |February 24, 2021 |Washington Post 

They flipped the lower chambers in North and South Carolina — an early incursion into a region that they’d fully convert over the next two decades. Republicans now enjoy unmatched power in the states. It was a 40-year effort. |David Byler |February 18, 2021 |Washington Post 

The incursion became the latest — and, it appears, by far the worst — in a string of hacks targeting the software supply chain. The U.S. Spent $2.2 Million on a Cybersecurity System That Wasn’t Implemented — and Might Have Stopped a Major Hack |by Peter Elkind and Jack Gillum |February 2, 2021 |ProPublica 

The violent, deadly incursion into the Capitol last week produced a shocking stream of images, and among them was the bizarre sight of some people wearing spacesuit-like transparent pieces of gear over their heads. Those ‘gas masks’ at the Capitol were actually escape hoods |Rob Verger |January 11, 2021 |Popular-Science 

ASRI, says Planetary Health Alliance head Myers, “is a fantastic example of how to prevent the incursions into wildlife habitat that are at the heart of a lot of emerging infectious disease.” The fight to stop the next pandemic starts in the jungles of Borneo |Brian Barth |December 2, 2020 |Popular-Science 

Griswold was undeniably an incursion on democratic powers with a definite whiff of activism. The Right Wing Screams for the Wambulance Over Gay Marriage Ruling |Walter Olson |October 13, 2014 |DAILY BEAST 

And I remember once there was a Nicaraguan Sandinista incursion in Honduras and the Nicaraguans denied it. Whit Stillman on the 20th Anniversary of ‘Barcelona’, His New Amazon Series, and the Myth of the Ugly Expat |Michael Weiss |August 10, 2014 |DAILY BEAST 

And at midnight Spanish time, the Nicaraguans said, ‘Okay, we had an incursion, but it was justified.’ Whit Stillman on the 20th Anniversary of ‘Barcelona’, His New Amazon Series, and the Myth of the Ugly Expat |Michael Weiss |August 10, 2014 |DAILY BEAST 

Much of the American left is critical of Israel, particularly since its incursion into Gaza. Even Left-Wing Politicians Can’t Quit Israel |Tim Mak |July 30, 2014 |DAILY BEAST 

Now Israel is promising to expand its ground incursion in what it calls a second phase of its operation. Fleeing Israeli Troops, Gaza Muslims Find Refuge in a Christian Church |Jesse Rosenfeld |July 23, 2014 |DAILY BEAST 

"I never heard of anything of the kind in Ormond's history," said Wanhope, tolerant of the incursion. Questionable Shapes |William Dean Howells 

Hence the incursion of a new weed is generally first noticed along the highway or the railroad. A Year in the Fields |John Burroughs 

It was the Radical quarter that was thus invaded, and its occupants were not disposed tamely to submit to the incursion. The Strand Magazine, Volume V, Issue 28, April 1893 |Various 

Less brilliant, but more solid, were the advantages which he had to expect from an incursion into the territories of the League. The Thirty Years War, Complete |Friedrich Schiller 

So Lætitia had her choice between an explicit statement of her meaning, and an unsupported incursion into the adagio. Somehow Good |William de Morgan