British law puts the right to a fair trial above freedom to report in an effort to secure an unprejudiced jury. Rebekah Brooks Phone Hacking Trial Begins In London |Peter Jukes, Nico Hines |October 28, 2013 |DAILY BEAST 

That countenance could never be once looked upon by an unprejudiced eye, without making an immediate interest in the heart. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

To make a response is difficult, because your article is not written in a fair, unprejudiced spirit. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

There is no one, however, who so needs competent, unprejudiced advice as the inventor. How to Succeed as an Inventor |Goodwin B. Smith 

Nothing will ever make me admit and no unprejudiced mind can admit that what was once food has become a horrible poison. More Hunting Wasps |J. Henri Fabre 

The bitterness of war had not yet receded far enough into the past to allow of unprejudiced judgment. In Connection with the De Willoughby Claim |Frances Hodgson Burnett