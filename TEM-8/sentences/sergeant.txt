Again, the prosecutor made a deal with the sergeant for probation. How Criminal Cops Often Avoid Jail |by Andrew Ford, Asbury Park Press |September 23, 2020 |ProPublica 

At least one officer who oversees the sergeant in charge of the ShotSpotter technology doesn’t think so. ShotSpotter Sensors Send SDPD Officers to False Alarms More Often Than Advertised |Kara Grant |September 22, 2020 |Voice of San Diego 

Every year, the Sheriff’s Department charges the city $197,000 per deputy, $206,000 per detective and $255,000 per sergeant. Morning Report: Oceanside Reboots Top Cop Search |Voice of San Diego |September 14, 2020 |Voice of San Diego 

As a former village public safety officer sergeant and an outspoken voice for justice in Alaska villages, Potts said she knew they might want her help with the election, which was weeks away. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

The report notes that the police sergeant leading the department’s Internal Affairs investigation of the shooting sought to interview Browder. Police Review Board Was Denied Docs, Interviews in 2015 Shooting Review |Sara Libby |July 22, 2020 |Voice of San Diego 

“Stay in formation,” a sergeant from the ceremonial unit said over a public address system to the cops along the street. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

Prince may have pranced around like a carefree libertine onstage, but in rehearsal he was more drill sergeant than sprite. Speed Read: The Juiciest Bits From the History of ‘Purple Rain’ |Jennie Yabroff |January 1, 2015 |DAILY BEAST 

While the desk sergeant ran a background check, he was roughed up by another officer in the lock-up. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 

As I forced my exhausted body to exercise, I yelled at my legs like a drill sergeant, demanding five more minutes or one more set. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

A squad soon arrived to take him away, and I saw the sergeant punch him in the face even though he went quietly. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

Napoleon landed at Elba at an early hour in disguise, with a sergeant's company of marines. The Every Day Book of History and Chronology |Joel Munsell 

Fixing bayonets and leaving the sergeant dead in the doorway, they charged again into the mass of the enemy. The Red Year |Louis Tracy 

He turned his arm so that we could see the ripped stitching where his sergeant's stripes had been cut away. Raw Gold |Bertrand W. Sinclair 

When the French generals reached the Austrian end they found a sergeant of engineers actually proceeding to fire the fuse. Napoleon's Marshals |R. P. Dunn-Pattison 

The Sergeant stared at him hard, saw that both he and his friend wore evening dress, and grew proportionately respectful. Dope |Sax Rohmer