The stock closed at $253, more than double the $120 the price at which the company and its bankers initially valued them. Investors in Snowflake’s IPO should prepare for a second-day slump |Oliver Staley |September 17, 2020 |Quartz 

Taking the company public in 2012 at a valuation of about $2 billion, ServiceNow today has a stock market value of almost $90 billion. Meet Snowflake, one of the buzziest tech IPOs ever |Aaron Pressman |September 15, 2020 |Fortune 

As for value stocks, investors are getting the message on that front too. Is M&A back? Investors hope so, and that’s lifting global stocks |Bernhard Warner |September 15, 2020 |Fortune 

The worst crash in the company’s stock was a plunge of almost 95%. Why even the best stocks have to crash |Ben Carlson |September 13, 2020 |Fortune 

Nikola's stock lost 11 percent on Thursday and is down an additional 15 percent in Friday morning trading. Nikola stock craters after chairman fails to rebut fraud allegations |Timothy B. Lee |September 11, 2020 |Ars Technica 

In our headlong quest for a legally perfect society, we don’t take the time to take stock of what‘s been created so far. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

If his 20s were about traveling and his 30s “about taking stock,” he hopes his 40s will be about “building and expanding.” William, Kate, and Jay Z’s Favorite Art Star: Alexander Gilkes' World of Rock Stars and Royalty |Tim Teeman |December 10, 2014 |DAILY BEAST 

The obnoxious meddling journalist is a stock character in fiction. I Blame People Who Blame the Media: Robert McCulloch’s Tone-Deaf Speech |Arthur Chu |November 25, 2014 |DAILY BEAST 

On Friday, the stock market hit new highs—even as wages were stagnating. With Immigration Move, Obama and the Welfare Party Strike Again |Lloyd Green |November 24, 2014 |DAILY BEAST 

Yes, the stock market is booming but overwhelmingly Americans are unhappy with their economic situation—and for good reason. Voters Remind D.C. That the Economy Still Sucks |Stuart Stevens |November 6, 2014 |DAILY BEAST 

Neither privately owned nor government stock is entitled to voting power. Readings in Money and Banking |Chester Arthur Phillips 

I didn't take much stock in the yarn at the time, but I'm beginning to think he had it straight. Raw Gold |Bertrand W. Sinclair 

One day she had heard a man say, "If there is a drought we shall have the devil to pay with our stock before winter is over." Ramona |Helen Hunt Jackson 

Cotton exchanges reopened on November 16, and stock exchanges opened for restricted trading shortly thereafter. Readings in Money and Banking |Chester Arthur Phillips 

The white ranchmen in the valley were all fencing in their lands; no more free running of stock. Ramona |Helen Hunt Jackson