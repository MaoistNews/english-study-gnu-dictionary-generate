So for now, he’s stuck with some students jumping over pillows. Kids are shooting hoops with rolled up socks, but pandemic physical education is not canceled |Kelly Field |February 12, 2021 |Washington Post 

Their line now includes weighted robes, weighted sleep masks, sheets, pillows, and more. Best weighted blanket: Sleep like a baby with our comfy bedding picks |PopSci Commerce Team |February 12, 2021 |Popular-Science 

The more strongly I disidentify, the more likely I am to get rid of that pillow. MyPillow boycott: How a product can spark an identity crisis |Elizabeth Chang |February 12, 2021 |Washington Post 

The problem was that I'd get it all over the pillows and sheets. Hints From Heloise: New scam tied to coronavirus pandemic |Heloise Heloise |February 12, 2021 |Washington Post 

However, if you’re trying different approaches to improve your sleep—a relaxing pillow spray, avoiding screens before bed, or winding down with a warm shower—a tracking app can help you figure out what works best for you. Best fitness tracker: Hit your health goals and sleep better with these picks |PopSci Commerce Team |January 27, 2021 |Popular-Science 

The forums and message boards all cite “waking up to loose strands on your pillow” as a real indicator of significant hair loss. Birth Control Made My Hair Fall Out, and I’m Not the Only One |Molly Oswaks |October 14, 2014 |DAILY BEAST 

I moved my head one inch off the pillow to find out which it was, and something growled. The Stacks: Pete Dexter on What It’s Like to Lose the Knack of Having Fun |Pete Dexter |September 20, 2014 |DAILY BEAST 

We were about to go to sleep, but I decided to tease him about his weird habit of having the pillow a certain way on the bed. I Was Pregnant When He Hit Me. Here's #WhyIStayed. |Anonymous |September 10, 2014 |DAILY BEAST 

Her pillow talk with Coulson when they were lovers for six years had never included mention of it. Murdoch on the Rocks: How a Lone Reporter Revealed the Mogul's Tabloid Terror Machine |Clive Irving |August 25, 2014 |DAILY BEAST 

Her demand was simple: I will not marry Loras Tyrell (i.e., the “renowned pillow-biter” to whom Papa Tywin has betrothed her). Best ‘Game of Thrones’ Season Yet |Andrew Romano |June 16, 2014 |DAILY BEAST 

At last Aristide fed him desperately, dandled him eventually to sleep, and returned to an excited pillow. The Joyous Adventures of Aristide Pujol |William J. Locke 

She suddenly sank back upon the pillow and gave up to bitter anguish, when she recalled what had followed. The Homesteader |Oscar Micheaux 

Hilda bent over her trembling head whose right side pressed upon the pillow. Hilda Lessways |Arnold Bennett 

Mafuta rose and left the tent, and Tom, turning on his side, observed the Bible lying on the pillow. Hunting the Lions |R.M. Ballantyne 

She had sunk down beside the bed, her head was buried in the pillow; she was sobbing wildly. The World Before Them |Susanna Moodie