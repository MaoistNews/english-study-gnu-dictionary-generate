Frustration about the uncertain future also vexed some alumni. Ivy League sports were shut down quickly by coronavirus. A restart is proving much slower. |Glynn A. Hill |February 10, 2021 |Washington Post 

Very few campuses had the resources to extend credit to students in the hopes that alumni would earn enough after graduation to pay back the cost of earning their degrees. Even forgiving student loans won’t solve the higher education funding crisis |Elizabeth Shermer |January 22, 2021 |Washington Post 

Those alumni all graduated on time from high school, most to study at a two- or four-year college, and others to join the military or begin trade work. Next One Up wraps teen boys in support and watches them fly |Tatyana Turner |January 19, 2021 |Washington Post 

Thousands of law school alumni and students push for disbarment of Sens. 157 law deans denounce attempted insurrection and effort to decertify election — but don’t name names |Valerie Strauss |January 12, 2021 |Washington Post 

One of the important things that I would like to do as dean is to continue to engage alumni. Helping scientists succeed |Katie McLean |December 18, 2020 |MIT Technology Review 

She says she met Cosby, a Temple alumnus and big-time donor to the university, in November 2002. How Bill Cosby Allegedly Silenced His Accusers Through A Tabloid Smear Campaign |Marlow Stern |November 21, 2014 |DAILY BEAST 

A former House Budget chairman and Fox News alumnus, Kasich was a libertarian leaning fiscal conservative before it was cool. The Secret GOP Swing State Election Romp |John Avlon |October 28, 2014 |DAILY BEAST 

The best known was Brotherhood alumnus Sayyid Qutb, whom the Egyptian state executed in 1966. Why the Caliphate Will Devour Its Children |Philip Jenkins |July 11, 2014 |DAILY BEAST 

The celebrated nanny college counts as its most famous (if fictional) alumnus Mary Poppins. More Details About Prince George's New Nanny |Tom Sykes |March 21, 2014 |DAILY BEAST 

Hayes was very much a creature of the left, a proud alumnus of The Nation and In These Times. Steve Kornacki, MSNBC’s Brainy Replacement for Chris Hayes, Just Wants to Be Useful |David Freedlander |March 21, 2013 |DAILY BEAST 

Alumnus, al-um′nus, n. one educated at a college is called an alumnus of it:—pl. Chambers's Twentieth Century Dictionary (part 1 of 4: A-D) |Various 

It is a recollection blended of many feelings, that which the recurring Commencement brings to the alumnus. From the Easy Chair, series 2 |George William Curtis 

The University, however, shares the attachment of the alumnus. Oxford and Her Colleges |Goldwin Smith 

He didn't look like a Dumbarton Oaks product: I thought he was more likely an alumnus of some private detective agency. Lone Star Planet |Henry Beam Piper and John Joseph McGuire 

Some high school alumnus in whose heart there is appreciation of Rome's gift to us might present a book to his Alma Mater. A Handbook for Latin Clubs |Various