I understand that sometimes undergrowth has to be cleared to ensure that the healthiest trees grow big and strong. At the Copenhagen Zoo, Humans Can Be Animals |Kevin Bleyer |March 28, 2014 |DAILY BEAST 

But I admit that I first make my way through a fast-growing undergrowth of business. Emma Donoghue: The How I Write Interview |Noah Charney |October 24, 2012 |DAILY BEAST 

The advance had to be carefully made, for the country was rough, wooded, and covered with a dense undergrowth of bushes. The Courier of the Ozarks |Byron A. Dunn 

In the dense undergrowth hummed and rustled a hidden life of greater mystery. The Red Year |Louis Tracy 

Then all this stopped and on the wet undergrowth again there was a movement like the zig-zag stripe of the tiger's skin. Kari the Elephant |Dhan Gopal Mukerji 

I could feel in the distance the shiver of the undergrowth of grass and saplings indicating the way the animals had passed. Kari the Elephant |Dhan Gopal Mukerji 

At the end of ten minutes, leaving the undergrowth, David ran across a prairie which ended with the bluff of the river. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue