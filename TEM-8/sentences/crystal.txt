This material is soft and deformable, consisting of molecules loosely bound in a crystal structure. The Shape-Shifting Squeeze Coolers |Marcus Woo |August 24, 2020 |Quanta Magazine 

Juno has also seen evidence that violent storms in deeper cloud layers sometimes toss ice crystals high above where they’re normally found. ‘Exotic’ lightning crackles across Jupiter’s cloud tops |Lisa Grossman |August 5, 2020 |Science News 

This more basic environment causes little crystals of calcium carbonate to form. This ‘living’ concrete slurps up a greenhouse gas |Carolyn Wilke |May 6, 2020 |Science News For Students 

That will tell you how many grams of sugar crystals had grown. Rock Candy Science 2: No such thing as too much sugar |Bethany Brookshire |April 30, 2020 |Science News For Students 

At the bottom, I calculated the mean — the average crystal growth — for each group. Rock Candy Science 2: No such thing as too much sugar |Bethany Brookshire |April 30, 2020 |Science News For Students 

Some of the video was crystal clear, but in other footage the figures were just fuzzy shadows in black and white. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

Sailor Moon Crystal is expected to wrap up its initial storylines by the end of the year. ‘Sailor Moon’ Is an Oasis for Superheroes Who Can Save the Universe in Heels |Rich Goldstein |November 26, 2014 |DAILY BEAST 

Sailor Moon Crystal is a more ambitious retelling of multiple Sailor Moon story arcs. ‘Sailor Moon’ Is an Oasis for Superheroes Who Can Save the Universe in Heels |Rich Goldstein |November 26, 2014 |DAILY BEAST 

Crystal is a onetime member of the Avengers, and in various storylines is also married to Quicksilver. Inside Marvel’s Phase 3: How ‘The Avengers’ Cross Paths with Black Panther and the New Superheroes |Marlow Stern |October 30, 2014 |DAILY BEAST 

Hers is a particular brand of essay: writing at its most crystal clear, subject matter at its most slippery and interesting. From Didion to Dunham, Female Essayists Seize the Day |Lucy Scholes |October 17, 2014 |DAILY BEAST 

The long axis of the hip-roof crystal is often so shortened that it resembles the envelop crystal of calcium oxalate. A Manual of Clinical Diagnosis |James Campbell Todd 

The soul of music which is embodied in them is imprisoned within wood and crystal, and is no more heard of men. Violins and Violin Makers |Joseph Pearce 

Jessie had bought a galena crystal mounted, as that was more satisfactory, the book said. The Campfire Girls of Roselawn |Margaret Penrose 

You look at a clear perfect rich colour, as it were, through the purest crystal. Violins and Violin Makers |Joseph Pearce 

There were silver and gold, as she had said there would be, and crystal which glittered like the gems which the women wore. The Awakening and Selected Short Stories |Kate Chopin