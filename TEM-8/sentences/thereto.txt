In 1205 wheat was worth 12 pence per bushel, which was cheap, as there had been some years of famine previous thereto. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

The Admiral came aboard and between us we tried to size up the new situation and to readjust ourselves thereto. Gallipoli Diary, Volume I |Ian Hamilton 

Can you furnish me with a copy of your report to Mr. Spring Rice, or something relating thereto? Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

It is true that an adverse claimant cannot give any title to her husband's land that would bar her right thereto. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

And a license to do a particular act necessarily involves any act essential thereto. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles