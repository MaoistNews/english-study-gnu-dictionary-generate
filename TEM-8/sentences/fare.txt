In addition, the hyper-competition between a slew of low-cost carriers had made it hard for airlines to make much money as they lured potential customers with the cheapest fares. India’s refusal to bail out airlines is putting them on the verge of collapse |Niharika Sharma |August 10, 2020 |Quartz 

The Metropolitan Transit System made a decision several years ago to step up its enforcement of fare evasions and other quality of life issues on buses and trolleys. Morning Report: MTS Enforcement Chief Departing |Voice of San Diego |July 28, 2020 |Voice of San Diego 

Earlier this year, a Voice of San Diego investigation revealed how fare evasion tickets can terrorize those who receive them – and how most of the roughly 1,470 fare evasion tickets the agency wrote in a single week went unpaid a year later. MTS Police Chief Departs as Agency Pulls Back Enforcement Push |Lisa Halverstadt |July 27, 2020 |Voice of San Diego 

In September, the agency will roll out a pilot fare diversion program with reduced fines and the option to perform community service instead. Morning Report: MTS Doled Out Violations Disproportionately |Voice of San Diego |July 27, 2020 |Voice of San Diego 

MTS has for years decided that its citations for offenses such as fare evasion or loitering, the vast majority of its enforcement actions, do not need to be retained longer than 60 days. MTS Purged Body Camera Footage Before Man’s Attorney Could Access it |Lisa Halverstadt |July 21, 2020 |Voice of San Diego 

In a bizarre twist to proceedings, Miss Manners sought to have her £30 cab fare from her Kensington flat to court refunded. How A British Aristocrat Used Big Game Hunter’s Sperm To Get Pregnant Without His Permission |Tom Sykes |December 2, 2014 |DAILY BEAST 

In response to hearing her story, Uber apologized for the "inefficient route" and partially refunded her fare. The Ten Worst Uber Horror Stories |Olivia Nuzzi |November 19, 2014 |DAILY BEAST 

Neither is appealing in a world of easy-to-find gourmet fare. Relax—Both Parties Are Going Extinct |Nick Gillespie |November 4, 2014 |DAILY BEAST 

They were tired of the fare at restaurants catering to tourists and were craving something a bit more authentic. The Airbnb of Home-Cooked Meals |Itay Hod |November 3, 2014 |DAILY BEAST 

He handed over his fare card so detectives could determine exactly when he had entered the subway system. From Ebola Country to NYC’s Subways |Michael Daly |October 25, 2014 |DAILY BEAST 

Every rigor of hard fare, and severe usage, was inexorably brought upon him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Every time Man pushes ahead a little, a percentage of the pushers pay the fare. Fee of the Frontier |Horace Brown Fyfe 

It may be that gasolene and repairs cost more than a railroad fare once a week, but I have abstained from making a comparison. Ancestors |Gertrude Atherton 

Beans and bacon, cabbage and brown hard dumplings, formed the bill of fare, which the men washed down with plenty of table beer. The World Before Them |Susanna Moodie 

It ran from Leicester to Loughborough and back at a fare of one shilling, and carried 570 passengers. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow