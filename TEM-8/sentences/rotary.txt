If you’re looking for a more specific tool kit, one for model making and fine detail, you’ll need a great rotary tool. Best tool combo kits: Take on any project with these power tool sets |Florie Korani |August 14, 2021 |Popular-Science 

“From the old rotary phone that they were on a waiting list for, to the scooter they all piled on to for a monthly dinner,” — in interviews, Pichai often discusses the impact that technology has had on his own life. The July core update is “effectively complete”; Tuesday’s daily brief |George Nguyen |July 13, 2021 |Search Engine Land 

After dialing the number on a rotary phone he hears distressed sounds coming from the other end before the line abruptly clicks off. ‘Genesis Noir’: A stunning game about love, murder, joy and the Big Bang |Christopher Byrd |April 12, 2021 |Washington Post 

Brainard suspected that the engineer was confusing roundabouts with the much larger, higher-speed rotaries — a forgivable confusion, given the nomenclature. Should Traffic Lights Be Abolished? (Ep. 454) |Stephen J. Dubner |March 11, 2021 |Freakonomics 

The rotary and foil blades in waterproof models may not get quite as close to the skin as regular disposable razors, but they will get you smoother than most dry electric shavers. Best electric shaver: Get a smooth shave with our picks |Jeremy Helligar |December 18, 2020 |Popular-Science 

Inquiries will be accepted only via Western Union telegram or rotary phone. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

Bob Bashara was a married Rotary Club president who lived in a ritzy Detroit suburb. A Murder in Detroit’s Sexual Underworld |Steve Miller |October 8, 2014 |DAILY BEAST 

There's an old rotary phone and a few yellowed papers on a cluttered old wooden desk. The Secret Speakeasies of Buenos Aires |Jeff Campagna |February 25, 2014 |DAILY BEAST 

“I think for sure he will,” said Kit Jennings, treasurer of the Spartanburg Rotary Club, which Perry addressed Tuesday. Rick Perry Can’t Quit Campaigning |David Catanese |December 4, 2013 |DAILY BEAST 

The thwap-thwap-thwap of rotary wings above triggered an intense reaction in Army trauma surgeon Dr. (Maj.) Tara Dixon. From Hero to Homeless: Tara Dixon’s Story |Paula Broadwell |July 4, 2013 |DAILY BEAST 

The rotary tuner shown in the sketch was designed by a correspondent of Modern Electrics. The Boy Mechanic, Book 2 |Various 

The Gnome rotary engine was first used with success at this meeting. Aviation in Peace and War |Sir Frederick Hugh Sykes 

The first steam engine invented by Hero was a rotary engine, but it was of course, most uneconomical of steam. Invention |Bradley A. Fiske 

They not unfrequently acquire a rotary movement, whereby the circumference attains a velocity of several miles per hour. The Book of Curiosities |I. Platts 

It is by this means that the reciprocating movement of the piston is transformed into a rotary motion of the crank-shaft. Aviation Engines |Victor Wilfred Pag