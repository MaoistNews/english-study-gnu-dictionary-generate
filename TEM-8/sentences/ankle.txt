However, unlike these virus-specific measures, the increased use of ankle monitors won’t necessarily end when the health emergency does. Covid-19 has led to a worrisome uptick in the use of electronic ankle monitors |Amy Nordrum |October 8, 2020 |MIT Technology Review 

I think a lot of people believe that this rising tide of disinformation and hate did not exist until it was lapping at their ankles. How the truth was murdered |Abby Ohlheiser |October 7, 2020 |MIT Technology Review 

Their socks have a compression rating of 15-25mmHg, with the most pressure starting at the ankle. Clothing and accessories that make great gifts |PopSci Commerce Team |October 6, 2020 |Popular-Science 

Luka Dončić went for a 43-point triple-double on a bum ankle and hit a step-back buzzer-beater to beat the Clippers, becoming the youngest player in NBA history to hit a game-winner in the postseason. These NBA Playoffs Are All About The Clutch Shooters |Josh Planos |October 5, 2020 |FiveThirtyEight 

While keeping your skis as parallel as possible, pole-plant, roll your ankles into a turn around your pole, and set up again in the opposite direction, engaging your edges to fully stop your momentum. 6 Easy Ways to Be a Better Skier |Heather Hansman |October 1, 2020 |Outside Online 

It was the type of fall that should have resulted in bruises and maybe a sprained ankle. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

It paralleled a much happier time when he carried her around after she twisted her ankle, back in Season 4. Exit Interview: The Walking Dead's Beth Tells All |Melissa Leon |December 1, 2014 |DAILY BEAST 

My ankle—I never got to fix it, because I still had to walk on it in heels. Eliza Coupe Finds Her ‘Happy Ending’ With ‘Benched’ |Kevin Fallon |October 28, 2014 |DAILY BEAST 

And the whole time I had broken ankle, when we filmed the whole series. Eliza Coupe Finds Her ‘Happy Ending’ With ‘Benched’ |Kevin Fallon |October 28, 2014 |DAILY BEAST 

Models are always a few faltering footsteps away from breaking an ankle (or worse). Up, Up, Up: The Hottest High Heels in History |Lizzie Crocker |September 11, 2014 |DAILY BEAST 

Christian Science nor mind cure wasn't invented then, or I should of used 'em, and said my ankle wasn't sprained. The Cromptons |Mary J. Holmes 

Eloise never had, and the pain in her ankle was so sharp that she gave little heed to what Mrs. Biggs was saying. The Cromptons |Mary J. Holmes 

The vault was ankle deep in mire and so crowded were the prisoners that no one could sit without leaning upon another. British Highways And Byways From A Motor Car |Thomas D. Murphy 

He scarcely dares glance at the trim ankle which she shows as she holds her dress out of the mud. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

One man, solid and stolid, stood his ground on the edge of the chain and administered a hearty kick upon each ankle as it passed. Ancestors |Gertrude Atherton