During the cold, rainy winters, the flying insects that are the woodpeckers' preferred diet disappear and the birds depend on the nuts in these pantries. Blood, death, and eye gouging: welcome to the world of acorn woodpeckers |Kate Baggaley |September 9, 2020 |Popular-Science 

Nighttime temperatures often dip below freezing in these rainy tropical highlands. This hummingbird survives cold nights by nearly freezing itself solid |Jonathan Lambert |September 8, 2020 |Science News 

If you’re headed to the coast, go in the springtime or midsummer to avoid the rainy seasons, which run from May to mid-July and mid-November to mid-January. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

Short-term forecasts like those in your smartphone’s weather app are based on the observations that feed into them, such as whether it is currently rainy in Northern California or whether there are strong winds over central Alaska. Improved three-week weather forecasts could save lives from disaster |Alexandra Witze |August 27, 2020 |Science News 

The Water Authority also dipped into its rainy day reserve fund up to a limit set by its board of directors to keep the rate increase for consumers lower. Environment Report: Why Your Water Bill Might Spike |MacKenzie Elmer |July 27, 2020 |Voice of San Diego 

Then she managed to struggle a mile through dark, rainy woods. The 7-Year-Old Plane Crash Survivor’s Brutal Journey Through the Woods |James Higdon |January 7, 2015 |DAILY BEAST 

There were mats everywhere and it was like a big rainy-day sleepover with most of the cast. Orange Is the New Black’s Kimiko Glenn on Hippie Brook Soso and Chapel Sex with Natasha Lyonne |Marlow Stern |June 18, 2014 |DAILY BEAST 

In the lean rainy season, the women switched out meat and oily sauces for vegetables. How Famines Make Future Generations Fat |Carrie Arnold |May 11, 2014 |DAILY BEAST 

Potholes are common in the rainy region as well as a heavy fog that often hinders visibility. The Road to Cinco de Mayo |Tania Lara |May 5, 2014 |DAILY BEAST 

It was a very rainy summer—the summer Arthur Ashe won Wimbledon against Jimmy Connors, which I watched in the pub. Meet Alexandre Desplat, Hollywood’s Master Composer |Andrew Romano |February 11, 2014 |DAILY BEAST 

In that part, it was little better than a morass, from the occasional overflowing of the waters at the rainy seasons. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

That we will, and you never need want, Mark, for I've many a fine bone buried away against old age and rainy weather. The Soldier of the Valley |Nelson Lloyd 

In the rainy season they fear firearms but little, for they know that they are of less effect than none at all. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

On rainy or melancholy days Edna went out and sought the society of the friends she had made at Grand Isle. The Awakening and Selected Short Stories |Kate Chopin 

During the rainy season, most of these cliffs and rocks are covered with water, and the river then appears more majestic. A Woman's Journey Round the World |Ida Pfeiffer