It was a giant leap for mankind—using a picture to portray a sound. Reading, That Strange and Uniquely Human Thing - Issue 94: Evolving |Lydia Wilson |December 23, 2020 |Nautilus 

She says, “Birds are not aggressive creatures… It is mankind, rather who insists upon making it difficult for life to exist on this planet.” Here’s the real story behind Alfred Hitchcock’s ‘The Birds’ |Tom McNamara |December 3, 2020 |Popular-Science 

“It is well documented that fossil fuel emission is very harmful to mankind, claiming 9 million deaths yearly,” he told the group. Why Accenture thinks the ‘Henry Ford moment of the digital era’ is coming |Alan Murray |September 17, 2020 |Fortune 

This is for the first time in the history of mankind when more than 70% of countries – which include developed, developing, and underdeveloped nations – are under lockdown. The impact of Coronavirus on digital marketing and effective solutions |Birbahadur Kathayat |July 23, 2020 |Search Engine Watch 

In spite of this, it was the sheer ingenuity of the Roman army engineers that brought victory to the Romans in what might have been the largest naval battle in the history of mankind. 14 Exceptional Weapon Systems from History That Were Ahead of their Time |Dattatreya Mandal |March 26, 2020 |Realm of History 

Kennedy: "Mankind must put an end to war — or war will put an end to mankind." Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

The current attack on the Jews,” he wrote in a 1937 essay, “targets not just this people of 15 million but mankind as such. The Catholic Philosopher Who Took on Hitler |John Henry Crosby |December 26, 2014 |DAILY BEAST 

Heracles goes on his twelve labours, not to better mankind, but to achieve immortality and atone for his own sins. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

If I had an idea that mankind could be cured, I should not believe in God. Albert Camus, Our Existential Epidemiologist |Malcolm Jones |October 17, 2014 |DAILY BEAST 

The rest of the essays and lectures in The Masters of Mankind show how Chomsky insists on breaking all the rules. Noam Chomsky—Infuriating and Necessary |David Masciotra |September 28, 2014 |DAILY BEAST 

His hero, Gulliver, discovers race after race of beings who typify the genera in his classification of mankind. Gulliver's Travels |Jonathan Swift 

In the old world, poverty seemed, and poverty was, the natural and inevitable lot of the greater portion of mankind. The Unsolved Riddle of Social Justice |Stephen Leacock 

And it would be hard indeed, if so remote a prince's notions of virtue and vice were to be offered as a standard for all mankind. Gulliver's Travels |Jonathan Swift 

The ne'er-do-well blew, like seed before the wind, to distant places, but mankind at large stayed at home. The Unsolved Riddle of Social Justice |Stephen Leacock 

Eve, too, lovely as she is, seems to bear no likelihood of resemblance to Milton's superb mother of mankind. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement