After all, you miss 100 percent of the shots you don’t take, but you also miss 100 percent of the shots that get blocked by a sledge. How Paralympic Ice Hockey Is Evolving |John Loeppky |March 7, 2022 |FiveThirtyEight 

Guests can rent cottages near his residence, where they have access to a swimming pool, sports facilities such as skis and sledges, a restaurant and a New Year’s souvenirs store. How Soviet Russia Banished Their Version of Santa Claus, Then Brought Him Back to Spread Communist Cheer |Madeline Roache |December 24, 2020 |Time 

Webb made his clay from ancient bricks that he found on walks in Brooklyn, then smashed to a powder with a sledge hammer. The Fine Art of Recycling |Blake Gopnik |January 14, 2014 |DAILY BEAST 

Americans love winners and they love underdogs, and when we took a sledge hammer to kill ants, people turned against us. The GOP’s Kamikazes Are Back |Eleanor Clift |June 22, 2013 |DAILY BEAST 

Fortunately, the last crash had been passed without dislocating the parts of either sledge or rider. The Giant of the North |R.M. Ballantyne 

In truth, it was so intently engaged with a sleeping seal that it had not observed the approach of the sledge. The Giant of the North |R.M. Ballantyne 

Rising at once he bundled up his traps, threw the line of his small hand-sledge over his shoulder, and stepped out for home. The Giant of the North |R.M. Ballantyne 

Soon the sledge which the dogs had drawn to the woods was piled high with deer and other game. Stories the Iroquois Tell Their Children |Mabel Powers 

Thrice, all his strength flew with a downright stroke,—a smithy's sledge less crushing. God Wills It! |William Stearns Davis