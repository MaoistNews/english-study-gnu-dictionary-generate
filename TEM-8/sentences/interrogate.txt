People love the creative storytelling aspect of the game and the strategy behind sounding believable or interrogating someone for the truth. Meet The Husband And Wife Duo Who Created This Family Game Now Available In Target |Kimberly Wilson |August 27, 2021 |Essence.com 

We want to interrogate how did we get to this place and how do we change it. Ambitious new Smithsonian initiative aims to help America deal with the history and legacy of racism |Peggy McGlone |August 24, 2021 |Washington Post 

We have a chance to really ask ourselves what good journalism looks like, and to interrogate the stereotypes around who is capable of achieving that and the attitudes they need to display. Masters of uncertainty: Pandemic’s unpredictability is making us adaptable, stronger and more creative, say experts |Jessica Davies |August 23, 2021 |Digiday 

Those communities joined worldwide protests over racial injustice over the past year, forcing London to interrogate its policing policies as well as disparities in employment and housing. 'We Know How to Bounce Back.' Sadiq Khan Has a Plan to Build a Greener, Fairer London Post-Pandemic |Ciara Nugent |August 11, 2021 |Time 

We want to interrogate that narrative and talk to people who don’t believe they check a set box. Are You A Gen-Z Republican? A Democrat In A Rural State? If So, You Might Be A Political Anomaly, And We Want To Hear From You. |Alex Samuels (Alex.L.Samuels@disney.com) |July 23, 2021 |FiveThirtyEight 

There is no pause to interrogate his own immersive activism. Nigeria’s Larger-Than-Life Nobel Laureate Chronicles a Fascinating Life |Chimamanda Adichie |August 9, 2014 |DAILY BEAST 

Taking the kids from the 10-foot-by-15-foot cell one by one, it took the jailers three days to interrogate them all. Abducted, Tortured, Indoctrinated: The Tale of a Teen Who Escaped ISIS |Yusuf Sayman |August 4, 2014 |DAILY BEAST 

They interrogate members of this strange community and discover that many of them might be complicit in a dark conspiracy. ‘True Detective,’ Obsessive-Compulsive Noir, and ‘Twin Peaks’ |Jimmy So |March 14, 2014 |DAILY BEAST 

I play a Homeland Security agent that gets to interrogate Johnny Depp. Aubrey Plaza on Playing A Zombie in ‘Life After Beth,’ the ‘Daria’ Movie, and More |Marlow Stern |January 21, 2014 |DAILY BEAST 

It took her eight days in the hospital to recover enough for the police to be able to interrogate her. A Serial Killer on the Loose in Nazi Berlin |Scott Andrew Selby |January 11, 2014 |DAILY BEAST 

Now the practice of Interrogative Analysis compels such persons to interrogate—to propose questions—to think. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

If you are so certain that Enid Orlebar is implicated in the affair, if not the actual assassin, why don't you interrogate her? The Doctor of Pimlico |William Le Queux 

His confessor brought to court impostors who pretended that they could interrogate the powers of darkness. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Old Mis' Meade was in the habit of going to bed before the others, and to-night she paused, candle in hand, to interrogate him. Country Neighbors |Alice Brown 

We come now to interrogate our oracle again, and we open the third chapter as we do so. Messages from the Epistle to the Hebrews |Handley C.G. Moule