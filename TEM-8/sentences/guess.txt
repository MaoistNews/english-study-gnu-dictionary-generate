My guess is that a lot of people who have previously had SARS-CoV-2 are probably still protected in part, or not as protected. How coronavirus variants may drive reinfection and shape vaccination efforts |Erin Garcia de Jesus |February 5, 2021 |Science News 

You had to assume that the mystery word was selected randomly from this word list, which was also the list your guesses had to be chosen from. Can You Randomly Move The Tower? |Zach Wissner-Gross |February 5, 2021 |FiveThirtyEight 

Right now, researchers say, their best guess is that vaccines will reduce transmission but may not prevent it entirely. So you got the vaccine. Can you still infect people? Pfizer is trying to find out. |Stephanie Arnett |February 2, 2021 |MIT Technology Review 

After each of your guesses, you are told which letters of your guess are also in the mystery word and whether any of the letters are in the correct position. Can You Guess The Mystery Word? |Zach Wissner-Gross |January 29, 2021 |FiveThirtyEight 

With these competing priorities, some solvers reasoned that the answer should be right in the middle, with a guess of 50. Can You Skillfully Ski The Slopes? |Zach Wissner-Gross |January 22, 2021 |FiveThirtyEight 

Haha, what a sad thing to be great at, but yeah, I guess I am. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

And then I got on a plane, and guess what was playing: I Never Sang for My Father. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

As Randy wrote, “I guess this speaks to the church not really having a place for gay people so getting married is still implicit.” Your Husband Is Definitely Gay: TLC’s Painful Portrait of Mormonism |Samantha Allen |January 1, 2015 |DAILY BEAST 

I guess we know how Bacchus kept his title as the god of wine and intoxication. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

“I guess it was their first incident where they lose a plane,” said Dobersberger, the travel agent. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

Kind of a reception-room in there—guess I know a reception-room from a hole in the wall. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

It would be a modest guess that Accadian culture implied a growth of at least ten thousand years. God and my Neighbour |Robert Blatchford 

Since that he has made his almanacs without weatherwise sayings, leaving every man to guess for himself. The Book of Anecdotes and Budget of Fun; |Various 

Squinty started to go back the way he had come, but I guess you can imagine what happened. Squinty the Comical Pig |Richard Barnum 

"I guess that is straight enough for Guitar to believe, instead of that upstart lieutenant," said Harry. The Courier of the Ozarks |Byron A. Dunn