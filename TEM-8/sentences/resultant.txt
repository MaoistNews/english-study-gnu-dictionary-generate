At the same time as William Vogt, the prophet, was sounding the alarm on overpopulation and what he saw as the resultant famine, there was another scientist whose discoveries would lead to a dramatic growth of the global population. Two (Totally Opposite) Ways to Save the Planet (Ep. 346 Rebroadcast) |Stephen J. Dubner |July 29, 2021 |Freakonomics 

The resultant vapor cloud explosion completely demolished the entire facility. Engineers raise alarms over the risk of major explosions at LNG plants |Will Englund |June 3, 2021 |Washington Post 

The resultant model will then serve more of those ads to women. How Facebook got addicted to spreading misinformation |Karen Hao |March 11, 2021 |MIT Technology Review 

If the resultant model had terrible accuracy, this would suggest that self-reported pain is rather arbitrary. AI could make healthcare fairer—by helping us believe what patients say |Karen Hao |January 22, 2021 |MIT Technology Review 

As of last year, overcrowding has led to increased use of force against inmates and a resultant loss of luxury. The Best Places to Be Locked Up If You Love Luxury |Eugene Robinson |October 7, 2020 |Ozy 

Muehl was of course the authority figure, with all resultant perks. The Life and Art of Radical Provocateur—and Commune Leader—Otto Muehl |Anthony Haden-Guest |September 22, 2014 |DAILY BEAST 

Further, the contagion effect of suicide and the resultant attention to it is a well-documented phenomenon. 'Genie, You're Free': Suicide Is Not Liberation |Russell Saunders |August 12, 2014 |DAILY BEAST 

The resultant pop culture is as morbid and contagious as the epidemics they depict. Ebola Rages in West Africa, Reigniting Humanity’s Oldest Fear: The Plague |Scott Bixby |August 4, 2014 |DAILY BEAST 

Could modern human DNA contamination affect the resultant radiocarbon date? Incontrovertible Evidence Proves the First Americans Came From Asia |Doug Peacock |March 27, 2014 |DAILY BEAST 

The resultant interview is the longest he has ever granted to any publication. Alex Haley’s 1965 Playboy Interview with Rev. Martin Luther King Jr. |Alex Haley |January 19, 2014 |DAILY BEAST 

When the resultant pressure is not vertical on the piers these must be constructed to meet the inclined pressure. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

It would give us time to watch Doria and see what direction the resultant of the forces now tearing her soul would take. Jaffery |William J. Locke 

The resultant national policy is the most selfish, but the most formidable in the world of nations. The Onlooker, Volume 1, Part 2 |Various 

The movements so far described, and the resultant fighting, may be styled the first stage of the battle. The Life of Nelson, Vol. I (of 2) |A. T. (Alfred Thayer) Mahan 

We must prove that the resultant accounts for all the forces in operation at the time. Theism or Atheism |Chapman Cohen