She softened my heart amid fearful isolation when it might otherwise have hardened in self-defense. How a sickly squirrel offered me unexpected comfort |Pam Spritzer |February 8, 2021 |Washington Post 

When investors are fearful, they tend to buy up these securities, pushing down Treasury bond yields and causing the yield curve to invert. Is it time to start worrying about inflation? |John Detrixhe |January 18, 2021 |Quartz 

If someone is fearful of getting a vaccine, we will speak to them privately about how we can best support them. With many employees wary, company leaders prepare their return-to-office coronavirus vaccine policies |Jessica Davies |January 18, 2021 |Digiday 

Among the economic lesson of 2020 is that fearful consumers means less economic activity. What job security? Americans are feeling worn down and fearful of layoffs |Lance Lambert |January 15, 2021 |Fortune 

Anxiety makes you feel bad physically, and those physical symptoms feel like danger, so you become even more fearful and anxious, which only makes the symptoms worse. How to keep your anxiety from spiraling out of control |Sara Chodosh |January 15, 2021 |Popular-Science 

Still fearful and smarting from the pain, I arrived on time and was led to chair in his office. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

The grand jury decision “makes us a little bit more fearful to leave our homes every day,” Cook told me. ‘They Let Him Off?’ Scenes from NYC in Disbelief |Jacob Siegel |December 4, 2014 |DAILY BEAST 

This past election, they were hardly mentioned by Republicans fearful of alienating moderates. It Gets Better—but Mostly if You Live in a Rich, Democratic Country |Jay Michaelson |November 11, 2014 |DAILY BEAST 

Oddly, though, Americans are not fearful enough when it comes to real threats. Ebola, ISIS, the Border: So Much to Fear, So Little Time! |Gene Robinson |November 2, 2014 |DAILY BEAST 

At first, the doctors write, the villagers were “fearful and agitated,” lacking the basic necessities needed to survive. 1976 Vs. Today: Ebola’s Terrifying Evolution |Abby Haglage |September 10, 2014 |DAILY BEAST 

And it is too true that ages of subjugation have demoralized, to a fearful extent, the Italian People. Glances at Europe |Horace Greeley 

His arm was drawn around the drum, and finally his whole body was drawn over the shaft, at a fearful rate. The Book of Anecdotes and Budget of Fun; |Various 

A bull-fight is fearful enough, but it cannot compare with the struggle between a maddened buffalo and his pursuer. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

The boy often wonders if there is another animal in the world with such fearful horns. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

So with a fearful growl, and a bark that might have frightened a lion, Bravo made a leap and a spring after poor little Downy. The Nursery, July 1873, Vol. XIV. No. 1 |Various