In 2017, it alienated customers by deliberately but quietly slowing the performance of older iPhones via a software update, ostensibly to spare the life of aging batteries. Apple CEO Tim Cook is fulfilling another Steve Jobs vision |Rachel Schallom |August 24, 2020 |Fortune 

Yet he apparently had no such qualms a week later when he signed a decree mandating that telecoms hand over data on 226 million Brazilians to IBGE, the government’s statistical agency, ostensibly for surveying households during the pandemic. Brazil is sliding into techno-authoritarianism |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

If it does not, candidates could ostensibly sue their rivals for claiming the paper’s endorsement and not just the endorsement of the Union-Tribune’s editorial board. Politics Report: Who Will Get the Midway Rose? |Scott Lewis and Andrew Keatts |August 15, 2020 |Voice of San Diego 

Researchers elicited anxiety among the women by giving them three minutes to put together a speech on their flood preparedness — natural disasters are a common threat to the island — to ostensibly be evaluated later by government experts. Why do we miss the rituals put on hold by the COVID-19 pandemic? |Sujata Gupta |August 14, 2020 |Science News 

To submit a number of bogus academic papers to ostensibly serious academic journals in those fields … and show how easily unsupported claims could be presented as factual without reasonable proofs. American Fringes: The Intellectual Dark Web Declares Its Independence |Nick Fouriezos |August 11, 2020 |Ozy 

Sorkin may not have won his fight, ostensibly to reform the news. 'The Newsroom' Ended As It Began: Weird, Controversial, and Noble |Kevin Fallon |December 15, 2014 |DAILY BEAST 

The uncle told RTL radio Hauchard called his grandmother, ostensibly from Syria, on Nov. 2, for her birthday. Showing the Faces of Its Murderers, ISIS Shows Its Global Reach |Tracy McNicoll |November 18, 2014 |DAILY BEAST 

Ostensibly meant to protect babies, these products are dangerous. Kids Eat the Darndest Things: Laundry Pods, Teething Necklaces, and More Of The Weirdest Stuff Sending Kids to the E.R. |Russell Saunders |November 14, 2014 |DAILY BEAST 

In June, Pakistan launched an all-out military offensive in the region, ostensibly to evict all the militants from the area. Obama’s Deadly Informants: The Drone Spotters of Pakistan |Umar Farooq, Syed Fakhar Kakakhel |November 12, 2014 |DAILY BEAST 

And while there are cutscenes that ostensibly explain the grander narrative, nothing really makes sense. Bayonetta Is Nintendo’s Graphic, Ass-Kicking Barbie |Alec Kubas-Meyer |October 24, 2014 |DAILY BEAST 

Every act of the Americans, ostensibly as courtesy and friendship, tend to that end. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

The thing was ostensibly done, and served very well to hide an exclamation of surprise. The Weight of the Crown |Fred M. White 

Yet, though ostensibly free, these local bodies are practically in the power of the political wire-puller, or cacique. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

In Kent there had been an p. 29alarming outbreak of the peasantry, ostensibly against the use of agricultural machinery. East Anglia |J. Ewing Ritchie 

But what we tolerate in uncivilized lands, even where we are ostensibly rulers, we will not suffer in our own. Ancient Faiths And Modern |Thomas Inman