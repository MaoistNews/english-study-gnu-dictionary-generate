Exhausted and frightened, they were forced to decide how to go about saving themselves. A Veteran Surfer’s Big-Wave Nightmare |Outside Editors |January 27, 2021 |Outside Online 

A female investigative journalist was severely harassed and temporarily silenced by such activity, and more recently, a female poet and novelist was frightened and shamed. The year deepfakes went mainstream |Karen Hao |December 24, 2020 |MIT Technology Review 

The agents sat at a table, across from Tanvir, who was frightened and spoke poor English. Supreme Court rules for Muslims placed on no-fly list after refusing to become FBI informants |Robert Barnes |December 10, 2020 |Washington Post 

It turned out to be a very big and very frightened stray dog. Some tasty, food-inspired names were on the menu for these pets |John Kelly |November 29, 2020 |Washington Post 

Sometimes there would be a new visitor to the island, like a wayward sailor named Gulliver or a delightfully frightened ghost named Wisp, who needed my help. Nintendo's Animal Crossing: New Horizons Is the Game We All Need Right Now |Matthew Gault |March 17, 2020 |Time 

I am frightened by how much of this is caused by organized retail crime. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

Even as early as December 4, remarks from inside the bubble were cryptic and frightened. Pyongyang Shuffle: Hollywood In Dead Panic Over Sony Hack |James Poulos |December 19, 2014 |DAILY BEAST 

Yeah, having a microphone and so many people under your control always frightened me. George Clinton on Industry ‘Mobsters’ and How Nobody Wants to Listen to a Crackhead |Curtis Stephen |November 19, 2014 |DAILY BEAST 

Holmes seemed to expedite matters promptly, amid rumors that she was frightened of the Church of Scientology. How Can Katie Holmes Escape Tom Cruise—and ‘Dawson’s Creek’? |Tim Teeman |October 30, 2014 |DAILY BEAST 

The people seemed not at all frightened as they trooped past to begin their day. From Ebola Country to NYC’s Subways |Michael Daly |October 25, 2014 |DAILY BEAST 

“I went into a great passion and frightened my mother into a fit,” said Wardle. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

His silence had frightened her: what if he should resent on her the cruel words spoken by Dr. Ashton? Elster's Folly |Mrs. Henry Wood 

Though frightened to death, I refused to part with my reserve and made ready to go and take command of it at break of dawn. Gallipoli Diary, Volume I |Ian Hamilton 

He went out into the garden, and the rustling of the laurel-bushes frightened him. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

The submarine scare is full on; the beastly things have frightened us more than all the Turks and all their German guns. Gallipoli Diary, Volume I |Ian Hamilton