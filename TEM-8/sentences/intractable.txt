What’s more, when I started asking fellow hikers if they’d suffered from intractable and unexplained pains, or the new sense that any exercise still hurt days later, a shocking percentage said yes. Did Thru-Hiking the Appalachian Trail Ruin My Body? |Grayson Haver Currin |January 25, 2021 |Outside Online 

Pioneering scientists like Rothermel dealt with this intractable problem by ignoring it. What the complex math of fire modeling tells us about the future of California’s forests |Amy Nordrum |January 18, 2021 |MIT Technology Review 

The explosive growth of artificial intelligence has fostered hope that it will help us solve many of the world’s most intractable problems. What Buddhism can do for AI ethics |Amy Nordrum |January 6, 2021 |MIT Technology Review 

Maritza Perez, director of national affairs for the Drug Policy Alliance, said the partisan nature of the marijuana debate on Capitol Hill reflected the deeply divided nature of Congress rather than an intractable difference on policy. House votes to decriminalize marijuana as GOP resists national shift on pot |Mike DeBonis |December 4, 2020 |Washington Post 

Traits such as “superposition” and “entanglement,” when combined with “interference,” have the potential to solve problems in science and industry that are otherwise intractable, even to state-of-the-art supercomputers. Quantum computing is entering a new dimension |rhhackettfortune |December 3, 2020 |Fortune 

The real problem—a problem that thus far has proven intractable—is overcoming the doubt and fear. Powdered Measles Vaccine Could Be Huge for Developing World |Kent Sepkowitz |December 2, 2014 |DAILY BEAST 

Some express a feeling of hopelessness and that their intractable sadness will never abate. 'Genie, You're Free': Suicide Is Not Liberation |Russell Saunders |August 12, 2014 |DAILY BEAST 

Unfortunately, this new study shows how intractable that problem truly is. Anti-Vaxxers Will Fuel the Next Pandemic |Russell Saunders |May 7, 2014 |DAILY BEAST 

The symptoms can show up as a wide array of intractable health problems. Pizza Might Be Your Enemy |Daniela Drake |March 9, 2014 |DAILY BEAST 

On the left, many see it as a civil rights issue–potentially ameliorating the problem of intractable poverty. Best Business Longreads |William O’Connor |November 17, 2013 |DAILY BEAST 

But on the subject of the foreign troops Hartington in one House and his father in the other were intractable. The History of England from the Accession of James II. |Thomas Babington Macaulay 

On the other hand they were likely to prove intractable and ungovernable, and many preferred even suicide to servitude. The Private Life of the Romans |Harold Whetstone Johnston 

A gallows was erected in the court, where the intractable underwent capital punishment as a warning to the rest. Travels in Peru, on the Coast, in the Sierra, Across the Cordilleras and the Andes, into the Primeval Forests |J. J. von Tschudi 

Nevertheless it would surprise those acquainted only with fresh water ice to find how tough, sticky and intractable is sea-ice. The Home of the Blizzard |Douglas Mawson 

This fellow, Lopez, had absolutely been allowed to make a good score off his own intractable disobedience. The Prime Minister |Anthony Trollope