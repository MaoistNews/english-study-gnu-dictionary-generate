Grouchy and wary and tender, he’s a sozzled hedonist seemingly out for himself—though his party-animal facade is just a mask for his bottomless generosity. The 19 Most Underrated Movies on Netflix |Stephanie Zacharek |October 26, 2021 |Time 

His Bill isn’t a fratty hedonist so much as a man too psychologically compartmentalized to face his own misdeeds. Impeachment: American Crime Story Frames the Clinton Scandal as a Case of Women Sabotaging Women. Is That Really So Revolutionary? |Judy Berman |August 31, 2021 |Time 

The old Don was a loving father and husband, and a hedonist. What's Happened to Don Draper? Why Everyone’s Favorite ‘Mad Men’ Stud Needs His Mojo Back |Lizzie Crocker |April 16, 2014 |DAILY BEAST 

In Dornan's telling, Clinton was a "self-indulgent hedonist and phony," a dabbler in drugs, a letch. The Original Tea Partier |Bryan Curtis |October 20, 2010 |DAILY BEAST 

Reading that, the stranger would not necessarily (I hope) be transformed into a detrimental Hedonist. Adventures and Enthusiasms |E. V. Lucas 

The greatest poets, however, do not accept the point of view either of the extreme moralist or of the hedonist. The Art of Letters |Robert Lynd 

He is not what is called a social philosopher, a pretentious hedonist, who talks continuously and floridly about himself. The Silent Isle |Arthur Christopher Benson 

The restraints of Christians saddened him simply because he was more hedonist than a healthy man should be. Orthodoxy |G. K. Chesterton 

In an hedonist age pleasure has always sunk low, so that it has to be encouraged. George Bernard Shaw |Gilbert K. Chesterton