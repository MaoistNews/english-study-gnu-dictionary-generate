Companies that pursue leases in ANWR also will have to weigh the prospects of litigation, investor anger and a tarnished brand—especially large firms with public name recognition. The Trump administration opened the Arctic National Wildlife Refuge to oil companies—but none may bite |By Scott L. Montgomery/The Conversation |August 26, 2020 |Popular-Science 

It was among the earliest litigation of its kind in California. Confirmed Nursing Home Complaints Plummet During Pandemic |Jared Whitlock |August 25, 2020 |Voice of San Diego 

Seemingly everyone is preparing or hinting at litigation, including the city. How the City Came to Lease a Lemon |Lisa Halverstadt and Jesse Marx |August 10, 2020 |Voice of San Diego 

The law firm that produced the report, Hugo Parker, is already advising the city on asbestos-related litigation. City’s Real Estate Assets Director Resigns Amid Scrutiny Over Ash Street Deal |Lisa Halverstadt and Jesse Marx |August 4, 2020 |Voice of San Diego 

Written by a law firm that’s already advising the city on asbestos litigation, the 40-page document offers a glimpse into San Diego’s flawed handling of its real estate. City Botched High-Rise Deal from Acquisition to Renovation, Investigation Finds |Jesse Marx and Lisa Halverstadt |July 30, 2020 |Voice of San Diego 

She and her family have been in litigation with NYU over the Acton estate since his death. In Tussle Over Will, Mistress’s Family Takes a Bite Out of NYU |Anthony Haden-Guest |November 10, 2014 |DAILY BEAST 

And the costs of litigation amount to chump change for guys who count their net worth in nine and 10 figures. Remember the $182 Billion AIG Bailout? It Just Wasn’t Generous Enough |Daniel Gross |October 15, 2014 |DAILY BEAST 

If the administration refuses, he will prepare for litigation. Why Did America’s Only Pot Researcher Suddenly Get Fired? |Abby Haglage |July 10, 2014 |DAILY BEAST 

But with McDaniel threatening litigation, this political saga may not be over yet. Thad Cochran Escapes Bitter Tea Party in Mississippi |Ben Jacobs |June 25, 2014 |DAILY BEAST 

Wood said this is common when people realize how much litigation and how much time and money pet custody cases often require. Divorce Is Going to the Dogs, Literally |Keli Goff |June 20, 2014 |DAILY BEAST 

That the Society be immediately dissolved, in view of pending litigation. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

Thus the history of the first66 London playhouse, which is chiefly the history of quarrels and litigation, came to a close. Shakespearean Playhouses |Joseph Quincy Adams 

The delivery of a brief to counsel gives him authority to act for his client in all matters which the litigation involves. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

It is evident that the irrationality of frequent appeals to expensive and bothersome litigation should arouse suspicion. Essays In Pastoral Medicine |Austin Malley 

Bassett's name did not appear in the office records to Dan's knowledge nor was he engaged in litigation. A Hoosier Chronicle |Meredith Nicholson