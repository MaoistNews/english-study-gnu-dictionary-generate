Amazon has a new health-tracking bracelet with a microphone and an app that tells you everything that’s wrong with you. Amazon’s new health band is the most invasive tech we’ve ever tested |Geoffrey Fowler, Heather Kelly |December 10, 2020 |Washington Post 

Shonte made and distributed the bracelets to honor her sister’s struggle, collecting donations for her care. Two School Districts Had Different Mask Policies. Only One Had a Teacher on a Ventilator. |by Annie Waldman and Heather Vogell |November 23, 2020 |ProPublica 

The padded base makes it safe to place on delicate surfaces and doubles as a dish for smaller accessories like rings and bracelets. Jewelry organizers that will completely transform your vanity |PopSci Commerce Team |October 14, 2020 |Popular-Science 

This spacious organizer is pretty enough to act as an accessory display, for jewelry, bracelets, or scarves—the design also prevents any of those precious items from snagging at the ends of any hooks. Over-door hooks for freeing up precious closet space |PopSci Commerce Team |October 8, 2020 |Popular-Science 

We would each tackle one type of bracelet and then swap knowledge, teaching each other what we’d just learned. Crafting our path |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

The new way to show your love and affection for your bestie is with a fashionable Little Scocha friendship bracelet. The Daily Beast’s 2014 Holiday Gift Guide: For the Blue Ivy in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

People with cognitive disabilities can wear an ID bracelet or a T-shirt that says, “I have autism; call 911 if I am alone.” Is It Wrong for Parents to Lock Up Their Disabled Kids? |Elizabeth Picciuto |August 4, 2014 |DAILY BEAST 

A steel bracelet on my wrists reads CPL BRIAN L. CHEVALIER - 14 MAR 2007 – DIYALA. Memorial Days After Mourning Has Passed |Alex Horton |May 25, 2014 |DAILY BEAST 

The cloth version was introduced and a pincushion bracelet was brought in. Tilda Swinton and Oliver Saillard Perform the Creation of Fashion in ‘Eternity Dress’ |Sarah Moroz |November 21, 2013 |DAILY BEAST 

Canaday still had the hospital bracelet on her wrist when she returned to Plaza Towers on Wednesday evening. Oklahoma Tornado Hero Teacher Anna Canaday: ‘Take Me Instead’ |Michael Daly |May 23, 2013 |DAILY BEAST 

OLeary yielded to the temptation of the moment far enough to play with the coiled bracelet which lay against the girls wrist. The Woman Gives |Owen Johnson 

Farewell, little Hulda; guard well the bracelet; I must to my ruined temple again. Wonder-Box Tales |Jean Ingelow 

The officer turned the bracelet around in his fingers, his dark eyes with their slitted pupils never leaving Dalgard's face. Star Born |Andre Norton 

One of the guards tore the bracelet from Dalgard's arm, trying not to touch the scout's flesh in the process. Star Born |Andre Norton 

Oh, Mum, I was going to get you a sweet little bracelet of old Irish paste—you know—a thing in four little chains. Happy House |Betsey Riddle, Freifrau von Hutten zum Stolzenberg