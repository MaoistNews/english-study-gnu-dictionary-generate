Entering through a window, Mono emerges into a foul kitchen where insects buzz over towering dirty dishes. ‘Little Nightmares II’: A hypnotic, dark fairy tale |Christopher Byrd |February 9, 2021 |Washington Post 

Jamorko Pickett posted 12 points, eight rebounds and three assists, and Chudier Bile also scored 12 points before fouling out. Georgetown continues to play well but falls short in the end against Villanova |Kareem Copeland |February 7, 2021 |Washington Post 

We all have to go in and rebound and take an offensive foul. Wizards’ defense falters again in a 119-97 loss to the Hornets |Ava Wallace |February 7, 2021 |Washington Post 

The 22-year-old forward didn’t shoot well and flubbed fouling Heat center Bam Adebayo when the big man caught the ball on an inbounds pass during the last play of the game with the Washington Wizards up three. Rui Hachimura and Deni Avdija are regaining their footing after missing so much time |Ava Wallace |February 4, 2021 |Washington Post 

Justyn Mutts added 17 points, and Hunter Cattoor scored 12 before fouling out. Pitt pulls away from Virginia Tech in second half |Gene Wang |February 4, 2021 |Washington Post 

Father Joel Román Salazar died in a car crash in 2013; his death was ruled an accident, but the suspicion of foul play persists. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

Malcolm Tucker, a foul-mouthed political advisor, was the role that turned Capaldi into a household name in Britain. Doctor Who: It’s Time For a Black, Asian, or Woman Doctor |Nico Hines |December 11, 2014 |DAILY BEAST 

Foul-mouthed chauvinist who flirted with chicks in a hot tub or celebrity-friendly sociopolitical satirist? Canada’s Subversive Sock Puppet: Ed the Sock Isn’t Afraid to Say Anything |Soraya Roberts |November 13, 2014 |DAILY BEAST 

Playing the foul-mouthed bad character will become as predictable and counter-intuitive as a playing a thousand Joeys. How Can Katie Holmes Escape Tom Cruise—and ‘Dawson’s Creek’? |Tim Teeman |October 30, 2014 |DAILY BEAST 

Miller took particular exception to a post in which Kelley had worried she might fall victim to foul play. The Mystery Woman Who Tried to Outdo Dillinger |Michael Daly |September 29, 2014 |DAILY BEAST 

Two years later this promising recruit, having fallen foul of the military authorities, had to leave the service under a cloud. Napoleon's Marshals |R. P. Dunn-Pattison 

But I have some more foul way to trot through still, in your Epistles and Satyrs, &c. A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

After he was securely bound he was forced to stand while the two, with foul epithets, hung the body of the corporal over the road. The Courier of the Ozarks |Byron A. Dunn 

Without warning, we found ourselves foul of a picket-line, and the vague forms of grazing horses loomed close by. Raw Gold |Bertrand W. Sinclair 

But it was strongly rumoured that there had been foul play, peculation, even forgery. The History of England from the Accession of James II. |Thomas Babington Macaulay