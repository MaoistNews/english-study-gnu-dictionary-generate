In the foreword of the last book she published before her death, Let Me Tell You What I Mean, writer Hilton Als described Didion as “a carver of words in the granite of the specific.” Joan Didion Wrote About Grief Like No One Else Could |Annabel Gutterman |December 23, 2021 |Time 

This, Nobel laureates Esther Duflo and Abhijit Banerjee note in a foreword to the report, is due to a loosening of state control and letting private wealth accumulate unabashedly. Half of India is so poor it owns almost nothing |Manavi Kapur |December 8, 2021 |Quartz 

Recently I was asked to write the foreword to a book of poetry written by female pilots. Amelia Earhart’s long-hidden poems reveal an enigma’s inner thoughts |Christine Negroni |October 17, 2021 |Washington Post 

Combining engrossing, urgent storytelling with illustrations, personal images and a foreword by Gloria Steinem, Chicago relays the story of an artist determined to ensure that women’s cultural achievements are permanently valued. 36 New Books You Need to Read This Summer |Cady Lang |May 25, 2021 |Time 

In the foreword to her little dog-centric collection A Dog Runs Through It, poet Linda Pastan refers to one of her many dogs as “the dog of my life,” as in the love of one’s life. How a Pandemic Puppy Saved My Grieving Family |Nicole Chung |April 6, 2021 |Time 

Music journalist Joel Selwin annotates, with a preface by Donovan, a foreword by Jorma Kaukonen, and an afterword by John Poppy. The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 

In a brand new foreword to her novel that is being re-released with branding ties to the movie, Lowry discusses this. Why 'The Giver' Movie Will Disappoint the Book's Fans |Kevin Fallon |August 15, 2014 |DAILY BEAST 

It is preceded by a foreword Cramer later wrote about the genesis of piece. The Strange and Mysterious Death of Mrs. Jerry Lee Lewis |Richard Ben Cramer |January 11, 2014 |DAILY BEAST 

Gen. Richard Dannatt, head of the British army from 2006 to 2009, wrote a foreword to the book, which is published on Monday. Bryan Adams’s Unlikely, Compelling Portrait Photography Book ‘Wounded’ |Nico Hines |November 11, 2013 |DAILY BEAST 

At least How to Be has a warning in its foreword: Morrissey and Juzwiak are professionals, but they are not experts. ‘Pot Psychology’: The Stoners’ Guide to Life |Adam Auriemma |November 11, 2012 |DAILY BEAST 

“Foreword” and “inwit” were good once; but “preface” and “conscience” 245 mean as much and have the advantage of being alive. English: Composition and Literature |W. F. (William Franklin) Webster 

I read the story itself first and afterwards the preface, or foreword. Daisy Ashford: Her Book |Daisy Ashford 

Idaho made a motion as if to stop him, but Graeme stepped quickly foreword and said sharply, 'Make way there, can't you?' Black Rock |Ralph Connor 

In a foreword to the readers of the New Dawn, however, a faintly ominous note was sounded. The Wrong Twin |Harry Leon Wilson 

The books and articles spoken of in the “Foreword” of this volume, pages 7–9, are not re-listed here. A History of Norwegian Immigration to the United States : From the Earliest Beginning down to the Year 1848 |George Tobias Flom