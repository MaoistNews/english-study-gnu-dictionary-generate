The internal review found that some teams of investigators would only substantiate chokeholds if they could prove that a victim’s breathing had been restricted. Still Can’t Breathe |by Topher Sanders, ProPublica, and Yoav Gonen, THE CITY, video by Lucas Waldron, ProPublica |January 21, 2021 |ProPublica 

The latter possibility, if substantiated, would, of course, cause a flurry of excitement among theoretical physicists to say the least, as it could imply the existence of new forces and particles. A Breakthrough in Measuring the Building Blocks of Nature - Facts So Romantic |Subodh Patil |January 8, 2021 |Nautilus 

Yet, the lack of OTAs this year demonstrated that those theories aren’t substantiated. NFLPA president calls for elimination of offseason practices |Mark Maske |December 31, 2020 |Washington Post 

She reported the death to the state, which, in a February letter to Williams, said it did not substantiate the complaint but found other unrelated violations at the facility. Profit and pain: How California’s largest nursing home chain amassed millions as scrutiny mounted |Debbie Cenziper, Joel Jacobs, Alice Crites, Will Englund |December 31, 2020 |Washington Post 

Judges, regardless of who appointed them, to date have overwhelmingly found that there was nothing to substantiate the allegations of massive fraud. ‘These are not crazy people’: GOP defends its voter-fraud push, ignoring obvious perils |Aaron Blake |December 16, 2020 |Washington Post 

So, President Obama does right by making every effort to substantiate the charges against Assad. Leslie H. Gelb: Obama Is Right on Chemical Warfare in Syria |Leslie H. Gelb |April 28, 2013 |DAILY BEAST 

In his comments Tuesday, Obama continued the hard-to-substantiate “job-creating” theme from his SOTU address. Thoughts on the BRAIN Initiative |Ilana Glazer |April 3, 2013 |DAILY BEAST 

Thus, catharsis, in a physiological sense, has been difficult to substantiate, but the results are by no means conclusive. Why Do We Cry? |Michael Trimble |January 10, 2013 |DAILY BEAST 

If the emails released today substantiate these claims, Hunt's career could be in jeopardy. James Murdoch Grilled at Leveson Inquiry |Mike Giglio, Peter Jukes |April 24, 2012 |DAILY BEAST 

It's why it is so important to substantiate both the plot, and its connection to Iran. Iran’s Covert War Against the United States |Kenneth M. Pollack |October 12, 2011 |DAILY BEAST 

How far Greene was able to substantiate his claim before the university is not recorded; he died in October, 1630. A History of the Cambridge University Press |S. C. Roberts 

This power of the pulpit enabled the hierarchy to set up and substantiate any claims which they chose. Ancient Faiths And Modern |Thomas Inman 

However, this is pure speculation, as there is nothing to substantiate such an assumption. Historic Fredericksburg |John T. Goolrick 

To substantiate and set forth at large the momentous distinction between Reason and Understanding. Aids to Reflection |Samuel Taylor Coleridge 

As we have hinted at the strength of his memory, we will now produce some facts to substantiate the truth. Portraits of Curious Characters in London, &amp;c. &amp;c. |Anonymous