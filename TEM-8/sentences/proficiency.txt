The answer to that question could shed light on the proficiency of these Stone Age humans as mariners and their mental capabilities overall. Ancient humans may have deliberately voyaged to Japan’s Ryukyu Islands |Charles Choi |December 3, 2020 |Science News 

Benn Eifert, founder of an investment fund that uses options to ride out market volatility, says that successful options trading requires sophisticated software and substantial math proficiency. Robinhood’s next adventure: Stealing market share from the rich |Jeff |December 3, 2020 |Fortune 

Overall, the tests show fewer than half of all students rated below proficiency, which is defined as demonstrating competency over challenging subject matter. ‘Nation’s report card’ shows declines for lowest-performing students |Laura Meckler |October 28, 2020 |Washington Post 

Augmedix recruits people who have a bachelor’s degree or the equivalent, and screens for proficiency in English reading, listening comprehension and writing, the company said. Time-crunched doctors are relying on remote-working scribes to take notes via video call |lbelanger225 |September 30, 2020 |Fortune 

Eifert says a high proficiency in math is merely “table stakes” when it comes to trading options, which he calls a “piece of craftsmanship from front to back.” ‘Going to Vegas:’ Newbie options traders face a reckoning as the tech stock rally fades |Jeff |September 14, 2020 |Fortune 

This official said ISIS “acted with tremendous military proficiency.” Obama’s Iraq Plan Has a Killer Flaw—and Airstrikes Alone May Not Save It |Jacob Siegel |August 8, 2014 |DAILY BEAST 

Batiste does not believe that entertainment is at odds with musical proficiency. Jon Batiste, a Thrilling Jazz Pianist Whose First Goal Is to Entertain |Howard Wolfson |February 16, 2014 |DAILY BEAST 

What the diatribe lacked in grammatical proficiency, it made up for in drama. Netflix Promises More ‘Arrested Development,’ Batman Prequel Coming to Fox |Culture Team |September 25, 2013 |DAILY BEAST 

McKinley was scoring well below state averages for reading and math proficiency. ‘We the Parents’ Chronicles L.A.’s Controversial Charter-School Law |Eliza Shapiro |August 15, 2013 |DAILY BEAST 

There is simply no substitute for the experience and proficiency of a properly trained pilot. Asiana Flight 214: The Pilot Screwed Up, Right? |Clive Irving |July 12, 2013 |DAILY BEAST 

I study your language in your Dickens, in your Thackeray; at last I attain proficiency. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

A host is presented as having invited to his table as many of his friends as were conspicuous for their proficiency in swearing. A Cursory History of Swearing |Julian Sharman 

Chivey had made a chatting acquaintance with the notary in the town, the Spaniard speaking English with tolerable proficiency. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng 

In his youth he attained a very considerable proficiency in the Latin tongue. Witch, Warlock, and Magician |William Henry Davenport Adams 

He does not write of his exploits as though he were always amazed at his own proficiency. American Sketches |Charles Whibley