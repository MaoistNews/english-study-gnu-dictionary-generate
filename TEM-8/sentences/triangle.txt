For example, you can change drawing A into drawing B by flipping the triangle made by nodes 1, 2 and 3 over the edge connecting nodes 2 and 3. A New Algorithm for Graph Crossings, Hiding in Plain Sight |Stephanie DeMarco |September 15, 2020 |Quanta Magazine 

A triangle with sides of all different lengths is “scalene,” or “unequal.” Why Mathematicians Should Stop Naming Things After Each Other - Issue 89: The Dark Side |Laura Ball |September 2, 2020 |Nautilus 

There is the classic triangle, a sturdy square, and you may even see some rectangular shapes if you are dealing with a nice flatbread. Pizza cutters that will get you the slice of your dreams |PopSci Commerce Team |September 2, 2020 |Popular-Science 

For the four Platonic solids built out of squares or equilateral triangles — the cube, tetrahedron, octahedron and icosahedron — mathematicians recently figured out that the answer is no. Mathematicians Report New Discovery About the Dodecahedron |Erica Klarreich |August 31, 2020 |Quanta Magazine 

Like the triangle and the quadrilateral, the N-gon with the maximum area was a regular N-gon, with equal side lengths and angles. Can You Cover The Globe? |Zach Wissner-Gross |August 28, 2020 |FiveThirtyEight 

This Texas Triangle now has the same population as the entire State of Florida. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

The scorned party in a love-triangle, he blew his head off while serving overnight tower duty in 2007. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

For her first film, she played a woman caught in a love triangle between a nightclub owner and a country boy. Does Fashion Week Exploit Teen Models? |Jennifer Sky |September 14, 2014 |DAILY BEAST 

We kept joking about the show evolving and blossoming from this love triangle into this five-pointed star. Kerry Washington’s Favorite ‘Scandal’ Season 3 Moments |Kerry Washington |August 8, 2014 |DAILY BEAST 

In reading about ASHA, its current location is hard to overlook—Research Triangle, North Carolina. STI Awareness Month Is Nothing More Than a Hallmark Holiday for Condoms |Kent Sepkowitz |April 18, 2014 |DAILY BEAST 

On the other hand, the arrows along the sides of the triangle represent actual circulation. Readings in Money and Banking |Chester Arthur Phillips 

He was a narrow-headed man with frail-looking sloped shoulders and a thin triangle of face. Hooded Detective, Volume III No. 2, January, 1942 |Various 

There are sixty thousand acres of mighty good spruce in that triangle between us, and it's as good as ours. Scattergood Baines |Clarence Budington Kelland 

(e) No part of the counter shall intersect a triangle or the produced perpendicular thereof shown on p. 186. Yachting Vol. 2 |Various. 

A third game was called trign, and was played by three persons, stationed at the angles of an equilateral triangle. The Private Life of the Romans |Harold Whetstone Johnston