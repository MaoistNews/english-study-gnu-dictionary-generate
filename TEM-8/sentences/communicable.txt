With the current even more communicable sub-variants it is likely that nearly all children have already been infected at least once. This Is the Phase of the Pandemic Where Life Returns To Normal |Steven Phillips |August 3, 2022 |Time 

The federal government requires only that donated sperm and eggs be treated like other human tissue and tested for communicable diseases – infectious conditions that spread through viruses, bacteria and other means – but not genetic diseases. Sperm Donation Is Largely Unregulated, But That Could Soon Change As Lawsuits Multiply |LGBTQ-Editor |January 19, 2022 |No Straight News 

However, too often we are unable to make productive use of the extra years we have been blessed with due to the fact that old age is associated with a host of non-communicable diseases, general physical frailty, and disability. Delaying Aging Would Bring Trillions of Dollars in Economic Gains, Study Finds |Edd Gent |July 12, 2021 |Singularity Hub 

During the war, infections and communicable diseases were rampant among troops, prompting American soldiers to organize and start a health care system. From AIDS to COVID-19, America’s Medical System Has a Long History of Relying on Filipino Nurses to Fight on the Frontlines |Paulina Cachero |May 30, 2021 |Time 

A key article in this law gives the president broad authority “to prevent the introduction of epidemic diseases into this country from abroad and to prevent the interstate spread of communicable diseases.” The new CDC guidelines remind that presidential leadership matters during pandemics |Howard Markel |May 14, 2021 |Washington Post 

Because the music here is so free, so joyous, so relaxed that all its pleasures are instantly communicable. Digging the Gold in Dylan’s ‘Basement’ |Malcolm Jones |November 5, 2014 |DAILY BEAST 

That said, SARS is much more communicable than Ebola, meaning it is easier to catch. Fact-Checking the Sunday Shows: October 19 |PunditFact.com |October 19, 2014 |DAILY BEAST 

Hunt said the state is seeking to treat the disease as all other communicable diseases are treated. Kansas Quarantine Bill Has HIV/AIDS Advocates Up in Arms |David Freedlander |April 2, 2013 |DAILY BEAST 

“Transnational corporations are major drivers of the global epidemic of NCDs [non-communicable diseases],” said the researchers. Britain’s Weight Crisis Almost Hits U.S. Proportions |Dan Jones |February 21, 2013 |DAILY BEAST 

Non-Communicable Diseases, which will attempt to address the latest threat to global health. On the Frontlines of Global Pandemics: Peter Piot’s Memoir, ‘No Time to Lose‘ |Edward Platt |July 1, 2012 |DAILY BEAST 

It is communicable by inoculation, but it is very doubtful that the disease has been communicated from man to man. Essays In Pastoral Medicine |Austin Malley 

But the outcome has to be expressed in words if it is to be communicable. The Analysis of Mind |Bertrand Russell 

The moral wisdom of the man is not communicable to women, so far as it partakes of rational wisdom, 168. The Delights of Wisdom Pertaining to Conjugial Love |Emanuel Swedenborg 

These are the symptoms of blood-poisoning, vividly portrayed; of some contagion, communicable by infection. Appletons' Popular Science Monthly, June 1899 |Various 

There was a report on the birth-rate, the death-rate, the anomaly-rate, and a breakdown of all reported communicable diseases. Pariah Planet |Murray Leinster