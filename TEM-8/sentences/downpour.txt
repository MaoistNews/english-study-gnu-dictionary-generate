We may see downpours end around sunrise but may still have some pesky showers linger into midday or early afternoon. D.C.-area forecast: Showers today, with soaking rain tonight |A. Camden Walker, Jason Samenow |December 4, 2020 |Washington Post 

Scattered downpours, gusty winds possible thru mid-afternoon. Updates: Tornado watch for areas east of District |Jason Samenow |November 30, 2020 |Washington Post 

Although climate change is expected to include gradual warming globally, scientists think that extreme events such as heat waves, cold snaps, droughts and torrential downpours could also grow in number and strength over time. How frigid lizards falling from trees revealed the reptiles’ growing cold tolerance |Charles Choi |October 30, 2020 |Science News 

Water and electronics simply don’t mix, and many an expensive device has been rendered useless by an errant spill or unexpected downpour. Best wireless earbuds: Five things to consider |PopSci Commerce Team |October 30, 2020 |Popular-Science 

Just don’t send it off down the street during a torrential downpour. Five classic paper toys you can make when you’re bored (whether you’re in school or not) |John Kennedy |October 7, 2020 |Popular-Science 

A few tear-sized raindrops were followed by a suddenly heavy downpour as the family inside stepped up for a last farewell. The Gentle Giant Cut Down by Cops |Michael Daly |July 24, 2014 |DAILY BEAST 

Then the air suddenly turns still, the downpour stops, and the sky transforms from grey to turquoise. Want to Write a Book? Go to Iceland |Adam LeBor |May 26, 2014 |DAILY BEAST 

Adding to the pathos, the fickle Charlotte skies chose that moment to unleash a biblical downpour on the entire tableau. Odd DNC Bedfellows: Immigration, Occupy, Anti-Abortion Protesters |Michelle Cottle |September 6, 2012 |DAILY BEAST 

Noting that a downpour has ended, Ringo gets a roar of laughter when he quips “I just said stop and it did.” Ringo Starr Is Selling His Pictures and Donating the Proceeds to Charities |Blake Gopnik |June 26, 2012 |DAILY BEAST 

Her first foray abroad, earlier this month, brought a rare downpour of brickbats. An Insider at Last, Burma’s Suu Kyi Encounters New Political Woes |Peter Popham |June 14, 2012 |DAILY BEAST 

And he stepped out into the rain, which had begun again a few minutes earlier, and was now falling in a steady downpour. St. Martin's Summer |Rafael Sabatini 

The wind and rain had ceased, but the dripping of the branches still kept up an excellent imitation of a downpour. Three More John Silence Stories |Algernon Blackwood 

With a good blinding snowfall, or a pelting downpour of cats and dogs, I might have hoped for a respite. Johnny Ludlow, Fourth Series |Mrs. Henry Wood 

A sudden downpour of rain, without thunder or lightning, had just passed rapidly over our wide plain. Dream Tales and Prose Poems |Ivan Turgenev 

For long the downpour never ceased by night or by day; it was the rain that rained away the Corn Laws. The World's Greatest Books, Vol X |Various