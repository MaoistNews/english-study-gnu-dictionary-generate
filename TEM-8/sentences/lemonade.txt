The grand fun of “Hitman” runs are just how it all goes wrong, and how 47 is able to make lemonade out of lemons and spilled blood. ‘Hitman 3’ is the grandest stage for your own stories, even as it tries to end its own |Gene Park |January 19, 2021 |Washington Post 

Discarding pounds has been a method of control, a way to corral the chaos and make lemons into sugar-free lemonade. Tallying up a year of loss: A lot of pounds, too many loved ones, countless connections |Jerry Brewer |December 27, 2020 |Washington Post 

Add a splash of rye, and lemonade quickly becomes very adult. 4 Hot Cocktail Recipes to Save Your Winter Social Life |AC Shilton |December 17, 2020 |Outside Online 

Heat up your thermos, then add three ounces of warm lemonade and two ounces of rye. 4 Hot Cocktail Recipes to Save Your Winter Social Life |AC Shilton |December 17, 2020 |Outside Online 

Still, a run through this exercise does reveal at least a few ways the selection committee could make lemonade out of lemons if it so chooses. A preseason March Madness bracket for a most unusual college basketball season |Patrick Stevens |November 23, 2020 |Washington Post 

One political observer summed up the atmospherics: “It looks like two guys drinking lemonade with the sugar left out.” Beijing’s ‘Star Trek’ APEC Summit |Jake Adelstein |November 11, 2014 |DAILY BEAST 

Doritos: Time Machine Forget your run-of-the-mill lemonade stand. The 15 Best Super Bowl 2014 Commercials |Ben Teitelbaum |February 2, 2014 |DAILY BEAST 

Vali sings while playing with an alligator on a pink swing set, hustling at a lemonade stand and dancing with shadowy creatures. Miley Cyrus, Arcade Fire & More Best Music Videos of the Week (VIDEO) |Victoria Kezra |September 15, 2013 |DAILY BEAST 

Mrs. Schulz tried to offer me more sour lemonade in a paper cup. The Day I Met Charles Schulz |Daniel J. Levitin |February 12, 2013 |DAILY BEAST 

Inviting us in, he offered us lemonade that his wife had in a big pitcher. The Day I Met Charles Schulz |Daniel J. Levitin |February 12, 2013 |DAILY BEAST 

At the old Rosewater dances we never had anything but cake and lemonade—ice-cream in very hot weather. Ancestors |Gertrude Atherton 

"Because I thought a pitcher wouldn't hold lemonade enough," said Willy. The Nursery, November 1881, Vol. XXX |Various 

Pie for sale on the grounds, and rocks to crack it with; and ciRcus-lemonade—three drops of lime juice to a barrel of water. A Connecticut Yankee in King Arthur's Court, Complete |Mark Twain (Samuel Clemens) 

The miners gave each of them a glass of fresh lemonade to drink, and rubbed their temples with vinegar. Black Diamonds |Mr Jkai 

But it was only Azalea Courtney, radiant as the morning, carrying a little silver pitcher full of iced lemonade. They Looked and Loved |Mrs. Alex McVeigh Miller