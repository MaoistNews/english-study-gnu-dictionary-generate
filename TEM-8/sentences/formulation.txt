We’ve continued to innovate on this since launch, releasing a brand-new midsole foam formulation that has even higher rebound while maintaining the same durability for a shoe health care workers will wear more than any other in their life. This new sneaker brand is meant for health care workers |Rachel King |December 6, 2020 |Fortune 

The group then recruited Sammy Hagar as lead singer —some critics called the new formulation “Van Hagar” — and the band went on to score its first No. Guitar legend Eddie Van Halen dies of cancer at 65 |radmarya |October 6, 2020 |Fortune 

Rogers says the formulation is different depending on the the type of fruit or vegetable. Exclusive: Startup Apeel is launching ‘plastic-free’ cucumbers at Walmart to cut back on waste |Beth Kowitt |September 21, 2020 |Fortune 

As a first step, Aaronson, a computer scientist, came up with an alternate formulation of the Collatz conjecture, called a rewriting system, that was more natural for computers to work with. Computer Scientists Attempt to Corner the Collatz Conjecture |Kevin Hartnett |August 26, 2020 |Quanta Magazine 

The original formulation of Keller’s conjecture is about smooth, continuous space. Computer Search Settles 90-Year-Old Math Problem |Kevin Hartnett |August 19, 2020 |Quanta Magazine 

But their formulation includes some solicitations with educational material on it as money spent on programs. Wounded Warrior Project Under Fire |Tim Mak |September 26, 2014 |DAILY BEAST 

How many times have you heard some formulation of this viewpoint? Communism's Victims Deserve a Museum |James Kirchick |August 25, 2014 |DAILY BEAST 

The only problem with Obama's formulation is its implicit assumption that Iraqi leaders can simply choose to make up. How Iran and America Can Beat ISIS Together |Ben Van Heuvelen |June 21, 2014 |DAILY BEAST 

For the last half decade, the simple formulation of immigration advocates pushing for reform had been simple: Democrats Good. Immigration Activists Blast Obama |David Freedlander |March 27, 2014 |DAILY BEAST 

That was the formulation—General Motors is alive and Osama bin Laden is dead. Obama’s Second-Term Crisis |David Freedlander |January 27, 2013 |DAILY BEAST 

Aware of this personal condition, I put aside thought of any present formulation of a future. The Onlooker, Volume 1, Part 2 |Various 

Attempts at formulation follow the lines of culture, and it is not till a comparatively late stage that they reach definite shape. Introduction to the History of Religions |Crawford Howell Toy 

It was the formulation of what he had vaguely felt in an uncomfortable way ever since the previous evening. The Fourth Estate, vol.1 |Armando Palacio Valds 

Matt smiled with satisfaction in what he felt to be his very successful formulation of Maxwell. The Quality of Mercy |W. D. Howells 

Last of all comes the systematic formulation of the political Constitution itself. Elements of Folk Psychology |Wilhelm Wundt