Schlacter’s installation, titled “Darn” is crafted from yarn, paper mâché sculptures, and hand-made, wall-mounted loom structures. Transformer unveils this year’s ‘Queer Threads’ exhibits |Parker Purifoy |October 22, 2020 |Washington Blade 

Marikar’s is a good yarn about a company that is doing well by trying to do good. A woolly story about how Allbirds makes its shoes |Adam Lashinsky |September 22, 2020 |Fortune 

They come in two versions, a liquid silicone rubber that looks a lot like Apple’s original Sport Band and a braided version made from recycled yarn that truly sparkles. Apple Watch Series 6 first impressions: A stretchy addition looks great |Aaron Pressman |September 17, 2020 |Fortune 

Christopher Plummer hopes audiences will enjoy watching a “good yarn” when his new series Departure debuts on Peacock Thursday. Christopher Plummer on his new series ‘Departure’ and acting in different formats |radmarya |September 16, 2020 |Fortune 

Meanwhile, 40 miles south of Conover, in the town of Belmont, the Textile Technology Center at Gaston College specializes in what the industry refers to as “yarn.” Unmade in America |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

To that regard, Mulaney opens with its star spinning a longer joke-yarn based on something that had really happened to him. Is John Mulaney the Next Seinfeld? |Kevin Fallon |October 5, 2014 |DAILY BEAST 

Many of us strike a happy medium, leaving enough wiggle room with reality to spin a good yarn. Writing a Novel: Even Making It Up Requires Research |Ridley Pearson |July 16, 2014 |DAILY BEAST 

But at least according to its creator, it's also a meta-page-turning crime yarn—a story about storytelling. ‘True Detective’ Episode 5 Review: ‘The Secret Fate of All Life’ is the Best Episode Yet |Andrew Romano |February 17, 2014 |DAILY BEAST 

Perhaps that's why Goldfarb didn't comment any further on his yarn. Michael Goldfarb Doesn't Care About Facts |Ali Gharib |February 25, 2013 |DAILY BEAST 

I got a bit of flak for interviewing Kelley, though the story was widely picked up: why dive into this tabloid yarn? Like Jill Kelley, Paula Broadwell Eyes Comeback After Petraeus Scandal |Howard Kurtz |January 23, 2013 |DAILY BEAST 

I didn't take much stock in the yarn at the time, but I'm beginning to think he had it straight. Raw Gold |Bertrand W. Sinclair 

Without embarking on another endless yarn let me note the fact that there are two schools amongst our brethren afloat. Gallipoli Diary, Volume I |Ian Hamilton 

You can probably realize just how headquarters would take the sort of yarn we'd spin if we dashed in and told them the truth. Raw Gold |Bertrand W. Sinclair 

Before that time we always put rope-yarn between the lap of the boiler-plates to make the seams tight. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

He said, 'Now you shall never make another boiler for me with rope-yarn.' Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick