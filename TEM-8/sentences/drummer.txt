Everybody wrote compositions except for our drummer Mickey Roker. Lee Morgan’s ‘Live at the Lighthouse’ was a masterpiece that turned out to be a farewell |Shannon J. Effinger |August 27, 2021 |Washington Post 

If you look at the size of his kit, it’s ludicrous compared with what most drummers use these days. Charlie Watts, Rolling Stones drummer and band’s rhythmic mainstay, dies at 80 |Matt Schudel |August 24, 2021 |Washington Post 

She didn’t know that we were out of practice or that half of the drummers were trained in other instruments. What science tells us about reducing coronavirus spread from wind instruments |Betsy Ladyzhets |August 6, 2021 |Science News 

After performing with a famed drummer and true icon in the music industry just two years back on The Tonight Show, some social media users are finding it hard to believe that DaBaby doesn’t know who Questlove is. DaBaby Gets DaBusiness For Claiming He Doesn’t Know Questlove |cmurray |July 30, 2021 |Essence.com 

Questlove, the Roots drummer who recently directed the music documentary “Summer of Soul,” about the 1969 Harlem Cultural Festival, shared a list on Instagram of artists he would have hypothetically liked to see at an updated version of the festival. DaBaby’s homophobic remarks draw a week’s worth of rebukes from Madonna, Elton John, Questlove and more |Sonia Rao |July 30, 2021 |Washington Post 

He plays an aging punk rocker and I play the drummer from his old band. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

I was friends with her drummer from Sleater-Kinney, and I met Carrie, and we just hit it off. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

He started to tell Cavett about his teenage dreams of becoming a jazz drummer. Why Comedians Still Think Bill Cosby Is a Genius |Mark Whitaker |October 5, 2014 |DAILY BEAST 

When Perl mentioned she was looking for a female drummer in the Hasidic community, a mutual friend introduced her to Dalia. The Sisterhood of Bulletproof Stockings: It’s Ladies’ Night for Hasidic Rockers |Emily Shire |September 30, 2014 |DAILY BEAST 

Max Drummer (Harrison Ford) is the CIA chief who ordered the expendables to take Stonebanks down. I Am The Hague! Sylvester Stallone's 'Expendable' Wet Dream |Amy Zimmerman |August 18, 2014 |DAILY BEAST 

The other drummer appealed to the stage-driver to support his contention that the court's action was novel, but entirely just. Mystery Ranch |Arthur Chapman 

A drummer soon came with a message from the besiegers, but a message which was utterly unintelligible to the besieged. The History of England from the Accession of James II. |Thomas Babington Macaulay 

So with a shout of victory, and the drummer beating a lively march, they rushed to the redoubt, where not a man was to be seen. Elsie's Vacation and After Events |Martha Finley 

He was not a drummer at Austerlitz, and for the instant he did not remember the tune the drummers played. When Valmond Came to Pontiac, Complete |Gilbert Parker 

Valmond had spoken down at the aged drummer, whose arms were young again, as once more he marched on Pratzen. When Valmond Came to Pontiac, Complete |Gilbert Parker