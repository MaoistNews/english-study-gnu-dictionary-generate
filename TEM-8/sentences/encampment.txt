Much of the armed encampment in downtown Washington, however, will be gone by the end of Sunday. The fortress around downtown D.C. is being dismantled. But heightened security may remain. |Emily Davies, Michael Brice-Saddler, Peter Hermann |January 21, 2021 |Washington Post 

The removal of encampments, such as the one on K Street, has been an ongoing issue, she said. Inauguration security puts homeless trans people at increased risk |Kaela Roeder |January 19, 2021 |Washington Blade 

Ramirez said he also decided to vote for Rodriguez in part because he’s younger than a lot of candidates on the ballot and thinks Rodriguez will put a stop to growing homeless encampments. ‘Every Candidate Is Different and So Complex’: Voices of the Voters in Oceanside |Kayla Jimenez |November 3, 2020 |Voice of San Diego 

He’s also launched a 2022 ballot measure committee aimed at reducing homelessness and growing encampments after he steps down from a position of power. Morning Report: The La Jolla Schools Exodus |Voice of San Diego |September 21, 2020 |Voice of San Diego 

Two days later, he told Politico about his plans to launch a committee backing a 2022 ballot initiative aimed at reducing homelessness and the growing encampments. Sacramento Report: The Debate Over SB 145 Sure Escalated Quickly |Voice of San Diego |September 18, 2020 |Voice of San Diego 

Somehow, a bold and eccentric encampment gave heart to progressives at all levels of respectability. Progressive-palooza: On Obama, Occupy, and Moral Monday |Jedediah Purdy |July 5, 2014 |DAILY BEAST 

The entire inner circle of the Maidan encampment was surrounded by rings of fire created by the protestors to keep the police out. My Life Behind Kiev’s Barricades |Vijai Maheshwari |February 21, 2014 |DAILY BEAST 

Members of the Farsala Roma community gave the videos of Maria during her time in the encampment to Greek Alpha TV. The Mysterious Case of Maria, Greece’s Fake Roma Child |Barbie Latza Nadeau |October 21, 2013 |DAILY BEAST 

Just after dawn this morning, I was inside the encampment around the Rabaa al-Adaweyah mosque in the Nasr City district of Cairo. Egypt’s Government Thugs Beat Me Up at the Rabaa Sit-In |Mike Giglio |August 14, 2013 |DAILY BEAST 

The people in Gezi Park, which has become home to a large encampment reminiscent of the Occupy movement, remained. Standoff in Taksim |Mike Giglio |June 12, 2013 |DAILY BEAST 

Washington attacked a French encampment at the confluence of the Alleghany and Monongahela. The Every Day Book of History and Chronology |Joel Munsell 

As they rode back toward the encampment the stars were peeping out, and the moon had begun to climb above the hills. God Wills It! |William Stearns Davis 

Richard stepped from the tent; the sky was graying in the east; encampment—men, horses, all—were vague black shadows just visible. God Wills It! |William Stearns Davis 

That the Seljouks should so prevail over the soldiers of the Cross as to menace the encampment, scarce entered his head. God Wills It! |William Stearns Davis 

A man came across the great water, up the river, and along the forest trail, to pause at our encampment with a solemn message. Menotah |Ernest G. Henham