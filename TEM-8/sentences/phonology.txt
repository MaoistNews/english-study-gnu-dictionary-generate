I wanted it to have a sound very much like Arabic, so the phonology of Arabic influenced the way it sounds and the rhythm. Game of Thrones: 10 Secrets About HBO's Adaptation |Jace Lacob |April 4, 2011 |DAILY BEAST 

Phonology: Orm supplemented the current graphic methods by devices of his own. Selections from Early Middle English 1130-1250: Part II: Notes |Various 

Phonology: This has been sufficiently described at pp. 493-9: a few forms call for comment. Selections from Early Middle English 1130-1250: Part II: Notes |Various 

Phonology: This section should be compared with pp. 581-586; explanations of abnormal forms offered there are not repeated. Selections from Early Middle English 1130-1250: Part II: Notes |Various 

The subsequent changes in orthography are due mainly to changes of sound, and find their explanation in the phonology. Encyclopaedia Britannica, 11th Edition, Volume 11, Slice 1 "Franciscans" to "French Language" |Various 

These three groups were distinguished from each other by characteristic points of phonology and inflection. Encyclopaedia Britannica, 11th Edition, Volume 9, Slice 6 |Various