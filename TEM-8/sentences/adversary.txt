Perhaps because it had been passed down by leaders who understood the significance of engaging the adversary in their own language, the phrase stuck. How ‘A Luta Continua’ Became the Rallying Slogan for African Revolutions |Eromo Egbejule |October 9, 2020 |Ozy 

It would “be a major challenge for an adversary” to forge or change enough ballots to shift the outcome of the election, he told the Senate Homeland Security Committee last week. Five key science takeaways from the first presidential debate |Purbita Saha |September 30, 2020 |Popular-Science 

Back then, Kolfage told Fox 10 Phoenix that he felt he needed to take legal action after adversaries started going after his family and tried to ruin the career of his wife, who was a teacher and a model. Veteran, War Hero, Defendant, Troll |by Jeremy Schwartz and Perla Trevizo |September 29, 2020 |ProPublica 

In particular, I was helping them to try and understand how the adversaries in Afghanistan and Pakistan were financing themselves. When Your Safety Becomes My Danger (Ep. 432) |Stephen J. Dubner |September 24, 2020 |Freakonomics 

We know our adversaries around the world are pursuing their own advances. U.S. vies for leadership in quantum and A.I. with $1 billion bet |rhhackettfortune |August 26, 2020 |Fortune 

The North Korean hackers have proven to be a persistent adversary, if not the most skilled one. Obama Could Hit China to Punish North Korea |Shane Harris, Tim Mak |December 20, 2014 |DAILY BEAST 

In spite of the many disagreeements, the Obama administration, he said, does not view Russia as “an adversary.” It’s Finally Time for the West to Stand Up to Putin |James Kirchick |July 18, 2014 |DAILY BEAST 

All the Americans, from FDR down, underestimated the capabilities of their adversary, and exaggerated their own. D-Day Was The Largest And One Of The Bloodiest Invasions In History |James A. Warren |June 6, 2014 |DAILY BEAST 

Are we not allowed to talk about our deepest human adversary without being sucked back into a political controversy? Love Versus the ‘Liberal Gulag’ |James Poulos |April 8, 2014 |DAILY BEAST 

The al Qaeda prisoners we held at CIA facilities helped us understand the adversary. Never Forget? The CIA Report and the Problem With Hindsight |Philip Mudd |March 15, 2014 |DAILY BEAST 

While he stood, apparently quiescent, in the clutch of his adversary, he still held his hand on his sword. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

He kept his sword pointed at the eyes of his adversary; but he never rested for an instant. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

This time there was no interference, and Ney so severely wounded his adversary that he was unable to continue his profession. Napoleon's Marshals |R. P. Dunn-Pattison 

It always pains me to acknowledge that any man, even an adversary, has acted dishonourably. God and my Neighbour |Robert Blatchford 

And thirdly he knew that his adversary would cheat if he could and that his adversary suspected him of fraudulent designs. The Joyous Adventures of Aristide Pujol |William J. Locke