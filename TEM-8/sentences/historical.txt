Yes, North America and the EU ought to acknowledge their historical responsibility for emissions. What’s causing climate change, in 10 charts |David Roberts |September 11, 2020 |Vox 

A Han Gwich’in dog musher and triathlete who grew up in the village of Eagle, she’d taken the stage at the group’s 2017 convention as a keynote speaker describing personal loss and historical trauma. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

Many of them didn’t have any content related to significant LGBTQ historical figures as part of the history curriculum. LGBTQ-inclusive curriculum laws take effect in N.J., Ill. |Philip Van Slooten |September 9, 2020 |Washington Blade 

After being trained on historical data, the algorithm falsely classified patients with asthma as extremely low risk. Disco, bell bottoms, big hair…and cutting-edge A.I.? |Jeremy Kahn |September 8, 2020 |Fortune 

Vitale has 30 years of experience studying policing and advising community-based movements for reform and will discuss the historical role police have played as well as their current role. Defunding the Police virtual info session Sept. 8 |Philip Van Slooten |September 2, 2020 |Washington Blade 

Historical justifications for most modern celebrations can be found in the ancient world. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

Hitchcock had the historical good fortune to have worked from silent films through television. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The central thrust of the sequence derives from historical fact. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

If there was anyone who thought otherwise, he left no historical record. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

“Unlike Turkey or Egypt, we have no art-historical tradition,” he told The Telegraph in 2002. The Mysterious Death of the Art World’s Favorite Sheikh |Lizzie Crocker |November 13, 2014 |DAILY BEAST 

And I finished all with a brief historical account of affairs and events in England for about a hundred years past. Gulliver's Travels |Jonathan Swift 

Eustache le Sueur died; one of the best French historical painters of his time. The Every Day Book of History and Chronology |Joel Munsell 

He distinguished himself by several military exploits, and wrote some valuable historical works. The Every Day Book of History and Chronology |Joel Munsell 

Samuel Squire, bishop of St. David's died; a poetical, historical and antiquarian writer of note. The Every Day Book of History and Chronology |Joel Munsell 

Paul de Rapin de Thoyras, an eminent French historical writer, died. The Every Day Book of History and Chronology |Joel Munsell