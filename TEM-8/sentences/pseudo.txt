Jeff Bezos, the founder of Amazon and newly minted pseudo-astronaut, has a lot of money to spend. What Jeff Bezos' Philanthropy Tells Us About His New Priorities—and What Change They May Bring |Belinda Luscombe |July 27, 2021 |Time 

Video producer Dave Jorgenson, who had been creating content for the platform in 2019, was able to turn his “uncool guy” persona into a relatable pseudo-influencer for news and political content while posting from his home during the pandemic. Digiday Guide: Everything you need to know about Gen Z’s media consumption habits |Kayleigh Barber |June 23, 2021 |Digiday 

Shares for substitutes in a potentially $100 billion market are anticipated to boom as the pseudo stuff replaces the real thing, which is less healthy primarily because it carries too many calories and wreaks havoc with your blood sugar levels. 15 Things to Know About Sugar |Sean Culligan |April 6, 2021 |Ozy 

Albums are supposed to be dead, but surprise, they’re back like zombies, dominating the streaming landscape with their bulky tracklists and pseudo-grandeur. Album of the year is the Grammy that nobody deserves to win |Chris Richards |March 12, 2021 |Washington Post 

Conservative economics, he writes, must accord equal respect to capital and labor, rather than pseudo-factually assume that “whatever is best for shareholders in the short run will eventually prove best for workers as well.” Fork in the Road: Where Will the GOP Turn? |Nick Fouriezos |January 10, 2021 |Ozy 

The office is standard Universal issue, sort of a pseudo English manor house. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Thankfully, the piece did not try to evoke the Internet through tired dance gestures or pseudo-digital music. Sneer and Clothing in Miami: Inside The $3 Billion Woodstock of Contemporary Art |Jay Michaelson |December 6, 2014 |DAILY BEAST 

So the real meat of Stalker, ostensibly, is the pseudo-intellectual conversation about misogyny. ‘Stalker’ Is an Awful Warning Against the Evils of Misogyny |Sujay Kumar |October 3, 2014 |DAILY BEAST 

He lived in the Pseudo office, where his downstairs neighbors included Jeff Koons. A ‘Truman Show’ For Today: The Return of Josh Harris |Anthony Haden-Guest |July 11, 2014 |DAILY BEAST 

He was busted but far from bust, and by February Pseudo had 10 channels. A ‘Truman Show’ For Today: The Return of Josh Harris |Anthony Haden-Guest |July 11, 2014 |DAILY BEAST 

The next source from which we learn anything of this part of the subject is the pseudo-Euclidean Introductio Harmonica. The Modes of Ancient Greek Music |David Binning Monro 

"We've very much obliged to you, Miss Fulton," Bristow said in his pseudo-pleasant way. The Winning Clue |James Hay, Jr. 

In pseudo-angina, frequently observed in hysterical women, its action is all that can be desired. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

And this false conviction I found in the pseudo-Christian religion which men had been teaching for fifteen hundred years. My Religion |Leo Tolstoy 

Ethical and moral instruction have disappeared from our pseudo-Christian society without leaving a trace. My Religion |Leo Tolstoy