They just turn on a sixpence and go and look for something else. Hanging Out with Ian McEwan: Full Transcript |The Daily Beast Video |April 14, 2010 |DAILY BEAST 

A friend of mine tells me that he smokes every day, at a cost of about sixpence a-week. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Now, I would like to know in what other way so much enjoyment is to be bought for sixpence. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

It is certain that he restored every sixpence that had been deposited in the morning, and could not die until he had done so. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

On arriving he tendered the driver sixpence, which was strictly the fare, though but scant remuneration for the distance. The Book of Anecdotes and Budget of Fun; |Various 

A message can be sent from any postoffice at a cost of sixpence for the first ten words. British Highways And Byways From A Motor Car |Thomas D. Murphy