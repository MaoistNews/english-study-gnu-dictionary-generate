It was also a comfortable jacket for ski transitions on days when the mercury dipped to the low thirties. Do You Actually Need a 1,000-Fill Down Jacket? |Ryan Wichelns |December 23, 2020 |Outside Online 

When the mercury drops, Frank Vallas, beverage manager at Dallas-based Tacos and Tequila, brews this mix of hot tea and tequila. 4 Hot Cocktail Recipes to Save Your Winter Social Life |AC Shilton |December 17, 2020 |Outside Online 

With coronavirus cases spiking and the mercury dropping, sparking a run on backyard heating devices, I knew Bryner could tell me what will happen when the fuel for those heaters is burned. Covid-19 sparked a run on outdoor heaters and fire pits. Which is better for the planet? |Sarah Kaplan |December 11, 2020 |Washington Post 

Turns out they’re also a prime source of toxic mercury, scientists reported on November 16 in Proceedings of the National Academy of Sciences. Researchers found signs of human pollution in animals living six miles beneath the sea |Kate Baggaley |November 19, 2020 |Popular-Science 

When someone on Earth goes from standing to lying down, that pressure rises from around 0 millimeters of mercury to about 15 mmHg. Surviving Mars missions will take planning and lots of innovation |Maria Temming |October 22, 2020 |Science News For Students 

According to Wynd, “Freddie Mercury once said he wanted to lead a Victorian life surrounded by exquisite clutter.” Dodo Bones and Kylie’s Poo: Inside London’s Strangest New Museum |Liza Foreman |November 11, 2014 |DAILY BEAST 

He was demoted at the Mercury News, and left the paper in 1997. Jeremy Renner Opens Up About Marriage, His Problems with the Media, and the Future of Hawk-Eye |Marlow Stern |September 29, 2014 |DAILY BEAST 

In an interview with the San Jose Mercury News, Shi described one unwelcome encounter with Zhang. Yahoo’s Accused Sexual Harasser Asked Women to Wear More Skirts at Work |Olivia Nuzzi |July 14, 2014 |DAILY BEAST 

That means most of these planets orbit closer than Mercury does to the Sun. The Exoplanet That Wasn’t There |Matthew R. Francis |July 6, 2014 |DAILY BEAST 

Caroline Sweeney, the police reporter for the Pottstown Mercury, updates the page at least once a week. Pinterest’s Most Wanted: The Cops Now Want Pinners’ Data, Too |Abby Haglage |March 11, 2014 |DAILY BEAST 

These have canted bay windows below them, and their pediments are surmounted by figures representing Mercury and Athæne. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

But the day he planned to start was very cold—the mercury stood twenty-seven below zero. The Homesteader |Oscar Micheaux 

Bang went the fragile bulb, as it splintered into a thousand atoms, and the mercury shot in sparkling globules over the table. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

If it be true that Venus does not turn upon its axis, such is likely to be the case also with the planet Mercury. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Next beyond Mercury is Venus, a sphere only a little less in diameter than the earth. Outlines of the Earth's History |Nathaniel Southgate Shaler