Gloria had “a bad feeling” about Vanessa enlisting, she recalls—a premonition that she now she chalks up to a mother’s intuition. How Vanessa Guillen's Tragic Death Is Helping Transform Sexual Assault Reporting in the U.S. Military |Jasmine Aguilera |December 30, 2021 |Time 

We’ve had four whales come out of nowhere — no premonition or warning that this was on the way. Four gray whales found dead in eight days in the San Francisco Bay area |Paulina Firozi |April 11, 2021 |Washington Post 

One night, when her daughter, Sarah Dawn Ray, called, the two women had a premonition that the situation could soon turn violent. Under Obama, a Breakdown in the Death Benefits Owed to Veterans |Aaron Glantz |May 1, 2013 |DAILY BEAST 

An accident victim named Johnny Smith wakes from a coma with a skill at premonition. Stephen L. Carter’s Book Bag: 5 Best Presidential Thrillers |Stephen L. Carter |July 10, 2012 |DAILY BEAST 

From the moment the Tea Party emerged on the scene, I had a premonition that I would eventually have to leave the GOP. Goodbye GOP |David Frum |June 12, 2012 |DAILY BEAST 

As Conan says with eerie premonition, “I just think that guy is going to hurt me.” Secrets of the Late Night War |Bryan Curtis |November 8, 2010 |DAILY BEAST 

But I am shocked how these new rumors have been codified into a kind of collective South African premonition. Will the World Cup Start a Riot? |Gretchen L. Wilson |June 10, 2010 |DAILY BEAST 

All through the sad duties of the next four days Felipe was conscious of the undercurrent of this premonition. Ramona |Helen Hunt Jackson 

If Delancy had stayed a little longer at the scene of his crime, he would have learned that his premonition was founded in truth. Hooded Detective, Volume III No. 2, January, 1942 |Various 

So solemn was his air, so sober his voice, that both girls felt a premonition of the untoward message that he bore. Mistress Wilding |Rafael Sabatini 

Her arms drew closer about her body, while a shiver ran through it—a premonition perhaps. The Woman Gives |Owen Johnson 

Nita felt a strange, tremulous thrill sweep over her—was it ecstasy or a premonition of evil? They Looked and Loved |Mrs. Alex McVeigh Miller