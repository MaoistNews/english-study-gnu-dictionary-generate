In March, the Arlington County Board approved the redevelopment of the nearby Key Bridge Marriott to make way for 300 apartments and more than 150 condominiums. Rosslyn’s Holiday Inn building will be imploded Sunday morning |Justin Wm. Moyer |December 10, 2020 |Washington Post 

Madison officials have been studying other mall redevelopment plans around the country for ideas about what to do. The death of the department store and the American middle class |Jason Del Rey |November 30, 2020 |Vox 

The new Midway community plan the city approved a couple years ago would allow builders to add thousands of new homes and pave the way for a redevelopment of the Sports Arena. Housing and Homelessness on the Brain: Voices of the Voters in Central San Diego |Lisa Halverstadt |November 3, 2020 |Voice of San Diego 

It’s also where the city recently cut a deal to lease the land around the Sports Arena for redevelopment. The Ultimate Guide to the Local Election |Voice of San Diego |October 19, 2020 |Voice of San Diego 

In contrast, the competing proposal does not address the redevelopment of the Sports Arena. Public Benefits Define Midway Proposal |Frank Wolden, Aruna Doddapaneni and Colin Parent |July 21, 2020 |Voice of San Diego 

So Hsieh hired Damania to create a health-care program for Las Vegas, where Hsieh has a downtown redevelopment project. The Health-Care System Is So Broken, It’s Time for Doctors to Strike |Daniela Drake |April 29, 2014 |DAILY BEAST 

The second project—for a redevelopment property in the Shaw neighborhood—raised $250,000 in a week among just 14 investors. Fundrise Crowdsources Real Estate Developments in Washington, D.C. |Miranda Green |June 27, 2013 |DAILY BEAST 

The Redevelopment Authority owns 1,500 lots, most of which are for sale. Philadelphia Wants its Eyesore Back |Megan McArdle |September 20, 2012 |DAILY BEAST 

Fortunately, saner heads prevailed, and the apathetic pace of Manhattan redevelopment would have prevented it in any case. Ground Zero's Politics Are Over |Michael Tomasky |May 5, 2011 |DAILY BEAST 

He and two classmates from Hamburg had a grant to study and critique a redevelopment project in Old Cairo. Mohammed Atta and the Egypt Revolution |Terry McDermott |February 14, 2011 |DAILY BEAST 

With plants I do not know whether the redevelopment of rudimentary organs occurs more frequently under culture than under nature. The Variation of Animals and Plants Under Domestication, Volume II (of 2) |Charles Darwin 

We must expand our small but our successful area redevelopment program. Complete State of the Union Addresses from 1790 to 2006 |Various