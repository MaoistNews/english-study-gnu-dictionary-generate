Wana recently released a gummy that encapsulates cannabis oil in a water-soluble outer layer, which Hennesy says allows the body to metabolize it more quickly. A Gummy for Whatever Ails You |Rachel Del Valle |August 27, 2021 |Eater 

Mankovich and Fuller think the reason the structure works is that the rocks and ice near Saturn’s center are soluble in hydrogen, allowing the core to behave like a fluid rather than a solid. Saturn’s insides are sloshing around |Neel V. Patel |August 16, 2021 |MIT Technology Review 

Since it’s a water-soluble vitamin, any excess you take in will simply be excreted out in your pee. How a Nobel Prize winner spread the vitamin C myth |empire |June 26, 2021 |Popular-Science 

Oxidants help clear the air by reacting with contaminants like methane to form molecules that are more water soluble or stickier, allowing them to more easily rain out of Earth’s atmosphere or stick to its surface. Lightning may be an important source of air-cleaning chemicals |Maria Temming |April 29, 2021 |Science News 

Water-soluble products such as powdered bubble soap should be okay, but if you want to be especially cautious, run the jets while only plain water is in the tub. How can I get rid of the gunk in my whirlpool tub? |Jeanne Huber |April 12, 2021 |Washington Post 

Since THC is fat soluble, it stays in the system much longer than alcohol. The Truth About Driving While Stoned |Abby Haglage |June 12, 2014 |DAILY BEAST 

The pesticides and fungicides applied to grapevines are not water-soluble. A Wine Revolution |Sophie Menin |May 23, 2011 |DAILY BEAST 

Amorphous urates are readily soluble in caustic soda solutions. A Manual of Clinical Diagnosis |James Campbell Todd 

In its pure state it is a transparent and colourless gas, having a peculiar pungent smell, and highly soluble in water. Elements of Agricultural Chemistry |Thomas Anderson 

They exist in the soil in particular states of combination, in which they are scarcely soluble in water. Elements of Agricultural Chemistry |Thomas Anderson 

It is extremely soluble in water, and can be obtained in large transparent prismatic crystals, as in common sugar-candy. Elements of Agricultural Chemistry |Thomas Anderson 

It is soluble in alkalies, and precipitated from its solution by acids, and in all other respects agrees with vegetable caseine. Elements of Agricultural Chemistry |Thomas Anderson