Yet American minimalism, isolationism, realism, mind-our-own-business-ism—whatever you want to call it—is cyclical. Rand Paul vs. the Real World |James Kirchick |September 10, 2014 |DAILY BEAST 

As you know, I wrote in our/your anthology: “All stories are, in some way, war stories.” Colum McCann Talks New Novel ‘TransAtlantic’ and Narrative4 |Phil Klay |June 14, 2013 |DAILY BEAST 

My boyfriend and I were super-passionate about making out, but the losing-our-virginity thing was overwhelming. How 10 Porn Stars Lost Their Virginity |Aurora Snow |May 7, 2013 |DAILY BEAST 

Romney purports to like 30 Rock, but I don't believe anything released by staffs in these let's-humanize-our-guy press releases. Does Mitt Romney Know How to Laugh? |Michael Tomasky |May 21, 2012 |DAILY BEAST 

On the other hand, when it comes to the upper-crusty, too-rich-to-feel-our-pain issue, Romney seems to have the edge over Kerry. Mitt Romney: The GOP’s Own John Kerry, or Is He More an Al Gore? |Michelle Cottle |January 14, 2012 |DAILY BEAST 

Arm′our-bear′er; Arm′ourer, a maker or repairer of, or one who has the charge of, armour. Chambers's Twentieth Century Dictionary (part 1 of 4: A-D) |Various 

The piscina probably belonged to the chantry of Our-Lady-in-the-Lady-loft. Bell's Cathedrals: The Cathedral Church of Ripon |Cecil Walter Charles Hallett 

He generally talks like one of those continued-in-our-next yarns in the magazines. Shavings |Joseph C. Lincoln 

For understanding the Prophecies, we are, in the first place, to acquaint our-selves with the figurative language of the Prophets. Observations upon the Prophecies of Daniel, and the Apocalypse of St. John |Isaac Newton 

March-of-mind became to many almost as wearisome a cry as wisdom-of-our-ancestors had been. The Life of William Ewart Gladstone, Vol. 1 (of 3) |John Morley