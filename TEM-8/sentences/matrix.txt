The matrix of batteries and drivetrains also affects performance. Ford’s electric Mustang Mach-E is an important leap into the future |Dan Carney |February 12, 2021 |Popular-Science 

Neither de Blasio’s proposals nor the matrix would limit the NYPD’s current discretion over discipline. New York City Council Proposes Sweeping NYPD Reforms |by Eric Umansky and Joaquin Sapien |February 1, 2021 |ProPublica 

A much-touted guideline for penalties, known as a discipline matrix, was implemented just days ago by the NYPD. Still Can’t Breathe |by Topher Sanders, ProPublica, and Yoav Gonen, THE CITY, video by Lucas Waldron, ProPublica |January 21, 2021 |ProPublica 

Quanta also explored the power of representation theory, which shows the links connecting complicated objects called groups with the much simpler concept of matrices. The Year in Math and Computer Science |Bill Andrews |December 23, 2020 |Quanta Magazine 

Many matrices have a measurement called a determinant, which is a single value calculated using the numbers in the matrix. A Mathematician’s Unanticipated Journey Through the Physical World |Kevin Hartnett |December 16, 2020 |Quanta Magazine 

So we sneaked The Matrix and the movie they gave us after was 10 Things I Hate About You. Joseph Gordon-Levitt on ‘Sin City’ and Why He Considers Himself a Male Feminist |Marlow Stern |August 14, 2014 |DAILY BEAST 

There was the groundbreaking action/science-fiction franchise-maker, The Matrix. Complaining Like It’s 1999: ‘Fight Club,’ ‘American Beauty,’ and the Revolt of the Cubicle Drone |Arthur Chu |June 3, 2014 |DAILY BEAST 

OnTheFly (free) – This search engine app is based on the ITA Matrix software, which is own buy Google. The Best Travel Apps for Road Warriors |Matthew Kepnes |May 27, 2014 |DAILY BEAST 

This is an ultimatum seeking a target in the disorienting matrix of asymmetric warfare. The History and Logic of Military Ultimatums, From Suez to Crimea |Jacob Siegel |March 4, 2014 |DAILY BEAST 

Is this not astonishing enough for Kaku that he has to resort to Star Trek references and discussions of the Matrix? What Will Happen to Our Minds in the Future? |Robert Herritt |March 2, 2014 |DAILY BEAST 

The matrix or die is placed in a power press and the records pressed from the material used in making the sound records. The Wonder Book of Knowledge |Various 

A tombstone under the west window shows the matrix of what was once a magnificent brass. Bell's Cathedrals: The Cathedral Church of Carlisle |C. King Eley 

Then to finish all, Margaret wore in the lace at her throat, a great brooch of turquoise matrix, which matched her eyes. The Butterfly House |Mary E. Wilkins Freeman 

Every environment leaves the stamp of its matrix on the individual shaped in it. Sense from Thought Divide |Mark Irvin Clifton 

This formed the matrix into which the molten metal was poured to make the stereotype plate, or die, for printing. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine