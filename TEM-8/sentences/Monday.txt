Woldetensae’s status is unclear heading into Monday’s senior night game against the Hurricanes. Virginia has fizzled against top opponents, sowing doubts about a deep NCAA tournament run |Gene Wang |February 26, 2021 |Washington Post 

On Monday, Lukashenko met Putin again in Sochi, Russia, to ask him for another $3 billion, according to Russia’s Kommersant newspaper—a loan that could open the way for Putin to have a far greater hold over Belarus. How a Belarusian Teacher and Stay-at-Home Mom Came to Lead a National Revolt |Vivienne Walt |February 25, 2021 |Time 

Allen Chen, the engineer in charge of the landing system, hinted during a Monday news conference that video footage of Perseverance’s parachute deploying contained a hidden missive. NASA Hid an Inspiring Message on the Parachute of the Mars Rover Perseverance |Megan McCluskey |February 25, 2021 |Time 

On Monday, Republican state senators sent a letter to the presidents and chancellors of Tennessee’s public universities. In Tennessee, GOP senators seek to ban protests during anthem, raising legal concerns |Glynn A. Hill |February 25, 2021 |Washington Post 

The FDA, which on Monday issued new guidance to manufacturers on how to deal with variants, has emphasized being ready to update vaccines. FDA review confirms safety, efficacy of single-shot Johnson & Johnson coronavirus vaccine, especially against severe cases |Carolyn Y. Johnson, Laurie McGinley |February 24, 2021 |Washington Post 

But the program is just six weeks long, the Pentagon admitted Monday. Pentagon Insider on New Plan to Fight ISIS: ‘Of Course It’s Not Enough’ |Nancy A. Youssef |January 6, 2015 |DAILY BEAST 

As the controversy unfurled late Monday, it created some odd bedfellows. No. 3 Republican Admits Talking to White Supremacist Conference |Tim Mak |December 30, 2014 |DAILY BEAST 

On Monday, Soelistyo had jolted relatives as well as searchers by suggesting that the plane could be “at the bottom of the sea.” Wreckage, Bodies of AirAsia Crash Found |Lennox Samuels |December 30, 2014 |DAILY BEAST 

After two nights in detention, he was scheduled to be deported back to Turkey on Monday. Pope-Shooter Ali Agca’s Very Weird Vatican Visit |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

As the sun set on Monday and the search was called off for the day, there had been no positive update on the possible wreckage. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

Liszt has returned from his trip, and I have played to him twice this week, and am to go again on Monday. Music-Study in Germany |Amy Fay 

On Monday the clouds cleared away and the whole country was gloriously bright and fresh after the heavy showers. British Highways And Byways From A Motor Car |Thomas D. Murphy 

You met him at Biarritz six months ago, and on Monday last he lunched with you up at Monifieth. The Doctor of Pimlico |William Le Queux 

There was a rustle of expectancyupon the girls side, at leastat Assembly on Monday morning. The Girls of Central High on the Stage |Gertrude W. Morrison 

Get the gray colt and I'll give you a lift over the mountain, but I'll bring you back on Monday, too. The Soldier of the Valley |Nelson Lloyd