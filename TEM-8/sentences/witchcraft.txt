Check out the Technically Fiction series for the science on how to make witchcraft real and build a dragon. Let’s learn about the creatures of Halloween |Maria Temming |October 26, 2021 |Science News For Students 

Beyond that, though, the coverage of the virus has mostly been partisanship and witchcraft. Comparing the red-state pandemic response now to blue states in early 2020 is dishonest |Philip Bump |August 31, 2021 |Washington Post 

Ghostly, superimposed figures dance in circles in Lauren Woods’s piece, and dabble in what Alexander D’Agostino terms “witchcraft” in his. In the galleries: A focus on the intersection of art and movement |Mark Jenkins |February 26, 2021 |Washington Post 

I graduated from the University of Chicago with a degree in medieval studies, with a thesis on the late medieval witchcraft trials. How to Be Better at Death (Ep. 450) |Maria Konnikova |February 4, 2021 |Freakonomics 

O’Donnell told Bill Maher “I dabbled in witchcraft,” which spurred her to run a campaign ad announcing “I am not a witch,” which was parodied on “Saturday Night Live.” Lovely, little Delaware — long famous for corporations, chickens and credit cards — is ready for its big moment |Karen Heller |January 12, 2021 |Washington Post 

Being pro-life is all fun and games until you realize the movement would sooner endorse witchcraft over science, apparently. Rand Paul’s Plan B for Pro-Life Critics |Olivia Nuzzi |October 5, 2014 |DAILY BEAST 

Other uteri, tainted by low, regular doses of progesterone and/or witchcraft, become evil. Getting to Know the ‘Beyoncé Voter’ |Kelly Williams Brown |July 7, 2014 |DAILY BEAST 

An accusation of witchcraft is vague enough to serve as a kind of catchall for discontent. Will Saudi Arabia Execute Guest Workers for 'Witchcraft'? |Michael Schulson |March 29, 2014 |DAILY BEAST 

Indonesian guest workers are on trial in Saudi Arabia for “witchcraft.” Will Saudi Arabia Execute Guest Workers for 'Witchcraft'? |Michael Schulson |March 29, 2014 |DAILY BEAST 

In 2011, a special witchcraft-busting unit reported that it had handled nearly 600 claims in the preceding few years. Will Saudi Arabia Execute Guest Workers for 'Witchcraft'? |Michael Schulson |March 29, 2014 |DAILY BEAST 

They used to believe in witchcraft, and they burned millions—yes, millions—of innocent women as witches. God and my Neighbour |Robert Blatchford 

Many other of the famous inquirers in those years which ushered in modern science believed in witchcraft. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Wesley said if we gave up belief in witchcraft we must give up belief in the Bible. God and my Neighbour |Robert Blatchford 

English act of parliament against witchcraft, passed in the reign of James I, repealed. The Every Day Book of History and Chronology |Joel Munsell 

They believe in the transmigration of the soul into other men and into animals, and in demons, witchcraft and magic. The Wonder Book of Knowledge |Various