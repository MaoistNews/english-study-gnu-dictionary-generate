Potential solutions to the problem include building radiation shielding around quantum computers or locating them underground, where cosmic rays aren’t able to penetrate so easily. Could Quantum Computing Progress Be Halted by Background Radiation? |Edd Gent |August 31, 2020 |Singularity Hub 

It was founded by the Bentley brothers—Barry, Keith, Ray, and Greg—in 1984. Buy or pass? The pros and cons of investing in 9 upcoming tech IPOs from Palantir to Asana |Aaron Pressman |August 29, 2020 |Fortune 

As seniors, we finally got to take the computer graphics course we were so looking forward to, learning many amazing ways to visually express the world using code—for example, with ray tracing and particle simulation. Crafting our path |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

When they create lower-energy gamma rays, those gamma rays won’t be pointed at us. The Hidden Magnetic Universe Begins to Come Into View |Natalie Wolchover |July 2, 2020 |Quanta Magazine 

These particles then collide with other photons, turning them into low-energy gamma rays. The Hidden Magnetic Universe Begins to Come Into View |Natalie Wolchover |July 2, 2020 |Quanta Magazine 

"He brought Ray Charles to the mix as an influence on rock & roll," E Street Band guitarist Steven Van Zandt once raved. Joe Cocker's Deep Live Cuts |Asawin Suebsaeng |December 22, 2014 |DAILY BEAST 

An x-ray two hours later confirms my hunch: my tibia (the big bone behind the shin) is snapped clean in two. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

This is admittedly a loaded question, but do you feel James Earl Ray really killed Martin Luther King Jr.? Ava DuVernay on ‘Selma,’ the Racist Sony Emails, and Making Golden Globes History |Marlow Stern |December 15, 2014 |DAILY BEAST 

Thanks to CompStat and strategies added by Police Commissioner Ray Kelly, crime continued to decline. Eric Garner Was Just a Number to Them |Michael Daly |December 5, 2014 |DAILY BEAST 

I have been a Ray Harryhausen fan since I was six or seven years old. ‘No Regrets’: Peter Jackson Says Goodbye to Middle-Earth |Alex Suskind |December 4, 2014 |DAILY BEAST 

A ray of Consciousness is passed over that impression and you re-read it, you re-awaken the record. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Gordon turned his pleading eyes on his old friend without a ray of concession; but for a moment he hesitated. Confidence |Henry James 

Presently a ray of inspiration dispelled the cloud from the features of the battered man. The Joyous Adventures of Aristide Pujol |William J. Locke 

The landing was in darkness, but as Gunn spoke he directed the ray of a pocket lamp upon a bronze plate bearing the name "Kazmah." Dope |Sax Rohmer 

But the ray had shone upon the frosted glass long enough to enable Kerry to read the words painted there in square black letters. Dope |Sax Rohmer