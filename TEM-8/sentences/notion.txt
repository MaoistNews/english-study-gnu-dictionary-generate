The burrowing freeloaders “challenge our notion of what plants even do,” he says. ‘Vampire’ parasite challenges the definition of a plant |Susan Milius |September 16, 2020 |Science News For Students 

There’s another notion of recursion, which is you can reuse stuff. Talking Is Throwing Fictional Worlds at One Another - Issue 89: The Dark Side |Kevin Berger |September 9, 2020 |Nautilus 

The answer largely depends on how much we build into the notion of understanding. Welcome to the Next Level of Bullshit - Issue 89: The Dark Side |Raphaël Millière |September 9, 2020 |Nautilus 

The competitive advantage they once had when raising — the notion that they’re focused on an area no one else is — is potentially threatened. 3 views on the future of geographic-focused funds |Natasha Mascarenhas |September 4, 2020 |TechCrunch 

The notion that the state’s attorney’s office can’t simultaneously pursue public safety and a broader sense of justice is false, she said, and it would be a mistake to stop the reform efforts now. As Trump Calls for Law and Order, Can Chicago’s Top Prosecutor Beat the Charge That She’s Soft on Crime? |by Mick Dumke |September 4, 2020 |ProPublica 

But Brooke was out of step with the New Left and its notion of radical chic. Ed Brooke: The Senate's Civil Rights Pioneer and Prophet of a Post-Racial America |John Avlon |January 4, 2015 |DAILY BEAST 

As Puar further pointed out, this notion of a global gay identity is easily manipulated. How Canadian Oilmen Pinkwash the Keystone Pipeline |Jay Michaelson |December 28, 2014 |DAILY BEAST 

Could you talk a minute about the notion of being an unreliable narrator? Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

They embraced the notion of a growing America, whose economy could be expanded for the benefit of the majority. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

The biggest misfire here, though, was the notion that anyone would believe that this dude looked at all like Prince Harry. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

But the Mexican caballeros had no notion of coming up to the scratch a third time. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Although everybody laughed at such a notion, the Worm-eating Warbler declared that he had a right to his own belief. The Tale of Grandfather Mole |Arthur Scott Bailey 

But I couldn't get rid of the notion that he would hand me out the same dose he had given MacRae if only he had the power. Raw Gold |Bertrand W. Sinclair 

He had not the least idea what wadding was, and his notion of a bullet was a dockyard cannon-ball bigger than his own head. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Yet, try as I would to strangle the idea, all through the evening the same horrible, unaccountable notion clung to me. Uncanny Tales |Various