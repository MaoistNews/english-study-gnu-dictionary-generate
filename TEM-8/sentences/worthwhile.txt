What’s harder, and what would have made for a more worthwhile film, is to explain why such vapidity appeals to young people. What HBO’s Fake Famous Doesn’t Understand About Young People and Influencer Culture |Judy Berman |February 4, 2021 |Time 

I got to wondering whether the field of economics had anything worthwhile to say about disgust. The Downside of Disgust (Ep. 448) |Stephen J. Dubner |January 21, 2021 |Freakonomics 

Once you’ve sized up the risk to yourself and others and still think it’s worthwhile to submit potential evidence to the proper authorities, be strategic. How to save important photos and video from the web |John Kennedy |January 19, 2021 |Popular-Science 

Buccaneers Coach Bruce Arians also struggles to be a worthwhile betting interest. NFL picks against the spread for the conference championship games |Neil Greenberg |January 19, 2021 |Washington Post 

Every few years, it can be worthwhile to change or upgrade hosting to faster infrastructure. Core Web Vitals report: 28 Ways to supercharge your site |James Parsons |January 13, 2021 |Search Engine Watch 

Whatever else may have been on the viewing docket, this is more worthwhile. Hey Anti-Vaxxers, Watch NOVA: Vaccines--Calling the Shots |Russell Saunders |September 11, 2014 |DAILY BEAST 

The most worthwhile things in life rarely come easy, this is a lesson I've always known. Michael Sam, the First Openly Gay NFL Draftee, Has Earned the Right to Play Ball |Robert Silverman |August 31, 2014 |DAILY BEAST 

Dr. Saltz also believes confronting the uncomfortable realities of your past can be worthwhile. Should You Confront Your Old Bully? |Keli Goff |August 4, 2014 |DAILY BEAST 

Obama instead attended a pair of political fundraisers, though one was for Sen. Dick Durbin and was unquestionably worthwhile. Brooklyn Shooting Hits Close to Bill de Blasio’s Park Slope Home |Michael Daly |July 1, 2014 |DAILY BEAST 

The final requirement of a great director is that he has something worthwhile to say. ‘Jersey Boys’ Proves Clint Eastwood is Hollywood’s Most Overrated Director |Andrew Romano |June 20, 2014 |DAILY BEAST 

"They'll sell their lives for something worthwhile, tonight," said Holaf into my ear, as we set off on their trail. Valley of the Croen |Lee Tarbell 

Ann makes many new, worthwhile friends during her first year at Forest Hill College. Molly Brown's Orchard Home |Nell Speed 

Because more worthwhile information was gathered for some species than others, the length and completeness of the accounts vary. Amphibians and Reptiles of the Rainforests of Southern El Peten, Guatemala |William E. Duellman 

Somehow, since I've talked so much with Mr. McPhearson, learning things seems more worthwhile. Christopher and the Clockmakers |Sara Ware Bassett 

Always there was some really worthwhile present about which endless whispering and the greatest secrecy was maintained. Christopher and the Clockmakers |Sara Ware Bassett