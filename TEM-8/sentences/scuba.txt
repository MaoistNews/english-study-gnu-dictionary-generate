If you’re feeling especially adventurous, you can day trip in a rented 4×4 or ATV into the spectacular Þórsmörk Valley, then scuba dive with Arctic Adventures in Silfra, a glacier-fed fissure with some of the best underwater visibility in the world. The 25 Best Fall Trips in the World |jversteegh |August 9, 2021 |Outside Online 

Anyone planning to use a sea scooter to dive with oxygen should have proper training and scuba certification. Best underwater scooter for safe and fun aquatic adventures |Irena Collaku |July 21, 2021 |Popular-Science 

For years, my best friend, who loves the ocean and is a certified open water scuba diver, has been trying to convince me to go on a trip with her. How to Build the Perfect Adventure Bucket List |Maren Larsen |May 4, 2021 |Outside Online 

This was the 1950s, before scuba gear was commercially available. Protecting the world’s vanishing coral reefs |Ari Daniel, PhD ’08 |April 28, 2021 |MIT Technology Review 

If you tend to do more snorkeling than scuba diving, look for a fin with an adjustable heel and short blade. The best scuba and snorkel fins for divers of all abilities |PopSci Commerce Team |January 25, 2021 |Popular-Science 

The Oculus Rift is a wearable headset that goes over the eyes like a clunky pair of scuba goggles. Welcome to Oculus XXX: In-Your-Face 3D is the Future of Porn |Aurora Snow |October 18, 2014 |DAILY BEAST 

For me, as a scientist, it all began in 1953 when I first tried scuba. ‘Mission Blue’ Warning: The Ocean Is Not Too Big to Fail |Sylvia A. Earle |August 15, 2014 |DAILY BEAST 

Accompanied by a guide, viewers can snorkel or scuba dive to the sculptures for an up-close look. Artist Jason deCaires Taylor’s Underwater Sculptures Are a Sight to Sea |Justin Jones |April 7, 2014 |DAILY BEAST 

Scannon found his calling while on a scuba diving trip to Palau, a group of small islands near Japan. Will These Men Ever Come Home? The Search for Missing WWII Pilots in “Vanished” |Jordan Michael Smith |November 14, 2013 |DAILY BEAST 

If I were on a desert island I would love to have a scuba diving kit. Benedict Cumberbatch’s Revealing Reddit AMA: On Julian Assange’s Letter and Fame |Marlow Stern |October 11, 2013 |DAILY BEAST 

Watch the scuba diver hand feed giant turtles, sting rays and even the menacing moray eels in the Hawaiian Reef Tank. Oahu Traveler's guide |Bill Gleasner 

When he's wearing his scuba mask, he's practically a self-contained submarine. Anything You Can Do ... |Gordon Randall Garrett 

His Scuba was the type that combined the breathing apparatus with the full face plate. The Wailing Octopus |Harold Leland Goodwin 

When he's wearing his scuba apparatus, he's practically a self-contained submarine. Anything You Can Do |Gordon Randall Garrett 

The speedboat was there, and so was the Scuba cart, but the rowboat wasn't. The Electronic Mind Reader |John Blaine