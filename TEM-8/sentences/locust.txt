Scientists weren’t sure what coaxed solitary migratory locusts of another species, Locusta migratoria, to congregate. A single chemical may draw lonely locusts into a hungry swarm |Jonathan Lambert |September 7, 2020 |Science News For Students 

They started by identifying chemicals that gregarious locusts pump out. A single chemical may draw lonely locusts into a hungry swarm |Jonathan Lambert |September 7, 2020 |Science News For Students 

When solitary locusts get together, they do more than hang out. A single chemical may draw lonely locusts into a hungry swarm |Jonathan Lambert |September 7, 2020 |Science News For Students 

Traps laced with 4VA could concentrate locusts and make treatment with insecticides or pathogens much easier, especially if 4VA acts as an attractant in other species as well, like the desert locust. A single molecule may entice normally solitary locusts to form massive swarms |Jonathan Lambert |August 12, 2020 |Science News 

Alternatively, locusts genetically engineered to lack this 4VA detector, and thus less likely to swarm, could also be introduced into populations as a control measure. A single molecule may entice normally solitary locusts to form massive swarms |Jonathan Lambert |August 12, 2020 |Science News 

This spring, Israel found itself in the midst of a locust invasion. Cicadas, Grasshoppers, Locusts, Ants Among the Tastiest Insects |Nina Strochlic |May 14, 2013 |DAILY BEAST 

I long for the day I no longer have to think about this pestilential little locust. Ron Paul: Batty Old Reactionary for President |Michael Tomasky |December 14, 2011 |DAILY BEAST 

In refurbishing the park, the designers added 54 honey locust trees and one larger London Plane at the northeastern corner. A Tale of Two Trees |Michael Daly |November 5, 2011 |DAILY BEAST 

"No wonder things git pindlin' under this old locust-tree," Sophy heard him grumble. Country Neighbors |Alice Brown 

Something sang in the sunny air above my head, and I flicked with my whip to drive the locust away. The Relief of Mafeking |Filson Young 

Oak and walnut trees abound upon this creek, besides elm, ash, and locust. Early Western Travels 1748-1846, Volume XVI |Various 

It bears a pod similar to that of the locust, to which it is related, containing eight to twelve beans. Early Western Travels 1748-1846, Volume XVI |Various 

Say, that's why they call a policeman's club his locust, ain't it? The Banner Boy Scouts on a Tour |George A. Warren