Preferably, the place you’re renting has a one-two setup—an outdoor shower for the initial emergency removals, then indoors for more soapy fine-tuning. My 10 Best Post-Adventure Showers |aheard |July 26, 2021 |Outside Online 

Instead of an eye-opening docuseries or a tear-jerking drama, it’s a reality show in the glossy, soapy Bravo mold. Netflix’s My Unorthodox Life Is More Bravo Docusoap Than Real-Life Unorthodox |Judy Berman |July 14, 2021 |Time 

For most of that time, all we see is soapy water ebbing and flowing over the stone tiles. The Best International Movies on Netflix |Chris Grasinger |June 11, 2021 |Time 

I found a brush used for washing dishes with a handle that holds a soapy gel is the ideal cleaning tool for things like that, and it's not expensive. Hints From Heloise: Don’t be a no-show for coronavirus vaccine appointments |Heloise Heloise |May 7, 2021 |Washington Post 

A soapy glass slipping through your fingers produces a shearing force — and possibly a crash. Capturing the sense of touch could upgrade prosthetics and our digital lives |Kathiann Kowalski |April 22, 2021 |Science News 

I was totally resistant to it because I always thought that the minute that happened, it was likely to become soapy! Meet ‘Inspector Lewis’: Kevin Whately on ‘Morse,’ John Thaw, and the End of the Series |Jace Lacob |June 14, 2013 |DAILY BEAST 

The characters in Pitch Dark can seem plucked from this soapy version of reality. Renata Adler, Poet of a Chaotic Generation |Jen Vafidis |March 21, 2013 |DAILY BEAST 

There is a steady flow of bowls of warm water, soapy and clear, delivered by a stream of helpers. Inside a Home Funeral |Melissa Roberts Weidman |February 5, 2013 |DAILY BEAST 

I stroke his muscles with a soapy washcloth, picking out the embedded flecks of glass with my fingernails. Inside a Home Funeral |Melissa Roberts Weidman |February 5, 2013 |DAILY BEAST 

The story is more rich and complicated than the trailers give away without any soapy melodrama. TV Preview: Snap Judgments of 2012-13’s New Shows |Jace Lacob, Maria Elena Fernandez |June 12, 2012 |DAILY BEAST 

After sweeping it is good to wipe the carpet with a cloth wrung out of warm, soapy water in which is a little ammonia. The Library of Work and Play: Housekeeping |Elizabeth Hale Gilman 

Glycerine and soapy water, equal parts, may be introduced into the bowel for temporary relief of a persistent constipation. The Mother and Her Child |William S. Sadler 

Freydissa enforced her command by sending a mass of soapy cloth which she had just wrung out after the retreating Bertha. The Norsemen in the West |R.M. Ballantyne 

Mr. Brown scrubbed him vigorously, and Bobby splashed and swam and churned the soapy water to foam. Greyfriars Bobby |Eleanor Atkinson 

At the bottom of the cliff, a hundred metres below the road on which you ride, break the soapy waves of the sea. Italian Highways and Byways from a Motor Car |Francis Miltoun