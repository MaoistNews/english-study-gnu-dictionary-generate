Some thought if they discovered that recipe, they would be able to copy it and transmute any cell into an egg. How Silicon Valley hatched a plan to turn blood into human eggs |Antonio Regalado |October 28, 2021 |MIT Technology Review 

In this vision, the mill would transmute into something wholly different, just as uranium itself does, in a process that is the exact opposite of decay. Once the Uranium Capital of the World, Moab, Utah, wants to unload its radioactive legacy |Sarah Scoles |September 9, 2021 |Popular-Science 

Comedy, when done well, has the power to transmute our personal and collective pain. Padma Lakshmi says disparaging Indian food isn’t funny. It’s ugly. |Padma Lakshmi |August 25, 2021 |Washington Post 

It’s a cauldron of extremes, a turbulent soup of transmuting matter, where particles and forces often ignored in our everyday world become critical. Secret Ingredient Found to Power Supernovas |Thomas Lewton |January 21, 2021 |Quanta Magazine 

In her world, insight into nature starts on the scale of miniature and is transmuted to the monumental. Imitation Is the Sincerest Form of Environmentalism - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |October 7, 2020 |Nautilus