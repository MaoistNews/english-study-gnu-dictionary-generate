A perfect vaccine would create what is called “sterilizing” immunity, which means the virus can’t get a foothold in your body at all. So you got the vaccine. Can you still infect people? Pfizer is trying to find out. |Stephanie Arnett |February 2, 2021 |MIT Technology Review 

We didn’t recognize it until it already had its foothold because it was novel, and we weren’t looking for it. U.S. response to coronavirus variants emphasizes masks and vaccines instead of lockdowns |Fenit Nirappil, Brittany Shammas |February 2, 2021 |Washington Post 

Kirsten has done amazing work, helping to elevate our foothold in the transportation space and making our Mobility events some of the most successful we’ve produced. Welcome Tage Kene-Okafor, Mary Ann Azevedo, Sophie Burkholder and a guy named Drew |Matthew Panzarino |February 2, 2021 |TechCrunch 

Many of the attacks gained initial footholds by password spraying to compromise individual email accounts at targeted organizations. 30% of “SolarWinds hack” victims didn’t actually use SolarWinds |Jim Salter |January 29, 2021 |Ars Technica 

Apple started making iPhones in India in 2017 to comply with local sourcing rules and to establish a bigger foothold in the market. Why workers in India attacked an iPhone factory |Naomi Xu Elegant |December 14, 2020 |Fortune 

The area is 98 percent white, and the Klan has a strong foothold even to this very day. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 

For decades, the Chinese government has had a foothold on the African continent. 'Made in China' Now Being Made in Africa |Brendon Hong |August 23, 2014 |DAILY BEAST 

[Laughs] But, yes, I suppose Scientology has more of a foothold there than it does here. Paul Haggis on Scientology, the ‘Crash’ Oscar, and ‘Third Person’ |Kevin Fallon |June 19, 2014 |DAILY BEAST 

Every time one fails to find a firm foothold, there is a danger of falling, sometimes to alarming depths. Exploring the Amazon, While We Still Can |Darrell Hartman |May 15, 2014 |DAILY BEAST 

It would allow the old way of doing things to regain a foothold in this young century. President Obama’s Belgian Waffle |Stuart Stevens |March 27, 2014 |DAILY BEAST 

Likewise your Majesty will have shelter for his vessels, and a foothold in that country, which abounds with meat and rice. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

On these shallows water-loving plants and even certain trees, such as the willows and poplars, find a foothold. Outlines of the Earth's History |Nathaniel Southgate Shaler 

He liked money, and what it would bring him, and if he had been sure of his foothold he would have been very happy. The Cromptons |Mary J. Holmes 

They clung to it here and there with their hands while they felt for a foothold among the banks of gravel. The Gold Trail |Harold Bindloss 

I leaped and caught the bottom rung of a fire escape, pulled myself up until I could get a foothold. Hooded Detective, Volume III No. 2, January, 1942 |Various