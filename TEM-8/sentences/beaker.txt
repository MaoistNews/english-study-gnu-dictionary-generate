Tucked away in an inconspicuous corner is a typical lab bench—a table with flasks, scales, and beakers beneath a fume hood—where graduate students can practice chemistry in much the same way their grandparents’ generation did. This chemist is reimagining the discovery of materials using AI and automation |Simon Lewsen |October 27, 2021 |MIT Technology Review 

He knows this firsthand, as he will occasionally provoke defensive responses with his own fingers instead of the dead mice or membrane-covered beakers he and his students use to collect venom. Everything you need to know about scorpion stings and venom |John Kennedy |July 22, 2021 |Popular-Science 

To test this, her team added microplastics to beakers containing amphipods. Polluting microplastics harm both animals and ecosystems |Alison Pearce Stevens |November 3, 2020 |Science News For Students 

As if abstractedly, he now took up the beaker, pledged madame with his glance, and drank. St. Martin's Summer |Rafael Sabatini 

But—my cigar has reached its last dying speech, and there is but a drop left in the beaker. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The king to the brim filled a beaker with wine: “I beg of thee drink to me, dear sister mine!” The Tale of Brynild, and King Valdemar and his Sister |Anonymous 

The string was then fastened around the beaker as shown, and the whole suspended from a shelf. The Boy Mechanic, Book 2 |Various 

The boy took the beaker, but being openly on bad terms with the elves, argued no good to himself from such an offering. The Science of Fairy Tales |Edwin Sidney Hartland