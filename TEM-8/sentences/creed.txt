First, they must become a fully national party where all races, creeds, genders, and preferences are equally welcome and all the major groups can have visible members in prominent positions. Time for GOP to open up to LGBTQ Americans |James Driscoll |January 28, 2021 |Washington Blade 

Sanford was the first Southern governor to call for employment without regard to race or creed. How a courageous Southern governor broke ranks with segregationists in 1961 |John Drescher |January 1, 2021 |Washington Post 

The principle that “no one is above the law” is supposed to be a foundational democratic creed, but political leaders, at least in recent decades, have rarely been held accountable for their malfeasance. Vice President Agnew’s misdeeds, and the challenges of holding him accountable |Matthew Dallek |December 18, 2020 |Washington Post 

No matter whether someone was a Democrat or a Republican, the creed was capitalism. There is no such thing as ‘apolitical culture’ |rhhackettfortune |September 30, 2020 |Fortune 

Regardless of race or creed—in many cases, regardless of highly limited capital—these entrepreneurs were able to buy a McDonald’s franchise and reap the rewards. Why McDonald’s sets the standard for equitable business models |jakemeth |September 16, 2020 |Fortune 

One person who tuned into my story was Matt Creed, the director of Lily. Blogger Shares and Shames Cancer in ‘Lily’ |Amy Grantham |December 9, 2014 |DAILY BEAST 

“But no way are we playing Creed, man,” says one of his friends. Creed Singer Scott Stapp’s Fall From Grace: From 40 Million Albums Sold to Living in a Holiday Inn |Marlow Stern |November 27, 2014 |DAILY BEAST 

And the black hejab was imposed on all non-Saudi women, regardless of their religion or creed. Saudi Activist Manal Al-Sharif on Why She Removed the Veil |Manal Al Sharif, Advancing Human Rights |October 30, 2014 |DAILY BEAST 

Well talking about big movies, you've been cast to play Apollo Creed's grandson in a new Rocky franchise installment. Michael B. Jordan: Playing a Black Superhero in 'Fantastic Four' Is a 'Huge Responsibility' |Kevin Fallon |September 28, 2014 |DAILY BEAST 

With every race and creed of the the world paddling on a lake in Queens, I was eager to see how a team can win or lose. Bros Love Dragon Boats |Daniel Genis |August 10, 2014 |DAILY BEAST 

The new creed, called the King's Book, approved by the houses of convocation, and made the standard of English orthodoxy. The Every Day Book of History and Chronology |Joel Munsell 

But the reading was, just then, a much more serious matter than any creed. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

For it must always be remembered in fairness that the creed of violence has no necessary connection with socialism. The Unsolved Riddle of Social Justice |Stephen Leacock 

She moved slightly, like a dreamer in pain, as again she faced the creed she had hated through many a sleepless night. Uncanny Tales |Various 

In his simple creed if a girl accepted a man and let him kiss her and wore his ring it was a reciprocal love affair. The Amazing Interlude |Mary Roberts Rinehart