It turns out there are a number of ways of divvying up the blame. What’s causing climate change, in 10 charts |David Roberts |September 11, 2020 |Vox 

It’s easy to throw blame at university leaders and students for the chaos that schools have already experienced this year. Donald Trump has failed to protect me and other students from COVID-19 |jakemeth |September 3, 2020 |Fortune 

A law firm preparing the city’s defense has put the blame on environmental contractors. How the City Came to Lease a Lemon |Lisa Halverstadt and Jesse Marx |August 10, 2020 |Voice of San Diego 

The Kenyan government also shared blame for being in breach of both environmental and human rights laws within the Kenyan constitution. How a grassroots campaign won $12 million taking on a lead-poisoning battery plant in Kenya |Soila Kenya |July 30, 2020 |Quartz 

This suggests that at least some voters are open to an argument that gives China a significant share of the blame for the pandemic. Why Blaming China For The Pandemic Probably Won’t Help Trump Win |Likhitha Butchireddygari (likhitha.butchireddygari@disney.com) |July 28, 2020 |FiveThirtyEight 

If so, he has his silence -- on top of poor judgment -- to blame. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

And who can blame them for feeling disenfranchised when they see their efforts dwarfed by the mega donors. The 100 Rich People Who Run America |Mark McKinnon |January 5, 2015 |DAILY BEAST 

And in so many of these events, the pattern of “blame the victim” was quickly in evidence. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

“Most of the diseases we blame on nutrition are actually diseases of disempowerment,” Bacon said. Why Your New Year’s Diet Will Fail |Carrie Arnold |December 30, 2014 |DAILY BEAST 

Authorities blame anarchists protesting a proposed high-speed rail line called TAV that will link Turin and Lyon, France. Italy’s Terror on the Tracks |Barbie Latza Nadeau |December 28, 2014 |DAILY BEAST 

Tell Lady Maude the candid truth, and take shame and blame to yourself, as you deserve. Elster's Folly |Mrs. Henry Wood 

And he was inclined to believe that it was Grandfather Mole that was to blame for the scarcity of worms in the neighborhood. The Tale of Grandfather Mole |Arthur Scott Bailey 

If the journey is now distasteful to her, she has but her own rashness to blame in having sought it herself. St. Martin's Summer |Rafael Sabatini 

My own yielding folly alone is to blame, and I shall take shame to myself for ever. Elster's Folly |Mrs. Henry Wood 

I don't blame him for killin' the cuss, not a bit; I'd have shot any man livin' that 'ad taken a good horse o' mine up that trail. Ramona |Helen Hunt Jackson