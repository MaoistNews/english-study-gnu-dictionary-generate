Three zippered exterior pockets hold sunscreen, car keys, and a phone, and a brimmed hood offers extra protection against harsh sun and gales. The Best Jackets of 2022 |jversteegh |October 26, 2021 |Outside Online 

He and Gale allegedly earned up to $23 million that they sought to conceal from authorities. John McAfee, software entrepreneur with outlaw persona, dies in prison at 75 |Glenn Rifkin |June 23, 2021 |Washington Post 

That grants Arctic weather, along with gales spinning off the Great Lakes, unimpeded access to the West Virginia highlands. Skiing West Virginia’s ‘Canadian Valley’ in a banner season |John Briley |February 18, 2021 |Washington Post 

Specifically, the party failed to submit affidavits in person signed by Scroggin and Gale, according to the court. Pennsylvania Supreme Court strikes Green Party presidential ticket from ballot, clearing the way for mail ballots to be sent out |Amy Gardner |September 17, 2020 |Washington Post 

It struck me from behind and carried me willy-nilly and with great force like a leaf in a gale. Fire on the Bay: 115 Years Ago This Month, a Deadly Explosion Rocked a Navy Ship |Randy Dotinga |July 14, 2020 |Voice of San Diego 

Movie Gale fails to conjure emotions more complicated than “oooh, what pretty eyes he has.” Team Peeta or Team Gale: Why the ‘Hunger Games’ Love Triangle Ruins ‘Mockingjay – Part 1’ |Kevin Fallon |November 28, 2014 |DAILY BEAST 

Unfortunately, Peeta Mellark and Gale Hawthorne are not among them. Team Peeta or Team Gale: Why the ‘Hunger Games’ Love Triangle Ruins ‘Mockingjay – Part 1’ |Kevin Fallon |November 28, 2014 |DAILY BEAST 

Comedian Paul Gale has an answer as to why: they do it on purpose. Viral Video of the Day: The Truth About Why Starbucks Employees Misspell Your Name (NSFW) |Alex Chancey |September 10, 2014 |DAILY BEAST 

We scrambled for the prime spots… bottom bunks on the wall, just close enough to the gale-forced air-conditioning ducts. How I’ll End the War: The Trip Over to Afghanistan |Nick Willard |April 23, 2014 |DAILY BEAST 

One of the scenes was when she first gets called to go into The Hunger Games and has to say goodbye to her mother and Gale. Shailene Woodley Opens Up About Coming of Age, ‘Divergent,’ and the Faults in Our World |Marlow Stern |January 22, 2014 |DAILY BEAST 

The gale still lasted, and the steamer was in momentary danger of becoming a complete wreck. Uncanny Tales |Various 

At a quarter before seven o'clock we hauled to the wind for the night with a fresh gale from the southward. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Loud and clear were both the signals, but four and a half miles of distance and a fresh gale neutralised their influence. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

It is not an easy matter to sit up in a gale of wind, with freezing spray, and sometimes green seas, sweeping over one! The Floating Light of the Goodwin Sands |R.M. Ballantyne 

A violent gale of wind from the south-west; the only thing like a hard gale since we left England. Journal of a Voyage to Brazil |Maria Graham