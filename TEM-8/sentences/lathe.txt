Creative writing in Room 205 hones my literary skills like a lathe. My Teacher Who Brought Magic to Room 205 |Susan Jane Gilman |July 20, 2009 |DAILY BEAST 

On the opposite side of the alley, sparks leap out of a lathe machine as a young man wearing protective goggles sharpens metal. Shooting Slumdog on the Gritty Streets of Mumbai |Amitava Kumar |November 24, 2008 |DAILY BEAST 

There was a lathe near the spot where David Arden stood, and shavings and splinters under his feet. Checkmate |Joseph Sheridan Le Fanu 

If an emery wheel mounted in a polishing head or lathe is at hand, this can be easily accomplished. The Boy Mechanic, Book 2 |Various 

The owner of a wood or metal lathe can easily construct a tool that will turn dowels of any size quickly. The Boy Mechanic, Book 2 |Various 

In the absence of a full equipment of lathe dogs the amateur can make them cheaply from pieces of iron pipe. The Boy Mechanic, Book 2 |Various 

The sander is easily placed in the lathe centers when needed, and the sandpaper may be replaced at any time. The Boy Mechanic, Book 2 |Various