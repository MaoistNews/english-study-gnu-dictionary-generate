So don’t expect him to spout some hackneyed nostrums for success to explain an elixir-producing expedition that would make his Viking ancestors proud. When Dead Beer Walks: A Dane in Vietnam Turns Strange Brew Into Craft Gin |Eugene Robinson |February 12, 2021 |Ozy 

He explained that the memo with the standard was written on “deadline” and that critics “rightly saw this as a threat to our journalism,” said sources present at the meeting. New York Times bails on intent-neutral standard when discussing epithets |Erik Wemple |February 11, 2021 |Washington Post 

In a viral TikTok video, the Louisiana native explained that the adhesive in her hair wouldn’t budge after fifteen washes. Tessica Brown Successfully Removes Gorilla Glue Via Surgery |Jasmine Grant |February 11, 2021 |Essence.com 

“Re-creating a scent is never enough,” said Dutch scent historian Caro Verbeek, explaining that together, storytelling and aroma can merge into a singularly powerful experience. Aromas can evoke beloved journeys — or voyages not yet taken |Jen Rose Smith |February 11, 2021 |Washington Post 

Police explained Wednesday morning that their investigation could take several weeks as detective dissect the crash scene, interview witnesses and await laboratory results. As police continue probe of Britt Reid crash, 5-year-old remains in critical condition |Rick Maese |February 9, 2021 |Washington Post 

A grand juror in the Ferguson case is suing to be able to explain exactly what went down in the courtroom. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

I asked her to explain more about these “private customers.” Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

After the curtain calls, Christopher comes back to explain a complicated math problem. Hedwig, Hugh & Michael Cera: 12 Powerhouse Theater Performances of 2014 |Janice Kaplan |December 31, 2014 |DAILY BEAST 

The problem is, how do you find a movie narrative that can explain genius, British or otherwise? Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

Nothing can be explained without the language to explain it. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

We stood staring after the fugitives in perfect bewilderment, totally unable to explain their apparently causeless panic. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

How, then, are we to explain this extraordinary discrepancy between human power and resulting human happiness? The Unsolved Riddle of Social Justice |Stephen Leacock 

The pulse in Louis's temples beat hard; yet he was determined not to anticipate, but make Wharton explain himself. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Would it not be more advisable to write to the London house itself, and explain the object of his coming up? Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Father Fochel of the Cathedral had attempted to explain it; but he had not done so to her satisfaction. The Awakening and Selected Short Stories |Kate Chopin