State officials said the findings justified their contention that the $200 million was not missing, but that it simply had not yet been collected. Utility Companies Owe Millions to This State Regulatory Agency. The Problem? The Agency Can’t Track What It’s Owed. |by Scott Morris, Bay City News Foundation |February 10, 2021 |ProPublica 

Fail to get at least one point in these matchups and you can fall out of contention very easily. ‘Loser points’ could produce some unlikely winners in 2021 NHL season |Neil Greenberg |February 5, 2021 |Washington Post 

Despite dips in scoring average and shooting efficiency, Green has continued easing and directing the workload of his teammates, filling in crucial gaps as Golden State balances contention with player development. Draymond Green Isn’t Scoring, But He’s Doing Everything Else For The Warriors |James L. Jackson |February 4, 2021 |FiveThirtyEight 

For now, there’s really no telling whether legislators will come to together on preemption and the right to legal action, or whether they’ll remain points of contention blocking federal privacy law. Cheat sheet: What to expect in state and federal privacy regulation in 2021 |Kate Kaye |February 1, 2021 |Digiday 

It’s not even the duo of Hailey Van Lith and Olivia Cochran of Louisville, a pair of newcomers both north of two win shares already, helping Jeff Walz’s Cardinals to title contention once more alongside player of the year candidate Dana Evans. Women’s Basketball Is Awash With Standout Freshmen |Howard Megdal |January 14, 2021 |FiveThirtyEight 

But that was so yesterday, much like his contention that he lacked the power to unilaterally confer amnesty. With Immigration Move, Obama and the Welfare Party Strike Again |Lloyd Green |November 24, 2014 |DAILY BEAST 

That last statistic, however, is a point of hot contention between both sides of the battle for Tony. Should Tony the Truck Stop Tiger Go Free? |Melissa Leon |June 28, 2014 |DAILY BEAST 

Rather, she alleges that Madaleno has made it a point of heated contention throughout the campaign. It’s Trans vs. Gay in This Maryland Election |Gideon Resnick |June 24, 2014 |DAILY BEAST 

Koch had girded himself for at least a year of contention before his research would be accepted. When TB Was a Death Sentence: An Excerpt From ‘The Remedy’ |Thomas Goetz |April 16, 2014 |DAILY BEAST 

So even back then, this was an obvious point of concern and contention. Transcript: Thomas Friedman Interviews Hillary Clinton and Christine Lagarde | |April 5, 2014 |DAILY BEAST 

He has felt strongly, and he was feeling strongly now; he was feeling passionately—that was my whole contention. Confidence |Henry James 

I knew not that contention could be rendered so sweet and pleasurable a thing to the nerves as I then felt it. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Dorothy had tried her best to relieve Letty of half her burthen, and in return had been made a bone of contention between them. The World Before Them |Susanna Moodie 

Woe is me, my mother: why hast thou borne me a man of strife, a man of contention to all the earth? The Bible, Douay-Rheims Version |Various 

Whether a description is clear, exact and sufficient is a question for the jury whenever it is a matter of legal contention. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles