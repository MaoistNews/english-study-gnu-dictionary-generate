In 2018, the state passed a law dedicating $1 billion over five years to wildfire prevention. Suppressing fires has failed. Here’s what California needs to do instead. |James Temple |September 17, 2020 |MIT Technology Review 

Social justice and progress are the absolute guarantors of riot prevention. Kenosha’s looting is a symptom of a decrepit democracy |Aaron Ross Coleman |September 4, 2020 |Vox 

Stop the smells before they startYour grandmother may have covered this one when you were young, but in case you need a reminder, keep in mind that an ounce of prevention is worth a pound of cure. Working out at home? Here’s how to keep your house from smelling like a gym. |Harry Guinness |September 3, 2020 |Popular-Science 

Above all, workers shouldn’t put themselves in the direct line of danger, according to the federal agency that usually focuses on disease prevention. Slapping, choking, kicking: The CDC has some interesting workplace warnings for dealing with angry customers |Lee Clifford |August 25, 2020 |Fortune 

According to one recent contract I obtained, customers must “use the system only for the detection, prevention, and investigation of crimes and terrorism and ensure the system will not be used for human rights violations.” Inside NSO, Israel’s billion-dollar spyware giant |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Once, when occupying a cell in near a phone, I saw the suicide prevention protocols in action. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

In 2005, the company got a call from a new, unexpected client: The Centers for Disease Control and Prevention. The American Ebola Rescue Plan Hinges on One Company. Meet Phoenix. |Abby Haglage |November 22, 2014 |DAILY BEAST 

As the Centers for Disease Control and Prevention has repeatedly noted, Ebola is not transferred through air. The Sham, Scaremongering Guide to Ebola |Abby Haglage |November 20, 2014 |DAILY BEAST 

Since the 10th century, Bulgaria has practiced varying forms of prevention to keep vampires from coming back to life. Bulgaria’s Vampire Graveyards |Nina Strochlic |October 15, 2014 |DAILY BEAST 

The NFL is now requiring violence prevention education and training for all players and staff. How Your Company Can End Violence Against Girls |Gary Cohen |October 9, 2014 |DAILY BEAST 

The prevention of this evil, therefore, was an object which a reformed house of commons was especially bound to secure. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Hence the only safe, as well as the only advantageous way out of this confusion is to go forward on the Principle of Prevention. English Poor Law Policy |Sidney Webb 

The important practical question is the prevention of the fulfilment of the morbid impulse during these impressionable years. Essays In Pastoral Medicine |Austin Malley 

Prevention consists in cutting the roots; not feeding them when the animals are very hungry, and not disturbing them while eating. Domestic Animals |Richard L. Allen 

Thus was the metropolis happily preserved; but the bloody part of the intended tragedy was past prevention. Fox's Book of Martyrs |John Foxe