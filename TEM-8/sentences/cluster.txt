According to health authorities, workplaces were the source for a quarter of the infection clusters reported since the lifting of heavy lockdown. As COVID-19 rates rise, France will require most people to wear masks at work |David Meyer |August 18, 2020 |Fortune 

That’s when teams in Germany and Austria each used Gaia to show many stars already had left the cluster. Milky Way’s tidal forces are shredding a nearby star cluster |Ken Croswell |August 18, 2020 |Science News For Students 

Where a cluster may hold a few thousand stars, a galaxy can be home to far more. Explainer: Stars and their families |Ken Croswell |August 18, 2020 |Science News For Students 

Last year, astronomers finally managed to examine a far sparser region of space—the expanse between galaxy clusters. This Week’s Awesome Tech Stories From Around the Web (Through July 11) |Singularity Hub Staff |July 11, 2020 |Singularity Hub 

Last year, astronomers finally managed to examine a far sparser region of space — the expanse between galaxy clusters. The Hidden Magnetic Universe Begins to Come Into View |Natalie Wolchover |July 2, 2020 |Quanta Magazine 

To his credit, Huckabee is conscious of the fact that he will need a cluster of deep-pocketed patrons and bundlers. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

Art Basel itself is but the center of a cluster of mega-shows taking place in Miami this week. Sneer and Clothing in Miami: Inside The $3 Billion Woodstock of Contemporary Art |Jay Michaelson |December 6, 2014 |DAILY BEAST 

But the runaway best example of the game is another cluster of British luxury vehicles—Range Rover, Land Rover and Jaguar. Nationalism on Four Wheels |Clive Irving |October 18, 2014 |DAILY BEAST 

But the crown itself (or headband or cluster of buds) screams disingenuous. Flower Crowns Are Phony and Must Die |Sara Lieberman |September 5, 2014 |DAILY BEAST 

There is now a black dot in a cluster of reeds about two hundred meters downstream. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

With the tertian parasite, the segments more frequently form an irregular cluster. A Manual of Clinical Diagnosis |James Campbell Todd 

In the course of an hour a cluster of dark objects appeared on the bench, coming rapidly toward us. Raw Gold |Bertrand W. Sinclair 

Behind it was a cluster of low hills set with narrow fields and tiny white houses. Ancestors |Gertrude Atherton 

They made a cluster of flashing color whose center point was a tiny airship, a speedster, a gay little craft. Astounding Stories, May, 1931 |Various 

He kept looking at the open sky above the river, waiting for the cluster to rise high so he could see it. The Stars, My Brothers |Edmond Hamilton