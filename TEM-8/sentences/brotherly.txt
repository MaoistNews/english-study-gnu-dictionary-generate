The protesters disagreed with the Kremlin’s reading of Ukrainian history, viewing the Soviet era as more of a hostile occupation than a cohesive union between brotherly states. How Putin's Denial of Ukraine's Statehood Rewrites History |Billy Perrigo |February 22, 2022 |Time 

It oscillates between matter-of-fact narration and shots of the men’s playful, brotherly dynamic, like when Antin spills a liter of coffee in Chambers’s front seat and pretends it’s water. ‘Beat Monday’ Rethinks What You Can Fit into a Weekend |Anna Callaghan |May 20, 2021 |Outside Online 

But brotherly love among enemy combatants was not something that the high command encouraged. Royals Remember The Christmas Truce of 1914 |Tom Sykes |December 12, 2014 |DAILY BEAST 

Bill Cosby was born July 12, 1937, in Philadelphia, the City of Brotherly Love. I Warned You About Bill Cosby in 2007 |Mark Ebner |November 20, 2014 |DAILY BEAST 

In a morning filled with brotherly love and jocularity over how everyone was getting along so well, no one wanted to offend. At American Enterprise Institute, NeoCons Say ‘Hello, Dalai’ |Eleanor Clift |February 21, 2014 |DAILY BEAST 

The City of Brotherly Love is closer to earning its moniker. The Year in Murder: 2013 Marks a Historic Low for Many Cities |Brandy Zadrozny |January 1, 2014 |DAILY BEAST 

One might question whether the bakers are acting in true and brotherly solidarity. The Twinkie is Dead! Long Live the Twinkie! |Megan McArdle |January 29, 2013 |DAILY BEAST 

For if there is one God, there is one brotherhood, and all humanity can only be linked to that God by brotherly love. Ways of War and Peace |Delia Austrian 

I made thy fortune along with my own in the days when we shared all things in brotherly community. The Nabob |Alphonse Daudet 

We spent the night in equally brotherly fashion, our beds being spread out for us side by side on the floor of the room. The Cradle of Mankind |W.A. Wigram 

Then I learned to know that which I felt was not a mere brotherly affection, but a deep love. The Cornet of Horse |G. A. Henty 

There were messages of brotherly interest and solicitude just as in the old days. Old-Time Makers of Medicine |James J. Walsh