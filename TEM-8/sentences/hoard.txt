We cannot be confident that Amazon will treat this new data any better than it has treated its existing data hoard. Amazon's Dangerous Ambition to Dominate Healthcare |Johnny Ryan |July 28, 2022 |Time 

In the 1940s, geographer and anthropologist George Carter amassed a hoard of Indigenous goods farmed in the Southwest, which he later donated to the University of California, Berkeley. Indigenous farmers are ‘rematriating’ centuries-old seeds to plant a movement |Kalen Goodluck |February 10, 2022 |Popular-Science 

In addition to pure speed, RSC will give Meta the ability to train algorithms on its massive hoard of user data. Meta Is Making a Monster AI Supercomputer for the Metaverse |Jason Dorrier |January 26, 2022 |Singularity Hub 

Scientists are excited to mine this data hoard to further refine the universe’s origin story. How Will the Universe End? Scientists Seek an Answer in the Biggest Galaxy Map Yet |Jason Dorrier |January 16, 2022 |Singularity Hub 

Slowly, they removed soil, pebbles, and fist-sized stones a fraction of an inch at a time, revealing the outline of the hoard. How Scotland forged a rare alliance between amateur treasure hunters and archaeologists |Corinne Iozzio |August 24, 2021 |Popular-Science 

That I hoard medication and go to sleep each night on a big pile of Zithromax? Doctors Can’t Prescribe a Magical Cure for Everything |Russell Saunders |March 26, 2014 |DAILY BEAST 

JML: Attractive celebrities are usually followed by a hoard of women. Murder, Sex, and the Writing Life: Norman Mailer’s Biography |Ronald K. Fried |November 19, 2013 |DAILY BEAST 

And by the end of the war he had managed to hoard at least 1,400 hugely valuable works for himself. The Man Who Hoarded Art for the Nazis |Christopher Dickey, Nadette De Visser |November 5, 2013 |DAILY BEAST 

Apple, “a pioneer in tactics to avoid taxes,” has kept much of its cash hoard abroad, out of reach of Uncle Sam. America’s New Oligarchs—Fwd.us and Silicon Valley’s Shady 1 Percenters |Joel Kotkin |May 14, 2013 |DAILY BEAST 

When would you rather hoard money instead of lending it out? Stop Paying Banks to Hoard |Noah Kristula-Green |July 31, 2012 |DAILY BEAST 

That hoard up silver and gold, wherein men trust, and there is no end of their getting? The Bible, Douay-Rheims Version |Various 

If I take no money for the money that I lend, how shall I then increase my hoard? German Culture Past and Present |Ernest Belfort Bax 

They are no wiser than the savages, who hide and hoard their little heaps of cowrie-shells. American Sketches |Charles Whibley 

What you enjoy is yours; what for your heirs / 45 You hoard, already is not yours, but theirs. Dictionary of Quotations from Ancient and Modern, English and Foreign Sources |James Wood 

Gunnar refuses Atli's command to reveal the hiding-place of the hoard, bidding them bring to him the heart of Hogni. The Nibelungenlied |Unknown