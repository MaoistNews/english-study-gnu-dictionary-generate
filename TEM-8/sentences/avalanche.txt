Now I take 45 minutes for coffee and a little breakfast, check the avalanche report, and I’m out the door. The Definitive Guide to Dawn Patrol |Outside Editors |October 16, 2020 |Outside Online 

No one wants to be caught under a snack or linen avalanche every time they open their pantry. The best bins and baskets to keep your pantry perfectly organized |PopSci Commerce Team |October 13, 2020 |Popular-Science 

These days, when something important or “newsy” happens, there’s an avalanche of content online that overwhelms people and leaves them less certain of what’s happening than before. Our misinformation problem is about to get much, much worse |Sean Illing |October 6, 2020 |Vox 

After the Thomas Fire burned across hills around the town, rain led to a kind of muddy avalanche so powerful it carried entire boulders. California wildfires may give way to massive mudslides |Ula Chrobak |September 17, 2020 |Popular-Science 

After staff and guests are cut off from all access to the outside world by a devastating avalanche, resentments are laid bare as the corporate food chain unravels and office politics take a deadly turn. 9 new books to read in September |Rachel King |September 1, 2020 |Fortune 

And Duke was a closet Nazi getting exposed by an avalanche of reporting. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

Horst Ulrich, a 72-year-old German on a trek with a group of friends, watched four Nepali guides swept away by an avalanche. Nepal’s Deadliest Avalanche Was Totally Avoidable |Dick Dorworth |October 20, 2014 |DAILY BEAST 

It was an avalanche in lower Manhattan, reaching 2.4 on the Richter scale. The Resilient City: New York After 9/11 |John Avlon |September 11, 2014 |DAILY BEAST 

And after enough snowflakes of conflict comes the avalanche. Is Democracy Doomed Abroad? |James Poulos |August 31, 2014 |DAILY BEAST 

Many of the other Nepali Sherpas working on the mountain witnessed the avalanche as it covered their friends and fellow workers. Everest's Sherpas Are Right To Revolt |Dick Dorworth |April 22, 2014 |DAILY BEAST 

However that may be, they were overtaken by an avalanche, the mother was buried beneath it, and the child saw her no more. Stories about Animals: with Pictures to Match |Francis C. Woodworth 

When we cut out the foundation—they're afraid that the vibration will loosen the rest and start an avalanche. The White Desert |Courtney Ryley Cooper 

Sheppy was coming around the corner of the granary in his most sedate manner, when the pop-eyed avalanche almost stepped on him. The Red Cow and Her Friends |Peter McArthur 

One man of our acquaintance was caught by a descending avalanche and swept down the hill by the moving mass. The Cradle of Mankind |W.A. Wigram 

At one particular place an enormous avalanche is an annual event, owing to the peculiar configuration of the gorges. The Cradle of Mankind |W.A. Wigram