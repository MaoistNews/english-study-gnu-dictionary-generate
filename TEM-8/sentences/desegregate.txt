By then, he’d had a long career as a prominent civil liberties lawyer working to desegregate public schools throughout the country. Judge David Tatel’s lack of eyesight never defined him, but his blindness is woven into the culture of the influential appeals court in D.C. |Ann Marimow |July 8, 2021 |Washington Post 

Elder’s story is hailed as one that helped desegregate the game. Lee Elder made history at the Masters. He made an impact in Washington. |Barry Svrluga |April 8, 2021 |Washington Post 

I would say I want a completely desegregated New York City public school system. The Educator Fighting to Rectify the Achievement Gap |Esabelle Lee |February 6, 2021 |Ozy 

It was one of many tactics the union used to desegregate the city. How the History of Waterloo, Iowa, Explains How Meatpacking Plants Became Hotbeds of COVID-19 |by Bernice Yeung and Michael Grabell |December 21, 2020 |ProPublica 

There’s going to be no Medicare money if the hospital doesn’t desegregate. Philip Lee, physician and LBJ official who rolled out Medicare, dies at 96 |Emily Langer |November 6, 2020 |Washington Post 

In 1962, the Kennedy administration was looking for a way to desegregate the Washington Redskins. The Presidents Who Made America’s Sports |Evan Weiner |February 17, 2014 |DAILY BEAST 

Attempting to desegregate Dar Ul Hijra, known in law enforcement circles as “the 9/11 mosque,” is significant. Showdown at the Mosque |Asra Q. Nomani |May 17, 2010 |DAILY BEAST