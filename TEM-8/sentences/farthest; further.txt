Wildfires and hurricanes raged through states that lagged farthest behind on the count, displacing residents and eating up crucial time. Will Americans be able to trust the results of the 2020 Census? |Tara Bahrampour |April 22, 2021 |Washington Post 

Parts farthest from the equator look much bigger than they really are. Earth as you’ve never seen it before |Avery Elizabeth Hurt |April 19, 2021 |Science News For Students 

Flett suggests packing the heaviest items closest to your seat and the lightest items farthest away. How to Pack for Your First Overnight Kayak Trip |Joe Jackson |March 30, 2021 |Outside Online 

Precincts farthest from the coast carried the height limit removal to victory instead. Environment Report: Coastal Neighborhoods Supported Measure E Least |MacKenzie Elmer |November 9, 2020 |Voice of San Diego 

Here are five alternative places we should study in greater detail, from the nearest to farthest. The 5 best places to explore in the solar system—besides Mars |Neel Patel |August 17, 2020 |MIT Technology Review 

Farthest out on the other arm were the chiefs of Throndhjem, and to them likewise was a large and goodly host. The Sagas of Olaf Tryggvason and of Harald The Tyrant (Harald Haardraade) |Snorri Sturluson 

Farthest away in the West, as near the western sea as mountains can stand, are the Cascades. Your National Parks |Enos A. Mills 

Farthest to the north lies the province of Nueva Segovia, which is administered by Dominican friars. The Philippine Islands, 1493-1898 |Emma Helen Blair 

It resides in the land realm of the Farthest North and also throughout the West upon high mountain-tops. The Rocky Mountain Wonderland |Enos A. Mills 

Lowest place Is that of all, obscurest, and remov'd Farthest from heav'n's all-circling orb. The Vision of Hell, Complete |Dante Alighieri