Vincent reportedly said that the mastermind behind the plot was a foreigner named “Mike” who spoke Spanish and English. Family of U.S. Suspect in Haiti Assassination: He’s a Loving, ‘Nice Guy’ Who’s ‘Always Joking’ |Zoe Richards, Noor Ibrahim |July 9, 2021 |The Daily Beast 

Actor and producer Vin Diesel, the star and arguably the creative mastermind of the Fast & Furious franchise, is a big Dungeons & Dragons fan. The rise and rise and rise of the Fast & Furious franchise |Emily VanDerWerff |June 25, 2021 |Vox 

She did not want to be spared simply out of pity for being a wife, nor could she accept that she was the mastermind. How Today’s World Can Help Us See That There’s More to the Ethel Rosenberg Story |Anne Sebba |June 9, 2021 |Time 

The mastermind behind Chula Vista’s drone program, retired Capt. Chula Vista PD’s Drone Program Opened a Revolving Door for Officers |Sofía Mejías Pascoe |April 5, 2021 |Voice of San Diego 

Anita Egbune and Marian Arafiena are the masterminds behind the new platform that has already attracted five live projects. 3 Places to Find Funding For Your Business |Steven Psyllos |April 2, 2021 |Essence.com 

Zubaydah indentified Khalid Sheikh Mohammed as the mastermind of the 9/11 attacks. The Luxury Homes That Torture and Your Tax Dollars Built |Michael Daly |December 12, 2014 |DAILY BEAST 

Self-described 9/11 mastermind Khalid Sheikh Mohammed was also kept at Cobalt after his March 2003 in Pakistan. Inside the CIA’s Sadistic Dungeon |Tim Mak |December 9, 2014 |DAILY BEAST 

Surely only an advanced hacker could mastermind such a fiendish act — right? A Female Writer’s New Milestone: Her First Death Threat |Annie Gaus |December 1, 2014 |DAILY BEAST 

And he was also the secret mastermind behind the biggest bank heists of his day. The High Society Bank Robber of the 1800s |J. North Conway |October 19, 2014 |DAILY BEAST 

But the mastermind of the Hamas kidnapping strategy is al-Arouri, Israeli officials say. Israel Bombs Gaza While Hamas’ Kidnapping Mastermind Sits in Turkey |Eli Lake |July 1, 2014 |DAILY BEAST 

A banquet extraordinary was shortly to take place, and M. Sapin, the mastermind, came to beg of Regali the recipe for his ragout. The Orchard of Tears |Sax Rohmer 

His mastermind comprehended the importance and necessity of combined and harmonious effort. The Great Events by Famous Historians, v. 13 |Various 

This girl was obviously the assistant to the Mastermind who originated the scheme. The Misplaced Battleship |Harry Harrison (AKA Henry Maxwell Dempsey) 

Or, in contact with a mastermind, he may take fire, and the glow of the enthusiasm may be the inspiration of his life. Modern Essays |John Macy 

He said to Professor Garet, "All right, mastermind, untie me." And Then the Town Took Off |Richard Wilson