You can’t overlook the farm system that YouTube is building for content. After years of ‘too much TV,’ the pandemic means there’s now barely enough |Aric Jenkins |August 27, 2020 |Fortune 

Vaccinating animals for their own health and for the protection of humans is commonly done on farms. Can Vaccines for Wildlife Prevent Human Pandemics? |Rodrigo Pérez Ortega |August 24, 2020 |Quanta Magazine 

There’s a huge boom — lots of new chemicals, fertilizers, machinery, that make farms more productive. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

The Miami Marlins, in the midst of a deep rebuild with what we project to be the second-worst run differential in the NL, have improved their farm system, which had been one of the worst in baseball. The Dodgers Lead Our National League Predictions, But Don’t Count Out The Nats Or … Reds? |Travis Sawchik |July 22, 2020 |FiveThirtyEight 

Because you need a wider base to support a taller turbine, beyond a certain tower height, the bases are too wide to be driven from factory to wind farm. GE Will 3D Print the Bases of Wind Turbines Taller Than Seattle’s Space Needle |Jason Dorrier |June 21, 2020 |Singularity Hub 

They were just way too aggressive to try and maintain on a farm here,” says Gow of his “Nazi cows. ‘Nazi Cows’ Tried to Kill British Farmer |Tom Sykes |January 6, 2015 |DAILY BEAST 

But they are serious: what large-scale fracking does is change small farm towns into industrial sites. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

When Reid came on board, he had only leased part of the land to farm on; the deal did not include the house. Ester Elchies, The Estate Built By Whiskey | |December 10, 2014 |DAILY BEAST 

A land farm is the term used for a commercial operation where waste from oil and gas extraction is spread on top of the ground. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

Cold Comfort Farm by Stella Gibbons This novel won the literary Prix Femina Étranger. These Female Contemporaries Weren’t Afraid of Virginia Woolf |Louisa Treger |November 20, 2014 |DAILY BEAST 

The evening previous to his death he was walking about the farm, in the full possession of all his faculties of mind and body. The Every Day Book of History and Chronology |Joel Munsell 

Van Twiller was himself a grower of the plant and had his tobacco farm at Greenwich. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Could this man, who had been sent out to take care of Indians, get back his San Pasquale farm for him? Ramona |Helen Hunt Jackson 

We did plan a great trip—father and mother and Tim and I—we were going to England together when the farm showed a surplus. The Soldier of the Valley |Nelson Lloyd 

The road led to an old fashioned, high gabled farm-house at the foot of the hill; the only tenement visible from that lonely spot. The World Before Them |Susanna Moodie