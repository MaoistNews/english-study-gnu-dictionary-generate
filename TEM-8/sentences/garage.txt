The Thing was created by two entrepreneurs, Kelley Higney and her mother, Ellen McAlister, who started selling the product out of their garage before appearing on an episode of Shark Tank in 2019 to get funding. The Bug Bite Thing Really Works |Abbey Gingras |September 7, 2020 |Outside Online 

Before I knew it, we were back in the garage in the rear seat of the limo. ‘The Dream Architects’: Inside the making of gaming’s biggest franchises |Rachel King |September 1, 2020 |Fortune 

An industrial engineer, Hutchins helped design the M16, the weapon of choice for American soldiers during the Vietnam War, and he invented several tools that may be currently sitting in your garage. Hundreds of Thousands of Nursing Home Residents May Not Be Able to Vote in November Because of the Pandemic |by Ryan McCarthy and Jack Gillum |August 26, 2020 |ProPublica 

His garage was frequently opened when he was in there tinkering. The most secure ways to lock up your bike |By Michael Frank/Cycle Volta |August 26, 2020 |Popular-Science 

In the tumult of the previous weeks, you never visited the car garage. The first murder |Katie McLean |August 19, 2020 |MIT Technology Review 

She began operating out of her home garage in 1980, slowly acquiring the many props and tools that would decorate her dungeon. Whip It: Secrets of a Dominatrix |Justin Jones |November 25, 2014 |DAILY BEAST 

“Wrong place, wrong time,” DeCarli confirmed in a garage at Ferguson police headquarters. Ferguson’s Only Unsolved Murder |Justin Glawe |October 20, 2014 |DAILY BEAST 

Baugh responded after the first attack there in 1993 and helped tow wrecked cars from the bombed garage. The President and the Tow Truck Driver |Michael Daly |September 25, 2014 |DAILY BEAST 

Ford began tinkering in his garage in Detroit in the 1890s, trains and the horse and buggy was the dominant mode of transport. From the Model T to the Model S |The Daily Beast |September 24, 2014 |DAILY BEAST 

Fall in love with the garage-y sounds of Rock N Roll and you might get lost in the Grateful Dead-leaning Cold Roses. ‘Ryan Adams’ Is No Domestic Bliss Album |Keith Phipps |September 12, 2014 |DAILY BEAST 

She knew there would be plenty of rope in the Norwood barn or the garage for their need in erecting the aerials. The Campfire Girls of Roselawn |Margaret Penrose 

Delancy turned the sedan through the door of the big garage, rolled across the wide parking floor to the cement ramp at the rear. Hooded Detective, Volume III No. 2, January, 1942 |Various 

It is a poorly appointed hotel that does not now have a garage of some sort, and in many cases, necessary supplies are available. British Highways And Byways From A Motor Car |Thomas D. Murphy 

At the first garage where I applied, a quotation made was withdrawn when it was learned that I was an American. British Highways And Byways From A Motor Car |Thomas D. Murphy 

In no case should a motorist pay a bill at a London garage without a proper receipt. British Highways And Byways From A Motor Car |Thomas D. Murphy