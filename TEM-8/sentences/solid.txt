They are essentially solid aluminum discs that bolt onto each of your hubs. Six ways to make your ATV even more rugged |By Tyler Freel/Outdoor Life |September 17, 2020 |Popular-Science 

Those minerals might serve as nucleation points — making the bottled water freeze into a solid block of ice as soon as it gets cold enough to freeze. Build ice towers with bottled water and ice |Bethany Brookshire |September 16, 2020 |Science News For Students 

They are so solid everywhere that they’d be better off with a game manager. We Knew A Football Team Would Win In Week 1. But Maybe Not ‘Football Team.’ |Sara Ziegler (sara.ziegler@fivethirtyeight.com) |September 14, 2020 |FiveThirtyEight 

We knew it would be a couple of years of solid, thorough and cohesive investment. After reaching profitability in 2019, Politico EU aims for 10% revenue growth this year |Lucinda Southern |September 11, 2020 |Digiday 

The device, which Microsoft calls “the world’s most powerful console,” will support 4K graphics and feature a solid-state drive, allowing for faster loading times than previous video-game hardware. It’s official: The Xbox Series X will launch in November |radmarya |September 9, 2020 |Fortune 

“This will take a lot of solid negotiating,” says Mark Schneider of the International Crisis Group. Meet America’s Next Ambassador to Cuba |Eleanor Clift |December 18, 2014 |DAILY BEAST 

By nightfall, I had showered, eaten some soup that a friend brought me, and I slept in my room for 12 solid hours. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

So there is nothing wrong with using the charms of, say, Parks and Recreation, to create some solid bonding time. Binge Watching is the New Bonding Time |The Daily Beast |December 10, 2014 |DAILY BEAST 

But I'm pretty solid in the knowing that he's disgusted by that. The Chris Brown vs. Drake Feud Continues: Brown Claims Ex GF Karrueche Tran Cheated with Drizzy |Marlow Stern |December 7, 2014 |DAILY BEAST 

Now, the Memphis congressman is one of only a handful of white Southerners in his caucus and the once Solid South is deep red. Southern Dems Won’t Rise Again |Ben Jacobs |December 5, 2014 |DAILY BEAST 

When it cleared, the valley was a solid expanse of white, and the stars shone out as if in an Arctic sky. Ramona |Helen Hunt Jackson 

I hope the French Government will recognize this dashing stroke of d'Amade's by something more solid than a thank you. Gallipoli Diary, Volume I |Ian Hamilton 

But it was neither his talents as a diplomatist, nor his remarkable mind, nor his solid erudition, which made Nicot immortal. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Black Sheep was sent to the drawing-room and charged into a solid tea-table laden with china. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

In my house there has never been sufficient food for a solid meal, and I have not land enough even for an insect to rest upon. Our Little Korean Cousin |H. Lee M. Pike