For years, brands and agencies were caught in a self-defeating cycle, where agencies didn’t pitch gaming ideas to brands because they assumed brands didn’t know what to do with them. Why new agencies are trying to capitalize on the online gaming boom |jim cooper |February 12, 2021 |Digiday 

Oftentimes, you can soak your clothing, linen, or other fabrics in a pre-soak cycle, wash, and then enjoy the fresh-scented and stain-free result. Pet Stain & Odor Removers That Get The Job Done |PopSci Commerce Team |February 9, 2021 |Popular-Science 

This is just the latest flashpoint in a cycle of delayed elections, public frustration and fraying political legitimacy in the impoverished Caribbean nation. Coup claim deepens Haiti's political crisis |Dave Lawler |February 8, 2021 |Axios 

Our bodies groove to the familiar rhythms of heartbeats and breaths — persistent cycles essential to survival. Brain’s ‘Background Noise’ May Hold Clues to Persistent Mysteries |Elizabeth Landau |February 8, 2021 |Quanta Magazine 

This cycle of defensiveness and shame can make it harder to parent in that moment. Remote school is stressing parents out. Here’s how to tame the anxiety. |Devorah Heitner |February 8, 2021 |Washington Post 

It almost mirrors the Buddhist cycle of life, death, and rebirth. The Buddhist Business of Poaching Animals for Good Karma |Brendon Hong |December 28, 2014 |DAILY BEAST 

Eventually, the mistletoe bush grows, blooms, and forms berries, and the cycle begins anew. Mistletoe is the Vampire of Plants |Helen Thompson |December 21, 2014 |DAILY BEAST 

The Newsroom aired its final episode on Sunday, already an eternity ago in news-cycle terms. A Few Great Men Too Many: Aaron Sorkin Doesn’t Think You Can Handle the Truth |Arthur Chu |December 21, 2014 |DAILY BEAST 

To break her self-destructive cycle and heal, she decides to hike 1,100 miles of the Pacific Crest Trail solo. Exclusive: The Making of Reese Witherspoon’s Golden Globe-Nominated ‘Wild’ |Marlow Stern |December 12, 2014 |DAILY BEAST 

Especially given that we just spent almost $4 billion during the 2013-14 election cycle? Just What We Needed: More Campaign Spending |Mark McKinnon |December 8, 2014 |DAILY BEAST 

The sexual cycle can take place only within the body of one genus of mosquito, anopheles. A Manual of Clinical Diagnosis |James Campbell Todd 

Here they enter red corpuscles as young malarial parasites, and the majority pass through the asexual cycle just described. A Manual of Clinical Diagnosis |James Campbell Todd 

Still hanging to Clip's canteen, he jerked the motor-cycle away from the bushes, got into the saddle, and started the pedals. Motor Matt's "Century" Run |Stanley R. Matthews 

Matt went down, with the motor-cycle on top of him, head and shoulders over the brink of the precipice. Motor Matt's "Century" Run |Stanley R. Matthews 

Matt had come to a quick stop, disengaging his right foot from the toe-clip and bracing the motor-cycle upright. Motor Matt's "Century" Run |Stanley R. Matthews