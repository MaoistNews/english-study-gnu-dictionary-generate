To do so, governments need to boost the adoption of electric vehicles through clear tax incentives, diesel and petrol engine bans, and major infrastructure investments. Use today’s tech solutions to meet the climate crisis and do it profitably |Walter Thompson |February 12, 2021 |TechCrunch 

If such a ban is not possible, steps should be taken to improve air circulation, the study said. Airports have taken steps to reduce coronavirus transmission but risks still remain, study says |Lori Aratani |February 12, 2021 |Washington Post 

The ban will apply to plastics that are designed to be used once and then discarded — water bottles, cutlery, candy wrappers and bags — that clog local waterways and overwhelm landfills, LeBlanc said. George Washington University commits to single-use-plastic ban |Lauren Lumpkin |February 11, 2021 |Washington Post 

Iowa’s House speaker said he can’t make lawmakers wear masks — but he did enforce a ban on jeansRep. The governor of Iowa lifted coronavirus restrictions. These cities are keeping them anyway. |Paulina Villegas |February 8, 2021 |Washington Post 

To get the space off right, Smith set up the pop-up in a matter of days, installing everything from the electric lines to blow-up snowmen to ensure the igloos would be ready for the deep winter chill and the temporary complete ban on indoor dining. Shaw’s HalfSmoke is so cool, it’s hot |Evan Caplan |February 5, 2021 |Washington Blade 

If the Israel model ban were directed towards disordered eating, Ravin says she would support it whole-heartedly. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

But in 1969, a longstanding practice was challenged—its ban on women. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

A ban on the ringing of church bells, lifted in 1941, was reimposed. Remembering the Russian Priest Who Fought the Orthodox Church |Cathy Young |December 28, 2014 |DAILY BEAST 

Aside from a blanket ban, social media platforms like Twitter, Facebook, and Reddit are nearly impossible to control. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

Or are the risks so great that a temporary ban is necessary? New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

We no longer live in an age when down-trodden laborers meet by candlelight with the ban of the law upon their meeting. The Unsolved Riddle of Social Justice |Stephen Leacock 

Weary of their ungodliness the Church placed its ban upon them under this ban it seems they die. St. Martin's Summer |Rafael Sabatini 

Even when Christianity fell under the ban of persecution that freedom of sepulture was not at first interfered with. The Catacombs of Rome |William Henry Withrow 

For ten years Matthew and Mary lived happily together, or would have been happy if it had not been for the ban of the church. Bell's Cathedrals: A Short Account of Romsey Abbey |Thomas Perkins 

"Aye ban tank we joost get it nice quiet van you come back again," Anderson remarked in mock melancholy. The Argus Pheasant |John Charles Beecham