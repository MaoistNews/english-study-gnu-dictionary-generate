She climbed the few front steps to explain that we were standing at the Blockhouse, a fortress predating the park that was used to fire against enemy combatants during the War of 1812. Meet the Rangers of New York City’s Parks |klindsey |July 30, 2021 |Outside Online 

After the break, and likely realizing they had to get through the rest of the hour, the two combatants issued mea culpas. Whoopi Goldberg Has Had It With Meghan McCain: ‘I Don’t Care That You Don’t Care!’ |Justin Baragona |June 17, 2021 |The Daily Beast 

It should come as no surprise that women fight alongside men in the ethnic armed organizations, whereas the Myanmar military has no women in its combatant ranks. Myanmar's Women Are Fighting for a New Future After a Long History of Military Oppression |MiMi Aye |June 1, 2021 |Time 

Yet the book nonetheless ends with the American combatants looking back on the Pech Valley — and their war — with a little wistfulness. American troops fought hard in Afghanistan’s Pech Valley. But why? |Andrew Exum |March 26, 2021 |Washington Post 

Each combatant will choose their weapons — be it a dagger, a shield, or a fork and knife — and whoever inflicts the most pain will be declared the winner. On Discord, bots find a foothold as mini indie success stories |Luke Winkie |March 25, 2021 |Washington Post 

But Broyles is too good here, and admits that women too feel the pull of war by being exposed to it, even as a non-combatant. War Is Hell and Such Good Fun |Don Gomez |November 11, 2014 |DAILY BEAST 

It is immaterial if the infidel is a combatant or a civilian. The CIA’s Wrong: Arming Rebels Works |Christopher Dickey |October 19, 2014 |DAILY BEAST 

He was captured by Afghans responding to leaflets distributed by the U.S. promising $5,000 per combatant. A Navy Lawyer Cries Foul on Gitmo’s Kafkaesque Legal System |Eleanor Clift |September 26, 2014 |DAILY BEAST 

Of most interest are the English-speaking narrator and a masked American-accented combatant featured in the film. How ISIS Ripped Off ‘Natural Born Killers' |Jamie Dettmer |September 23, 2014 |DAILY BEAST 

Both my combatant and I claimed that the other was at fault. Ben Carson Was a Role Model for Black Teens Until He Sold Out to the Right |Joshua DuBois |March 16, 2014 |DAILY BEAST 

The boats were then pulled close to one another, and each combatant endeavoured to push his antagonist into the water. A Woman's Journey Round the World |Ida Pfeiffer 

In one minute from the signal for retreat the top of the hill did not contain a single painted combatant. Overland |John William De Forest 

General Lee simply said the town was non-combatant; that he would not occupy it, nor would he allow any one else to occupy it. Historic Fredericksburg |John T. Goolrick 

As a combatant, a warrior, a reformer, his person and character somewhat change. Beacon Lights of History, Volume VI |John Lord 

How strange that such an encounter did take place sooner than either white or red combatant dreamed! The Phantom of the River |Edward S. Ellis