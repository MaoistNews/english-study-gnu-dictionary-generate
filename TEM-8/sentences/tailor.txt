It’s a way to get properly fitted clothing without a trip to the tailor, where social distancing is impossible, or having to try on clothes in crowded retail stores. For Amazon’s $25 custom T-shirt, your body is a wonderland (of data) |Heather Kelly |January 5, 2021 |Washington Post 

I worked with my stylists and friends Marni Senofonte and Deonte Nash to create a custom look with our tailor Arturo and his daughter Christina from Rancho Tailors. Cassie Is Pregnant With Her Second Child |Jasmine Grant |December 11, 2020 |Essence.com 

The team of milliners, tailors and textile experts has been studiously sewing face coverings since the spring, so everyone working on the 300-acre property, whether they’re maintenance or Martha Washington, has masks made on-site. At tourist sites, masking up without diluting the experience |Vicky Hallett |November 23, 2020 |Washington Post 

Our next step is to combine cuts and folds, thereby becoming even better tailors. A Scientist Who Delights in the Mundane |Steve Nadis |October 26, 2020 |Quanta Magazine 

It can identify and reproduce the “chemical fingerprints” of aged liquors, he said, or tailor tastes, colors and aromas to specific requirements. A Nespresso Machine for Whiskey |Daniel Malloy |October 9, 2020 |Ozy 

One minute the script, the next a story about Ivor Novello's tailor or the Tahiti steamer schedule in the Thirties. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He had a tailor who ran up dozens of the same suit in different sizes to account for slight variations in his weight. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He looked, that dreadful afternoon, as if he had just come from his barber, tailor and haberdasher. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 

The reason why these guys are so successful is because they have no egos, and specifically tailor the parts for the actor. Dave Franco Uncut: The Actor on ’22 Jump Street,’ ‘The Room,’ and His Bro’s Nude Instagrams |Marlow Stern |June 13, 2014 |DAILY BEAST 

We should think about mental health more like how we tailor physical training routines. Fixing Military Mental Healthcare |Marjorie Morrison |April 6, 2014 |DAILY BEAST 

The tailor of the fairy tale with his "seven at a blow" is not in it with the gunnery Lieutenant of a battleship. Gallipoli Diary, Volume I |Ian Hamilton 

But she is greatly interested in certain shops that she is buying out, and especially in her visits to her tailor. Confidence |Henry James 

Is a Tailor, that can make a new Coat well, the worse Workman, because he can mend an old one? A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

Well, I am either a tailor or a cooper, and for the life of me I can't tell which: at any rate, I'm either one or the other. The Book of Anecdotes and Budget of Fun; |Various 

Purt was gorgeous in a Canadian skating suitor so the tailor who sold it to him had called it. The Girls of Central High on the Stage |Gertrude W. Morrison