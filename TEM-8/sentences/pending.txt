So as soon as you switch it on, you may receive a plethora of alerts prompting you to install pending updates. The first 4 things you should do right after you buy a new gadget |empire |June 29, 2021 |Popular-Science 

Except, Nomadness currently has 3,000 pending membership requests. ‘We Out Here’: Inside the New Black Travel Movement |Charlise Ferguson |January 4, 2015 |DAILY BEAST 

In August, the Obama administration intervened to stop what it called a pending genocide of Yazidi minorities in Iraq. Yazidis Face Genocide by ISIS After U.S. Turns Away |Josh Rogin |November 4, 2014 |DAILY BEAST 

Addison was allowed out of jail, finally, but her passport was held pending an investigation—even though nobody questioned her. Let’s Free Stacey Addison, The Oregon Woman Jailed at the Ends of the Earth |Christopher Dickey |October 30, 2014 |DAILY BEAST 

His punishment: Suspension from all team activities indefinitely, pending a criminal investigation of the charges. Gator Quarterback’s Lawyer: Blame This Victim |Kelsey Meany |October 10, 2014 |DAILY BEAST 

The decision was stayed however, pending Supreme Court review. The Heroic Lesbian Couple of Oklahoma Who Fought for Equal Marriage—and Won |Randy R. Potts |October 7, 2014 |DAILY BEAST 

That the Society be immediately dissolved, in view of pending litigation. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

It is suggested, too, that patents are pending that may make outside aerials unnecessary, anyway. The Campfire Girls of Roselawn |Margaret Penrose 

An appeal by a member of a subordinate lodge from a vote of expulsion does not abate by his death while the appeal is pending. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

They have encamped in the rear of our office, pending the arrival of the next coasting steamer. The Book of Anecdotes and Budget of Fun; |Various 

He therefore commenced a lawsuit against her and her guardians, and the suit is still pending. Journal of a Voyage to Brazil |Maria Graham