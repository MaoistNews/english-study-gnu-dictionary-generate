Graham, 65, has long been a shoo-in in South Carolina, winning Senate races by double digits in three successive races. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

The Oxford report suggests a number of possible solutions, including granting two successive games to each host and larger cost contingencies. Want to Host the Olympics? Plan to Go Over Budget |Fiona Zublin |September 14, 2020 |Ozy 

As successive years of record-breaking funding signaled investor conviction in the promise of the continent’s startups, the topical focus soon shifted with more scrutiny paid to just how quickly investors could expect profitable exits. African tech startups are beating the pandemic’s odds and racking up multimillion-dollar exits |Yomi Kazeem |September 10, 2020 |Quartz 

The system then maps a strike zone for each successive batter based on his height in its database. A robot referee can really keep its ‘eye’ on the ball |Kathryn Hulick |August 20, 2020 |Science News For Students 

Conversely, the Raptors got more aggressive the second time they played the Portland Trail Blazers, as well as with each successive game they played against the Charlotte Hornets. The Raptors’ Defense Is Almost Never The Same, But It’s Always Really Good |Jared Dubin |August 17, 2020 |FiveThirtyEight 

With each successive layer of networks, the U.S. became more potent. Electricity Superhighway | |October 16, 2014 |DAILY BEAST 

After the three others were killed in successive Israeli operations in the mid-1990s, Zeif took command. A Who’s Who of Iran’s Favorite Palestinian Terrorists |IranWire |August 13, 2014 |DAILY BEAST 

Laypeople tend to regard two successive quarters of negative growth as a recession. The U.S. Economy Had a Hiccup, Not a Heart Attack, This Year |Daniel Gross |May 29, 2014 |DAILY BEAST 

The West has seen what Putin has done with Russia during his successive terms in office. El-Sisi Is The Putin Of The Nile |Jamie Dettmer |February 22, 2014 |DAILY BEAST 

Perhaps hotels will offer package deals through which visitors can visit all the talk-shows on three successive nights. Can Jimmy Fallon Be Himself on ‘The Tonight Show’? |Tom Shales |February 17, 2014 |DAILY BEAST 

Deppe believes that one must go through successive steps of preparation before one is fitted to attack the great concert works. Music-Study in Germany |Amy Fay 

Marked variation in the amount at successive examinations strongly suggests a neurosis. A Manual of Clinical Diagnosis |James Campbell Todd 

Successive examinations may show normal, increased, or diminished hydrochloric acid, or even entire absence of the free acid. A Manual of Clinical Diagnosis |James Campbell Todd 

On lifting the fingers in successive order from the bottom end, we get the seven notes of the major scale. The Recent Revolution in Organ Building |George Laing Miller 

In successive letters he reiterates the caution to beware of surprise and treason, and his anxiety for constant news. King Robert the Bruce |A. F. Murison