She had been taught to value her mind over her body, which led her to view her sobriety as a mental power she exerted over herself. Julien Baker questioned her faith. Music helped her embrace the uncertainty. |Sonia Rao |February 26, 2021 |Washington Post 

In a pandemic, when so many people are suffering with depression and anxiety, scheduling a few visits with a therapist as you work toward sobriety may be surprisingly useful. We’re a Year into COVID. We Still Drink Too Much. |AC Shilton |February 18, 2021 |Outside Online 

The officer conducted a field sobriety test on Reid and noted “four clues of impairment.” As police continue probe of Britt Reid crash, 5-year-old remains in critical condition |Rick Maese |February 9, 2021 |Washington Post 

There’s that group for whom the meeting was not only a way for them to work toward their sobriety . What was lost when covid forced addiction support groups online — and what was gained |Allyson Chiu |November 23, 2020 |Washington Post 

You can be someone working really hard on sobriety or you can be someone with 30-plus years of sobriety. The pandemic is taking a toll on parents, and it’s showing in alcohol consumption rates |Sarah Hosseini |November 9, 2020 |Washington Post 

When he slurred and smelled of alcohol, they ordered him out of the car for sobriety tests. The U.S. Veteran and Wisconsin Boy Who Went to Fight ISIS in Syria |Jacob Siegel |October 3, 2014 |DAILY BEAST 

A police officer suspected that she was under the influence of a drug after she failed a series of field sobriety tests. The Amanda Bynes Train Wreck Is Back Again, Following a New DUI Arrest |Kevin Fallon |September 29, 2014 |DAILY BEAST 

Sobriety brought a new, kinder, and gentler Womack, who often expressed remorse and regret over his past offenses. Bobby Womack’s Sexual Democracy: The Late Soul Legend Preached Mutual Pleasure |David Masciotra |June 29, 2014 |DAILY BEAST 

Twitter, like the national debt or Lindsay Lohans's sobriety, is in a constant state of flux. A Song of Twitter and George R.R. Martin: The Unexpected Players of the Twitterverse |Amy Zimmerman |June 10, 2014 |DAILY BEAST 

In a speech to the crowd, the smiling 19-year-old called her journey to sobriety “a miracle.” Coming Out of the Addiction Closet |Abby Haglage |May 10, 2014 |DAILY BEAST 

Sometimes it comes in literal sobriety, sometimes in derisive travesti, sometimes in tragic aggravation. Checkmate |Joseph Sheridan Le Fanu 

It was one of his good days and, in spite of his sobriety, he had himself in very good control when he left his aunt. Uncanny Tales |Various 

If the reasons in favour of sobriety seem to him to outweigh the reasons in favour of drink, he will keep sober. God and my Neighbour |Robert Blatchford 

Bill Talpers's return to sobriety was considerably hastened by alarm after the trader's words with Lowell. Mystery Ranch |Arthur Chapman 

Robert Nelson's sobriety of judgment and sound practical sense made him a far more effective champion. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton