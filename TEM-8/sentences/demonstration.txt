ClearFlame is also conducting a demonstration on a Cummins engine platform supported by funding from the Department of Energy. ClearFlame Engine Technologies takes aim at cleaning up diesel engines |Kirsten Korosec |September 17, 2020 |TechCrunch 

Next month, the company will unveil a demonstration aircraft called the XB-1, a new supersonic-capable plane that is one-third the size of what they hope their commercial airliner will be. Air Force transport jets for VIPs could have a supersonic future |Rob Verger |September 10, 2020 |Popular-Science 

Federal officials have arrested more than 300 people since the demonstrations began. A summer of protests exposed deep demands for change—and entrenched obstacles |kdunn6 |September 7, 2020 |Fortune 

There’s a lot of demonstrations that were happening in LA for years that I didn’t know about until all of this. After Playing So Many Roles, Who Is the Real Tatiana Maslany? |Eromo Egbejule |September 4, 2020 |Ozy 

It was the beginning of a week of unrest and protests that mirrored destructive demonstrations seen earlier this summer following police violence. Kenosha’s looting is a symptom of a decrepit democracy |Aaron Ross Coleman |September 4, 2020 |Vox 

The reviews in themselves constitute a demonstration of why the regime restricts the Internet. Inside the ‘Surprisingly Great’ North Korean Hacker Hotel |Michael Daly |December 20, 2014 |DAILY BEAST 

As Sarah and her sister and mother headed for the warmth of home, the demonstration continued. Synagogue Slay: When Cops Have to Kill |Michael Daly |December 10, 2014 |DAILY BEAST 

Not that the demonstration had anything to do with this couple, whom Sarah seems to see as a fairy tale come to life. Synagogue Slay: When Cops Have to Kill |Michael Daly |December 10, 2014 |DAILY BEAST 

There is an extreme demonstration of this divide in the nation. Beer Countries vs. Wine Countries |Clive Irving |December 7, 2014 |DAILY BEAST 

For anyone who cared to watch, the event and its denouement provided a graphic demonstration that the Iron Curtain was crumbling. How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

But for the delinquency of his son, she had ocular demonstration; and her indignation was hardly to be repressed. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Already its demonstration had tried her temper, but to-night, for the first time, she felt her whole being set on edge by it. Bella Donna |Robert Hichens 

He determined therefore to make a threatening demonstration by day and slip past it by night. Napoleon's Marshals |R. P. Dunn-Pattison 

When they form short chains, demonstration of the capsule is necessary to distinguish them from streptococci. A Manual of Clinical Diagnosis |James Campbell Todd 

This truth is as old as Homer, and its proofs are as capable of demonstration as a mathematical axiom. The Book of Anecdotes and Budget of Fun; |Various