Apple held its second-ever virtual-only product event on Tuesday that centered on upgrades for two major product lines, Apple Watch and iPad. Everything announced at Apple’s ‘Time Flies’ event today |rhhackettfortune |September 15, 2020 |Fortune 

He also has an upgraded defensive supporting cast, which added a few pieces to help stop the Derrick Henrys of the world from running all over them. What To Watch For In An Abnormally Normal 2020 NFL Season |Neil Paine (neil.paine@fivethirtyeight.com) |September 9, 2020 |FiveThirtyEight 

“I’m a big fan of capitalism, but like everything else, it needs an upgrade,” he says. PayPal’s CEO on why moral leadership makes clear capitalism needs an upgrade |McKenna Moore |September 8, 2020 |Fortune 

The new treadmill will likely go to market before the upgraded smart bike, in terms of availability, according to the report. Peloton said to be launching new, cheaper treadmill and higher-end stationary smart bike |Darrell Etherington |September 4, 2020 |TechCrunch 

Also on the chopping block would be 8,400 jobs across the MTA system and further delays of long-planned upgrades to the system. New York City transit needs a $12 billion bailout—or the entire U.S. economic recovery may suffer |dzanemorris |September 3, 2020 |Fortune 

The Raptor is also difficult and expensive to maintain and is exceedingly difficult to upgrade. $70 Billion Stealth Jet Finally Flies in Its First War |Dave Majumdar |September 23, 2014 |DAILY BEAST 

In the 70s it went through a brief stint as an X-rated adult cinema before an upgrade and clean sweep in the early 80s. Catch a Show at Atlanta’s Oldest Indie Cinema |Starbucks |July 10, 2014 |DAILY BEAST 

(Free for limited access, upgrade for $15 a month or $80 a year; available for iOS) 2. 10 Best Apps to Train Your Brain |DailyBurn |June 9, 2014 |DAILY BEAST 

First, the group is aiming to “upgrade” legislative seats currently held by Republicans. With Incumbents To Protect, The Tea Party Is Now Playing Defense |Michelle Cottle |March 20, 2014 |DAILY BEAST 

So there you have it—three simple, easy-to-use programs that biohackers use to upgrade their performance every day. These 3 Apps Will Help You Sleep Better, Feel Great, and Eat Well |Dave Asprey |December 18, 2013 |DAILY BEAST 

As he swept upon the upgrade to the track, he heard from behind Peter Barrett's shout. The Boy Scouts of Lakeville High |Leslie W. Quirk 

The Catholics, through their colegios (ecclesiastical schools) attempted to upgrade life. The Haciendas of Mexico |Paul Alexander Bartlett 

Heat of a wind that might have come out of the fire-box of a Mogul on an upgrade pull. Two Thousand Miles Below |Charles Willard Diffin 

Then, the red-roofed buildings of the Boyville School came into sight as they started upgrade. Joan of the Journal |Helen Diehl Olds 

The inference is obvious: that the Monarchists are on the upgrade, and the Republicans on the downgrade. France and the Republic |William Henry Hurlbert