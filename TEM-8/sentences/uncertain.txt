It’s essential for companies to capitalize on the essential time together during these uncertain times. Best practices for Zoom board meetings at early-stage startups |Walter Thompson |February 12, 2021 |TechCrunch 

Startled and uncertain, Teichner takes in the proposal and answers with a solid maybe. A ‘money pit’ dog, an ailing owner and one big question |Diane Cole |February 5, 2021 |Washington Post 

The end of this sickness is still uncertain and many months away, especially for those who live in the poorest parts of the planet. The virus caused more than a pandemic. It set us all ablaze. |Philip Kennicott |February 5, 2021 |Washington Post 

Today, the hotel and restaurant remain closed, their future as uncertain as Riley’s. Sommeliers have been forced to pivot in the pandemic, and their futures remain uncertain |Dave McIntyre |February 4, 2021 |Washington Post 

Back then, it was still uncertain whether fall marathons would be canceled. Will Major Marathons Actually Come Back This Fall? |Martin Fritz Huber |February 4, 2021 |Outside Online 

If the certainty of the wisdom of uncertainty is itself uncertain, the force of the definition crumbles by logical standards. The Birth of the Novel |Nick Romeo |November 27, 2014 |DAILY BEAST 

In war, he wrote, “everything is uncertain … all military action is intertwined with psychological forces and effects.” How Clausewitz Invented Modern War |James A. Warren |November 24, 2014 |DAILY BEAST 

“The jailer had a high-school-age daughter and was uncertain how to help her get in college,” writes Ferris. The Tragic History of Southern Food |Jason Berry |November 12, 2014 |DAILY BEAST 

Meanwhile, ISIS is on the march, and President Obama strikes an uncertain pose. Iowa Frontrunner Mike Huckabee Talks to The Daily Beast |Lloyd Green |September 22, 2014 |DAILY BEAST 

Even “destroyed” becomes an uncertain term when applied to these sort of digital files. Your Arrest Video Is Going Online. Who Will See It? |Jacob Siegel |September 11, 2014 |DAILY BEAST 

Distance, the uncertain light, and imagination, magnified it to a high wall; high as the wall of China. The Giant of the North |R.M. Ballantyne 

Uric acid is decreased before an attack of gout and increased afterward, but its etiologic relation is still uncertain. A Manual of Clinical Diagnosis |James Campbell Todd 

An estate upon condition is one which depends upon the happening or not happening of some uncertain event. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

It is therefore uncertain from these statements which furnace consumes the greater quantity of air. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Again Arabella inclined her head, and looked uneasily round as if uncertain whether to call for assistance. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens