As Senators, who serve as jurors, consider whether the then-President incited the mob that overtook the Capitol, yesterday’s first day of presentations left those of us watching closely with plenty of reason to shake. This Video of Jan. 6’s Insurrection Should Be Mandatory |Philip Elliott |February 10, 2021 |Time 

A new plan laying out how San Diego’s federal court will select jurors is drawing criticism from law professors, social scientists, local attorneys and community organizations. Morning Report: Federal Court’s Jury Selection Plan Draws Scrutiny |Voice of San Diego |January 22, 2021 |Voice of San Diego 

The new plan, the groups argue, still doesn’t do enough to ensure that the community will be reflected in potential juror pools. Morning Report: Federal Court’s Jury Selection Plan Draws Scrutiny |Voice of San Diego |January 22, 2021 |Voice of San Diego 

They provided several recommendations for how the court could expand its pool of potential jurors, which could help increase jury diversity. Federal Court’s Jury Selection Plan Under Fire |Maya Srikrishnan |January 22, 2021 |Voice of San Diego 

The jurors also thought his original attorneys did a bad job of defending him at trial. Brandon Bernard’s Execution Raises Serious Questions About Efficacy Of Death Penalty |Kirsten West Savali |December 11, 2020 |Essence.com 

A grand juror in the Ferguson case is suing to be able to explain exactly what went down in the courtroom. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

There is no perfect juror, just as there is no such thing as a perfect witness. The Post-Brown and Garner Question: Who ‘Deserves’ to Die? |Goldie Taylor |December 9, 2014 |DAILY BEAST 

The juror is said to have invoked common sense in the face of the statutes as codified by the State of Illinois. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

Even so, at least one juror apparently balked at convicting a man for violating wildlife protection laws by protecting wildlife. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

“We really want to know about that day,” another juror said. The Day Monica Lewinsky Beat the Feds' Shakedown |Michael Daly |May 8, 2014 |DAILY BEAST 

What any special juror knows from any other source is not relevant there to procure conviction. The Trial of Theodore Parker |Theodore Parker 

The one put to me in my official capacity as juror, is this: "Did Greatheart aid the woman?" The Trial of Theodore Parker |Theodore Parker 

Reader, you are empannelled as a juror to try a plain case and bring in an honest verdict. The Anti-Slavery Examiner, Part 3 of 4 |American Anti-Slavery Society 

The ordinary juror tends to be sceptical as to the good faith of the defence of insanity. Courts and Criminals |Arthur Train 

It is almost needless to say that the son of the non-juror and his immediate posterity were staunch Jacobites, one and all. In the Days of My Youth |Amelia Ann Blandford Edwards