As Packer said about dashboards as an easier entry, that’s more eyeballs on sponsored products without the NHL cost. Women’s Hockey Is Seeing A Sponsorship Boom |Marisa Ingemi |January 26, 2021 |FiveThirtyEight 

The smartest buyers are learning CTV can be about much more than chasing eyeballs. Four big connections every connected TV buyer needs to start making |Samsung Ads |January 18, 2021 |Digiday 

It has forced them to get creative and lean on the wealth of knowledge they have about consumers to create original, flashy, engaging, and attention-grabbing content that draws eyeballs. What a chaotic digital media landscape means for advertisers as 2020 ends |Adam Ortman |December 31, 2020 |Search Engine Watch 

They work by changing how light enters the eyeball, preventing the conditions that cause myopia to progress. Five good-news health stories from this past year |Kat Eschner |December 24, 2020 |Popular-Science 

For example, Google has made several changes that keep eyeballs on their own results pages instead of diverting that traffic to other websites. The rise of experience-driven immersive content |Ceros |December 6, 2020 |Digiday 

The two countries were eyeball to eyeball in a tense standoff for almost a year. ICYMI: India-Pakistan Head for Nuke War |Bruce Riedel |October 20, 2014 |DAILY BEAST 

For many this can be totally doable, while those averse to numbers might rather just eyeball their portions. Is the IIFYM Diet Right for You? |DailyBurn |December 2, 2013 |DAILY BEAST 

Hedren narrowly missed having an eyeball clawed out during one take. The ‘Blue Is the Warmest Color’ Feud and More Actresses Who Were Terrorized By Directors |Kevin Fallon |October 25, 2013 |DAILY BEAST 

The arctic char eyeball, on the other hand, which I had the day after—a raw eyeball is a whole other matter. How to Think With Your Gut |Mindy Farabee |April 9, 2013 |DAILY BEAST 

Bigwigs headhunting superhumans to run splashy enterprises eyeball you. Zodiac Beast: April 24-30 |Starsky + Cox |April 23, 2011 |DAILY BEAST 

But thou art silent, And from thine eyeball flames contemptuous anger. The Death of Balder |Johannes Ewald 

They had a secret iv rasslin' be which a Jap rassler cud blow on his opponent's eyeball an' break his ankle. Mr. Dooley Says |Finley Dunne 

Here the radium rays had acted upon the eyeball through the bones of the head. The Strand Magazine, Volume XXVII, January 1904, No. 157 |Various 

My heart was in my mouth, for at first I believed from his expression that he had detected the gleam of my eyeball. The Moon Metal |Garrett P. Serviss 

Things that rode meaningless on the eyeball an instant before slid into proper proportion. Kim |Rudyard Kipling