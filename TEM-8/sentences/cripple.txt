At one time Myanmar was subjected to harsh sanctions that crippled its economy. In Myanmar coup, Suu Kyi’s ouster heralds return to military rule |Shibani Mahtani, Timothy McLaughlin |February 1, 2021 |Washington Post 

Second, our sense of the possible has been crippled by two decades of helplessness and resignation under the thumb of the tech giants. 'We Need a Fundamental Reset.' Shoshana Zuboff on Building an Internet That Lets Democracy Flourish |Billy Perrigo |January 22, 2021 |Time 

Our sense of the possible has been crippled by two decades of helplessness and resignation under the thumb of the tech giants. Big Tech's Business Model Is a Threat to Democracy. Here's How to Build a Fairer Digital Future |Billy Perrigo |January 22, 2021 |Time 

During the Nigerian Civil War of the late 1960s, the Nigerian government famously cut off food supplies from the citizens of Biafra, leaving many Biafrans hungry while significantly crippling its military base. Feeding a New Nigeria |Nelson C.J. |January 22, 2021 |Eater 

As the immune system tries to fight off the virus, the lungs and those air sacs become inflamed and fill with fluid, crippling their ability to transport oxygen into the blood. We Didn’t Evolve for This - Issue 94: Evolving |Claudia Geib |January 6, 2021 |Nautilus 

After she battled polio and learned to walk again, the doctors told her she would be a cripple her entire life. Uzo Aduba: My Road to ‘Orange Is the New Black’ |Uzoamaka Aduba |August 4, 2014 |DAILY BEAST 

Sectoral sanctions that could cripple the Russian economy are also long overdue. It’s Finally Time for the West to Stand Up to Putin |James Kirchick |July 18, 2014 |DAILY BEAST 

On September 16 he was called into court in Manhattan, charged with the alarming crime of punching a cripple. Babe Ruth’s Summer of Records |Bill Bryson |September 29, 2013 |DAILY BEAST 

Bring down the Assads, and you cripple the mullahs in both Iran and Lebanon. Up to Speed: Five Things You Need to Know on Syria |Christopher Dickey |September 3, 2013 |DAILY BEAST 

In some cases, the aftermath of disasters can cripple the very infrastructure that would enable recovery. Three Years After Gulf Oil Spill, Money Continues to Flow to Region |Filipa Ioannou |July 29, 2013 |DAILY BEAST 

That would have ended our little Alila's life in a moment, or at least made him a cripple for the rest of his days. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

There was, however, only one waggon and that a cripple, and neither carpenters nor smiths were at the station to repair it. Robert Moffat |David J. Deane 

The first of these unfortunates was of the parish of Barking, aged sixty-eight, a painter and a cripple. Fox's Book of Martyrs |John Foxe 

For a man only to give a half confidence, is to cripple to that extent the capacity of the one who is responsible. Ancient Faiths And Modern |Thomas Inman 

And I'm a cripple, and she's beautiful—— Oh, my mind's in a muddle! Patchwork |Anna Balmer Myers