A business park called Grove 270 sits where Steve Miller once tarried too long onstage. Montgomery County’s musical past to be explored at upcoming history conference |John Kelly |January 18, 2022 |Washington Post 

They were all made by monks, and from memory; for the monks did not tarry. Twain's Conversations with Satan |Mark Twain |April 26, 2009 |DAILY BEAST 

If I could tarry four or five days longer I could set it to work before going to London. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Much as we wished to tarry in this vicinity, our time was so limited that we were compelled to hasten on. British Highways And Byways From A Motor Car |Thomas D. Murphy 

The east pier, as far as it was dry, was covered by the fashion and beauty—as well as by the fishy and tarry—of the town. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

If thou be not a buyer of gold, nor a vendor of silver, tarry not at my door; I have no time for beggars. Kari the Elephant |Dhan Gopal Mukerji 

Black Hoof was there with a large force, but he could not tarry without leaving the Scioto towns uncovered. A Virginia Scout |Hugh Pendexter