The company has been working on a lot of this stuff already and clearly saw an opportunity to capitalize on it in mask form. LG is releasing a ‘wearable air purifier’ |Brian Heater |August 27, 2020 |TechCrunch 

Inspired by the rapid development of AI tools like AlphaZero — the DeepMind program that can defeat humans at chess, Go and shogi — Szegedy’s group wants to capitalize on recent advances in language recognition to write proofs. How Close Are Computers to Automating Mathematical Reasoning? |Stephen Ornes |August 27, 2020 |Quanta Magazine 

While the social network remains focused on keeping its ad revenue intact as the crisis unfolds, it is also trying to capitalize on the surge in online shopping that’s currently taking place in the midst of it. As online shopping intensifies, e-commerce marketers are becoming increasingly reliant on Facebook’s ads |Seb Joseph |August 25, 2020 |Digiday 

Both countries are in deep recessions and had to try to balance the need to keep people safe with the desire to try and capitalize on a lucrative summer season. Europe is at a turning point as COVID cases spike, and fragile governments feel the heat |Bernhard Warner |August 20, 2020 |Fortune 

Even though almost everybody in the league was a person whose whole life and job was to observe cultural trends and capitalize on them. The Economics of Sports Gambling (Ep. 388 Rebroadcast) |Stephen J. Dubner |August 20, 2020 |Freakonomics 

Someone was sure to capitalize on the Ebola panic, and Dr. Joseph Alton is that guy. The Sham, Scaremongering Guide to Ebola |Abby Haglage |November 20, 2014 |DAILY BEAST 

With the CPRIT investigation ongoing, Lehmberg got into trouble of her own - and it seemed Perry was trying to capitalize on it. Rick Perry’s Plan to Defang State Watchdog Unit |Olivia Nuzzi |August 23, 2014 |DAILY BEAST 

She only likes signing controversial clients, and relishes being able to capitalize on a scandal. Meet the PR Guru for the ‘Hot Convict,’ the Octomom, and Every Other D-List Trainwreck |Erin Cunningham |July 17, 2014 |DAILY BEAST 

“My job was to assess their fear and then harp on that fear, capitalize on that fear and get them to buy,” said Maddox, 33. ‘Degree Mills’ Are Exploiting Veterans and Making Millions Off the GI Bill |Aaron Glantz |June 28, 2014 |DAILY BEAST 

But in order to succeed, the band of ministerial mavericks needs to capitalize on their momentum. Argentina’s Drag & Drop Democracy |Jeff Campagna |March 12, 2014 |DAILY BEAST 

The grocer becomes a peer of France, artists capitalize their money, vaudevillists have incomes. Unconscious Comedians |Honore de Balzac 

Among other things, it required that Clemens should not only complete the machine, but promote it, capitalize it commercially. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 

I don't know just yet how much it is goin' to take to capitalize you, but here's ten dollars for an option. The Skipper and the Skipped |Holman Day 

Crœsus was a very rich man, but he was not a capitalist, for he could do anything with his wealth except capitalize it. Contemporary Socialism |John Rae 

Capitalize the pronoun I, the interjection O, titles that accompany a name, and abbreviations of proper names. The Century Handbook of Writing |Garland Greever