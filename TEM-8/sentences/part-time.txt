Carillo has worked full time in banquet services at the local Sheraton and part time at a nearby DoubleTree hotel for years. Laid-off hotel employees fight for ‘right to return’ to work |Ovetta Wiggins |September 26, 2021 |Washington Post 

While most students across the region were relegated to online instruction alone for the last year, some schools did open at least part time – even at the secondary level. 5 Takeaways on Schools’ Coronavirus Aid Spending |Ashly McGlone |April 26, 2021 |Voice of San Diego 

Three servers have stayed on “super part time” to work as delivery drivers. Restaurants Avoiding Big Delivery Apps Have to Get Creative |Kristen Hawley |February 1, 2021 |Eater 

The move to Austin from Redwood City “means that many of our employees can choose their office location as well as continue to work from home part time or all of the time,” Oracle said Friday in a regulatory filing. Oracle moves its headquarters to Texas, adding to Silicon Valley exodus |Verne Kopytoff |December 11, 2020 |Fortune 

It started as part time about a decade ago and is on track to break $2 million in sales this year, nearly doubling sales since 2018. After reimagining apartment storage, Bradyl bloomed |Thomas Heath |November 22, 2020 |Washington Post 

Since the 1950s, fluoride has adapted itself to the prevailing concerns of the time. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Added to drinking water at concentrations of around one part per million, fluoride ions stick to dental plaque. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

But give the Kingdom credit for its sense of mercy: The lashes will be administered only 50 at a time. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

“I think for trans men who are dating every time they hook up they have another coming out,” Sandler said. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

Music is a huge part of the tone of Black Dynamite overall—going back to the original 2009 movie on which the series is based. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

It ended on a complaint that she was 'tired rather and spending my time at full length on a deck-chair in the garden.' The Wave |Algernon Blackwood 

The vision—it had been an instantaneous flash after all and nothing more—had left his mind completely for the time. The Wave |Algernon Blackwood 

About this time the famous Philippine painter, Juan Luna (vide p. 195), was released after six monthsʼ imprisonment as a suspect. The Philippine Islands |John Foreman 

I hate to be long at my toilette at any time; but to delay much in such a matter while travelling is folly. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

On the upper part of the stem the whorls are very close together, but they are more widely separated at the lower portion. How to Know the Ferns |S. Leonard Bastin