Before adjourning, the outgoing members got through the consent agenda as well as items certifying the election results and mid-year capital improvement project budget adjustments, said Dave Rolland, the City Council communications director. Morning Report: The Faulconer Doctrine on Homelessness, Explained |Voice of San Diego |December 9, 2020 |Voice of San Diego 

As the Senate adjourned Monday for a pre-election break, senators left the Capitol—very likely taking any hopes of an imminent stimulus agreement with them. Stimulus update: As Senate adjourns, hopes for a pre-election stimulus agreement evaporate |Anne Sraders |October 27, 2020 |Fortune 

Results of the investigation were not ready for lawmakers before the Legislature adjourned in March due to the coronavirus. Maine Hires Lawyers With Criminal Records to Defend Its Poorest Residents |by Samantha Hogan, The Maine Monitor, with data analysis by Agnel Philip |October 6, 2020 |ProPublica 

It comes with the House days away from adjourning through the election, and with the two sides still at odds on key issues including aid to cities and states, liability protections for businesses, and the overall cost of the bill. Pelosi, Mnuchin meet on economic relief deal as both sides express optimism |Erica Werner, Jeff Stein, Rachael Bade |September 30, 2020 |Washington Post 

Congress is supposed to adjourn at the end of next week through the election, although lawmakers could be called back to vote on a deal. Pelosi abruptly shifts course, restarts relief push amid signs economy is straining |Erica Werner, Rachael Bade |September 24, 2020 |Washington Post 

[H]e may adjourn them to such Time as he shall think proper. Obama’s Congressional Test |Michael Tomasky |August 10, 2011 |DAILY BEAST 

Boehner turned the vote to adjourn into a proxy battle over the tax cuts, and Speaker Pelosi won by a mere 210 votes to 209. The GOP's Secret Weapon |Reihan Salam |October 1, 2010 |DAILY BEAST 

The thought of making anything good enough to sell was inspiring, and they worked with a will till it was time to adjourn. The Story of the Big Front Door |Mary Finley Leonard 

Now, without further comment, I move that this Congress adjourn sine die. Proceedings of the Second National Conservation Congress |Various 

Now, I am going to propose that when you adjourn, it be to meet at Court Square, to-morrow morning at nine o'clock. The Trial of Theodore Parker |Theodore Parker 

I move that the Conference adjourn until half-past seven o'clock this evening. A Report of the Debates and Proceedings in the Secret Sessions of the Conference Convention |Lucius Eugene Chittenden 

The motion to adjourn was agreed to; ayes 17, noes 3, and the Conference adjourned. A Report of the Debates and Proceedings in the Secret Sessions of the Conference Convention |Lucius Eugene Chittenden