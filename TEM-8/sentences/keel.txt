You have to understand and bounce back from those challenges and stay pretty even keel by using your energy to have resilience. Sonos CEO Patrick Spence Sounds Off About Big Tech Behaving Badly |Joann S. Lublin |July 24, 2022 |Time 

Melfi is the even keel to Tony’s volatility — a thoughtful, occasionally horrified audience surrogate. Pandemic TV hero Lorraine Bracco on why ‘The Sopranos’ was a quarantine hit and that house in Sicily she bought for a buck |Jessica M. Goldstein |April 23, 2021 |Washington Post 

They sense that their ship of state is no longer on an even keel. New Hope for Mideast Peace |Martin Indyk |July 7, 2010 |DAILY BEAST 

But, as the keel of the boats touched bottom, each boat-load dashed into the water and then into the enemy's fire. Gallipoli Diary, Volume I |Ian Hamilton 

Robert was out there under the shed, reclining in the shade against the sloping keel of the overturned boat. The Awakening and Selected Short Stories |Kate Chopin 

To maintain the vessel on an even keel he introduced four vanes, called “hydroplanes,” for regulating the depth of descent. The Wonder Book of Knowledge |Various 

The tip often forms an abrupt angle with the shaft and there is a keel on the dorsal surface of the tip (see figs. 5, 6). Genera and Subgenera of Chipmunks |John A. White 

Microscopic examination reveals that there is a faint keel on the dorsal surface of the tip. Genera and Subgenera of Chipmunks |John A. White