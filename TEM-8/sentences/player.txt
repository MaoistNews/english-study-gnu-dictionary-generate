In every turn of the game, both players flip over and reveal the top card of their deck. Can You Cover The Globe? |Zach Wissner-Gross |August 28, 2020 |FiveThirtyEight 

His players considered boycotting a playoff game in 2014 after audio tapes featuring former owner Donald Sterling were revealed. Athletes, from the NBA to tennis stars, are striking to protest the police shooting of Jacob Blake |kdunn6 |August 27, 2020 |Fortune 

Breanna Stewart has been, by a pretty clear measure, the most valuable player in the league as a whole so far. There’s No WNBA All-Star Game This Year, But We Picked The Rosters Anyway |Howard Megdal |August 26, 2020 |FiveThirtyEight 

If players have gotten sick of one another, expect it to only get worse in the coming weeks. Fighting Didn’t Stop In The NHL Bubble |Josh Planos |August 26, 2020 |FiveThirtyEight 

In the first, a player takes actions, such as moving animals to let them mate or obtaining money. The board game Endangered shows just how hard conservation can be |Sarah Zielinski |August 21, 2020 |Science News 

Dora is seen getting dressed as a mermaid by a cursor being manned by some omniscient game player. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

By setting no goals, the player must find their own purpose. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

And that gets to the heart of what makes the game so incredible: By staying silent, it turns the player into the game master. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

When he is awarded Player of the Match while competing for India in England, he is given champagne at the ceremony. The Story of the World’s Greatest Cricket Player |William O’Connor |December 24, 2014 |DAILY BEAST 

His accuser was smeared and demeaned, and a star football player was allowed to keep on playing. Jameis Winston Cleared of Rape Like Every Other College Sports Star |Robert Silverman |December 22, 2014 |DAILY BEAST 

The player to his right holds eight, the player to his left has only six—the right side wins, the left side loses. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The croupier pushes the seven hundred and forty pounds of the unlucky player a foot nearer to the bank. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

But "the cards never forgive," and as a rule Dame Fortune is relentless to the reckless player. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

However, I have felt some comfort in knowing that it is not Liszt's genius alone that makes him such a player. Music-Study in Germany |Amy Fay 

In the conservatory he seemed to be a very passionate player; but, somehow, in public that was not the case. Music-Study in Germany |Amy Fay