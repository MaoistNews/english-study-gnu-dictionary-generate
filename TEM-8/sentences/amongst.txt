Industry experts claim an increase in awareness amongst men when it comes to styles, design, and price regarding their underwear. Would You Pay $100 For a 50 Cent Bulge? Men’s Undies Get Expensive |James Joiner |December 23, 2014 |DAILY BEAST 

For those unfamiliar, soba is buckwheat noodle dish—and they proved much more popular amongst the public. Inside The World’s 10 Oldest Restaurants |Justin Jones |December 20, 2014 |DAILY BEAST 

Founded by German monks in present-day Old Town Stockholm, Zum Franziskaner has become a legend amongst locals and tourists. Inside The World’s 10 Oldest Restaurants |Justin Jones |December 20, 2014 |DAILY BEAST 

No wonder video chat seems to be most popular amongst grandparents who want to see their grandchildren. Why Every Home Needs a Drone This Holiday |Charlie Gilbert |December 8, 2014 |DAILY BEAST 

This way, the costs are spread amongst those who wish to own guns. A Navy Vet’s Case for Gun Control |Shawn VanDiver |November 23, 2014 |DAILY BEAST 

To this end they spread a distorted epitome of his favourite views, amongst their retainers. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Around the table were seated about twenty persons, amongst whom the usual sprinkling of sacerdotes was not wanting. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

The fourth year dawned, and Mr Brammel suddenly appeared amongst his friends. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Martini prepared a couple of stout mules, and concealed them amongst the thickets on the opposite side of the fosse. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Smoking now is as common as eating and drinking, and to smoke amongst ladies is a vulgarity. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.