Cotton and bamboo are great for sweaty sleepers, but keep an eye out for brands that specifically tout blankets that will keep you cool throughout the night. Best weighted blanket: Sleep like a baby with our comfy bedding picks |PopSci Commerce Team |February 12, 2021 |Popular-Science 

The university will work to find alternatives, such as reusable straws made from bamboo or snacks with compostable wrappers. George Washington University commits to single-use-plastic ban |Lauren Lumpkin |February 11, 2021 |Washington Post 

All of our pieces are thoughtfully designed using earth-friendly fabrics like bamboo, cupro, and post-consumer plastic bottles, and come from suppliers that have Oeko-Tex and Global Recycled Standard certifications. The future of maternity workwear is all in the details |Rachel King |January 31, 2021 |Fortune 

By age 1, he will start eating more solid foods such as bamboo, carrots, apples and “nutrient-rich biscuits.” Baby panda makes debut — online — at National Zoo |Dana Hedgpeth, Justin Wm. Moyer |January 27, 2021 |Washington Post 

These bamboo organizers are both functional and elegant, just like traditional shoji. Marie Kondo is back with a new collaboration to keep you organized while staying at home |Rachel King |January 11, 2021 |Fortune 

The young man weaves through clusters of bamboo and cuts a diagonal slash into a tree, positioning a hollow log at the end. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

Some facilities had begun erecting “bamboo hand washing stations” at the entrance to hospitals, but not enough. $10,000 a Month for Ebola Fighters |Abby Haglage |October 7, 2014 |DAILY BEAST 

His early works were mostly high-end residences, and he often used bamboo-like paper tubes in these designs. Shigeru Ban: Triumph From Disaster |Nate Berg |August 31, 2014 |DAILY BEAST 

When she was young, the homes were made of bamboo and there were no cars or motorbikes. A Little Too Off the Beaten Path in Burma |Katya Cengel |June 2, 2014 |DAILY BEAST 

Tell that to the nearby pandas, happily ensconced with their bamboo, magnificently ignoring everything. How to Catch a Depressed Gorilla, Japanese-Style |Tim Teeman |February 6, 2014 |DAILY BEAST 

The doors were made of wood, though in many houses paper or plaited bamboo was used. Our Little Korean Cousin |H. Lee M. Pike 

He swims every day in the river; he fishes from his bamboo raft; he hunts in the forest with his father. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

Suppose for one instant that the bamboo should give way under the boy's feet or failed to hold in the tree-top! Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

All the bamboo clappers, cocoanut shells, tin pans, and red flags that could be found were seized and put into use. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

He carries a bamboo basket in which he has put a mixture containing a curious kind of poison. Alila, Our Little Philippine Cousin |Mary Hazelton Wade