During his career, he had served his country as consul in Italy for many years, and he had learned the language. Life Always Wins. Follow Me - Issue 104: Harmony |Stefano Mancuso |July 28, 2021 |Nautilus 

The consul had led me through a magnificent garden—whose name I unfortunately don’t remember—to “meet” three trees that had survived the bomb. Life Always Wins. Follow Me - Issue 104: Harmony |Stefano Mancuso |July 28, 2021 |Nautilus 

No one would have guessed it from looking at him, but the consul was well over 80 years old, and he had had a lot to drink. Life Always Wins. Follow Me - Issue 104: Harmony |Stefano Mancuso |July 28, 2021 |Nautilus 

In the early 1990s he was British trade commissioner to China, and later first secretary at the British embassy in Beijing, and consul for Macau. Richard Graham, the politician fortifying the UK-China trade relationship |Annabelle Timsit |May 18, 2021 |Quartz 

“The Syrian war is having its effects here as well,” said Yehyavi, the Iranian consul general in Quetta. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

The U.S. position is that as a consul, Khobragade was not immune from arrest for allegedly under-paying her maid. Husain Haqqani on America’s Diplomat Shame |Husain Haqqani |December 19, 2013 |DAILY BEAST 

The Japanese consul in Alexandria was sending the Germans reports on the movement of the Mediterranean Fleet. Week in Death: The Woman Who Cracked Hitler’s Codes |The Telegraph |November 17, 2013 |DAILY BEAST 

The consul was a keen golfer, so Cunningham ostentatiously visited the clubhouse with his clubs and an overnight bag. Week in Death: The Woman Who Cracked Hitler’s Codes |The Telegraph |November 17, 2013 |DAILY BEAST 

But the consul denied there had been any kind of direct Sandinista pressure to cut off funding for the group. Nicaragua’s President Accused of Sex Abuse by His Stepdaughter |Mac Margolis |May 20, 2013 |DAILY BEAST 

He was in early life a shipcarpenter, and subsequently American consul at Antwerp. The Every Day Book of History and Chronology |Joel Munsell 

He was further instructed to hand over his consulate archives to the British Consul, who would take charge of American interests. The Philippine Islands |John Foreman 

Scarcely a year passed in which his name was not connected with some conspiracy to overthrow the First Consul. Napoleon's Marshals |R. P. Dunn-Pattison 

Thanks to Massna's warm introduction and his own reputation, he found himself cordially received by the First Consul. Napoleon's Marshals |R. P. Dunn-Pattison 

The relations between Ney and the First Consul soon became closer. Napoleon's Marshals |R. P. Dunn-Pattison