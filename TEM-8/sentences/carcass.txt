If the thalattosaur was a carcass when the ichthyosaur found it, the prey’s limbs would have rotted off before its tail did, the team argues. This ancient reptile’s last meal may have truly been a killer |Maria Temming |October 9, 2020 |Science News For Students 

Botswana first discovered carcasses of elephants along the wildlife rich Okavango Delta in May and June but was authorities were uncertain as to the cause of the mass deaths, leaving scientists and conservationists puzzled. Scientists blame climate change for bacteria that caused the mysterious deaths of 300 African elephants |Tawanda Karombo |September 21, 2020 |Quartz 

Their carcasses took a day or longer to pass through the frogs. Some beetles can be eaten by a frog, then walk out the other end |Jonathan Lambert |September 4, 2020 |Science News For Students 

“The mining space is littered with the carcasses of failed mining efforts,” Silbert acknowledges. There’s (digital) gold in them thar hills: Crypto giant DCG is betting $100 million on mining Bitcoin in North America |Jeff |August 27, 2020 |Fortune 

None of these immobilized beetles survived, and their carcasses took a day or longer to pass through the frogs. Water beetles can live on after being eaten and excreted by a frog |Jonathan Lambert |August 3, 2020 |Science News 

Jeb next found himself as an advisor to Barclays, which had picked through the carcass of what was left of Lehman. Bush, Christie, Romney: Who’ll Be the GOP Class Warrior? |Lloyd Green |December 15, 2014 |DAILY BEAST 

I like to get the soup going using the turkey bones and carcass. Marcus Samuelsson Talks Thanksgiving: Glogg And Berbere-Spiced Turkey |Katie Baker |November 22, 2012 |DAILY BEAST 

Once the bee dies, maggots eat the carcass, turn into zombie flies, and buzz off in search of their next host. When ‘Zombees’ Attack |Winston Ross |October 11, 2012 |DAILY BEAST 

In hunting groups of fewer than 10 people, the average carcass weight per hunter without dogs was 8.4 kilograms per day. Do We Owe Our Existence to the Dog? |David Frum |June 2, 2012 |DAILY BEAST 

His quartered carcass was impaled above other London city gates. The Real Story of "O" |Robert McCrum |January 23, 2011 |DAILY BEAST 

Then he just kept triggering until the gun was emptied and he had put five slugs fatally into Big Sid's carcass. Hooded Detective, Volume III No. 2, January, 1942 |Various 

In making tires, the strips of fabric are built together about a steel core to form the body or carcass of the tire. The Wonder Book of Knowledge |Various 

That white Injun beside you will be one of the first to stick burning splinters into your carcass. A Virginia Scout |Hugh Pendexter 

Uncle Will and Phil set to work to cut up the carcass, first removing the hide, which the former wished to preserve. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

A practical-minded man, he preferred to owe the safety of his carcass to his rival rather than have it impaled on Apache lances. Overland |John William De Forest