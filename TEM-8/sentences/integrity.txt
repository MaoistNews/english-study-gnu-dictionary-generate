The move was intended to give researchers time to examine safety data while maintaining the integrity of the trials, the company said. Some scientists downplay significance of AstraZeneca’s COVID-19 vaccine trial halt |Claire Zillman, reporter |September 9, 2020 |Fortune 

The Hatch Act and other rules like it aren’t just about obscure regulations or Washington norms — they safeguard the values of election integrity and good governance. The GOP Convention Violated Plenty Of Norms, But Did It Undermine Democratic Values? |Julia Azari |September 1, 2020 |FiveThirtyEight 

Gambling certainly does have the possibility to undermine the integrity of sports. The Economics of Sports Gambling (Ep. 388 Rebroadcast) |Stephen J. Dubner |August 20, 2020 |Freakonomics 

We believe that many of the lawsuits they have initiated would destroy the integrity of our elections, so we’re fighting back. The Republican Choice |Clare Malone (clare.malone@fivethirtyeight.com) |June 24, 2020 |FiveThirtyEight 

One idea that’s been pitched is a so-called “integrity fee” — an incentive to keep the matches clean. Inside the Sports-Industrial Complex (Ep. 364) |Stephen J. Dubner |January 24, 2019 |Freakonomics 

But the qualities Mario Cuomo brought to public life—compassion, integrity, commitment to principle—remain in short supply today. President Cuomo Would’ve Been a Lion |Jonathan Alter |January 2, 2015 |DAILY BEAST 

The Supreme Court justices who decided the Integrity case make $244,440 a year (Chief Justice Roberts makes $255,500). How Amazon Became Santa’s Sweatshop |Sally Kohn |December 11, 2014 |DAILY BEAST 

This story is part of an ongoing project by InsideClimate News and the Center for Public Integrity. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

Other artisans see purity and integrity in their craft and have no dream of becoming recognized as the author of their own work. Damien Hirst’s Army of Geppettos |Tim Teeman |December 2, 2014 |DAILY BEAST 

UVA prides itself on holding all students to a high standard of accountability and integrity. How UVA Is Failing Its Women |Allison McNearney |November 20, 2014 |DAILY BEAST 

Thus will my beloved country be governed, without detriment to the integrity of Spain. The Philippine Islands |John Foreman 

He possessed a mind of great vigor and enterprise, and was characterized by integrity and amiableness. The Every Day Book of History and Chronology |Joel Munsell 

Still, Napoleon knew his devotion, his blind obedience to orders, and his absolute integrity. Napoleon's Marshals |R. P. Dunn-Pattison 

The courage of Vergniaud was above suspicion, and his integrity above reproach. Madame Roland, Makers of History |John S. C. Abbott 

Could she obtain a triumphant acquittal, through the force of her own integrity, she would greatly exult. Madame Roland, Makers of History |John S. C. Abbott