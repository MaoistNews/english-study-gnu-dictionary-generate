In 2015, researchers found that British English speakers are using lilac and turquoise as basic colors. Is the sky really blue? It depends on what language you speak |Kathryn Hulick |March 17, 2022 |Science News For Students 

The smells of spruce and lilac from overhanging branches soak my nostrils. Chronic Illness Had Me Stuck in Grief. My E-Bike Helped Me Find Joy Again. |mmirhashem |October 11, 2021 |Outside Online 

Like the “misty dream” and the falling lilac petals she sings of, IU’s timbre echoes the transient quality of memories. The Best K-Pop Songs of 2021 So Far |Kat Moon |July 1, 2021 |Time 

Sky and soft sunlight tint the snow blue, pink, lilac, peach. Visiting the Arctic Circle…Before It’s Irreversibly Changed |Terry Greene Sterling |April 1, 2014 |DAILY BEAST 

As the sky turned lilac, we sat at a low circular table and looked out over the palm groves of Skoura. On Foot in the High Atlas Mountains of Morocco |Joanna Eede |January 22, 2014 |DAILY BEAST 

We reached our last camp as lighting pulsed through lilac-dark clouds over the Kenyan hills. Walking With Wildebeests: Exploring the Serengeti on Foot |Joanna Eede |July 9, 2013 |DAILY BEAST 

Trim trousers in nubby shades of lilac were paired with jackets in iridescent hues of melon. Paris Fall 2012 Fashion Week: Haider Ackermann, Lanvin, and Comme des Garçons |Robin Givhan |March 4, 2012 |DAILY BEAST 

One of these collaborations includes a lilac hourglass-shaped Tadashi gown in which Spencer collected her very first Golden Globe. Octavia Spencer: Nominated for ‘The Help’ and Star of the Red Carpet |Justine Harman |February 23, 2012 |DAILY BEAST 

He muttered something about his gray hairs, but came along after an amused glance at Lilac and Konnel. Fee of the Frontier |Horace Brown Fyfe 

Even Konnel had a small pile before him, although he seemed to be losing some of Lilac's attention to Meadows. Fee of the Frontier |Horace Brown Fyfe 

"And less of the 'lady' business outa you," said Lilac, but low enough to keep it private. Fee of the Frontier |Horace Brown Fyfe 

She had lived twenty-five years up-stairs and down-stairs in that white house with the lilac shrubbery and low iron fence. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

Right near the bay window, in the thick lilac tree, Marmaduke spied Red Robin's nest. Seven O'Clock Stories |Robert Gordon Anderson