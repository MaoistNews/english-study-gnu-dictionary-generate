An alternative option is to leave the road itself alone, and instead string charging wires above the road that can charge trucks in much the same way urban trams are powered. Magnetizable Concrete in Roads Could Charge Electric Cars While You Drive |Edd Gent |August 9, 2021 |Singularity Hub 

This is a moderate backpacking trip, but can easily be done solo—park at the Leigh Lake Trailhead and take the tram to start at the Granite Canyon trailhead. Ten Ideal Hikes for the Solo Adventurer |syadron |July 30, 2021 |Outside Online 

That’s more of a sort of wireless tram, though, to be fair, compared to what’s being proposed in Indiana. Indiana wants magnetic highways that wirelessly charge electric vehicles |Purbita Saha |July 23, 2021 |Popular-Science 

Be sure to book your tram ticket early if you want to soar to the top of Gateway Arch—they sell out quickly on weekends and during summer months. The Best Hipcamp for Every National Park |Emily Pennington |May 19, 2021 |Outside Online 

The resort is also considering a new boot-pack route to offer skiers and riders a way to climb Lone Peak from the top of Dakota lift, bypassing the tram. What This Ski Season Will Look Like |Megan Michelson |October 6, 2020 |Outside Online 

Eva and Adele, the Art Couple, were on my tram, both in high-collared baby-pink dresses. Live From Art Basel |Anthony Haden-Guest |June 17, 2010 |DAILY BEAST 

By day you'll be coerced to hike "the Peak" (I like the tram, thank you) for a quiet view of Kowloon. Gal With a Suitcase |Jolie Hunt |January 16, 2010 |DAILY BEAST 

Luckily, public transport (the tram) is brilliantly efficient, cost-effective, and blissfully above ground. The Breathtaking Mosques of Istanbul |Jolie Hunt |January 9, 2010 |DAILY BEAST 

Tram cars often ran along the middle of the street, with barely room for a vehicle to pass on either side. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Tram cars were numerous and children played everywherePg 140 with utter unconcern for the vehicles which crowded the streets. British Highways And Byways From A Motor Car |Thomas D. Murphy 

In London, for instance, certain tramway companies double the tram-fares on Sundays. Friend Mac Donald |Max O'Rell 

She walked to Merrion Gates along the tram line about four miles, when she was stopped by sentries. The Sinn Fein rebellion As I Saw It. |Mrs. Hamilton Norway 

How often I would watch some tram-car, some string of barges go from me slowly out of sight. The New Machiavelli |Herbert George Wells