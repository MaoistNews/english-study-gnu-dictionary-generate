I’ve endured spiteful comments, like the ones I read at Outside that day, throughout my entire life, from being bullied in school to being spit on and harassed in the streets by random strangers. What I Learned from Writing Outside’s Pride Newsletter |awise |August 26, 2021 |Outside Online 

With pop-punk intensity, she sing-talks her way through a song that’s unapologetically bitter and spiteful, with a guitar-driven chorus that just asks for a cathartic singalong. The Best Songs of 2021 So Far |Raisa Bruner |May 26, 2021 |Time 

For example, at ages 8 and 9, children made spiteful choices 41 percent of the time with in-group members and 44 percent of the time with out-group members, as compared with 17 versus 33 percent for 12- and 13-year-olds. Was the Golden Rule Born in the Mind of a Monkey? - Facts So Romantic |Julie Sedivy |March 4, 2021 |Nautilus 

What remains is just bigotry, and probably a spiteful resistance to being seen as caving in to the relativists. The Grotesque Ban On Gays In New York’s St Patrick’s Day Parade |Michael Tomasky |March 17, 2014 |DAILY BEAST 

He describes himself on a train platform in Hanover, spiteful and sexually frustrated, throwing coins on the floor. In Defense of Jonathan Franzen |Michelle Goldberg |September 26, 2013 |DAILY BEAST 

Subordinated as it is here rewritten, it does not half express the spiteful independence she assumed to teach Coppy a lesson. English: Composition and Literature |W. F. (William Franklin) Webster 

This was the first time he had smarted in his penetrable part—the skin—and it made him very spiteful. It Is Never Too Late to Mend |Charles Reade 

This sentence evidently cannot mean that a father may refuse food to his son if the latter is spiteful. My Religion |Leo Tolstoy 

He was not only terrified but angered, and whirling about, he brought down his gun with spiteful violence on the writhing body. Two Boys in Wyoming |Edward S. Ellis 

No one has yet enjoyed any spiteful fun at Mrs. Depew's expense though many were on the qui vive for entertainment. The Onlooker, Volume 1, Part 2 |Various