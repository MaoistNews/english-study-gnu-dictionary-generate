The seagulls expected to clean up the rest were also scared off, but the remaining pieces were so small that it would not have been worth the bother anyway, he added. Why the Forest Service has a manual for blowing up horse carcasses |David Roza/Task &amp; Purpose |August 4, 2022 |Popular-Science 

Musk says he wants to provide a “superintelligence layer” in the human brain to help protect us from artificial intelligence, and Zuckerberg reportedly wants users to upload their thoughts and emotions over the internet without the bother of typing. Can Machines Control Our Brains? |R. Douglas Fields |May 17, 2021 |Quanta Magazine 

I would go from being a charmingly eccentric bohemian to being a monstrously crass bother. Such a pretty face |Aubrey Gordon |February 26, 2021 |Vox 

But if the goal is to not interact with people, why bother going to a bar in the first place? How Much Do You Tip A Robot Bartender? |Kayleigh Kulp |October 25, 2014 |DAILY BEAST 

A soldier asks all the men to come off the bus, but only half do, and he decides not to bother with rest. On the Bus: Ukraine’s Frontline Express Across the Battle Lines |Ted Phillips |September 8, 2014 |DAILY BEAST 

There are no signs of any police—why would the traffic cops bother, there is no one to fine and demand bribes from? As the Key Battle Looms, a Report from Ukraine's Front Lines |Jamie Dettmer |August 13, 2014 |DAILY BEAST 

Since there's a near infinite number of possible exercises, Amiigo doesn't bother trying to categorize them ahead of time. Amiigo Tracker Pairs Data With Deadlifts |Gregory Ferenstein |August 4, 2014 |DAILY BEAST 

As mildly irritating as David Tseng may be, he is not someone who troubles me enough to even bother with. Morrissey Denies Ordering Hit on Fan |Tim Teeman |July 31, 2014 |DAILY BEAST 

But I don't suppose Weston would bother spreading the tails out when he sat down. The Soldier of the Valley |Nelson Lloyd 

Great tracts of land in this part of the State are out of date, and more bother than they are worth, anywhere. Ancestors |Gertrude Atherton 

Baroudi would probably never think of her as Englishmen thought of her, would never "bother about" her age. Bella Donna |Robert Hichens 

He ceased to bother his brain with Bascomb and his affairs, wrapping himself completely in the noble work of the roadster. Motor Matt's "Century" Run |Stanley R. Matthews 

And a drop of poteen is a wonderful thing to drive away the melancholy thoughts that haunt and bother so many of us. The Whale and the Grasshopper |Seumas O'Brien