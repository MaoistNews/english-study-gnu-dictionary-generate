Bridgewater, which fought the panel’s decision that it must pay the Tekmerion founders’ legal fees, has since settled the case. The losses continue to pile up for hedge fund king Ray Dalio |Bernhard Warner |September 15, 2020 |Fortune 

Researchers have generally settled for repeatedly measuring flow speed at several points in the turbulence. An Unexpected Twist Lights Up the Secrets of Turbulence |David H. Freedman |September 3, 2020 |Quanta Magazine 

They don’t jump back in and trade until mid-November once the dust has settled. The markets rally is this close to becoming the ‘greatest of all time’ |Bernhard Warner |September 2, 2020 |Fortune 

As businesses have settled into this new normal, they’ve also been looking inward. Deep Dive: How companies and their employees are facing the future of work |Digiday |September 1, 2020 |Digiday 

As the realization settles in that the pandemic will stretch into multiple quarters rather than multiple months, CEOs must again grapple with how to advise their employees on returning to the office. I’m a physician and a CEO. Why I won’t bring my employees back to the office before Labor Day 2021 |matthewheimer |August 26, 2020 |Fortune 

I settle for a sweater and jacket and throw a tie in my briefcase just in case it turns out to be the prom. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

I started to squirm in my chair and Jimbo put his hand back on my shoulder to settle me down. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

The pressure is on the Supreme Court to settle this once and for all. Gay Marriage Chaos Begins |Jay Michaelson |November 11, 2014 |DAILY BEAST 

Now they either settle, which they should have done 20 years ago, or they go in front of the judge. In Tussle Over Will, Mistress’s Family Takes a Bite Out of NYU |Anthony Haden-Guest |November 10, 2014 |DAILY BEAST 

They form a daily currency by which we settle relationships, but they also create doubt. Wonder Woman’s Creation Story Is Wilder Than You Could Ever Imagine |Tom Arnold-Forster |November 3, 2014 |DAILY BEAST 

Having accomplished his task within three months Datto Mandi withdrew with all his men, except two who wished to settle at Pardo. The Philippine Islands |John Foreman 

As night began to settle down over the land, the Queen Elizabeth seemed to feel the time had come to give full vent to her wrath. Gallipoli Diary, Volume I |Ian Hamilton 

And we will also settle and assure the particular rights and interests of every planter and adventurer. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

In fact, one evening, his wife and children deserted him entirely and went to settle elsewhere, thinking it was all over with him. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Many people when presented with the argument above, would settle it at once with the word "socialism." The Unsolved Riddle of Social Justice |Stephen Leacock