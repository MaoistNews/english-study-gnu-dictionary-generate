A very lightweight option at only one pound, the Pendaflex file box is by far the most easily transportable. Great filing cabinets for your home office |PopSci Commerce Team |September 17, 2020 |Popular-Science 

We don’t have to box ourselves in to our present limitations. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

It’s thinking outside the box and using the current technology that we already have. Malala Yousafzai tells the business community: Education is the best way to guard against future crises |Michal Lev-Ram, writer |September 15, 2020 |Fortune 

It also includes a USB-C to USB-C charging cable in the box. Grab a recommended 90W charger for your phone, Switch, and laptop for $43 |Ars Staff |September 11, 2020 |Ars Technica 

Variety reported that box office revenue in South Korea was down 30 to 40 percent in January 2020 compared to previous years. How the coronavirus outbreak is roiling the film and entertainment industries |Alissa Wilkinson |September 11, 2020 |Vox 

In response to the screen quota cut, South Korea established a “cinema tax” on the box office. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

He goes into some detail into what it took to persuade voters to pass marriage equality at the ballot box in four states in 2012. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

How to Train Your Dragon 2, the tenth highest grossing movie in 2014 America, made $22 million at the Korean box office. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

By 2012, the marriage equality movement had won in courts and legislatures—but not at the ballot box. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

That November, many of us were stunned as voters in four states supported marriage equality at the ballot box. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

Beside her was a box of bonbons, which she held out at intervals to Madame Ratignolle. The Awakening and Selected Short Stories |Kate Chopin 

Now and then the boy who had bought Squinty, and who was taking him home, would look around at his pet in the slatted box. Squinty the Comical Pig |Richard Barnum 

The little pig in the box felt himself being lifted out of the wagon. Squinty the Comical Pig |Richard Barnum 

Mrs. Newbolt was cutting splints for her new sun-bonnet out of a pasteboard box. The Bondboy |George W. (George Washington) Ogden 

With a hammer the boy knocked off some of the slats of the small box in which Squinty had made his journey. Squinty the Comical Pig |Richard Barnum