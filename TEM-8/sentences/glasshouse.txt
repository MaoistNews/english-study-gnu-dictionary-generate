When the glasshouse site was excavated in 1948 only small fragments and drippings—dark green in color—were found. A Pictorial Booklet on Early Jamestown Commodities and Industries |J. Paul Hudson 

On the way thither an old-fashioned yew hedge may be seen round about a vast glasshouse. Nature Near London |Richard Jefferies 

Daisy said it was a place in Glasshouse Street for which she had no very great affection. Sinister Street, vol. 2 |Compton Mackenzie 

All the glasshouse was spanned and arched over with one beautiful vine. Bliss, and Other Stories |Katherine Mansfield 

A long-established legend that beads were manufactured at the Jamestown glasshouse is without archeological evidence. Contributions From the Museum of History and Technology |Ivor Noel Hume