The financial terms were largely symbolic as Purdue Pharma’s bankruptcy has put it deeply in debt and its earnings from OxyContin have plunged. McKinsey, adviser to businesses around the world, agrees to pay $573.9 million to settle charges for its role in opioid epidemic |Peter Whoriskey, Christopher Rowland |February 5, 2021 |Washington Post 

Sadly, Puerto Rico is an island full of symbolic laws, which are usually ignored by law enforcement authorities and have no consequences. Puerto Rico: Number one in hate crimes |Alberto J. Valentín |February 2, 2021 |Washington Blade 

Closing off the Capitol would be an enormous symbolic victory for the insurrectionists. The danger of right-wing mobs is real. Fencing at the U.S. Capitol won’t help. |Philip Kennicott |February 1, 2021 |Washington Post 

Wednesday’s Board of Supervisors meeting is symbolic in that the county is essentially opening the floor to new supervisors and the public who have other ideas to reach this target. County’s New Climate Plan Could Use the System That Doomed the Old Plan |MacKenzie Elmer |January 13, 2021 |Voice of San Diego 

Tokyo 2020 will be symbolic to prove that people, all together from across the world, have defeated the virus. Can Tokyo Safely Host the Olympic Games This Summer? |Charlie Campbell |January 12, 2021 |Time 

Bridging the divide between the police and those who distrust them will take more than protests and symbolic gestures. How to Solve the Policing Crisis |Keli Goff |January 5, 2015 |DAILY BEAST 

Your death is symbolic of the harsh reality facing so many of us. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

Along the way, he accidentally embeds a nail in his foot, which is not symbolic at all. The Walking Dead’s ‘Crossed’: The Stage Is Now Set for a Bloody, Deadly Midseason Finale |Melissa Leon |November 24, 2014 |DAILY BEAST 

But the amendment was non-binding -- and thus largely symbolic. Fact-Checking the Sunday Shows: November 16 |PunditFact.com |November 16, 2014 |DAILY BEAST 

She lost that claim long ago, but her symbolic importance has only grown. 128 Years Old and Still a Looker: Happy Birthday to Lady Liberty |Elizabeth Mitchell |October 28, 2014 |DAILY BEAST 

In like manner the mouth, from being a bare symbolic indication, gradually takes on form and likeness. Children's Ways |James Sully 

This element of symbolic indication will be found to run through the whole of childish drawing. Children's Ways |James Sully 

The celebrant sprinkled the victim with wine and salted cake, and made a symbolic gesture with the knife. The Religion of Ancient Rome |Cyril Bailey 

Because of its recumbent position, symbolic of General Lee resting on a battlefield cot, this statue is considered most unique. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

If she was no longer a symbolic and sympathetic figure—like that young mother among her children—she had her own claims. Marriage la mode |Mrs. Humphry Ward