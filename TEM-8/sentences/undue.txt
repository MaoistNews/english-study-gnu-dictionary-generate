He still saw nothing of undue concern, according to those involved. The CDC’s failed race against covid-19: A threat underestimated and a test overcomplicated |David Willman |December 26, 2020 |Washington Post 

Beth Milito, the group’s senior executive counsel, said that while small-business owners have been “highly sensitive” to their workers’ needs during the pandemic, mandating paid sick days and extended leave puts an undue burden on them. Worker advocates push to extend COVID-related emergency paid leave program |lbelanger225 |December 18, 2020 |Fortune 

The firm said it will now notify customers and issue credits for undue fees. JPMorgan Chase Bank Wrongly Charged 170,000 Customers Overdraft Fees. Federal Regulators Refused to Penalize It. |by Patrick Rucker, The Capitol Forum |December 14, 2020 |ProPublica 

The Glass-Steagall and BHC acts separated banking from commerce to prevent undue concentrations of financial and economic power and to minimize conflicts of interest in bank lending and investment advice. Why it would be a huge mistake to allow Big Tech firms to acquire banks |jakemeth |November 26, 2020 |Fortune 

We’re talking about the kinds of information that can potentially cause people to take on undue risk. Podcast: How online misinformation murdered the truth |Anthony Green |October 29, 2020 |MIT Technology Review 

Ryan did not go into detail on what an “undue burden” would be. Paul Ryan’s Plan: Rebooting Compassionate Conservatism |Eleanor Clift |July 24, 2014 |DAILY BEAST 

Critics contend that bundlers have undue influence over politicians. Senate Democrats Snag Campaign Cash From Lobbyist-Bundlers |Michael Beckel |June 16, 2014 |DAILY BEAST 

But a seemingly undue reliance on stop-and-frisk in particular caused tensions in minority neighborhoods. My Patrol With the NYPD’s Bill Bratton |Michael Daly |March 14, 2014 |DAILY BEAST 

Are there other areas of American life that may be creating undue strain on the health care system? The Pain Is Not the Problem: How to Fix America’s Health-Care Crisis |Elizabeth Bradley and Lauren Taylor, Elizabeth Bradley, Lauren Taylor |November 29, 2013 |DAILY BEAST 

Placing an undue emphasis on our ‘separateness’ is a step backward. Fringe Factor: Penn. Health Dept. Says Gays Are Like 12 Year Olds |Caitlin Dickson |September 1, 2013 |DAILY BEAST 

In writing K. I try to convey the truth in terms which will neither give him needless anxiety or undue confidence. Gallipoli Diary, Volume I |Ian Hamilton 

Reasonable facilities for receiving and forwarding traffic The subject of undue preference, which was forbiddenp. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

If she ignored his note it would give undue importance to a trivial affair. The Awakening and Selected Short Stories |Kate Chopin 

He saw the sudden change in her, and realised what a supreme effort she was making to betray no undue alarm. The Doctor of Pimlico |William Le Queux 

He had no undue trial of his patience, for a moment later the editor of the Mercury bustled into the room. The Weight of the Crown |Fred M. White