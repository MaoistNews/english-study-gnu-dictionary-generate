You can carry a change of clothes, make-up, a briefcase or big purse, and lunch box in their convenient side racks. Incentivizing E-Bikes Would Be Money Well Spent |Paul Krueger |May 10, 2021 |Voice of San Diego 

I left a metal briefcase, a gift that was sent to me, sitting near a colleague’s desk, unmarked and unattended. The upside of teasing, gossip, and eavesdropping at the office |Lila MacLellan |May 10, 2021 |Quartz 

Put that briefcase away for as long as your out of office response is turned on, and try to drive down different roads when you head into town for errands. Stuck at home? Trick your brain into treating a staycation like the real thing. |Eleanor Cummins |December 22, 2020 |Popular-Science 

She would often buy thoughtful gifts for her colleagues and friends, and just before the pandemic, Pineda said, Campos gave her a briefcase for carrying papers to work. Elsi Mabelicia Campos, devoted mother who kept the Pew building spotless, dies of covid-19 |Samantha Schmidt |December 10, 2020 |Washington Post 

I’m going to get up and put my shoes on and get my briefcase and go back to work. She fell into QAnon and went viral for destroying a Target mask display. Now she’s rebuilding her life. |Travis Andrews |November 11, 2020 |Washington Post 

I settle for a sweater and jacket and throw a tie in my briefcase just in case it turns out to be the prom. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Although in that film, the briefcase contained an atomic bomb. The Secrets of ‘Pulp Fiction’: 20 Things You Didn’t Know About the Movie on Its 20th Anniversary |Marlow Stern |October 19, 2014 |DAILY BEAST 

He is clutching more out of habit than for any practical reason his black briefcase. Inside East Ukraine’s Make-Believe Republics |Jamie Dettmer |May 15, 2014 |DAILY BEAST 

He searches patiently through his briefcase and produces a round-trip ticket for the seat in question. Stacks: Hitting the Note with the Allman Brothers Band |Grover Lewis |March 15, 2014 |DAILY BEAST 

He opens the briefcase to reveal hundreds of well-thumbed sheets of paper filled with typewritten words. The Stacks: The True Greatness of Muhammad Ali |Peter Richmond |February 23, 2014 |DAILY BEAST 

There was something smug about the way he picked up his heels, swung his briefcase. Hooded Detective, Volume III No. 2, January, 1942 |Various 

A stocky man dodged around from behind the woman and came rapidly down the platform, neat, crisp, briefcase under his arm. Hooded Detective, Volume III No. 2, January, 1942 |Various 

He'd probably try to get his foolish little briefcase in front of him like a shield. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Engel called "Fourteen" and got out there, briefcase tightly clutched up under his arm, its flap unbuckled. Hooded Detective, Volume III No. 2, January, 1942 |Various 

He brought himself back with a grimace, aware that he was clutching the briefcase of tapes possessively from long habit. We're Friends, Now |Henry Hasse