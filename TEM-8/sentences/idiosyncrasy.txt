They’ll have new idiosyncrasies that you’ll come to love just as much as the old ones. Saints Row, the funhouse mirror version of Grand Theft Auto, will reboot in 2022 |Gene Park |August 25, 2021 |Washington Post 

Chicago is an idiosyncrasy or an exception to the rule, where the O is left intact and the -an is added afterward. From Chicagoan to Phoenician, here’s what to call the residents of the biggest US cities |John Kennedy |July 13, 2021 |Popular-Science 

So at each stop, I’ll pick up some supplies and can take advantage of a bit of variety and the idiosyncrasies of local offerings, including foods that can’t be sent in the mail, like cheese. Hiking the Appalachian Trail: A Beginner’s Guide |Karen Berger |June 28, 2021 |Outside Online 

Such idiosyncrasies, like the weird complexity and variability of smell, now turn out vital to understanding the brain—how it maneuvers an organism through a landscape of fast-changing molecular combinations. Our Mind-Boggling Sense of Smell - Issue 91: The Amazing Brain |Ann-Sophie Barwich |October 14, 2020 |Nautilus 

While some of these idiosyncrasies can be explained by gravitational interactions in systems with multiple planets, there might be conditions where planets could form in bizarre orbits. Three stars, warped rings may show how planets end up moving backward |John Timmer |September 3, 2020 |Ars Technica 

In hay fever certain patients present a peculiar idiosyncrasy, often inherited, almost always neuroarthritic. The Treatment of Hay Fever |George Frederick Laidlaw 

What reveals perhaps more distinctly than anything else Chopin's idiosyncrasy is his friendship for Titus Woyciechowski. Frederick Chopin as a Man and Musician |Frederick Niecks 

He had a constitutional dislike for falsehoods, which was perhaps not so much a virtue as an idiosyncrasy. The Butterfly House |Mary E. Wilkins Freeman 

They chatted volubly over this idiosyncrasy, and even laughed at it, but quite decorously so that our feelings might be spared. A Woman's Journey through the Philippines |Florence Kimball Russel 

This very singular idiosyncrasy he attributed to a fright when he was an infant in the arms of his nurse. A Mortal Antipathy |Oliver Wendell Holmes, Sr.