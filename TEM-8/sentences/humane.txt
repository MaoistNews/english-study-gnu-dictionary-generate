She also has taken a new approach to some nonviolent offenses that she said would not only be more humane, but would free resources for fighting violent crimes. As Trump Calls for Law and Order, Can Chicago’s Top Prosecutor Beat the Charge That She’s Soft on Crime? |by Mick Dumke |September 4, 2020 |ProPublica 

Pandemic prevention aside, the use of this kind of vaccine could also provide more humane control of rabies, because culling bats is currently the go-to method of keeping the disease at bay in South America. Can Vaccines for Wildlife Prevent Human Pandemics? |Rodrigo Pérez Ortega |August 24, 2020 |Quanta Magazine 

Despite all these efforts, questions remain as to whether biotech can ever dramatically reduce the industry’s emissions or afford humane treatment to captive animals in resource-intensive operations. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

“I want to see people unify, regardless of differences, to win a humane balance for these otherwise voiceless souls,” said Maitely Weismann, who formed the group. No Visitors Leading to Despair and Isolation in Senior Care Homes |Jared Whitlock |July 28, 2020 |Voice of San Diego 

This solution is fair, it’s humane and most important, it will provide all schools with the funds they need to open safely in the fall. New State Budget Will Do Irreparable Harm to Schools |Alec Patton |June 29, 2020 |Voice of San Diego 

The change is an unalloyed good: It makes us more humane and our relationships more real. Why Your Waiter Hates You |Jedediah Purdy |October 26, 2014 |DAILY BEAST 

That is why I believe that you feel the value of this ethical and humane togetherness. Dear Turkish PM: It’s Time to Act to Save Kobani’s Kurds |Bahman Ghobani |October 8, 2014 |DAILY BEAST 

“We have to be humane and we care about our patients but we are also not a prison,” the anonymous doctor said. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 

And into the fray comes a weapon I never thought would have been legal, let alone humane—something called Knee Defender. For Short Men in 2014, The News Is Surprisingly Good |Kevin Bleyer |September 13, 2014 |DAILY BEAST 

It is framed, rightly so, as a painful act done in the service of being as humane and respectful as possible. Wendy Davis and the 'Good Abortion' Myth |Emily Shire |September 10, 2014 |DAILY BEAST 

But the rebels, presumably interpreting his humane suggestion as a sign of weakness, continued to fire on the Spanish troops. The Philippine Islands |John Foreman 

There were semi-savage native chiefs, and there were others, like Aguinaldo himself, with humane instincts. The Philippine Islands |John Foreman 

But this is not a humane and civilised nation, and never will be while it accepts Christianity as its religion. God and my Neighbour |Robert Blatchford 

It was a good and humane action to prevent bloodshed, but the world is not always worthy of good actions. The Atlantic Monthly, Volume 17, No. 101, March, 1866 |Various 

But would it not be more humane and more charitable to foresee the misery and to prevent the poor from increasing? Superstition In All Ages (1732) |Jean Meslier