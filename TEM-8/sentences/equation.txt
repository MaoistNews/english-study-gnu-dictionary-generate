The right-hand side of the equations describes the contents of the universe — anything that has mass and energy. The Physicist Who Slayed Gravity’s Ghosts |Thomas Lewton |August 18, 2020 |Quanta Magazine 

In other words, machines that augment or supersede organic biology and change the equation limiting the scope of how, where, and when “genetic” information can be shared and transferred. How Life Could Continue to Evolve - Issue 88: Love & Sex |Caleb Scharf |August 12, 2020 |Nautilus 

The scientists took this curve and developed a new mathematical equation to calculate a dog’s age. To figure out your dog’s ‘real’ age, you’ll need a calculator |Bethany Brookshire |August 12, 2020 |Science News For Students 

Whatever the reason, once these individuals become infected and are removed from the equation through death or immunity, the effect on the pandemic is outsized. Population immunity is slowing down the pandemic in parts of the US |David Rotman |August 11, 2020 |MIT Technology Review 

The equations of general relativity let them evolve independently, and, crucially, you can flatten the universe by changing either. Big Bounce Simulations Challenge the Big Bang |Charlie Wood |August 4, 2020 |Quanta Magazine 

Perhaps, like Hawking searching for his elegant equation, filmmakers will never find the answer. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

But vibrant industry requires solid infrastructure, which is where the Chinese government enters the equation. 'Made in China' Now Being Made in Africa |Brendon Hong |August 23, 2014 |DAILY BEAST 

Until that equation tips, individual Republicans may break ranks on gay rights, but the party remains a countercultural bastion. Red America’s Anti-Gay Backlash |Jonathan Rauch |June 15, 2014 |DAILY BEAST 

“We want to take out those biases, those prejudices, out of the equation, to remove those barriers,” says Halfteck. How the Hiring Process Marginalizes Candidates on the Autism Spectrum |Joshua Rivera |May 23, 2014 |DAILY BEAST 

Sadly, the “small room” part of that equation seems to have been a one-time thing. Beck’s Musical Time Machine: This Wasn’t a Concert. It was a Spectacular Party. |Andrew Romano |April 18, 2014 |DAILY BEAST 

Now the trouble with the main proposition just quoted is that each side of the equation is used as the measure of the other. The Unsolved Riddle of Social Justice |Stephen Leacock 

In other words, it is a truism, mere equation in terms, telling nothing whatever. The Unsolved Riddle of Social Justice |Stephen Leacock 

The fundamental equation of the economist, then, is that the value of everything is proportionate to its cost. The Unsolved Riddle of Social Justice |Stephen Leacock 

We are ourselves a term in the equation, a note of the chord, and make discord or harmony almost at will. The Pocket R.L.S. |Robert Louis Stevenson 

Inserting these values in our general equation and calculating the result, we obtain 18.1E6 as the value of the constant. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz