Climate change, which exacerbates these risks, seems to have finally tipped the balance of what was an increasingly untenable situation, says Anthony LeRoy Westerling, who is also at UC Merced. Suppressing fires has failed. Here’s what California needs to do instead. |James Temple |September 17, 2020 |MIT Technology Review 

This summer has seen more fires, more heat, more storms — all of it making life increasingly untenable in larger areas of the nation. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

Chrome made up most of DigiTrust’s cookie aggregation, making its future untenable. ‘Changing the pipes to run on a better currency’: Publishers’ first-party data strategies take shape |Lucinda Southern |August 11, 2020 |Digiday 

Bloom even announced the result in a lecture, only to realize afterward that some of their shortcuts were untenable. Landmark Math Proof Clears Hurdle in Top Erdős Conjecture |Erica Klarreich |August 3, 2020 |Quanta Magazine 

“I appreciate the issue getting attention because it’s just an untenable situation,” he said. Businesses Say Border Patrol Is Seizing Legal Cannabis Between San Diego, Imperial |Jesse Marx and Maya Srikrishnan |June 29, 2020 |Voice of San Diego 

He added that if the agreement eventually proves untenable, “we could walk any time.” Is The Guardian Holding Back The New York Times’ Snowden Stories? |Lloyd Grove |October 14, 2014 |DAILY BEAST 

Does this hunk of glass make up for his untenable relationships? And The Escort of The Year Is… Backstage at The Sex Oscars |Scott Bixby |March 24, 2014 |DAILY BEAST 

The enemy effected and exploited a breach on the left flank, rendering the friendly positions untenable. Rocker Lenny Kravitz’s Namesake Receives Medal of Honor |Michael Daly |March 19, 2014 |DAILY BEAST 

But two failed wars and a massive national debt have made their ultra-expensive, ultra-bloody foreign policy vision untenable. Why Doesn’t Anyone Care About the Rising U.S.-China Tension? |Peter Beinart |December 16, 2013 |DAILY BEAST 

Rebel-held territory in northern Syria has grown untenable for professional journalists and aid workers as well. War Tourists Flock to Syria’s Front Lines |Ben Taub |November 2, 2013 |DAILY BEAST 

The popping of dry, scrubby timber warned us that our position would soon be untenable. Raw Gold |Bertrand W. Sinclair 

Now, in our judgment, both the positions successively taken up in the Majority Report are untenable. English Poor Law Policy |Sidney Webb 

But in the light of scientific discoveries and demonstrations, such a belief is unfounded and utterly untenable to-day. Tyranny of God |Joseph Lewis 

Hence, although seriously inconvenienced, the French did not find their position untenable. The Life of Nelson, Vol. I (of 2) |A. T. (Alfred Thayer) Mahan 

From the square itself Beresford was forced by artillery fire to retreat, and the situation was soon seen to be untenable. Argentina |W. A. Hirst