So why does one species of dads dote over their children while the other bolts? Do Hands-On Dads Raise Healthier Kids? |Andy Hinds |June 15, 2014 |DAILY BEAST 

Sure, I like the going out to the restaurants and the the-ayters, and I dote on dancing; but—say—thats not all the game. The Woman Gives |Owen Johnson 

We'll all dote on him, hell be my reward If I dissemble.Lam. The Little French Lawyer |Francis Beaumont 

It is long since the sick world began to dote and talk idly: would she had but doted still! Discoveries Made Upon Men and Matter |Ben Jonson 

Oftentimes I forget my very name, so sharp a pang striking through my forehead that I dote and stare and forget all else. Joan of the Sword Hand |S(amuel) R(utherford) Crockett 

If ever one creature did dote upon another, Mr. George loved that sweet child. Bentley's Miscellany, Volume II |Various