Stewart has broken his silence about “the rumor” in a startling and candid new essay for The Players’ Tribune, a site founded by Derek Jeter that offers athletes “a platform to connect directly with their fans, in their own words.” Thank you, Kordell Stewart, for thoughtful response to ‘the rumor’ |Kevin Naff |February 5, 2021 |Washington Blade 

Partly due to her plain-spoken and candid interviewing style, Williams was able to easily transition from the airwaves to daytime television and managed to still make quite an impact on listeners, who have since become viewers, everywhere. A Look Back At Wendy Williams’ Most Talked About Interviews |Steven Psyllos |February 2, 2021 |Essence.com 

We still are very candid about having work to do, but we’ve come a long, long way. Despite record stock surge, GameStop is still struggling to stay afloat |Abha Bhattarai, Taylor Telford |February 1, 2021 |Washington Post 

If you’re already engaged with an agency, be prepared to have open and candid conversations about your needs and understand that some of those needs may not be able to be accommodated by the agency. Top four SEO myths debunked: Director’s cut |Houston Barnett-Gearhart |January 21, 2021 |Search Engine Watch 

Hardy, feeling similarly at the time, said the song “just sort of fell out” of them, and they didn’t hold back with candid lyrics. You might know Morgan Wallen because of the SNL incident. But he’s poised to be the future of country music. |Emily Yahr |January 21, 2021 |Washington Post 

Even though we were running late, Scott was jovial and candid in his conversation. Remembering ESPN’s Sly, Cocky, and Cool Anchor Stuart Scott |Stereo Williams |January 4, 2015 |DAILY BEAST 

In a candid interview, she compares the brutality of life in North Korea to the Holocaust. How ‘Titanic ’Helped This Brave Young Woman Escape North Korea’s Totalitarian State |Lizzie Crocker |October 31, 2014 |DAILY BEAST 

It was refreshingly candid, but not necessarily theologically sound or comforting to Christian voters. Is Rand Paul Christian Enough for the GOP? |Olivia Nuzzi |August 2, 2014 |DAILY BEAST 

I soon saw why and could not resist sneaking a few candid shots. Patted Down by India’s Hugging Saint |Daniel Genis |July 20, 2014 |DAILY BEAST 

He had always come up with solutions that were at once simple and elegant, while he himself would be both candid and reasonable. The Black Widow of Silicon Valley |Michael Daly |July 14, 2014 |DAILY BEAST 

Tell Lady Maude the candid truth, and take shame and blame to yourself, as you deserve. Elster's Folly |Mrs. Henry Wood 

In spite of all the sneers of infidels, the candid reader finds the divine record sublime in its simplicity. Gospel Philosophy |J. H. Ward 

No candid observer will deny that whatever of good there is in our civilization is the product of the gospel. Gospel Philosophy |J. H. Ward 

She was, to be candid, knitting an afghan against an interesting event which involved a friend of hers. The Amazing Interlude |Mary Roberts Rinehart 

No,” answered the Reverend Frank with a candid smile, “I saw no shepherds run past here. Hunted and Harried |R.M. Ballantyne