Think about a microorganism gathering to build a larger design. PS5 designer: ‘When I started drawing, it was much larger’ |Joe Moore |November 9, 2020 |Washington Post 

Gore has discovered rules that allow him to make predictions about simple micro-ecological systems, including the gut microorganisms of laboratory worms and bacterial communities in samples of soil. A Physicist’s Approach to Biology Brings Ecological Insights |Gabriel Popkin |October 13, 2020 |Quanta Magazine 

As the months progressed and the sun rose higher, the team found that algal growth accelerated, reaching its peak growth rate for the year in April and May, despite the microorganisms still being covered by ice. Trapped under ice, light-loving algae grow in the dark Arctic winter |Jonathan Lambert |September 25, 2020 |Science News 

There is a reason that biologists call the “biofilm” of microorganisms that cover moist surfaces “bacterial lawns.” What the Meadow Teaches Us - Issue 90: Something Green |Andreas Weber |September 16, 2020 |Nautilus 

It would be prudent to consider the arms race between humans and microorganisms as a long-term affair, and not to take our apparent success for granted. Our Genes May Explain Severity of COVID-19 and Other Infections |Monique Brouillette |July 27, 2020 |Quanta Magazine 

And the same thing happens when a Venerian is attacked by an Earthly microorganism. Islands of Space |John W Campbell 

A microorganism which apparently has a specific relationship to Rocky Mountain spotted fever. Handbook of Medical Entomology |William Albert Riley 

The disease is caused by a microorganism and several bacteria have been suspected of being responsible. Special Report on Diseases of the Horse |United States Department of Agriculture 

Staphylococcus pyogenes aureus—a microorganism producing yellow pus. Surgery, with Special Reference to Podiatry |Maximilian Stern 

Staphylococcus pyogenes citreus—a microorganism producing lemon-colored pus. Surgery, with Special Reference to Podiatry |Maximilian Stern