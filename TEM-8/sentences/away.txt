The real challenge would be we’re four weeks away from election. Trump moves closer to Pelosi in economic aid talks, and House speaker must decide next move |Rachael Bade, Erica Werner |September 17, 2020 |Washington Post 

I think that is something this industry makes you look away from. Christian Puglisi Is Closing His Influential Copenhagen Restaurants. COVID Is Only Partly to Blame |Rafael Tonon |September 17, 2020 |Eater 

This is a situation that they thought they had the upper hand on politically, but that advantage seems to be slipping away. Why House Democrats have good reason to be anxious about no coronavirus relief deal |Amber Phillips |September 17, 2020 |Washington Post 

It would go away without the vaccine, George, but it’s going to go away a lot faster with it. Timeline: The 124 times Trump has downplayed the coronavirus threat |Aaron Blake, JM Rieger |September 17, 2020 |Washington Post 

It’s probably going to go away a lot faster now because of the vaccine. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

Something like fluoride, which is too small for normal filters, yanks away that feeling of agency. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

He observes the bodies floating away on the river, pulling on his cigarette with a sneer. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

So it might be me projecting my desires onto Archer to want to just get away from work for a few weeks. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

But sources said that the evidence so far is pointing away from an ISIS connection. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

In this war, the targeting is often happening on computer monitors thousands of miles away, capturing images from drones. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

It was a decayed house of superb proportions, but of a fashion long passed away. Checkmate |Joseph Sheridan Le Fanu 

She walked away toward another door, which was masked with a curtain that she lifted. Confidence |Henry James 

If you throw away this chance, you will both richly deserve to be hanged, as I sincerely trust you will be. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

The bear laughed and joined his companion, and the torpedo thundered away. The Joyous Adventures of Aristide Pujol |William J. Locke 

Nevertheless the evening and the night passed away without incident. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various