Apple also unveiled Apple One, a bundle of its subscription services. Everything announced at Apple’s ‘Time Flies’ event today |rhhackettfortune |September 15, 2020 |Fortune 

That matches earlier rumors that Apple would make its music app the centerpiece of a bundle or choice of bundles that would include various services at prices offering a modest savings to consumers. Apple’s ‘Time Flies’ event: 5 things to look for |Aaron Pressman |September 15, 2020 |Fortune 

I brought a few bags of groceries, a pair of rubber boots, and a bundle of older clothes that I didn’t mind destroying. How to hunt for star-nosed moles (and their holes) |Kenneth Catania |September 15, 2020 |Popular-Science 

He also helped handle the development of Apple’s upgraded TV app last year and is leading the charge on Apple’s push into services bundles. Apple’s leadership evolves ahead of a post-Tim Cook era |radmarya |September 12, 2020 |Fortune 

Egyptian animal mummies can look like little more than bundles of cloth. X-rays reveal what ancient animal mummies keep under wraps |Helen Thompson |August 20, 2020 |Science News 

Ultimately, the big news in the saga of the cable bundle are the effects of the new lower priced tiers evolving. Oh Yes, He’s The Great Connector: Jason Hirschhorn’s Expertly Curated World |Lloyd Grove |October 17, 2014 |DAILY BEAST 

The larger transactions, say, for several street bags or an entire bundle of ten ($250) were paid for through Western Union. This Anti-Heroin Drug Is Now King of the Jailhouse Drug Trade |Daniel Genis |July 17, 2014 |DAILY BEAST 

Now it is true that this bundle of blunders and errors does not constitute “participation”in genocide. Bernard-Henri Lévy: Yes, France Is To Blame For Rwanda |Bernard-Henri Lévy |April 24, 2014 |DAILY BEAST 

Mingma also stepped back and let Arnot bundle the bleeding man into a tent. Breaking Mount Everest’s Glass Ceiling |Amanda Padoan, Peter Zuckerman |March 30, 2014 |DAILY BEAST 

He is quite a bundle of stimulus and reflex, with no reflection. The Real Wolf of Wall Street: Jordan Belfort’s Vulgar Memoirs |Jimmy So |December 20, 2013 |DAILY BEAST 

Strathland would bundle me out in ten minutes if anything happened to Jack. Ancestors |Gertrude Atherton 

He swerved as he passed it, and, looking, saw that it was a bundle wrapped in a striped blanket. The Joyous Adventures of Aristide Pujol |William J. Locke 

On breaking the seal, a letter at the top of a bundle of papers presented itself. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Across his shoulder he carried a bundle knotted into an old red handkerchief with a polka spot. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward 

He spurned the bundle with his foot, while the stranger stopped suddenly, as if a blow had been struck him. The Cromptons |Mary J. Holmes