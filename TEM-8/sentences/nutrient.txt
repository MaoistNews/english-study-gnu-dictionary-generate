They’re also a great way to consume a ton of nutrient-dense items and, when assembled with care, are surprisingly portable. How to Make Salad You'll Actually Want to Eat |AC Shilton |August 26, 2020 |Outside Online 

That means water and nutrients stick around, too, creating ideal conditions for plants to grow. A Norwegian Startup Is Turning Dry Deserts Into Fertile Cropland |Vanessa Bates Ramirez |August 19, 2020 |Singularity Hub 

The values they estimated included the sediments’ age, the density and distribution of cells in them, how those cells got their energy, and the rates at which the cells metabolized the available nutrients. ‘Zombie’ Microbes Redefine Life’s Energy Limits |Jordana Cepelewicz |August 12, 2020 |Quanta Magazine 

After “feeding” microbes from the collected sediments with nutrients including carbon and nitrogen, the team tracked the organisms’ activity based on what was consumed. These ancient seafloor microbes woke up after over 100 million years |Carolyn Gramling |July 28, 2020 |Science News 

We’re trying to see how that influences their geochemistry and nutrients, and how life has adapted to the different conditions. He Found ‘Islands of Fertility’ Beneath Antarctica’s Ice |Steve Nadis |July 20, 2020 |Quanta Magazine 

The deep is much too nutrient poor to support such a large animal. Shark Week Is Lying Again: Megalodon Is Definitely Extinct |David Shiffman |August 15, 2014 |DAILY BEAST 

After all, water is the most essential nutrient our system needs. 10 Ways to Stay Hydrated (That Aren’t Water) |DailyBurn |July 25, 2014 |DAILY BEAST 

This nutrient helps balance the sodium in your diet and assists with regulating blood pressure. The Truth About Salt: Should You Shake the Habit? |DailyBurn |May 5, 2014 |DAILY BEAST 

This may cause a negative autoimmune response, and inhibits proper digestion and nutrient absorption. Research Shows Link Between NSAID Use and Gut Disease |Valerie Vande Panne |April 21, 2014 |DAILY BEAST 

Smart snacks are nutrient dense; they have lower calories with healthy benefits. 6 Ways to Avoid ‘Sochi Gut’ While Watching the Olympics |Jenna A. Bell |February 12, 2014 |DAILY BEAST 

Variety is very desirable, provided that there is no important sacrifice in nutrient value. The Home of the Blizzard |Douglas Mawson 

By autumn, plankton concentrations diminish as light and nutrient levels decrease. Humpback Whales in Glacier Bay National Monument, Alaska |United States Department of Commerce, Marine Mammal Commission 

The composition, reaction, etc., of the nutrient medium in which the organism is growing. The Elements of Bacteriological Technique |John William Henry Eyre 

A watery solution of the extractives, etc., of lean meat (usually beef) forms the basis of several nutrient media. The Elements of Bacteriological Technique |John William Henry Eyre 

Complete the solution of the various ingredients by bubbling live steam through the flask as in making nutrient agar. The Elements of Bacteriological Technique |John William Henry Eyre