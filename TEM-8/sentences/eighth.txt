This was only an eighth-grader, barely a teenager, who felt so worthless in this world she would rather not live in it altogether. Why the Equality Act Matters For Our Family—And For Many Others Across America |Marie Newman and Evie Newman |March 3, 2021 |Time 

Another parent-led coalition, CPS Sick-Out, is asking Chicagoans to call their children in sick on Monday, when the city says it will open for preschoolers through eighth-graders and special-education students. Chicago parents say remote learning isn’t working and want their voices heard in a city still grappling with a plan |Dawn Reiss |January 30, 2021 |Washington Post 

Among the parents pushing for a speedy reopening through the Chicago Parents Collective is Lisa Mishkin, mother of a sixth-grader and an eighth-grader at Coonley Elementary School. Chicago parents say remote learning isn’t working and want their voices heard in a city still grappling with a plan |Dawn Reiss |January 30, 2021 |Washington Post 

Catrin Morris, mother of an eighth-grader in the school system who lives in Ward 3, said that if the city delivers on its safety promises and the measures in schools are adequate, it’s time for families to have the option to return. As D.C. schools work to reopen, other systems in region rethink plans for second semester |Perry Stein |January 6, 2021 |Washington Post 

Success Academy eighth-graders also must pass comprehensives. Can charter schools pick the best students? No, but many believe the myth. |Jay Mathews |January 2, 2021 |Washington Post 

On his eighth try, more than three decades after he went in, the parole board finally voted to release Sam. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

Meryl Streep makes an eighth of what Justin Bieber does a year. Justin Bieber Isn’t Even 21, Yet Makes More Money Than Meryl Streep |Amy Zimmerman |November 25, 2014 |DAILY BEAST 

She was a beanpole growing up, before gaining 30 pounds after getting her period in eighth grade. Speed Read: Lena Dunham’s Most Shocking Confessions From ‘Not That Kind of Girl’ |Kevin Fallon |September 26, 2014 |DAILY BEAST 

On the third circuit they make up just 17 percent of justices, and on the eighth circuit 18 percent. Why Women Should Forget the White House |Keli Goff |July 21, 2014 |DAILY BEAST 

He lost the bet on the twenty-eighth question, when a duplicate birthday turned up. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 

John of Damascus, an important Greek theologian of the eighth century, often cited by Thomas. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

In the eighth line we have “bells” seven times repeated in all—bells being taken in their utmost generality, viz., musical action. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

On the eighth of the said month it was washed, and another small grain of gold obtained, of the same size as the preceding. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The steam in ordinary working was shut off when the piston had moved from an eighth to a quarter of its stroke. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Then, measured by inhabitants, it was the fifth town in Ireland; now it is the eighth. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow