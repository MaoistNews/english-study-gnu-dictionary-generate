This offseason, he wants to add weight to become more durable and refine his consistency. Quarterback Taylor Heinicke signs two-year contract to return to Washington Football Team |Sam Fortier |February 10, 2021 |Washington Post 

In other words, it takes more energy to recover, process, transport, and refine a barrel of Tar Sands oil than one from say, Texas’ Permian Basin. Keystone XL was supposed to be a green pipeline. What does that even mean? |Juliet Grable |February 5, 2021 |Popular-Science 

It’s vital to think both in terms that are broader and more refined by being visible to those who are interested in what you have to offer but might not yet know about your product or brand. Semantic search drives expanded outreach to potential customers |Nikki Vegenski |February 4, 2021 |Search Engine Watch 

Those candidates spent a week in virtual intensive collaboration with industry experts, who tweaked and refined their innovations. NIH Director Francis Collins Is Fighting This Coronavirus While Preparing for the Next One |Belinda Luscombe |February 4, 2021 |Time 

Without A-list superstars to lean on, Utah has refined its four-out offensive system while sticking to its tried-and-true defensive identity around Gobert. The front-runners for NBA awards aren’t the usual suspects |Ben Golliver |February 1, 2021 |Washington Post 

It makes us refine our arguments, and search for greater efficiencies, and do our jobs better. A U.S. Thanksgiving—Family Style: Fractious but Friendly |Joshua DuBois |November 24, 2013 |DAILY BEAST 

Saudi Arabia sits on top of a vast reservoir of high quality oil that is cheap to pump and cheap to refine. Fracking is Pitting OPEC Members Against Each Other. It Couldn't Happen to a Nicer Bunch of Cartel Members. |Megan McArdle |May 29, 2013 |DAILY BEAST 

But in the late 19th century, we learned how to refine grain and make white flour. It’s the End of the World Unless We All Start Cooking |Rachel Khong |April 23, 2013 |DAILY BEAST 

We continue to research and refine abortion care while helping to break down barriers separating women from high-quality services. More on Dr. Gosnell |Michael Tomasky |April 18, 2013 |DAILY BEAST 

I can continually refine the characters, their histories, and their damage, until they are exactly the right people I need. Barbara Kingsolver: How I Write |Noah Charney |December 5, 2012 |DAILY BEAST 

And I will bring the third part through the fire, and will refine them as silver is refined: and I will try them as gold is tried. The Bible, Douay-Rheims Version |Various 

Refine and purge our earthy parts; But, oh, inflame and fire our hearts! The Ontario Readers: The High School Reader, 1886 |Ministry of Education 

They could refine gold and silver and make weapons of tempered copper, but they were entirely ignorant of the use of iron. South American Fights and Fighters |Cyrus Townsend Brady 

Does it refine the moral taste, or call into action the best feelings of our nature? The Sheepfold and the Common, Vol. II (of 2) |Timothy East 

The letter to Bliss and the proofs were full of suggested changes that would refine and beautify the text. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine