An essential conservative insight about everything is that nothing necessarily endures. Will Senate Republicans allow their louts to rule the party? |George Will |February 12, 2021 |Washington Post 

The two endure a terrifying adventure where survival is never guaranteed. ‘Little Nightmares II’ made me dread every moment. And I loved it. |Elise Favis |February 9, 2021 |Washington Post 

Since socializing in winter now requires us to endure frigid temperatures, at least in many parts of the country, a layer that won’t stay put just won’t do. The Most Practical Outdoor Dining Outfit Is a Snuggie |Jaya Saxena |February 9, 2021 |Eater 

More responsible leadership could have made an immense difference in the suffering and the death that America has endured. “We did the worst job in the world”: Lawrence Wright on America’s botched Covid-19 response |Sean Illing |February 9, 2021 |Vox 

The reader must endure a slow start as various plotlines are established, but the pace quickens at the halfway mark. Two historical mystery novels plunge readers into the past while keeping them guessing |Clare McHugh |February 8, 2021 |Washington Post 

This is a degrading and shameful state which no man or woman should be forced to endure. The Sony Hack and America’s Craven Capitulation To Terror |David Keyes |December 19, 2014 |DAILY BEAST 

But alas, a snub is yet another of the many indignities Valerie Cherish shall endure. 15 Enraging Golden Globe TV Snubs and Surprises: Amy Poehler, 'Mad Men' & More |Kevin Fallon |December 11, 2014 |DAILY BEAST 

Mary Soames is an exception to the rule that gilded offspring endure life rather than enjoy it. Churchill’s Secret Treasures for Sale: A British PM’s Life on the Auction Block |Tom Teodorczuk |December 8, 2014 |DAILY BEAST 

Some might lack the fortitude—or masochism—required to endure a grueling campaign (Rubio). What Republicans Need Right Now Is a Good Internal Fight |James Poulos |November 6, 2014 |DAILY BEAST 

“That was the first time I realized I would endure a lot of discrimination,” she says. How ‘Titanic ’Helped This Brave Young Woman Escape North Korea’s Totalitarian State |Lizzie Crocker |October 31, 2014 |DAILY BEAST 

But this alliance is rotten, and cannot endure; the Western men are no partizans of slavery. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Who could suppose that two tolerably civilized nations would endure this in the middle of 1851? Glances at Europe |Horace Greeley 

(b) All those who are under 20 and more than 50 years of age, and who are strong enough to endure the fatigue of a campaign. The Philippine Islands |John Foreman 

Though built upon the sand, they still endured, and would continue to endure. The Wave |Algernon Blackwood 

It is astonishing how much petting a big boy of ten can endure when he is quite sure that there is no one to laugh at him. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling