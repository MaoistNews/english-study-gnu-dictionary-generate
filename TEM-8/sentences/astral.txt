Her voice is more present and close on this album, slightly different from the more muffled sound heard on the original, as if beaming back from the astral plane so as to reach us here on Earth. Alice Coltrane is finally heralded as a jazz great. A new reissue doesn’t do her justice. |Andy Beta |July 9, 2021 |Washington Post 

He talked about cosmic sounds, higher dimensions, astral levels and other worlds, and realms of music and sound that I could feel. Alice Coltrane is finally heralded as a jazz great. A new reissue doesn’t do her justice. |Andy Beta |July 9, 2021 |Washington Post 

We weren’t there to know exactly what “cosmic sounds, higher dimensions, astral levels and other worlds” the mighty Turiyasangitananda could hear. Alice Coltrane is finally heralded as a jazz great. A new reissue doesn’t do her justice. |Andy Beta |July 9, 2021 |Washington Post 

In the 2016 movie, Strange astral projects to read and research magic while he’s asleep. WandaVision’s series finale sets up the future of Wanda and the MCU |Alex Abad-Santos |March 5, 2021 |Vox 

Because he wanted to make sure he didn’t squander any opportunity to capture the astral illuminations, he booked three nights with Redman a couple of winters ago. The aurora borealis skipped this Alaska photography class. But some swinging light cords saved the night. |Bailey Berg |February 26, 2021 |Washington Post 

Oftentimes public fear like this can be a self-indulgent kind of collective astral projection. The Real Nightmare of Ferguson |James Poulos |August 15, 2014 |DAILY BEAST 

Astraea is the starry Lady of the Scales, a sort of karmic, astral Venus who weighs one's deeds. The Stars Predict Your Week |Starsky + Cox |October 9, 2011 |DAILY BEAST 

It gave me great pleasure to expose one of them in The Astral. Novelist Kate Christensen's Winning Novels About Losers | |June 13, 2011 |DAILY BEAST 

The Astral captures the cozy pleasures and messy troubles inherent in contemporary social networks. Novelist Kate Christensen's Winning Novels About Losers | |June 13, 2011 |DAILY BEAST 

The Astral is set in north Brooklyn, where Christensen, 48, lived for almost 20 years (she moved to New Hampshire two years ago). Novelist Kate Christensen's Winning Novels About Losers | |June 13, 2011 |DAILY BEAST 

For this Double, or astral body as you call it, is really the seat of the passions, emotions and desires in the psychical economy. Three More John Silence Stories |Algernon Blackwood 

Then come the chakras already spoken of as the centres for the working of consciousness in the astral body. Evolution of Life and Form |Annie Wood Besant 

Did Zio Giacomo in the library hear with his astral ear his son's gratifying assertion? The Devourers |Annie Vivanti Chartres 

They were, moreover, persuaded that a great number of phenomena and events have their origin in astral influences. An Epitome of the History of Medicine |Roswell Park 

There isn't any, unless you believe in black cats or astral influence, or the curse of Shielygh—or something. Where the Pavement Ends |John Russell