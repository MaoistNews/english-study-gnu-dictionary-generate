People have a complicated relationship with robots, torn between admiring them, fearing them, rejecting them, and even boycotting them, as has happened in the automobile industry. Is the Pandemic Spurring a Robot Revolution? |Marius Robles |November 30, 2020 |Singularity Hub 

I admire anybody who can look forward, and make a statement about 2021. Last week’s vaccine-led rally was one for the record books—but is all that good news already priced in? |Bernhard Warner |November 16, 2020 |Fortune 

Russell, a player he admired above all others, scored 19 points for Boston, with 32 rebounds and five blocked shots. Tom Heinsohn, mainstay of Boston Celtics dynasty as player, coach, dies at 86 |Matt Schudel |November 11, 2020 |Washington Post 

It was a publishing company, based in New York, whose product I really admired. To take control of your career, ask for what you want—directly and explicitly |matthewheimer |November 8, 2020 |Fortune 

So, the second rookie mistake, I would say, was that because I trust and admire Gerard and his experience greatly, I did not put a great deal of oversight over the company. Why the Left Had to Steal the Right’s Dark-Money Playbook (Bonus Episode) |Sudhir Venkatesh |October 31, 2020 |Freakonomics 

Something about it I admire and something about it I find unpersuasive. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

He also recalls the many visitors who would often go to the island to admire its harvests and wildlife. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

You have to admire his convictions; most frustrated auteurs in this town just call such things “an Alan Smithee project.” Sony Hack: A Dictator Move? |Kevin Bleyer |December 14, 2014 |DAILY BEAST 

He allows the subject to float over to Hitchcock with a calm directness that I admire. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

It rests in the message of hope in songs so many young Americans admire: New Jersey's own Bruce Springsteen. Are Politicians Too Dumb to Understand the Lyrics to ‘Born in the USA’? |Parker Molloy |November 6, 2014 |DAILY BEAST 

Let them that sail on the sea, tell the dangers thereof: and when we hear with our ears, we shall admire. The Bible, Douay-Rheims Version |Various 

I'd admire to see him cavorting around on the pinnacles after horse-thieves or whisky-runners or a bunch of bad Indians. Raw Gold |Bertrand W. Sinclair 

We idlers had permission granted us to land and visit the town, in which, however, we found but little to admire. A Woman's Journey Round the World |Ida Pfeiffer 

The dining room was for the souls of the locals, who could admire the desert more conveniently than find a good meal. Fee of the Frontier |Horace Brown Fyfe 

I greatly admire his character, but he positively could not have made his way along the fire trenches I inspected yesterday. Gallipoli Diary, Volume I |Ian Hamilton