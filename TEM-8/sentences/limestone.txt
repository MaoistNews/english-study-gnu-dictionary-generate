Among the piles of specimens were six large slabs of limestone. Smugglers almost made off with a near-complete fossil of a bizarre flying reptile |Hannah Seo |August 26, 2021 |Popular-Science 

For now, you can ride 195 miles of its crushed limestone from the town of Norfolk in the east to Valentine in the west. The 25 Best Fall Trips in the World |jversteegh |August 9, 2021 |Outside Online 

With limestone outcroppings, rocky terrain, and vast, dry prairies, Missouri’s 12,423-acre Hercules-Glades Wilderness is a mish-mash of Ozark Mountain terrain. 50 U.S. Wilderness Areas, Ranked |eriley |July 26, 2021 |Outside Online 

For limestone, he estimates that the number could be as much as twice that, because of the heat required to process the rock. There’s Never Been a Better Time to Buy a Sustainable Wetsuit |wtaylor |July 23, 2021 |Outside Online 

That process would convert calcium oxide back into calcium carbonate, the main component of limestone, at which point the process can simply begin again. A startup using minerals to draw down CO2 has scored funding—and its first buyer |James Temple |May 26, 2021 |MIT Technology Review 

So he and the scouts dug into a limestone hill and built a factory in record speed. Israel Had a Secret, Underground Bullet Factory |Nina Strochlic |July 18, 2014 |DAILY BEAST 

The ship, which was carrying a consignment of limestone, broke in half. Prince William's Dramatic Rescue Mission Boosts Royals' Image |Tom Sykes |November 28, 2011 |DAILY BEAST 

Middlebury is gorgeous, with big, open quads and limestone buildings that echo Yale's. The Great College Road Trip |Nick Summers |April 8, 2011 |DAILY BEAST 

The soul of his output in this period: a series of 20 limestone heads, evocative of African sculpture. "Mad, Bad Modigliani" |Brad Gooch |March 5, 2011 |DAILY BEAST 

For instance, the Limestone Polypody is not happy unless there is a certain amount of lime present in the soil. How to Know the Ferns |S. Leonard Bastin 

The train had long passed Hornberg, and far below the streams tumbled in white foam down the limestone rocks. Three More John Silence Stories |Algernon Blackwood 

A band of limestone also occurs at Templeton containing masses of a light-coloured translucent serpentine. Asbestos |Robert H. Jones 

An abundance of limestone makes the soil exceptionally fertile and productive. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

We were now in the "limestone country," and the roads are exceedingly dusty in dry weather. British Highways And Byways From A Motor Car |Thomas D. Murphy