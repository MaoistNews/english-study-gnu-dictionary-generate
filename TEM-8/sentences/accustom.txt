This seems to be broadly rolling out now and may surprise some advertisers who have been accustomed to the older version of this screen. Title changes in Google Search causing distress; Wednesday’s daily brief |Barry Schwartz |August 25, 2021 |Search Engine Land 

Anderson, like most cosplayers, was accustomed to planning her outfits around events. All dressed up with nowhere to go: Cosplaying in the pandemic |Lauren Orsini |June 25, 2021 |Washington Post 

We’re a culture accustomed to the rah-rah inspiration of fitness personalities, ones like Sheila aspires to be. Rose Byrne on Unleashing Her Meanest Streak Yet in ‘Physical’: ‘It’s Deeply Uncomfortable’ |Kevin Fallon |June 17, 2021 |The Daily Beast 

So start accustoming your dog to future changes in lifestyle. I Was a Bad Dog Owner. Don’t Be Like Me. |Kate Siber |May 11, 2021 |Outside Online 

The swift cascade of reactions to his death indicate how accustomed the United States — and the Twin Cities area in particular — have grown to responding to such incidents. Minn. police officer who shot Daunte Wright apparently meant to use Taser but accidentally fired gun, police chief says |Kim Bellware, Andrea Salcedo, Sheila Regan |April 12, 2021 |Washington Post 

Many tears were shed by Valeria; for a long time she could not accustom herself to her loss. Dream Tales and Prose Poems |Ivan Turgenev 

He endeavored also to accustom himself to eat raw flesh, but this was a point of perfection to which he never could arrive. Great Men and Famous Women. Vol. 3 of 8 |Various 

He was trying to accustom himself to the idea of having a name. The Status Civilization |Robert Sheckley 

Try as hard as I would, I could not accustom my muscles to these new conditions. Pharaoh's Broker |Ellsworth Douglass 

You see that this, your last summer in Italy, is manufactured on purpose to accustom you to the English seasons. The Life and Letters of Mary Wollstonecraft Shelley, Volume I (of 2) |Florence A. Thomas Marshall