Will the next few hours be both didactic and entertaining, providing us with ample high and lowbrow cocktail party fodder? From ‘American Hustle’ to ‘Saving Mr. Banks,’ Why Is Hollywood Hooked On Embellishing the Truth? |Marina Watts, Marlow Stern |January 8, 2014 |DAILY BEAST 

We mixed highbrow and lowbrow, the celebrated and the not-yet-celebrated. Charles Michener on Newsweek’s Cultural Edge |Charles Michener |December 24, 2012 |DAILY BEAST 

As with Beck and Limbaugh, hordes of advertisers began pulling out over its edgy format and occasional lowbrow content. ‘Evocateur: The Morton Downey Jr. Movie’: The Pundit’s Rise and Fall |Marlow Stern |April 24, 2012 |DAILY BEAST 

Highbrow meets lowbrow when the staff of NPR covers “Telephone.” 11 Hilarious Lady Gaga Wannabes |The Daily Beast Video |May 30, 2010 |DAILY BEAST 

As does, perhaps, the so-called Lowbrow School, of whom he is a persuasive representative. The King of Old-time Kitsch |Anthony Haden-Guest |April 29, 2010 |DAILY BEAST 

As I was saying when this lowbrow interrupted me, I was thinking that it might be a good idea to go nutting. The Radio Boys at Mountain Pass |Allen Chapman 

It was one of our chief delights to watch Frankling grind his teeth when some lowbrow—as he called them—drew her name. At Good Old Siwash |George Fitch 

When I came to I found myself in that room, with one lowbrow on guard. Bert Wilson on the Gridiron |J. W. Duffield