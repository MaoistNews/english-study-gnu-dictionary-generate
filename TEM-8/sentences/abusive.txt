Unfortunately, some abusive users were beginning to use Send to distribute malware and as part of spear phishing attacks. Mozilla shutters Firefox Send and Notes |Frederic Lardinois |September 17, 2020 |TechCrunch 

Across the United States, cities are experiencing turbulence and a rise in gun violence following the protests of abusive policing sparked by the May 25 death of George Floyd in Minneapolis. What Can Mayors Do When the Police Stop Doing Their Jobs? |by Alec MacGillis |September 3, 2020 |ProPublica 

Pender told NBC Washington in an interview that he initially had a consensual relationship with Grant and Grant became abusive after Pender ended the relationship. Male staffer accuses Seat Pleasant mayor of sexual harassment |Lou Chibbaro Jr. |August 11, 2020 |Washington Blade 

Apart from this, Twitter authorities also informed about efforts being taken to automate the removal of abusive and manipulated posts. How Twitter is contributing to support masses during the Coronavirus outbreak |Harry Liam |May 22, 2020 |Search Engine Watch 

So the implication is, you can make your kids stupider by being a bad parent, particularly if you’re abusive or something like that. Reasons to Be Cheerful (Ep. 417) |Stephen J. Dubner |May 7, 2020 |Freakonomics 

They witnessed and experienced the same types of abusive events, Fenner claims. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

And so it goes, another chapter written in the mutually abusive relationship of bad government and bad culture. The FBI’s Bogus ISIS Bust |James Poulos |November 21, 2014 |DAILY BEAST 

That would put a real crimp in the dating seminars of a man who actively encourages abusive behavior toward women. ‘Pick-Up Artist’ to Be Banned from UK |Tom Sykes |November 19, 2014 |DAILY BEAST 

Julien Blanc says he can teach any man to bed women successfully thanks to easy—and very abusive—techniques. ‘Pick-Up Artist’ to Be Banned from UK |Tom Sykes |November 19, 2014 |DAILY BEAST 

But she was very abusive to the girls, with punishments ranging from being called ugly to getting beaten with a baseball bat. Azealia Banks Opens Up About Her Journey from Stripping to Rap Stardom |Marlow Stern |November 17, 2014 |DAILY BEAST 

He delivered to the Secretary of State a note abusive and impertinent beyond all example and all endurance. The History of England from the Accession of James II. |Thomas Babington Macaulay 

One day Captain Bainbridge, of the Essex, was talked to in an abusive way, and said little back. Stories of Our Naval Heroes |Various 

Pointing his musket first at one and then at another, he returned yell for yell, and was in fact abusive. Overland |John William De Forest 

If you had not prevented him, that blackguard would have used abusive language to me and ranged himself on your side. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 

But his vehement and abusive style of declamation could not in debate be compared with the calm reasoning of Castlereagh. Is Ulster Right? |Anonymous