Instead, it’s more consistent with the idea that repeated cold exposure might actually impair your toes’ ability to handle the cold. How Your Body Does (and Doesn't) Adapt to Cold |Alex Hutchinson |February 10, 2021 |Outside Online 

A smart pill detects an athlete’s body temperature and transmits it to an external device, so coaches can look for spikes that might impair performance. As biometrics boom, who owns athletes’ data? It depends on the sport. |Nick Busca |February 2, 2021 |Washington Post 

Another study from 2017 had athletes eat a low-carb diet for three weeks and found that it impaired performance by reducing exercise efficiency. 7 Wellness Strategies to Build Resilience |Brad Stulberg |January 22, 2021 |Outside Online 

Alterations to hotels, public buildings and transport hubs prepared Tokyo not only for physically impaired Paralympic athletes and fans, but also for the creaking limbs of its own population, the world’s oldest. Can Tokyo Safely Host the Olympic Games This Summer? |Charlie Campbell |January 12, 2021 |Time 

Stress actually impairs the body’s ability to repair itself. Outwitting the Grim Reaper - Issue 94: Evolving |Kevin Berger |December 23, 2020 |Nautilus 

Formerly to impair the morals was a minor was a punishable offense. Holy Homophobia, Batman! A Queer Reading of the Dark Knight |Rich Goldstein |July 26, 2014 |DAILY BEAST 

In other words, researchers were able to prove that THC should, technically, impair driving, but not that it does. The Truth About Driving While Stoned |Abby Haglage |June 12, 2014 |DAILY BEAST 

Does Ambien impair judgment enough to drive one to violent crime? Is the ‘Ambien Defense’ Total Bullsh*t? |Janelle Dumalaon |April 15, 2014 |DAILY BEAST 

A host of environmental influences more directly impair brain functioning in a way that predisposes to violence. What Made the Boston Bombers Do It |Adrian Raine |May 3, 2013 |DAILY BEAST 

But recent research indicates that stress-inducing measures can actually impair memory. New Research Suggests Enhanced Interrogation Not Effective |R.M. Schneiderman |May 25, 2012 |DAILY BEAST 

They can never be taken from the capital, for this would impair it and, if continued, result in the insolvency of the corporation. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Such history never loses its interest, nor does the lapse of ages, in the least degree, impair its credibility. Gospel Philosophy |J. H. Ward 

Yes, there is that unchangeable oval cut of face, those features which time will never impair, that graceful and thoughtful brow. The Petty Troubles of Married Life, Complete |Honore de Balzac 

That the acts in question impair this contract, has already been sufficiently shown. Select Speeches of Daniel Webster |Daniel Webster 

They impair and take away the charter; and they appropriate the property to new uses, against their consent. Select Speeches of Daniel Webster |Daniel Webster