Just as China, with its opulent palaces, porcelain and scholar-gentry, would dominate the landscape of Western imagination, Persia invited its own share of adventurers. America and Iran, from fascination to antagonism |Ray Takeyh |February 26, 2021 |Washington Post 

In China, for example, Pinduoduo has become a formidable rival to Alibaba through its group-selling model and focus on fresh produce. Singapore-based Raena gets $9M Series A for its pivot to skincare and beauty-focused social commerce |Catherine Shu |February 26, 2021 |TechCrunch 

The round comes two years after Tim Hortons made its foray into China’s booming coffee industry. Tim Hortons marks two years in China with Tencent investment |Rita Liao |February 26, 2021 |TechCrunch 

Since its launch in China in 2016, TikTok has become one of the world’s fastest-growing social networks. 10 Breakthrough Technologies 2021 |David Rotman |February 24, 2021 |MIT Technology Review 

Since TikTok launched in China in 2016, it has become one of the most engaging and fastest-growing social media platforms in the world. The beauty of TikTok’s secret, surprising, and eerily accurate recommendation algorithms |Abby Ohlheiser |February 24, 2021 |MIT Technology Review 

He did travel to China and Australia while the story was unfolding. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

In 1957, the islands came under repeated shelling by Mainland--or as it was then called, “Red”-- China. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

The two islands are now tourist sites for visitors from Taiwan and mainland China. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

At least 70 percent of the children were adopted from overseas, including Russia, China, Ethiopia and Ukraine. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 

The Communist Party of China gets a bad rap for cracking down on religion. The Buddhist Business of Poaching Animals for Good Karma |Brendon Hong |December 28, 2014 |DAILY BEAST 

Distance, the uncertain light, and imagination, magnified it to a high wall; high as the wall of China. The Giant of the North |R.M. Ballantyne 

I shipped for a voyage to Japan and China, and spent several more years trying to penetrate the forbidden fastnesses of Tibet. The Boarded-Up House |Augusta Huiell Seaman 

The ships from China do not come, and it is with their merchandise that our ships must go to Nueva Spaña. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Black Sheep was sent to the drawing-room and charged into a solid tea-table laden with china. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

In former years, Korea had paid an annual tribute or tax to China, but for some time it had been held back by this king. Our Little Korean Cousin |H. Lee M. Pike