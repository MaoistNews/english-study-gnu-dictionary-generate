Today, the city is an Asian hipster outpost, with shopping malls, clothing boutiques, and mixologist-prepared cocktails. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 

Just 47 percent of Asian-Americans voted in the 2012 presidential election. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 

Asian-Americans may vote for Democrats now, but they are a highly persuadable—and growing—part of the electorate. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 

Asian-Americans are a group of persuadable swing voters, growing faster than any other group in America today. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 

In 1992, Republican George H.W. Bush won the Asian-American vote by 24 points. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 

We were close upon the Asian shore, and I put the helm down to go about. Paul Patoff |F. Marion Crawford 

Malagigi sat repenting in his Asian grotto, like S. Gerolamo in the pictures. Castellinaria |Henry Festing Jones 

In remoter days Huns had swept through these passes, coming from Asian deserts to the pillage of Europe. The Hosts of the Air |Joseph A. Altsheler 

In Central Asian rugs we find the cone replaced by the pomegranate, and the tree of life becomes a pomegranate tree. The Gates of India |Thomas Holdich 

Taking it as it is, with the test of language added, nothing short of an Asian origin will account for it. More Celtic Fairy Tales |Various