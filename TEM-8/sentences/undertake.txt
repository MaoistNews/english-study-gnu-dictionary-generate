We undertook extra mechanical checks which we believed had resolved the issue and informed the supplier. HumanForest suspends London e-bike sharing service, cuts jobs after customer accident |Natasha Lomas |September 25, 2020 |TechCrunch 

Scientists around the world are currently undertaking one of the fastest vaccine-development programs in history, trying to get the novel coronavirus under control as quickly as humanly possible. How To Know When You Can Trust A COVID-19 Vaccine |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |September 23, 2020 |FiveThirtyEight 

Everyone should also remember that in our daily lives, each of us undertakes activities that pose at least a little risk to others. Philosophy And Psychology Agree – Yelling At People Who Aren’t Wearing Masks Won’t Work |LGBTQ-Editor |September 21, 2020 |No Straight News 

In it, 1,000 subjects undertook a three-month exercise program to see whether it would lower their blood pressure. Why Altitude Training Helps Some but Not Others |Alex Hutchinson |September 11, 2020 |Outside Online 

NASA and Boeing subsequently undertook a comprehensive review of Boeing’s software development program, as well as the agency’s own practices surrounding the public-private partnership, and determined a number of corrective actions. Boeing and NASA target December for second try at uncrewed orbital demonstration flight |Darrell Etherington |August 28, 2020 |TechCrunch 

(1)  Only charities and non-profits should ask for unpaid workers to staff their operations or undertake time-consuming projects. Rich People Want You to Work for Free |Ted Gioia |October 20, 2014 |DAILY BEAST 

“However, it is still unclear to me if the U.S. and its allies are prepared to undertake such a comprehensive approach,” he says. Obama's Iraq-Syria Dilemma: No Force Now on the Ground Can Beat ISIS |Jamie Dettmer |August 26, 2014 |DAILY BEAST 

His gun is available to anyone willing to undertake a few minutes of Internet research. The Assassin's Gun: Internet Liberty Gone Way Too Far |David Frum |May 11, 2013 |DAILY BEAST 

Pronouncing illegality, governments will often undertake demolitions of slum houses. They All Fall Down: The Perils of Mumbai Housing |Dilip D’Souza |April 12, 2013 |DAILY BEAST 

She will be remembered as a strong leader and a person willing to undertake difficult tasks to achieve long-term objectives. Margaret Thatcher and Ronald Reagan: The Ultimate ’80s Power Couple |George Shultz |April 8, 2013 |DAILY BEAST 

We are ourselves satisfied, and undertake to demonstrate to our readers, that this question must be answered in the affirmative. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

He thus probably acquired sufficient confidence to undertake and perform the operation himself. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The work which he gives countenance to some to undertake, according to his own good pleasure, he commits to others. The Ordinance of Covenanting |John Cunningham 

Passage over it is often one of the most difficult feats to accomplish which the Alpine explorer has to undertake. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Be sure, before you accept any invitation to play, that you know perfectly the piece you undertake. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley