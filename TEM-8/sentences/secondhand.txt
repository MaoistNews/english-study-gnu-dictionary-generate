Online marketplace for secondhand luxury goods Poshmark, pet supply retailer Petco Animal Supplies, mobile game developer Playtika and auto service and supply company Driven Brands Holdings are all on deck to go public. Online lender Affirm gets $12 billion valuation in IPO |Verne Kopytoff |January 13, 2021 |Fortune 

These services are particularly popular in China’s maturing secondhand luxury market. The Unlikely New Champion of Consumer Rights: China |Charu Kasturi |December 17, 2020 |Ozy 

Buying secondhand clothing could provide consumers a way to push back against the fast-fashion system. Secondhand Clothing Sales Are Booming – And May Help Solve The Sustainability Crisis In The Fashion Industry |LGBTQ-Editor |November 25, 2020 |No Straight News 

Research shows that journaling about both negative and positive emotions can ward off secondhand stress and improve mental health. Six ways adults can help children make sense of a divisive election |Phyllis Fagell |October 29, 2020 |Washington Post 

Lechman said she too initially heard about her clients’ sicknesses secondhand – through family members or other attorneys with clients in the same housing units. Docs, Witnesses Say Lax Procedures Fueled Virus Spread in Federal Jail |Maya Srikrishnan |September 28, 2020 |Voice of San Diego 

The last film most people probably remember of his was the underwhelming sexagenarian comedy Secondhand Lions back in 2003. Gone Guy: The Return of Haley Joel Osment |Marlow Stern |October 29, 2014 |DAILY BEAST 

“I think if anybody saw real fame first or secondhand, they would not want to pursue it at all,” says a somber Culkin. The Revival of Kieran Culkin: A Reluctant Star Seizes the Spotlight |Marlow Stern |October 23, 2014 |DAILY BEAST 

But recently—more than three decades after my initial reading—I picked up a secondhand copy of the out-of-print book. They Saw Our Sports Problem Coming |T.D. Thornton |September 14, 2014 |DAILY BEAST 

Gordon picked up a pair secondhand and began to stencil then onto blank canvases with spray paint. Kim Gordon: Going Solo After Sonic Youth, and Why She Identifies With ‘Girls’ |Andrew Romano |April 10, 2014 |DAILY BEAST 

However studies have found dangerous compounds like benzene and formaldehyde in inhaled or secondhand vapor. E-Cigarettes, Facing Ban, Still Figuring Out What They Want to Be |Alex Halperin |December 19, 2013 |DAILY BEAST 

However, at secondhand, the details very shortly reached him. From Place to Place |Irvin S. Cobb 

A secondhand copy would be interesting not only for its material but for the price it would fetch. Recollections of Calcutta for over Half a Century |Montague Massey 

Then there is a large, more or less cultured, public that know something of the science at secondhand through books. The Astronomy of the Bible |E. Walter Maunder 

It has already been pointed out that language is a means through which we can get experience secondhand. The Science of Human Nature |William Henry Pyle 

Very good furniture can sometimes be obtained secondhand, but one must be on their guard against "bargains" that are worthless. How to be Happy Though Married |E. J. Hardy.