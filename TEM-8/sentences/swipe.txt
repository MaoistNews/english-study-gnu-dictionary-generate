Instead of using physical buttons, the right cup features swipe gestures to adjust volume or switch tracks. Electronics and exercise gear that make excellent gifts |PopSci Commerce Team |October 8, 2020 |Popular-Science 

Helmed by licensed real estate broker Hannah Bomze, Casa Blanca introduces dating app-like swipe technology, combining machine learning with real-life insight from some of New York City’s most experienced real estate agents. Meet the app that wants to be the Bumble of real estate |Rachel King |October 6, 2020 |Fortune 

The nonstick six inch plates cook perfect Mickey waffles in five minutes and can be cleaned just as quickly with a few swipes of a damp cloth. The best mini waffle makers for delectable breakfasts |PopSci Commerce Team |October 2, 2020 |Popular-Science 

You can also access the camera quickly from the Control Center, via a swipe down from the top right of the screen when the phone is unlocked. Get around your phone more quickly than you already are |David Nield |September 16, 2020 |Popular-Science 

Assembly Speaker Anthnoy Rendon and Senate President Pro Tem Toni Atkins took swipes at each other in George Skelton’s Los Angeles Times column Thursday. Morning Report: Downtown Jail Outbreak Goes From Bad to Worse |Voice of San Diego |September 11, 2020 |Voice of San Diego 

Swipe, pass judgment, see who likes you and see if anyone likes you back. Swipe Right For Sex: Mixxxer Is Tinder for the Porn Star Set |Aurora Snow |October 4, 2014 |DAILY BEAST 

We are going through and looking for people that we would honestly ‘swipe right’ for. Grindr and Tinder Help the Holy Land Make Love, Not War |Gideon Resnick |July 23, 2014 |DAILY BEAST 

“One swipe with a crowbar and he would have been down,” Sasha said. I Heard About the Latest Crazed Shooter While I Watched the World Cup with Guys He Almost Killed |Daniel Genis |July 1, 2014 |DAILY BEAST 

Meanwhile, Paul has claimed the mantle of Reaganism for his foreign policy views, in a direct swipe at Ted Cruz. Will Rand Paul’s Unorthodox Foreign Policy Fit in the GOP? |Kristen Soltis Anderson |April 9, 2014 |DAILY BEAST 

Flawless skin, perfect hair, and bright white teeth are all just a swipe away. New App, SkinneePix, Promises to Shave 15 Pounds Off Your Selfies |Charlotte Lytton |April 7, 2014 |DAILY BEAST 

And Robarts raised his bat, prepared for a good swipe if the ball came within reach, which he did not much expect. Dr. Jolliffe's Boys |Lewis Hough 

First they swipe my girl and erase all traces of her; then when I go looking they offer me help and sympathy for my distress. Highways in Hiding |George Oliver Smith 

So far, Security has played one nation against another enough to keep any from daring to swipe power on the planets. Police Your Planet |Lester del Rey 

I wade in as far as I can, and make a tremendous swipe with the rod. Mr. Punch With Rod and Gun |Various 

They've got their auto out in some sort of a shed and if we could run it we could swipe the whole thing. The Motor Rangers Through the Sierras |Marvin West