At these bountiful outdoor meals, slices of juicy watermelon served as dessert — traditional sweets were never very prevalent at all-church gatherings. A Blueberry-Glazed Cornbread Cake That Proves the Side Dish Can Be Dessert |Joy Cho |July 9, 2021 |Eater 

High heat encourages most of the watermelon’s, well, water to evaporate, distilling flavor and adding a little smokiness. The Best Way to Enjoy Summer Produce Is Over the Grill |Aliza Abarbanel |June 11, 2021 |Eater 

From there, the watermelons were inspected—nothing to see here, they were in better shape than they would’ve been with a human driving the whole time—then distributed to stores all over the state. A Self-Driving Truck Got a Shipment Cross-Country 10 Hours Faster Than a Human Driver |Vanessa Bates Ramirez |June 1, 2021 |Singularity Hub 

The reason the watermelons were in better shape was because they were a day fresher. A Self-Driving Truck Got a Shipment Cross-Country 10 Hours Faster Than a Human Driver |Vanessa Bates Ramirez |June 1, 2021 |Singularity Hub 

Look for flavors of red berries and watermelon, kissed with salty air and wild herbs. Fire up the grill and uncork this $15 French malbec for a fabulous pairing |Dave McIntyre |May 21, 2021 |Washington Post 

Even the watermelon he serves me is in keeping with a certain aesthetic—perfectly arranged squares, like a Mondrian painting. Gosta Peterson's Bohemian Rhapsody: Unpacking a Photographer's '60s Secrets |Lizzie Crocker |September 10, 2014 |DAILY BEAST 

WatermelonIn the world of thirst quenchers, watermelon weighs in as a major contender. 10 Ways to Stay Hydrated (That Aren’t Water) |DailyBurn |July 25, 2014 |DAILY BEAST 

“There was never any rifle, there was never any watermelon,” he says now. These Clinton Haters Can’t Quit the Crazy |David Freedlander |May 22, 2014 |DAILY BEAST 

Hell, think of the center cut from a watermelon, that sweetest part. Hillbilly Heaven: The History of Small-Batch Bourbon |Dane Huckelbridge |March 29, 2014 |DAILY BEAST 

After the maklouba, Laila treats Tony to fried watermelon stew. Watching Anthony Bourdain in Palestine |Maysoon Zayid |September 19, 2013 |DAILY BEAST 

I was going to ask you if you could tell me what you were doing up there and where you got that watermelon. Tabitha at Ivy Hall |Ruth Alberta Brown 

"I'm a comrade already," I said, meaning it as a merry jest, that I would be anything for a watermelon. The Iron Puddler |James J. Davis 

I counted the seeds in each slice of watermelon and gave that as the number of comrades in each mill. The Iron Puddler |James J. Davis 

Don't you remember we were once the pride of the school because we robbed watermelon patches so skilfully? Belford's Magazine, Vol. II, No. 3, February 1889 |Various 

So they are, and three peaches with sugar enough to sweeten them ought not to cost much, surely, nor would frozen watermelon. Living on a Little |Caroline French Benton