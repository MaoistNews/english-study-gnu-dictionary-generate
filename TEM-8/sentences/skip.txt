That seems like an opportunity for the app to have deeper playback controls, such as skip track backward, fast forward, and rewind, but perhaps JBL will add those in an update. JBL PartyBox 110 review: Make (and take) a splash |Markkus Rovito |September 16, 2021 |Popular-Science 

Even a brief skip across the floor after falling from the charger or a quick scrap against a wall can be enough contact to leave a scratch that will annoy you for the rest of the gadget’s life. The best Apple Watch case to protect the computer on your wrist |Stan Horaczek |August 23, 2021 |Popular-Science 

Ultimately, 2015 might be the year American anti-LGBT advocates wish they could skip. ‘Only God’ Can Stop Gay Marriage |Tim Mak |January 6, 2015 |DAILY BEAST 

But failing that, he advised pro-immigration reform Republican candidates such as former Gov. Jeb Bush to just skip the state. Can This Republican Bring the GOP Back to Its Senses on Immigration? |Tim Mak |December 29, 2014 |DAILY BEAST 

But the man appears so weary that I decide to skip the dull stuff and get to the heat. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The human attention span is evolving in such a way that they can skip around. Meghan Daum On Tackling The Unspeakable Parts Of Life |David Yaffe |December 6, 2014 |DAILY BEAST 

She jumped at the chance to watch RT, or jumped at the chance to skip calculus homework. Up To a Point: Binge Watching Putin's Propaganda Network |P. J. O’Rourke |September 20, 2014 |DAILY BEAST 

Well, though they do muster strong, we may make Edward's party skip for all that; if we have but justice on our side. The Battle of Hexham; |George Colman 

Nobody could read it twice, of 276 course; and the first time even it was necessary to skip. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

Now and then a gun was fired at the Indians, forcing them to skip nimbly behind the trees. Stories of Our Naval Heroes |Various 

It is probable that the word was the same in both passages,though whether skip or slip I have no means of determining. The Fatal Dowry |Philip Massinger 

Let him skip, if, like myself, he is weary; for the substance of the story is elsewhere given. History Of Friedrich II. of Prussia, Vol. VII. (of XXI.) |Thomas Carlyle