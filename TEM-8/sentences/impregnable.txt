It was a ponderous labyrinth of bolts, locks, and steel doors, making it an almost impregnable fortress. The High Society Bank Robber of the 1800s |J. North Conway |October 19, 2014 |DAILY BEAST 

When Sherman reached those earthworks, he thought them the most impregnable he had ever seen. Atlanta’s Fall Foretold The End Of Civil War Bloodshed |Marc Wortman |September 1, 2014 |DAILY BEAST 

His place in the hearts of most Thai is central and impregnable. The Real Crisis in Thailand is the Coming Royal Succession |Somchai Samizdat |February 22, 2014 |DAILY BEAST 

Meanwhile, the Russians made the salient virtually impregnable. WWII’s Greatest Battle: How Kursk Changed the War |Andrew Roberts |August 31, 2013 |DAILY BEAST 

But it was really the uemployment data that turned out to be his impregnable fortress. Forget Ohio—September Jobs Report Was Romney’s Undoing |Daniel Gross |November 7, 2012 |DAILY BEAST 

The Prussians held an apparently impregnable position on the Landgrafenberg, a precipitous hill which commanded the town. Napoleon's Marshals |R. P. Dunn-Pattison 

These are truths—fixed facts, that quaint theory and exhausted moralising, are impregnable to, and fall harmlessly before. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

This done, the fort was declared impregnable, and the tired travellers turned in, well assured of complete security. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

At the beginning of this year Antwerp, supposed to have been impregnable, surrendered to Marshal Gerard. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Here, at least, was a citadel impregnable by right-hand defections or left-hand extremes. Tales and Fantasies |Robert Louis Stevenson