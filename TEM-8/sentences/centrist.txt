She put forward Imamoglu, a mild-mannered centrist, to contest Istanbul’s mayoral race. The 'Badass Chief of Staff' of Turkey's Opposition Faces Years in Jail After Challenging Erdogan's Power. She's Not Backing Down |Joseph Hincks / Istanbul |February 24, 2021 |Time 

The move was in keeping with NBA Commissioner Adam Silver’s desire for a centrist position. The NBA’s week of controversies show how hard life is outside of the bubble |Ben Golliver |February 12, 2021 |Washington Post 

Bari Weiss, a centrist former Times opinion editor, recently tweeted support for Amash’s idea of forming a new political party. Bernie Sanders is often called a liberal. He’d beg to differ. Who is actually a liberal? |Graham Vyse |February 2, 2021 |Washington Post 

This moment in our nation is not about left, right, or centrist. 'We Must Have a Third Reconstruction.' Read the Full Text of the Rev. William J. Barber II's Sermon at the Inaugural Prayer Service |William J. Barber II |January 21, 2021 |Time 

Many business leaders were hoping for a different result—a divided government whose center of gravity would rest with centrists like Romney, Collins, Manchin and Warner. A coup attempt and an undivided government |Alan Murray |January 7, 2021 |Fortune 

More centrist Democrats will make a few gestures in the Warren direction, but nothing more. The Most Powerful Democrat in America |Michael Tomasky |December 15, 2014 |DAILY BEAST 

Another dark horse, Tennessee Senator Al Gore, was finding little traction in his efforts to become a centrist alternative. Want President Hillary? Then Primary Her |Jeff Greenfield |November 24, 2014 |DAILY BEAST 

The Duchess — more of a mainstream centrist and a bit of a belle-lettrist herself — was totally delightful. The Duchess Who Secretly Loved Elvis: Remembering Lunch with 'Debo,' The Last Mitford Sister |Lloyd Grove |September 27, 2014 |DAILY BEAST 

The centrist group thinks they can build centrist grassroots army. No Labels’ Bid For The Big Time |Ben Jacobs |September 18, 2014 |DAILY BEAST 

He is running as a centrist alternative to Tea Party-aligned GOP candidate Ben Sasse and Democrat Dave Domina, a trial lawyer. Candidates in Maine, Nebraska, Massachusetts, and Washington, D.C., Challenge Republicans and Democrats Alike |Linda Killian |July 5, 2014 |DAILY BEAST 

Only on one of the Centrist benches was any sincere attention paid, like that of eager scholars to their master's explanations. The conquest of Rome |Matilde Serao 

Besides, Sid and I were the centrist party of two in our fresh-out-of-the-shell Place politics. The Big Time |Fritz Reuter Leiber