That weekend, Willie made prime rib, which Renee said was the best she’s ever had. Date Lab: One of our setups is still going strong two years later. We caught up with Willie and Renee for an update. |Marin Cogan |February 11, 2021 |Washington Post 

Of course, I remember another where he did the same and took a really hard shot to the ribs and lost the ball on the way down. The Packers Kicked A Field Goal, Josh Allen Had A Bad Day, And The GOAT And Baby GOAT Are Super Bowl-Bound |Sara Ziegler (sara.ziegler@fivethirtyeight.com) |January 25, 2021 |FiveThirtyEight 

Throughout the ride, Dennis said, Danese put his knee on his back, punched him several times in the face and pulled his neck back in a prolonged chokehold while punching his ribs. Still Can’t Breathe |by Topher Sanders, ProPublica, and Yoav Gonen, THE CITY, video by Lucas Waldron, ProPublica |January 21, 2021 |ProPublica 

Popular materials for these winter hats include wool, cashmere, and rib-knit. Best winter hats: Comfortable hats to keep you warm |Carsen Joenk |January 20, 2021 |Popular-Science 

All-everything defensive tackle Aaron Donald exited the Rams’ first-round victory with a rib injury. What to know from NFL playoffs first round: Lamar Jackson wins and Tom Brady finds a way |Adam Kilgore |January 11, 2021 |Washington Post 

Veselka layered its latke with pork goulash, and Toloache added beef short rib chorizo. I Ate Potato Pancakes Til I Plotzed |Emily Shire |December 17, 2014 |DAILY BEAST 

Maybe I could turn all these quarters into some kind of rib-sticking casserole. ‘Asteroids’ & The Dawn of the Gamer Age |David Owen |November 29, 2014 |DAILY BEAST 

She suffered 10 broken bones, missing and broken teeth, a fractured rib, and a severely ruptured liver. Deep Thoughts from War Machine's Sexist, Racist Prison Blog |Melissa Leon |August 21, 2014 |DAILY BEAST 

French and Crown one rib rack and season with salt and pepper, cover exposed bones with foil, cook in oven at 350 for 2.5 hours. Epic Meal Empire’s Meat Monstrosities: From the Bacon Spider to the Cinnabattleship |Harley Morenstein |July 26, 2014 |DAILY BEAST 

Last year his left shoulder was dislocated, and a week later he was hospitalized for ten days with torn rib cartilages. Gordie Howe Hockey’s Greatest War Horse |W.C. Heinz |May 31, 2014 |DAILY BEAST 

Benny gave them no peace at all until they had admired his wonderful new stockings, and felt of each rib. The Box-Car Children |Gertrude Chandler Warner 

If glue is used, there is danger of breaking two or more ribs, should it be necessary to remove a broken or defective rib. The Boy Mechanic, Book 2 |Various 

He cud devote his life to paintin' wan rib iv a fan, f'r which he got two dollars, or he cud become a cab horse. Mr. Dooley Says |Finley Dunne 

A rib springs from the additional shafts to the centre of the corner column. Bell's Cathedrals: The Cathedral Church of Carlisle |C. King Eley 

The rapier had penetrated, just under the right breast, almost to the rib. The Child of Pleasure |Gabriele D'Annunzio