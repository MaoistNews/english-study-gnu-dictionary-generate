The trick to making this vacation as special as possible will be to go all-out and do things you wouldn’t ordinarily do, things that feel dreamy and extravagant no matter where you end up. How to Live Big with a Chronic Illness |Blair Braverman |October 2, 2020 |Outside Online 

An electric toothbrush—at once practical and extravagant—makes a surprisingly good gift. Electric toothbrushes that make great gifts |PopSci Commerce Team |October 1, 2020 |Popular-Science 

That doesn’t include the extravagant jewels and other trinkets used to adorn them. The Unlikely History of Acrylic Nails |Joshua Eferighe |September 30, 2020 |Ozy 

Moreover, he has managed to thrive despite being a teetotaler in a political party whose leaders are famed for wheeling and dealing at extravagant late-night drinking sessions. How the son of strawberry pickers became Japan’s most likely choice for next prime minister |claychandler |September 3, 2020 |Fortune 

When more extravagant luxuries seem out of reach, the index suggests, lipstick is an affordable treat. It is time for the “lipstick index” to go away for good |Alexandra Ossola |August 6, 2020 |Quartz 

At first glance, it might be tempting to interpret this extravagant level of compensation as a victory for the once-humble intern. Silicon Valley Interns Make a Service Worker’s Yearly Salary In Three Months |Samantha Allen |November 25, 2014 |DAILY BEAST 

Costumes worn by each reinvented persona act—in all their extravagant glory—serve as the anchors for the exhibit. The Making—and Remaking—of David Bowie |Justin Jones |September 23, 2014 |DAILY BEAST 

After 11 years at the e-commerce brand, CEO Mark Sebba got an extravagant global-wide send off as he plans to retire. Martha Stewart Poses for Terry Richardson; Net-a-Porter CEO Gets Epic Send Off |The Fashion Beast Team |July 31, 2014 |DAILY BEAST 

Expect rhinestone realness from pop's current queen of all things shiny, girly, and extravagant. The Top 10 Summer Concert Tours |The Daily Beast |June 9, 2014 |DAILY BEAST 

The openings have gone from simplistic to extravagant, featuring funny monologues, dance numbers, and lots of celebrities. Watch the Best Oscar Openers |Marina Watts |March 2, 2014 |DAILY BEAST 

I should have made the dumplings with more fruit in them, master, only Letty cautioned me not to be extravagant with the plums. The World Before Them |Susanna Moodie 

Immediately Aguinaldo had fallen captive, all kinds of extravagant and erroneous versions were current as to how it had happened. The Philippine Islands |John Foreman 

I thought the little fellow extravagant in his demands, but, rather than lose the chance, submitted. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

In their voyage up the river Niger, their description of the scenes is extravagant. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

In his dressing-room Diotti was besieged by hosts of people, congratulating him in extravagant terms. The Fifth String |John Philip Sousa