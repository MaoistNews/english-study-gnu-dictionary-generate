The report notes only a third of respondents “were actively engaged in economic activity at the time of the study, a majority depended on the food rations distributed in the camp.” Report details anti-LGBTQ discrimination, violence in Kenya refugee camp |Michael K. Lavers |October 20, 2021 |Washington Blade 

Unable to feed themselves, they were dependent on rations promised under treaties with the US—rations that were frequently late or stolen. Walker and the “Indian Question” |Simson Garfinkel ’87, PhD ’05 |August 24, 2021 |MIT Technology Review 

When 48-year-old Milan Kumar Jha read about the Narendra Modi government’s “One Nation One Ration” scheme in a newspaper in February, he immediately rushed to the nearest government ration shop. Why has the Modi government’s plan to make food grain more accessible not taken off? |Vijayta Lalwani |July 7, 2021 |Quartz 

This is because, until recently, ration beneficiaries in India could only draw grain from the ration shop where they are registered, usually the one closest to their native homes. Why has the Modi government’s plan to make food grain more accessible not taken off? |Vijayta Lalwani |July 7, 2021 |Quartz 

In addition to field-tested recipes, the book contains advice on on how to pack and ration supplies, as well as nutrition tips for long-distance hiking. A NOLS Instructor’s Favorite Backcountry Meals |Aleta Burchyski |May 31, 2021 |Outside Online 

And when word of that economic debacle spread, the government said it would ration imports of newsprint. Venezuela’s Audio Hoax Sees Chavez Speaking From the Grave |Mac Margolis |October 8, 2013 |DAILY BEAST 

This is the relevant passage: And who will suffer the most when they ration care? The Reality of Death Panels |Megan McArdle |October 31, 2012 |DAILY BEAST 

That first couple of weeks, before any contact had been made, they survived on what was meant to be a two-day ration of food. Behind Chile's Miracle Mine Rescue |Constantino Diaz-Duran |October 9, 2010 |DAILY BEAST 

I had a pleasant little dinner last night on Ration Beef at the General's. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie 

Our ration at this time consisted of raw corn-meal and sorghum molasses, without salt or any provision of utensils for cooking. Famous Adventures And Prison Escapes of the Civil War |Various 

She brings breakfast of coffee without milk and an omelette, but we always have our ration of bacon as well. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie 

The general served out a small ration of water and rum, every drop of which was precious. Famous Adventures And Prison Escapes of the Civil War |Various 

All summer we have had three calves that came to the orchard fence twice a day to get their ration of skim milk and feeding flour. The Red Cow and Her Friends |Peter McArthur