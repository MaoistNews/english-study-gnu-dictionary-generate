I would say that changing Section 230 would actually make those problems even worse, because you’re going to start to provide incentives for platforms to moderate less. Twenty-Six Words Created the Internet. What Will It Take to Save It? |Stephen Engelberg |February 9, 2021 |ProPublica 

Though we seemed to crest this wave a few weeks ago, epidemiologists are concerned that the upcoming holiday season and the travel and small gatherings that accompany it, could make the surge even worse. The US has topped 400,000 COVID-19 deaths |Sara Chodosh |January 20, 2021 |Popular-Science 

The pandemic has ravaged the local economy and could make the situation even worse. 7 Big Promises and Priorities From Gloria’s State of the City Address |Lisa Halverstadt and MacKenzie Elmer |January 14, 2021 |Voice of San Diego 

If it spreads here, it will make an already-bad situation even worse. We may have only weeks to act before a variant coronavirus dominates the US |Stephanie Arnett |January 13, 2021 |MIT Technology Review 

Meanwhile, climate change is making existing deficiencies worse. Battling America’s ‘dirty secret’ |Sarah Kaplan |December 17, 2020 |Washington Post 

A Republican candidate hoping to win red state support could find a worse team to root for than one from Dallas. Will Chris Christie Regret His Cowboy Hug? |Matt Lewis |January 5, 2015 |DAILY BEAST 

Among whites, the situation is also bad — in some ways, even worse. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

The headaches, fevers (and worse) you may experience on Thursday are nothing new. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

Or is it simply that what you are hearing and seeing about race in the media seems worse? Obama Is Right on Race. The Media Is Wrong. |Keli Goff |December 29, 2014 |DAILY BEAST 

Worse still is how much of this is being made into performance. Public Marriage Proposals Must Die |Tauriq Moosa |December 28, 2014 |DAILY BEAST 

Conditions in the new country had gone from bad to worse, and if the season should experience another drought, the worst was come. The Homesteader |Oscar Micheaux 

He saw that the situation was worse than even he had bargained for, and all his irresolution began to return upon him. Elster's Folly |Mrs. Henry Wood 

Hence, shortage of ammunition and shortage of water, which last was the worse felt to-day. Gallipoli Diary, Volume I |Ian Hamilton 

“Alas, mon bon Monsieur, it goes from bad to worse,” sighed the old man. The Joyous Adventures of Aristide Pujol |William J. Locke 

Lyn was looking at me intently, and her voice was steady; that squeezed kind of steadiness that is almost worse than tears. Raw Gold |Bertrand W. Sinclair