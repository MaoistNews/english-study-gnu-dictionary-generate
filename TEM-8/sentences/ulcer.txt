Some studies have found that excess salt brings on ulcers, infections, and may even hasten stomach cancer. What happens if you eat too much salt? |Alexandra Ossola |August 23, 2021 |Popular-Science 

Melioidosis infections can present like an infected wound, with swelling or ulcers at the exposure site, but they can also occur in the lungs, the bloodstream or widely spread throughout the body. CDC probes how people contracted a dangerous infection found in the tropics — without leaving the U.S. |Katie Shepherd |July 1, 2021 |Washington Post 

Three months after they appeared, the ulcers completely cleared up, leaving Gaither to wonder what really caused them. Flesh-eating parasites are on the rise in the US |Purbita Saha |June 22, 2021 |Popular-Science 

A lot of times people equate their abdominal pain with having an ulcer. Ibuprofen and ulcers: What Rep. Debbie Dingell’s emergency surgery can teach us |Allyson Chiu |May 26, 2021 |Washington Post 

But, experts said, the belief that certain foods and carbonated or acidic beverages can cause or worsen ulcers is a common misconception. Ibuprofen and ulcers: What Rep. Debbie Dingell’s emergency surgery can teach us |Allyson Chiu |May 26, 2021 |Washington Post 

Indeed, Rep. Paul Ryan may talk a good game about the poor, but his policies still give social-justice advocates an ulcer. How This Pope Is Remaking the GOP |Michelle Cottle |April 18, 2014 |DAILY BEAST 

The doctors suspected a heart issue or an ulcer and recommended he follow up with his regular physician. No Answers in Death of Technician Linked to Andrew Breitbart |Christine Pelisek |November 30, 2012 |DAILY BEAST 

Talk about a Super Tuesday designed to give all parties involved a massive ulcer. Super Tuesday: Mitt Romney’s Senior Citizen Surge |Michelle Cottle |March 7, 2012 |DAILY BEAST 

Buffered and enteric-coated aspirin do not eliminate the risk of developing an ulcer. Could a Daily Aspirin Be Deadly? |Arthur Agatston, M.D. |February 23, 2010 |DAILY BEAST 

Except in gastric ulcer, the danger lies in the retching produced, and the tube can safely be used if the patient takes it easily. A Manual of Clinical Diagnosis |James Campbell Todd 

When found in the fluid removed after a test-meal, it commonly points toward ulcer or carcinoma. A Manual of Clinical Diagnosis |James Campbell Todd 

Recognition of occult hemorrhage has its greatest value in diagnosis of gastric cancer and ulcer. A Manual of Clinical Diagnosis |James Campbell Todd 

And among others, Sir J. Denham he told me he had cured, after it was come to an ulcer all over his face, to a miracle. Diary of Samuel Pepys, Complete |Samuel Pepys 

It chanced one day, when he was fifteen, he went into the woods, and the ulcer pained him. The Works of Robert Louis Stevenson, Volume XXI |Robert Louis Stevenson