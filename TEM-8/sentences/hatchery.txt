There are hatcheries right now that we have, they’re in Alaska, other places, that need community-college-level workers. Is the Future of Farming in the Ocean? (Ep. 467) |Stephen J. Dubner |June 24, 2021 |Freakonomics 

Now about 35 percent are, and the rest require at least some support from hatchery fish. COVID drove more people to go fishing, but at what cost? |Purbita Saha |June 23, 2021 |Popular-Science 

Not only would chick culling end but hatcheries wouldn’t “lose” half of all fertilized eggs that would have hatched into males. Why the US egg industry is still killing 300 million chicks a year |Tove K. Danovich |April 12, 2021 |Vox 

In July 2020, the UEP put out a statement saying it was still looking for “an economically feasible, commercially viable alternative to the practice of male chick culling at hatcheries.” Why the US egg industry is still killing 300 million chicks a year |Tove K. Danovich |April 12, 2021 |Vox 

The organization will bury the eggs at its hatcheries and, about two months later, bid the hatchlings adieu as they swim off to sea. A virtual menagerie: How conservation and rescue efforts can connect us with wildlife around the world |Andrea Sachs |January 15, 2021 |Washington Post 

What a to-do they make when you approach their outdoor hatchery! Birds of the Rockies |Leander Sylvester Keyser 

He began by dumping out into a worthless and landlocked bass-pond every brown trout in the hatchery. The Firing Line |Robert W. Chambers 

Salmon have a peculiarity which makes it easy for the hatchery people. Bill Bruce on Forest Patrol |Henry Harley Arnold 

At the head of the lake is a hotel and a fish hatchery; no store, no factory, not even a Chautauqua. Days in the Open |Lathan A. Crandall 

The female sun-fish (called, I believe, in England, the roach or bream) makes a "hatchery" for her eggs in this wise. Scientific American Supplement, No. 711, August 17, 1889 |Various