I’m saying maths is full of ways that you can think carefully, but it doesn’t have to be slow. What’s Wrong With Shortcuts? (Ep. 483) |Stephen J. Dubner |November 18, 2021 |Freakonomics 

Surprising examples are a big part of what makes maths fun and keeps it weird and wonderful. Mathematician Disproves 80-Year-Old Algebra Conjecture |Erica Klarreich |April 12, 2021 |Quanta Magazine 

So, I would change the curriculum to really reflect real mathematics, and I would also change it to reflect the 21st century, because maths still looks in classrooms pretty much as it did in Victorian days. America’s Math Curriculum Doesn’t Add Up (Ep. 391) |Steven D. Levitt |October 3, 2019 |Freakonomics 

“Well, I did maths at school,” he replied as evenly as possible. Why 7 Times 8 Tripped Up the UK Chancellor |Tim Teeman |July 4, 2014 |DAILY BEAST 

It's very tiresome, for soon the other school insp. for maths. A Young Girl's Diary |An Anonymous Young Girl 

Opal Earnshaw was fearfully angry that you beat her in maths. A Fortunate Term |Angela Brazil 

One morning just after breakfast Gordon discovered that he had done the wrong maths for Jenks. The Loom of Youth |Alec Waugh 

Im a Soph myself, by rights, if old Hammond hadnt marked me low in maths. Tom Fairfield's Schooldays |Allen Chapman 

Laura Kirby's A1 at maths., so I'm afraid you won't meet with too tender a reception. The Girls of St. Cyprian's |Angela Brazil