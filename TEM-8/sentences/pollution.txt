Reductions in air pollution therefore trigger potential for growth in economic activity through decreased absenteeism and increased worker productivity. Can emissions cuts and economic growth coexist? Europe is certain they can |David Meyer |September 17, 2020 |Fortune 

That ability may imply that the moth can find food and pollinate plants, including crucial crops, despite some air pollution, researchers report September 2 in the Journal of Chemical Ecology. This moth may outsmart smog by learning to like pollution-altered aromas |Carmen Drahl |September 11, 2020 |Science News 

Research has long supported an association between asthma and exposure to air pollution. New Research Shows Disproportionate Rate of Coronavirus Deaths in Polluted Areas |by Lylla Younes, ProPublica, and Sara Sneath |September 11, 2020 |ProPublica 

It’s a good thing, because ridesharing actually causes more pollution than driving one’s own car. Uber Wants to Go All-Electric by 2030. It Won’t Be Easy |Vanessa Bates Ramirez |September 10, 2020 |Singularity Hub 

Cars still add a lot of pollution, and are a dominant source of ozone. City pavement is a big source of air pollution |Ula Chrobak |September 3, 2020 |Popular-Science 

That is a lot of air pollution, noise, and yet more kicking up of dust. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

No one really argues with the massive amount of pollution and toxins. Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 

In certain storylines, illegal industrial pollution is just as much a villain as the vengeful monsters it creates. A Political History of the ‘Teenage Mutant Ninja Turtles’ |Asawin Suebsaeng |August 6, 2014 |DAILY BEAST 

Burning charcoal generates hydrocarbons, particulate air pollution, wildfires, and carbon footprints. P.J. O'Rourke: 27 Sensitive, Caring, Green, and Politically Committed Reasons to Ban July 4th |P. J. O’Rourke |July 3, 2014 |DAILY BEAST 

For many of us, plastic pollution means the debris we can see. Your Favorite Facewash Is Hurting Nemo |Alexa C. Kurzius |June 18, 2014 |DAILY BEAST 

The whole valley is considered sacred, and hence strongly guarded against the pollution of any heretical evangelical influences. Silver Chimes in Syria |W. S. Nelson 

To her the plague was better than self-slaughter, as self-slaughter was better than pollution. The Proud Prince |Justin Huntly McCarthy 

Fundulus kansae has been found in the lower part of the Walnut River Basin, especially where petroleum pollution was evident. Fishes of Chautauqua, Cowley and Elk Counties, Kansas |Artie L. Metcalf 

Pollution by petroleum wastes from refineries has also affected the streams studied. Fishes of Chautauqua, Cowley and Elk Counties, Kansas |Artie L. Metcalf 

The pollution (miasma) of sin is precipitated by his blood, the power of sin is conquered by his Spirit. Aids to Reflection |Samuel Taylor Coleridge