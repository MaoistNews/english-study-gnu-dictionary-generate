But enough humus–time to get back outside, and see a little archeology! An Alternative Travel Itinerary for Mitt Romney |Emily L. Hauser |July 13, 2012 |DAILY BEAST 

The particular phenomena of vegetation also afford abundant evidence that humus cannot be the only source of carbon. Elements of Agricultural Chemistry |Thomas Anderson 

It did not notice Forrester scurrying away in the shape of an ant through the leaves and thick humus of the jungle floor. Pagan Passions |Gordon Randall Garrett 

The generation of the humus-acids is probably hastened during the digestion of the many half-decayed leaves which worms consume. The Ontario Readers: The High School Reader, 1886 |Ministry of Education 

The upper few inches of soil is humus rich in organic matter; below this is clay. Amphibians and Reptiles of the Rainforests of Southern El Peten, Guatemala |William E. Duellman 

See that the soil contains enough humus or vegetable mold to make it rich and to enable it to hold moisture. The Practical Garden-Book |C. E. Hunn