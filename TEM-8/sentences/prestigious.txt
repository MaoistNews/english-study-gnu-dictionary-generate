Sherri Anne Green is an award-winning Realtor with Coldwell Banker Residential Brokerage having earned the prestigious International President’s Circle Award designating her among the top 5% internationally. Know your limits when it comes to DIY |Sherri Anne Green |August 15, 2020 |Washington Blade 

The instances of perhaps college football’s most prestigious program resisting temptations to join a conference are far too many to name here. Notre Dame Didn’t Want To Join A Conference. But The ACC Could Have Helped It Win. |Jake Lourim |August 10, 2020 |FiveThirtyEight 

There is a significant gender gap in prestigious economics journals, according to new analysis of decades of data. Only 17% of the studies published in top economics journals are by women |Karen Ho |August 5, 2020 |Quartz 

Big tech isn’t the only sector hiring top artificial intelligence talent from the US’s most prestigious PhD programs. The non-tech companies snapping up PhDs from America’s elite AI programs |Nicolás Rivero |August 3, 2020 |Quartz 

Established in 1942, the Regeneron Science Talent Search, or STS, is the nation’s oldest and most prestigious science and math competition. For teens, big problems may lead to meaningful research |Carolyn Wilke |July 28, 2020 |Science News For Students 

His photography has won more than a hundred awards, including the prestigious Alfred Eisenstaedt Award for Magazine Photography. The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 

Joan used words like hip, classy, elegant, and prestigious to describe the clubs. Joan Rivers: The Playboy Bunnies Weren’t Sluts! |Patty Farmer |November 7, 2014 |DAILY BEAST 

She reportedly studied French and Italian at Oxford before attending the prestigious Jacques Lecoq school of theatre in Paris. Benedict Cumberbatch Announces Engagement in The Times |Tom Sykes |November 5, 2014 |DAILY BEAST 

In 1998, she was selected to represent Israel in the prestigious Eurovision contest, winning first place. Trans in the Holy Land: ‘Marzipan Flowers,’ Tal Kallai, and the Shattering of Israel’s LGBT Taboos |Itay Hod |November 4, 2014 |DAILY BEAST 

Beck later went on to receive a Ph.D. in Organizational Behavior at Harvard, and teach at a number of prestigious universities. The Intern Who Birthed The KAL007 Conspiracy Theories |Tim Mak |September 8, 2014 |DAILY BEAST 

These are some of the prestigious merits of the bicycle, though many more might be added. Hortus Vitae |Violet Paget, AKA Vernon Lee 

This was the title of the cover page of the prestigious magazine, "The Economist" in its issue of 10/1/98. After the Rain |Sam Vaknin 

He had had nothing out of them—nothing of the prestigious or the desirable things of the earth, craved for by predatory natures. Chance |Joseph Conrad 

The "prestigious feat" of causing flowers to appear in winter was a common one. A Handbook to the Works of Browning (6th ed.) |Mrs. Sutherland Orr