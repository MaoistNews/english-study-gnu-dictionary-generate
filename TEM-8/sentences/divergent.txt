Those two divergent developments — his two-pick, 12-of-27 performance against Northwestern in the Big Ten championship game and Friday night’s masterpiece — were related. Justin Fields put No. 1 Alabama — and the NFL — on notice with a masterful Sugar Bowl |Barry Svrluga |January 2, 2021 |Washington Post 

Your thoughts can be absolutely right on, but your practices can be completely divergent. The Man Who Saw the Pandemic Coming - Issue 94: Evolving |Kevin Berger |December 30, 2020 |Nautilus 

Over time, increasingly complex regulatory issues and divergent ideologies will create either separate platforms, or platforms that ostensibly have the same name but deliver fundamentally different user experiences in different geographies. The fragmentation of everything |Martha Leibs |December 4, 2020 |MIT Technology Review 

Tensions between Washington and Idaho over their divergent responses to the pandemic escalated in October. States With Few Coronavirus Restrictions Are Spreading the Virus Beyond Their Borders |by David Armstrong |December 1, 2020 |ProPublica 

The three sectors often had wildly divergent reasons for engaging. Better conversations: The 7 essential elements of meaningful communication |matthewheimer |November 24, 2020 |Fortune 

In the Divergent series, factions are at war with each other. Why 'The Giver' Movie Will Disappoint the Book's Fans |Kevin Fallon |August 15, 2014 |DAILY BEAST 

Before Divergent, before The Hunger Games, there was The Giver. A Trailblazer in YA Dystopian Fiction: An Interview With 'The Giver' Author Lois Lowry |Marianne Hayes |August 12, 2014 |DAILY BEAST 

So during my last hiatus, I really wanted to do a movie, and Divergent came along, so I did it. Tony Goldwyn Tackles Political Scandal Again on ‘The Divide’ |Jason Lynch |July 16, 2014 |DAILY BEAST 

Most interesting, Goldblatt explicates the role of soccer in various divergent cultures. The Literature of Futbol: 11 Great Books About Soccer |Robert Birnbaum |June 25, 2014 |DAILY BEAST 

We can entertain these divergent visions of the future because same-sex marriage was really a campaign, not a movement. Were Christians Right About Gay Marriage All Along? |Jay Michaelson |May 27, 2014 |DAILY BEAST 

We shall have here two divergent lines of approach within parallel fields. We're Friends, Now |Henry Hasse 

This doctrine has been followed, and is still followed, by the majority of men; it is the source of divergent beliefs and acts. My Religion |Leo Tolstoy 

Opinions as to the wisdom of giving such manuals to penitents are certainly very divergent. The Sexual Life of the Child |Albert Moll 

The divergent interests of the farming, free trade West and of the manufacturing, protectionist East made for friction. The Canadian Dominion |Oscar D. Skelton 

The two creeds represent two absolutely divergent sections of humanity. Islam Her Moral And Spiritual Value |Arthur Glyn Leonard