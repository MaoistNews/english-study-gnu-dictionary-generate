For the death of one of its members cannot, I am determined, dissolve the T.C.B.S… Yes, publish…. Why World War I Is at the Heart of ‘Lord of the Rings’ |John Garth |July 29, 2014 |DAILY BEAST 

Boundaries between gay and straight and public and private dissolve. New York’s Naughtiest Show (Maybe Avoid the Front Row) |Tim Teeman |January 18, 2014 |DAILY BEAST 

Hasina will almost certainly dissolve the government and call fresh elections. Bangladesh’s Radical Islamists Get U.S. Backing |Kapil Komireddi |January 12, 2014 |DAILY BEAST 

“YCT come get me,” they chanted until the crowd began to dissolve. How a Game of Tag Turned a Texas Campus Into Immigration’s Ground Zero |Kelsey McKinney |November 21, 2013 |DAILY BEAST 

Fans grow up, and their youthful interests quickly dissolve. Miley Cyrus’s Album ‘Bangerz’ Is Totally Schizo and Catchy as Hell |Marlow Stern |October 7, 2013 |DAILY BEAST 

When treated with hydrochloric or acetic acid they slowly dissolve and rhombic crystals of uric acid appear. A Manual of Clinical Diagnosis |James Campbell Todd 

Upon addition of acetic acid they dissolve, and rhombic plates of uric acid appear. A Manual of Clinical Diagnosis |James Campbell Todd 

Dissolve the suspected stain in a few drops of normal salt solution upon a slide. A Manual of Clinical Diagnosis |James Campbell Todd 

Should an association dissolve, then the members may divide its property among themselves. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

He repairs to it with eagerness, and clings to it with a tenacity that time cannot relax, nor all the agonies of death dissolve. The Ordinance of Covenanting |John Cunningham