If they were submerged for longer, the plants might have drowned, Armitage notes. Soggy coastal soils? Here’s why ecologists love them |Alison Pearce Stevens |September 17, 2020 |Science News For Students 

One can imagine the captain of the Titanic insisting on pointing out that 900 people didn’t drown. The problem with Trump’s ‘herd mentality’ line isn’t the verbal flub. It’s the mass death. |Philip Bump |September 16, 2020 |Washington Post 

Any student of modern math must know what it feels like to drown in a well of telescoping terminology. Why Mathematicians Should Stop Naming Things After Each Other - Issue 89: The Dark Side |Laura Ball |September 2, 2020 |Nautilus 

Nala is Ophelia, Simba’s love, though thankfully she doesn’t go mad from being ignored by him and drown. 7 daring movie adaptations of literary classics |Allegra Frank |August 28, 2020 |Vox 

It could be how I so clearly remember the night I almost drowned. Towards ‘Eternal Sunshine’? New Links Found Between Memory and Emotion |Shelly Fan |July 28, 2020 |Singularity Hub 

It seems like, since we live in the sound bite era, grabby headlines like “EBOLA” and “ISIS” tend to drown out those numbers. Jon Stewart Talks ‘Rosewater’ and the ‘Chickensh-t’ Democrats’ Midterm Massacre |Marlow Stern |November 9, 2014 |DAILY BEAST 

Goldman, wisely, does not raise a raft of questions that drown a writer in the answering. Mexico City: Francisco Goldman’s Other Lost Love |Jason Berry |September 25, 2014 |DAILY BEAST 

Watching her drown her sorrows in hooch and then get beat up by Crazy Eyes in the showers was ghastly…but great television. Inside ‘Orange Is the New Black’ S2, Eps. 6-12: About That Shocking Incest Scene |Kevin Fallon, Marlow Stern |June 20, 2014 |DAILY BEAST 

If prioritizing guns over dead kids makes you angry, stand up and drown his words out with action. Joe the Plumber’s ‘Dead Kid’ Callousness |Erica Lafferty |May 29, 2014 |DAILY BEAST 

The ice breaks, the Reds drown, the Whites rally to take the Island. This 1979 Novel Predicted Putin’s Invasion Of Crimea |Michael Weiss |May 18, 2014 |DAILY BEAST 

Consequently, I haven't been very bright, though I am gradually coming up to the surface again, for I'm pretty hard to drown! Music-Study in Germany |Amy Fay 

He noticed the date on the hotel calendar, and realised that the Fates had another ten days in which to drown him. Uncanny Tales |Various 

"If you had been Reff you wouldn't have run away and left me to drown," went on Coulter, stubbornly. The Mystery at Putnam Hall |Arthur M. Winfield 

And the noise it makes is something terrific, I assure you—loud enough to drown half-a-dozen pianos. Punch, or the London Charivari, Vol. 109, July 27, 1895 |Various 

She threatened to go beyond sea, to throw herself out of window, to drown herself. The History of England from the Accession of James II. |Thomas Babington Macaulay