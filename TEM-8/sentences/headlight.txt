We stood on its head for several minutes, watching tiny headlights pierce the twilight below. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

Base models boast LED headlights, heated outside mirrors and smartphone compatibility. Top rides for 2021 |Joe Phillips |January 15, 2021 |Washington Blade 

Hospitals have frozen, like deer in the headlights, and put in place questionable policies. The lesson from the botched COVID vaccine rollout: Sometimes you need ‘Big Government’ |Matthew Heimer |January 13, 2021 |Fortune 

The larger model, which is slated to have working headlights and taillights, sold out within hours. Tesla Cybertruck Hot Wheels toy ships late—just like real Tesla cars |Timothy B. Lee |December 17, 2020 |Ars Technica 

The teal color is a nice touch, as are the lighted headlights. Holiday gifts for gearheads |Joe Phillips |November 23, 2020 |Washington Blade 

As he drove through the headlight-broken darkness now, other cars passed him bringing other men home from other jobs. Gordie Howe Hockey’s Greatest War Horse |W.C. Heinz |May 31, 2014 |DAILY BEAST 

So if you are stopped for jaywalking or driving with a broken headlight, the police can ask to see your papers. What the Supreme Court’s Arizona Ruling Means for Immigration and Health Care |Adam Winkler |June 25, 2012 |DAILY BEAST 

Away off to the southwest a bright light showed briefly—the headlight of a Santa Fe train, he guessed it must be. Cabin Fever |B. M. Bower 

One of the men stood on the track waving the red lantern; we could see him plainly in the glare of the headlight. The Wreckers |Francis Lynde 

Close below the headlight was a moving shadow they finally made out as company men, they could not tell how many. Mountain |Clement Wood 

As the train rolled up, the headlight flashing far down the track and the steam hissing from the engine, I turned weak all over. Sixes and Sevens |O. Henry 

The pilot doubled back into the ponies, and the headlight was scoured with nut, pea, and slack; but the stack was hardly bruised. The Nerve of Foley |Frank H. Spearman