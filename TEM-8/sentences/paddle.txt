This video follows professional kayaker Dane Jackson and his friends as they paddle through one of its narrowest sections, only accessible by kayak. Paddling One of the Narrowest Rivers in the World |Outside Editors |September 11, 2020 |Outside Online 

The moment you land, two paddlers hop out, hoist the two packs and paddles, and hit the trail. How (and Why) to Execute the Perfect Canoe Portage |Alex Hutchinson |September 9, 2020 |Outside Online 

This means that on portages, two people each grab a bag and some paddles, and the third person hoists the canoe. How (and Why) to Execute the Perfect Canoe Portage |Alex Hutchinson |September 9, 2020 |Outside Online 

These breaks can vary from beginner to advanced, depending on where you paddle out, how shallow the sandbank is, and how strong the current is. Carissa Moore's 5 Favorite Surf Towns |Megan Michelson |September 9, 2020 |Outside Online 

Meanwhile, the rover’s back wheels paddled from side to side. Wiggly wheels might help rovers plow through loose lunar soils |Maria Temming |June 26, 2020 |Science News For Students 

Others plan where to paddle into the surf off Black's Beach to catch a wave when the big breakers start rolling in. Finding Food Heaven on the Cali Coast |Jane & Michael Stern |August 17, 2014 |DAILY BEAST 

Ever obliging, Springsteen then flexed all the right things on a paddle board. Bruce Springsteen’s Bond Moment: The Boss’ Body Beautiful at 64 |Tim Teeman |July 25, 2014 |DAILY BEAST 

His paddle had “FAH-Q” painted on one side, and “O.B./Badass” painted on the other. ‘Dazed and Confused’ 20th Anniversary: 20 Craziest Facts About the Cult Classic |Marlow Stern |September 24, 2013 |DAILY BEAST 

I have good balance, but it was basically the first time I had ever been on a paddle board. Off the Hook: Eric Young’s Craziest Shark Catches (Video) |Anna Klassen |August 5, 2013 |DAILY BEAST 

What do you call it when a husband beats his wife with a paddle for disobeying him? Spanking for Jesus: Inside the Unholy World of ‘Christian Domestic Discipline’ |Brandy Zadrozny |June 19, 2013 |DAILY BEAST 

Then she would turn him over on his back and paddle his stomach with a ladle to make sure that he was well filled! Our Little Korean Cousin |H. Lee M. Pike 

The girls were dressed in such boating costumes as gave them the very freest movement, and they both used the paddle skillfully. The Campfire Girls of Roselawn |Margaret Penrose 

But suddenly Jessie drove her paddle deep into the water and sent the canoe in a dash to the landing. The Campfire Girls of Roselawn |Margaret Penrose 

The 'Syria' was originally a paddle-wheel steamer, having oscillating cylinders worked with steam of 25 lbs. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

An article in 'The Times' gives in strong contrast the relative value of screw and paddle-wheels as propellers. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick