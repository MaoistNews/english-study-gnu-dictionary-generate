The legislative counsel wrote that “legislating remotely arguably violates the constitutional guarantee of open and public meetings,” and that it could also violate Prop. Sacramento Report: Jones, COVID-19 and the Irony of Remote Voting |Sara Libby and Jesse Marx |August 28, 2020 |Voice of San Diego 

Google named a new top lawyer on Tuesday, appointing a 14-year veteran of the company, Halimah DeLaine Prado, as its new general counsel. Google names Halimah DeLaine Prado as new general counsel |Jeff |August 25, 2020 |Fortune 

From the first day Shmuel Sunray joined NSO as its general counsel, he faced one international incident after another. Inside NSO, Israel’s billion-dollar spyware giant |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

On par with speeding tickets, infractions don’t entitle defendants to legal counsel or a trial by jury. SDPD Says it Will Stop Enforcing Seditious Language Law |Kate Nucci |August 17, 2020 |Voice of San Diego 

Voters will also decide whether the city should create a new police oversight commission with independent legal counsel and subpoena power. Morning Report: Tensions, Then a Pricey Resignation at National School District |Voice of San Diego |July 20, 2020 |Voice of San Diego 

Currently, Israel is holding around 470 Palestinians without charge or access to legal counsel. The Strange Case of the Christian Zionist Terrorist |Creede Newton |December 14, 2014 |DAILY BEAST 

Israel commonly holds people suspected of threatening national security for long periods without access to legal counsel. The Strange Case of the Christian Zionist Terrorist |Creede Newton |December 14, 2014 |DAILY BEAST 

Brown sought his counsel as he prepared to start college and make a life of his own. 90 Seconds of Fury in Ferguson Are the Key to Making Peace in America |Michael Daly |November 26, 2014 |DAILY BEAST 

She goes to church with her husband, but when the pastor asks to counsel her in private, she shuts him down. The Good Wife’s Religion Politics: Voters Have No Faith in Alicia's Atheism |Regina Lizik |November 24, 2014 |DAILY BEAST 

At LLGS, you certainly felt proud to be able it to offer help or counsel. Sex, Suicide, and Homework: The Secret World of the Telephone Hotline |Tim Teeman |November 20, 2014 |DAILY BEAST 

Gold and silver make the feet stand sure: but wise counsel is above them both. The Bible, Douay-Rheims Version |Various 

Consult not with him that layeth a snare for thee, and hide thy counsel from them that envy thee. The Bible, Douay-Rheims Version |Various 

Every counsellor giveth out counsel, but there is one that is a counsellor for himself. The Bible, Douay-Rheims Version |Various 

Bahadur Shah held out against the vehement urging of his daughter aided now by the counsel of her brothers. The Red Year |Louis Tracy 

Take counsel together, and it shall be defeated: speak a word, and it shall not be done: because God is with us. The Bible, Douay-Rheims Version |Various