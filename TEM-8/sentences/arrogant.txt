We don’t need to get into the details but if you want to read up on him, just do a search for de Blasio and then “arrogant” or “hypocrite” or even “schmuck.” Is New York City Over? (Ep. 434) |Stephen J. Dubner |October 8, 2020 |Freakonomics 

A recent lawsuit had laid bare the reality of the upper management at the company, a boys-only club of powerful and arrogant men with outsize egos in an echelon that even the most competent and talented of women had trouble reaching. Inside a secret running program at Nike and a win-at-all-costs corporate culture |Rachel King |October 6, 2020 |Fortune 

The other side of the coin of this glowing account is that Nigerians are also seen as arrogant people coming from a corrupt country who have exported their corruption abroad in the form of internet scams. At 60, Nigeria Is Still A Country Of The Future |cmurray |October 1, 2020 |Essence.com 

They did not engage with this investor or his firm ever again, but the exchange was the most egregiously arrogant and dismissive example of a whole set of responses they received from the predominantly male venture capital class. ‘How I Built This’ host Guy Raz on insights from some of the world’s most famous entrepreneurs |Rachel King |September 15, 2020 |Fortune 

It had been arrogant of me to think we could stroll up to some of the least-studied birds in Northeast Asia and assume they’d hand us their secrets. The quest to snare—and save—the world’s largest owl |Jonathan Slaght |August 28, 2020 |Popular-Science 

Mizell called the Justice Department “arrogant” for bringing this frivolous case to the court. Honoring The Late John Doar, A Nearly Forgotten Hero Of The Civil Rights Era |Gary May |November 15, 2014 |DAILY BEAST 

Preening, arrogant, vindictive, and inexorable; awash with cash; corrupt; in bed with corporate America and big finance. Meet The Democrats’ Secret Savior Against Cuomo Corporatism |James Poulos |September 14, 2014 |DAILY BEAST 

He struck many people there, at the time, not in retrospect, as arrogant and cold. When I Met Robin Williams in Afghanistan |Matthew Kaminski |August 20, 2014 |DAILY BEAST 

All that would be tolerable if he were an arrogant jerk, or just plain dumb. Castro Street’s Hot Cop Is the Batman to Sexy Mug Shot Guy’s Joker |Itay Hod |July 9, 2014 |DAILY BEAST 

It was arrogant,” he says, adding, “If you go along with the status quo in South Carolina, you can break the law with impunity. T-Rav: The Reality TV Star Running for Senate in South Carolina |Patricia Murphy |July 4, 2014 |DAILY BEAST 

Despite the arrogant manner of his address, Garnache felt prepossessed in the newcomer's favour. St. Martin's Summer |Rafael Sabatini 

Parr was an arrogant old coxcomb, who abused the respectful kindness he received, and took his pipe into drawing-rooms. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Because of his enormous wealth and arrogant manner, he was nicknamed "King" Carter. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

The most ferocious and arrogant Mindanao tribes occupy regions within easy access of the coast. The Philippine Islands |John Foreman 

How vain, how arrogant the babblings of the sectarians who tell us that the book of revelation is forever closed! Gospel Philosophy |J. H. Ward