High winds broke branches above the water and stripped off their leaves. Soggy coastal soils? Here’s why ecologists love them |Alison Pearce Stevens |September 17, 2020 |Science News For Students 

If those wind patterns themselves stall, slow down dramatically, or change directions rather abruptly, the hurricane will be sort of directionless and it can sit there stalling. Slow, meandering hurricanes are often more dangerous—and they’re getting more common |Greta Moran |September 9, 2020 |Popular-Science 

That event may take the wind out of the traditional Cyberweek sales if people spend their money early. A Corona Xmas: Why physical stores will power online shopping this holiday season |Greg Sterling |September 4, 2020 |Search Engine Land 

When winds from the north dominated, and there was more sea ice, there was less oxygen-18 in the cellulose. Bering Sea winter ice shrank to its lowest level in 5,500 years in 2018 |Carolyn Gramling |September 3, 2020 |Science News 

Researchers say still don’t know what whipped up this newly discovered wind event. Here’s the summer science you might have missed |Janet Raloff |September 1, 2020 |Science News For Students 

Increasingly, as these industries develop, on-site solar and wind is a way of guaranteeing a lower price for electricity. Solar Powered Ski Lift |The Daily Beast |November 24, 2014 |DAILY BEAST 

One and all, they come shaking their tin cups at election time then run like the wind when a critical vote comes up. How the Lame Democrats Blew It |Goldie Taylor |November 5, 2014 |DAILY BEAST 

For instance: suppose the Republicans wind up with a clear Senate majority on November 4th. Election Day In The Big Sleazy |Jeff Greenfield |November 2, 2014 |DAILY BEAST 

But as it takes away the safety net, their corpses wind up in fishing nets. Britain’s Let-Em-All-Die Policy |Nico Hines, Barbie Latza Nadeau |November 1, 2014 |DAILY BEAST 

But then they saw which way the post-Citizens United wind was blowing and became anti-disclosure. Dark Money and Our Looming Oligarchy |Michael Tomasky |October 22, 2014 |DAILY BEAST 

There are three things a wise man will not trust: the wind, the sunshine of an April day, and woman's plighted faith. Pearls of Thought |Maturin M. Ballou 

But there was a breeze blowing, a choppy, stiff wind that whipped the water into froth. The Awakening and Selected Short Stories |Kate Chopin 

The man that giveth heed to lying visions, is like to him that catcheth at a shadow, and followeth after the wind. The Bible, Douay-Rheims Version |Various 

It was a cloudy, stormy evening: high wind was blowing, and the branches of the trees groaned and creaked above our heads. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The ne'er-do-well blew, like seed before the wind, to distant places, but mankind at large stayed at home. The Unsolved Riddle of Social Justice |Stephen Leacock