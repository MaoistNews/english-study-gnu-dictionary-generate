He added that deGrom combines an ideally long, lean pitching build with exceptional mechanics. Jacob DeGrom Just Keeps Throwing Faster |Travis Sawchik |September 17, 2020 |FiveThirtyEight 

The view that mail-in ballots are rife with voter fraud is most widely held among Republicans and GOP-leaning independents who use only Fox News Channel or talk radio as their main sources of political news. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

We learned to lean in with our partners to help provide Honda with a credible voice. ‘A credible voice’: Why Honda is doubling down on esports |Lara O'Reilly |September 16, 2020 |Digiday 

So CEO Tim Cook and his team leaned hard on the company’s smartwatch to build excitement. Two new Apple Watches announced at Apple’s ‘Time Flies’ event |Aaron Pressman |September 15, 2020 |Fortune 

For almost two months, from a few weeks before the team dealt Capela to a few weeks after it had fully leaned into that short-ball style, Westbrook was fantastic. Everything Should Be On The Table For The Houston Rockets. Even James Harden’s Future. |Chris Herring (chris.herring@fivethirtyeight.com) |September 14, 2020 |FiveThirtyEight 

In 2012, as a 10th grader, Lean says he recorded his first legitimate song, “Hurt.” The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

Yung Lean was born Jonatan Leandoer Håstad in Belarus, before moving to Sweden at the age of 3. The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

The trio formed the Sad Boys collective, with Sherm and Gud on production and Lean manning the mic. The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

“We broke off shortly after because we were more ambitious,” says Lean. The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

His flesh is sagging a bit, but he is still trim and looks lean, sinewy and tough. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

When she got to the cadenza, he laid down his bton, and retired to lean against the door and enjoy it. Music-Study in Germany |Amy Fay 

Past thirty all men begin to dry up or fatten, and he was certainly a lean person. The Soldier of the Valley |Nelson Lloyd 

He was a dark impish looking fellow, as lean as Cassius and as crafty and envious as Iago. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

And it shall come to pass in that day, that the glory of Jacob shall be made thin, and the fatness of his flesh shall grow lean. The Bible, Douay-Rheims Version |Various 

The tall, lean youngster wore a junior pilot's bands on the sleeves of his blue uniform. Fee of the Frontier |Horace Brown Fyfe