The majority of CU Boulder cases come from off-campus students. Even the most cautious schools are seeing outbreaks |Sy Mukherjee |September 17, 2020 |Fortune 

Busting the filibuster and using a newly unleashed majority to uncork several major changes to the structure of American politics would be a shocking turn of events. America needs a democratic revolution |Matthew Yglesias |September 17, 2020 |Vox 

Since mid-June, a majority of the new coronavirus deaths each day have occurred in red states. Trump blames blue states for the coronavirus death toll — but most recent deaths have been in red states |Philip Bump |September 16, 2020 |Washington Post 

Polls conducted in the past two months show a majority of Americans worry development of the vaccine is being rushed and a third wouldn’t get inoculated. ‘There’s been some cracks:’ Bill Gates questions FDA’s credibility on a COVID-19 vaccine |Claire Zillman, reporter |September 16, 2020 |Fortune 

The truth is that the majority of gang members never commit a gang-related crime in their life. While We’re Rethinking Policing, It’s Time to End Gang Injunctions |Jamie Wilson |September 15, 2020 |Voice of San Diego 

I think a large majority of our fans are [other] nationalities. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

House rules require an absolute majority of members voting to choose a speaker. Kamikaze Congress Prepares to Strike Boehner |Ben Jacobs |January 6, 2015 |DAILY BEAST 

Again, I do not know House Majority Whip Scalise or President Obama personally. Reverend Jeremiah Wright Was Worse Than Scalise |Ron Christie |January 2, 2015 |DAILY BEAST 

This is a state, after all, where Duke, in his statewide race for governor in 1991, received a majority of the white vote. Steve Scalise and the Right’s Ridiculous Racial Blame Game |Michael Tomasky |January 2, 2015 |DAILY BEAST 

Though this too is debatable given that 25,000 to 40,000 people a year die of influenza—the vast majority of them unvaccinated. When You Get the Flu This Winter, You Can Blame Anti-Vaxxers |Kent Sepkowitz |January 1, 2015 |DAILY BEAST 

Ever since his majority Lord Hetton had annually entered a colt in the great race. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Such is the opinion of this Correspondent to the Times, and it is doubtless the opinion of a fair and just majority. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

While the majority pulled in one way there was an active minority that wished the Nana to set up an independent kingdom. The Red Year |Louis Tracy 

At any rate, whatsoever that curious reservation meant, the majority of the staff were opposed to surrender. The Red Year |Louis Tracy 

The majority pick up a job when they can, but are inevitably idle and suffering two-thirds of the time. Glances at Europe |Horace Greeley