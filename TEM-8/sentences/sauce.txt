The latter was a macrobiotics company established in 1961 that got its start importing products like soy sauce from Japan and that eventually developed brown rice cakes in the 1970s — the prototype for the larger snack trend. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

We have a lab where we have focused on making mostly fermented products ranging from hot sauces to salts to soy sauce and versions of miso. Momofuku’s David Chang on the big changes the restaurant industry needs to make to survive |Beth Kowitt |September 14, 2020 |Fortune 

After all, without that secret sauce, an outright buy becomes a lot less desirable. Will Oracle’s reported TikTok deal satisfy Trump? |David Meyer |September 14, 2020 |Fortune 

While designed to mist out oils, the dispenser can also be used for many different seasonings, such as vinegars, soy sauce, lime juice and more. Best oil sprayers and misters for home chefs |PopSci Commerce Team |September 11, 2020 |Popular-Science 

From September 8 until October 4, McDonald’s will be serving the Please Millennials and Gen Z, Take Some Interest in Us Meal “Travis Scott Meal,” which consists of a quarter pounder with cheese, bacon and lettuce, a Sprite, and fries with BBQ sauce. Travis Scott Is the First Celebrity Since Michael Jordan to Get McDonald’s Meal Named After Him |Jaya Saxena |September 4, 2020 |Eater 

Serve with the warm sauce and your choice of ice cream, whipped cream, or yogurt. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

Combine the beans and onion sauce in a 9x9-inch casserole dish and bake for 20 to 25 minutes. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 

Continue to cook until the sauce has reduced by three quarters. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

Finish the sauce by putting the roasting pan on the stovetop over medium-high heat. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

The future congressman also had some spaghetti but no sauce. Kerry Bentivolio: The Congressman Who Believes in Santa Claus |Ben Jacobs |December 24, 2014 |DAILY BEAST 

Marriage is like Mayonnaise sauce, either a great success or an absolute and entire failure. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

It is to be feared that like the sauce of sauces in the hands of the inexperienced cook, the result is more than doubtful. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Mac took that pretty hard, and came mighty near making the major eat his words with gunpowder sauce on the side. Raw Gold |Bertrand W. Sinclair 

I must first make a dish of apple-sauce for the seven and seventy guests who are coming to my wedding-feast. The Nursery, July 1873, Vol. XIV. No. 1 |Various 

From the tables outside, one can see into the small kitchen, with its polished copper sauce-pans hanging about the grill. The Real Latin Quarter |F. Berkeley Smith