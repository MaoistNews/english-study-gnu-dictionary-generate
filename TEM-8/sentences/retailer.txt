The giant retailer is realizing its dream of getting into the banking business, which should terrify populists of all stripes. The People Vs. the Bank of Walmart |James Poulos |October 1, 2014 |DAILY BEAST 

J. Crew is the only mass retailer that shows at NYFW and Jenna Lyons may as well be the main reason why. It's Who You Know: The Power Players of New York Fashion Week |Barbara Ragghianti |September 3, 2014 |DAILY BEAST 

Lululemon Promotes Anti-Sunscreen: Printed on the bags of retailer Lululemon is a warning against the use of sunscreen. Lottie Moss Channels Kate for Calvin Klein; Terry Richardson Shoots for 'Playboy' |The Fashion Beast Team |July 14, 2014 |DAILY BEAST 

Affordable retailer Forever 21, for example, fired the 28-year-old model mid-way through her photo shoot. World Cup Fan Axelle Despiegelaere Already Dropped From L’Oreal; Forever 21 Fired Chrissy Teigan For Being "Fat" |The Fashion Beast Team |July 11, 2014 |DAILY BEAST 

The online retailer is asking all eBay users to change their passwords immediately. EVERY eBay Account Holder Worldwide Has Been Hacked, Company Says |Tom Sykes |May 22, 2014 |DAILY BEAST 

The patient, in turn, expressed her opinion of the product to the retailer from whom she had been purchasing it. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

At this juncture a public-spirited retailer of Boston, Mr. Lincoln Filene, entered the controversy. Making Both Ends Meet |Sue Ainslie Clark and Edith Wyatt 

All this activity, however, is concerned with the retailer; in the meantime manufacture was not neglected. Consumers and Wage-Earners |J. Elliot Ross 

These boards secure from wholesale representatives the prices charged to the retailer for various staple foods. Harper's Pictorial Library of the World War, Volume XII |Various 

Every week new price lists are prepared so as to cover new fluctuations of cost to the retailer. Harper's Pictorial Library of the World War, Volume XII |Various