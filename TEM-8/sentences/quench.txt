It’s an increasingly precarious resource for the over 40 million people it quenches, which include the cities of San Diego and Los Angeles, as long-term climate change threatens to destabilize the regular snowpack that feeds it. Who Owns the Tijuana River – and Who Needs Its Water Most |MacKenzie Elmer and Vicente Calderón |January 11, 2021 |Voice of San Diego 

The British retreated after a huge storm struck the city — perhaps a hurricane or a tornado — quenching the fires. Political extremists have attacked the U.S. Capitol before: A history of the violence |Gillian Brockell |January 7, 2021 |Washington Post 

Kosas’s ever so slightly sticky formula is loaded with ultra-moisturizing hyaluronic acid, so it wears tenaciously under a neck tube or mask and leaves lips noticeably more quenched than the average wax stick. The Gear Our Editors Loved in November |The Editors |December 4, 2020 |Outside Online 

No one needs to prove to you that a glass of water will quench your thirst, because everyone has experienced that result firsthand. The founder of wellness startup Mab & Stoke on the growth of ‘pay what you can’ options during the pandemic |Rachel King |September 20, 2020 |Fortune 

With a good hydration bladder in your pack, you’ll be ready to get outside without worrying about quenching your thirst. Hydration bladders for outdoor adventures |PopSci Commerce Team |September 1, 2020 |Popular-Science 

Luckily, ‘Doldo’ from the Second City Network has arrived to quench their thirsts, and have a laugh at their expense. Amateur Stuntmen, the iPhone 6, and More Viral Videos |Jack Holmes |August 30, 2014 |DAILY BEAST 

Her father runs an antique store and frequently sends the girls goods to quench the Berlin vintage drought. A Hip Haven in Berlin |Molly Hannon |September 20, 2011 |DAILY BEAST 

The bruised reed he shall not break, and smoking flax he shall not quench, he shall bring forth judgment unto truth. The Bible, Douay-Rheims Version |Various 

One copy was quite able to quench the thirst for "keeping up," and was often read aloud in the intervals between cards. Ancestors |Gertrude Atherton 

"It would be idle," came Wilding's icy voice to quench the gleam of hope kindling anew in Richard's breast. Mistress Wilding |Rafael Sabatini 

The water that was used to quench the fire being pumped into the river Idria, all the fish died excepting the eels. St. Nicholas, Vol. 5, No. 5, March, 1878 |Various 

Somehow this question seemed to quench the teacher of mathematics' good spirits. Ruth Fielding At College |Alice B. Emerson