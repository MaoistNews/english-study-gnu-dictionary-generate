The specific targets of these assaults—the eggs of ostriches, pelicans, and flamingoes among them—depends on the location within the vultures’ considerable range, which stretches across three continents. A Close Encounter with a Flame-Bright Egyptian Vulture - Facts So Romantic |Sarah Gilman |December 15, 2021 |Nautilus 

Among the animals it is credited with saving are icons such as the bald eagle, brown pelican, gray wolf and American alligator. Ivory-billed woodpecker officially declared extinct, along with 22 other species |Dino Grandoni |September 29, 2021 |Washington Post 

During a recent trip, we spotted a pair of humpback whales, along with a colossal pod of dolphins who were in cahoots with the pelicans, staging an elaborate routine to round up fish for one another. The Ultimate Channel Islands National Park Travel Guide |Shawnté Salabert |June 23, 2021 |Outside Online 

Home to hippos, pelicans and lots of fish, this lake often becomes polluted with human wastes from the city. A dirty and growing problem: Too few toilets |Stephanie Parker |September 24, 2020 |Science News For Students 

Something can happen in Whittier this morning and by the afternoon the brothers in Pelican Bay know all about it. The Mexican Mafia Is the Daddy of All Street Gangs |Seth Ferranti |December 11, 2014 |DAILY BEAST 

Gang warlords, locked down in Super Maxes like Pelican Bay pass on instructions to thousands of followers. The Mexican Mafia Is the Daddy of All Street Gangs |Seth Ferranti |December 11, 2014 |DAILY BEAST 

At Pelican Bay, there are no windows, and there is no reason not to have windows. Extreme Solitary Confinement: What Did Bradley Manning Experience? |Caitlin Dickson |June 5, 2013 |DAILY BEAST 

In 2011 a Minnesota farmer smashed thousands of eggs and young chicks of the federally protected American white pelican. Why Do We Save Some Species and Let Others Get Devastated? |Melissa Holbrook Pierson |May 21, 2013 |DAILY BEAST 

I returned the following year with The Pelican Brief, then The Client. John Grisham’s Favorite Bookstore: That One in Blytheville, Ark. |John Grisham |November 6, 2012 |DAILY BEAST 

Pelican, bird of ill omen, go to thy hole and hide thy sorry face.' Frdric Mistral |Charles Alfred Downer 

His tone and expression satisfied us that pelican would not keep us from starving. Famous Adventures And Prison Escapes of the Civil War |Various 

One day I captured a young pelican, and trained him to accompany me in my walks and assist me in my fishing operations. The Adventures of Louis de Rougemont |Louis de Rougemont 

But it was not impossible that there might be some other opening, and the Pelican crawled in search of it along the Java coast. The Ontario Readers: Fourth Book |Various 

He said that if it were not that he had to go to Pelican Lake that very night he would go along and help blow up the old rascal. Duffels |Edward Eggleston