I looked at my phone and it was just like, you know, a paperweight. Podcast: How a 135-year-old law lets India shutdown the internet |Anthony Green |September 2, 2020 |MIT Technology Review 

All of the work that comes with being an escort, and all of its attendant horrors, for this… paperweight? And The Escort of The Year Is… Backstage at The Sex Oscars |Scott Bixby |March 24, 2014 |DAILY BEAST 

The physical Hookie is an etched glass paperweight in the shape of a brilliant-cut diamond. And The Escort of The Year Is… Backstage at The Sex Oscars |Scott Bixby |March 24, 2014 |DAILY BEAST 

A paperweight that my mother gave me; it is made of dark blue glass and is in the shape of a heart. Ben Yagoda: How I Not Write Bad |Noah Charney |February 13, 2013 |DAILY BEAST 

It was broad but not heavy; the fingers that opened and shut quietly on a small paperweight were supple. A Hoosier Chronicle |Meredith Nicholson 

Next to it were several sheets of blank paper and a small traveling clock sat on them as a paperweight. Unwise Child |Gordon Randall Garrett 

Even an odd stone he had found on the desert and brought into the Wigwam one day, she used now as a paperweight. Mary Ware's Promised Land |Annie Fellows Johnston 

Kerk picked up a length of steel pipe from the desk, that he used as a paperweight, and toyed with it as he thought. Deathworld |Harry Harrison 

A paperweight representing a ragged little dog and an entomological photograph of the common ant. Suppers |Paul Pierce