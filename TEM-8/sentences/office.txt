By 2030, Google plans to precisely match every electron of electricity flowing into its offices and data centers with one produced from a renewable source. Google made clean energy cool for corporations, and it’s about to do the same for batteries |Michael J. Coren |September 17, 2020 |Quartz 

If your state offers the option to drop your ballot in a dropbox at the election office, do it. Why you should vote as early as possible (and how to do it) |John Kennedy |September 17, 2020 |Popular-Science 

Signatures change over time, so if you’re concerned the one on file could be out of date, ask your local election office how to update it. ProPublica’s Pandemic Guide to Making Sure Your Vote Counts |by Susie Armitage |September 16, 2020 |ProPublica 

I have an incredible amount of respect for both the speaker and the office she holds. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

They have not won the state's sole seat in the House of Representatives since 2008, they have not won a Senate election since 1994, and they have been locked out of the governor's office since 1988. The Trailer: The First State goes last |David Weigel |September 15, 2020 |Washington Post 

How do you feel about Archer and the gang abandoning the cartel and returning to the office? ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Two witnesses outside the Charlie Hebdo office building quoted the Kouachi brothers claiming they were members of al Qaeda. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

Who among Scalise's constituents could possibly care if he supported naming a post office for a black judge who died in 1988? The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

Granted, James is in an office in the Pentagon, and not on the front lines. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

In contrast, Boehner's leadership team filed into his ceremonial office and greeted the teary newly-elected Speaker with hugs. Democrats Accidentally Save Boehner From Republican Coup |Ben Jacobs, Jackie Kucinich |January 6, 2015 |DAILY BEAST 

It was with a feeling of relief on both sides that the arrival of Mr. Haggard, of the Home Office, was announced. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

On a small scale map, in an office, you may make mole-hills of mountains; on the ground there's no escaping from its features. Gallipoli Diary, Volume I |Ian Hamilton 

The next morning he came rushing into the office, in a violent state of excitement. The Book of Anecdotes and Budget of Fun; |Various 

They ran side by side across the yard to a roofed flight of steps that led to the printing-office. Hilda Lessways |Arnold Bennett 

On the third day after the declaration of his recall, Ripperda took his official leave, and presented his son in his new office. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter