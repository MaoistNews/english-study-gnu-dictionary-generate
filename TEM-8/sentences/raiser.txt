As a result, MSCHF upped the ante and banked a $200 million valuation for Push Party in this raise. MSCHF’s Push Party raises an unconventional seed round at a $200 million valuation |Lucas Matney |November 9, 2020 |TechCrunch 

With this raise, Teampay has now raised $21 million in known equity financing to date. Teampay adds $5M to its Series A at higher valuation after growing ARR 320% growth since the round |Alex Wilhelm |October 30, 2020 |TechCrunch 

The cash injection, led by Upfront Ventures and investment firm Wafra, follows an initial $7 million debt financing round, putting the company’s total raise at $47 million to date. Jiko raises $40 million to become a most unusual challenger bank |rhhackettfortune |October 29, 2020 |Fortune 

However, the annual findings of a shortfall in average federal salaries, which have ranged as high as 35 percent, have had little impact on actual pay raises for federal workers, which have been in the range of 1 to 2 percent in recent years. Federal salaries are lower than the private sector by 23 percent, advisory council reports |Eric Yoder |October 22, 2020 |Washington Post 

The city did agree to provide those raises, but that was before Covid hit. Why Are Cities (Still) So Expensive? (Ep. 435) |Stephen J. Dubner |October 15, 2020 |Freakonomics 

She is a money-raiser much in demand on the national campaign trail as the first female Hispanic governor in U.S. history. The Secret GOP Swing State Election Romp |John Avlon |October 28, 2014 |DAILY BEAST 

Value Added Tax:  Huge revenue-raiser, which is why Republicans hate it. After the Fiscal Cliff: What do Democrats Want? |Megan McArdle |January 2, 2013 |DAILY BEAST 

Of all the attacks he had at his disposal, was hitting Obama for going to a fund-raiser really the strongest one? Obama Is Back! |Michael Tomasky |October 17, 2012 |DAILY BEAST 

The net effect is that Romney is, however against his own will it might be, prepared to be smeared as a tax-raiser. Romney’s Big Tax Bluff: Why It Will Haunt Him |Michael Tomasky |July 5, 2012 |DAILY BEAST 

The two men, who met at a California fund-raiser in July 1951, became fast friends. The Death of the Texas Oilman |Bryan Burrough |February 4, 2009 |DAILY BEAST 

That was a regular hair-raiser, as the fellow said when he finished the blood-and-thunder story. Frank Merriwell's Races |Burt L. Standish 

He is a most progressive agriculturist and stock raiser whose interests are wisely directed and carefully managed. Lyman's History of old Walla Walla County, Vol. 2 (of 2) |William Denison Lyman 

They are then kept warm during the time occupied in hatching, sometimes about the person of the raiser. Textiles |William H. Dooley 

Richard, she had decided, should become a stock-raiser and farmer on the several-thousand-acre ranch they owned in Texas. How to Analyze People on Sight |Elsie Lincoln Benedict and Ralph Paine Benedict 

The lintel of the fitting-shed protected the fire-raiser a little. A Tatter of Scarlet |S. R. Crockett