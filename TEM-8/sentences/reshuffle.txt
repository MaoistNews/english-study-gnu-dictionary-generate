An NFL reshuffle has left Tim Tebow heading to New York and Peyton Manning going to Denver. The Broncos Sell Their Future: Tim Tebow Heads to New York Jets | Allen Barra | March 21, 2012 | DAILY BEAST 

High-stakes Politburo tussling on the eve of a key power reshuffle . China’s Top Cop Goes Missing, as Officials Stay Mum and Bloggers Erupt | Melinda Liu | February 10, 2012 | DAILY BEAST 

Plans are already being hatched to reshuffle key U.S. embassy personnel. Punching Back at WikiLeaks | Howard Kurtz | December 7, 2010 | DAILY BEAST