Sharing materials with participants about three days ahead of time tends to be the best. Best practices for Zoom board meetings at early-stage startups |Walter Thompson |February 12, 2021 |TechCrunch 

Milagritos Tapia, a Maryland pediatrics professor who helps coordinate the trials, said the arrangement allows researchers to confirm the vaccine is effective for an especially hard-hit community, while sowing seeds of trust though the participants. Among Latino immigrants, false vaccine claims are spreading as fast as the virus |Teo Armus |February 11, 2021 |Washington Post 

After the trial, 33 percent of participants reported they planned to continue feeding their cats meat-rich diets, and 76 percent reported they’d play more with their cats. Meatier meals and more playtime might reduce cats’ toll on wildlife |Jonathan Lambert |February 11, 2021 |Science News 

Be an active participant in every moment of the creation of your life, please. Meet D.C.’s Most Eligible LGBTQ Singles |Staff reports |February 11, 2021 |Washington Blade 

Pfizer’s 12- to 15-year-old study has enrolled 2,259 participants and Moderna’s adolescent trial is a similar size, aiming for about 3,000 participants. Fauci: Vaccines for Kids as Young as First Graders Could Be Authorized by September |by Caroline Chen |February 11, 2021 |ProPublica 

The Moscow protest was not sanctioned, which meant every participant risked arrest. Russia’s Rebel In Chief Escapes House Arrest |Anna Nemtsova |December 30, 2014 |DAILY BEAST 

He eventually ended up at as a digital producer for Participant Media. ‘Dear White People’: How An Ex-Publicist’s Twitter Became One of the Year’s Most Important Films |Marlow Stern |October 30, 2014 |DAILY BEAST 

To earn a spot on the trip, each participant agreed to raise a minimum of $1,000 to fund critical initiatives at Ninos del Sol. Motherless Daughters and Parentless Parents Trek to the Andes to Aid Orphans |Allison Gilbert |August 21, 2014 |DAILY BEAST 

Pretty much every single participant was complicit in the effort to put on a show. The Scopes Monkey Trial 2.0: It’s Not About the Stupid Science-Deniers |Michael Schulson |July 21, 2014 |DAILY BEAST 

Bell argues that Google is “clearly a reluctant participant in what effectively amounts to censorship.” Should We Have the Right to Be Forgotten Online? |Kyle Chayka |July 15, 2014 |DAILY BEAST 

I never had to fight again except as an unwilling participant in our foreign warfare. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Omnes enim unam participant essentiam, ac per hoc, quia omnibus communis est, nullius proprie est. The Supposed Autographa of John the Scot |Edward Kennard Rand 

I do not intend to cross out this statement about the most striking landmark of Paris, the participant in most of my vistas. Paris Vistas |Helen Davenport Gibbons 

Sanctification here becomes deification for every participant in the sacrifice. Elements of Folk Psychology |Wilhelm Wundt 

Only in the case when the two component elements which do not tend to the same end are evil is the participant better than either. Euthydemus |Plato