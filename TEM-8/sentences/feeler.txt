Privately, Banks put out feelers about running for the job himself. Tucker Carlson’s Ugly Feud With Eric Swalwell Has Sucked in Family |Maxwell Tani |August 27, 2021 |The Daily Beast 

As In the Heights was declared the toast of Broadway, Hollywood started putting out feelers. How In the Heights went from a student musical to one of the summer’s biggest movies |Constance Grady |June 11, 2021 |Vox 

Washington, a 6-foot-3, 185-pounder who averaged 24 points, seven assists and six rebounds for the 30-2 Compass Prep team, has gotten feelers from other schools, his father told the Athletic. Creighton loses a prized recruit over Coach Greg McDermott’s plantation comment |Cindy Boren |April 15, 2021 |Washington Post 

So for the past few years, Gordon has put out feelers to institutions to take over Lynne’s life’s work. The Internet’s Most Incredible Collection of Food History Has Been Saved |Dayna Evans |January 12, 2021 |Eater 

All those cells allow these flexible feelers to operate almost on their own. Touching allows octopuses to pre-taste their food |Jonathan Lambert |January 4, 2021 |Science News For Students 

He threw out this last suggestion as a kind of feeler; and then suddenly made the plunge. North and South |Elizabeth Cleghorn Gaskell 

A mild electrical shock coursed through his body, as if an invisible feeler had passed over him. The Whispering Spheres |Russell Robert Winterbotham 

The feeler had all but touched Quade, and with the closeness of his escape, the remnants of his courage gave. Astounding Stories, April, 1931 |Various 

It's feeler rays, Dr. Lee; the first wave, low penetration surface rays. The Brain |Alexander Blade 

The huge disk with the feeler-ray antennae sank down close to his chest, heavy as the keystone upon a tomb. The Brain |Alexander Blade