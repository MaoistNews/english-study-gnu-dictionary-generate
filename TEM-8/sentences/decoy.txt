Batlle’s team began working on decoy proteins in January 2020 after learning about the first US case, building on knowledge gleaned from China’s 2003 SARS-CoV outbreak. Can you spot the fake receptor? The coronavirus can’t either. |Tatyana Woodall |August 27, 2021 |MIT Technology Review 

The decoys don’t attach to cells but float in the fluid between them to catch the virus before it binds to the real ACE2 receptors. Can you spot the fake receptor? The coronavirus can’t either. |Tatyana Woodall |August 27, 2021 |MIT Technology Review 

Perkins angered some customers who complained that Orvis’s catalogues were suddenly filled with copper wastebaskets, silk underwear and items such as the Phona-Duck, a telephone that looked like a duck decoy. Leigh H. Perkins, who built Orvis into an upscale sporting brand, dies at 93 |Harrison Smith |May 13, 2021 |Washington Post 

I kind of had to be a decoy and continue to move around without the ball. Wizards finally turn back Raptors in OT, strengthen bid for NBA’s play-in tourney |Kareem Copeland |May 7, 2021 |Washington Post 

Irving, a more elusive player than Harden, should have been the decoy and release valve if Durant was covered. With Kyrie Irving back, rookie coach Steve Nash has to figure out how to get his stars aligned |Ben Golliver |January 21, 2021 |Washington Post 

The former decoy did as bid as Federal Judge Lewis Kaplan entered. Gripping His Koran, Anas al-Liby Has His Day in Court |Michael Daly |October 16, 2013 |DAILY BEAST 

Lönnborg, who works as a lawyer in London, has on several occasions posed as Elin, acting as a decoy to throw off the paparazzi. Elin's New Life in Sweden |Katarina Andersson |September 6, 2010 |DAILY BEAST 

The impudence of the authorities, to decoy an unsuspecting workingman across the State line, and then arrest him as my accomplice! Prison Memoirs of an Anarchist |Alexander Berkman 

The boy's pulses leaped toward these things even while his lips curled in disdain at the shallow decoy. The Dragon Painter |Mary McNeil Fenollosa 

You make use of your power to run a common decoy house, to do away with men for money. The Double Four |E. Phillips Oppenheim 

The decoy was barely in place before he was on the floor while a volley of lead and a flight of arrows rained against the roof. A Virginia Scout |Hugh Pendexter 

They saw nothing but the wretched decoy vanishing behind the nearest tents. It Is Never Too Late to Mend |Charles Reade