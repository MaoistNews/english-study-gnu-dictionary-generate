Assistant director Dave Halls reportedly told Baldwin the revolver was “cold,” meaning it did not have live rounds, before he shot Hutchins and director Joel Souza, who survived. Why Alec Baldwin could face charges even if the “Rust” shooting was accidental |Courtney Vinopal |October 27, 2021 |Quartz 

The assistant director could only recall seeing three rounds when Gutierrez showed him the revolver before Baldwin rehearsed the scene. ‘Lead projectile’ recovered from director’s shoulder as evidence in fatal shooting on Alec Baldwin film set, authorities say |Sonia Rao |October 27, 2021 |Washington Post 

We see a distraught Martin show Kate a family heirloom, an antique revolver that he indicates he plans to use on himself. ‘Cruel Summer’ finale reveals the truth about its biggest mystery. We think. |Bethonie Butler |June 16, 2021 |Washington Post 

Paige Berhow, who retired as assistant police chief in the Ogden suburb of Riverdale and now lives in the city, became an officer in the early 1980s, when her on-duty equipment consisted of little more than a uniform and a revolver. Inside the rise of police department real-time crime centers |Rowan Moore Gerety |April 19, 2021 |MIT Technology Review 

The city was quick to release a photo of a revolver the man was allegedly carrying and camera footage from a nearby business that appears to show a person raising an object before getting shot. Morning Report: Border Patrol Is Tripping Up Legal Pot Operations |Voice of San Diego |June 29, 2020 |Voice of San Diego 

The cops gave chase and the gunman fired the big revolver twice more. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Peters qualified as an “expert” on the M16A1 in 1993 and the .38 revolver in 1995. The (Sloppy) Swift-Boating of Michigan Democrat Gary Peters |Tim Mak |October 13, 2014 |DAILY BEAST 

On November 4, 1928, someone put a revolver slug into Rothstein's body in Room 349 of the Park Central Hotel. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 

Gdenisku displays for me an old revolver equipped with rubber bullets; this is the only weapon he bears. Ukraine’s Vigilante Peacemakers |James Kirchick |May 17, 2014 |DAILY BEAST 

He orders a Smith Wesson .38 snub-​nosed revolver through the mail, using the same alias. The Man Oswald First Tried to Kill Before JFK |Bill Minutaglio, Steven L. Davis |October 3, 2013 |DAILY BEAST 

A look of passion came into the face of the watching boy, and again he fingered his revolver. The Courier of the Ozarks |Byron A. Dunn 

During this conversation Harry's right hand was resting beneath his jacket, grasping the butt of his revolver. The Courier of the Ozarks |Byron A. Dunn 

"So that is Jim Poindexter, the bloody villain," muttered the boy between his set teeth, and nervously fingering his revolver. The Courier of the Ozarks |Byron A. Dunn 

Frank loosened his sword from its fastenings and took a revolver in his left hand, in which he also held the reins. The Red Year |Louis Tracy 

Instantly Longcluse had used his revolver; but before he could make assurance doubly sure, his quick ear detected a step outside. Checkmate |Joseph Sheridan Le Fanu