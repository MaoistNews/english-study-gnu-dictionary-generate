A thread of anxiety started its twist into my stomach as the bus wound ever onwards into the dense blackness of night. Two Chickens, an Old Guitar, and a Group of Strangers: A Life-Changing Feast in Brazil |Annabel Langbein |November 29, 2013 |DAILY BEAST 

But this will not mean that we drive ever onwards towards greater sexual freedom--rather, it will mean quite the reverse. Why Gay Marriage Will Win, and Sexual Freedom Will Lose |Megan McArdle |March 26, 2013 |DAILY BEAST 

From the 80s onwards, after manufacturing collapsed, the inner cities were awash with hard drugs. A Tinderbox Waiting for a Match |Gavin Knight |August 11, 2011 |DAILY BEAST 

She soon graduated to singing, songwriting and after a couple of years she moved onwards and upwards. The Lost Madonna Tapes |Andrew Morton |October 20, 2008 |DAILY BEAST 

She would not see the offered hand, but swept onwards with a cold curtsey, stopping just a moment to speak to her husband. Elster's Folly |Mrs. Henry Wood 

From this time onwards Aragon became the base from which was organised the conquest of Catalonia and Valencia. Napoleon's Marshals |R. P. Dunn-Pattison 

From Castiglione onwards the soldiers of Augereau's division would do anything for their commander. Napoleon's Marshals |R. P. Dunn-Pattison 

Bigyns, Beguines; these were members of certain lay sisterhoods in the Low Countries, from the twelfth century onwards. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

In 207 onwards, Severus built a new wall along the line of Hadrian's rampart. The Towns of Roman Britain |James Oliver Bevan