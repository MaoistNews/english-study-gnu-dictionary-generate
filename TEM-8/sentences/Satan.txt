Because when Satan wants to extinguish a light, he will stop at nothing. The mess in Maricopa |Dan Zak |May 21, 2021 |Washington Post 

And so, the horned god became Satan—and others in his demonic retinue. Meet Krampus, the Seriously Bad Santa |Jay Michaelson |December 5, 2014 |DAILY BEAST 

Some historians say that the Christian church gave Satan horns to demonize the Wiccan faith. ‘Gods of Suburbia’: Dina Goldstein’s Arresting Photo Series on Religion vs. Consumerism |Dina Goldstein |November 8, 2014 |DAILY BEAST 

Context: Rumored to be the last words of the French enlightenment writer, when a priest asked him to renounce Satan. Tupac’s ‘F*ck You’ to a Cop and the Best Last Words |Marlow Stern |May 27, 2014 |DAILY BEAST 

Besides, among most Satan-fearing Christians, things like confirmation and proof are overrated. Why American Christians Love Satan |Matthew Paul Turner |February 16, 2014 |DAILY BEAST 

Iran will be strengthened in the region by virtue of the mere fact that it was able to bring the Great Satan to the table. Obamacare, Impeachment, Iran, and More Political Predictions for 2014 |Michael Tomasky |December 30, 2013 |DAILY BEAST 

And the resolution to abandon Satan and his cause enters into the covenant engagement. The Ordinance of Covenanting |John Cunningham 

That will do, interrupted Satan, taking the violin from the little man, who bowed low and ceremoniously took his departure. The Fifth String |John Philip Sousa 

If he has painted vice and shown Satan in all his pomp, it is without the least complacence in the task. Charles Baudelaire, His Life |Thophile Gautier 

I am certain that he is one of those devils who war against mankind, endowed by their Prince Satan with superhuman power. Balsamo, The Magician |Alexander Dumas 

"He would make Satan stand up and take off his hat, if he paid Hades a visit," said Mrs. Tynan admiringly. You Never Know Your Luck, Complete |Gilbert Parker