Despite the collective outpouring of grief and emotion in the days and weeks after Lennon’s death, the aggrieved widow was not really embraced by her husband’s mourning fans. The Love Story of John and Yoko, 50 Years Later |Sean Braswell |August 3, 2021 |Ozy 

Interventions from — and lawsuits by — civil rights organizations, the federal government and aggrieved potential employees forced breweries to diversify their workforces in the 1970s and 1980s. The solution to the craft beer industry’s sexism and diversity problems |Allyson Brantley |June 3, 2021 |Washington Post 

Automatically posting your images or video to social media can add to the aggrieved party’s distress. With airline altercations on the rise, a guide to best practices for bystanders |Tanya Ward Goodman |May 20, 2021 |Washington Post 

We’ll shine a light on why so many Americans feel so aggrieved, and why they see the world and their place in it so differently. Welcome to Made by History |Brian Rosenwald, Carly Goodman, Kathryn Brownell |April 21, 2021 |Washington Post 

I was such a fan of the ever-so-innocent books, where the rudest thing ever said was “golly gosh,” that I was aggrieved to learn that the writer had a darker side. 12 Crazy True Stories About Children’s Authors |Kate Bartlett |April 1, 2021 |Ozy 

In short, Pakistan is an aggrieved state that got the short end of the stick when Partition happened. CIA Agents Assess: How Real Is ‘Homeland’? |Chuck Cogan, John MacGaffin |December 15, 2014 |DAILY BEAST 

Some are genuinely aggrieved by the disruption caused to the transport system. Hong Kong Between Calm and Chaos |Ben Leung |October 3, 2014 |DAILY BEAST 

I have written that actually black communities are quite aggrieved about black-on-black violence. The War on Drugs Is What Makes Thugs |John McWhorter |August 21, 2014 |DAILY BEAST 

I would rather not say it annoys me because it sounds so aggrieved and me-centered. The Author Of The Summer's Hit Paranoid Fantasy Opens Up |William O’Connor |August 15, 2014 |DAILY BEAST 

Not just crazy-eyed Michele Bachmann or perpetually aggrieved Sarah Palin types. It’s Not the President’s Speech That Makes News but the Reactions to It |Michelle Cottle |January 29, 2014 |DAILY BEAST 

On Corpus Christi's Eve, the usual celebration greatly aggrieved the perth weekly assembly. The Every Day Book of History and Chronology |Joel Munsell 

"I mean the secret that affects him" she interrupted, in aggrieved tones, feeling that Mr. Carr was playing with her. Elster's Folly |Mrs. Henry Wood 

Now-a-days, a real, good, wrong-headed aggrieved parishioner is exactly what you do want. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

If he should be attacked by any who felt aggrieved by his recent proceedings, the Bishop was to help him at all costs. Belgium |George W. T. (George William Thomson) Omond 

The poorer towns felt themselves aggrieved, and often put insuperable obstacles in the way of the collector. A short history of Rhode Island |George Washington Greene