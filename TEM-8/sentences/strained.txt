Still, the narrative offers informed, exacting characterizations of the uncertain political alliances, strained interactions and ideological growing pains that elites of the post-revolutionary decades put the country through. Four presidents who put Virginia’s stamp on early America |Andrew Burstein |October 30, 2020 |Washington Post 

It’s got the same pump-up-the-heart music, the same slick videos, the same strained, middle-aged corporate exuberance. Virtual Apple events just aren’t the same |Adam Lashinsky |October 14, 2020 |Fortune 

Despite this, e-commerce has been one of the bright spots for publishers’ stretched and strained ad revenue lines. How Hearst UK’s e-commerce revenue grew 322% during the second quarter |Lucinda Southern |August 21, 2020 |Digiday 

Now, sharing strained relations with the United States, the two are increasingly finding a common cause. Turning a Blind Eye Internationally Will Cost Us |Tracy Moran |August 7, 2020 |Ozy 

Winners and losers in our suddenly super-strained food system. What Happens When Everyone Stays Home to Eat? (Ep. 412) |Stephen J. Dubner |April 9, 2020 |Freakonomics 

His breath became so strained that he was forced to quit his job as a horticulturalist for the parks department. Before Eric Garner, There Was Michael Stewart: The Tragic Story of the Real-Life Radio Raheem |Marlow Stern |December 4, 2014 |DAILY BEAST 

We unzipped the body bag, and a crowd of craned necks strained to get a look. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

The alliance between America and rebel forces has been strained by the U.S. refusal to directly attack the Assad regime. Al Qaeda Makes a Play for the U.S. Allies the War Against ISIS Depends On |Jacob Siegel |September 29, 2014 |DAILY BEAST 

This has resulted in a huge influx of patients into an already strained system. Will US Health Care Follow in China’s Bloody Footsteps? |Daniela Drake |September 21, 2014 |DAILY BEAST 

But Schiff said this “has the feeling of a lawyerly kind of argument, to put the best case on a pretty strained legal theory.” Even a Top Democrat Thinks Obama's Legal Case for War Makes No Sense |Eli Lake |September 19, 2014 |DAILY BEAST 

So strained became the relations between them, that for the last part of the advance they no longer met at meals. Napoleon's Marshals |R. P. Dunn-Pattison 

But being himself in somewhat strained relations with the existing Government, he did not think it prudent to show himself. The Philippine Islands |John Foreman 

Relations became so strained that Bonaparte was soon glad to seize on any excuse to dismiss Lannes from his post. Napoleon's Marshals |R. P. Dunn-Pattison 

"Sing," said the Bull, as the stiff, muddy ox-bow creaked and strained. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Collars or sleeves, pinned over or tightly strained to meet, will entirely mar the effect of the prettiest dress. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley