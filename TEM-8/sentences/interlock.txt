In newborns, the larynx is positioned higher in the throat, and interlocks with the soft palate to create a separation between the food pathway in the mouth and the breathing pathway in the nose. 10 absolutely wild facts about babies |Lauren Leffer |July 2, 2021 |Popular-Science 

The abstract yet seemingly organic forms dovetail, interlock and sometimes appear to tie themselves into knots. In the galleries: Tracing a generational progression in abstract art |Mark Jenkins |March 5, 2021 |Washington Post 

The legs adjust to balance on uneven terrain, and the components interlock. AbleNook designers offer alternative to disaster-relief tents and trailers |Nina Strochlic |August 18, 2013 |DAILY BEAST 

They in turn, as they grow, interlock their boughs, and repeat in a season or two the same process of mutual suffocation. A Half Century of Conflict - Volume I |Francis Parkman 

Both are navigable streams, and their head waters interlock with Grand river, or Washtenong, which flows into lake Michigan. A New Guide for Emigrants to the West |J. M. Peck 

Some of its head waters interlock with those of Tippecanoe, a prominent tributary of the Wabash. A New Guide for Emigrants to the West |J. M. Peck 

Different tariffs may interlock with complicated cross references. Railroads: Rates and Regulations |William Z. Ripley 

On the edges of the barbs are set the barbules, which interlock with those of adjacent barbs, and thus give strength to the vane. The New Gresham Encyclopedia |Various