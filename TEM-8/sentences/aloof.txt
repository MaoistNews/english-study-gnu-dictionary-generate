Al-Sistani then successfully brokered a ceasefire before returning to his position of aloof authority. What Ancient Laws Can Teach Us About Holding Autocrats to Account Today |Fernanda Pirie |December 23, 2021 |Time 

A bisexual, wig-wearing triathlete who was raised in poverty and became a social worker, Sinema is as famous on Capitol Hill for her funky clothes and aloof style as for any particular policy vision. What Does Kyrsten Sinema Want? |Charlotte Alter |October 2, 2021 |Time 

Rodrigo has stayed professionally aloof about her personal life, letting the music do the talking. How Olivia Rodrigo Became America’s Biggest New Pop Star |Raisa Bruner |May 24, 2021 |Time 

I seek companions for activities when the mood strikes and can be aloof. To pod or not to pod? |Tom Sommers |February 23, 2021 |Washington Blade 

Neutrinos are famously aloof particles, and questions remained over exactly how neutrinos transfer their energy to the star’s ordinary matter under the extreme conditions of a collapsing star. Secret Ingredient Found to Power Supernovas |Thomas Lewton |January 21, 2021 |Quanta Magazine 

Lawmakers were open about their displeasure with Pierson, who appeared aloof as she testified before them Tuesday morning. Why Secret Service Chief Julia Pierson Was Shown the Door |Tim Mak |October 2, 2014 |DAILY BEAST 

They should ask themselves instead how anyone as bored and aloof as Barack Obama could bother himself to hate anything. No Drama Obama's Israel Ambivalence |James Poulos |July 26, 2014 |DAILY BEAST 

A Utah mother charged with killing six of her infant children was described as cold and aloof by a neighbor. Utah’s Murderer Mom Is a Monster but She’s Not the First |Steve Miller |April 16, 2014 |DAILY BEAST 

“She was always aloof, quiet, and never put out any effort to reciprocate,” Wall said. Utah’s Murderer Mom Is a Monster but She’s Not the First |Steve Miller |April 16, 2014 |DAILY BEAST 

I can see how it would make people come across as cagey or aloof. How One Doctor Mastered the Art of Delivering Life-Changing Diagnoses |Russell Saunders |March 22, 2014 |DAILY BEAST 

No one could be on such terms with whites, and be at heart an Indian, they thought; so they held aloof from Ramona. Ramona |Helen Hunt Jackson 

He stood aloof from Balliol, and, in spite of marked snubbing, steadily adhered to Edward. King Robert the Bruce |A. F. Murison 

So Piegan Smith and I stood aloof and watched the grim play, for the fate of a woman hung in the balance. Raw Gold |Bertrand W. Sinclair 

The images and pictures on the outside of the wall were made repellent, to keep strangers aloof. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Unquestionably he stood aloof from the battle, watching its progress at a safe distance. King Robert the Bruce |A. F. Murison