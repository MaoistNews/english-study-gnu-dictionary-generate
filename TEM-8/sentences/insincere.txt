When you give insincere compliments, for example, you may make your friends feel good — at first, anyway. Lying won’t stretch your nose, but it will steal some brainpower |Avery Elizabeth Hurt |April 21, 2022 |Science News For Students 

Certainly he should not expect you to have inferred his insincere intent. Miss Manners: If he couldn’t pay for dinner, he shouldn’t have offered |Judith Martin, Nicholas Martin, Jacobina Martin |May 6, 2021 |Washington Post 

We want you to feel like you’re watching a show that was made by a small group of people that really attacked everything that feels generic or unnecessary or insincere. The Pandemic Oscars Were Surprisingly Decent. But Will the Academy Learn Anything From Its Break With Tradition? |Judy Berman |April 26, 2021 |Time 

That exposes moderate Democrats to criticism that they went along with their party to increasing the deficit — criticism that will almost certainly emerge however insincere or hypocritical it might be. The complex bipartisanship driving the coronavirus relief bill |Philip Bump |March 3, 2021 |Washington Post 

A genuine show of celebrity rudeness, followed by an insincere PR performance? An Analysis of Vitalii Sediuk’s Pranks (He’s the Guy Who Touched Brad Pitt) |Amy Zimmerman |May 29, 2014 |DAILY BEAST 

Hormel publicly dismissed it as insincere and politically motivated. How the Chuck Hagel Fight Changed the American Jewish Landscape in Washington |J. J. Goldberg |August 20, 2013 |DAILY BEAST 

“You are a great man,” Arafat said, turning on his insincere, oleaginous charm. Bibi's Right: Palestinian Rejectionism Threatens Peace |Gil Troy |May 3, 2013 |DAILY BEAST 

On the 47 percent, to whom Romney “apologized” in the most staged and insincere manner possible. Michael Tomasky on How Obama Needs to Make Mitt Unacceptable Again |Michael Tomasky |October 10, 2012 |DAILY BEAST 

In the most recent Frum-Tomasky dialogue, David argued that Obama's opposition to same sex marriage was insincere. Obama Supports Same Sex Marriage |Noah Kristula-Green |May 9, 2012 |DAILY BEAST 

The protestations of Mr. O'Connell were as insincere as his statements were historically untrue. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

It was so evident that the Chinese commissioner was insincere, that hostilities recommenced on the 7th of January. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

I never saw the man, though she gave me a long history of the affair, to which I listened with a forced and insincere sympathy. The New Machiavelli |Herbert George Wells 

His political ideas were beneath contempt; they were insincere, as he proved, and they were merely an excuse for riot. Florence and Northern Tuscany with Genoa |Edward Hutton 

It might be said that the word sincere is a proof of love, and insincere a proof of falsehood. My Recollections of Lord Byron |Teresa Guiccioli