By now, everyone who’s ever seen a Paul Schrader movie knows that he favors deep moral explorations, and often writes characters who are obsessed with routines and rituals, ascetics for better or worse. Oscar Isaac Smolders in the Pensive Romantic Thriller The Card Counter |Stephanie Zacharek |September 2, 2021 |Time 

In that distorted state, it felt good to deprive myself, as if it were some ascetic form of self-mastery. How Nature Helped Me Recover from an Eating Disorder |Kate Siber |March 25, 2021 |Outside Online 

I thought that I’d wind up among the “enlightened,” a digital ascetic who prioritized attentiveness above all else. I Ditched Texting and Picked Up the Phone |Abigail Barronian |February 22, 2021 |Outside Online 

Having carefully set up the circumstances of his narrative within a viscerally realistic place and time, he lets it all unfold with an almost ascetic sparseness of dialogue. Winslet, Ronan have seaside rendezvous in ‘Ammonite’ |John Paul King |December 11, 2020 |Washington Blade 

Not surprisingly, this did not sit well with the ascetic early Christians. Meet Krampus, the Seriously Bad Santa |Jay Michaelson |December 5, 2014 |DAILY BEAST 

Soyinka is a food and wine enthusiast, but he also sinks easily into a kind of ascetic mode and fasts regularly. Nigeria’s Larger-Than-Life Nobel Laureate Chronicles a Fascinating Life |Chimamanda Adichie |August 9, 2014 |DAILY BEAST 

An Arab legend has it that the intoxicating effects of hashish were discovered by an ascetic monk in 1155. The Chronic Chronicles: A History of Pot |Roger Roffman |July 6, 2014 |DAILY BEAST 

Maybe this is better than self-denying ascetic teenage subculture anarchism. How the Pussy Riot Girls Trial Fell Apart |Alisa Obraztsova |May 8, 2013 |DAILY BEAST 

He works around an impossibly long and lean ideal, but never allows his work to grow ascetic and cold. Balenciaga and Designer Nicolas Ghesquiere Split |Robin Givhan |November 5, 2012 |DAILY BEAST 

His cowl was thrown back, revealing his pale, ascetic countenance and shaven head. St. Martin's Summer |Rafael Sabatini 

Take him in repose, and he looked a lank ascetic who dreamed of a happy land where flagellation was a joy and pain a panacea. You Never Know Your Luck, Complete |Gilbert Parker 

In appearance, Terry was an ill-adjusted compromise between an ascetic and a young man about town. Mushroom Town |Oliver Onions 

The Nazarenes are archological and ascetic; the Dsseldorf school is insipid in a modern way, feeble, colourless, and sentimental. The History of Modern Painting, Volume 1 (of 4) |Richard Muther 

His philosophy had   made him neither an ascetic nor an anchorite. Mary Wollstonecraft |Elizabeth Robins Pennell