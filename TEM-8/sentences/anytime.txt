While investors should understand that massive earnings beats likely won’t repeat themselves, the increasingly important role our homes are playing in our lives probably won’t be changing anytime soon. These 2 home improvement stocks are up 30% this year. They’re still a buy |Anne Sraders |August 19, 2020 |Fortune 

That is the message from San Diego Unified officials, who announced Monday that school would not likely be physically reopening anytime soon, City News Service reports. Morning Report: How the City Came to Lease a Lemon |Voice of San Diego |August 11, 2020 |Voice of San Diego 

There are signs that the influx in gun sales might not be slowing anytime soon. Gun Sales Are Surging, But Background Checks Aren’t Keeping Up |Joshua Eaton (joshua.eaton2@gmail.com) |July 30, 2020 |FiveThirtyEight 

Going forward, the commission would make recommendations anytime officials want to purchase new devices capable of watching or listening to the public. Morning Report: Newly Unsealed Docs Highlight Police Watchdog’s Limitations |Voice of San Diego |July 23, 2020 |Voice of San Diego 

That said, the unemployment rate is still higher than it’s been since the Great Depression, and a lot of the lost jobs — in retail and restaurants, for instance — are unlikely to return anytime soon. How to Prevent Another Great Depression (Ep. 421) |Stephen J. Dubner |June 11, 2020 |Freakonomics 

It will not be an easy road, nor will it happen anytime soon. State of LGBT Rights: Married on Sunday, but Fired on Monday |Gene Robinson |December 14, 2014 |DAILY BEAST 

The president of Mongolia is one foreigner who may not be invited back to Kim Il Sung University anytime soon. North Korea's Top College: Brainwash U |Justin Rohrlich |December 13, 2014 |DAILY BEAST 

The identity of the skeleton remains the big question, and answers may not be forthcoming anytime soon. Is This Alexander the Great’s Tomb—or His Wife’s? |James Romm |December 12, 2014 |DAILY BEAST 

Ebola, while terrifying from its high mortality rate, cannot take you down anytime. The Sham, Scaremongering Guide to Ebola |Abby Haglage |November 20, 2014 |DAILY BEAST 

As for EatWith, whether it “unicorns” anytime soon is anyone guess. The Airbnb of Home-Cooked Meals |Itay Hod |November 3, 2014 |DAILY BEAST 

Did you have occasion to look into your garage area at anytime during the period you were preparing dinner? Warren Commission (3 of 26): Hearings Vol. III (of 15) |The President's Commission on the Assassination of President Kennedy 

At anytime while you were preparing dinner was Lee Oswald in the garage? Warren Commission (3 of 26): Hearings Vol. III (of 15) |The President's Commission on the Assassination of President Kennedy 

At anytime during the dinner period, did Lee Oswald leave your home? Warren Commission (3 of 26): Hearings Vol. III (of 15) |The President's Commission on the Assassination of President Kennedy 

At anytime during that period did Lee Oswald enter the garage area? Warren Commission (3 of 26): Hearings Vol. III (of 15) |The President's Commission on the Assassination of President Kennedy 

Had the light been on at anytime to your knowledge prior to that? Warren Commission (3 of 26): Hearings Vol. III (of 15) |The President's Commission on the Assassination of President Kennedy