As disappointing as that might be, there is fortunately still a path forward. The Bipartisan Infrastructure Deal Is a Return to the Old Way of Politics. That’s A Problem for the Climate |Michael E. Mann |August 6, 2021 |Time 

There were, fortunately, no more severe consequences and the Cuban returned to the pod. Locked up in the Land of Liberty: Part III |Yariel Valdés González |July 21, 2021 |Washington Blade 

The officers fortunately do not understand any Spanish, so what they got their hands on was simply a bunch of incomprehensible words. Locked up in the Land of Liberty: Part II |Michael K. Lavers |July 14, 2021 |Washington Blade 

So, fortunately enough, modern vehicle fleets have these monitoring systems that produce quite a lot of data. Using machine learning to build maps that give smarter driving advice |MIT Technology Review Insights |June 23, 2021 |MIT Technology Review 

The pandemic heightened that fear, but fortunately, Thompson’s primary care provider, Village Medical at Home, wanted to try CAPABLE for some of its patients. COVID-19 Exposed the Faults in America’s Elder Care System. This Is Our Best Shot to Fix Them |Abigail Abrams |June 15, 2021 |Time 

There is, fortunately, not too much telling of the future in Harry Potter. Harry Potter and the Torah of Terror |Candida Moss, Joel Baden |January 4, 2015 |DAILY BEAST 

Fortunately, another group is leaning in: Turkish women business leaders. The Women Battling an Islamist Strongman |Christina Asquith |December 22, 2014 |DAILY BEAST 

Fortunately, Pomplamoose made some money to offset some of these expenses. How Much Money Does a Band Really Make on Tour? |Jack Conte |December 8, 2014 |DAILY BEAST 

Fortunately, no one gives a damn about a Daily Beast reporter. Sneer and Clothing in Miami: Inside The $3 Billion Woodstock of Contemporary Art |Jay Michaelson |December 6, 2014 |DAILY BEAST 

Fortunately, they are drawn from a pathetic preterite far beneath the contempt of our cultural elite. The FBI’s Bogus ISIS Bust |James Poulos |November 21, 2014 |DAILY BEAST 

Fortunately, the last crash had been passed without dislocating the parts of either sledge or rider. The Giant of the North |R.M. Ballantyne 

Fortunately, Massna had time to make his way through the Austrian skirmishers and resume his command. Napoleon's Marshals |R. P. Dunn-Pattison 

It was in the reading-room at the time of the fire, but fortunately escaped injury. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

I only know that there are amongst us, rare instances fortunately, but existent nevertheless—men with the souls of beasts. Uncanny Tales |Various 

Fortunately, the water was not deep, and quickly the drenched animal and man were pulled from the water. Our Little Korean Cousin |H. Lee M. Pike