Walker says that he was under the impression that the officers were intruders, and is protected by Kentucky’s Stand Your Ground law according to his legal council. Officer Involved In Killing Of Breonna Taylor Files Lawsuit Against Taylor’s Boyfriend |Kirsten West Savali |October 30, 2020 |Essence.com 

The no-knock warrant ban stemmed from the shooting death of Breonna Taylor in March, when Louisville police broke down her apartment door in the middle of the night and her boyfriend, mistaking officers for intruders, opened fire. Ban on chokeholds, no-knock warrants among bills Northam signed into law |Laura Vozzella |October 28, 2020 |Washington Post 

We’d assumed our supplies would be safe from intruders, but the flimsy windows were no match for a hungry bear. These Bears Have a Job, and It's Destroying Coolers |Emma Walker |October 9, 2020 |Outside Online 

In this case, Taylor’s boyfriend saw the police and thought they were intruders. A woman killed. An officer shot. And no one legally responsible. |David Fahrenthold |September 24, 2020 |Washington Post 

He had a registered handgun and fired it once at the intruders. A woman killed. An officer shot. And no one legally responsible. |David Fahrenthold |September 24, 2020 |Washington Post 

“Ordinarily, you see punch-counterpunch-punch,” as the attacked party tries to fend off the intruder, the former official said. Obama Could Hit China to Punish North Korea |Shane Harris, Tim Mak |December 20, 2014 |DAILY BEAST 

Pistorius maintains that he mistook Steenkamp for an intruder. Witness: Pistorius Suicidal After Shooting Reeva |Kelly Berold |May 5, 2014 |DAILY BEAST 

In spite of these reports of student anxiety, state lawmakers continue to pass new regulations requiring intruder drills. School Shooting Drills Can Be More Terrifying Than Helpful |Rachel Bronstein |April 3, 2014 |DAILY BEAST 

One day the site caught an intruder attempting to copy their conversations. They Want to Know Everything About You |Jake Whitney |March 5, 2014 |DAILY BEAST 

Pistorius claims he thought the person in the bathroom was an intruder and that his girlfriend was sleeping in their bed. ‘Ray Donovan’ Vs. ‘Scandal’: Could TV Fixers Handle Real-Life Crises? |Kevin Fallon |June 25, 2013 |DAILY BEAST 

A terrific yell of rage burst from every one, and each hastily threw something or other at the bold intruder. Hunting the Lions |R.M. Ballantyne 

He locked the door as he spoke, and then, striding forward in a towering rage, threatened vengeance on the intruder. The Garret and the Garden |R.M. Ballantyne 

Upon the intruder he turned a crimson, furious face, perspiration gleaming like varnish on brow and nose. St. Martin's Summer |Rafael Sabatini 

As Garnache's tall figure loomed before him he let the girl go and turned a half-laughing, half-startled face upon the intruder. St. Martin's Summer |Rafael Sabatini 

Maxgregor grimly intimated that the basin was at the disposal of the intruder, who did not cease to pour out floods of apologies. The Weight of the Crown |Fred M. White