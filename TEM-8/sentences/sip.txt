Inside, patrons can sip on bespoke whisky and coffee while getting that buffed and polished look. The Most Exciting New Hotels, Restaurants, and Submarines of 2014 |Charlie Gilbert |December 29, 2014 |DAILY BEAST 

“We try to not but before the show, a little sip,” she said. I Got Kicked Out Of The Victoria’s Secret Fashion Show |Nico Hines |December 3, 2014 |DAILY BEAST 

Venetians sip their coffee in quiet squares and walk their dogs along the waterfront with nary a tourist in sight. After the Wedding: George Clooney and Amal Alamuddin in Venice |Barbie Latza Nadeau |September 28, 2014 |DAILY BEAST 

Now, there are several high quality, handcrafted tequilas out there you can sip and enjoy just as much as a fine whiskey. Grab A Shot Glass: Craft Tequila Needs Your Help |Kayleigh Kulp |September 7, 2014 |DAILY BEAST 

Sip on This You may have heard that alcohol is packed with empty calories, which is true. Is Alcohol Killing Your Workout? |DailyBurn |September 2, 2014 |DAILY BEAST 

When Fate has first granted just a sip of the wine of success the slip between the cup and lip comes hardest. Gallipoli Diary, Volume I |Ian Hamilton 

He felt that to eat would choke him, but forced himself to take a sip of coffee and a bit of corn bread. The Cromptons |Mary J. Holmes 

How daintily they sip it; how happy they seem; how that lucky rogue of an Irishman prattles away! Little Travels and Roadside Sketches |William Makepeace Thackeray 

But at this Worse looked so ferocious, that his friend took a long sip, and followed it by a fit of coughing. Skipper Worse |Alexander Lange Kielland 

Billy shook hands, and took a sip out of the case-bottle, by way of clenching the reconciliation. The Floating Light of the Goodwin Sands |R.M. Ballantyne