They lifted her up, and when they saw that she was laced too tightly, they cut the stay lace in two. In New Brothers Grimm 'Snow White', The Prince Doesn't Save Her |The Brothers Grimm |November 30, 2014 |DAILY BEAST 

Nonetheless, Turing killed himself on June 7, 1954, in a deliberately prepared way, by eating a cyanide-laced apple. The Castration of Alan Turing, Britain’s Code-Breaking WWII Hero |Clive Irving |November 29, 2014 |DAILY BEAST 

About $10 billion was earmarked to build a bridge across the Kerch Strait, with its unpredictable currents during spring floods. Putin's Crimea Is a Big Anti-Gay Casino |Anna Nemtsova |September 8, 2014 |DAILY BEAST 

Here, the whiskey-laced meat masters give you their favorite recipes. Epic Meal Empire’s Meat Monstrosities: From the Bacon Spider to the Cinnabattleship |Harley Morenstein |July 26, 2014 |DAILY BEAST 

Next he laced on first the right skate, with a pad of cotton under the tongue, and then the left. Gordie Howe Hockey’s Greatest War Horse |W.C. Heinz |May 31, 2014 |DAILY BEAST 

The children of thy barrenness shall still say in thy ears: The place is too strait for me, make me room to dwell in. The Bible, Douay-Rheims Version |Various 

Departure on the fourth voyage, accompanied by a merchant-ship bound through Torres Strait. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Directions for the passage within the reefs through Torres Strait. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

And the brave explorer sailed safely through the dangerous strait now named for him. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

On the south-side of Clarence Strait the land is low, like the coast to the eastward. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King