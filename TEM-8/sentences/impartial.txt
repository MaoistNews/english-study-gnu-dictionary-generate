Finding an impartial opinion on blockchain projects isn’t easy. The blockchain industry faces a moment of truth as high-profile projects go live |Jeff |October 21, 2020 |Fortune 

While national security as a blanket term is all well and good, CFIUS itself and its ability to stay impartial is under question. Has CFIUS become a tool for crony capitalism? |Lucinda Shen |October 19, 2020 |Fortune 

She is trying to appear impartial on matters that could come before the court. 4 takeaways from Supreme Court nominee Amy Coney Barrett’s final day of questioning |Amber Phillips |October 14, 2020 |Washington Post 

It is unreasonable to think that she would be able to administer fair and impartial justice to our communities if she can’t even accept our basic humanity and dignity. Barrett dodges on same-sex marriage, downplays ties to anti-LGBTQ law firm |Chris Johnson |October 13, 2020 |Washington Blade 

Nigerian historian Uche Uwaezuoke Okonkwo also posits that a more impartial analysis would have earned decolonial leaders in their own right, Margaret Ekpo, Janet Mokelu, or Funmilayo Ransom-Kuti, as “mother of Nigerian nationalism.” At 60, Nigeria Is Still A Country Of The Future |cmurray |October 1, 2020 |Essence.com 

Carles told me that MormonThink strives to be objective and impartial. God vs. the Internet. And the Winner is… |Michael Schulson |November 16, 2014 |DAILY BEAST 

How can a Tea Party-backed judge ever be fair and impartial given the rigid right-wing ideology of that movement?! In North Carolina, GOP Overreach May Be More Unpopular Than Obama |Dean Obeidallah |November 3, 2014 |DAILY BEAST 

D'Souza did not say whether or not he had similar concerns about whether or not Michael Brown had received impartial justice. Dinesh D’Souza: Ferguson Protesters Are Just Like ISIS |Olivia Nuzzi |August 25, 2014 |DAILY BEAST 

Because my disproportionate reaction is precisely why the law needs to be impartial. How to Stay Liberal After You Get Robbed |Kelly Williams Brown |June 29, 2014 |DAILY BEAST 

He said that I was being watched and my impartial journalism was appreciated. Fighting The Talibanization Of Pakistan |Dr. Mona Kazim Shah |June 14, 2014 |DAILY BEAST 

How well they have merited that Degree of Confidence is left to the impartial World to determine. Remarks on a Pamphlet Lately published by the Rev. Mr. Maskelyne, |John Harrison 

Should you bestow upon my letter a fair and impartial perusal, it will neither be useless to you nor to my country. Madame Roland, Makers of History |John S. C. Abbott 

Bill had been a frequent and impartial visitor to the bottles that were tucked away at both ends of his store. Mystery Ranch |Arthur Chapman 

Oh, no, the man so speaking is not doing so of himself; the avowals are too frank—the opinions too impartial. Balsamo, The Magician |Alexander Dumas 

Philip started, for his impartial mind, like Andrea's, was struck by the painful loneliness in which the youth was left. Balsamo, The Magician |Alexander Dumas