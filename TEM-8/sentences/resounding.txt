For company is younger brother SpecialNedu, who finds himself almost always at the receiving end of Mama Chinedu’s resounding smacks. 18 Comics of Tomorrow |Sohini Das Gupta |August 1, 2021 |Ozy 

“West Virginia is a resounding no on these agreements and will continue to litigate and negotiate outside the framework of today’s announcement,” Morrisey said in a statement Wednesday. Tentative opioid deal would provide $26 billion and a new way to regulate painkillers |Meryl Kornfield, Lenny Bernstein |July 22, 2021 |Washington Post 

RNC Chairwoman Ronna McDaniel called the ruling “a resounding victory for election integrity and the rule of law.” Supreme Court upholds Arizona voting laws that lower court found were unfair to minorities |Robert Barnes |July 1, 2021 |Washington Post 

If you’re wondering if it lives up to that buzz, the answer is a resounding “yes.” Triumphant ‘In the Heights’ is the musical we need |John Paul King |June 11, 2021 |Washington Blade 

In West Bengal, a crucial state where Modi was aggressively campaigning this spring as cases were spiking, voters handed him a resounding defeat. 'How Can Modi Be Forgiven?' India's COVID-19 Crisis May Be Turning the Middle Class Against the Prime Minister |Nilanjana Bhowmick/New Delhi |May 7, 2021 |Time 

With every fiber in my body I hope the answer is a resounding “No.” As Michael Brown Grand Jury Winds Down, Is Ferguson on the Brink of War? |Ron Christie |November 16, 2014 |DAILY BEAST 

It was such a resounding failure, but I was coming at that conversation as a fan of the movie as a pop culture artifact. Andy Cohen Reveals His ‘Watch What Happens Live’ Dream Guests |Kevin Fallon |July 16, 2014 |DAILY BEAST 

With more than 95 percent of precincts reporting, Lankford held a resounding 57.5 percent to 34.3 percent lead over Shannon. Tea Party Darling T.W. Shannon Crashes to Oklahoma Senate Primary Defeat |Tim Mak |June 25, 2014 |DAILY BEAST 

Another resounding factor was the role Sen. Tom Coburn played in the race. Tea Party Darling T.W. Shannon Crashes to Oklahoma Senate Primary Defeat |Tim Mak |June 25, 2014 |DAILY BEAST 

Hollywood bestows trophies on movies and actors and the general public responds with a resounding “WTF!?” The Worst Oscar Winners, From ‘Rocky’ and ‘Crash’ to Gwyneth Paltrow |Kevin Fallon, Marlow Stern |February 26, 2014 |DAILY BEAST 

The sewing-machine made a resounding clatter in the room; it was of a ponderous, by-gone make. The Awakening and Selected Short Stories |Kate Chopin 

Yet these resounding phrases doubtless meant something less to Americans of 1764 than one is apt to suppose. The Eve of the Revolution |Carl Becker 

The man followed the Marquise across the bare floor, their steps resounding as they went, and he held the outer door for her. St. Martin's Summer |Rafael Sabatini 

He brought his fist down with a resounding blow on the table beside which they were sitting. The Courier of the Ozarks |Byron A. Dunn 

I should think 'twould make a splendid subject—you could use such sonorous, resounding words. The Sunbridge Girls at Six Star Ranch |Eleanor H. (Eleanor Hodgman) Porter