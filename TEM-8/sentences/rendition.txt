For example, while Southern enslavers emphasized their summary right of recaption of fugitives, abolitionists emphasized states’ authority to require due process in renditions. Working to end slavery, Lincoln found power — and limits — in the Constitution |Elizabeth R. Varon |January 22, 2021 |Washington Post 

He said their table-pounding elevates their renditions, which have since attracted the singer’s attention, into something more upbeat. With an Adele song as its battle cry, a lowly English soccer club looks to rewrite history |Glynn A. Hill |January 21, 2021 |Washington Post 

Then a couple months ago, a 26-year-old Scottish postman named Nathan Evans sang a rendition on TikTok that made the world become re-obsessed. Sea shanties show TikTok is the global proving grounds for culture |Robert Hackett |January 19, 2021 |Fortune 

I love this song and even did my own rendition of it last year. Charlie Wilson Embraces 2021 With A ‘Thankful New Year’ Playlist |cmurray |January 1, 2021 |Essence.com 

When you add the musical contributions of Branford Marsalis and a handful of stellar renditions of some of Rainey’s classic blues songs, you have more than enough ingredients to make a damn good movie. Netflix delivers bold, bisexual ‘Ma Rainey’ |John Paul King |December 19, 2020 |Washington Blade 

The agency will neither defend the so-called rendition, detention, and interrogation programs. CIA Won’t Defend Its One-Time Torturers |Shane Harris, Tim Mak |December 6, 2014 |DAILY BEAST 

For instance, the Beatles rendition of Crying, Waiting, Hoping, the great Buddy Holly song. Greil Marcus Talks About Trying to Unlock Rock and Roll in 10 Songs |Allen Barra |November 17, 2014 |DAILY BEAST 

Earle also gave a short musical performance, which included this rendition of his own “Christmas in Washington.” Watch Steve Earle Rant About GOP Victory |The Daily Beast Video |November 6, 2014 |DAILY BEAST 

He is also supposed to have serenaded his captives with his own rendition of Charles Aznavour love songs. French Jihadi Mehdi Nemmouche Is the Shape of Terror to Come |Christopher Dickey |September 9, 2014 |DAILY BEAST 

In his rendition Charlie Sheen stands in a foyer and overturns a bucket filled with checks, not ice water. #IceBucketChallenge Wisdom From 'Jackass' Steve-O |Kevin Zawacki |August 21, 2014 |DAILY BEAST 

In “The Miller who grinds for Love,” the feeling and intensity and dramatic quality he puts into its rendition are stirring. The Real Latin Quarter |F. Berkeley Smith 

Why cannot the same principle be applied to the rendition of fugitives from service? A Report of the Debates and Proceedings in the Secret Sessions of the Conference Convention |Lucius Eugene Chittenden 

No loophole could be found in the Canadian law that would permit the rendition of a slave. The Journal of Negro History, Volume 5, 1920 |Various 

Miss Waddleton vetoed my plans for the rendition of the balcony scene at commencement next month. Fibble, D. D. |Irvin Shrewsbury Cobb 

He called John McCulloch to his box one night and congratulated him on his successful rendition of the part he was playing. Abraham Lincoln: Was He A Christian? |John B. Remsburg