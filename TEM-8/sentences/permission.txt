Sergey Toshin, founder of Oversecured, told TechCrunch, that the malicious app could also hijack TikTok’s app permissions, allowing it access to the Android device’s camera, microphone and private data on the device, like photos and videos. TikTok fixes Android bugs that could have led to account hijacks |Zack Whittaker |September 11, 2020 |TechCrunch 

In the past, if she wanted to stand up in her physical classroom, she had to first ask permission. Healthy screen time is one challenge of distance learning |Kathryn Hulick |September 11, 2020 |Science News For Students 

He blogs at The Splintered Mind, where this post originally appeared and is reprinted here with permission. Believing in Monsters: David Livingstone Smith on the Subhuman - Facts So Romantic |Eric Schwitzgebel |September 11, 2020 |Nautilus 

In the case of 1Password on Android, you’ll need to make sure that you’ve enabled permissions in the Accessibility menu in Settings. How to get started using a password manager |David Nield |September 8, 2020 |Popular-Science 

The bill would have allowed single-family home owners to build duplexes on their lot, without requiring special permission. Morning Report: Hotel Workers Want Their Jobs Back |Voice of San Diego |September 8, 2020 |Voice of San Diego 

Parents who want to transfer custody of a child to someone other than a relative must seek permission from a judge. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 

The pilot asked air-traffic control for permission to climb from 32,000 to 38,000 feet to avoid the bad weather. Wreckage, Bodies of AirAsia Crash Found |Lennox Samuels |December 30, 2014 |DAILY BEAST 

Reprinted by permission of Kingswell/Disney Publishing Worldwide. Carla Hall’s Christmas Day Treat: Rum Balls |Carla Hall |December 25, 2014 |DAILY BEAST 

Excerpted from Moneyball for Government, published by Disruption Books, and reprinted with permission. Can the U.S. Government Go Moneyball? |Peter Orszag, Jim Nussle |December 23, 2014 |DAILY BEAST 

The preacher gave sheriff deputies permission to search his SUV but warned them “there was something bad” inside. Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 

As long as may be necessary, Sam,” replied Mr. Pickwick, “you have my full permission to remain. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

We idlers had permission granted us to land and visit the town, in which, however, we found but little to admire. A Woman's Journey Round the World |Ida Pfeiffer 

She had hidden his hat and would insist on his playing; that was, if I did not mind and her uncle gave his permission. The Soldier of the Valley |Nelson Lloyd 

The nod of assent was given, and the permission put in force with hearty good will. The Book of Anecdotes and Budget of Fun; |Various 

"You must promise never again to leave without permission, or this is your last scout with me," said Harry, sternly. The Courier of the Ozarks |Byron A. Dunn