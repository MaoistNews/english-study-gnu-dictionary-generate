We handed him a packet enumerating our observations and ideas. American Schools Are Failing to Equip Students With Racial Literacy—And We've Seen Firsthand How It Happens |Priya Vulchi |August 13, 2021 |Time 

Her reporting uncovered people who not only hear what trees have to say, but can enumerate essential roles of wooded areas as homes for other plants and animals, including humans. Tasking trees with averting the climate crisis is a big ask |Nancy Shute |July 3, 2021 |Science News 

A legal document his attorneys submitted to the California Supreme Court enumerates various frustrations. The American parole system is an endless trap — and a moral outrage |Jennifer Miller |May 24, 2021 |Washington Post 

The once-in-a-decade project of enumerating every person in every household in the country inevitably results in some being missed and some being counted more than once. Will Americans be able to trust the results of the 2020 Census? |Tara Bahrampour |April 22, 2021 |Washington Post 

“Word gets out quickly,” said Larry Cartwright, who compiles the District’s annual Christmas bird count, when birders from all over identify and enumerate as many birds as they can see. Catch me if you can: An unlikely hummingbird is banded at a Virginia park |John Kelly |January 18, 2021 |Washington Post 

For reasons Lehman may someday wish to enumerate, he and Hitchcock had a falling out. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Mustafa then proceeded to enumerate five factors that he believed are contributing to the prevailing despair. The Rise Of Palestinian Non-Violent Resistance: A Conversation With Mustafa Barghouti |Raja Shehadeh |March 7, 2013 |DAILY BEAST 

He called Asali a “visionary” and “trail blazer” and went on to enumerate a list of US politicians who have had dinner with ATFP. Beyond Words: A Response to Hussein Ibish |Susan Abulhawa, Sa’ed Atshan |May 11, 2012 |DAILY BEAST 

The meeting wound down forlornly, with Obama attempting to enumerate issues that the two sides had in common. What Was Obama Thinking? |Tunku Varadarajan |February 25, 2010 |DAILY BEAST 

The two passages enumerate the scales in a slightly different manner. The Modes of Ancient Greek Music |David Binning Monro 

It would be difficult to enumerate the many thoughts suggested by these words; each person has his own idea of wasting time. English: Composition and Literature |W. F. (William Franklin) Webster 

Time would fail us to enumerate the various objects and acts of typical service which were all fulfilled in Him. Separation and Service |James Hudson Taylor 

Because he knows how to enumerate; which indeed he knows if he know the number, and this he can know only if the number exist. Plotinos: Complete Works, v. 3 |Plotinos (Plotinus) 

On many accounts your station is critical; I shall enumerate only a few. Journal and Letters of Philip Vickers Fithian: A Plantation Tutor of the Old Dominion, 1773-1774. |Philip Vickers Fithian