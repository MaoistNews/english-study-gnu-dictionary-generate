It also had a strikingly large, oblong carapace with the mouth and claws at the front and eyes at the very back. This ancient ‘mothership’ used probing ‘fingers’ to scrape the ocean floor for prey |Kate Baggaley |September 9, 2021 |Popular-Science 

Several of Max-Karl Winkler’s subjects are enclosed within circles, and in his “The Checkered Scarf,” a woman’s torso, topknot and titular scarf all protrude from the oblong that contains her head. In the galleries: Posters as a medium for serious but jubilant communication |Mark Jenkins |July 9, 2021 |Washington Post 

Each Dime bud comes equipped with an oblong stem, about the size and shape of a Mike and Ike candy. Skullcandy Dime earphones review: These wireless earbuds are a sweet deal |Billy Cadden |June 28, 2021 |Popular-Science 

In the videos, the Brazil nuts, which are oblong, mostly laid horizontally when they were first dumped into the container. X-ray scans explain how the ‘Brazil nut effect’ works |Maria Temming |April 22, 2021 |Science News 

An oblong camera that captured and saved photos and short videos of you in various outfits, it used a combination of artificial intelligence and “advice from fashion specialists” to judge how you looked and give advice. For Amazon’s $25 custom T-shirt, your body is a wonderland (of data) |Heather Kelly |January 5, 2021 |Washington Post 

Or, maybe a satellite in an oblong orbit around earth—the closer it gets, the faster it flies. Telling My Family That I’m Leaving Again for Afghanistan |Nick Willard |April 9, 2014 |DAILY BEAST 

Flustered, I typed “iTunes” into the tiny oblong search box to see how many charges there were in total. Woman Finds Mysterious Charges on Her iTunes Bill: A Modern Whodunit! |Nancy Neufeld Callaway |January 31, 2014 |DAILY BEAST 

An oblong bomb-hole marked the back wall, and windows were empty of their glass. Kids of War: Libyan Children Fight a New Battle at School |Danielle Shapiro |June 30, 2012 |DAILY BEAST 

Gently it slid across the oblong of sunlight, blotting out the figures of the two men from her sight. Bella Donna |Robert Hichens 

As that big oblong crowd of Turks showed their left flank to Baikie's nine batteries they were swept in enfilade by shrapnel. Gallipoli Diary, Volume I |Ian Hamilton 

Its white front glimmered indistinctly through the trees, showing only one oblong of light on the lower floor. Summer |Edith Wharton 

She turned the oblong handle which released two of the windows, pulled it towards her, and drank in the fresh night air. A Butterfly on the Wheel |Cyril Arthur Edward Ranger Gull 

Her deck appearance is that of an oblong raft, and the section of her hull is that of a flattened W. Yachting Vol. 2 |Various.