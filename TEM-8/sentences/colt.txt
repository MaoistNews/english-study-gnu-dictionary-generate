You progress in the game every time Colt discovers a new lead, information critical to breaking the loop, the game’s principle quest. Don’t read this ‘Deathloop’ preview |Mike Hume |August 26, 2021 |Washington Post 

The paranoia sets in and wreaks havoc on your decision-making, just as it would a character in Colt’s situation. Don’t read this ‘Deathloop’ preview |Mike Hume |August 26, 2021 |Washington Post 

The learning process begins on the aforementioned beach, where Colt stumbles ahead guided by the map’s design and floating words that appear to be visual reminders from Colt’s runs during previous loops. Don’t read this ‘Deathloop’ preview |Mike Hume |August 26, 2021 |Washington Post 

My head threatened to explode and at times I felt my heart beating like a wild colt. Locked up in the Land of Liberty: Part IV |Yariel Valdés González |July 28, 2021 |Washington Blade 

Jockey John Velazquez and dark bay colt Medina Spirit delivered trainer Bob Baffert his record-setting seventh Kentucky Derby victory two weeks ago, and then they set their sights on the second jewel of the Triple Crown. Two horses that can beat Kentucky Derby winner Medina Spirit at the Preakness Stakes |Neil Greenberg |May 13, 2021 |Washington Post 

Political ads demonizing some politicians and praising others play on as an older man sips his Colt 45. Gary, Indiana Is a Serial Killer’s Playground |Justin Glawe |October 22, 2014 |DAILY BEAST 

As Americans say, ‘God made every man different; Sam Colt made them equal!’ Can Ukraine Control Its Far Right Ultranationalists? |Oleg Shynkarenko |March 1, 2014 |DAILY BEAST 

Colt, Remington, and Gauge have all soared, and Gunner is much more common than the traditional name Gunnar. Baby Names: Gunning For Trouble? |Abby Haglage |February 19, 2014 |DAILY BEAST 

In 2002, only 194 babies were named Colt, while in 2012 there were 955. Baby Names: Gunning For Trouble? |Abby Haglage |February 19, 2014 |DAILY BEAST 

He gives Carl back his gun and holsters his own famous Colt Python back around his waist. The Walking Dead ‘Infected’ Recap: There Will Be Exploding Faces |Melissa Leon |October 21, 2013 |DAILY BEAST 

Ever since his majority Lord Hetton had annually entered a colt in the great race. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

"Never teched the Yank or tree, either," and he kicked up his heels like a young colt. The Courier of the Ozarks |Byron A. Dunn 

And they went away, and found a colt tied at the door without in the open street: and they loose him. His Last Week |William E. Barton 

Get the gray colt and I'll give you a lift over the mountain, but I'll bring you back on Monday, too. The Soldier of the Valley |Nelson Lloyd 

The gray colt halted to catch his breath, and with the whip I pointed to the west, glowing with the warm evening fires. The Soldier of the Valley |Nelson Lloyd