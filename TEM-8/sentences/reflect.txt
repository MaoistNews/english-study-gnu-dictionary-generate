“It was pretty crazy how it all came about,” he said this week, reflecting on his rapid rise. Matthew Hoppe was a little-known American soccer player — until he reached the Bundesliga |Steven Goff |February 11, 2021 |Washington Post 

Tiffany Shackelford had a unique, fun style that her friends and family members said reflected her bright personality, sense of humor and love of helping others to connect. Tiffany Shackelford, 46, was known as a unique, fun ‘force’ to friends, family |Dana Hedgpeth |February 11, 2021 |Washington Post 

If my personality profile reflected the traits most specific to people who are successful in the role, I’d advance to the next hiring stage. Auditors are testing hiring algorithms for bias, but there’s no easy fix |Amy Nordrum |February 11, 2021 |MIT Technology Review 

“The time is right to reimagine our entire game day experience, to reinvent it in a way that reflects our modern identity and aligns with what today’s fan seeks,” team president Jason Wright said in a news release. Former cheerleaders settle with Washington Football Team as program’s future is in doubt |Beth Reinhard |February 10, 2021 |Washington Post 

This means the agency may be logging debts from companies that no longer exist or have accounts on their books that do not accurately reflect the amount due. Utility Companies Owe Millions to This State Regulatory Agency. The Problem? The Agency Can’t Track What It’s Owed. |by Scott Morris, Bay City News Foundation |February 10, 2021 |ProPublica 

They just reflect the range of breeds that were used to create the Heck cattle in the first instance. ‘Nazi Cows’ Tried to Kill British Farmer |Tom Sykes |January 6, 2015 |DAILY BEAST 

But it is not only small airlines that reflect the laxity of the system. Who Will Get AsiaAir 8501’s Black Boxes? |Clive Irving |December 30, 2014 |DAILY BEAST 

This does not reflect lack of interest in a better environment. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

In many ways these attitudes reflect the increasingly urban-centric focus of the party. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

Note: This piece was updated to reflect that Mrs. Landingham died while Aaron Sorkin was still writing The West Wing. 'The Newsroom' Ended As It Began: Weird, Controversial, and Noble |Kevin Fallon |December 15, 2014 |DAILY BEAST 

He was too drowsy to hold the thought more than a moment in his mind, much less to reflect upon it. The Wave |Algernon Blackwood 

It goes far to reconciling me to being a woman when I reflect that I am thus in no danger of ever marrying one. Pearls of Thought |Maturin M. Ballou 

And as bronze reflects the light, her mentality seemed to reflect all the cold lights in her nature. Bella Donna |Robert Hichens 

Gwynne made a wry face as he sat down before the dressing-table that he might reflect his visage while he brushed his hair. Ancestors |Gertrude Atherton 

This accident led his parents to reflect upon the childs incurable tendency and consider the question of his musical education. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky