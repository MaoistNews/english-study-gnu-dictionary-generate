According to its tax records, MHS spent $91,095 per child on academics, sports, trips, mental and physical health care, scholarships and living expenses in the 2018-19 academic year — intended as a glide path to the middle class. “I Finally Got to the Mountaintop and I Failed” |by Bob Fernandez, The Philadelphia Inquirer, and Charlotte Keith, Spotlight PA |June 10, 2021 |ProPublica 

The company may also attempt glide flights for its newly unveiled SpaceShipThree this summer. Virgin Galactic’s SpaceShipTwo has flown to the edge of space |Neel V. Patel |May 24, 2021 |MIT Technology Review 

VSS Imagine will soon begin ground testing, with glide flights planned for this summer from Virgin Galactic’s Spaceport America base in New Mexico. This Week’s Awesome Tech Stories From Around the Web (Through April 3) |Singularity Hub Staff |April 3, 2021 |Singularity Hub 

My new faves have a smooth glide, good hold on the uphill, solid speed on the downhill, and edges that handle well on narrow trails with mellow turns. The Gear Our Editors Loved in December |The Editors |January 6, 2021 |Outside Online 

This dual-bevel sliding miter saw comes with a glide system that allows for wider, more precise crosscuts. Make every project a breeze with the right miter saw |PopSci Commerce Team |August 26, 2020 |Popular-Science 

Angelina Jolie was able to seemingly glide into the Vatican on Thursday to present her new film ‘Unbroken.’ Pope Francis Has the Pleasure of Meeting Angelina Jolie for a Few Seconds |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

With the South Carolina GOP primary behind him, Sen. Lindsey Graham appears to be on a glide path to re-election. T-Rav: The Reality TV Star Running for Senate in South Carolina |Patricia Murphy |July 4, 2014 |DAILY BEAST 

He started to glide the window back up to get out of the car, and at once the officer began to beat his gun butt on the window. The Cost: What Stop and Frisk Does to a Young Man’s Soul |Rilla Askew |May 21, 2014 |DAILY BEAST 

I describe the pink hued iridescent bubbles in the bathtub, and the way they glide away from my skin as if it's made of silk. ASMR and the Rise of the Whisper Fetish |Aurora Snow |December 7, 2013 |DAILY BEAST 

McAuliffe had been given a glide path to the nomination and had the active support of almost every Democratic elected official. How the Richer, Better Run Campaign Barely Won in Virginia |Ben Jacobs |November 7, 2013 |DAILY BEAST 

Immediately the door was opened just enough to let the two men glide in; then it was shut with a bang and bolted. The Garret and the Garden |R.M. Ballantyne 

They glide across my face with tender, soft caress, and I feel something melt within me. Prison Memoirs of an Anarchist |Alexander Berkman 

Fortunately, childhood is protected by a resisting candour, by an enamel over which all impurities glide. The Nabob |Alphonse Daudet 

Then he stooped down and begun to glide along the wall, just his shoulders showing over the people's heads. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 

He noted that Sssuri had stepped up the pace, passing into his sure-footed glide which made Dalgard exert himself to keep up. Star Born |Andre Norton