Plus, Treasury yields have increasingly been driven by technical factors that are divorced from economic fundamentals, says Jim Caron, head of macro strategies for global fixed income at Morgan Stanley Investment Management. The bond market is warped |Kate Marino |September 29, 2021 |Axios 

Farming and herding communities across Xinjiang, Tibet and Inner Mongolia are being forced to settle in new, fixed and thus monitorable communities. How Beijing Is Redefining What It Means to Be Chinese, from Xinjiang to Inner Mongolia |CHARLIE CAMPBELL/SHANGHAI |July 12, 2021 |Time 

It’s expensive to build out fixed broadband networks in rural areas like Fallbrook and Rainbow, so many major providers just don’t. North County Report: Feds Helping to Bridge Digital Divide, for Now |Kayla Jimenez |May 26, 2021 |Voice of San Diego 

Fathoming one’s gender, an identity innate and performed, personal and social, fixed and evolving, is complicated enough without being under a spotlight that never seems to turn off. Elliot Page Is Ready for This Moment |Katy Steinmetz |March 16, 2021 |Time 

In 2012, the researchers calculated a ceiling on how statistically correlated the polarization results can be with the rotations performed at A and B if the rotations occurred in a fixed causal order. Quantum Mischief Rewrites the Laws of Cause and Effect |Natalie Wolchover |March 11, 2021 |Quanta Magazine 

One Air Force official said that with enough time and more money, the EOTS could be fixed. Newest U.S. Stealth Fighter ‘10 Years Behind’ Older Jets |Dave Majumdar |December 26, 2014 |DAILY BEAST 

People on fixed incomes and government pensions are the first to feel the pain. How Crimea Crashed the Russian Economy |Anna Nemtsova |December 17, 2014 |DAILY BEAST 

In 1870, the very Germanically-named August Ruengling fixed a harness for a circus rider and obtained free passes for his family. We’re All Carnies Now: Why We Can’t Quit the Circus |Anthony Paletta |November 27, 2014 |DAILY BEAST 

The teen refused to drop his knife, according to officers, fixed them with “a 100-yard stare,” and walked toward them. The 14 Teens Killed by Cops Since Michael Brown |Nina Strochlic |November 25, 2014 |DAILY BEAST 

But they have high fixed costs—overhead, maintenance, staff, and power. Solar Powered Ski Lift |The Daily Beast |November 24, 2014 |DAILY BEAST 

As men fixed in the grip of nightmare, we were powerless—unable to do anything but wait. Gallipoli Diary, Volume I |Ian Hamilton 

Her eyes, for a moment, fixed themselves with a horrid conviction of a wide and nameless treachery. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The Princess still kept her eyes fixed on Louis, while, in a suppressed and unsteady voice, she answered her governess. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

She kept her eyes fixed steadily on his, saying what followed gently, calmly, yet as though another woman spoke the words. The Wave |Algernon Blackwood 

“It looks better than any house around here now, since you fixed it up and painted it,” said Sol. The Bondboy |George W. (George Washington) Ogden