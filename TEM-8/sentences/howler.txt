Monkeys, including baboons, macaques and howler monkeys, make up another group of primates. Scientists Say: Primate |Maria Temming |May 23, 2022 |Science News For Students 

Meanwhile, the idea that kids and chores would somehow fit into a standard workweek is a howler. Carolyn Hax: Wife becomes at-home mom, husband quits all ‘house and kid stuff’ |Carolyn Hax |January 18, 2022 |Washington Post 

“No matter the cause, she moves with such skill, she seems to know secrets I never will,” goes one of the many howlers in David Bryan and Joe DiPietro’s lay-it-on-thick score. In time for Thanksgiving, Broadway serves up a turkey: ‘Diana’ the musical |Peter Marks |November 18, 2021 |Washington Post 

On one cattle ranch, he remembers, one partner knew that there was a howler monkey living on a forest island alongside several dozen workers. This wild grassland in the Amazon isn’t as untouched as it seems |Philip Kiefer |October 7, 2021 |Popular-Science 

The latest is maybe the biggest howler yet, bigger than even the ad that mistook Duke basketball players for UK Wildcats. Mitch McConnell’s Big Obamacare-Kynect Lie |Michael Tomasky |May 28, 2014 |DAILY BEAST 

His portrait of Izzy Yanay, a partner in the highly regarded Hudson Valley Foie Gras, is a howler. A Three-Star Food Fight |Bob Spitz |March 12, 2009 |DAILY BEAST 

I remember, some years ago, how merrily you used to laugh about the "calamity-howler," whose habitat at that time was Kansas. Humanly Speaking |Samuel McChord Crothers 

The old man was alive during the telephone call from the Grand Central, and dead when the howler was put on for the first time. Whispering Wires |Henry Leverage 

The telephone call at five minutes past twelve, and the howler put on soon afterward, checks up. Whispering Wires |Henry Leverage 

She came a fearful howler over a book which she herself has read, to my knowledge, within the last fortnight. Some Persons Unknown |E. W. Hornung 

A reiterated word of the convulsive howler on the dock had stuck in the Tyro's mind. Little Miss Grouch |Samuel Hopkins Adams