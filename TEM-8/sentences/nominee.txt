The Golden Globes 2021 nominees were announced today in Los Angeles and there were a multitude of LGBTQ actors, films, television and other nominees announced. Nominees (and 2 winners) for 2021 Golden Globes announced |Troy Masters |February 3, 2021 |Washington Blade 

This is because the wealth in his name really belongs to Putin, claims FBK — “he is just a nominee”, says Pevchikh. The Inside Story of How Alexey Navalny Uncovered Putin's $1.3 Billion Palace |Madeline Roache |January 29, 2021 |Time 

The legislators drew those nominees from a pool of more than 1,200 citizens who had applied for the commission, which was created after voters approved it in a constitutional amendment in November. Citizen members chosen for Virginia’s new bipartisan redistricting commission |Gregory S. Schneider |January 6, 2021 |Washington Post 

More than 9 in 10 Republicans supported both Perdue and Loeffler, while more than 9 in 10 Democrats supported both their party’s nominees, Ossoff and Warnock. Pair of Georgia runoff races are razor close with U.S. Senate control at stake |Felicia Sonmez, Colby Itkowitz, John Wagner, Paulina Firozi, Amy B Wang |January 6, 2021 |Washington Post 

The nominees for all of the categories at The Game Awards 2020 can be found here. ‘The Last of Us Part II’ wins game of the year at The Game Awards, alongside new ‘Mass Effect’ and ‘Among Us’ reveals |Elise Favis, Gene Park, Mikhail Klimentov |December 11, 2020 |Washington Post 

Should lightning strike and Hillary Clinton forgoes a presidential run, Democrats have a nominee in waiting. Sen. Warren’s Main Street Crusade to Pressure Clinton |Eleanor Clift |January 8, 2015 |DAILY BEAST 

By contrast, John McCain, the eventual GOP nominee, had raised approximately $12.7 million in the first quarter of 2007 alone. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

Boehner was unanimously selected by the conference as its official nominee for speaker in the coming Congress. The YOLO Caucus' New Cry for Attention |Ben Jacobs |January 4, 2015 |DAILY BEAST 

Why a 26-year-old with no ties to the 2012 GOP nominee and no campaign experience has them worried. ‘Ready for Romney’ Is Amateur Hour |Tim Mak |December 23, 2014 |DAILY BEAST 

Lisa Kudrow - The Comeback How—HOW—is Lisa Kudrow not a nominee for the revival of The Comeback? 15 Enraging Golden Globe TV Snubs and Surprises: Amy Poehler, 'Mad Men' & More |Kevin Fallon |December 11, 2014 |DAILY BEAST 

Her nominee was unable to hold his ground in Kerry, nor was the Papal Bishop permanently resident. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell 

The leaders talked to the President singly, in the order of their importance, against his nominee, on the score of party peace. The Art of Disappearing |John Talbot Smith 

Yet this Tory nominee of a Tory Cabinet, in his brief term of office, insured a great advance along this very path toward freedom. The Canadian Dominion |Oscar D. Skelton 

Molly was one of the nominees for secretary and, standing beside a nominee from the opposing side, she also shone in comparison. Molly Brown's Sophomore Days |Nell Speed 

The Indian representative was, of course, sure to be merely a British nominee; the other four would be colonial politicians. The Outline of History: Being a Plain History of Life and Mankind |Herbert George Wells