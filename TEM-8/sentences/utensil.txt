Cutting down on car trips, refusing disposable utensils with takeout, growing your own food—these are all miniscule drops in the bucket when it comes to reversing the effects of greenhouse gases and deforestation. Four science conversations worth having this holiday |Purbita Saha |November 27, 2020 |Popular-Science 

That way you’re minimizing the number of people who are having contact with the food and the utensils versus passing it around the table or having everyone line up or go through a buffet. How to plan a COVID-safe Thanksgiving, week-by-week |Kate Baggaley |October 30, 2020 |Popular-Science 

The three-layer interior can withstand the scrap of metal utensils, which is unique for nonstick cookware. The best cookware sets for every aspiring chef |PopSci Commerce Team |October 28, 2020 |Popular-Science 

Two open cubbies allow you to store notebooks, writing utensils, and papers for easy access and minimal clutter. Narrow desks that can turn any corner into a comfortable workspace |PopSci Commerce Team |October 13, 2020 |Popular-Science 

While sharing food or utensils could theoretically pose a risk for infection, no clusters have been linked to eating itself. COVID-19 case clusters offer lessons and warnings for reopening |Helen Thompson |June 18, 2020 |Science News 

“When the dog licks the utensil, wash it seven times, and rub it with earth the eighth time,” advised one hadith. The Lighter Side of Islam |Neil MacFarquhar |May 6, 2009 |DAILY BEAST 

The beds are clean to an extreme degree, as was likewise every utensil in the kitchen, and the kitchen itself. Travels through the South of France and the Interior of Provinces of Provence and Languedoc in the Years 1807 and 1808 |Lt-Col. Pinkney 

On coming back from her lodge, the outflowing current had carried off her valued utensil. The Indian in his Wigwam |Henry R. Schoolcraft 

They have requisitioned every utensil that will hold water in the village. Under Wellington's Command |G. A. Henty 

There could be no other such in the palace, where every utensil was gold or silver. The Arabian Nights |Unknown 

Has anyone here lost a diminutive utensil containing, unless I am mistaken, a favourite preparation for the toilet? The Circle |W. Somerset Maugham