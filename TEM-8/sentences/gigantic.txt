Meanwhile, Microsoft is also butting heads with Amazon over cloud computing, the gigantic business that involves supplying data center computing power to corporate customers. TikTok: Everything to know about tech’s biggest soap opera |Danielle Abril |September 5, 2020 |Fortune 

It’s important to note, however, that our analysis assumes that the non-car, 15% of Tesla’s business also grows at gigantic, 20%-plus rates in the future. Tesla has a business model problem: It can never justify its current stock price by simply making cars |Shawn Tully |August 29, 2020 |Fortune 

Flashy department stores sprang up in cities, sparkling new malls dotted the suburbs, and big-box stores grew to gigantic proportions around the country. Retail’s future is in Asia |Marc Bain |August 20, 2020 |Quartz 

Fortunately, plasmas can be manipulated using magnetic fields, and so gigantic electromagnets are used to keep the plasma spinning around a donut-shaped reactor called a tokamak. Construction of the World’s Biggest Nuclear Fusion Plant Just Started in France |Edd Gent |August 3, 2020 |Singularity Hub 

Maybe a massive computer-science lab or a well-funded medical-device workshop or a flavor-profile laboratory run by a gigantic food company. Honey, I Grew the Economy (Ep. 399) |Stephen J. Dubner |December 5, 2019 |Freakonomics 

The five men, their Land Rover, and their supplies were loaded into the gigantic military aircraft. The Original Ebola Hunter |Abby Haglage |September 14, 2014 |DAILY BEAST 

A gigantic solar storm could fry power grids, knocking out electricity for months. The Sun Is Pummeling Earth. Now What? Solar Storms for Dummies |Lizzie Crocker |September 12, 2014 |DAILY BEAST 

She throws gigantic, destructive parties, and purposely lights his ottomans on fire. 'BoJack Horseman': The Debauched Tales of a Drunken, Groupie-Sexing D-List Horse, Hits Netflix |Marlow Stern |August 22, 2014 |DAILY BEAST 

In January 1915 gigantic German Zeppelin airships appeared in the night over London and dropped bombs at random. Life Under Air Strikes: Children Under Fire Will Never Forget — or Forgive |Clive Irving |August 3, 2014 |DAILY BEAST 

Consider the gigantic implications of this precise logical observation. Today’s Wonky Elite Is in Love With the Wrong French Intellectual |James Poulos |April 23, 2014 |DAILY BEAST 

He used to walk through the park, and note with pleasure the care that his father bestowed on the gigantic property. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The embankment or road-bed was commenced by gigantic piling, and is very broad and substantial. Glances at Europe |Horace Greeley 

The machine penetrated everywhere, thrusting aside with its gigantic arm the feeble efforts of handicraft. The Unsolved Riddle of Social Justice |Stephen Leacock 

The gigantic pylon, its shoulders breaking the sky four-square far overhead, seemed the prodigious portal of another world. The Wave |Algernon Blackwood 

Emigration is now proceeding with gigantic strides, and is destined for some time to continue. Glances at Europe |Horace Greeley