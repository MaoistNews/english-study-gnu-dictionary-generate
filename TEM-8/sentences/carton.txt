Unless, of course, there’s something saucy in a carton of takeout waiting to be used. The food critic gets critiqued: What Tom Sietsema learned in a year of pandemic home cooking |Tom Sietsema |July 14, 2021 |Washington Post 

As the dairy industry struggles and demand for plant milks booms, shoppers have grown accustomed to seeing cartons of Almond Breeze and Rice Dream. NotMilk says it has achieved a breakthrough: Plant-based milk that mimics dairy |Emily Codik |June 16, 2021 |Washington Post 

The cartons I opened suggested that someone knew their way around a Thai kitchen. Sisters Thai in Potomac reminds us that food isn’t everything |Tom Sietsema |April 23, 2021 |Washington Post 

Even when served in a carton, the entree looks impressive and tastes ambitious. Twin bistros in Northern Virginia put a personal spin on Indian cooking |Tom Sietsema |March 5, 2021 |Washington Post 

I spooned into a carton of winter pumpkin soup from the new-in-November Pennyroyal Station in Mount Rainier and ended up scrape-scrape-scraping the bottom of the package. Like a good neighbor, Pennyroyal Station is there for you |Tom Sietsema |January 22, 2021 |Washington Post 

And at $29.99, lighting up all sense of parental dignity and responsibility costs less than a carton of actual cigarettes. Sexy Ebola Nurse & More of the Year’s Worst Halloween Costumes |Kevin Fallon |October 30, 2014 |DAILY BEAST 

Just be sure to choose a slimmed-down carton since the fat in whole milk can delay fluid replacement. 10 Ways to Stay Hydrated (That Aren’t Water) |DailyBurn |July 25, 2014 |DAILY BEAST 

The police managed to seize an empty egg carton left behind at the scene; they already have a male suspect in custody, they say. How Sexism Could Bring Down Japan’s Government |Angela Erika Kubo, Jake Adelstein |June 25, 2014 |DAILY BEAST 

On each carton of Plan B One-Step, a widely used emergency contraceptive, is an FDA-approved drug label (PDF). Why Can’t the FDA Fix Outdated Birth Control Labels? |Tiffany Stanley |March 22, 2014 |DAILY BEAST 

The child was then sent on his or her way, with a piece of fruit and a carton of milk. The Schools That Starve Students to Punish Deadbeat Parents |Brandy Zadrozny |January 30, 2014 |DAILY BEAST 

A boy with a carton of groceries almost ran down Louis, then ducked down into the delivery entrance of the apartment-hotel. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Up to the present, Mr. Carton has shown that he possesses wit and talent, but neither observation nor the inventive faculty. The English Stage |Augustin Filon 

If it is bought in paper cartons, it should be rewrapped and replaced in the carton each time some is cut off for use. Woman's Institute Library of Cookery, Vol. 2 |Woman's Institute of Domestic Arts and Sciences 

Carton drank the toast, and flung his glass over his shoulder against the wall, where it shivered in pieces. The World's Greatest Books, Vol III |Arthur Mee and J.A. Hammerton, Eds. 

Climbing to a high chamber in a well of houses, Carton threw himself down in his clothes on a neglected bed. The World's Greatest Books, Vol III |Arthur Mee and J.A. Hammerton, Eds.