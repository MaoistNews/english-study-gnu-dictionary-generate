In the outer fringes of the solar system, objects were so cold and slow-moving, apparently, that they could simply touch and stick together. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

Before the pandemic, Frailey knew a little bit about QAnon, but he hadn’t given such an easily debunked fringe theory much of his time. Evangelicals are looking for answers online. They’re finding QAnon instead. |Abby Ohlheiser |August 26, 2020 |MIT Technology Review 

If successful, they might not be on the fringes of American politics for much longer. American Fringes: The Intellectual Dark Web Declares Its Independence |Nick Fouriezos |August 11, 2020 |Ozy 

Altogether, I’ve spent the fringes of three winters there — in 1991, 1995 to 1996, and 2007 to 2008. He Found ‘Islands of Fertility’ Beneath Antarctica’s Ice |Steve Nadis |July 20, 2020 |Quanta Magazine 

Most of the deaths have occurred near the village of Seronga on the northern fringes of the Okavango Delta, a vast swampy inland region that hosts huge wildlife populations. Scientists have some theories on why hundreds of elephants are mysteriously dying in Botswana |Vicky Boult |July 7, 2020 |Quartz 

The Five Percent Nation of Islam was there as well as fringe Stalinist groups. Sharpton Recalls Civil Rights Struggle in DC March Against Police Violence |Ben Jacobs |December 13, 2014 |DAILY BEAST 

GALLERY: 'JUSTICE FOR ALL' MARCH IN WASHINGTON DC There were plenty of representatives of the fringe too. Sharpton Recalls Civil Rights Struggle in DC March Against Police Violence |Ben Jacobs |December 13, 2014 |DAILY BEAST 

Indeed, the chatter for the past year on the anti-gay fringe has been of resistance. The Right Wing Screams for the Wambulance Over Gay Marriage Ruling |Walter Olson |October 13, 2014 |DAILY BEAST 

Indeed, Wolf's journey to the crackpot fringe was completed a long time ago. From ISIS to Ebola, What Has Made Naomi Wolf So Paranoid? |Michael Moynihan |October 11, 2014 |DAILY BEAST 

Unlike Cosby, who had only a fringe of gray hair left, he still sported a silver mane. Why Comedians Still Think Bill Cosby Is a Genius |Mark Whitaker |October 5, 2014 |DAILY BEAST 

These form one of the many island groups that hang like a fringe or festoon on the skirt of the continent of Asia. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

The crowd disposed itself on the fringe of the sward, and the duellists went forward, and set about the preparations. St. Martin's Summer |Rafael Sabatini 

The walls were covered with silk and velvet hangings, ornamented with gold fringe, while rich carpets were spread underfoot. A Woman's Journey Round the World |Ida Pfeiffer 

The late Caleb Whitefoord, seeing a lady knotting fringe for a petticoat, asked her, what she was doing? The Book of Anecdotes and Budget of Fun; |Various 

He was not a man to dodge trouble that might bring profit dangling to the fringe of her skirt. Scattergood Baines |Clarence Budington Kelland