My students were sullen, off-task, unfocused, and wouldn’t talk to each other or to me. School Districts Must Invest in Student and Teacher Well-Being |Louise Williamson |December 16, 2021 |Voice of San Diego 

Thursday was supposed to be the day we shelved all the sullen, sad reminders of the difficulties and tragedies of the past year, if only for three hours. Opening Day was supposed to bring back hope. Instead, the virus won — again. |Barry Svrluga |April 2, 2021 |Washington Post 

There was the Michelin-starred restaurant where the somewhat sullen chef ruled dinner service by fear and intimidation. The Toxicity of Restaurant Kitchens Is Exactly Why I Never Reported Abuses |Lindsey Danis |February 4, 2021 |Eater 

They waved you into the sullen parking garage on — sigh — your word alone. Nick Saban, the ultimate control freak, learned how to handle disruption this season |Chuck Culpepper |January 1, 2021 |Washington Post 

Hard and sullen, she barely bothers to conceal her misanthropy from the tourists who come into her shop, and maintains a near-reclusive distance from residents of the town. Winslet, Ronan have seaside rendezvous in ‘Ammonite’ |John Paul King |December 11, 2020 |Washington Blade 

The pro-Russian rebel militia remained sullen and hostile toward the monitors. In the Killing Fields of Ukraine with Children Who Saw the MH17 Horror |Anna Nemtsova |July 20, 2014 |DAILY BEAST 

Spring is a time of new beginnings, but in the years that followed, I became inward and sullen as those memories took me under. Memorial Days After Mourning Has Passed |Alex Horton |May 25, 2014 |DAILY BEAST 

When the candidate was sullen and grumpy—which was often—he could read his mood and adjust the bubble accordingly. No Drama Obama’s Dramatic 2012 Reelection Campaign |Richard Wolffe |September 12, 2013 |DAILY BEAST 

Strung out on a punishing regimen of diet pills, the once genial young man becomes a sullen, self-pitying wreck. Richard Porton on Steven Soderbergh’s ‘Behind the Candelabra’ |Richard Porton |May 23, 2013 |DAILY BEAST 

Kristen Stewart was cast as Marylou at 17, before she played fair-skinned and often sullen Bella Swan in The Twilight series. ‘On the Road’: Differences Between Jack Kerouac’s Novel and This Year’s Film |Anna Klassen |December 21, 2012 |DAILY BEAST 

They threw down their weapons with sullen obedience and the first great step towards the re-conquest of India was taken. The Red Year |Louis Tracy 

The boy, a trifle sullen since the last words, stood on the hearth with his back to the fire, his hands clasped behind him. St. Martin's Summer |Rafael Sabatini 

But for the trees, these sullen skies and level grounds would render England dreary enough. Glances at Europe |Horace Greeley 

But it was with sullen reluctance; and mutterings were to be heard, on all sides, that the time would come yet. Ramona |Helen Hunt Jackson 

They made an odd procession as they marched out of the hall, under the sullen eyes of the baulked cut-throats and their mistress. St. Martin's Summer |Rafael Sabatini