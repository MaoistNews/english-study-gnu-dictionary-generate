It’s easy to grill a variety of fruit—from pineapple to citrus to stone fruit. 5 exciting ways to make smoky desserts |Purbita Saha |August 22, 2021 |Popular-Science 

The eye-opener of the bunch is the pineapple boat, swollen with a rum syrup and bright with mint and lime. José Andrés brings Spanish comfort food — including a lot of eggs — to Bethesda |Tom Sietsema |July 9, 2021 |Washington Post 

You could also try mellowing the spice with a fruit juice such as apple, lime, orange, or pineapple. The Ultimate Guide to Barbecue Sauces |Lazarus Lynch |June 11, 2021 |Eater 

Serves four4 small pineapples 1⁄2 cup maple syrup, plus more for drizzling 2 cups coconut yogurt 4 limesLight your fire and let it burn down for about one hour, or until you obtain a medium heat. ‘Wild Child’ Is a Perfect Cookbook for Family Campouts |Heather Greenwood Davis |May 30, 2021 |Outside Online 

Place the pineapples, cut side up, in the coals of the fire, and drizzle the maple syrup in the center. ‘Wild Child’ Is a Perfect Cookbook for Family Campouts |Heather Greenwood Davis |May 30, 2021 |Outside Online 

It is served atop tamarind butter sauce and topped with grilled pineapple salsa. A Magical Meal at Louie’s Backyard in the Conch Republic |Jane & Michael Stern |July 13, 2014 |DAILY BEAST 

The Bee Pollen juice looked appetizing and resembled a juice I routinely enjoy that consists of pineapple, apple, and lime. We Were Gwyneth’s GOOP Guinea Pigs |Erin Cunningham, Olivia Nuzzi |March 30, 2014 |DAILY BEAST 

I threw back a few pineapple-vodka-whatever drinks and, after a handful of songs, headed for the exits. Sundance ’14 Party Report: Anne Hathaway’s A Great Wingwoman, Kristen Stewart Cuts A Rug, and More |Marlow Stern |January 25, 2014 |DAILY BEAST 

The co-stars of Pineapple Express and Freaks and Geeks have really outdone themselves this time. Watch James Franco and Seth Rogen’s Hilarious Parody of Kanye West’s Music Video For 'Bound 2' |Marlow Stern |November 25, 2013 |DAILY BEAST 

“For 10 days prior to the Versace show, I just drank juice—carrot, ginger, pineapple—to cleanse,” she divulged. Rihanna’s New Music Video Was Banned After Only 10 Minutes; Tamara Mellon Spills on Jimmy Choo |The Fashion Beast Team |October 4, 2013 |DAILY BEAST 

The most common forms resemble the pineapple with its leafy fruit apex cut off. Philippine Mats |Hugo H. Miller 

Pare your pineapple; cut it in small pieces, and leave out the core. A Poetical Cook-Book |Maria J. Moss 

A fig-tree was in a very thriving way, as were two vines, a pineapple plant, and some slips of a shaddock-tree. A Voyage to the South Sea |William Bligh 

One was of moderate size, and the other the very smallest they could find; a perfect baby of a pineapple. Living on a Little |Caroline French Benton 

Raspberries, peaches, shredded pineapple, or other fruit can be substituted for strawberries. The New Dr. Price Cookbook |Anonymous