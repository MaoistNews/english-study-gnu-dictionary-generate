But the police nevertheless declared Stone to be “armed and dangerous,” despite getting around with a cane. Hunt for Iraq Vet After Killing Spree |M.L. Nestel |December 16, 2014 |DAILY BEAST 

Nevertheless, I saw a man die 10 feet from me in my first year. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

Nevertheless, the family of the little girl sued the company for wrongful death. The Ten Worst Uber Horror Stories |Olivia Nuzzi |November 19, 2014 |DAILY BEAST 

Abbas has been committed—quite unsuccessfully, but nevertheless committed—to nonviolence. After the Israel Synagogue Massacre: A New Intifada? |Michael Tomasky |November 19, 2014 |DAILY BEAST 

Nevertheless, Brian Rogers, a McCain aide pushed back against UANI, calling the Rio Tinto-Iran connection “a red herring.” McCain Helps a Business Partner of Iran |Ben Jacobs |November 13, 2014 |DAILY BEAST 

Nevertheless the evening and the night passed away without incident. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

But such a thing had, nevertheless, come quite glibly out of her mouth, and she knew not why. Hilda Lessways |Arnold Bennett 

Nevertheless, this world of mankind to-day seems to me to be a very sinister and dreadful world. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

This was very agreeable; but he was, nevertheless, greatly relieved when a boat came in sight sailing toward him. Davy and The Goblin |Charles E. Carryl 

Nevertheless, both our arrival that evening and our landing the next morning were very quiet and peaceful. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various