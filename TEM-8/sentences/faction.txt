Even more recently, researchers have documented QAnon accounts pushing false claims that members of antifa, a loosely organized, far-left political faction, had started wildfires in the Pacific Northwest. As QAnon grew, Facebook and Twitter missed years of warning signs about the conspiracy theory’s violent nature |Craig Timberg, Elizabeth Dwoskin |October 1, 2020 |Washington Post 

In 1998, he bolted from the faction of Keizo Obuchi, a party bigwig who later became prime minister, to support a rival Suga believed more capable. How the son of strawberry pickers became Japan’s most likely choice for next prime minister |claychandler |September 3, 2020 |Fortune 

That left the selection of prime minister in the hands of the small coterie of senior power brokers who control the party’s largest factions. How the son of strawberry pickers became Japan’s most likely choice for next prime minister |claychandler |September 3, 2020 |Fortune 

Ishiba, thought by many to be the front-runner, is popular among the party’s rank and file, though less so among senior faction leaders. The legacy Shinzo Abe, Japan’s longest-serving Prime Minister, will leave after resigning |claychandler |August 28, 2020 |Fortune 

A total of five states24 will hold their down-ballot primaries or runoffs today, in which the fight between the radical wings and more moderate factions will continue for both parties. What You Need To Know About Today’s Elections In Minnesota And Georgia |Geoffrey Skelley (geoffrey.skelley@abc.com) |August 11, 2020 |FiveThirtyEight 

He was part of an extreme, racialized white faction in the Louisiana state house that was clearly dead-set against honoring King. Steve Scalise and the Right’s Ridiculous Racial Blame Game |Michael Tomasky |January 2, 2015 |DAILY BEAST 

But it is not clear if the latest action is at the hands of that faction or another. Fierce Fighting in Grozny Raises Specter of ISIS Influence in Russia |Anna Nemtsova |December 4, 2014 |DAILY BEAST 

Jobbik, whose paramilitary militia faction was banned in 2009, won 20 percent of the national vote in April. In Hands of Hungarian Artist, Jewish Home Movies of the ’30s a Warning of Coming Holocaust |Daniel Genis |October 25, 2014 |DAILY BEAST 

This faction of the opposition is itself fractured into dozens of splinter groups. Al Qaeda Makes a Play for the U.S. Allies the War Against ISIS Depends On |Jacob Siegel |September 29, 2014 |DAILY BEAST 

Predictably, the pro-slavery faction also used the threat of hell to their favor. Americans’ Burning Obsession With Hell |William O’Connor |September 26, 2014 |DAILY BEAST 

One of these persons tried to enlist Prior in Portland's faction, but with very little success. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Ignorance and party faction, and a variety of such other unworthy components, entered largely into them. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton 

But even in those words the malevolence of faction sought and found matter for a quarrel. The History of England from the Accession of James II. |Thomas Babington Macaulay 

The fires of the Puritan faction had smouldered out; those of the Jacobite frenzy had hardly had time to rekindle. A Cursory History of Swearing |Julian Sharman 

Many of the popular faction fled to France; others took refuge among the Ardennes; some were executed. Belgium |George W. T. (George William Thomson) Omond