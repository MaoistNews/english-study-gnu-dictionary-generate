Several characteristics of Waun Mawn link it to Stonehenge, the scientists contend. Stonehenge may have had roots in a Welsh stone circle |Bruce Bower |February 12, 2021 |Science News 

The US may have to contend with sporadic outbreaks and even vaccine boosters, as immunity declines and new variants of the virus emerge. Covid-19 vaccines are great — but you still need to wear a mask for now |Umair Irfan |February 9, 2021 |Vox 

She said such a move would lead to more job cuts as the industry contends with economic losses from one of Oregon’s worst wildfire seasons. “We Have Counties in Deep Trouble”: Oregon Lawmakers Seek to Reverse Timber Tax Cuts That Cost Communities Billions |by Rob Davis, The Oregonian/OregonLive, and Tony Schick, Oregon Public Broadcasting |February 5, 2021 |ProPublica 

It’s a question Tampa Bay has to contend with as we collectively contemplate other major sports mascots with dubious legacies, like their Super Bowl rivals in Kansas City. The Buccaneers embody Tampa’s love of pirates. Is that a problem? |Jamie Goodall |February 5, 2021 |Washington Post 

When it comes to a mass-participation event, you have to contend with thousands of people traveling to and from the race, not to mention the inevitable clusters of spectators along the route. Will Major Marathons Actually Come Back This Fall? |Martin Fritz Huber |February 4, 2021 |Outside Online 

They contend that the protests are not anti-police brutality, but anti-police, period. Justice League Vigil for Slain NYPD Officers Asks Whose Life Matters |Olivia Nuzzi |December 22, 2014 |DAILY BEAST 

“Contend” is a very Cruzian word, and “I would contend” a favorite phrase. Ted Cruz, Accused of Being ‘Sidelined’ for the Midterms, Shows Off Schedule |Tim Mak |November 3, 2014 |DAILY BEAST 

The center had seemingly proven wrong people who contend that rescued eaglets can only survive in captivity. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

He has to contend with much more conservative bishops, archbishops, and cardinals appointed by his two immediate predecessors. Pope Francis Pushes the Church Another Step Further on Gays |Gene Robinson |October 16, 2014 |DAILY BEAST 

Still, many contend the original Amber Room was smuggled out of Königsberg and hidden during the final days of the war. The Biggest Art Heist of WWII is Still Unsolved | |October 15, 2014 |DAILY BEAST 

Many contend that this system produces better results than if pipes of the actual lengths of 32 or 64 feet were employed. The Recent Revolution in Organ Building |George Laing Miller 

If thou hast been wearied with running with footmen, how canst thou contend with horses? The Bible, Douay-Rheims Version |Various 

Once set, the young plant must contend, not only with the ordinary risk of transplanting, but the cut-worm is now to be dreaded. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He did not, like some hot headed men, among both the Whigs and the Tories, contend that there ought to be no regular soldiers. The History of England from the Accession of James II. |Thomas Babington Macaulay 

This was very flattering to those ladies, especially the Diplomast, considering the great odds they had to contend with. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany