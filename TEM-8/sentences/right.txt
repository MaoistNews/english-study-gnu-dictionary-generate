Many iPhone owners are seeing a strange orange dot appearing from time to time in the top right corner of their screen. iPhone’s new ‘orange dot’ feature warns you when an app is listening |Jeff |September 17, 2020 |Fortune 

Ajami says that, right now, officials can’t do much while the fires are still ongoing. California wildfires may give way to massive mudslides |Ula Chrobak |September 17, 2020 |Popular-Science 

What I have found is likely no surprise, and why I believe this is the most important conversation happening right now, and in the near future with Digital Marketing. This decade’s most important marketing question: What data rights do advertisers possess? |Kirk Williams |September 17, 2020 |Search Engine Land 

It turned out to be the right move, and we only have 15 people and they are getting better, and hopefully they’re all better. Timeline: The 124 times Trump has downplayed the coronavirus threat |Aaron Blake, JM Rieger |September 17, 2020 |Washington Post 

To choose the right name, we must first find the birthplace of our new epoch, which is the same thing as finding the deathplace of the Holocene. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

I disapprove of what you say, but I will defend to the death your right to say it. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 

Everywhere I go, ‘Hey Cartman, you must like Family Guy, right?’ Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

Charlie ridiculed my faith and culture and I died defending his right to do so. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 

Gay marriage was the hot-button fight on the left and right. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

It is grandstanding for a right rarely protected unless under immediate attack. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

What need to look to right or left when you are swallowing up free mile after mile of dizzying road? The Joyous Adventures of Aristide Pujol |William J. Locke 

Mrs. Wurzel was quite right; they had been supplied, regardless of cost, from Messrs. Rochet and Stole's well-known establishment. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

She is quite true, but not wise, and your left hand must not know what your right hand is doing. Checkmate |Joseph Sheridan Le Fanu 

In Spain he was regarded as the right arm of the ultra-clericals and a possible supporter of Carlism. The Philippine Islands |John Foreman 

The thought seemed to produce the dreaded object, for next moment a large hummock appeared right ahead. The Giant of the North |R.M. Ballantyne