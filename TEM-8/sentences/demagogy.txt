Clear away all the demagogy and scare tactics, and Obamacare is, at its core, Romneycare across state lines. A Conservative Defense of Obamacare... from an AEI Scholar |Justin Green |September 30, 2012 |DAILY BEAST 

"The country became very susceptibly to demagogy," Brzezinski said. An Evening of Bush Bashing with Scowcroft and Brzezinski |Ross Goldberg |October 20, 2008 |DAILY BEAST 

The next six years found unhappy Spain delivered up to every excess of demagogy and disorder. The War Upon Religion |Rev. Francis A. Cunningham 

But it was not enough to abolish liberty by conjuring up the spectre of demagogy. Encyclopaedia Britannica, 11th Edition, Volume 10, Slice 7 |Various 

Besides, atheism fits in very well, whatever Robespierre may have thought, with the general sentiments of the baser demagogy. The Cult of Incompetence |Emile Faguet 

The minority who have no turn for demagogy are demagogues though they do not like it, and because they are forced by necessity. The Cult of Incompetence |Emile Faguet 

Sir Edward Carson's speeches in Ulster, indeed, are the most extreme instances of demagogy we have had in recent years. The Book of This and That |Robert Lynd