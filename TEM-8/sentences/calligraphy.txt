It portrays a gnarled tree overlaid with multicolored squiggles that might recall balloons, or some sort of calligraphy. In the galleries: A heightened homage to trees and what they can teach us |Mark Jenkins |May 7, 2021 |Washington Post 

It’s universally available, it’s affordable, and it can be as simple as a note scrawled on a postcard or as complex as a work of calligraphy. Letter-writing staved off lockdown loneliness. Now it’s getting out the vote. |Tanya Basu |September 18, 2020 |MIT Technology Review 

The paper is suitable for calligraphy pens, brush and ball pens, acrylic colors, watercolor, and more. Notable notebooks for writing and drawing |PopSci Commerce Team |September 17, 2020 |Popular-Science 

So he dropped out, exploring only the few classes that interested him; he took calligraphy. What Steve Jobs Taught Me |Spencer Porter |October 11, 2011 |DAILY BEAST 

Sora has been publicly practicing her Chinese calligraphy and studying Mandarin. Porn Star Sora Aoi Is Taking China by Storm |Isaac Stone Fish |May 20, 2011 |DAILY BEAST 

Hilda took the letter with apprehension, as she recognized the down-slanting calligraphy of Sarah Gailey. Hilda Lessways |Arnold Bennett 

The two notes looked harmless enough, and the calligraphy of one was vaguely familiar. Tales Of Men And Ghosts |Edith Wharton 

It was a good hand now, though the scamp would never be a professor of calligraphy. Pippin; A Wandering Flame |Laura E. Richards 

Hence the introduction of calligraphy must be placed in 405. Encyclopaedia Britannica, 11th Edition, Volume 15, Slice 3 |Various 

In his later years Backhuysen employed his time in etching and calligraphy. Encyclopaedia Britannica, 11th Edition, Volume 3, Part 1, Slice 1 |Various