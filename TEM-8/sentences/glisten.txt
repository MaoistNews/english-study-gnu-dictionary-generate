I wanted the whole dress to glisten, so I used a lace with silver thread in it. Meet the ‘Downton Abbey’ Costume Queen |Katie Baker |January 8, 2014 |DAILY BEAST 

Her tiny, 5-foot frame and pale white prison skin seemed to glisten with anguish. Casey Anthony's Mystery Tears |Diane Dimond |May 9, 2011 |DAILY BEAST 

When it begins to glisten, add the onion and sauté until it is soft and slightly golden, about five minutes. Eco-Chic Safari |Sophie Menin |July 27, 2010 |DAILY BEAST 

See that boy's eyes glisten while you are speaking of a neighbor in a language you would not wish to have repeated. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Only once, when Sogrange incautiously displayed a gold watch, did the eyes of one or two of their number glisten. The Double Four |E. Phillips Oppenheim 

Wait until nightfall, and it will positively glisten with lamps and gleam with merriment. A Cursory History of Swearing |Julian Sharman 

Their monasteries rise in the midst of the fertile plains of Syria, their basilicas glisten in our cities beside our mosques. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 

The streets glisten clean in the sunlight, and every window is a reflector of glad promise. The Onlooker, Volume 1, Part 2 |Various