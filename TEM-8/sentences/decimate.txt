After the closure of stores decimated the in-store sampling that many beauty brands relied on, Marie Claire launched a direct mail sampling operation, expecting to gather tens of thousands of signups within the first year. With America still on lockdown, publishers lean into direct mail |Max Willens |February 1, 2021 |Digiday 

The ASF outbreak that decimated China’s pig population in 2019 resulted in national pork output hitting a 16-year low. How China Could Change the World by Taking Meat Off the Menu |Charlie Campbell/Shanghai |January 22, 2021 |Time 

That’s as scared as I’ve ever seen Thanos, and if he hadn’t said decimate my entire team to get her off of me, I think she would have done it. Everything You Need to Know Before Watching WandaVision |Eliana Dockterman |January 14, 2021 |Time 

Stocks have been on a record-shattering tear after a devastating drop-off during the pandemic’s first wave, even as the coronavirus continues to cause mass deaths, halt travel, decimate businesses and push millions into poverty. Wall Street retreats from record highs amid virus, political uncertainty |Taylor Telford |January 11, 2021 |Washington Post 

With the coronavirus pandemic still raging and decimating local businesses, Manna, a drone delivery company, gained approval in late October to launch its service in Oranmore, Ireland. With grocery store access limited during the pandemic, drone delivery offers a high-tech boost to business |Alyssa Newcomb |January 11, 2021 |Fortune 

And now, the plan is not only to decimate public-sector unions, but all unions—to deplete the money they can spend on politics. The Next Phase of the Koch Brothers’ War on Unions |Carl Deal and Tia Lessin |December 22, 2014 |DAILY BEAST 

And, from the south, chronic wasting disease is poised to decimate the elk herds. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

When we decimate the funding for hospital preparedness, we put ourselves in great peril. Ebola Panic Is Worse Than the Disease |Abby Haglage |October 9, 2014 |DAILY BEAST 

Instead we must decimate the mid-level leadership ISIS relies on. Who the U.S. Should Really Hit in ISIS |Daniel Trombly, Yasir Abbas |September 23, 2014 |DAILY BEAST 

One set of officials advocated for a campaign to decimate ISIS in both countries by striking ISIS targets across Syria. Why Obama Backed Off More ISIS Strikes: His Own Team Couldn’t Agree on a Syria Strategy |Josh Rogin, Eli Lake |August 29, 2014 |DAILY BEAST 

These are the dreaded wild dogs which decimate the game in the jungle. Life in an Indian Outpost |Gordon Casserly 

Soon the artillery of both armies opened, and a rain of cannon balls began to decimate the opposing ranks. Famous Men and Great Events of the Nineteenth Century |Charles Morris 

This was an ominous hint that he intended to decimate them, after the fashion of Field-Marshal Liposcak. The Birth of Yugoslavia, Volume 1 |Henry Baerlein 

One might easily decimate a whole population, as indeed happened in the South Sea Islands when smallpox was introduced. The Romance of Plant Life |G. F. Scott Elliot 

Our men continued to decimate the enemy so thoroughly that they had scarcely five men on deck alive or unwounded. The Philippine Islands, 1493-1898: Volume XVII, 1609-1616 |Various