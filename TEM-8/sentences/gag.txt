The movie was bursting with brilliant visual and verbal gags that paid irreverent tribute to Universal Studios horror films such as “Frankenstein” with Boris Karloff and “Dracula” with Bela Lugosi. Cloris Leachman, Oscar-winning actress who played Frau Blücher (neighhh!) in ‘Young Frankenstein,’ dies at 94 |Adam Bernstein |January 27, 2021 |Washington Post 

The gags come in hard and heavy with the kind of pop culture references you’d find in a Dreamworks 3-D animated movie, which make me wonder if the game is better suited for children. ‘Immortals: Fenyx Rising’ is a worthy adventure wavering between cringe and charm |Gene Park |November 30, 2020 |Washington Post 

These letters, which are the subject of ongoing constitutional disputes in federal courts, allow the FBI to include a gag order with demands for data—barring the company from even disclosing the existence of such a request. Coinbase received 1,914 requests in 6 months from FBI, ICE, and other agencies |Jeff |October 16, 2020 |Fortune 

It is critical for the Kenyan government to look to its own policies and increase budgetary allocation for sexual and reproductive health services so that they cushion the impact of the global gag rule. Insights Into How The US Abortion Gag Rule Affects Health Services In Kenya |LGBTQ-Editor |October 2, 2020 |No Straight News 

The initial indications are that, at least to date, publishers are underwhelmed by the economics and offended by the gag order. ‘Quite cynical’: Publishers leery about Google’s $1 billion news licensing pot |Lucinda Southern |October 1, 2020 |Digiday 

But when a serial sex predator is playing fanboy, the gag reflex kicks in. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

The following page details a tribute gag the Simpsons team inserted into the background of a scene. Here’s the Lost Judd Apatow ‘Simpsons’ Episode, Penned by Judd Apatow |Asawin Suebsaeng |January 6, 2015 |DAILY BEAST 

Neil Patrick Harris, Hedwig and the Angry Inch Neil Patrick Harris in fishnets, high heels, and glitter could be a great gag. Hedwig, Hugh & Michael Cera: 12 Powerhouse Theater Performances of 2014 |Janice Kaplan |December 31, 2014 |DAILY BEAST 

They obtained a gag order against the defendant and his lawyers restricting what they could say about the case for several months. Sentencing Looms for Barrett Brown, Advocate for “Anonymous” |Kevin M. Gallagher |December 15, 2014 |DAILY BEAST 

And bonus points for the school bus that burst into flames with the comic timing of a Simpsons gag. The Walking Dead’s ‘Self Help’: A Grim Show Displays Its Comedy Streak, and A Major Reveal |Melissa Leon |November 10, 2014 |DAILY BEAST 

Braceway had made the trip to gag Morley, to see that he didn't uncover something which, after all, Morley didn't know—and I did! The Winning Clue |James Hay, Jr. 

They may buck and gag you, as they ought to, 'bout every day, but that won't be nothin' to the welting one of us 'll261 give you. Si Klegg, Book 2 (of 6) |John McElroy 

Bumper felt a tickling sensation in his throat, and he wanted to gag, but the bill prevented him. Bumper, The White Rabbit |George Ethelbert Walsh 

The tickling went on for some time until Bumper, in spite of himself, began to gag and retch. Bumper, The White Rabbit |George Ethelbert Walsh 

"'Cause I remember that coat gag now," said Al with a far-off look. The Varmint |Owen Johnson