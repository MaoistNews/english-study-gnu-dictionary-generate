Tegna will create a number of digital properties under the Verify brand, including a dedicated website and email newsletter projected to debut mid-Q2, said Adam Ostrow, chief digital officer at Tegna. Why local broadcaster Tegna is making a big bet on its fact-checking vertical Verify |Sara Guaglione |February 12, 2021 |Digiday 

They gave me the chance to make my debut, and I haven’t looked back. Matthew Hoppe was a little-known American soccer player — until he reached the Bundesliga |Steven Goff |February 11, 2021 |Washington Post 

The rest of that interview resurfaced after “Framing Britney Spears” debuted. Britney Spears’s conservatorship is back in court — and back in the public eye |Ashley Fetters |February 11, 2021 |Washington Post 

The soapy serial, which debuted on Netflix last month, does not shy away from intimate scenes, including newlyweds consummating their marriage. Watching edgier TV with your kids during the pandemic? You’re not alone. |Bonnie Miller Rubin |February 11, 2021 |Washington Post 

We are discussing recording artist Sia’s Golden Globe-nominated directorial debut “Music,” which will be released on video-on-demand platforms on Friday, making it viewable to a global audience. The very real, very painful reasons the autistic community demanded two restraint scenes be removed from Sia’s new film ‘Music’ |Theresa Vargas |February 10, 2021 |Washington Post 

I enjoyed it, but thought it paled in comparison to their debut. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

The brother of a girl who made her debut in New Orleans society was shaking his fists in excitement. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

Forty-two years after its debut, The Godfather casts a long shadow over American cinema. The Alterna-‘Godfather’: ‘A Most Violent Year’ |Nick Schager |December 30, 2014 |DAILY BEAST 

It was creative thinking like this that helped it debut at number one on Billboard Top Heatseekers Chart. OK Go Is Helping Redefine the Music Video For the Internet Age |Lauren Schwartzberg |December 15, 2014 |DAILY BEAST 

After such a barnstorming stage debut, and while “my heart is in theater,” screen acting intrigues him. The Brit Who Stormed Broadway |Tim Teeman |December 7, 2014 |DAILY BEAST 

Then in 1823 she made her debut at the Opera in a trio skit with Mariette and Tullia. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Yet, if he were in very truth a prince—she thought of his debut in flowered waistcoat, panama hat, and enamelled boots! When Valmond Came to Pontiac, Complete |Gilbert Parker 

In passing we will note some other musical events of interest which occurred about the same time that Chopin made his debut. Frederick Chopin as a Man and Musician |Frederick Niecks 

Never was debut so successful, as Chloe's first appearance in Camden Place. The Widow's Dog |Mary Russell Mitford 

And to-night was the grand debut of a more remarkable phenomenon than any of the others. Blackwood's Edinburgh Magazine, No. CCCXLII. Vol. LV. April, 1844 |Various