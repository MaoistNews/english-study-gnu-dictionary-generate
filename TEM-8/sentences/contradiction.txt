These days, his emails have homed in on the contradictions of covid-19 health orders. Getting vaccinated is hard. It’s even harder without the internet. |Eileen Guo |February 3, 2021 |MIT Technology Review 

Just remember, Google is also an emerging technology, and a lot of what it’s doing in the algorithm stands in contradiction to the full-on adoption of AI written content. Taking your SEO content beyond the acquisition |Mordy Oberstein |February 2, 2021 |Search Engine Watch 

Pack hired two Washington firms, McGuireWoods and Caplin & Drysdale — apparently in contradiction to federal contracting regulations and guidelines, according to internal documents. Former Voice of America overseer hired two law firms to $4 million no-bid contracts. |Paul Farhi |January 25, 2021 |Washington Post 

Takeuchi doesn’t see a contradiction with the county’s withholding of outbreak data, on the one hand, and the sharing of home addresses with law enforcement, on the other. County Distributes COVID Patients’ Addresses to Police Agencies |Jesse Marx |January 21, 2021 |Voice of San Diego 

The contradiction is more apparent as, over a lifetime, one expects to shift from role to role. Miss Manners: Bossy invitation offends guests |Judith Martin, Nicholas Martin, Jacobina Martin |January 20, 2021 |Washington Post 

This may seem like a contradiction, if “conservative” means political conservatism. Supreme Court to Gay-Marriage Foes: Get Lost |Jay Michaelson |October 6, 2014 |DAILY BEAST 

On some level, Brecht meant for Mother Courage to be an ambivalent figure—he called her “a great living contradiction.” Brecht's Mercenary Mother Courage Turns 75 |Katie Baker |September 10, 2014 |DAILY BEAST 

And an inherent contradiction within the Sunni coalition could well trigger a breakup in the longer term. Iraq Preps for a Civil War Rematch |Jamie Dettmer |June 23, 2014 |DAILY BEAST 

Indeed, the very idea of a same-sex marriage seemed to most people a contradiction in terms. Opposing Gay Marriage Doesn’t Make You a Crypto-Racist |Jonathan Rauch |April 24, 2014 |DAILY BEAST 

He laughs at the unintentional contradiction of that statement. And The Escort of The Year Is… Backstage at The Sex Oscars |Scott Bixby |March 24, 2014 |DAILY BEAST 

But all of this is in contradiction to the curses of Jahveh on the serpent, and on those to whom the serpent brought wisdom. Solomon and Solomonic Literature |Moncure Daniel Conway 

This established beyond contradiction, the general equality of men. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

I found here a contradiction to the vulgar opinion, that hydrophobia is not known in Brazil. Journal of a Voyage to Brazil |Maria Graham 

On the fourth of October was put forth a vehement contradiction of the story. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Her wanting me to lend her money in contradiction to all rules established between Aunts and Nephews is a very ugly circumstance. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon