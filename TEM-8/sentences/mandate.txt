For instance, if members of Congress advocate for specific reforms like a new financial transaction tax or new mandates for regulators rather than just expressing their concerns, the issue could become more partisan. What Americans Think About The GameStop Investors |Dhrumil Mehta (dhrumil.mehta@fivethirtyeight.com) |February 12, 2021 |FiveThirtyEight 

Day has been advocating for a local mask mandate, as seen in other cities and counties. Despite Crackdown Announcement, Not Much COVID-19 Enforcement Is Happening |Jesse Marx |February 11, 2021 |Voice of San Diego 

In other posts, she mocked mask mandates to stop the spread of the coronavirus. Gina Carano is off ‘The Mandalorian’ over ‘abhorrent and unacceptable’ social media posts, Lucasfilm says |Timothy Bella |February 11, 2021 |Washington Post 

Three states — Iowa, North Dakota and Mississippi — have lifted such mandates. Masks should fit better or be doubled up to protect against coronavirus variants, CDC says |Lena H. Sun, Fenit Nirappil |February 11, 2021 |Washington Post 

This was most likely due to public health mandates that required a shift away from in-person care. One big hiccup in US efforts to distribute COVID-19 vaccines? Poor internet access. |By Tamra Burns Loeb, Et Al./The Conversation |February 10, 2021 |Popular-Science 

“This is a federal mandate that is causing some real problems for schools across the country,” Kline told a CBS affiliate in July. The Republican War on Kale |Patricia Murphy |January 7, 2015 |DAILY BEAST 

Part of the problem is the mandate of the war and the means with which the U.S. is fighting it do not match up. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

According to Schumer, Obama and his administration had misinterpreted their 2008 electoral mandate. 2016 Is No Democratic Slam Dunk |Lloyd Green |December 1, 2014 |DAILY BEAST 

And if the Little Sisters prevail, the entire contraception mandate falls. RFRA Madness: What’s Next for Anti-Democratic ‘Religious Exemptions’ |Jay Michaelson |November 16, 2014 |DAILY BEAST 

And to other parts of the Affordable Care Act, not just the so-called “contraception mandate.” RFRA Madness: What’s Next for Anti-Democratic ‘Religious Exemptions’ |Jay Michaelson |November 16, 2014 |DAILY BEAST 

My crutch emphasized this mandate, but I could not see how it was received, for every scholar's face was hidden from me by a book. The Soldier of the Valley |Nelson Lloyd 

I suppose uncle's letter must be taken as a royal mandate, and that we must leave here at once. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

The officers of the Lisbon troops talk loudly of his being obliged to do his duty, and obey the mandate of the Cortes. Journal of a Voyage to Brazil |Maria Graham 

Having thus issued his mandate, the groom came forth from the stable. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various 

No necessity to answer him; make signs that you obey the sultan's mandate; you know how they do it. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng