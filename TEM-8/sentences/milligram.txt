Other potential culprits include the loss of as much as 150 milligrams per hour of calcium through sweating, and chronic inflammation and elevated stress hormones due to overtraining, which might interfere with bone repair. How Cyclists Can Avoid Low Bone Density |mmirhashem |October 20, 2021 |Outside Online 

Increasing the amount of the enzyme from 1 milligram per gram of PET to 3 milligrams made it even more efficient — breaking down about 90 percent of the plastic. New recycling technologies could keep more plastic out of landfills |Maria Temming |April 29, 2021 |Science News For Students 

Previous experiments have involved objects with masses of hundreds of milligrams or more. A tiny gold ball is the smallest object to have its gravity measured |Emily Conover |March 10, 2021 |Science News 

“If they were ten-milligram tablets, she would have taken ten to twenty of them,” Ballard quoted Francisco. The Strange and Mysterious Death of Mrs. Jerry Lee Lewis |Richard Ben Cramer |January 11, 2014 |DAILY BEAST 

One milligram of californium-252 will spontaneously produce over 10⁹ neutrons per second. The Atomic Fingerprint |Bernard Keisch 

If after a second trial the loss is the same, or only increased by a milligram, the determination is finished. A Textbook of Assaying: For the Use of Those Connected with Mines. |Cornelius Beringer and John Jacob Beringer 

They lose slightly in weight when in use, but the loss is uniform, and averages half a milligram per month when in daily use. A Textbook of Assaying: For the Use of Those Connected with Mines. |Cornelius Beringer and John Jacob Beringer 

They are always weighed on the balance with a counterpoise, but no attempt is made to weigh them closer than to 0.5 milligram. Respiration Calorimeters for Studying the Respiratory Exchange and Energy Transformations of Man |Francis Gano Benedict 

The masses commonly used are the milligram, gram and kilogram. Physics |Willis Eugene Tower