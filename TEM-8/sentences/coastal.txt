Like many salmon, juvenile masu spend several years in freshwater systems before migrating to the sea, and the coastal rivers of Primorye are full of these pencil-length fish. The quest to snare—and save—the world’s largest owl |Jonathan Slaght |August 28, 2020 |Popular-Science 

Right now, the second and third largest fires in the state’s history are ripping through coastal forests and inland shrubland in Northern California. California and the Forest Service have a plan to prevent future catastrophic fires |Ula Chrobak |August 27, 2020 |Popular-Science 

Yet throughout most of California, Elmer wrote Monday in her biweekly Environment Report, there’s no requirement to disclose anything about sea level rise or coastal flooding during real estate transactions. Morning Report: Power Company Challenged on Fire Prevention Plans |Voice of San Diego |August 25, 2020 |Voice of San Diego 

Her December report suggested state lawmakers require some kind of coastal flooding disclosure to spread public awareness about sea level rise and help Californians make informed decisions about the risks of purchasing coastal properties. Environment Report: Real Estate Sellers Aren’t Required to Disclose Sea Level Rise Risk |MacKenzie Elmer |August 24, 2020 |Voice of San Diego 

That’s without any unseasonably large storms, which cause coastal waters to surge and increase the potential for flooding. Nobody’s Talking About the Sports Arena Flood Zone |MacKenzie Elmer |August 19, 2020 |Voice of San Diego 

Are you bi-coastal now, between Portlandia and Late Night with Seth Meyers? Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

The trend is particularly concentrated in the coastal states where women are wealthier, more educated, and more liberal. Men Will Someday Have Kids Without Women |Samantha Allen |January 3, 2015 |DAILY BEAST 

No surprise then that aside from wealthy coastal suburbs, the Democratic base has shrunk to the urban cores and college towns. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

The vertigo your coastal sophisticate might get from perusing 1791. Glenn Beck Is Now Selling Hipster Clothes. Really. |Ana Marie Cox |December 20, 2014 |DAILY BEAST 

The strategic coastal city of Mariupol is bracing for attack as pro-Russian rebels move south and say the capital isn't safe. Pro-Russian Rebels Brag Kiev Is Next |Ted Phillips |September 2, 2014 |DAILY BEAST 

A plateau is defined as a high lowland; therefore, this section is higher in elevation than the Coastal Plain region. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

His hand directed the captains reddened eyes far across the strait toward the coastal hills, palm-crowded. Cursed |George Allan England 

The bright weather of the immediate coastal region was soon exchanged for the foggy gloom of the pack. The Home of the Blizzard |Douglas Mawson 

With the exception of North America and a narrow strip of coastal waters, the entire map was tinted an unhealthy pink. Greylorn |John Keith Laumer 

On the crest of a ridge, which bore away in distinct outline, on our left, a fine panorama of coastal scenery was visible. The Home of the Blizzard |Douglas Mawson