Finally, even in the worst-case scenario, in which a child does contract Covid-19, the outcomes of the disease are less severe in younger people than among older adults. I’m an epidemiologist and a dad. Here’s why I think schools should reopen. |Benjamin P. Linas |July 9, 2020 |Vox 

To be sure, people basically gambling with money they would be devastated to lose is bad. Who gets to be reckless on Wall Street? |Emily Stewart |July 9, 2020 |Vox 

In doing so, the app aims to bring more transparency to how social networks moderate hate speech by showing those who report it what is and isn’t deemed bad enough to be removed. Pernod Ricard thinks the Facebook advertiser revolt won’t be enough to curb hate speech online, so it’s developing an app to help |Seb Joseph |July 9, 2020 |Digiday 

What investors do appear to have is conviction that earnings for the second quarter likely won’t be as bad as expected. 5 theories on why the stock market is up 42% since March—ranked by likelihood |Anne Sraders |July 8, 2020 |Fortune 

Sometimes, much as we hate to admit it, a bad race is simply a bad race. The Athlete's Guide to Boosting Iron |Alex Hutchinson |July 8, 2020 |Outside Online 

We need to recover and grow the idea that the proper answer to bad speech is more and better speech. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

I gotta say—I think this past year was pretty bad for music. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Ass-kicking, bad guy-killing Carter is just a future spinster. Marvel’s ‘Agent Carter’ Stomps on the Patriarchy |Melissa Leon |January 7, 2015 |DAILY BEAST 

They all immediately dashed out to their car to catch the bad guys. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Terrorism is bad news anywhere, but especially rough on Odessa, where the city motto seems to be “make love, not war.” Is Putin Turning to Terrorism in Ukraine? |Anna Nemtsova |January 6, 2015 |DAILY BEAST 

The "bad form" of telling a lie to the head-master is a later illustration of the same thing. Children's Ways |James Sully 

The men arrived in very bad condition, and many of them blinded with the salt water which had dashed into their eyes. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Their sin began on Holy Thursday, with so little secrecy and so bad an example, that the affair was beginning to leak out. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Conditions in the new country had gone from bad to worse, and if the season should experience another drought, the worst was come. The Homesteader |Oscar Micheaux 

If any one has lost his temper, as well as his money, he takes good care not to show it; to do so here would be indeed bad form. The Pit Town Coronet, Volume I (of 3) |Charles James Wills