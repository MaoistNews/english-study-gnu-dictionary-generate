Survivors describe scooping it into their helmets before lowering themselves into shark-infested waters. How Ice Cream Became the Ultimate American Comfort Food |Matt Siegel |August 27, 2021 |Eater 

Being infested with those microbes can slow a mussel’s growth — and reproduction, too. Common parasite may help mussels survive heat waves |Sid Perkins |June 16, 2021 |Science News For Students 

Another woman’s rental home was infested with rats and cockroaches. Wall Street isn’t to blame for the chaotic housing market |Jerusalem Demsas |June 11, 2021 |Vox 

As one expert notes, 2000 through 2011 saw high levels of pirate activity, with the Gulf of Aden area off the coast of East Africa becoming the most pirate-infested part of the world, and the most dangerous for cargo ships. Watch a Jet Suit Pilot Fly Onto a Ship to Trial the Tech for Fighting Pirates |Vanessa Bates Ramirez |May 10, 2021 |Singularity Hub 

Today, the roughly four dozen known species of Rafflesiaceae all infest vines from a single genus, Tetrastigma. DNA of Giant ‘Corpse Flower’ Parasite Surprises Biologists |Christie Wilcox |April 21, 2021 |Quanta Magazine 

If the actions of men are necessary, if men are not free, what right has society to punish the wicked who infest it? Superstition In All Ages (1732) |Jean Meslier 

The horrible staring eyes began again to infest his journey, and seemed to accompany him wherever he went. The Underworld |James C. Welsh 

The sultry nights in Chorillos are rendered doubly unpleasant by the swarms of vermin which infest the houses. Travels in Peru, on the Coast, in the Sierra, Across the Cordilleras and the Andes, into the Primeval Forests |J. J. von Tschudi 

The same holds true of the multitude of nature demons that infest field and forest and the vicinity of streams and gorges. Elements of Folk Psychology |Wilhelm Wundt 

The Bark lice or Scale-insects should also be collected in connection with the leaves or twigs which they infest. Directions for Collecting and Preserving Insects |C. V. Riley