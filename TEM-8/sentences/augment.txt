Building on the core Xbox One controller, the Xbox Elite Series 2 features a host of augments and upgrades, including customizable buttons and swappable D-pads, to a wireless charging dock. Best Xbox One controllers: Gamepads for every player |Brittany Vincent |September 24, 2021 |Popular-Science 

But Sex Box, with its ridiculous guinea pigs screwing conceit, will only augment our cultural hang-ups about sex. 'Sex Box,' a Reality TV Show Where Couples Have Sex in a Box and Discuss It, Is Coming to America |Marlow Stern |August 21, 2014 |DAILY BEAST 

These are successful Americanizations of the culturally unique ancestors, and they augment what American cinema prizes. What Asian Film Remakes Like ‘Oldboy’ Get Wrong |Jimmy So |November 26, 2013 |DAILY BEAST 

He seems to have done an acceptable job in that posting, so why not just continue and augment the relationship? Six Nominees to Succeed Hillary Clinton as Secretary of State |Michael Tomasky |November 29, 2012 |DAILY BEAST 

SmartGlass will augment your gaming experience in a couple of ways. What’s So Smart About SmartGlass? 15 Questions |Alex Klein |June 5, 2012 |DAILY BEAST 

At dinner, she instructed the waiter to augment the french fries her husband had ordered with a side of spinach. On Being the Other Woman in an Affair |Elizabeth Kaye |February 14, 2012 |DAILY BEAST 

Le nombre des Marchaux fut limit et non augment sous la Restoration. Notes and Queries, Number 177, March 19, 1853 |Various 

The Secretary's remedy is to set them to raise provisions themselves, and thus augment the supply, while they diminish the demand. Select Speeches of Daniel Webster |Daniel Webster 

But this one affection only served to augment the mischief that he wrought. Emily Bront |A. Mary F. (Agnes Mary Frances) Robinson 

Evacuantia, L. Medicines which augment the secretions or excretions. Cooley's Cyclopdia of Practical Receipts and Collateral Information in the Arts, Manufactures, Professions, and Trades..., Sixth Edition, Volume I |Arnold Cooley 

The duty of a being is to persevere in its being and even to augment the characteristics which specialize it. The Natural Philosophy of Love |Remy de Gourmont