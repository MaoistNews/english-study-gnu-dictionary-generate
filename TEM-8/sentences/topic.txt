Shaw and McCarthy both agree that these are tricky topics, especially for people whose own parents never talked to them about these issues. A feminine wash for teens? Angry parents and gynecologists are on a social media crusade. |Abigail Higgins |February 12, 2021 |Washington Post 

You can check Google Trends to find out which topics are trending in your niche and then keep track of traffic. A comprehensive guide on using Google Trends for keyword research |Aayush Gupta |February 12, 2021 |Search Engine Watch 

You might be hesitant to ask them about it because it’s a sensitive topic. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

Nearly two years ago, other city leaders asked the San Diego Housing Commission to study that very topic, but that didn’t happen. Vacancy Tax Study Is Giving City Officials Déjà Vu |Lisa Halverstadt and Andrew Keatts |February 10, 2021 |Voice of San Diego 

As the Times is looking to grow its core portfolio of newsletters, topics covered in The Morning can be spun out into standalone newsletters that “don’t have to last forever,” Pasick said. The New York Times aims to convert newsletter readers into paid subscribers as The Morning newsletter tops 1 billion opens |Sara Guaglione |February 10, 2021 |Digiday 

For many years afterward it was a never-ending topic of conversation, and is more or less talked of even to this day. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

One topic that comes up among the members, she says, is dealing with loss years later. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

The other Twitter topic you are well known for is the topic of Salon. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

A few minor notes, born of reflection: Traditionally, the best columns are dominated by politics—its most popular topic. The Best Columns of 2014 |John Avlon, Errol Louis |December 31, 2014 |DAILY BEAST 

Of course there was no official way to have these interviews given the sensitivity of the topic. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

Naturally the conversation fell on the all-absorbing topic of the day and the object of his mission. The Philippine Islands |John Foreman 

Cecilia says they were all talking of it at Maundrell Abbey, where of course it is a peculiarly interesting topic. Ancestors |Gertrude Atherton 

She chatted freely, discoursed on almost every topic, and during it all he saw what a wonderfully courageous woman she was. The Homesteader |Oscar Micheaux 

In continuation of this topic, may I inquire when and where the two following bishops, deprived in 1690, died? Notes and Queries, Number 177, March 19, 1853 |Various 

Surely the topic is capable of being handled in a sufficiently exciting manner! Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various