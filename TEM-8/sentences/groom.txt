Instead, for the select few in attendance, it was almost like celebrating a wedding without the bride and groom. What You Didn’t See on TV at the Tokyo Olympics Opening Ceremony |Sean Gregory, Alice Park and Aria Chen/Tokyo |July 23, 2021 |Time 

As a medical student in Richmond, Eleanor Love showed up to as many wedding venues as possible, even when she didn’t know the bride and groom. For years, this doctor has showed up after strangers’ weddings and — with permission — brought flowers back to her patients |Kellie B. Gormly |July 9, 2021 |Washington Post 

The bad news for guests—a group not mutually exclusive from the brides and grooms, especially those of a certain age who find themselves on the wedding circuit—is there may not be, at least not for a while. 2021 Could Be the Biggest Wedding Year Ever. But Are Guests Ready to Gather? |Eliana Dockterman |May 29, 2021 |Time 

Many brides and grooms have sunk a significant amount of money into rescheduling their events. 2021 Could Be the Biggest Wedding Year Ever. But Are Guests Ready to Gather? |Eliana Dockterman |May 29, 2021 |Time 

Brides and grooms have been forced to become amateur public-health prognosticators. 2021 Could Be the Biggest Wedding Year Ever. But Are Guests Ready to Gather? |Eliana Dockterman |May 29, 2021 |Time 

Women threw rice on peshmerga fighters, a tradition practiced at Syrian weddings when neighbors welcome the bride and groom. Remembering Kobani Before The Siege |Mustafa Abdi, Movements.Org, Advancing Human Rights |November 8, 2014 |DAILY BEAST 

A couple on Merwedeplein got married on this day, and a friend captured the bride and groom leaving their apartment. Anne Frank’s Amsterdam |Russell Shorto |October 12, 2013 |DAILY BEAST 

Manhattan was the patient groom in my unspoken arranged marriage, the implicit goal of any tri-state suburban childhood. I Can’t Shake Hawaii: An Ode to Returning to Places You’ve Been Before |Debra A. Klein |October 7, 2013 |DAILY BEAST 

The charges against the groom as well as against a bridesmaid were dropped. Anthony Weiner Has Some Competition |Michael Daly |August 4, 2013 |DAILY BEAST 

And Republicans have just founded a new organization to groom minorities in the party. Homophobia in GOP Makes It Hard for Party to Compete Nationally |Peter Beinart |February 19, 2013 |DAILY BEAST 

A groom is a chap, that a gentleman keeps to clean his 'osses, and be blown up, when things go wrong. The Book of Anecdotes and Budget of Fun; |Various 

Throwing up the window, he saw his young son attempting to mount the groom's pony: the latter objecting. Elster's Folly |Mrs. Henry Wood 

Lady Hartledon driving, the boy-groom sitting beside her, and Eddie's short legs striding the pony. Elster's Folly |Mrs. Henry Wood 

But the groom who took care of them sprang instantly after them, and kept swimming beside them, guiding and cheering them. The Nursery, January 1873, Vol. XIII. |Various 

A former groom; born about 1767; short, thickset, wife-led, one-eyed. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe