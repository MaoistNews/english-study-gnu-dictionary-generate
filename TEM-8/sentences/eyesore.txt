If you thought the rare division winner with a losing record was an eyesore, wait until you get that in addition to frequent 8-9 wild-card teams. The NFL chose quantity over quality, and its product suffered |Jerry Brewer |January 18, 2022 |Washington Post 

This often leads to cable clutter that can be both an eyesore and a nuisance to your workspace. Save on this 3-in-1 wireless Charging Pad this week only |Stack Commerce |January 9, 2022 |Popular-Science 

In Port Isabel, Hockema said he fears that the Texas LNG terminal will be approved and built, and then abandoned for lack of business, leaving the town with a permanent, hulking eyesore. Engineers raise alarms over the risk of major explosions at LNG plants |Will Englund |June 3, 2021 |Washington Post 

Even if Lynn and his staff didn’t call the run, it remains an eyesore in an overall picture that also includes a 3-8 record. What to know from NFL Week 12: Tyreek Hill and Derrick Henry took over as the coronavirus loomed |Adam Kilgore |November 30, 2020 |Washington Post 

And, not insignificantly, huge arrays of solar panels can be an eyesore. Can farmland fix solar power’s real estate problem? |Tim McDonnell |October 8, 2020 |Quartz 

In 1991, Detroit Mayor Colman Young, who thought the area was an eyesore, had many of the installations demolished. Who Is Burning Down Detroit’s World-Famous Street Art? |James Fassinger |March 14, 2014 |DAILY BEAST 

Republicans are also moving on immigration, which leaves sequester as the eyesore of the evening. Budget Battles Ahead, Obama Prepares High-Stakes State of the Union |Eleanor Clift |February 11, 2013 |DAILY BEAST 

It began building an important project on an area that was an eyesore—an ugly multistory parking lot—then stumbled into this mess. Response to Rashid Khalidi |Gil Troy |April 27, 2012 |DAILY BEAST 

Meanwhile, the lifeless shell of the Concordia is a rusting eyesore on the rocks off the coast of Giglio. Costa Concordia Inquiry Begins: Transcripts and Reports From the Scene |Barbie Latza Nadeau |March 3, 2012 |DAILY BEAST 

In the limelight, every glitch and wart becomes an eyesore for an international audience. Rio Buildings’ Collapse New Sign of World Cup Headache for Brazil |Mac Margolis |January 28, 2012 |DAILY BEAST 

The Commissioners, from the first moment of their institution, had been an eyesore to the people of Boston. The Loyalists of America and Their Times, Vol. 1 of 2 |Egerton Ryerson 

If this is not done, they become that greatest eyesore, a degenerated ornament. The Library of Work and Play: Housekeeping |Elizabeth Hale Gilman 

At last she questioned Knight, and complained that the bristly barrier was an eyesore. The Second Latchkey |Charles Norris Williamson and Alice Muriel Williamson 

And as for the son and heir, he shall be an eyesore to no young revellers, for he shall be drawn in cloth-of-gold breeches. A Select Collection of Old English Plays, Volume 10 (of 15) |Various 

Two other cuts—mere rabblement and eyesore—leave on the mind a feeling of disgust almost without interest and without shame. George Cruikshank |W. H. Chesson