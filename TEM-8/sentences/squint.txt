So, if you want step-by-step directions from a maps app, or real-time health monitoring, you can actually see it without squinting. Rolling phones, smart glasses, and other cool new tech from CES 2021 |Stan Horaczek |January 12, 2021 |Popular-Science 

Roethlisberger flailed and looked hopelessly skyward, like a squinting tourist in New York. Browns shake off their history, secure first playoff win since 1995 |Adam Kilgore, Des Bieler |January 11, 2021 |Washington Post 

It was the way they bathed the field, the stands and made him squint. Spencer Kieboom built his life around baseball. Then he quietly walked away. |Jesse Dougherty |November 30, 2020 |Washington Post 

“There it is,” said Chris, squinting and pointing through cigarette smoke. My Friend, the Murderer |Eugene Robinson |November 24, 2020 |Ozy 

I tensed up and kind of squinted my eyes a little bit like, Ooooh, this is going to hurt. "Cougar Guy" Tells the Story Behind His Viral Video |Luke Whelan |October 16, 2020 |Outside Online 

You have to squint very hard to make the novel begin to line up with reality. American Dreams: How Bush Shaped Our Reading of Roth’s ‘The Plot Against America’ |Nathaniel Rich |November 23, 2014 |DAILY BEAST 

No squint-eyed carny could ever guess it; it's a shock to find out he's 20 years older than you always thought. The Stacks: Harold Conrad Was Many Things, But He Was Never, Ever Dull |Mark Jacobson |March 8, 2014 |DAILY BEAST 

Yekaterina Samutsevich tried not to squint in the bright light of the studio lamps. Pussy Riot’s Yekaterina Samutsevich Speaks Out |Anna Nemtsova |November 30, 2012 |DAILY BEAST 

If you squint, you can see the Home Depot behind it gleaming bright and orange in the clear Texas air. Tourist Attractions of West Texas |David Frum |November 1, 2012 |DAILY BEAST 

Bonnebault was squint-eyed and his physical appearance did not belie his depravity. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

To the measured gestures of the little preachers were corresponding words learned by heart and false enough to make one squint. The Nabob |Alphonse Daudet 

"Now you jist cut out yer comedy until I can squint 'em over," said Fisheye impatiently. David Lannarck, Midget |George S. Harney 

"Port tack and headed acrost us," snarled the master, after a sniff at the air and a squint at the sluggish ripple. Blow The Man Down |Holman Day 

"I ain't making any rash promises," stated Captain Downs, walking to the rail and taking a squint at the top-hamper. Blow The Man Down |Holman Day