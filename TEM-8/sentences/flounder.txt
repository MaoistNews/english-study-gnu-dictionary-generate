As the continent is constituted currently, you cannot have a successful Pan-Africanism economically, socially, and culturally, while its biggest nation flounders. At 60, Nigeria Is Still A Country Of The Future |cmurray |October 1, 2020 |Essence.com 

It’s unlikely the HEROES Act will get passed as is, but you know, a floundering industry can dream. What the New Iteration of the HEROES Act Would Mean for Restaurants |Jaya Saxena |September 29, 2020 |Eater 

As Magic Leap floundered, it began to re-examine its options. Magic Leap tried to create an alternate reality. Its founder was already in one |Verne Kopytoff |September 26, 2020 |Fortune 

Starting several decades ago, four psychologists decided to examine how individuals flourish or flounder over the long run. ‘The Origins of You’ explores how kids develop into their adult selves |Bruce Bower |September 16, 2020 |Science News 

A decade ago, city leaders floated a brand new City Hall complex but the idea floundered and the city, instead, began renegotiating its leases. How the City Came to Lease a Lemon |Lisa Halverstadt and Jesse Marx |August 10, 2020 |Voice of San Diego 

So far, proposals of “Senate flounder,” “House blowfish,” and “Hope and Change smelt” have met with little public acceptance. Up to a Point: P.J. O’Rourke on Valentine’s Day and Oral Hygiene |P. J. O’Rourke |February 14, 2014 |DAILY BEAST 

Instead, Bayou, Israel's hedge-fund group, continued to flounder and the deception only grew. This Week’s Hot Reads: July 2, 2012 |Mythili Rao |July 2, 2012 |DAILY BEAST 

Why did Obama's White House flounder in its initial response to the economic crisis? Blame the Weak Recovery on Larry Summers? |Noah Kristula-Green |February 28, 2012 |DAILY BEAST 

China may flounder on the soccer field, but the country is in the grip of a mad World Cup fever. China Goes Mad for Soccer |Dan Levin |June 26, 2010 |DAILY BEAST 

It has a bathing beach where the gals show what they've got and fat men flounder and cavort far beyond their capacities. David Lannarck, Midget |George S. Harney 

Men crawled over one another, then dropped to the first open spot, to flounder there a moment, then roar in snoring sleep. The White Desert |Courtney Ryley Cooper 

Those who followed were compelled to flounder on the best way they could. The Battle of New Orleans |Zachary F. Smith 

And they can go where horses couldn't do anything but flounder and probably cut themselves with their own feet. The Tale of Pony Twinkleheels |Arthur Scott Bailey 

She was most aptly named; indeed, I think the Flounder would have been a still more appropriate designation. The Sportswoman's Library, v. 2 |Various