He was charming, diffident but above all very friendly, with no airs or graces. How John Lennon Rediscovered His Music in Bermuda |The Telegraph |November 3, 2013 |DAILY BEAST 

Every study ever performed has shown that the fit and lean outlive the dumpy and diffident every time. Cool It on the CrossFit: What’s Rhabdomyolysis? |Kent Sepkowitz |October 11, 2013 |DAILY BEAST 

“I have a bit of a reputation as a grim reaper,” she says, with her typically diffident smile. Tacita Dean’s ‘Five Americans’ Captures a Quiet Brilliance |Blake Gopnik |May 7, 2012 |DAILY BEAST 

Obama seemed equally diffident in his East Room news conference last week. The Decider vs. The Agonizer |Howard Kurtz |November 9, 2010 |DAILY BEAST 

And, on a global level, the usually diffident IMF is proposing that banks be subject to special punitive taxes. Wall Street Will Reform Washington |Jeffrey E. Garten |April 24, 2010 |DAILY BEAST 

And, incidentally, to encourage retiring and diffident lady interviewers. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

He seemed diffident, but it was evident that he did not wish her to go, and once more she felt that he aroused her curiosity. The Gold Trail |Harold Bindloss 

He reached out and touched my hand––a fleeting, diffident touch––and gently answered, “Ay, lad; your feet will stray.” The Cruise of the Shining Light |Norman Duncan 

He pushed the diffident greenness back, and went whistling rudely across the lands. The Devourers |Annie Vivanti Chartres 

If the tyrannous day of our fathers had but possessed the means of these our more diffident times! Ceres' Runaway |Alice Meynell