With three regular season games remaining, including Saturday’s trip to DePaul, the hope is to build momentum going into the Big East Tournament. Jamorko Pickett and Jahvon Blair have been through just about everything at Georgetown |Kareem Copeland |February 26, 2021 |Washington Post 

Saturday will be Marsh’s last game as senior associate athletic director for facilities and events. After decades together, Jay Marsh is saying good-bye, and George Mason is saying thanks |John Feinstein |February 26, 2021 |Washington Post 

In addition to Lee, other 2021 Olympic contenders competing Saturday include fellow team 2019 world gold medalists Riley McCusker and Jade Carey, who has essentially clinched the addition spot as an event specialist. For competition-starved U.S. gymnasts, Winter Cup will measure Olympic readiness |Liz Clarke |February 26, 2021 |Washington Post 

Even NBC’s “Saturday Night Live” devoted its cold open on Saturday to mocking Cuomo’s bullying behavior. Andrew Cuomo, once touted as the ‘gold standard,’ finds his brand tarnished by multiple crises |Michael Scherer, Josh Dawsey |February 26, 2021 |Washington Post 

The Health Department said Thursday afternoon that the problem was fixed and announced that an extra 3,500 appointments would be available to newly eligible residents on Saturday, on top of 4,350 slots available Friday. D.C. vaccine registrations freeze up, then fill up on first day for residents with underlying conditions |Antonio Olivo, Lola Fadulu, Michael Brice-Saddler |February 25, 2021 |Washington Post 

SATURDAY, MARCH 12TH Curious what Disneyland is like when an earthquake hits? Shocking Videos and Photos From the 2011 Japan Earthquake and Tsunami |The Daily Beast |March 11, 2011 |DAILY BEAST 

(9 p.m.) SATURDAY DECEMBER 20 There are metaphorical balls and chains, and then there are real ones. 'Momma's Boys' and Other TV Highlights |Nicole Ankowski |December 14, 2008 |DAILY BEAST