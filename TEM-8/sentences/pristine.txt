To weed out possible contamination, Candela and his colleagues sorted out the old, obviously degraded ancient DNA from the more pristine modern sequences. Oldest DNA from poop contains a Neanderthal’s microbiome |Kiona N. Smith |February 8, 2021 |Ars Technica 

The PDK monomers can then be used to make pristine new plastic, again and again. Chemists are reimagining recycling to keep plastics out of landfills |Maria Temming |January 27, 2021 |Science News 

Their ball movement was pristine, and perhaps the most impressive part of Michigan’s performance was that star freshman center Hunter Dickinson hardly factored into his team’s success. Maryland can’t keep up with No. 7 Michigan and never leads during an 87-63 loss |Emily Giambalvo |January 20, 2021 |Washington Post 

It’s packed with pristine lakes, rivers, and views of the southern Rocky Mountains. Your New Adventure Travel Bucket List |The Editors |December 21, 2020 |Outside Online 

Sestito speculated that perhaps pockets of pristine gas managed to dodge all the metals expelled from supernovas for eons, then collapsed to form stars that looked deceptively old. The New History of the Milky Way |Charlie Wood |December 15, 2020 |Quanta Magazine 

The grand prize is a pristine white Fiat Panda 4X4 – with full options. Pope Francis Raffles Off His Swag to Help the Poor |Barbie Latza Nadeau |November 18, 2014 |DAILY BEAST 

Today the Stanley is in pristine shape after another series of renovations were completed in 2013. How to Save Silent Movies: Inside New Jersey’s Cinema Paradiso |Rich Goldstein |October 2, 2014 |DAILY BEAST 

There are still places in the sea as pristine as I knew as a child. ‘Mission Blue’ Warning: The Ocean Is Not Too Big to Fail |Sylvia A. Earle |August 15, 2014 |DAILY BEAST 

There are always examples of degradation, but there are very few examples of ecosystems left that are that pristine. Republicans: Obama’s Ocean Protection Plan Evidence of ‘Imperial Presidency’ |Abigail Golden |June 23, 2014 |DAILY BEAST 

Beauty is often pristine, Harding says, while the word “gorgeous” contains more the idea of extremes and can include decay. Hello, ‘Gorgeous’: Grit and Glamour In San Francisco |Emily Wilson |June 20, 2014 |DAILY BEAST 

We have native hearts and virtues, just as other nations; which in their pristine purity are noble, potent, and worthy of example. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

Medicine and religion have been closely associated from the most pristine time. The Necessity of Atheism |Dr. D.M. Brooks 

Then agriculture will be "restored to right uses" and held in its pristine honor; and the earth will yield its fruits abundantly. Beyond |Henry Seward Hubbard 

It was of course the shadow of the Midas statue, which the boys had never permitted to be restored to its pristine state. Magnum Bonum |Charlotte M. Yonge 

But it is far from being the case that all tribes with this pristine organisation possess identical ceremonies and ideas. The Secret of the Totem |Andrew Lang