She wanted to present the survivors’ stories together to lighten the individual burden of illustrating the human toll of sexual violence and the resilience of survivors. How We Report on Pain, Death and Trauma Without Losing Our Humanity |by Karim Doumar |August 26, 2021 |ProPublica 

Perhaps we can lighten the burden by acknowledging what parents around us are experiencing and maybe even find ways to support them where we can. The shifting sands of SERPs; Tuesday’s daily brief |George Nguyen |August 24, 2021 |Search Engine Land 

Over 200 million people use Snapchat Lenses every day, some of them to lighten their skin tone. How digital beauty filters perpetuate colorism |Tate Ryan-Mosley |August 15, 2021 |MIT Technology Review 

In the winter, the club even offers a freight-hauling service so you can lighten your load on the ski in. Add These 5 Backcountry Lodges to Your Adventure Bucket List |elessard |August 12, 2021 |Outside Online 

In her time as a model, Joleen Mitton has experienced firsthand photographers’ desire to lighten her skin tone, as well as offensive comments made in her direction. Model Revolutionaries Rock Fashion |Sean Culligan |August 5, 2021 |Ozy 

Doane tweeted, “the film does make fun of Wannabe Berean unimaginative Christians who need to lighten up.” Kirk Cameron Saves Christmas from Abominable Killjoys (Other Christians) |Brandy Zadrozny |November 14, 2014 |DAILY BEAST 

Jazz lovers who had taken exception to the previous bashing were told to lighten up, and stop complaining. What’s With This Uncool Surge in Jazz Bashing? |Ted Gioia |November 2, 2014 |DAILY BEAST 

As mourning time progressed and the emotional strain began to subside, the black hues began to lighten. The Best-Dressed Way to Say Goodbye |Justin Jones |October 21, 2014 |DAILY BEAST 

“That sound is a requirement for renting a car in Costa Rica,” Mugianis jokes to lighten the mood. Hallucinating Away a Heroin Addiction |Abby Haglage |May 4, 2014 |DAILY BEAST 

Of course, I am aware that this is a simply a video game and critics would say I should lighten up. Most Young Gamers Unfit For Call of Duty |Don Gomez |October 8, 2013 |DAILY BEAST 

Thou art the help of Christians; lighten our tribulations, and help us with motherly intercession at the throne of thy divine Son. Mary, Help of Christians |Various 

Obviously, the very least they could do, was to try by all means in their power, to lighten the burden they had laid upon her. The Daughters of Danaus |Mona Caird 

Pretty soon it darkened up, and begun to thunder and lighten; so the birds was right about it. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 

Elvedon was rather like that, though the present tenants have managed to lighten it a good deal. Robin Redbreast |Mary Louisa Molesworth 

If the rollways could be definitely cleared once, the work would lighten all along the line. Blazed Trail Stories |Stewart Edward White