That’s the reality for Will Laven, an operations executive for Publicis Media, who has a stammer that has become accentuated as a direct result of the enforced remote working thrust on most industries in the last nine months. Remote working spotlights neurodiversity challenges |Jessica Davies |December 21, 2020 |Digiday 

Before the coronavirus pandemic hit, Laven attended regular social events and in-person meetings which provided frequent opportunities to speak and helped provide the necessary practice for his stammer. Remote working spotlights neurodiversity challenges |Jessica Davies |December 21, 2020 |Digiday 

That all warms up my stammer ready for when I have a meeting or a catch up with my team. Remote working spotlights neurodiversity challenges |Jessica Davies |December 21, 2020 |Digiday 

All she could stammer, however, was, “It would be an honor.” Iran Hikers' Two-Year Ordeal |Ramin Setoodeh |September 14, 2011 |DAILY BEAST 

As an adult, I have heard people affecting a stammer or a stutter. How I Found My Voice |Carly Simon |June 7, 2009 |DAILY BEAST 

This was the unhappy and astonishing birth of my stammer or at least my first gripping self-conscious awareness of it. How I Found My Voice |Carly Simon |June 7, 2009 |DAILY BEAST 

But I also had an index in the back of my diary that explained that famul meant stutter of stammer. How I Found My Voice |Carly Simon |June 7, 2009 |DAILY BEAST 

Malcolm was hardly able to stammer his acceptance of the appointment thus offered, but the General had no time for useless talk. The Red Year |Louis Tracy 

He becomes quiet and less boisterous only to stammer out some idle talk and some nonsense. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

There was nothing in the question to make me blush and stammer, yet I did both. The Rose of Old St. Louis |Mary Dillon 

We must pass over Peter as usual, or will you try again once more—I will not say to read, but to stammer through a sentence. Heidi |Johanna Spyri 

He began to stammer out something like gentleness, and something like reproof. The Mirror of Literature, Amusement, and Instruction, |Various