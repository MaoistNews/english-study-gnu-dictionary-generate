In the weeks leading up to the premiere of Inventing Anna, a nine-part series about the ersatz heiress Anna Delvey, Netflix’s marketing team posted a giant receipt on one of the perimeter walls of the Gansevoort Hotel. Inventing Anna’s lazy girlboss revenge fantasy |Alex Abad-Santos |February 28, 2022 |Vox 

The 59-year-old heiress is on trial for grooming underage girls for Epstein, her former boyfriend, from 1994 to 2004, and of abusing them herself. ‘I Felt Frozen’: Accuser Recounts Weekend From Hell With Ghislaine Maxwell and Epstein |Kate Briquelet |December 10, 2021 |The Daily Beast 

The tax records of both Kelley, the tobacco billionaire, and Weber, the soup heiress, show losses for at least 16 straight years, with their horse racing entities never once turning a profit. When You’re a Billionaire, Your Hobbies Can Slash Your Tax Bill |by Paul Kiel, Jesse Eisinger and Jeff Ernsthausen |December 8, 2021 |ProPublica 

She was tarred with accusations of being an entitled heiress, a child of privilege, the product of too much leniency. Drawing a line from Cold War brainwashing to the misinformation age |Dina Temple-Raston |October 8, 2021 |Washington Post 

The hotel heiress was photographed back in 2005 at her younger sister Nicky’s fashion show in Las Vegas. Artist Reveals Paris Hilton’s Infamous ‘Stop Being Poor’ Tank Was Fake |Cheyenne Roundtree |May 6, 2021 |The Daily Beast 

To get to watch (his Heiress co-star) Jessica Chastain was a big inspiration. Dan Stevens Blows Up ‘Downton’: From Chubby-Cheeked Aristo to Lean, Mean American Psycho |Tim Teeman |September 19, 2014 |DAILY BEAST 

DS: I was on stage doing The Heiress on Broadway, and that was a great way to turn 30. Dan Stevens Blows Up ‘Downton’: From Chubby-Cheeked Aristo to Lean, Mean American Psycho |Tim Teeman |September 19, 2014 |DAILY BEAST 

Miss Berki said that the heiress later e-mailed to apologise for any misunderstanding. Masseuse Claims Assault by Russell Brand |Tom Sykes |August 2, 2014 |DAILY BEAST 

After turning down Playboy, the acting heiress poses for her first cover shoot, which was captured by photographer Tony Duran. Dylan Penn, the Stunning Daughter of Robin Wright and Sean Penn, Bares All |Marlow Stern |April 9, 2014 |DAILY BEAST 

Rivals tagged her an apparatchik, an heiress, handpicked by a mayor/mentor said to nickname her “ma petite Anne,” my little Anne. French Vote Thrusts Two Women Into the Spotlight |Tracy McNicoll |March 31, 2014 |DAILY BEAST 

Old Mrs. Wurzel and the buxom but not too well-favoured heiress of the house of Grains were at the head of the table. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

It had come into the family through the marriage of a former earl with the heiress of the great Chudleigh family. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Mrs. Barford, as a country heiress, had received a boarding-school education, and was very superior to Letty in every respect. The World Before Them |Susanna Moodie 

This is the time when he was in correspondence with Modeste Mignon and wished to espouse that rich heiress. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Janet Marward, heiress and titular baroness of Skryne in Meath, a manor worth some 200l. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell