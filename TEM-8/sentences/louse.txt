The hog louse lives primarily on swine, where the insect feeds on blood, and is one of the largest sucking lice. A beautiful oak leaf portrait won the 2021 Nikon Small World photography contest |Erin Garcia de Jesús |September 13, 2021 |Science News 

The tubes running along the louse are respiratory tubes, called tracheae, carrying oxygen to tissues. A beautiful oak leaf portrait won the 2021 Nikon Small World photography contest |Erin Garcia de Jesús |September 13, 2021 |Science News 

All louse species, he notes, even those that live on humans, need to stick with their host animals to survive. One tiny sea parasite survives 200 times atmospheric pressure |Shi En Kim |September 25, 2020 |Science News For Students 

The seal louse is the only one of them able to survive in the ocean, says Leonardi. One tiny sea parasite survives 200 times atmospheric pressure |Shi En Kim |September 25, 2020 |Science News For Students 

Joshua Benoit isn’t surprised that the first insect found to survive such high pressures is the seal louse. One tiny sea parasite survives 200 times atmospheric pressure |Shi En Kim |September 25, 2020 |Science News For Students 

Only two soil types in the world are inhabitable for this root louse: one is sand, and the other is slate. Germany’s Wine Revolution Is Just Getting Started |Jordan Salcito |April 26, 2014 |DAILY BEAST 

His spineless duplicity confirms that the good guy is actually pretty much a louse. Bravo’s ‘Online Dating Rituals’ Reveals American Males Are Creepy and Want Sex |Emily Shire |March 10, 2014 |DAILY BEAST 

Next in line is the body louse—the only dangerous member of the troika. Ding Dong, You Have Lice |Kent Sepkowitz |October 31, 2013 |DAILY BEAST 

This not only makes you look like a louse, it makes you look like a helpless, bed-wetting man-child. Arnold's Divorce Scandal: 7 Basic Tips for Horny Politicians |Michelle Cottle |May 18, 2011 |DAILY BEAST 

When Silajdzic raised this, Milosevic said, “I am not a louse,” and yielded immediately. Richard Holbrooke on the Dayton Peace Accords |Richard Holbrooke |December 15, 2010 |DAILY BEAST 

While a counsellor was pleading at the Irish bar, a louse unluckily peeped from under his wig. The Book of Anecdotes and Budget of Fun; |Various 

The itch-mite (Acarus scabiei) and the louse (Pediculus capitis, corporis, vel pubis) are the more common members of the group. A Manual of Clinical Diagnosis |James Campbell Todd 

The good influence of the operations of the Trinity louse might be shown by many interesting instances. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

The messengers came to the house of Xmucane, who, filled with alarm, dispatched a louse to carry the summons to her grandsons. The Works of Hubert Howe Bancroft, Volume 5 |Hubert Howe Bancroft 

Brisk as a body-louse she trips,Clean as a penny drest; Sweet as a rose her breath and lips,Round as the globe her breast. The Book of Humorous Verse |Various