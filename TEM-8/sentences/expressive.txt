From Great Moments with Mr Lincoln, to incredibly expressive characters like the Na’Vi Shaman anchored inside a dark ride, to characters that hold up in bright, well lit spaces. Disney Imagineering’s Project Kiwi is a free-walking robot that will make you believe in Groot |Matthew Panzarino |April 23, 2021 |TechCrunch 

As Alina, actress Jessie Mei Li’s conviction anchors Shadow and Bone, and her expressive gentleness imparts soul. Netflix’s ‘Shadow and Bone’ Is a Chaotic, Confounding, Totally Engrossing Ride |Laura Bradley |April 23, 2021 |The Daily Beast 

Shan is not an expressive kid, not one to talk about his feelings, so his silence is how Anjy Cramer knows he is feeling upset. Kids are returning to classrooms. But what will happen to those who stay at home? |Hannah Natanson, Donna St. George |April 11, 2021 |Washington Post 

If the government aims to protect particular ideas, then it’s aiming directly at the expressive autonomy of American citizens—and companies. A Surprising Opinion From Justice Thomas May Signal an Ominous Shift on Free Speech |David French |April 9, 2021 |Time 

Young people have grown up in a time when politics is more “expressive and conversational when compared to previous generations,” says Feezell. How ‘Cancel Culture’ Became An Issue For Young Republicans |Meredith Conroy |March 22, 2021 |FiveThirtyEight 

The instrumental view of culture has it wrong, she argues, and should be replaced with what she calls an “expressive view.” The Insane Swedish Plan to Rate Games for Sexism |Nick Gillespie |November 20, 2014 |DAILY BEAST 

Robin Williams, as I knew him, was warm, gentle, expressive, nurturing, and brilliant. Mara Wilson Remembers Robin Williams: We're All His Goddamn Kids |Mara Wilson |August 18, 2014 |DAILY BEAST 

With me, finding out how expressive people could be in music really saved me. La Roux Discusses New Album ‘Trouble in Paradise,’ the 5-Year Gap, and Embracing Her Androgyny |Marlow Stern |July 6, 2014 |DAILY BEAST 

Bitcoin serves a purpose that is at once expressive and purposeful. Bitcoin Forever! |Nick Gillespie |February 27, 2014 |DAILY BEAST 

He combines fabrics and textures and prints in a very expressive way. The ‘American Hustle’ Style Guide |Erin Cunningham |February 14, 2014 |DAILY BEAST 

The rightly cultivated expressive voice is the man—speaking. Expressive Voice Culture |Jessie Eldridge Southwick 

Mrs. Kaye's expressive eyes, which had dwelt on Isabel with flattering attention, fell to the tip of her cigarette. Ancestors |Gertrude Atherton 

The circle around did not exactly contradict him, but exhibited expressive appearances of incredulity. The Book of Anecdotes and Budget of Fun; |Various 

The two most pleasing, expressive, and powerful single instruments of music are the human voice and the violin. Violins and Violin Makers |Joseph Pearce 

He was also the inventor of "Poikilorgue," an expressive organ, which was the origin of the harmonium. The Recent Revolution in Organ Building |George Laing Miller