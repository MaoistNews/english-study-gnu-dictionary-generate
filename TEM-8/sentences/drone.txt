It even began showing off some of its other products, such as a covid-19 tracking system called Fleming, and Eclipse, which can hack drones deemed a security threat. Inside NSO, Israel’s billion-dollar spyware giant |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

NASA is planning a new mission for 2026 called Dragonfly, in which a rotorcraft drone is to fly around Titan and study the moon’s potential hospitability to life in greater detail. The 5 best places to explore in the solar system—besides Mars |Neel Patel |August 17, 2020 |MIT Technology Review 

A local whale watching group captured the event with a drone, according to Smithsonian Magazine. Environment Report: Why Your Water Bill Might Spike |MacKenzie Elmer |July 27, 2020 |Voice of San Diego 

Facebook wanted to use solar-powered drones and laser-based tech to shoot wifi to antennas. Google Loon Is Now Beaming WiFi Down to Earth From Giant Balloons |Vanessa Bates Ramirez |July 12, 2020 |Singularity Hub 

Materials chemist Eijiro Miyako of the Japan Advanced Institute of Science and Technology in Nomi imagines outsourcing pollination to automatous drones that deliver pollen grains to individual flowers. Bubble-blowing drones may one day aid artificial pollination |Maria Temming |June 22, 2020 |Science News 

The influential al Qaeda propagandist, who was born in New Mexico, died in a U.S. drone strike later that year. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

Employees strap a device to their heads and power a helicopter drone with their minds. Use Your Brain—Control a Drone |The Daily Beast Video |January 5, 2015 |DAILY BEAST 

But it takes more than just pilots to operate the drone fleet. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

In other words, the Air Force is saying that its drone force has been stretched to its limits. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

The result is that drone operators are leaving the Air Force in droves. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

Drone: the largest tube of a bag-pipe, giving forth a dull heavy tone. Gulliver's Travels |Jonathan Swift 

The melody or tune is played on one of the pipes furnished with holes for the purpose, while the other three give a drone, bass. The Recent Revolution in Organ Building |George Laing Miller 

Let those who are fit for nothing else go and drone over A B C with ragged children, if they like. The Daisy Chain |Charlotte Yonge 

Not a sound except the drone of a mountain honey-bee hanging over some blossom. God Wills It! |William Stearns Davis 

The buzzer which he had expected to roar in his ears was only a faint drone, and above it he could easily hear other sounds. Star Born |Andre Norton