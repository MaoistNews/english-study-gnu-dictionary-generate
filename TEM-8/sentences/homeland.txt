I think they both relate to our homeland security and our economy as well. What’s Different About Gretchen Whitmer, ‘That Woman From Michigan’ |Nick Fouriezos |February 10, 2021 |Ozy 

As the outgoing San Diego City Council was winding down last month, it signed off on a grant application for homeland security dollars. Morning Report: Council OK’d Police Wish List for Surveillance Gear |Voice of San Diego |January 14, 2021 |Voice of San Diego 

Instead, the various projects and pieces of equipment are scored and ranked in proportion to how well they conform to the region’s homeland security goals. Council Unknowingly OK’d Surveillance Gear for Secretive Police Group |Jesse Marx |January 13, 2021 |Voice of San Diego 

“We have our own state laws that we need to be consistent with,” said Kevin Klein, director of Colorado’s division of homeland security and emergency management. ‘Will I get a reminder for my 2nd shot?’ 6 questions about vaccine tracking—answered |Claire Zillman, reporter |January 6, 2021 |Fortune 

Early on, he naively planned to drive across his new homeland in his gifted Volga. George Blake, notorious Cold War double agent who helped Soviets, dies at 98 |Taylor Shapiro |December 26, 2020 |Washington Post 

This is a blow against freedom of speech, we were told, by the likes of Homeland Security chief Jeh Johnson. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

Or will we simply see more senseless bloodshed and another generation of Palestinians defer their dreams of a homeland? In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

What is important is the success Homeland has in capturing the core of the business. CIA Agents Assess: How Real Is ‘Homeland’? |Chuck Cogan, John MacGaffin |December 15, 2014 |DAILY BEAST 

Of course there are times when actual CIA practice would have been different from that portrayed in Homeland. CIA Agents Assess: How Real Is ‘Homeland’? |Chuck Cogan, John MacGaffin |December 15, 2014 |DAILY BEAST 

Finally, John has been a consultant to Homeland throughout this season. CIA Agents Assess: How Real Is ‘Homeland’? |Chuck Cogan, John MacGaffin |December 15, 2014 |DAILY BEAST 

They will thank God that they have come to prevent such a devastation on the soil of their own homeland. Private Peat |Harold R. Peat 

We have had two most thoroughly enjoyable furloughs in the homeland, during our missionary life. Silver Chimes in Syria |W. S. Nelson 

Civil wars broke out and internal struggles caused the invasions to fall back to the homeland. The Jewels of Aptor |Samuel R. Delany 

The homeland of the Indo-Europeans must have supported a large population to send out all the tribes which went out from it. The New Stone Age in Northern Europe |John M. Tyler 

Salaam, indeed, O happy little folk of my own homeland across the seas! Where Half The World Is Waking Up |Clarence Poe