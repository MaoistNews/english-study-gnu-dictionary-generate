The female flowers, which have a slight bulge at the base, will eventually turn into fruit. How do giant pumpkins grow so big? |Jason Bittel |October 18, 2021 |Washington Post 

Naked wires that once ran between devices and the speakers near your ears all grew an unsightly bulge packed with controls for navigating audio, answering phone calls, and adjusting the volume. Sennheiser IE 300 review: A new old-school approach to audio |Roberto Baldwin |September 8, 2021 |Popular-Science 

A bulge in a washing machine or dishwasher hose, for example, means it is ready to burst. Is your house safe? Use this checklist to find out. |Jean Feingold |September 7, 2021 |Popular-Science 

From proximity, the bulge shown in the previous view from Sterling morphs into a well-defined hook echo, which is indicative of a small area of rotation called a mesocyclone. Explaining the tornado warning and storm damage in D.C. on Thursday night |Jeffrey Halverson, Jason Samenow, Ian Livingston |July 2, 2021 |Washington Post 

In some geckos, the tumors grew into large bulges that could make it difficult for the animals to move and could get infected if ruptured. How a gecko named Mr. Frosty could help shed new light on skin cancer |Maria Temming |June 24, 2021 |Science News 

Targeting pods can bulge out a bit, and leak out unwanted signals. Newest U.S. Stealth Fighter ‘10 Years Behind’ Older Jets |Dave Majumdar |December 26, 2014 |DAILY BEAST 

But for now, the battle of the deficit bulge seems to have been won. The Battle of the Deficit Bulge Has Been Won |Daniel Gross |October 6, 2014 |DAILY BEAST 

The Huffington Post wrote, “PSA: That Bulge Is Not Actually Idris Elba's Penis.” Idris Elba’s Battle of the Bulge: Moose Knuckles and Sexist Double Standards |Keli Goff |August 11, 2014 |DAILY BEAST 

A photo of the studly actor Idris Elba sporting what appeared to be a huge bulge in his pants recently went viral. Idris Elba’s Battle of the Bulge: Moose Knuckles and Sexist Double Standards |Keli Goff |August 11, 2014 |DAILY BEAST 

One commonly cited reason for the uprising was the combination of an economic downturn with a youth bulge in the Arab world. What Do Arab Millennials Want? |William O’Connor |July 1, 2014 |DAILY BEAST 

He too, then, was awake and restless, for I saw the canvas sides bulge this way and that as he moved within. Three More John Silence Stories |Algernon Blackwood 

The temples and dome of the head seemed to bulge, as if there was too much inside for the strength of the restraining walls. Scattergood Baines |Clarence Budington Kelland 

Presently the bulge of the boat's bow glanced along the ship's side, and Joe flung his painter. The Chequers |James Runciman 

No helicopters swung their blades above; there were only the bulge of a conning tower and the heavy inset glasses of the lookouts. Astounding Stories, May, 1931 |Various 

After she had tripped over two large paving-stones that had met in an upward bulge, she took more note of detail. Ancestors |Gertrude Atherton