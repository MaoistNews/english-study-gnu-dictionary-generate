Be aware that there is a fine line between too watery and too dry. Make your own maple syrup without harming the trees |By Tim MacWelch/Outdoor Life |February 7, 2021 |Popular-Science 

Whey is the watery part of milk that separates during the cheese-making process. Best protein powder: Better nutrition in a bottle |Carsen Joenk |January 11, 2021 |Popular-Science 

Octopuses move through their watery world “by touching everything,” he notes. Touching allows octopuses to pre-taste their food |Jonathan Lambert |January 4, 2021 |Science News For Students 

Easter Island, its iconic statues whittled away by watery erosion. Authenticity Versus Longevity for Preservation: Which Matters More? |Nick Fouriezos |December 11, 2020 |Ozy 

So devising ways to study a filmy species alive in its watery home is the way to move the science forward. Larvaceans’ underwater ‘snot palaces’ boast elaborate plumbing |Susan Milius |June 15, 2020 |Science News 

He said he watched waste haulers back up to the pit and unleash torrents of watery muck. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

When multiple cases of watery diarrhea spread through one village, doctors feared it was cholera. Fighting Ebola With Nothing but Hope |Abby Haglage |August 27, 2014 |DAILY BEAST 

Many were heard coughing and seen rubbing watery, stinging red eyes. Street Battle Against Cops Again in Ferguson Despite Midnight Curfew |Justin Glawe |August 17, 2014 |DAILY BEAST 

It depicts an exhausted Texas oil field on scrub land, an old railroad bed and a watery ditch converging in the distance. Rackstraw Downes’s Art and Essays Are Two Sides of the Same Genius |Bill Morris |June 4, 2014 |DAILY BEAST 

In Ring, Sadako, the protagonist had died in a well, and returns from the watery grave to haunt the living. Japanese Horror Director Tackles the 3/11 Tsunami |Jake Adelstein, Nathalie-Kyoko Stucky |February 23, 2013 |DAILY BEAST 

And its watery places shall be dry, all they shall mourn that made pools to take fishes. The Bible, Douay-Rheims Version |Various 

As you neared it, however, the watery veil seemed flung over them, like the foamy tulle over a bride. Music-Study in Germany |Amy Fay 

As I receded, the watery veil would disappear, and as I approached it would again take form. Music-Study in Germany |Amy Fay 

He again sank, and it was only after great exertion that the brave sailors succeeded in rescuing him from a watery grave. A Woman's Journey Round the World |Ida Pfeiffer 

Long-yen is somewhat smaller, but is also white and tender, though the taste is rather watery. A Woman's Journey Round the World |Ida Pfeiffer