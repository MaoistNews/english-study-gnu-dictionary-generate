The second was how much fun the game was to watch, measured by the overall win probability swings — the steeper the dips and climbs, the better. That really was one of the least enjoyable Super Bowls of all time |Neil Greenberg |February 9, 2021 |Washington Post 

This path heads to the top of the 520-foot-high dome, which overlooks Sand Beach and also features a near vertical climb with narrow ledges and rungs embedded in the rock face. The Ultimate Acadia National Park Travel Guide |Virginia M. Wright |February 8, 2021 |Outside Online 

During the slow climb up the mountain, Jim and I chatted about our summers and the winter ahead. After the Crash, They Said I Was Fine. I Wasn't. |Erin Tierney |February 4, 2021 |Outside Online 

The stock’s IPO debuted at $27 a share, then once the pandemic hit, started a climb that looks like a ride up Mount Ventoux, today trading at almost $150 a share. Is Peloton overvalued? |Alan Murray |February 4, 2021 |Fortune 

However, Vice still faces an uphill climb to increase its revenue because its growing TV and movie business accounts for less than half of its total revenue. Media Briefing: The media industry’s top trends at the moment |Tim Peterson |January 28, 2021 |Digiday 

The pilot asked air-traffic control for permission to climb from 32,000 to 38,000 feet to avoid the bad weather. Wreckage, Bodies of AirAsia Crash Found |Lennox Samuels |December 30, 2014 |DAILY BEAST 

Make a batch of these rum balls, climb into a onesie, and let your favorite movie do the rest. Carla Hall’s Christmas Day Treat: Rum Balls |Carla Hall |December 25, 2014 |DAILY BEAST 

Republicans have the highest hill to climb but greatest opportunity. Is This the Beginning of the End for Blacks and Dems? |Keli Goff |November 3, 2014 |DAILY BEAST 

It took you quite a while to climb the Hollywood mountain, so to speak. David Oyelowo on Playing Martin Luther King Jr., Ebola Fears, and Race in Hollywood |Marlow Stern |October 15, 2014 |DAILY BEAST 

Until the epidemic is brought under control, the CDC predicts the numbers will continue to climb at that rate. Why Isn't Silicon Valley Doing More to Fight Ebola? |Abby Haglage |October 8, 2014 |DAILY BEAST 

Some of the alarm returned, however, when the creature attempted to climb up by his own ladder. The Giant of the North |R.M. Ballantyne 

I made two attempts to climb up, but both times slipped back. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Analysis and practice in preparation are the steps over which we must climb to the platform of power. Expressive Voice Culture |Jessie Eldridge Southwick 

It was held to be certain death to climb to its summit, and foolhardy in the extreme to go far up its sides. Ramona |Helen Hunt Jackson 

Climb up the hill to the old fort and look at the little wriggling gold snakes, and watch the lizards sun themselves. The Awakening and Selected Short Stories |Kate Chopin