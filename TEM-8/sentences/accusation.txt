When it comes to the accusation that Nielsen lied during sworn congressional testimony, though — which is potentially illegal — the evidence seems to be thin. What to make of the DHS whistleblower’s shocking complaint |Alex Ward |September 11, 2020 |Vox 

Foxx said the accusations from O’Brien and others that she has helped create a “revolving door” of criminals going in and out of jail is not supported by evidence. As Trump Calls for Law and Order, Can Chicago’s Top Prosecutor Beat the Charge That She’s Soft on Crime? |by Mick Dumke |September 4, 2020 |ProPublica 

A property manager repeated the fraud accusation, saying Gladden had been paid during the weeks she was out of work. She Was Sued Over Rent She Didn’t Owe. It Took Seven Court Dates to Prove She Was Right. |by Danielle Ohl, Capital Gazette, and Talia Buford and Beena Raghavendran, ProPublica |August 25, 2020 |ProPublica 

In statements to BuzzFeed News after the July 30 story was published online, Leman denied “any kind of sexual impropriety” and Norman said he categorically denied the accusations. 3 ‘Ellen DeGeneres Show’ producers exit in the midst of workplace complaints |radmarya |August 18, 2020 |Fortune 

Some of the editors acknowledge that their accusations are similar to those faced by female-led brands like the Wing and Refinery29, whose leaders stepped down amid the scandals. Former Glossier employees say they faced racism at the brand’s retail stores |ehinchliffe |August 18, 2020 |Fortune 

The concern is that a public accusation would result in an escalation. U.S. Should Make North Korea Pay for Sony Hack |Gordon G. Chang |December 18, 2014 |DAILY BEAST 

The Copperheads, a group of Midwestern Democrats, made the accusation—and far worse—against President Lincoln during Emancipation. Why We Can’t Quit Calling Presidents ‘Kings’ |Kevin Bleyer |November 22, 2014 |DAILY BEAST 

Barack Obama, made the accusation against President Bush during the Iraq War. Why We Can’t Quit Calling Presidents ‘Kings’ |Kevin Bleyer |November 22, 2014 |DAILY BEAST 

Constand claimed that the accusation was patently false, and demanded $150,000 in damages from the tabloid and attorney. How Bill Cosby Allegedly Silenced His Accusers Through A Tabloid Smear Campaign |Marlow Stern |November 21, 2014 |DAILY BEAST 

He was accused of “formalism,” a catch-all accusation that, like “Trotskyite,” had the ring of execution about it. When Stalin Met Lady Macbeth |Brian Moynahan |November 9, 2014 |DAILY BEAST 

In cases in which no attempt is made to ignore the accusation, the small wits are wont to be busy discovering exculpations. Children's Ways |James Sully 

How would the involuntary accusation have been embittered, had he known that the Empress drew the same conclusion! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

And when Lessard flung out that last unthinkable accusation, the explosion came. Raw Gold |Bertrand W. Sinclair 

The one thing that loomed big in my mind's eye was the monstrous injustice of the accusation. Raw Gold |Bertrand W. Sinclair 

Whether he had shot a man, or robbed a bank, or fired a church, the incipient accusation died away. Elster's Folly |Mrs. Henry Wood