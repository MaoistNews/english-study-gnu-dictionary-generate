Grown-up giraffes just aren’t huggy, cuddling, demonstrative animals. Having more friends may help female giraffes live longer |Susan Milius |February 25, 2021 |Science News 

There is something about a clown that stays with people: the bright colors, their tendency to be demonstrative. Nightmares in Face Paint: Why We’ll Always Be Afraid of Clowns |Oliver Jones |October 18, 2014 |DAILY BEAST 

A decade ago, Junger says, he would not have departed from a strict journalistic style to be demonstrative about himself. Sebastian Junger's War |Claire Martin |May 12, 2010 |DAILY BEAST 

Walking with arms encircling waists, or such demonstrative tokens of love, are marks of low breeding. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

But perhaps you are a chemist, and proud, as most chemists are, of the accuracy attainable in that most demonstrative science. Gospel Philosophy |J. H. Ward 

A cold reserve on Frederick's part had succeeded the demonstrative affection that had formerly existed between mother and son. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 

Asused here in its demonstrative meaning, to introduce a parenthetical clause. The Fatal Dowry |Philip Massinger 

She was preternaturally animated and demonstrative at the station—your sex's little guileful way ever since the world began. Jaffery |William J. Locke