It came from a part of the abdomen that slides under a neighboring, bald piece. Abdominal fuzz makes bee bodies super slippery |Alison Pearce Stevens |August 9, 2021 |Science News For Students 

Like Scott’s first husband, Jewett is bald and has a booming voice. MacKenzie Scott Gave Away $6 Billion Last Year. It's Not As Easy As It Sounds |Belinda Luscombe |May 25, 2021 |Time 

Mysterious deaths of bald eagles, mallards and other lake life in the southeastern United States have puzzled scientists for more than 20 years. A toxin behind mysterious eagle die-offs may have finally been found |Susan Milius |March 26, 2021 |Science News 

Natalie Mamerow noticed her first bald spot two weeks after receiving 19 stitches. A New Frontier to Fight This Hair-Loss Disease |Charu Kasturi |March 2, 2021 |Ozy 

For sweeping panoramas, hike to the top of its numerous bald peaks. The Ultimate Acadia National Park Travel Guide |Virginia M. Wright |February 8, 2021 |Outside Online 

“You can cut my hair, you can bald me, you can strip me naked and take away my dignity,” she said. A Quorum For Change: The Fight For Global LGBT Equality |Justin Jones |December 11, 2014 |DAILY BEAST 

He was a large man, totally bald, with the rough hands of a peasant. In Chile, Poetry Outlives the Dictators |Jay Parini |October 27, 2014 |DAILY BEAST 

And sitting down is a bald man who looks like Hank from Breaking Bad. The Holy Grail of Comic Books Hid in Plain Site at New York Comic Con |Sujay Kumar |October 14, 2014 |DAILY BEAST 

Director Dan Reed, a tall, bald-headed Brit, landed in Nairobi soon after the attack. Westgate's Chilling Security Video Reveals Shopping Mall Bloodbath |Nina Strochlic |September 15, 2014 |DAILY BEAST 

Omran, who was 17 at the time, was completely bald, weak, and as frail as a burnt match. Beating Cancer & Dodging Israel's Bombs |Itay Hod |September 1, 2014 |DAILY BEAST 

Though the amount played for is serious, a good deal of rather bald conversation and chaff goes on. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

But he forgot the stagnant town, the bald-headed man at the club window, the organ and "The Manola." Bella Donna |Robert Hichens 

As observation widens and grows finer, the first bald representation becomes fuller and more life-like. Children's Ways |James Sully 

She wondered if he would take his youth in his bald-headed season, like the self-made American millionaire. Ancestors |Gertrude Atherton 

The future Emperor saw at a glance that this small, stout, bald-headed young man had qualities which few others possessed. Napoleon's Marshals |R. P. Dunn-Pattison