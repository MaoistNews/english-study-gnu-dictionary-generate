The biggest decision the Space Force faces at the moment is how widespread the diplomatic corps will be and which host countries will get an attaché. The U.S Will Soon Have Space Force Ambassadors Around the World |Jeffrey Kluger |August 4, 2022 |Time 

Her father, Josef, was a member of the Czechoslovak Foreign Service and served as press attaché in Belgrade, Yugoslavia, and later became ambassador to Yugoslavia. Madeleine Albright passes away at 84 |Brody Levesque |March 23, 2022 |Washington Blade 

I had a very old, rather beautiful leather attaché case, which I thought was exactly the sort of thing that a spy would wander around in. The Mom Who Stole the Blueprints for the Atomic Bomb (The Freakonomics Radio Book Club Ep. 11) |Sarah Lyall |September 25, 2021 |Freakonomics 

A tutor at college and a Museum attache; very jocular; given to personal witticisms, which were often aimed at Goriot. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

On n'y apprend rien de nouveau, et cependant il attache et intresse. Baron d'Holbach |Max Pearson Cushing 

He was no less a personage than the attache, of whom she had written to Pamela, and his name was Victor Maurien. Theo |Mrs. Frances Hodgson Burnett 

In the discussion which followed the conclusion of the story of the Naval Attache the gentleman with the pearl took no part. In the Fog |Richard Harding Davis 

He caught up his glass, and slapped the Naval Attache violently upon the shoulder. In the Fog |Richard Harding Davis