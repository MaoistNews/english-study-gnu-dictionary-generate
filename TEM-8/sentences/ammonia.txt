Another option is to convert renewable electricity into some other form of relatively clean energy, such as hydrogen, ethanol, or ammonia. Facebook A.I. researchers push for a breakthrough in renewable energy storage |Jeremy Kahn |October 14, 2020 |Fortune 

Since ammonia is toxic to humans, such leaks require immediate action, involving lengthy spacewalks to identify holes in the coolant system and repair them. Astronauts on the ISS are hunting for the source of another mystery air leak |Neel Patel |September 30, 2020 |MIT Technology Review 

The ISS has previously dealt with ammonia leaks coming from the station’s cooling loops. Astronauts on the ISS are hunting for the source of another mystery air leak |Neel Patel |September 30, 2020 |MIT Technology Review 

Jain’s discovery could allow workers to transport ammonia instead, which is safer, and then free the hydrogen from the ammonia once it has arrived where’s it needed. This year’s SN 10 scientists aim to solve some of science’s biggest challenges |Science News Staff |September 30, 2020 |Science News 

It turns out there was an ammonia feed going into the tap water, and when they turned it off, the phone stopped ringing. Erin Brockovich has given up on the federal government saving the environment |Nicole Goodkind |September 25, 2020 |Fortune 

Is she back in the orphanage where it smells like ammonia and cooked cabbage? When An Adopted Child Won’t Attach |Tina Traster |May 2, 2014 |DAILY BEAST 

The plant was checked out only when the state agency received a complaint about a strong ammonia smell. They Saw It Coming: Life in West, Texas, After the Boom |Christine Pelisek |April 22, 2013 |DAILY BEAST 

He instinctively knew it was coming from the 50-year-old fertilizer plant and ammonia storage facility a few blocks away. They Saw It Coming: Life in West, Texas, After the Boom |Christine Pelisek |April 22, 2013 |DAILY BEAST 

It runs on combustible poison—ammonia and pressurized hydrogen. American Dreams: ‘The Mosquito Coast’ by Paul Theroux |Nathaniel Rich |September 20, 2012 |DAILY BEAST 

But the ammonia leak in November, and now the radiation leak and deteriorating tubes, might lead some to conclude otherwise. Latest Accident at San Onofre Nuclear Plant Worries Activists, Residents |Jamie Reno |February 13, 2012 |DAILY BEAST 

They are dissolved by strong hydrochloric acid, and recrystallize as octahedra upon addition of ammonia. A Manual of Clinical Diagnosis |James Campbell Todd 

On the other side the ammonia brought out a picture of the Victory, with the head of a roaring lion below it. Uncanny Tales |Various 

His results for ammonia, as well as nitric acid, are given in the subjoined table. Elements of Agricultural Chemistry |Thomas Anderson 

Ammonia is a compound of nitrogen and hydrogen, but it cannot be formed by the direct union of these gases. Elements of Agricultural Chemistry |Thomas Anderson 

It appears also, as far as absorption goes, to be immaterial whether the ammonia is free or combined. Elements of Agricultural Chemistry |Thomas Anderson