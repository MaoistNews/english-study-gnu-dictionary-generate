The shopping spree by the rich will not offset the spending cuts by the rest of the population. Haves and Have-Nots: Pandemic Recovery Explodes China’s Wealth Gap |Daniel Malloy |August 19, 2020 |Ozy 

Over the years, developers in San Diego have been required to fork over money to offset the impact of their development. Morning Report: School’s Out for … ??? |Voice of San Diego |July 14, 2020 |Voice of San Diego 

Early hot spots like New York City have cooled off, but that decrease in new cases is offset by a surge in states like Texas, Arizona, California and Florida. The U.S. largely wasted time bought by COVID-19 lockdowns. Now what? |Jonathan Lambert |July 1, 2020 |Science News 

Urchin, Dungeness crab and clam biomass fell when otters were present, but these losses were offset by gains in fish and other invertebrates that rely on kelp. Bringing sea otters back to the Pacific coast pays off, but not for everyone |Jonathan Lambert |June 11, 2020 |Science News 

So I’ve always believed that the euro is problematic, because you’re creating a currency, but without a single banking system, without a fiscal union, without offsets to deal with it. The Prime Minister Who Cried Brexit (Ep. 392) |Stephen J. Dubner |October 10, 2019 |Freakonomics 

Fortunately, Pomplamoose made some money to offset some of these expenses. How Much Money Does a Band Really Make on Tour? |Jack Conte |December 8, 2014 |DAILY BEAST 

He plants a tree in Central America for every bottle of Tru spirits he sells to offset the carbon produced in manufacturing. People for the Ethical Treatment of Vodka |Debra A. Klein |July 23, 2014 |DAILY BEAST 

The wealthy want to be seen as even more parsimonious, to offset the incriminating millions in their bank accounts. Sting and Hillary Are Just Like You: How the Very Rich Play at Being Very Ordinary |Tim Teeman |June 24, 2014 |DAILY BEAST 

And the potential growth in those businesses could easily offset the loss of revenues from selling tobacco. CVS Quits Tobacco to Become a Medical Giant |Daniel Gross |February 5, 2014 |DAILY BEAST 

It provides $63 billion in sequester relief, which is partially offset by a $23 billion mix of spending cuts and “fees.” Tea Party Republicans: The Biggest Sore Winners in Washington |Jamelle Bouie |December 12, 2013 |DAILY BEAST 

The Constitution requires that reapportionment or redistricting take place every ten years to offset population changes. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Russia thought by joining hands with France she would offset the power of Germany and Austria. Ways of War and Peace |Delia Austrian 

To offset this, I paid Mike $600 a year, and employed his son Joe at $1.75 a day, for twenty weeks. The Idyl of Twin Fires |Walter Prichard Eaton 

The two effects would consequently offset each other under such conditions. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz 

We find a strong offset to the horror of Aztec cruelty in the very Bible, which we regard as the mainstay of our religious world. Ancient Faiths And Modern |Thomas Inman