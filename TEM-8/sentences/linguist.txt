She started to read the work of linguist and political thinker Noam Chomsky in her spare time. One year after George Floyd’s death sparked a movement, many protesters’ lives are forever changed |Marissa Lang |May 28, 2021 |Washington Post 

“Semantic change” is what linguists call it when the meaning of a word changes over time. A crispy cauliflower recipe with lemon-mustard dressing proves a cutlet is what you make of it |Daniela Galarza |May 13, 2021 |Washington Post 

I recently talked to a gesture linguist to learn how people use their hands in different cultures. The woman who will decide what emoji we get to use |Tanya Basu |May 11, 2021 |MIT Technology Review 

The team consulted linguists to define the parameters for a “male” and “female” voice and figure out where they overlap. A feminist internet would be better for everyone |Emily Luong |April 1, 2021 |MIT Technology Review 

That is Steven Pinker, the Harvard psychologist and linguist. “We Get All Our Great Stuff from Europe — Including Witch Hunting.” (Ep. 446) |Steven D. Levitt |January 7, 2021 |Freakonomics 

Global Linguist Solutions, LLC (GLS) and the U.S. Military thanks you for your service. Obama Went to War to Save Them, But They Can’t Get U.S. Visas |Christine van den Toorn, Sherizaan Minwalla |September 28, 2014 |DAILY BEAST 

But as linguist Mark Liberman notes at Language Log, the president used the word “I” exactly 10 times in that speech. Why the Right Thinks Obama’s a Narcissist—and Why They’re Wrong |John McWhorter |September 18, 2014 |DAILY BEAST 

For example, the creator/writer of Alice in Arabia is Brooke Eikmeier, a former US Army linguist in Arabic. Hollywood’s Major Muslim Problem Doesn't End With 'Alice in Arabia' |Dean Obeidallah |March 22, 2014 |DAILY BEAST 

Utopia for Beginners Joshua Foer, The New Yorker An amateur linguist loses control of the language he invented. The Week’s Best Longreads: The Daily Beast Picks December 22, 2012 |David Sessions |December 22, 2012 |DAILY BEAST 

Linguist Geoffrey Nunberg gives the A-word the biography it so richly deserves. ‘Ascent of the A-Word:’ The Beauty of the Indispensable Vulgarity |Malcolm Jones |August 17, 2012 |DAILY BEAST 

Peter Elmsly, a partner of the celebrated Paul Valliant, and himself an importer of books and no mean critic and linguist, died. The Every Day Book of History and Chronology |Joel Munsell 

Bora on April 27, 1852, this prelate was a man of great culture and a distinguished linguist, who had travelled considerably. The Philippine Islands |John Foreman 

His mother was a highly educated woman, and was careful to make her son an accomplished linguist. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

He was a marvellously alert man, an unusually good linguist, and a cosmopolitan to his finger-tips. The Doctor of Pimlico |William Le Queux 

Edward Lhuyd died; a celebrated antiquary and linguist, and keeper of the Ashmolean museum. The Every Day Book of History and Chronology |Joel Munsell