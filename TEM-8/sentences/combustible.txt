West told the story of a house two miles from a fire that burned when an ember landed in combustible materials at its base. At Glacier’s Edge, the Flames Have Always Come for My Family Cabin |jversteegh |August 20, 2021 |Outside Online 

In theory, a full reopening during a surge in cases sounds like a combustible mix. Why England’s sudden lifting of covid restrictions is a massive gamble |Charlotte Jee |July 18, 2021 |MIT Technology Review 

After that time, it becomes less combustible, and components of it can separate, reducing its octane value. How to Safely Store and Transport Extra Gasoline |Wes Siler |May 14, 2021 |Outside Online 

Frankel tells his story through interweaving profiles, mostly of men who have to overcome financial woes, combustible egos and their own self-doubt. ‘Midnight Cowboy’ was a masterpiece made of desperation |James Hirsch |April 2, 2021 |Washington Post 

Here, in your small corner on Planet Earth, in the middle of a vast, indifferent cosmos, you can achieve zen-like calm by methodically layering pieces of combustible matter for future use. How to Find Zen by Splitting and Stacking Wood |Martin Fritz Huber |March 1, 2021 |Outside Online 

This means not offering provocative remarks on a combustible topic like immigration, which is sure to make them enemies. Obama and Latinos Are at the Breaking Point |Ruben Navarrette Jr. |July 21, 2014 |DAILY BEAST 

Children have fantasy lives so rich and combustible that rigging them with lies is like putting a propeller on a rocket. Why Jimmy Kimmel’s Lies Matter |Sam Harris |November 19, 2013 |DAILY BEAST 

This combustible brew of race, class, and economic anxieties bubbles all too closely to the surface. The GOP’s Backdoor Impeachment Scheme |Lloyd Green |October 14, 2013 |DAILY BEAST 

In both, devotion and yearning are fragile, easily combustible, and hard to replace. This Week’s Hot Reads, July 15, 2013 |Sarah Stodola, Jen Vafidis, Damaris Colhoun |July 15, 2013 |DAILY BEAST 

The Middle East today is more combustible and complex than it has ever been. Ted Cruz's War on Context |Justin Green |January 31, 2013 |DAILY BEAST 

It is very combustible, burns with a pale blue flame, and is converted into water. Elements of Agricultural Chemistry |Thomas Anderson 

Finally all the combustible portion of the fort was burnt to the ground, 12 cannon were captured, and about 60 Moros were slain. The Philippine Islands |John Foreman 

At such moments, there was something brooding and combustible about him that gave one the sensation of walking over a mine. The Woman Gives |Owen Johnson 

There was something so combustible and wild in his attitude, that, there, at least no one was under illusions as to the danger. The Woman Gives |Owen Johnson 

There was no greater foundation for this than for Newton's celebrated conjecture that the diamond was combustible. A System of Logic: Ratiocinative and Inductive |John Stuart Mill