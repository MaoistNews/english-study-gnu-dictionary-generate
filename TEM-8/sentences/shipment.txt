Hospitals hoarded doses, scared of not receiving more shipments. Confusion and chaos: Inside the vaccine rollout in D.C., Maryland and Virginia |Julie Zauzmer, Gregory S. Schneider, Erin Cox |February 9, 2021 |Washington Post 

We test every delivery of fruits, vegetables, rice and other ingredients for up to 255 contaminants to confirm that every shipment meets our strict quality standards. New report finds toxic heavy metals in popular baby foods. FDA failed to warn consumers of risk. |Laura Reiley |February 4, 2021 |Washington Post 

Connecticut tracks vaccination information and adjusts shipments of shots based on that data. Many States Don't Know Who's Getting COVID-19 Vaccines. That's a Huge Problem for Equity |Chris Wilson |January 28, 2021 |Time 

Tracking information is sent to Tiberius for every shipment so HHS can keep tabs on how deliveries are going. This is how America gets its vaccines |Cat Ferguson |January 27, 2021 |MIT Technology Review 

In Europe, officials said shipments of Pfizer’s vaccine to their countries have been cut in half. US joins Covax, China’s vaccine diplomacy, prehistoric vegetarians |John Detrixhe |January 22, 2021 |Quartz 

Sicilian seaports that will facilitate the shipment have already stepped up security measures, especially on incoming vessels. Italy Steps Up Security Over Alleged ISIS Plot to Kill The Pope |Barbie Latza Nadeau |August 28, 2014 |DAILY BEAST 

Her car shipment was delayed for three weeks, with no information about where it was or when it would show up. American G.I.s: Dude, Where’s My Car? |Tim Mak |July 16, 2014 |DAILY BEAST 

Since May, the Pentagon said, IAL has processed more than 22,000 cars for shipment. American G.I.s: Dude, Where’s My Car? |Tim Mak |July 16, 2014 |DAILY BEAST 

If the officials involved with the chemical weapons shipment are worried about the criminal surroundings, they are not showing it. Syria’s Chemical Weapons Arrive in Notorious Mafia Port |Barbie Latza Nadeau, Christopher Dickey |July 1, 2014 |DAILY BEAST 

The rocket shipment seized on Wednesday, according to an IDF spokesman who briefed reporters, began in Damascus. Israel Seizes Iranian Rockets—and Pounces on Tehran for ‘Supplying Terrorists’ |Eli Lake |March 6, 2014 |DAILY BEAST 

After some experimentation, his shipment was the first one to reach England in good condition. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

It will be observed that with the increase of coprah shipment, the export of cocoanut-oil has decreased. The Philippine Islands |John Foreman 

We've got to get that platinum shipment off the C-49 and get into communication with headquarters at Deliphon somehow. Spacewrecked on Venus |Neil R. Jones 

But nothing like that happened yet, and the shipment p. 106 was brought through. The Planet Strappers |Raymond Zinke Gallun 

Explain how the routing of a shipment may be influenced by the existence of a commodity rate. Cyclopedia of Commerce, Accountancy, Business Administration, v. 1 |Various