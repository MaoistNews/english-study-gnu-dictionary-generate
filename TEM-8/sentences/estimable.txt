Tucked somehow within his madhouse autumns as the estimable 11th-year Stanford football coach, Shaw got to observe in real time both Texas A&M 74, LSU 72, in seven overtimes, in 2018, and Illinois 20, Penn State 18, in nine overtimes, on Saturday. You might not like college football’s new overtime rules, but do you have a better plan? |Chuck Culpepper |October 28, 2021 |Washington Post 

Who we are is enough without commodities and other people, but in-group admission and approval — and the sense of safety and belonging that comes with it — feels like the estimable thing we need to gain in order to self-actualize. The great American cool |Safy-Hallan Farah |July 14, 2021 |Vox 

Now the estimable Cook Report has moved the race from Lean Republican to a Toss-Up. Mark Sanford Ditched by NRCC, but Counting Him Out Would Be Unwise |John Avlon |April 18, 2013 |DAILY BEAST 

To save himself, Nixon nominated the estimable Elliot Richardson to be his new attorney general. How Kennedy Brought Down Nixon |Chris Matthews |September 13, 2009 |DAILY BEAST 

He was a most estimable person, but he never ignored an opportunity to talk with a new and interesting woman. Ancestors |Gertrude Atherton 

You must imagine this sound as something between a grunt and a groan, that the estimable lady gave vent to whenever put out. Elster's Folly |Mrs. Henry Wood 

As sons of Freedom you are now called upon to defend your most estimable blessings. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

I am still a Loafer; Jim is a most estimable member of the gentlest society; and this is how it all came about. The Chequers |James Runciman 

And with this show of humility, which may not have been entirely sincere, this estimable lady took her departure. The Circular Study |Anna Katharine Green