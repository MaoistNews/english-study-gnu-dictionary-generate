She was disappointed because, instead of playing with the doll, I cut it open to see why it talked. The NASA Engineer Who’s a Mathematician at Heart |Susan D'Agostino |January 19, 2021 |Quanta Magazine 

The doll also holds a replica of the writer’s memoir, I Know Why the Caged Bird Sings. Dr. Maya Angelou Honored With A Barbie Doll |Brande Victorian |January 15, 2021 |Essence.com 

Another couple on Twitter asked her to create an Amazon Wishlist and then bought her kids a doll, cars and other toys for Christmas. ‘We are struggling’: Unemployed Americans face a bleak Christmas |Lee Clifford |December 24, 2020 |Fortune 

In the United Kingdom, Ildiko Duretz has been selling handmade dolls for children for the last 16 years. Here's How Shopping Scams on Facebook Are Ripping Off Thousands of Customers, With the Money Flowing Overseas |Andrew R. Chow |December 19, 2020 |Time 

No, no, the doll’s dress was blue, they will tell you, and they are right, but they can’t feel what you feel, that little echo of your mother’s dress, that little echo of your love for your mother, attached to your doll. Dark spaces on the map |Katie McLean |December 18, 2020 |MIT Technology Review 

Then Ziegler tosses the buff LaBeouf around like a rag doll. Sia and Shia LaBeouf’s Pedophilia Nontroversy Over ‘Elastic Heart’ |Marlow Stern |January 9, 2015 |DAILY BEAST 

Families stuff a life-size male doll with memories of the outgoing year and dress him in their clothing. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

To test out a doll he designed to have realistic human proportions, Nickolay Lamm went to a group of second-graders in Pittsburgh. Pot-Smoking Grannies, Jimmy Fallon Covers U2, and More Viral Videos |The Daily Beast Video |November 23, 2014 |DAILY BEAST 

The auction house reached out to the Levine estate to procur the doll. Bid on CIA’s Osama Action Figure, Lewinsky's Lingerie, and More at This L.A. Auction House |Asawin Suebsaeng |November 11, 2014 |DAILY BEAST 

Wilson later landed in hot water with some by pushing for the creation of the first Barbie for President doll. Will There Ever Be a ‘Good Wife’ Effect on Politics? |Keli Goff |October 20, 2014 |DAILY BEAST 

The heavy man unconsciously shook him in his powerful grasp, as a child might shake a doll. The Joyous Adventures of Aristide Pujol |William J. Locke 

If you can get a small skin, fit it to a doll the way you think the Cave-men fitted skins to their bodies. The Later Cave-Men |Katharine Elizabeth Dopp 

And there was Louis the Goon—his little clay pigeon—in one of the booths with a doll. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Her whole soul hangs upon the lips of a beautiful baby doll that seems to be calling her his mummy. Marguerite |Anatole France 

But once when Hepzebiah fell in the pond after her doll, Rover swam in and caught her dress in his mouth and brought her to shore. Seven O'Clock Stories |Robert Gordon Anderson