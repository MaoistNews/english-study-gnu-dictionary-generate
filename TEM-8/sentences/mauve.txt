Meet STEVE, the northern lights in mauve Say hello to a new member of the colorful night sky, STEVE. Let’s learn about auroras |Trishla Ostwal |November 9, 2021 |Science News For Students 

The aurora looked like a ribbon of pink or mauve light, sometimes with "picket fence" columns of green light passing through the ribbon. New analysis confirms hypothesis for source of mysterious auroral “dunes” |Jennifer Ouellette |May 13, 2021 |Ars Technica 

The movie’s images are rendered in soft, suedelike tones, mauves and grays and shadowy taupes, colors that speak of either giving up or hanging on, depending on your mood. Roy Andersson's Quietly Gorgeous About Endlessness Explores Questions for Which There Are No Answers |Stephanie Zacharek |April 29, 2021 |Time 

The unusual sky glow’s mauve arc looks a bit like an aurora, but isn’t. Amid winter’s darkness, flashes of brilliance |Nancy Shute |November 29, 2020 |Science News 

Scientists have broken the light of STEVE’s mauve streak into its many individual wavelengths. Newfound ‘dunes’ is among weirdest of northern lights |Maria Temming |March 9, 2020 |Science News For Students 

The village houses are done up in pale gray and mauve and preside over lawns so neat and green they look like carpeting. The Stacks: The Searing Story of How Murder Stalked a Tiny New York Town |E. Jean Carroll |April 19, 2014 |DAILY BEAST 

The opening look combined a light turquoise, slinky evening gown with a mauve headpiece. Gareth Pugh's Sci-Fi Galaxy |Liza Foreman |September 25, 2013 |DAILY BEAST 

The palette was gentle and pretty, focusing on rose pinks, lavenders, dusky mauve, mint, faun and pale lemon. Burberry Prorsum Spring/ Summer 2014 |Tom Sykes |September 16, 2013 |DAILY BEAST 

Wool lace blouses with dolman sleeves topped slim matching skirts in shades of pea green and mauve. Rodarte Channels Australia In Fall Show for New York Fashion Week |Robin Givhan |February 14, 2012 |DAILY BEAST 

The eddies beyond the breakwater were a light and delicate mauve and looked nervously alive. Bella Donna |Robert Hichens 

This time there was a faint scream in answer and a mauve-and-white bonnet bobbed agitatedly up the road. Punch, or the London Charivari, Vol. 147, November 4, 1914 |Various 

Oh, yes, and he wants you to send him some new pajamas—only two or three pairs, and you're not to send him mauve ones. Happy House |Betsey Riddle, Freifrau von Hutten zum Stolzenberg 

These Chian cigarettes in their diaphanous paper of mildest mauve would suit your oddly remote, your curiously shy glance. Sinister Street, vol. 1 |Compton Mackenzie 

In the sky, splinters of mauve tore at curtains of purplish flame. Hunters Out of Space |Joseph Everidge Kelleam