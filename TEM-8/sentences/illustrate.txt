Now, that last question illustrated the potential downside of asking sensitive questions. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

That illustrates just how much the creative development process has changed and how much can be achieved asynchronously, he added. Businesses adopt ‘asynchronous working’ to fight remote-working fatigue and encourage cross-border collaboration |Jessica Davies |February 9, 2021 |Digiday 

When talking about soundscapes, print articles can only do so much to illustrate the issue. Underwater Noise Pollution Is Disrupting Ocean Life—But We Can Fix It |Aryn Baker |February 5, 2021 |Time 

According to Mackinac, the example Overton often used to illustrate the window’s movement is the changed public perception of school choice. Betsy DeVos is gone — but ‘DeVosism’ sure isn’t. Look at what Florida, New Hampshire and other states are doing. |Valerie Strauss |February 5, 2021 |Washington Post 

Business needs to work in conjunction with government because, as the pandemic has illustrated all too clearly, when it comes to a global crisis, they depend on governments with their powers of monetary creation and taxation to bail them out. The Public Trusts Businesses Over Government to Solve Our Problems. But We Need Both to Try |Colin Mayer |February 3, 2021 |Time 

This video, courtesy of BuzzFeed, helps to illustrate this phenomenon. What Is Privilege? |The Daily Beast Video |December 11, 2014 |DAILY BEAST 

This is likely a lowball number but it has the merit to illustrate the tradeoff that raising the minimum wage requires. How a GOP Senate Can Help the Poor |Veronique de Rugy |November 23, 2014 |DAILY BEAST 

And as the above mentioned clips illustrate, Sesame Street is really good at staying relevant. ‘Sesame Street’ Is Middle-Aged and Awesome |Emily Shire |November 10, 2014 |DAILY BEAST 

His maquettes, or models, illustrate this, too, in their budding materiality. Frank Gehry Is Architecture’s Mad Genius |Sarah Moroz |October 27, 2014 |DAILY BEAST 

As if to illustrate the rampant sexism that still exists, hackers threatened to release nude photos of her after her speech. The Gender-Pay Gap: It’s Real, and Yes, It’s Sexism |Monica Potts |September 27, 2014 |DAILY BEAST 

Let me illustrate: Last week, month, or year you saw a military procession pass along the streets. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Nothing can more clearly illustrate Napoleon's dictum, "A la guerre les hommes ne sont rien, c'est un homme qui est tout." Napoleon's Marshals |R. P. Dunn-Pattison 

He paused, and to illustrate the imperious humor of the Scot, he waved his fingers and a red wrister at me. The Soldier of the Valley |Nelson Lloyd 

A few notes will serve to illustrate the chief subjects for care and some important items in fitting instruments properly. Violins and Violin Makers |Joseph Pearce 

It is enjoined in statutes of perpetual moral obligation, that illustrate the ten precepts of the law. The Ordinance of Covenanting |John Cunningham