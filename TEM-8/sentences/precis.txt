In each of these collections, you’ll find a quick, engaging précis on the topic that will keep you tapping through. Introducing Quartz Essentials, for extracting knowledge from the news |Zachary M. Seward |May 4, 2021 |Quartz 

There is also a brief and characteristic 'Precis de ma vie', dated November 17, 1797. The Memoires of Casanova, Complete |Jacques Casanova de Seingalt 

It is, in fact, a precis of the "Instruction Book," which deals with everything a police officer should know and be. Scotland Yard |George Dilnot 

The monkey ate various forms of Precis (a Vanessid), after which it was given Acraea halali. Mimicry in Butterflies |Reginald Crundall Punnett 

In submitting a precis of the gospel narratives I have not implied any estimate either of their credibility or of their truth. Preface to Androcles and the Lion |George Bernard Shaw 

It is the precis de toutes les choses (Boisguillebert), the compendium of the wealth of society. A Contribution to The Critique Of The Political Economy |Karl Marx