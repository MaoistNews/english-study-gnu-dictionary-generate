Fortunately, Hoover, who at the time was living in London, intervened and convinced both sides to let him organize food relief as a private citizen, essentially creating his own pirate nation with its own flag, naval fleet, and railroads. How Ice Cream Became the Ultimate American Comfort Food |Matt Siegel |August 27, 2021 |Eater 

During the American Revolutionary War, the revolutionaries were looking for creative ways to challenge British naval superiority. The Battle for the Ocean Floor |Sean Culligan |July 14, 2021 |Ozy 

There’s both a naval blockade and a 40-mile-long border fence barring entry into Israel, complemented by an additional nine miles of steel and concrete walls on the Gaza-Egyptian border. Israel Is Sending Robots With Machine Guns to the Gaza Border |Sébastien Roblin |June 25, 2021 |The Daily Beast 

In late April, the Pentagon officially released naval footage of “unidentified aerial phenomena” that had been shared online, which may have primed some people to seek UFOs in their own backyards. 50 years ago, UFO sightings in the United States went bust |Lillian Steenblik Hwang |June 21, 2021 |Science News 

A recent study of the remains of one victim, who died on the beach not far from a small naval vessel, suggests that he might have been a senior naval officer. Mount Vesuvius victims died just moments away from rescue |Kiona N. Smith |May 14, 2021 |Ars Technica 

Editor's Note: This story had been amended to clarify Jeffrey Wright's Naval service. The Navy ‘Hero’ Who Pimped an HIV-Positive Teen |M.L. Nestel |December 11, 2014 |DAILY BEAST 

However, there has been a lot of work done since the report was released, the naval architect said. Can the Navy's $12 Billion Stealth Destroyer Stay Afloat? |Dave Majumdar |October 22, 2014 |DAILY BEAST 

“This is an area that the Navy is taking seriously,” one naval architect familiar with the design told The Daily Beast. Can the Navy's $12 Billion Stealth Destroyer Stay Afloat? |Dave Majumdar |October 22, 2014 |DAILY BEAST 

She entered into the Naval Reserve of the Royal Canadian Navy at 17 while still in high school. The Making of Kiesza: From Navy Sharpshooter to Beauty Queen to Pop Diva |Marlow Stern |October 20, 2014 |DAILY BEAST 

Churchill welcomed the approach and immediately continued the exchanges using the code name “naval person.” Blood and War: The Hard Truth About ‘Boots on the Ground’ |Clive Irving |September 22, 2014 |DAILY BEAST 

He had seen through a powerful naval glass some figures standing erect and silhouetted against the sky on the parapet. Gallipoli Diary, Volume I |Ian Hamilton 

I decided after anxious searching of heart to help the French by taking over some portion of their line with the Naval Brigade. Gallipoli Diary, Volume I |Ian Hamilton 

First we attack with the naval half and are held up—next we attack with the army half and are held up. Gallipoli Diary, Volume I |Ian Hamilton 

During the first few weeks following the Cavite naval battle nothing remarkable occurred between the belligerents. The Philippine Islands |John Foreman 

Last night the French and the Naval Brigade made a good advance with slight loss. Gallipoli Diary, Volume I |Ian Hamilton