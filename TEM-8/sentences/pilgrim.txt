They would have one class in potato burlap sacks and colorful feathers, and then the other class dressed as pilgrims. What Thanksgiving Means Today to the Native American Tribe That Fed the Pilgrims |Olivia B. Waxman |November 23, 2021 |Time 

Singing at a concert with other fans, chanting prayers surrounded by pilgrims or doing the “wave” during a ballgame — these are all ways of making personal connections. Large events can be cesspools of germs. Here’s why we ache for them, anyway. |Galadriel Watson |May 13, 2021 |Washington Post 

Named after what some say is the patron saint of pandemics, the village is drawing day-trippers seeking to snap a photo of its street sign as well as pilgrims who want to pray at the altar of the saint. This Austrian ski resort is preparing for a snowless future |Denise Hruby |February 18, 2021 |Washington Post 

Not everyone is happy to let the pope play the humble pilgrim. The Pope’s Middle East Game of Shrines |Candida Moss |May 25, 2014 |DAILY BEAST 

It is home to Papa Pilgrim, a literal-minded, self-fashioned prophet; his wife, Country Rose; and their 15 dutiful children. This Week’s Hot Reads, July 15, 2013 |Sarah Stodola, Jen Vafidis, Damaris Colhoun |July 15, 2013 |DAILY BEAST 

Most were originally drove roads, paths to market, or pilgrim paths. The Unfindable Place: Robert Macfarlane’s Holloway |Edward Platt |June 26, 2013 |DAILY BEAST 

Pictured above is the sculpture titled Hopeful Had Much Ado from Pilgrim's Progress. The Future of Print! |William O’Connor |June 4, 2013 |DAILY BEAST 

The tip of the one of the spires at the National Cathedral fell onto the steps of Pilgrim Road. Earthquake Hits the East Coast! | |August 23, 2011 |DAILY BEAST 

Naw, I sold my outfit to a goggle-eyed pilgrim that has an idea buffalo hides is prime all summer. Raw Gold |Bertrand W. Sinclair 

There is a charm in Defoe's works that one hardly finds, excepting in the Pilgrim's Progress. Journal of a Voyage to Brazil |Maria Graham 

We can get a pretty good idea of the reasons which led the Pilgrim Fathers to brave everything to get away from their home land. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Every pilgrim would consider his pilgrimage of no account if he did not step in here immediately on his arrival. A Woman's Journey Round the World |Ida Pfeiffer 

The older nuns had turned from their tasks and paused, in passing by, to hear the pilgrim's story. The First Christmas Tree |Henry Van Dyke