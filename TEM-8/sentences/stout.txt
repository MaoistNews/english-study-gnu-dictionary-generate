Catch blue crabs from tidal banks, docks, and bridges by simply soaking a chicken neck or wing tied to a stout cord. 14 wild edibles you can pull right out of the ocean |By Bob McNally/Field & Stream |October 19, 2020 |Popular-Science 

After boiling them in seasoned water, remove the cooked meat from the shells with a pin or stout toothpick. 14 wild edibles you can pull right out of the ocean |By Bob McNally/Field & Stream |October 19, 2020 |Popular-Science 

Despite these visceral examples, broaching the urgency of addressing climate change and how it intersects with Alberta’s oil sector tends to come up against stout resistance. After the boom: Canada’s oil capital faces an uncertain future |kdunn6 |September 21, 2020 |Fortune 

To hang your food, tie a rock to one end of 100 feet of 550 cord and throw it over a stout tree branch while holding the other end. This essential survival tool can save your life 10 different ways |By Tim MacWelch/Outdoor Life |September 15, 2020 |Popular-Science 

Here, the scents are further multiplexed into even more complex scents and sent to higher-level areas, allowing us to distinguish between, say, a lager and a stout. A Highway to Smell: How Scientists Used Light to Incept Smell in Mice |Shelly Fan |July 1, 2020 |Singularity Hub 

A limited edition export stout known as the Indra Kunindra came to wash it down. Dinner at Nitehawk Cinema: ‘Christmas Vacation’ and a Beer in a Pear Tree |Rich Goldstein |December 12, 2014 |DAILY BEAST 

Princess Ariel and Prince Eric walk down the aisle, and are greeted by a stout clergyman who is allegedly too happy to see them. When the Religious Right Attacked ‘The Little Mermaid’ |Asawin Suebsaeng |November 20, 2014 |DAILY BEAST 

From the few photographs of him, we see a stout man with deep Indian features, a thick mustache and stoic face. New Orleans’ Carnivalesque Day of the Dead |Jason Berry |November 1, 2014 |DAILY BEAST 

In a quote usually associated with Bill Stout, designer of the Ford Tri-Motor: “Simplicate and add more lightness.” Why Old-School Airships Now Rule Our Warzones |Bill Sweetman |June 30, 2014 |DAILY BEAST 

A stout woman with a grating voice, she asked, “So you think life is so good here in Ukraine?” Putin’s People Stage Their Bogus Vote |Jamie Dettmer |May 11, 2014 |DAILY BEAST 

She was growing a little stout, but it did not seem to detract an iota from the grace of every step, pose, gesture. The Awakening and Selected Short Stories |Kate Chopin 

In some parts of Korea the houses were built of stout timbers, the chinks covered with woven cane and plastered with mud. Our Little Korean Cousin |H. Lee M. Pike 

Martini prepared a couple of stout mules, and concealed them amongst the thickets on the opposite side of the fosse. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The stout brigadier grunted an assent and rolled monumentally down the Avenue. The Joyous Adventures of Aristide Pujol |William J. Locke 

He was wedged in behind some stout women, and had the pleasure of hearing another word or two from Mrs. Kattle. Elster's Folly |Mrs. Henry Wood