Cats that started using puzzle feeders actually brought home more wildlife. Meatier meals and more playtime might reduce cats’ toll on wildlife |Jonathan Lambert |February 11, 2021 |Science News 

There’s also a risk of the disease being transferred from one bird species to another due to the intermingling that occurs at feeders — something that’s rare in nature. Your dirty bird feeder could be spreading disease |Melissa Hart |February 9, 2021 |Washington Post 

Stigma sanctioned by GOP bottom feeders like Pence is a major factor behind those tragic deaths. Time for GOP to open up to LGBTQ Americans |James Driscoll |January 28, 2021 |Washington Blade 

Five minutes later, the hummingbird flew to the feeder and Bruce shut the door. Catch me if you can: An unlikely hummingbird is banded at a Virginia park |John Kelly |January 18, 2021 |Washington Post 

University Avenue and El Cajon Boulevard and their feeder streets can’t be widened. What the Census Taught Me About the NIMBY vs. YIMBY Debate |Paul Krueger |December 14, 2020 |Voice of San Diego 

At first Wales and Sanger conceived of Wikipedia merely as an adjunct to Nupedia, sort of like a feeder product or farm team. You Can Look It Up: The Wikipedia Story |Walter Isaacson |October 19, 2014 |DAILY BEAST 

Counting the visitors to the bird feeder would probably be pushing it. Up to a Point: I Do My Own Taxes With No Help, Except From a Couple of Bloody Marys |P. J. O’Rourke |April 15, 2014 |DAILY BEAST 

Inside a feeder school for The Family, a secretive global network of evangelical power players. The Week’s Best Longreads for September 14, 2013 |David Sessions |September 14, 2013 |DAILY BEAST 

Many comedians have rejected the bro code and pushed back against resorting to bottom-feeder rape punchlines for cheap laughs. Louis C.K. on Daniel Tosh’s Rape Joke: Are Comedy and Feminism Enemies? |Jennifer L. Pozner |July 18, 2012 |DAILY BEAST 

They also need a voice in land rights and decisions to build feeder roads that make it possible to get their goods to market. South Sudan's Women: Building the World’s Newest Nation |Swanee Hunt |December 15, 2011 |DAILY BEAST 

It is the Book of Howth which accuses Sidney of being a ‘lusty feeder and surfeiter.’ Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell 

The Wye is the most important feeder of the Derwent, and runs through scenery that is romantically beautiful. The Rivers of Great Britain: Rivers of the East Coast |Various 

Two miles further down, on the west side, the Ouse receives another important feeder in the Aire. The Rivers of Great Britain: Rivers of the East Coast |Various 

It is very largely a bottom feeder and, therefore, rather of a sluggish nature. Game Birds and Game Fishes of the Pacific Coast |Harry Thom Payne 

While the pressman is laying out his plates the feeder should be cutting thin sheets of paper the size of one of the plates. The Building of a Book |Various