The holiday season is set to be a whirlwind, and there’s no time left to pivot, but Amazon is a good place to be at this time. Deep Dive: How to grow e-commerce capabilities on Amazon |Pierre Bienaimé |December 15, 2020 |Digiday 

It’s a heart-wrenching choice not to spend the holidays with your loved ones, especially after this whirlwind of a year, but unfortunately in pandemic times, the safest thing to do is stay apart. Here are 6 things to consider about holiday travel |Terry Nguyen |December 11, 2020 |Vox 

Alas, I live in Los Angeles, and even if I lived in New York, now doesn’t seem like the best time to be having a whirlwind love story with someone I’ve never met. One Good Thing: Netflix’s Dash & Lily is a sweet TV rom-com with a Christmas twist |Emily VanDerWerff |November 20, 2020 |Vox 

To eliminate it is to court the whirlwind of governance — to accept that your opponents may win elections, to risk their agenda passing into law. The definitive case for ending the filibuster |Ezra Klein |October 1, 2020 |Vox 

At our center, which comprises nothing, like the hollowness in the middle of a whirlwind, we fall back into the world. What the Meadow Teaches Us - Issue 90: Something Green |Andreas Weber |September 16, 2020 |Nautilus 

He talks about a whirlwind weekend-long affair with a man he met at a club in Berlin. Choose Your Own Neil Patrick Harris: The Star on ‘Doogie,’ ‘Gone Girl,’ Gay Sex and More |Kevin Fallon |October 10, 2014 |DAILY BEAST 

Well known for his inability to say no to worthy causes, Palmer has always been a whirlwind of good works. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

After a whirlwind romance, Sarkozy quickly married Bruni in February 2008. Francois Hollande Announces Breakup with First Lady Valerie Trierweiler |Tracy McNicoll |January 26, 2014 |DAILY BEAST 

Later that month he was making his first whirlwind foreign trip to identify countries that might be willing to take detainees. Congress Cooperates, Obama Pushes Hard, and Closing Gitmo Has a Chance |Daniel Klaidman |December 12, 2013 |DAILY BEAST 

This year has been a whirlwind for Kate Bosworth—featuring a new movie, new design collaboration, and even a new husband. Kate Bosworth Is Back and Crazy in Love |Erin Cunningham |November 5, 2013 |DAILY BEAST 

They burst out of the mouth of the canyon, a smoke-wreathed whirlwind, heading for the protection of the river. Raw Gold |Bertrand W. Sinclair 

She took one look, then struck her horse a sharp blow and, like a whirlwind, came upon the scene. The Courier of the Ozarks |Byron A. Dunn 

With regard to the Whirlwind, perhaps it might correspond better to Dors picture; it has not turned out quite what I wanted. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Before your pots can feel the thorns, he shall take them away as in a whirlwind, both living, and in his wrath. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

Meanwhile the leading Apaches, not now more than fifty in number, were swept along by the same whirlwind of brute instinct. Overland |John William De Forest