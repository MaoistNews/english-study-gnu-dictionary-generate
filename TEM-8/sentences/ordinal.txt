For example, in the ordinal 24th the suffix th is counted as another word. How to Write Letters (Formerly The Book of Letters) |Mary Owens Crowther 

A full stop is placed after most abbreviations, after initial letters, and after ordinal numbers in Roman characters. "Stops" |Paul Allardyce 

Pie, pī, n. a book which ordered the manner of performing divine service: a service-book: an ordinal. Chambers's Twentieth Century Dictionary (part 3 of 4: N-R) |Various 

I reach what M. Couturat calls the ordinal theory which is the foundation of arithmetic properly so called. The Foundations of Science: Science and Hypothesis, The Value of Science, Science and Method |Henri Poincar 

Still more, why should they not happen with regard to family, ordinal, and class distinctions? Darwin, and After Darwin, Volume II (of 3) |George John Romanes