Brex was founded by the mischievous Brazilian duo of Henrique Dubugras, an iPhone “jailbreaker” at age 12, and Pedro Franceschi, who had to shut down his gaming company for patent violations at 14. The New Wolves of Wall Street |Liam Jamieson |August 4, 2021 |Ozy 

Other pet owners may only need to get to the bottom of the shenanigans of a mischievous pet. Best dog camera: Find out if Fido is being a good boy while you’re gone |Florie Korani |June 25, 2021 |Popular-Science 

She later adds, in a mischievous understatement, “All that remained now was for Jones and Hill to go insane.” The greatest prison escape ever? ‘The Confidence Men’ tells a sensational true story. |Michael Dirda |June 16, 2021 |Washington Post 

In a mischievous turn, the anti-capitalist rap-metal group Rage Against the Machine was chosen as musical guest. Elon Musk is being brought in to save SNL’s sagging ratings. He could sink the show in other ways. |Steven Zeitchik |May 7, 2021 |Washington Post 

The beloved author of Madeline, a story about a mischievous Parisian girl, was also quite the rebel. 12 Crazy True Stories About Children’s Authors |Kate Bartlett |April 1, 2021 |Ozy 

He suppressed a belch and then looked up at her with a mischievous grin. Football Great Bob Suffridge Wanders Through the End Zone of Life |Paul Hemphill |September 6, 2014 |DAILY BEAST 

A mischievous cross between Sunset Boulevard and Adult Swim, BoJack Horseman boasts an exciting cast of supporting characters. 'BoJack Horseman': The Debauched Tales of a Drunken, Groupie-Sexing D-List Horse, Hits Netflix |Marlow Stern |August 22, 2014 |DAILY BEAST 

He was a biggish lad, with a boyish, slightly mischievous grin, and thoughtfulness and consideration were seamed in his character. The James Foley I Knew in the ISIS War Zone |Jamie Dettmer |August 20, 2014 |DAILY BEAST 

The biggest personality is Little My, utterly self-centred, mischievous, and rancidly funny. Tove Jansson, Queen of the Moomins |John Garth |August 9, 2014 |DAILY BEAST 

It lands with mischievous accuracy, mottling the crotch of my jeans. Leaky Ceilings, Catcalls, and Uncaged Pythons: 4 Hours on NYC’s Worst Subway |Kevin Zawacki |August 8, 2014 |DAILY BEAST 

Even Benny caught the fever of conquering the mischievous water which slipped from their grasp like quicksilver. The Box-Car Children |Gertrude Chandler Warner 

I can take good care of myself; beside, with a mischievous glance into his serious eyes, I really dont know whom to marry. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

I have seen in a single night the most flourishing orange-tree stripped of every leaf by this mischievous creature. Journal of a Voyage to Brazil |Maria Graham 

Of course, I used to get terribly frightened, fearing that the men would attack me for carrying such a mischievous monkey. Kari the Elephant |Dhan Gopal Mukerji 

Mischievous wags are a kind of insects which are in everybodys way and plentiful in all countries. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre