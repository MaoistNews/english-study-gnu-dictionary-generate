They launch into a noisy but triumphant rendition of Dolly Parton’s “9 to 5,” trading looks of pure, astonished joy as the crowd remains bemused. Peacock's All-Girl Muslim Punk Band Comedy We Are Lady Parts Is a Rockin' Good Time |Judy Berman |June 2, 2021 |Time 

He invited the bemused newsmen to examine the delicate tracery of the insect’s wings. The wings of war: Some people believed cicadas carried a dire warning |John Kelly |May 31, 2021 |Washington Post 

Carlson isn’t known for his emotive diversity, so Fox viewers simply saw Carlson flickering around, befuddled and bemused, as the text at the bottom of the screen assured them that it would all be over soon. Republican men are a central part of coronavirus vaccine resistance |Philip Bump |March 12, 2021 |Washington Post 

That typically involves engaging a bemused media owner on the other end with knock-knock jokes that don’t have a punchline. Broken windows, ‘cuddling breaks’ and interrupted video calls: Parents share realities of juggling work while homeschooling kids |Jessica Davies |February 26, 2021 |Digiday 

Assembled in the slick, now-familiar “docu-tainment” style that has become the fashion with high-profile shows of its ilk, “The Lady and the Dale” takes a deceptively bemused tone from the start. ‘The Lady and the Dale’ explores transphobia in 1970s America |John Paul King |February 10, 2021 |Washington Blade 

A bemused line reading sneaks in when you are anticipating a hammy hard sell. It’s OK to Like ‘The Big Bang Theory’ |Kevin Fallon |September 23, 2014 |DAILY BEAST 

But the prevailing emotion that day, even among us awardees, was a bemused sense of boredom, restlessness and insatiability. The Medal of Honor Disgrace |Brian Van Reet |March 26, 2014 |DAILY BEAST 

Some folks watch Bieber's challenges with bemused interest, others with disgust, and others with genuine concern. Justin Bieber's Spiritual Crisis |Joshua DuBois |January 26, 2014 |DAILY BEAST 

Messina would introduce himself to bemused staffers and ask them to visit his office for a second or two. No Drama Obama’s Dramatic 2012 Reelection Campaign |Richard Wolffe |September 12, 2013 |DAILY BEAST 

Bemused, two journalists behind me wondered aloud “what this guy is doing here.” Matt & Tom Berninger on The National’s Doc ‘Mistaken for Strangers’ |Melissa Leon |April 25, 2013 |DAILY BEAST 

The truth is, I have a little lost my way, and stand bemused at the cross-roads. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

But his trembling nervous fingers and bemused eyes could make nothing of the "case" thus so suddenly brought before him. The Doctor's Family |Mrs. (Margaret) Oliphant 

After a time I exchanged it for one of the volumes of sermons, only to be equally bemused. A Cabinet Secret |Guy Boothby 

She led him, unresisting but bemused, towards the gate, and into a confectioner's. The Grey Wig: Stories and Novelettes |Israel Zangwill 

Harky pondered a question that has bemused all great philosophers: how can humans be so foolish? The Duck-footed Hound |James Arthur Kjelgaard