Fortunately for those of us who resided outside SoCal, Scully also worked for national networks, allowing his economical narratives and poetic incantations to resonate far beyond Los Angeles. 'I Think He Stands Alone.' Remembering a Perfect Vin Scully Call |Sean Gregory |August 3, 2022 |Time 

Almost like an incantation of protection, a practice of softness. My Pandemic Baby Is Pulling Us Out of Our Cozy Cave. But How Will the World See a Disabled Mother Like Me? |Rebekah Taussig |April 28, 2021 |Time 

She sounds like Holiday, but what she’s offering is less an act of mimicry than an intimate incantation, a spell of protectiveness. The United States vs. Billie Holiday Is a Messy But Passionate Tribute to an American Legend |Stephanie Zacharek |February 26, 2021 |Time 

It is hard to make out exactly what she is saying, but her words act almost like an incantation. Giovanni Zoppé’s Real-Life Family Circus |Malcolm Jones |October 21, 2012 |DAILY BEAST 

There a familiar sound met his ears—the roll of a drum followed by an incantation in a quavering, high-pitched voice. The Joyous Adventures of Aristide Pujol |William J. Locke 

They met by the parapet of the Quai, just as Père Bracasse had come to the end of his incantation. The Joyous Adventures of Aristide Pujol |William J. Locke 

Whenever they passed an embedded fakir, they obtained an incantation from his lips, but still Baal-Zeboub failed. Devil-Worship in France |Arthur Edward Waite 

And now he began to speak, not loudly, but with solemn deliberation, as though he were uttering an incantation. Dream Tales and Prose Poems |Ivan Turgenev 

I have extensive knowledge of incantation, poetry, magic, and I know these concern your problem. The Jewels of Aptor |Samuel R. Delany