From a geeky standpoint, Smith’s reboot-y sequel series honors its predecessor in ways notable and sly, thereby allowing it to function both as an accessible entry point and as a fleshed-out extension of what came before. By the Power of Grayskull, ‘He-Man’ Is Back—Beefier and Better Than Ever |Nick Schager |July 23, 2021 |The Daily Beast 

It’s a show about fitness, but it’s also, in a sly way, about feminism. Rose Byrne on Unleashing Her Meanest Streak Yet in ‘Physical’: ‘It’s Deeply Uncomfortable’ |Kevin Fallon |June 17, 2021 |The Daily Beast 

On the subject of fashion, men have been quite the sly devils over the years. Why the tailored suit — not ruffles and lace — became synonymous with power |Robin Givhan |May 21, 2021 |Washington Post 

Google security researchers are warning people to be on the lookout for a squad of sly hackers believed to be North Korean agents. Google outs suspected North Korean hackers |Robert Hackett |January 26, 2021 |Fortune 

As Xu Bing himself has noted, his made-up characters “seem to upset intellectuals,” in a sly sendup of our respect for the written word. Reading, That Strange and Uniquely Human Thing - Issue 94: Evolving |Lydia Wilson |December 23, 2020 |Nautilus 

Rambo movie marathon—11 am-12 am, AMCBecause nothing says the holidays like Sly Stallone and blood. The Ultimate Thanksgiving Weekend TV Guide: Must-See Marathons, Specials, and Parades |Kevin Fallon |November 26, 2014 |DAILY BEAST 

The movie has a very sly sense of humor, and I think people might miss that. Sherman Alexie on His New Film, the Redskins, and Why It's OK to Laugh at His Work |William O’Connor |August 22, 2014 |DAILY BEAST 

This weekend, you can see Sly lay waste to hundreds of fools in The Expendables 3. Rambo Hates Guns: How Sylvester Stallone Became the Most Anti-Gun Celeb in Hollywood |Asawin Suebsaeng |August 14, 2014 |DAILY BEAST 

Rylance was unforgettable as the mordantly sly king in Richard III, giving a brilliant new twist to a classic role. Who Will Win the Tony Awards? |Janice Kaplan |June 7, 2014 |DAILY BEAST 

Her songs draw on Stevie Wonder, the Jackson 5, Sly Stone, Curtis Mayfield, and contemporary hip-hop. In the Backseat With Janelle Monae: A Limo Ride with the Stylish R&B Diva |Douglas Wolk |April 28, 2014 |DAILY BEAST 

Jones said this with a chuckle and a sly expression in his face, as he glanced meaningly at his companion. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

In the first place, one element of public-house talk—the overt or sly indecency—is left out. The Chequers |James Runciman 

He had known men who were almost as sly as rats; but on the whole he looked upon them as inferior beings. The Nursery, December 1881, Vol. XXX |Various 

There was a sly emphasis on the word, and a subtle humor in his look that showed the old detective at his worst. The Circular Study |Anna Katharine Green 

We got Fanny a dress on the sly, gaudy black velvet and Duchesse lace. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson