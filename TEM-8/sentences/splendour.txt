The rays of the ascending sun were bright upon it: and the superb palace of the Spanish kings shone in its fullest splendour. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

And he forgot his lesser self in this crowded atmosphere of ancient divinities and old-world splendour. The Wave |Algernon Blackwood 

It is true that they wanted the picturesque splendour of ancient warfare. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Only the western heavens, where the sun sank in a red mass of ominous splendour, was alive with energy. The Wave |Algernon Blackwood 

The Sultan declared he could never receive the Governor with such splendour, but he wanted him to promise to return his visit. The Philippine Islands |John Foreman