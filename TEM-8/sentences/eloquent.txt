As if to drive home the distinction, he left the lofty language to the event’s youngest speaker, Amanda Gorman, a 22-year-old poet from Los Angeles whose spoken-word poem, “The Hill We Climb,” was the ceremony’s eloquent capstone. America yearns for an era of good feeling. The inaugural ceremony launched one. |Peter Marks |January 20, 2021 |Washington Post 

An eloquent exception is a painting by Sina Ata, an American-born Iraqi who lives in Jordan. In the galleries: Middle East artists examine sheltering in place amid pandemic |Mark Jenkins |January 15, 2021 |Washington Post 

I do not believe it is a matter of opinion that Assemblywoman Shirley Weber is the most eloquent and moving speaker among California politicians. Politics Report: Shirley Weber’s Shoes to Fill |Scott Lewis |December 26, 2020 |Voice of San Diego 

The book is dotted with tight, eloquent passages that unite these concerns. Delusions, justice, accountability and freedom in America |Paul Taylor |December 18, 2020 |Washington Post 

Less explicit yet more eloquent are such wordless pictures as “Bloodletting,” in which torrents of pigment nearly submerge the stars and stripes. In the galleries: A wide array of media carry election-year messages |Mark Jenkins |October 30, 2020 |Washington Post 

He should also be remembered for being an early and eloquent foe of Nazism. The Catholic Philosopher Who Took on Hitler |John Henry Crosby |December 26, 2014 |DAILY BEAST 

For her part, Michele, in interviews, is eloquent, to the point, and assured. Why Does Everyone Hate Lea Michele? |Tim Teeman |October 9, 2014 |DAILY BEAST 

But what about the eloquent, book smart, interesting, quirky, inquisitive black woman, you ask? Lifetime’s ‘Girlfriend Intervention’: The Fairy Black Mothers TV Doesn’t Need |Phoebe Robinson |September 25, 2014 |DAILY BEAST 

In the eloquent words of colonial preacher John Winthrop, “When a man is to wade through deep water, there is required tallness.” For Short Men in 2014, The News Is Surprisingly Good |Kevin Bleyer |September 13, 2014 |DAILY BEAST 

What happened to that hopeful, eloquent man we elected in 2008? Why Won't Obama Go to Ferguson? |Stuart Stevens |August 19, 2014 |DAILY BEAST 

He was a patriot of the noblest and most extensive views, and justly celebrated as a man of learning, eloquent and refined. The Every Day Book of History and Chronology |Joel Munsell 

She looked up at him with sad and eloquent eyes, which softened his heart in spite of himself. Rosemary in Search of a Father |C. N. Williamson 

Tories will wax eloquent on "the pink miasma of revolutionary Radicalism." Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

Hugh said nothing, but his silence was eloquent to Evelyn, who knew now the whole story of the girl with the soft eyes. Rosemary in Search of a Father |C. N. Williamson 

Never had the black population of the city listened to or witnessed a more eloquent appeal. The Homesteader |Oscar Micheaux