We used it to prepare a few rib eyes, and the virtual temperature controls were helpful to get just the right amount of heat and flame out of our wood. 5 Portable Grills for All Your Outdoor Cooking Needs |Amy Marturana Winderl |September 2, 2020 |Outside Online 

Stoichiometric flames are the Goldilocks variety, with just the right amount of fuel for complete combustion. Four types of flames join forces to make this eerie ‘blue whirl’ |Emily Conover |August 12, 2020 |Science News 

Replacing the flammable liquid in lithium-ion batteries would tame their risk of flame. Batteries should not burst into flames |Carolyn Wilke |April 16, 2020 |Science News For Students 

The latter, however, was successful in mitigating the danger by putting out the flames. Know Your Historical Warships: From 7th Century BC – 17th Century AD |Dattatreya Mandal |April 4, 2020 |Realm of History 

Launching rice and its fixings allows a chef to cook it over really hot flames without burning. There’s science to making great fried rice |Emily Conover |March 4, 2020 |Science News For Students 

Hatuey asked the religious man holding the flame if indeed any Christians were in heaven. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

An orange blossom of flame exploded on our screens as a new reality dawned. The Resilient City: New York After 9/11 |John Avlon |September 11, 2014 |DAILY BEAST 

What if it was the divine being who was a symbol for the original object of worship: the flame? Spirit Tripping With Colombian Shamans |Chris Allbritton |August 24, 2014 |DAILY BEAST 

That is, until a rough-tongued Scotsman rekindled the flame. Queen Victoria’s Secret Scottish Sex Castle |Clive Irving |August 17, 2014 |DAILY BEAST 

According to their friend, producer/filmmaker Choke No Joke, it was over a mutual flame. Did Beyoncé Just Accuse Jay Z of Cheating? |Marlow Stern |July 2, 2014 |DAILY BEAST 

Like many another cavalier, he had a flame in every country, or rather, in every town which he visited. Napoleon's Marshals |R. P. Dunn-Pattison 

The very soil in which it grew must be burned out with the flame of avenging justice. The Unsolved Riddle of Social Justice |Stephen Leacock 

The bushes seemed to burst into smoke and flame, and then came a crashing volley in return. The Courier of the Ozarks |Byron A. Dunn 

A fiery intensity of light lay over it, as though any moment it must burst into sheets of flame. The Wave |Algernon Blackwood 

It is very combustible, burns with a pale blue flame, and is converted into water. Elements of Agricultural Chemistry |Thomas Anderson