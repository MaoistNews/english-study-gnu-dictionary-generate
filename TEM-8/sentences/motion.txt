Every frame looks like an ukiyo-e print in motion, kinetic and untethered to the physical world. 7 daring movie adaptations of literary classics |Allegra Frank |August 28, 2020 |Vox 

Committees for the Republican Senatorial and Congressional campaigns filed motions in the case, arguing that election rules, including the staff prohibition, should not be changed. Hundreds of Thousands of Nursing Home Residents May Not Be Able to Vote in November Because of the Pandemic |by Ryan McCarthy and Jack Gillum |August 26, 2020 |ProPublica 

Our eyes contain cones, which are the cells that sense color, and rods, which sense motion. You don’t see as much color as you think |Bethany Brookshire |August 21, 2020 |Science News For Students 

The Fujitsu team built an artificial-intelligence program that could learn to recognize and outline a human skeleton within these motion data. A robot referee can really keep its ‘eye’ on the ball |Kathryn Hulick |August 20, 2020 |Science News For Students 

To turn methanol into motion, the researchers coated a nickel-titanium alloy wire with platinum. Methanol fuel gives this tiny beetle bot the freedom to roam |Carmen Drahl |August 19, 2020 |Science News 

I wonder what that lady is doing now, and if she knows what she set in motion with Archer? ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

The train was already in motion as she tried to step inside, and her body was crushed beneath it. Riding Thailand’s WWII Death Railway |Liza Foreman |December 21, 2014 |DAILY BEAST 

Stop-motion animation artist PES has unveiled a new short this week. ‘Sexual’ Barbershop Quartet, a Panda Family Reunion, and More Viral Videos |The Daily Beast Video |December 14, 2014 |DAILY BEAST 

But what he did set in motion a series of events that ended in his life being lost. A Black Cop’s Tough Words for Mike Brown |Mary M. Chapman |December 3, 2014 |DAILY BEAST 

This year McQueen picked up three Oscars (including best picture) for his third motion picture 12 Years A Slave. Has the Turner Prize Gone Soft? |David Levesley |December 2, 2014 |DAILY BEAST 

In this situation we waited the motion of the enemy, without perceiving any advancement they made towards us. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

These sections also have vibrations of their own which are of shorter length and more rapid motion. Expressive Voice Culture |Jessie Eldridge Southwick 

Felipe watched over her as a lover might; her great mournful eyes followed his every motion. Ramona |Helen Hunt Jackson 

At six o'clock I felt once more the welcome motion of a Railroad car, and at eight was in Venice. Glances at Europe |Horace Greeley 

The first jolt had like to have shaken me out of my hammock, but afterwards the motion was easy enough. Gulliver's Travels |Jonathan Swift