The contractor turned out to have a criminal record, including convictions for armed robbery and drug offenses, according to the report. VA Secretary Focused on Smearing Woman Who Said She Was Sexually Assaulted in a VA Hospital, Probe Finds |by Isaac Arnsdorf |December 10, 2020 |ProPublica 

He said a co-worker who witnessed the shooting told him that the gunman announced a robbery but shot his father in the head before taking his wallet and fleeing. Police arrest suspect in fatal shooting of construction worker in Northeast Washington |Peter Hermann |December 9, 2020 |Washington Post 

A 17-year-old Leonardtown male was arrested and charged with armed robbery, robbery, and first- and second-degree assault. Southern Maryland crime report | |November 11, 2020 |Washington Post 

A 37-year-old Lexington Park man was arrested and charged with armed robbery, robbery, and first- and second-degree assault. Southern Maryland crime report | |November 11, 2020 |Washington Post 

Baghdasaryan, going by the nickname Svo Raf, would spend 34 years — more than half his life — behind bars for theft, robbery, hooliganism and drugs. The Mobster Who Brought Armenia and Azerbaijan Together … in Death |Fiona Zublin |October 9, 2020 |Ozy 

Hair Robbery Is Acutally A Crime in Venezuela: Women in Venezuela apparently need to be more protective over their hair. 'Hair Robbery' is Venezuela's Latest Crime; Oprah Dons 3.5-Pound Wig on O Magazine Cover |The Fashion Beast Team |August 6, 2013 |DAILY BEAST 

The Weather Underground Brinks Robbery, the last big fringe-violent left-wing attack that I remember, happened 32 years ago. In Defense of Measured Speculation |Michael Tomasky |April 16, 2013 |DAILY BEAST 

There is no such charge as Murder of a Good Person or Robbery of a Bad Person. To Shoot or Not to Shoot |Edward Conlon |April 14, 2011 |DAILY BEAST 

Robbery is supposed to have been the motive, and suspicion points in half-a-dozen directions. The Riddle of the Mysterious Light |Mary E. Hanshew 

To the left of the chief is the justice's clerk; and behind the bench is a placard, 'Robbery and Murder. Rowlandson the Caricaturist. First Volume |Joseph Grego 

Robbery and public plunder were rampant in the State capital. The Broken Sword |Dennison Worthington 

Robbery of women was practiced not only by the ancient Jews, but practically by all nations of antiquity. Woman and Socialism |August Bebel 

Robbery and fighting and everything that is bad, between the newly arrived rebels and the city people. Twelve Years of a Soldier's Life in India |W. S. R. Hodson