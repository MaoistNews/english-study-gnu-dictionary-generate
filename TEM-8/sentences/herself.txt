As the protagonist gets herself off in front of her impotent husband, she moans “Oh, Gronky.” ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

Myerson herself appears to have bought into that stigma, offering mixed to negative views on the Miss America pageant. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Real Housewives of New Jersey star Teresa Giudice turned herself in to serve a 15-month sentence for bankruptcy fraud. How a ‘Real Housewife’ Survives Prison: ‘I Don’t See [Teresa Giudice] Having a Cakewalk Here’ |Michael Howard |January 6, 2015 |DAILY BEAST 

One difference was that Chen was herself wearing white gloves. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

But the act of killing herself done, the message was sent, and heard, and things started changing. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

She herself had worn them in her youth, and they were the proper bonnets for "growing girls." The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Hilda, trembling at the door, more than half expected Mr. Orgreave to say: "You mean, she's invited herself." Hilda Lessways |Arnold Bennett 

None other would dare to show herself unveiled to a stranger, and a white man at that. The Red Year |Louis Tracy 

She took the fan from Madame Ratignolle and began to fan both herself and her companion. The Awakening and Selected Short Stories |Kate Chopin 

And once more, she found herself desiring to be like Janet--not only in appearance, but in soft manner and tone. Hilda Lessways |Arnold Bennett