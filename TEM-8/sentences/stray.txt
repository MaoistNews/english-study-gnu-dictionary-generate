A stray football rolled toward him from the other end, where the varsity had gathered, so he picked it up and threw it back. Byron Leftwich is a rising star on a Bucs coaching staff that shows the power of diversity |Adam Kilgore |February 5, 2021 |Washington Post 

We’ve strayed from that vision, but we are on our way back to it, now for a digital century. 'We Need a Fundamental Reset.' Shoshana Zuboff on Building an Internet That Lets Democracy Flourish |Billy Perrigo |January 22, 2021 |Time 

Using NLP, we can then analyze how far the mutant strays in its “meaning”—for example, its behavior. A Language AI Is Accurately Predicting Covid-19 ‘Escape’ Mutations |Shelly Fan |January 19, 2021 |Singularity Hub 

We even have a chance for a stray snow flurry, especially north and west of the city. D.C.-area forecast: Sunny skies ahead of a cold and windy Inauguration Day |Matt Rogers |January 19, 2021 |Washington Post 

Some have bars and dining rooms that stray from familiar bowling-alley fare, such as the Bay Area’s Castro Valley Bowl, with its casual Thai and Laotian stand, Lucky Lane 33 Cafe. America’s Independent Bowling Alleys Might Not Make It Through the Pandemic |Emma Orlow |January 15, 2021 |Eater 

She narrowed her eyes, bit her lip as if to chew over the question, and whisked some stray blond hairs away from her face. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

While some stray from the fold, most stay with the same pack their entire lives. Mongooses, Meerkats, and Ants, Oh My! Why Some Animals Keep Mating All in the Family |Helen Thompson |December 29, 2014 |DAILY BEAST 

On the weekends the birds and stray cats keep the artists company as they set up their displays. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

The film tells the story of Ron “Stray Dog” Hall, a burly, sixtysomething biker and Vietnam vet. Life After ‘Winter’s Bone’: Debra Granik on Finding J. Law and the Plight of the Female Director |Marlow Stern |October 24, 2014 |DAILY BEAST 

He karate-chops a final stray assassin, then they exchange vows. Angelina Jolie and Brad Pitt Got Married and We’re Worried About Jennifer Aniston |Kevin Fallon, Tim Teeman |August 28, 2014 |DAILY BEAST 

He rose and kissed her lightly on the forehead, experience teaching him to avoid a stray hair from the carefully built coiffure. Ancestors |Gertrude Atherton 

The men stirred, and stray words of mingling wonder and anger reached the Parisian. St. Martin's Summer |Rafael Sabatini 

Stray goats and mules gazed expectantly up and down the track. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward 

If there should happen t' be a stray trooper hangin' round there, the same would be mighty awkward for you fellers. Raw Gold |Bertrand W. Sinclair 

Dolcoath Stray Park engine, with a cylinder of 63 inches in diameter, did thirty-two millions. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick