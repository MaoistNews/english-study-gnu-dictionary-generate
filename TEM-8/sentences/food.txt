Rice cakes have even seen a resurgence in their fortunes in recent years due partly to a surging interest in gluten-free foods. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

To the haters, sunflower seeds are bird food or a sorry replacement for chewing tobacco. Sunflower Seeds Are the Best Snack for the Anxious Mind |Emma Alpern |September 17, 2020 |Eater 

Rafael Tonon is a journalist and food writer living between Brazil and Portugal. Christian Puglisi Is Closing His Influential Copenhagen Restaurants. COVID Is Only Partly to Blame |Rafael Tonon |September 17, 2020 |Eater 

First, reporting requirements similar to those for food and active pharmaceutical ingredients should extend to excipients. The ‘inactive' ingredients in your pills could harm you |By Yelena Ionova/The Conversation |September 15, 2020 |Popular-Science 

We’re not seeing a lot of tech companies traveling, but some in construction and food companies have to travel, and have been pretty much since late April. Are you ready to start traveling for work again? TripActions’ CEO is banking on it |Michal Lev-Ram, writer |September 15, 2020 |Fortune 

And there is definitely something to finding solace in food, familiarity, and memory. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

Finding a smuggler in Ventimiglia is easier than finding good food. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

Talking about death is never easy, but with food, comfort, and familiarity, a new kind of dinner party is making it easier. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

And if people find themselves dissatisfied with how often they turn to fast food, Bacon says to try things like batch cooking. Why Your New Year’s Diet Will Fail |Carrie Arnold |December 30, 2014 |DAILY BEAST 

Instead of just cutting out whole food groups, Bacon says people should pay attention to how food makes them feel. Why Your New Year’s Diet Will Fail |Carrie Arnold |December 30, 2014 |DAILY BEAST 

And I have not had the first morsel of food prepared from this grain offered me since I reached the shores of Europe. Glances at Europe |Horace Greeley 

The general commanded a halt, and ordered the men to refresh and strengthen themselves by food and drink. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

It was he who first said, If thine enemy hunger give him food, if he thirst give him drink. Solomon and Solomonic Literature |Moncure Daniel Conway 

These officers are bound to maintayne themselves and families with food and rayment by their owne and their servant's industrie. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

I suspect, from the evident care taken of it, that its product is considerably relied on for food. Glances at Europe |Horace Greeley