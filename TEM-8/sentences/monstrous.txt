Twenty-two-year-old wunderkind Luka Dončić started his Olympic career with a monstrous performance, one rarely witnessed in international basketball. Tokyo Surprises: Expect the Unexpected |Toyloy Brown III |August 6, 2021 |Ozy 

Enter Epstein, an old business contact of her father’s, whose short-lived romance with Maxwell gave way to a very different, allegedly monstrous kind of mutually beneficial arrangement. What’s the Point of Trying to Understand Ghislaine Maxwell? |Judy Berman |June 24, 2021 |Time 

Far too often, traditional narrative storytelling has worked to further marginalize people who are already marginalized in society by framing aspects of their identities as monstrous, inherently evil, or Other. Why it’s so satisfying to root for Disney villains |Aja Romano |May 28, 2021 |Vox 

The more layers of this scam you peel back, the more monstrous it looks. The hidden scam behind Tucker Carlson and the right’s ‘replacement’ game |Greg Sargent |April 23, 2021 |Washington Post 

One does not last weeks on this monstrous regimen without spiritual help. Gene Weingarten: Behold, the worst diet of all time |Gene Weingarten |April 15, 2021 |Washington Post 

“You know, I never had a monstrous ego,” Mailer confides to a friend in l987. Mailer’s Letters Pack a Punch and a Surprising Degree of Sweetness |Ronald K. Fried |December 14, 2014 |DAILY BEAST 

All I had in those days was a monstrous lack of ego which therefore required huge injections of actorly ego and misled people. Mailer’s Letters Pack a Punch and a Surprising Degree of Sweetness |Ronald K. Fried |December 14, 2014 |DAILY BEAST 

Even Godzilla, the ugliest star attraction of them all, is bigger than ever, both at the box office and in sheer monstrous height. Can Tarzan of the Apes Survive in a Post-Colonial World? |Ted Gioia |November 23, 2014 |DAILY BEAST 

“In the camp no-one knows themselves,” muses the monstrous commandant. How Hitch & Amis Discovered Evil In My House |Peter Foges |September 28, 2014 |DAILY BEAST 

And decency is our great, guiding strength as we face such monstrous foes as ISIS. From ISIS Videos to JLaw Nudes, When Is Looking Abetting Evil? |Michael Daly |September 3, 2014 |DAILY BEAST 

She was in a dream of oily odours and monstrous iron constructions, dominated by the grand foreman: and Edwin was in the dream. Hilda Lessways |Arnold Bennett 

Tressan was monstrous ill-at-ease, and his face lost a good deal of its habitual plethora of colour. St. Martin's Summer |Rafael Sabatini 

There are great and wonderful works: a variety of beasts, and of all living things, and the monstrous creatures of whales. The Bible, Douay-Rheims Version |Various 

The little name sounded so incongruous; it did not suit the big gaunt woman who had almost a touch of the monstrous in her. The Wave |Algernon Blackwood 

The one thing that loomed big in my mind's eye was the monstrous injustice of the accusation. Raw Gold |Bertrand W. Sinclair