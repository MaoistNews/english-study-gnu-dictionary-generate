For example, he quotes a study that found firstborns to be “more likely to be serious, sensitive,” “conscientious,” and “good” and—this is my favorite—“fond of books.” Does Birth Order Really Determine Personality? Here's What the Research Says |Lynn Berger |April 13, 2021 |Time 

Among Portuguese nobility in the 15th and 16th centuries, for example, second- and later-born sons were sent to the front as soldiers more often than firstborn sons. Does Birth Order Really Determine Personality? Here's What the Research Says |Lynn Berger |April 13, 2021 |Time 

Later on, these firstborns can become “shy, even fearful,” or they become “self-reliant, independent.” Does Birth Order Really Determine Personality? Here's What the Research Says |Lynn Berger |April 13, 2021 |Time 

Through no fault of his own, he’d missed out on the enviable position of firstborn. Does Birth Order Really Determine Personality? Here's What the Research Says |Lynn Berger |April 13, 2021 |Time 

Even a younger sibling in a multilingual family may experience language differently from a firstborn or an only child. Learning a second language early might have ripple effects throughout your life |Rahul Rao |February 9, 2021 |Popular-Science 

The christening of their firstborn Wednesday is a chance for William and Kate to honor and reward their most loyal buddies. Prince George’s Christening Will Reveal Kate and Will’s Closest, Most Loyal Pals |Tom Sykes |October 22, 2013 |DAILY BEAST 

“Omertà,” concedes the not-firstborn son Michael writing the book. Michael Hainey and Aleksandar Hemon’s Chicago Dreams |Chris Wallace |March 3, 2013 |DAILY BEAST 

The throne should go to the firstborn child, male or female. The Real Reason Why Kate Isn't Pregnant Yet? |Robert Lacey |May 17, 2012 |DAILY BEAST 

Foer began writing Eating Animals when his wife, the novelist Nicole Krauss, was expecting their firstborn son. Foer's Call to Arms |Jane Ciabattari |November 1, 2009 |DAILY BEAST 

DeMille showed three plagues—turning the Nile into blood, hail, and killing the firstborn sons. It's a Bird! It's a Plane! It's Moses! |Bruce Feiler |October 5, 2009 |DAILY BEAST 

Have mercy on thy people, upon whom thy name is invoked: and upon Israel, whom thou hast raised up to be thy firstborn. The Bible, Douay-Rheims Version |Various 

There was in his face the combined look of a man who sees the cradle and the coffin of his firstborn. The Master-Knot of Human Fate |Ellis Meredith 

When the father returned home, a near relative presented him with his firstborn son. Some Jewish Witnesses For Christ |Rev. A. Bernstein, B.D. 

The first intimation of this was given on that awful occasion when the firstborn of the Egyptians was slain. The Expositor's Bible: The Book of Joshua |William Garden Blaikie 

The mothers passion for her firstborn son was devotedly returned by him. Keats |Sidney Colvin