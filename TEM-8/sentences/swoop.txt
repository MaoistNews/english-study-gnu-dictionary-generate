Amazon says bright or dark lights can throw off the Show’s person-detection software, and it tries to focus on the center of the action without too many swoops that might look jarring for the person on the other end of the call. Amazon’s new rotating, follow-you camera is useful — and invasive |Geoffrey Fowler |February 26, 2021 |Washington Post 

This was a handy way to trigger various features, like clearing all notifications in one swoop. Apple Watch Series 6 first impressions: A stretchy addition looks great |Aaron Pressman |September 17, 2020 |Fortune 

The hard hat was to protect him should the parents swoop low. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

At the same time I was on an emotional upswing, a hyper-manic swoop and I was falling in love with my now-wife. The Author Of The Summer's Hit Paranoid Fantasy Opens Up |William O’Connor |August 15, 2014 |DAILY BEAST 

As you walk in front of the first screen, your silhouette appears and birds swoop down toward you. Frickin’ Laser Beams Run by Eyeballs: The Next Art Revolution Is Here |Nico Hines |July 7, 2014 |DAILY BEAST 

In one fell swoop, the Supreme Court has constrained government power, expanded corporate rights, and protected religious tyranny. Hobby Lobby: Sex, Lies, and Craft Supplies |Sally Kohn |July 2, 2014 |DAILY BEAST 

She needs to reassure Brody that all will be well—that the CIA will swoop in and iron everything out. ‘Homeland’ Finale Shocker: A Death in the Family |Andrew Romano |December 16, 2013 |DAILY BEAST 

At one fell swoop on the field of Jena, the famed military monarchy of the great Frederick fell in pieces like a potter's vessel. Napoleon's Marshals |R. P. Dunn-Pattison 

We always think of “eagle” when we think of “swoop,” but we do not often think of “swoop” when we think of “eagle.” Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

In a day or so they organise again, and swoop down on some other place, such as Belmont. Campaign Pictures of the War in South Africa (1899-1900) |A. G. Hales 

But Charlie didn't triumph, neither did he swoop; we watched carefully until we saw that Charlie was going astern! Yachting Vol. 2 |Various. 

There is something in the very words "flying column" to appease the impatient; wings in the air, a swoop upon the victim. The Relief of Mafeking |Filson Young