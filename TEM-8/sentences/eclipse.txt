If we were lucky, a planet might pass between us and its star, creating something like a miniature eclipse. My satellite would fit in a small suitcase. |Katie McLean |December 18, 2020 |MIT Technology Review 

Darkness defined the most awesome and most feared of astrological events, a total eclipse of the sun, and inspired some of the greatest advances in the history of science. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

Eclipsed by an Erupting CometAnything that passes in front of the sun can create an eclipse. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

During the lunar eclipse, Hubble examined sunlight that had passed through Earth’s atmosphere and reflected off of the moon for signatures of ozone. Hubble watched a lunar eclipse to see Earth from an alien’s perspective |Maria Temming |August 17, 2020 |Science News 

While Earth was between the sun and moon for a lunar eclipse in January 2019, the Hubble Space Telescope observed how chemicals in Earth’s atmosphere blocked certain wavelengths of sunlight from reaching the moon. Hubble watched a lunar eclipse to see Earth from an alien’s perspective |Maria Temming |August 17, 2020 |Science News 

Once 2007 rolled along, Kardashian's Ray J sex tape catapulted her to fame, helping her eclipse her former employer. Kim Kardashian’s Days as Paris Hilton’s Lowly Assistant |Amy Zimmerman |May 27, 2014 |DAILY BEAST 

That's what The Twilight Saga: Eclipse sounds like when it's up to the clever Bad Lip Reading folks. Viral Video of the Day: 'Twilight 3' Bad Lip Reading |The Daily Beast Video |April 25, 2014 |DAILY BEAST 

He wanted to be a big nova that would eclipse everything.... That was the only thing that would satisfy Andy. We Can’t Stop Talking About Andy Warhol |Bob Colacello |March 9, 2014 |DAILY BEAST 

He wanted to be a big nova that would eclipse everything. . . . That was the only thing that would satisfy Andy. Why We Can’t Stop Talking About Andy Warhol |Bob Colacello |March 9, 2014 |DAILY BEAST 

Tracking is for an opening weekend that could eclipse $100 million. Is There Really a Superman Curse, and Can Henry Cavill Break It? |Kevin Fallon |June 13, 2013 |DAILY BEAST 

But you, so formed to shine—to eclipse all others—do you never dance, seorita? Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

He held all the records for height, and it was known that at Attercliffe he meant to endeavour to eclipse his own achievements. Uncanny Tales |Various 

The architecture and paintings also indicate, with the increase of wealth and luxury, the decline and fatal eclipse of art. The Catacombs of Rome |William Henry Withrow 

Much of the architecture, however, is debased, indicating the decline and eclipse of art in the fifth or sixth century. The Catacombs of Rome |William Henry Withrow 

The expression of the face also underwent a change—a dire eclipse of woe—no less painful to behold. The Catacombs of Rome |William Henry Withrow