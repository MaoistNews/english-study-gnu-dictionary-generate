Avoid thick covers with flannel linings, as these can attract mice. Optimal Grilling Accessories and Grilling Tools For The Best Backyard BBQ |Irena Collaku |June 25, 2021 |Popular-Science 

Casually dressed in jeans and a flannel shirt, she arranges a couple of chairs and a slide projector. A mix of laughs and sadness in ‘2.5 Minute Ride’ |Patrick Folliard |May 27, 2021 |Washington Blade 

Cotton flannels also provide additional protection by absorbing moisture from breath, Vicenzi and colleagues report March 8 in ACS Applied Nano Materials. Microscopic images reveal the science and beauty of face masks |Emiliano Rodríguez Mega |April 2, 2021 |Science News 

He was wearing a blue plaid flannel shirt with more than one pen in the pocket, and he was both quick to smile and to qualify a point during our conversation. The Charmed Life of Frank Wilczek - Issue 98: Mind |Nell Freudenberger |March 17, 2021 |Nautilus 

Leather gloves can be lined in cashmere, flannel or fleece materials. Best winter gloves: Our picks for touch screen gloves, ski gloves, and more |PopSci Commerce Team |February 2, 2021 |Popular-Science 

Have you met the lumbersexual: all beards, flannel shirts, and work boots? How Straight World Stole ‘Gay’: The Last Gasp of the ‘Lumbersexual’ |Tim Teeman |November 12, 2014 |DAILY BEAST 

Until then, men shall all wearily grow our facial hair, wear flannel, and confuse the hell out of each other out on the streets. How Straight World Stole ‘Gay’: The Last Gasp of the ‘Lumbersexual’ |Tim Teeman |November 12, 2014 |DAILY BEAST 

It will prove so helpful, according to one radio flannel-mouth, that it was likely an act of strategic planning. Stop the Baby Clinton Madness |Jeff Greenfield |April 21, 2014 |DAILY BEAST 

We cared about your formerly firm stance against women who have Uncle Jessie mullets and wear flannel! The Tithe Is High: End the GOP Civil War! |Bill Schulz |January 11, 2014 |DAILY BEAST 

There's a bearded, flannel-shirted movement afoot to ditch the Internet and reclaim the pre-digital life. Dave Eggers, Arcade Fire and Other Hipsters Shun the Internet |James Poulos |September 22, 2013 |DAILY BEAST 

Behold a dumpy, comfortable British paterfamilias in a light flannel suit and a faded sun hat. God and my Neighbour |Robert Blatchford 

His hat was pushed back from his forehead, the collar of his blue flannel shirt was open. The Bondboy |George W. (George Washington) Ogden 

She was in a soiled dressing gown of purple flannel, with several of the buttons off. Rosemary in Search of a Father |C. N. Williamson 

A handkerchief, once red, with polka spots, contained a ragged flannel shirt and a stocking-heel tied with a piece of tape. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward 

A piece of red flannel was wrapped around Mademoiselle's throat; a stiff neck compelled her to hold her head on one side. The Awakening and Selected Short Stories |Kate Chopin