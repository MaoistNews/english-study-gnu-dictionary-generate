Reader Orlando Saint-Sebastien was surprised to learn that snails have tongues and asked how sticky they are. Readers ask about Cuvier’s beaked whales, microbes in the atmosphere and more |Science News Staff |January 10, 2021 |Science News 

They had the same shape as venom proteins from spiders and cone snails, Vetter says. Please do not touch the Australian stinging tree |Rebecca E. Hirsch |November 25, 2020 |Science News For Students 

She put the snails in her water tunnel and observed how well they were able to cling to different surfaces. Student scientists work to help all of us survive a warmer world |Bethany Brookshire |October 21, 2020 |Science News For Students 

After Glanzman’s team administered a “reminder” shock to the snails, the researchers were surprised to quickly notice different, newer synaptic connections growing between the neurons. Memories Can Be Injected and Survive Amputation and Metamorphosis - Facts So Romantic |Marco Altamirano |October 20, 2020 |Nautilus 

With it, researchers have edited genes in a wide variety of animals, including dogs, mice, snails, cows and mosquitoes. 2020 chemistry Nobel goes for CRISPR, the gene-editing tool |Tina Hesman Saey |October 8, 2020 |Science News For Students 

Most days, I might as well be studying some obscure species of sea snail. The $1-Billion-a-Year Right-Wing Conspiracy You Haven’t Heard Of |Jay Michaelson |September 25, 2014 |DAILY BEAST 

The Daily Pic: James Nares slows Manhattan's rat race to a snail's pace. New York on Quaaludes |Blake Gopnik |April 23, 2013 |DAILY BEAST 

The way to understand memory processing is not through Marcel Proust, as Kandel discovered in the 1960s, but through a sea snail. Nobel Winner Eric Kandel: ‘The Age of Insight,’ Memory, the Holocaust, and the Art of Vienna |Jimmy So |April 1, 2012 |DAILY BEAST 

The green damp hung upon the low walls, and the tracks of the snail and slug glistened in the light; but all was still as death. Oliver Twist, Vol. II (of 3) |Charles Dickens 

They were well-to-do folk and, according to Cesar Birotteau who knew them, old man Crottat was as "close as a snail." Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

As a snail which melteth, let every one of them pass away, like the untimely birth of a woman, that they may not see the sun. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

Aye, there it was, slowly winding up the steep white road, on which it seemed to move at a snail's pace. Ruth |Elizabeth Cleghorn Gaskell 

And in some respects that something that looked so very much like a railway resembled not so much a snail as a snake. Mushroom Town |Oliver Onions