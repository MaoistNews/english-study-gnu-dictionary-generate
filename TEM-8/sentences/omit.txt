You can’t work here if you cannot lift a certain amount of weight, so I think she may have omitted information about her injury to get hired. I can’t trust my new co-worker, and I have a list of reasons. How do I tell management? |Karla Miller |February 11, 2021 |Washington Post 

Every question includes a space for your name but please feel free to omit your name and type a subject line or the question’s topic. Q&A: Ask The Post your coronavirus questions. From the vaccine rollout to variants. |Julie Zauzmer, Fenit Nirappil |February 1, 2021 |Washington Post 

Current climate models tend to omit this thinning effect, she says. Ship exhaust studies overestimate cooling from pollution-altered clouds |Carolyn Gramling |February 1, 2021 |Science News 

Other states’ vaccine plans indicate they will omit some people — smokers in some cases, or pregnant people, or people with only one co-morbidity. All overweight D.C. residents will get priority for the coronavirus vaccine. Experts are skeptical. |Julie Zauzmer |January 21, 2021 |Washington Post 

The main benefit of this method, which will obviously involve a lot of precision maneuvering, is that it means SpaceX can save both cost and weight by omitting landing legs from the Super Heavy design altogether. Elon Musk says SpaceX will attempt to recover Super Heavy rocket by catching it with launch tower |Darrell Etherington |December 30, 2020 |TechCrunch 

This came across in the Showtime Omit the Logic documentary—in which you were a commentator—and it comes across here. How Richard Pryor Beat Bill Cosby and Transformed America |David Yaffe, Scott Saul |December 10, 2014 |DAILY BEAST 

Sister Cristina's lyrics also omit such lines as, “Feels so good inside, when you hold me, and your heart beats, and you love me.” What Does a Pop-Star Nun Sing? Madonna’s ‘Like A Virgin,’ Of Course |Barbie Latza Nadeau |October 21, 2014 |DAILY BEAST 

Until fairly recently, Miller said that the Air Force used to allow its troops to omit the “so help me God” phrase. U.S. Air Force: Swear to God—or Get Out |Dave Majumdar |September 8, 2014 |DAILY BEAST 

Israeli history book fail to mention the Palestinian Nakba; Palestinians omit the Holocaust. Palestinian and Israeli Citizens Bypass Their Governments in Search for Peace |Evie M. Salomon |August 17, 2013 |DAILY BEAST 

He'd instead omit the flyers, stuff his pockets with as many brushes as they could hold, and sell them at the very first call. My Eulogy for My Father, Murray Frum |David Frum |May 31, 2013 |DAILY BEAST 

It would not be fair to omit the name of the first mould-maker who made the tumbler-mould in question. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

But being observed, one evening, to omit it, a gentleman reminded him that he had forgotten to toast his favorite lady. The Book of Anecdotes and Budget of Fun; |Various 

But for I was so pleyne, Arcyte, In alle my werkes, much and lyte; and omit was in l. 266. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

The tobacconist whom he thus favored was his under-treasurer, Hardham, whom no writer about snuff should omit to notice. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

This line is too long; I omit ful wel devysed, which is not in the original. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer