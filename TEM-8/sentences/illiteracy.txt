Hacks and ransomware attacks depend in part on data illiteracy. Data Drives the World. You Need to Understand It |Richard Stengel |October 20, 2021 |Time 

It’s a cynical effort to weaponize the illiteracy and the lack of knowledge in this country generally about race, racism and the law. Critical race theory was the hot topic on Fox News this summer. Not so much anymore. |Jeremy Barr |October 6, 2021 |Washington Post 

Through the game, players learn how media illiteracy can make all of us unwitting accomplices in spreading fake news. The Latest Weapon Against Fake News? Video Games |Charu Kasturi |January 27, 2021 |Ozy 

They suffer sky-high maternal mortality rates, illiteracy, and a daily struggle against violence and poverty. The Sad Hidden Plight of Child Grooms |Nina Strochlic |September 18, 2014 |DAILY BEAST 

It is all a result of segregated communities where illiteracy is rife and the men think they can get away with anything. The Psychology of Sex Slave Rings |Charlotte Lytton |August 31, 2014 |DAILY BEAST 

The community suffers from significant illiteracy, poverty, unemployment, and crime. Jewish Groups, American Rabbis Call on Israel to Withdraw Bedouin Displacement Plan |Mira Sucharov |November 19, 2013 |DAILY BEAST 

The greatest barrier to improvement is probably adult illiteracy. Eight Years After Hurricane Katrina, New Orleans Has Been Resurrected |Jason Berry |August 29, 2013 |DAILY BEAST 

Indeed the state of Kentucky, facing staggering rates of adult illiteracy, recently began these programs in early elementary. The Real Juvenile Offenders |Daniela Drake |June 22, 2013 |DAILY BEAST 

She made up the envelope to match and addressed it, with consistent illiteracy, to the head of the mission. Hilda |Sarah Jeanette Duncan 

There is no ground for an explanation of such errors 207 as these except laziness and grossest illiteracy. English: Composition and Literature |W. F. (William Franklin) Webster 

Other branches of the family bearing the surname had gone to seed and lapsed into illiteracy. The Code of the Mountains |Charles Neville Buck 

The illiteracy for all Negro children was 25 per cent, whereas the illiteracy for all white children was only 10.5 per cent. The Journal of Negro History, Volume 5, 1920 |Various 

Illiteracy which in 1863 equaled about 95 per cent of the Negro population has been decreasing rapidly since the Civil War. The Journal of Negro History, Volume 5, 1920 |Various