Behind their silk hats loom shadows of their immigrant forbears. The Magazine That Made—and Unmade—Politicians |Anthony Haden-Guest |November 2, 2014 |DAILY BEAST 

In this valley so far away from Syria, questions loom like mist drifting off the Caucasus. The Secret Life of an ISIS Warlord |Will Cathcart, Vazha Tavberidze, Nino Burchuladze |October 27, 2014 |DAILY BEAST 

Over these environmental problems loom arguably greater social ones. Welcome to the Billion-Man Slum |Joel Kotkin |August 25, 2014 |DAILY BEAST 

Two hours east of Dallas, sun-drenched granite cliffs loom high above the cloudy waters of Possum Kingdom Lake. The World Series of Cliff Diving Takes Itself Very Seriously |Hampton Stevens |June 29, 2014 |DAILY BEAST 

Jagged walls of rock, a palette of blacks and greys, loom over us. Want to Write a Book? Go to Iceland |Adam LeBor |May 26, 2014 |DAILY BEAST 

It was in full sight from the door of the little shanty in which Aunt Ri's carpet-loom stood. Ramona |Helen Hunt Jackson 

From above, through the ceiling, came the vibration of some machine at work, and the machine might have been the loom of time. Hilda Lessways |Arnold Bennett 

The labour of the spade and of the loom, and the petty gains of trade, he contemptuously abandoned to men of a lower caste. The History of England from the Accession of James II. |Thomas Babington Macaulay 

No well-wisher of India, no patriot dare look upon the impending destruction of the hand-loom weaver with equanimity. Third class in Indian railways |Mahatma Gandhi 

I will lie down and round me wrap The cool, black curtains of the gloom That night hath woven in her loom. Charles Baudelaire, His Life |Thophile Gautier