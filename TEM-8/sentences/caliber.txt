He’s that caliber, a fine actor who was dedicated to the craft and wasn’t there for the glamour. Glynn Turman on How Chadwick Boseman ‘Spills His Guts’ on Screen |Daniel Malloy |January 5, 2021 |Ozy 

We needed to play against some teams that are high-major caliber, so this is the first time, and we weren’t ready for it. Top-ranked Gonzaga shows No. 16 Virginia how far it has to go in a 98-75 drubbing |Gene Wang |December 27, 2020 |Washington Post 

I attended and was blown away by the caliber of content and people. From mentorship to friendship to love: What I learned from three investing giants |matthewheimer |November 10, 2020 |Fortune 

To your point, though, it’s a really exhausting sort of assignment to guard someone of the caliber of LeBron or even Butler for a whole game. The Keys To Winning The NBA Finals For The Lakers And Heat |Chris Herring (chris.herring@fivethirtyeight.com) |September 30, 2020 |FiveThirtyEight 

Lastly, you need to invest in some common caliber ammunition. A Prepper’s Secrets to Surviving What’s Left of 2020 |Eugene Robinson |September 21, 2020 |Ozy 

The caliber at which Sam and the entire crew preformed dubbed them professionals not students. Nitehawk Shorts Festival: ‘Brute,’ a Twisted Take on Playing in the Dark |Julia Grinberg |November 28, 2014 |DAILY BEAST 

It was later reported that the weapon was a BB gun that appeared to be a .45-caliber pistol. The 14 Teens Killed by Cops Since Michael Brown |Nina Strochlic |November 25, 2014 |DAILY BEAST 

Still, the school has gotten more difficult to get into and the caliber of student attending has improved, as a result. How The University of Wisconsin Badgers Are Bucking the Big Ten Ticket Flop |Brian Weidy |October 31, 2014 |DAILY BEAST 

Maintaining the high caliber of guest to which his viewers have become accustomed, Galifianakis welcomes Brad Pitt to the ferns. Brad Pitt and Louis C.K. Go ‘Between Two Ferns’ |Alex Chancey, The Daily Beast Video |October 23, 2014 |DAILY BEAST 

When the body of Johnson was exhumed, the medical examiner was acutely chagrined when six .22 caliber rounds were removed from it. The Myth of the Central Park Five |Edward Conlon |October 19, 2014 |DAILY BEAST 

It should be of rather large caliber, and have an opening in the tip and one or two in the side near the tip. A Manual of Clinical Diagnosis |James Campbell Todd 

Mr. Wilson handed a small caliber revolver each to Gale and Phyllis. The Adventure Girls at K Bar O |Clair Blank 

He merely leaned his shotgun against his thigh, reached around beneath his coat and produced a forty-five caliber revolver. Average Jones |Samuel Hopkins Adams 

Even a twenty-two caliber may prove effective at short range. The Call of the Beaver Patrol |V. T. Sherman 

It is generally understood that men of the largest caliber are in demand as presidents of technical colleges and universities. Proceedings of the Second National Conservation Congress |Various