For example, studies in the 1960s showed that fishermen tended to have warmer fingers than non-fishermen, but that again runs into the possibility that only people with good circulation can hack it in the profession. How Your Body Does (and Doesn't) Adapt to Cold |Alex Hutchinson |February 10, 2021 |Outside Online 

Even with a half-dozen other fishermen, there were two days when I never ran into another soul on the water. In Big Sky country, a pandemic-era fly-fishing getaway |Carl Fincke |January 21, 2021 |Washington Post 

With one finger he proudly gestured to himself, as a fisherman might while posing with the big catch. Many of the images from the Capitol riot showed the terror. One highlighted the smugness. |Monica Hesse |January 7, 2021 |Washington Post 

For now, the commercial fishermen who rely on salmon and the outdoorsmen and women who hunt in the Bristol Bay region—or dream of making the trip someday—can rest a little easier. North America’s biggest salmon run may no longer be in danger |By Alex Robinson/Outdoor Life |November 30, 2020 |Popular-Science 

This peaceful short film focuses on Wayan, a 90-year-old fisherman in Bali, Indonesia, who can no longer fish. Stream These 5 Highlights from the Banff Film Festival |Erin Berger |November 26, 2020 |Outside Online 

I wonder about the local fisherman; could they file a class action suit? Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 

Find a fisherman to take you out on the water at dusk to watch the natural pyrotechnics at their bellowing best. It’s a Big, Big World: Sights That Make You Feel Small |Lonely Planet |December 24, 2013 |DAILY BEAST 

He worked unstable jobs as a fisherman and a construction worker before he entered the piracy business. The Pirate Negotiator |Caitlin Dickson |November 14, 2013 |DAILY BEAST 

This fall, its rocket fire on Israel appears to have won greater freedom of movement for Gazan farmers and fisherman. Mahmoud Abbas Bid for U.N. Sanction of Palestine State Could Explode West Bank |Peter Beinart |November 26, 2012 |DAILY BEAST 

Gemma Hardy is the orphaned daughter of a Scottish woman and an Icelandic fisherman. Must Reads: Margot Livesey, Robert Walser and More |Wendy Smith, Malcolm Forbes, Jane Ciabattari |February 16, 2012 |DAILY BEAST 

It is almost unnecessary to add, that the porter had his share well paid, and that the fisherman got the full value for his prize. The Book of Anecdotes and Budget of Fun; |Various 

Inscription copied, Nov. 21, 1833, from a tombstone to a fisherman in Bathford churchyard. Notes and Queries, Number 177, March 19, 1853 |Various 

The steward is a fisherman—a fisherman being very useful as a weather prophet. The Recent Revolution in Organ Building |George Laing Miller 

At any rate, they petitioned Parliament for the removal of this too ardent fisherman, and he was sequestered accordingly. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Thereupon the fisherman rushed to save his cloak, and the fox bolted out at the unguarded door. King Robert the Bruce |A. F. Murison