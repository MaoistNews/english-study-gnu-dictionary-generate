The launch is all linen, and includes tops and bottoms and a house dress for women as well. The Best New Launches From Parachute, Omsom, and More |Daniel Modlin |July 30, 2021 |The Daily Beast 

He carefully began cutting through the old linen, and then stopped. It may be time for museums to return Egyptian mummies to their coffins |Purbita Saha |June 28, 2021 |Popular-Science 

Several nearby shops and restaurants contributed to the event by delivering plates and utensils, flowers, linens, and other decorations to spruce up the space. A fire halted a wedding. So a tiny island community came together to make a new one. |Sydney Page |June 24, 2021 |Washington Post 

The combination of gray and yellow has been used in linens and fabrics for several years, albeit in softer hues, so while the stronger hues have been updated for 2021, I don’t find them fresh or exciting anymore. The trendiest paint colors of 2021 |Valerie Blake |June 10, 2021 |Washington Blade 

At dinner, you’re sitting down at a table with linens and introducing yourself to the people next to you. 10 Great Hut-to-Hut Trips in the U.S. |Megan Michelson |May 31, 2021 |Outside Online 

Davis jumped over a 4-foot porch wall and ran into a house, where he and others crammed themselves into a linen closet. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

De la Renta did design some stuff for Kennedy too—notably, one perfect belted sheath in crisp white linen. How Oscar de la Renta Created First Lady Fashion |Raquel Laneri |October 21, 2014 |DAILY BEAST 

The edges of the elegant paper are crackled; the ink bled into the linen weave long ago and has not faded. How Gettysburg Did Not Unlock the Past |Laird Hunt |September 21, 2014 |DAILY BEAST 

He moves her with no more concern for her condition than if she were a bag of dirty linen left behind by housekeeping. Ray Rice Should Have Remembered His 'Kindness' Anti-Bullying Wristband |Michael Daly |September 10, 2014 |DAILY BEAST 

“Better to wash dirty linen in public than have it grow mildew in a dark cupboard,” a party insider told The Daily Beast. A Sleaze Civil War Engulfs Britain’s Liberal Democrats |Nico Hines |January 24, 2014 |DAILY BEAST 

They all wore very large linen collars and black cravats, which gave them a very serious appearance. Davy and The Goblin |Charles E. Carryl 

Sergeant Burton knelt down and gingerly laid his hand upon the stained linen over the breast of Sir Lucien. Dope |Sax Rohmer 

Most of the farmers wore linen dusters and broad straw hats, but their women had put on all their finery. Ancestors |Gertrude Atherton 

In one of ole Miss's bureau drawers was a large plain linen handkerchief which was never used. The Cromptons |Mary J. Holmes 

A lace collar will look as badly over a chintz dress, as a linen one would with velvet, though each may be perfect of its kind. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley