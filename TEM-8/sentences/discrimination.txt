No one should be at risk of discrimination simply because of who they are, whom they love, or the state in which they reside. How the Trump administration is getting around Bostock to allow anti-trans discrimination |Chris Johnson |September 16, 2020 |Washington Blade 

Former co-CEO Eileen Murray sued Bridgewater in July over her deferred compensation, and alleged gender discrimination in an ongoing battle over her departure package. The losses continue to pile up for hedge fund king Ray Dalio |Bernhard Warner |September 15, 2020 |Fortune 

They battled harassment, discrimination, and a wall of socio-political opposition. The CEO striving to make vintage, secondhand clothing as popular as fast fashion |Rachel King |September 6, 2020 |Fortune 

On August 31, Airbnb launched Project Lighthouse, an initiative meant to “uncover, measure, and overcome discrimination” on the home-sharing platform. What’s missing from corporate statements on racial injustice? The real cause of racism. |Amy Nordrum |September 5, 2020 |MIT Technology Review 

Observers told the Blade they think the audience is not LGBTQ voters, but suburban mothers who are fearful of discrimination against their children and friends. Facing Trump’s LGBTQ outreach, advocates hold firm on plan to show his record |Chris Johnson |September 2, 2020 |Washington Blade 

In Scandinavian countries this discrimination has been dramatically reduced. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

But most of this gap, say the researchers who carried out the study, is due to discrimination. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

After the Iranian Revolution, discrimination took on a sectarian flavor. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

In 2013, with help from corporations, the federal Employment Non-Discrimination Act finally passed in the Senate. Corporations Are No Longer Silent on LGBT Issues | |December 24, 2014 |DAILY BEAST 

And as bad as it might be for gay or lesbian people, the discrimination is markedly worse for transgender people. State of LGBT Rights: Married on Sunday, but Fired on Monday |Gene Robinson |December 14, 2014 |DAILY BEAST 

If we had shot 'em without discrimination, the cowards would have got bold, seein' that they weren't safer in rear than in front. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Let the student continue this comparison till he attains very nearly the brevity and discrimination displayed by Mr. Killick. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

In selecting him for this important post Napoleon showed that power of discrimination which contributed so greatly to his success. Napoleon's Marshals |R. P. Dunn-Pattison 

Dividends must be distributed among the stockholders without unjust discrimination. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The Unemployed Workmen Act carries this contrary policy of discrimination according to merit into the class of the able-bodied. English Poor Law Policy |Sidney Webb