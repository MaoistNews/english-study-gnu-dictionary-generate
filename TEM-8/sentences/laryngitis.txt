Laryngitis had kept him from those three, to which however, he had sent a stenographic friend so that the chain was unbroken. Philosophy 4 |Owen Wister 

Leeches were applied to the larynx and the trachea for bronchitis and laryngitis and for relieving the cough of phthisis. Bloodletting Instruments in the National Museum of History and Technology |Audrey Davis 

In bronchitis and laryngitis the heated leaves are applied over the chest or neck after rubbing the parts with oil. The Medicinal Plants of the Philippines |T. H. Pardo de Tavera 

Chronic Laryngitis usually occurs as a result of repeated attacks of the acute form. Encyclopaedia Britannica, 11th Edition, Volume 16, Slice 2 |Various 

Oedematous Laryngitis is a very fatal condition, which may occur, though rarely, as a sequence of acute laryngitis. Encyclopaedia Britannica, 11th Edition, Volume 16, Slice 2 |Various