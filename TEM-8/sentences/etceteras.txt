There is only half of me here—the etceteras are in the family vault in Scotland. A Book of Ghosts |Sabine Baring-Gould 

All these etceteras are superfluous and unwholesome, and entail extra plates and additional trouble to everybody. Household Organization |Florence Caddy 

We shall need riding-habits, and summer things, and evening-dresses, and hosts of etceteras. The Fortunes of the Farrells |Mrs. George de Horne Vaizey 

So away we went, I in a small boat with a few boys, the others in another boat with the etceteras. West African studies |Mary Henrietta Kingsley 

Well, those ugly etceteras of an ugly habit are a fitting commentary upon the Church. The American Egypt |Channing Arnold