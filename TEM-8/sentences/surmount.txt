The good news is that every state seems to have surmounted the wave and new cases are now generally on the decline. COVID-19 cases aren’t dropping anymore |Erin Fennessy |March 2, 2021 |Popular-Science 

For Bullock in Gravity, all those miles above Earth, reconnecting with home means surmounting the death of a daughter. In Science Fiction, We Are Never Home - Issue 95: Escape |Steve Erickson |February 10, 2021 |Nautilus 

As a consequence, Nina is placed in an impossible position, but one she is determined to surmount. Lesbian love story becomes thriller in ‘Two of Us’ |John Paul King |February 4, 2021 |Washington Blade 

Formulaic action scenes became riotous steeplechases as Zorro surmounted obstacles with somersaults and handsprings, sometimes pausing for a snack. Zorro at 100: Why the original swashbuckler is still the quintessential American action hero |Michael Sragow |January 1, 2021 |Washington Post 

Now he’s seeking his fourth ring, inching closer to Michael Jordan’s six — which, no matter how much he downplays it, is a hill he’s still trying to surmount. The NBA Finals Burst the Analytics Bubble |Joshua Eferighe |September 29, 2020 |Ozy 

And that is why, as veterans, the onus is on us to surmount it and bridge the divide. Divided in the Wake of Fort Hood |Thomas Gibbons-Neff |April 16, 2014 |DAILY BEAST 

His fiery enthusiasm swept aside all difficulties; his inventive genius ever showed him the way to surmount all obstacles. Napoleon's Marshals |R. P. Dunn-Pattison 

It was the personality of their general which had taught the French soldiers to surmount all difficulties. Napoleon's Marshals |R. P. Dunn-Pattison 

The true spirit of success is not to look at obstacles, but to keep the eye on the many ways in which to surmount them. Choice Readings for the Home Circle |Anonymous 

They are daunted by nothing they surmount walls and hedges, and enter enclosed gardens or inhabited houses. The Ontario Readers: The High School Reader, 1886 |Ministry of Education 

My mind was staggered with a view of the difficulties I had to surmount and the little interest I possessed. The Life of Horatio Lord Nelson |Robert Southey