The modern world has made it ever easier to detach ourselves from consequences and accountability. Humanity is stuck in short-term thinking. Here’s how we escape. |Katie McLean |October 21, 2020 |MIT Technology Review 

Alongside its emissions tracking software, the company has also expanded its microgrid offerings, which can be detached from larger electric grids and run on their own renewable energy sources, for example solar panels. This electricity giant’s stock is booming as the corporate world gets serious about its ‘net zero’ goals |kdunn6 |October 20, 2020 |Fortune 

The predator’s fossilized body and head are detached from one another. This ancient reptile’s last meal may have truly been a killer |Maria Temming |October 9, 2020 |Science News For Students 

It’s not that apartments are magically greener than single-family detached homes. Is New York City Over? (Ep. 434) |Stephen J. Dubner |October 8, 2020 |Freakonomics 

Although touted as multipurpose, these OXO kitchen shears aren’t loaded with special features—just a simple pair of sharp blades that can be detached, and an herb stripper nestled between two padded and comfortable handles. These pieces of kitchen gear make excellent gifts |PopSci Commerce Team |October 6, 2020 |Popular-Science 

The bond between women and detachable showerheads, after all, is both a marriage of convenience and one of love. I Tried Cosmo’s New Lesbian Sex Tips |Samantha Allen |November 18, 2014 |DAILY BEAST 

Robert Wright proposes limiting firearms to six bullets and banning detachable magazines. Should We Cap Guns at Six Bullets? |Justin Green |December 17, 2012 |DAILY BEAST 

This is, in its simpler forms, a stiff, detachable tube from fifteen to twenty feet long and about four inches in diameter. The Wonder Book of Knowledge |Various 

The interiors are gilt, often furnished with detachable plates and sometimes set with brilliants. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

A detachable, padded support can be easily made at home for placing on any low-back chair and used as a head rest. The Boy Mechanic, Book 2 |Various 

His patent detachable lock, however, had disappeared, like the jewels. The Second Latchkey |Charles Norris Williamson and Alice Muriel Williamson 

Needless to say, the holder should be entirely of metal, and the wire cage detachable and easily renewed. The Elements of Bacteriological Technique |John William Henry Eyre