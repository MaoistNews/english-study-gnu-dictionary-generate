If you’re not camping solo, you can also take advantage of a camping partner to keep you both warm. How to stay warm while sleeping in the frigid outdoors |Alisha McDarris |February 12, 2021 |Popular-Science 

This year, dumpling-making might be more of a solo activity in my kitchen, but I take comfort in knowing that as I wrap my dumplings, home cooks around the world are doing the same as they prepare for the Lunar New Year. Homemade dumplings bring me closer to family this Lunar New Year, even from far away |Marian Liu |February 5, 2021 |Washington Post 

Not only does it help improve reflex, balance, and hand-eye coordination, it’s a game that can be played socially or solo. Advanced tables to enhance your ping pong game |PopSci Commerce Team |February 5, 2021 |Popular-Science 

I agree with this so strongly that I’m doing the race solo, skinning up and then down Sunlight Mountain as many times as I can in 24 hours. Small ski areas, big impact |Dina Mishev |February 5, 2021 |Washington Post 

Jordan prepares a bit differently these days for solo travel with her son, now 5, and 2-year-old daughter by carrying passports, birth certificates and a letter of consent from her spouse. If you’re a solo parent traveling internationally with your kids, be ready for this question |Gina Rich |February 4, 2021 |Washington Post 

Aaron Paul may play a young Han Solo in the first Star Wars spin-off. Juiciest ‘Star Wars: The Force Awakens’ Rumors (and Some Debunked Ones) |Rich Goldstein |January 3, 2015 |DAILY BEAST 

I was bored, but I grabbed a red Solo cup, filled it with beer, and stayed with my group, chatting with the brothers about Jim. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

To break her self-destructive cycle and heal, she decides to hike 1,100 miles of the Pacific Crest Trail solo. Exclusive: The Making of Reese Witherspoon’s Golden Globe-Nominated ‘Wild’ |Marlow Stern |December 12, 2014 |DAILY BEAST 

He started out with solo flights, but in this session over the desert outside Dubai he really pushes the envelope. Daredevil in a Jetpack Flies Alongside A Plane |Jack Holmes, The Daily Beast Video |December 12, 2014 |DAILY BEAST 

During the immediate protests for Michael Brown I walked in the crowd solo and mostly silent. The Unbearable Whiteness of Protesting |Rawiya Kameir, Judnick Mayard |December 10, 2014 |DAILY BEAST 

The solo voice of the Nubian sailor was lost in the chorus of voices which came floating over the Nile. Bella Donna |Robert Hichens 

The Solo organ and one-third of the Pedal organ are under the first arch on the north side of the chancel. The Recent Revolution in Organ Building |George Laing Miller 

The Altar organ, which can be played through the Solo organ keys, is under the second arch on the north side of the chancel. The Recent Revolution in Organ Building |George Laing Miller 

There was a ceaseless chorus of distant machinery, and above it rose the grinding and rattling solo of a steam winch. Dope |Sax Rohmer 

Solo and choral singing were to be taught with special regard to dramatic expression. Frederick Chopin as a Man and Musician |Frederick Niecks