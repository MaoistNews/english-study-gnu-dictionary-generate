France’s wealthiest man spoke of Tiffany as an American icon and expressed his “intense respect and admiration” for the business. LVMH could be nearing a deal to revive its bid for Tiffany & Co. |Rachel King |October 28, 2020 |Fortune 

They have a deep and sometimes pathological need for admiration and status—in fact, they may feel entitled to it. How a narcissistic leader infects company culture |Lila MacLellan |October 21, 2020 |Quartz 

Improvisation is a highly complex form of creative behavior that justly inspires our awe and admiration. The Neurology of Flow States - Issue 91: The Amazing Brain |Heather Berlin |October 14, 2020 |Nautilus 

Others placed hand written messages next framed photos of the late-Supreme Court justice expressing their love and admiration for her. Ginsburg ‘was a hero for us’ |Lou Chibbaro Jr. |September 19, 2020 |Washington Blade 

I once managed to land a job offer by casually referencing my admiration for Leonard Cohen during the interview. The best job interview question, according to Angela Duckworth of “Grit” fame |Lila MacLellan |August 21, 2020 |Quartz 

Others earn our admiration because they belong more to a particular moment. Renaissance Man Jared Leto Defies Categorization |The Daily Beast |December 8, 2014 |DAILY BEAST 

The pride and admiration Vial has for the artists who put on Cirque du Soleil is evident. A Backstage Love Affair With Cirque du Soleil |Allison McNearney |December 1, 2014 |DAILY BEAST 

He made no bones about his great admiration for FDR, who was his mentor, and he had roots too in the Truman administration. This Republican Loved Taxes & Modern Art |Scott Porch |November 19, 2014 |DAILY BEAST 

In the ruling, she expressed her admiration for Ms. Fitzmaurice. U.K. Courts Grant Mother Right to End Her 12-Year-Old Disabled Daughter’s Life |Elizabeth Picciuto |November 4, 2014 |DAILY BEAST 

To his close friends, Picasso did not hide his admiration for the Iberian sculptures. Did Picasso Try to Steal the Mona Lisa? |Nick Mafi |October 23, 2014 |DAILY BEAST 

One of the first out-goings of admiration towards form is the child's praise of "tiny" things. Children's Ways |James Sully 

Admiration for brilliant colours, for moving things, such as feathers, is common to the two. Children's Ways |James Sully 

The mother played her accompaniments and at the same time watched her daughter with greedy admiration and nervous apprehension. The Awakening and Selected Short Stories |Kate Chopin 

His unbounded generosity won for him the admiration of all his race, who graciously recognized him as their Maguinoó. The Philippine Islands |John Foreman 

Mrs. Pell was a very elegant and accomplished woman; her manners were the theme of universal admiration in our neighbourhood. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens