Don’t worry, though—with basic research skills and a solid strategy in mind, you’ll be able to sift through the noise and turn your new addiction into a more healthy habit. Use TikTok to build the perfect workout |Sandra Gutierrez G. |September 17, 2020 |Popular-Science 

Still, Morris has gotten some attention by pitching himself as “the great American comeback story” — beating an opioid addiction to turn his life around. The Trailer: The First State goes last |David Weigel |September 15, 2020 |Washington Post 

If you have an addiction problem, you shouldn’t put someone in a situation where they might be tempted. Momofuku’s David Chang on the big changes the restaurant industry needs to make to survive |Beth Kowitt |September 14, 2020 |Fortune 

They’re areas that light up in response to images of food when a person is hungry, or to drug-related images in people with addiction. Why do you feel lonely? Neuroscience is starting to find answers. |Amy Nordrum |September 4, 2020 |MIT Technology Review 

Experts say addiction has led men to spend money on khat that could have been used for their children’s education or to upgrade the quality of their family’s life. How the Pandemic Is Saving Lives in the Horn of Africa |Eromo Egbejule |September 3, 2020 |Ozy 

What made you want to write a memoir now about your “addiction” to film? Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

I was talking to one of my friends, who had just recently gotten over a drug addiction, who she tried to talk to about this case. The Deal With Serial’s Jay? He’s Pissed Off, Mucks Up Our Timeline |Emily Shire |December 31, 2014 |DAILY BEAST 

Methamphetamine addiction is central to more than one story. This Week’s Hot Reads: December 22, 2014 |Mythili Rao |December 22, 2014 |DAILY BEAST 

I had graduated NYU just a few years earlier and begun a career in publishing, but the addiction got the best of me. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

The lure and addiction of gaming—which went back to pinball, of course—became a sensation with Asteroids. ‘Asteroids’ & The Dawn of the Gamer Age |David Owen |November 29, 2014 |DAILY BEAST 

It is most frequent among those whose addiction to alcohol for years has caused repeated paroxysms of delirium tremens. Essays In Pastoral Medicine |Austin Malley 

But non-drug addiction is a major crime against the state of Omega. The Status Civilization |Robert Sheckley 

Bagsby was a punchy man, with a bald head, and a nose which betokened his habitual addiction to the fiery grape of Portugal. Tales from Blackwood |Various 

Addiction to the practice of occult arts had evidently become general in the now semi-orientalized city. Encyclopaedia Britannica, 11th Edition, Volume 9, Slice 6 |Various 

The addiction to sports, therefore, in a peculiar degree marks an arrested development of the man's moral nature. The Theory of the Leisure Class |Thorstein Veblen