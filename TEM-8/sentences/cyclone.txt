Very warm ocean waters, such as in the Atlantic Ocean this year, foster tropical cyclone formation. Wildfires, heat waves and hurricanes broke all kinds of records in 2020 |Carolyn Gramling |December 21, 2020 |Science News 

That’s because the intense winds of cyclones feed on moisture and heat picked up from the warm waters, and warmer air can also hold more moisture. Once hurricanes make landfall, they’re lingering longer and staying stronger |Carolyn Gramling |November 11, 2020 |Science News 

In its annual report, India’s central bank stated that the country is witnessing more intense droughts, downward shifts in average rainfall as well as a higher frequency of cyclones. How climate change will hurt India’s already wounded economy |Prathamesh Mulye |September 8, 2020 |Quartz 

The strength of a tropical cyclone is defined by its wind speeds. Let’s learn about hurricanes |Bethany Brookshire |July 15, 2020 |Science News For Students 

Even worse, rain can fall in extreme amounts, especially during hurricanes and cyclones. Let’s learn about rain |Bethany Brookshire |June 17, 2020 |Science News For Students 

Tropical cyclone Oswald has produced a blanketing of sea foam in parts of Queensland, Australia. A Giant Bubble Bath |Ilana Glazer |January 28, 2013 |DAILY BEAST 

The courtroom itself is surrounded by high cyclone fences, braided with coiled razor wire, and watched by heavily armed guards. Inside the Khalid Sheik Mohammed Hearing Circus |Terry McDermott |May 6, 2012 |DAILY BEAST 

On Ren, the guard, he descended like a young cyclone, with warnings for mademoiselle's safety and comfort. The Amazing Interlude |Mary Roberts Rinehart 

What a cyclone there is going to be to-morrow when this piece of paper gets to work! A Connecticut Yankee in King Arthur's Court, Complete |Mark Twain (Samuel Clemens) 

It is only a cyclone that seems to be able to overthrow a sound tree, and then it more commonly breaks its trunk than uproots it. Birds and All Nature, Vol. VI, No. 3, October 1899 |Various 

"Hope we're not in for a cyclone," says one of the men, appearing out of the smoking-room with a pipe in his mouth. Round the Wonderful World |G. E. Mitton 

When Molly arrived the next morning, she flew into the house like a small and well-wrapped-up cyclone. Marjorie's Busy Days |Carolyn Wells