Another possibility is that SARS-CoV-2 is impacting the heart muscle in a way that cardiologists aren’t accustomed to, absent some of the usual signs of inflammation and stress. College athletes show signs of possible heart injury after COVID-19 |Aimee Cunningham |September 11, 2020 |Science News 

As the elevator rose, Jody Potts wondered at the possibilities. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

The name you’ll hear thrown around as a possibility to replace him is Chris Paul, who has a pricey deal but is on an Oklahoma City team that may be ready to fully pivot toward its youth movement. The Bucks Played It Safe And Made The Wrong Kind Of History |Chris Herring (chris.herring@fivethirtyeight.com) |September 9, 2020 |FiveThirtyEight 

While the watch was not perfectly accurate in my case, it still opens up the possibility for wheelchair users to improve their health outcomes. Smart Watches Could Do More For Wheelchair Users |John Loeppky |September 4, 2020 |FiveThirtyEight 

Right now the project is in its first version, but Thio already sees more possibilities. Create your own moody quarantine music with Google’s AI |Karen Hao |September 4, 2020 |MIT Technology Review 

The possibility that the same outcome could happen another way -- namely a guy asks me out -- keeps me from taking action. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 

In a romantic relationship, facing humiliation or awkwardness is a strong possibility. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 

This suggests that places combining—but also maintaining—place and possibility have a lot to offer people. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

This is a testament to the fundamental human—and American—desire to combine place and possibility. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

But the way that Texas combines place and possibility will not be appealing to everyone. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

"Yes, as there seems no possibility of making any more mistakes on our way, you are free," replied the gravest of the two. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

All possibility of a general increase of wages depended on the relation of available capital to the numbers of the working men. The Unsolved Riddle of Social Justice |Stephen Leacock 

The Berlin exchange, while war was as yet only a dreaded possibility, rose from 20 m. 50 pf. Readings in Money and Banking |Chester Arthur Phillips 

Many of those who are not on pay escape to India and other regions, without any possibility of avoiding it. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

It was still in the verge of possibility that his son might seek his father in that dismal chamber. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter