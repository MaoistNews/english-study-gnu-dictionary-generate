After David learns a long-hidden secret, Ebenezer — quite the Dickensian rascal — conspires to have the lad tricked onto a brig sailing for the Carolinas, where he will be sold into indentured slavery. Robert Louis Stevenson’s ‘Kidnapped’ is not just an adventure tale, it’s a timely novel about politics and dissent |Michael Dirda |January 20, 2021 |Washington Post 

Seeing how they crack open a tasty crustacean or comparing how otters of different ages perform when it comes to varying tests will assist researchers in learning as much as they can about these fuzzy little rascals. These otters learn how to snag snacks by watching their friends |Sara Kiley Watson |November 24, 2020 |Popular-Science 

One of the last great rascal pols, Marion Barry left his mark—for good and ill—on Washington, D.C., and the country. Despite Crack and Graft, D.C. Loved ‘Hizzoner’ Marion Barry |Lloyd Grove |November 23, 2014 |DAILY BEAST 

Yes, Trainor managed to pen a few songs for Rascal Flatts, but she was more interested in crafting pop tunes. ‘All About That Bass’ Singer Meghan Trainor On Haters and Her Polarizing (and Unlikely) No. 1 Hit |Marlow Stern |October 7, 2014 |DAILY BEAST 

And that means these are the words a modern rascal uses to make us jump. Justin Bieber: Not a Racist, But Is He Really a N*****? |John McWhorter |June 5, 2014 |DAILY BEAST 

As her daughter Sara says, Eakin “ended up thinking that maybe Solomon was a bit of a rascal”. The Woman Who Saved Solomon |Guy Walters |October 20, 2013 |DAILY BEAST 

"He's a little bit of a rascal, I'll put it that way," he said. First Official Photos of Prince George Arrive |Nico Hines |August 19, 2013 |DAILY BEAST 

He thought so, at least, as he laughed and agreed with her, saying Tony was an unscrupulous rascal at the best of times. The Wave |Algernon Blackwood 

This rascal was owed a debt for the indignity he had offered the sahib in the village, and now he was paid in full. The Red Year |Louis Tracy 

The most myopic of creatures could have seen that Anne was foolishly in love with her rascal husband. The Joyous Adventures of Aristide Pujol |William J. Locke 

It would do little or no good to acquaint the constable with their suspicion that the rascal might be the man named Cameron Smith. The Mystery at Putnam Hall |Arthur M. Winfield 

I cant see your family suffer, for your wife is a nice woman, if you are a rascal! The Girls of Central High on the Stage |Gertrude W. Morrison