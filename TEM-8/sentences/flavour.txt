So there is nothing that starts with ‘Take 17 litres of stock…’ Everything in there is about flavour. Tom Parker Bowles on Camilla's Roast Chicken, His Cocaine Sting and Those Pictures of Kate |Tom Sykes |October 3, 2012 |DAILY BEAST 

There seemed the flavour of some strange authority in her that baffled all approach to the former intimacy. The Wave |Algernon Blackwood 

The time passed along quickly; the coffee was excellent, the cigars soft and of the nutty flavour he loved. Three More John Silence Stories |Algernon Blackwood 

To detect the flavour of an olive is no less a piece of human perfection than to find beauty in the colours of the sunset. The Pocket R.L.S. |Robert Louis Stevenson 

John Dory pulled at his cigar appreciatively, sniffed its flavour for a moment, and then leaned forward in his chair. The Double Four |E. Phillips Oppenheim 

There was often a brusqueness in her comings and goings, but she usually left a flavour of herself behind. Hilda |Sarah Jeanette Duncan