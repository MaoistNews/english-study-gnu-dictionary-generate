In this installment of the FiveThirtyEight Politics podcast, Cardozo School of Law professor and ABC News contributor Kate Shaw discusses that evidence and its legal ramifications. Politics Podcasts: The Meaning Of Democrats’ Impeachment Argument |Galen Druke |February 12, 2021 |FiveThirtyEight 

Tia Mitchell of The Atlanta Journal-Constitution joins this installment of the FiveThirtyEight Politics podcast to discuss what to expect. Politics Podcast: What Georgia Republicans’ Proposed Voting Restrictions Would Do |Galen Druke |February 8, 2021 |FiveThirtyEight 

In this installment of the Freakonomics Radio Book Club, she is interviewed by guest host Maria Konnikova. How to Be Better at Death (Ep. 450) |Maria Konnikova |February 4, 2021 |Freakonomics 

In this installment of Model Talk on the FiveThirtyEight Politics podcast, Nate Silver and Galen Druke look back at the results of those runoffs and discuss what the Democratic wins say about polling and what they mean for elections going forward. Politics Podcast: The Final Model Talk Of 2020 |Galen Druke |January 28, 2021 |FiveThirtyEight 

Mainstream news sources are reporting, and in some cases fueling, the latest installment of the “reading wars.” Is there really a ‘science of reading’ that tells us exactly how to teach kids to read? |Valerie Strauss |January 26, 2021 |Washington Post 

Follett is out this fall with his final installment of an historical fiction trilogy that races through the 20th century. Popular Novelist Ken Follett Is a Slightly Unlikely and Certainly Unsung Gay Icon |William O’Connor |October 1, 2014 |DAILY BEAST 

Well talking about big movies, you've been cast to play Apollo Creed's grandson in a new Rocky franchise installment. Michael B. Jordan: Playing a Black Superhero in 'Fantastic Four' Is a 'Huge Responsibility' |Kevin Fallon |September 28, 2014 |DAILY BEAST 

In a gush of pro-America, anti–Soviet Union glory, the fourth installment in the Rocky saga pulls out all the stops. 13 Most Patriotic Movies Ever: ‘Act of Valor,’ ‘Top Gun’ & More (VIDEO) |Melissa Leon |July 4, 2014 |DAILY BEAST 

The casting of the upcoming Star Wars: Episode VII is shaping up to be it's own, multi-installment odyssey. Lupita Nyong’o Joins ‘Star Wars: Episode VII’ Cast |Marlow Stern |June 2, 2014 |DAILY BEAST 

The two-time Oscar winner is the villain in the next installment of the Call of Duty video game series. Viral Video of the Day: Kevin Spacey Heeds the ‘Call of Duty’ |Alex Chancey |May 2, 2014 |DAILY BEAST 

What general ledger controlling accounts of the installment ledger are necessary? Cyclopedia of Commerce, Accountancy, Business Administration, v. 1 |Various 

For a time the installment of self-government thus granted satisfied the people. The Canadian Dominion |Oscar D. Skelton 

Very few letters will sell articles costing as much as fifty dollars unless perhaps the payments are on the installment plan. How to Write Letters (Formerly The Book of Letters) |Mary Owens Crowther 

Any postponement, total or partial, of any installment falling due after 1926 for a period exceeding three years. Harper's Pictorial Library of the World War, Volume XII |Various 

The first installment covers about the first twenty-one years of Lincoln's life, which were spent in Kentucky and Indiana. McClure's Magazine, January, 1896, Vol. VI. No. 2 |Various