Obviously we’re still figuring this out, but I wanted to note it here given the above paragraph. Stocks are selling off again, and SaaS shares are taking the biggest lumps |Alex Wilhelm |September 4, 2020 |TechCrunch 

Back in July, OpenAI’s latest language model, GPT-3, dazzled with its ability to churn out paragraphs that look as if they could have been written by a human. This know-it-all AI learns by reading the entire web nonstop |Will Heaven |September 4, 2020 |MIT Technology Review 

Digging through the previous paragraph, we just saw that after 48 hours there was a 25 percent chance of having two worms, a 50 percent chance of having three worms and a 25 percent chance of having four worms. Are You Hip Enough To Be Square? |Zach Wissner-Gross |August 14, 2020 |FiveThirtyEight 

You can convey something in an instant that would take a paragraph. Data visualizations turn numbers into a story |Nancy Shute |August 3, 2020 |Science News 

Use paragraphs, headings, and signal words to display your content nicely on your webpage, allowing for greater user experience. Five ways SEO and web design go together |Javier Bello |July 21, 2020 |Search Engine Watch 

Think about that for a second if after the preceding paragraph you remain convinced of the infallibility of our system. For Ricky Jackson, a Just Verdict—But 39 Years Too Late |Cliff Schecter |November 26, 2014 |DAILY BEAST 

It goes on like that for another half a paragraph, but you get the idea. David Mitchell’s ‘The Bone Clocks’ Is Fun But Mostly Empty Calories |William O’Connor |September 14, 2014 |DAILY BEAST 

For a writer who was a master of reduction, never one to linger on the passing view, this was an unusually effulgent paragraph. Is This Hemingway’s Pamplona or a Lot of Bull? |Clive Irving |July 13, 2014 |DAILY BEAST 

If the story fell apart by the first paragraph, it would not save itself by the end. Amy Tan: How I Write |Noah Charney |December 11, 2013 |DAILY BEAST 

Whitman is made to share a chapter, lumped in with Proust, Wilde, and Baudelaire, in which he is allotted a mere paragraph. John Sutherland‘s Enjoyable Little History of Literature |Malcolm Forbes |November 29, 2013 |DAILY BEAST 

So they often occured mid-paragraph; here they have been moved to a more appropriate place. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

I mark this by inserting a paragraph-mark ( ) at the beginning of each tern. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

If we get to the bottom of it, we shall find that the countess inspired the paragraph that the Evening Mercury had to-night. The Weight of the Crown |Fred M. White 

I would not have believed it; it came to me quite as a shock—that paragraph in the late Mercury. The Weight of the Crown |Fred M. White 

In the original draft of the instructions was a curious paragraph which, on second thoughts, it was determined to omit. The History of England from the Accession of James II. |Thomas Babington Macaulay