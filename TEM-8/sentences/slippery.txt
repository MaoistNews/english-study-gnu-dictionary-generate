That ability to vector torque towards traction should endow the Hummer EV with an unprecedented ability to keep moving forward through even the gnarliest of slippery conditions. A First Look at the 2022 GMC Hummer EV (It's Awesome) |Wes Siler |October 21, 2020 |Outside Online 

Charlotte opted to design a new, more slippery coating for a ship’s hull so that fewer creatures would be able to hitch a ride. Student scientists work to help all of us survive a warmer world |Bethany Brookshire |October 21, 2020 |Science News For Students 

People opposed to Measure E say nixing the height limit in Midway is the beginning of a slippery slope and could inspire other coastal neighborhoods to do the same. The Climate Problem Facing Coastal Building Height Limits |MacKenzie Elmer |October 20, 2020 |Voice of San Diego 

Opponents think the measure is a slippery slope to removing the height limit in other places. VOSD Podcast: The Local Ballot Measures, Explained |Nate John |October 16, 2020 |Voice of San Diego 

The fact that then-Justice Antonin Scalia died 269 days before that year’s presidential election, while Ginsberg passed away 46 days before this one, is just butter on the greased pole of slippery words. Donald Dossier: Seizing the Moment to Fill RBG’s Seat |Tracy Moran |September 19, 2020 |Ozy 

Historically, conservatives treated the minimum wage as an affront to free labor and a step on a slippery slope towards statism. To Make Their Victory Durable, the GOP Must Fix the Minimum Wage |Dmitri Mehlhorn |November 6, 2014 |DAILY BEAST 

Swiss leaders also dispel the “slippery slope” idea by repeatedly rejecting substantial minimum wage increases. To Make Their Victory Durable, the GOP Must Fix the Minimum Wage |Dmitri Mehlhorn |November 6, 2014 |DAILY BEAST 

Hers is a particular brand of essay: writing at its most crystal clear, subject matter at its most slippery and interesting. From Didion to Dunham, Female Essayists Seize the Day |Lucy Scholes |October 17, 2014 |DAILY BEAST 

The slippery slope argument is a way of keeping the hands-off-the-Internet-entirely philosophy going. Congress, Big Tech Fight Over Child Prostitution Bill |Tim Mak |October 6, 2014 |DAILY BEAST 

Which is why his efforts to justify his rabid consumption of football wind up feeling so slippery and convoluted. Forget the Wife Beating—Are You Ready for Some Football? |Steve Almond |September 11, 2014 |DAILY BEAST 

He will find that “Ice” is a concrete word, and “Slippery” indicates a quality of “Ice” and of other things. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

He must write down the first two words, “Ice” and “Slippery,” the latter word under the former. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

As Isabel walked carefully down the slippery stair she veiled her eyes to hide the wonder in them. Ancestors |Gertrude Atherton 

If these gentlemen had thought to avoid slippery ground, they should have elected to appoint the meeting elsewhere. St. Martin's Summer |Rafael Sabatini 

The floors and steps are wet and slippery with brine and with the blood of herrings dripping down from one floor to another. Skipper Worse |Alexander Lange Kielland