This victory would be impossible without the incredible work of hundreds of grassroots volunteers. Sarah McBride wins primary, likely to be first out trans state senator in U.S. |Chris Johnson |September 16, 2020 |Washington Blade 

Maskhane, a grassroots organization, brings together natural-language-processing researchers from Africa to bolster machine-translation work that has neglected nondominant languages. AI ethics groups are repeating one of society’s classic mistakes |Amy Nordrum |September 14, 2020 |MIT Technology Review 

The “Drag Ambassadors” have hosted dozens of grassroots events throughout the year to encourage their audiences to register and have a plan for voting. Virtual Drag Out the Vote event features ‘Drag Race’ alum |Steph Purifoy |September 11, 2020 |Washington Blade 

She had to promise them that her grassroots organization, Girls United for Human Rights, would arrange for the girl’s books and school uniform. The Teenager Breaking Up Child Marriages, Door to Door |Pallabi Munsi |September 2, 2020 |Ozy 

It started with a grassroots effort and is doing some wonderful things. Private Border Wall Fundraisers Have Been Arrested on Fraud Charges |by Perla Trevizo, Jeremy Schwartz and Lexi Churchill |August 20, 2020 |ProPublica 

The delegates were given envelopes containing $40,000 during a meeting about grassroots soccer initiatives. Is Soccer Great Lionel Messi Corrupt? |Jack Holmes |December 8, 2014 |DAILY BEAST 

Either way, this is a conversation that is occurring not just at the penthouse level, but among the grassroots as well. Rand Paul, Chris Christie Laid Out Plans for Black Voters at Penthouse Forum |David Freedlander |October 17, 2014 |DAILY BEAST 

Grassroots organizing accompanied an agenda of legislative sabotage led by the Republican congressional hierarchy. Why Bigotry Persists |Stephen Eric Bronner |September 28, 2014 |DAILY BEAST 

Then, Democratic strategists say, the grassroots were hungry for a real debate after eight years of George Bush. Does Team Hillary Want a Democratic Challenge? |David Freedlander |September 25, 2014 |DAILY BEAST 

The centrist group thinks they can build centrist grassroots army. No Labels’ Bid For The Big Time |Ben Jacobs |September 18, 2014 |DAILY BEAST 

His eye to the grassroots, Gramps had long ago understood that everything was as old as creation itself and yet was eternally new. The Black Fawn |James Arthur Kjelgaard 

I saw the indications of richness there, and, overturning the earth with my pick, found gold among the very grassroots. At the Time Appointed |A. Maynard Barbour 

Goals 2000 links world-class standards to grassroots reforms and I hope Congress will pass it without delay. Complete State of the Union Addresses from 1790 to 2006 |Various 

In my budget, I propose a grassroots campaign to help inform families about these medical risks. Complete State of the Union Addresses from 1790 to 2006 |Various 

DA enjoyed grassroots support especially by young professionals, businessmen and liberals. After the Rain |Sam Vaknin