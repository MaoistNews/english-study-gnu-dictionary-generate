At the time, Texas allowed only Christian and Muslim chaplains to attend executions. Supreme Court says Alabama cannot execute inmate without his pastor present |Robert Barnes |February 12, 2021 |Washington Post 

Stand-up comedy is still in a nascent stage in India, and Faruqui was among a small group of comics and an even smaller group of Muslim comics. How An Indian Stand Up Comic Found Himself Arrested for a Joke He Didn't Tell |Sonia Faleiro |February 10, 2021 |Time 

Indonesia is a majority Muslim country, but Aceh is the only province with Shariah law. Two men in Indonesia’s Aceh province caned for having sex |Michael K. Lavers |January 30, 2021 |Washington Blade 

Much of it is a backlash to immigration in general and Muslim communities in particular. Global Right-Wing Extremism Networks Are Growing. The U.S. Is Just Now Catching Up. |by Sebastian Rotella |January 22, 2021 |ProPublica 

If you think about it, the biggest mistake in a way in the partition of Pakistan and India was all the Muslim-majority areas became Pakistan, so the people who were left in India are scattered around. Fareed Zakaria on Putin, the Dalai Lama and Race in America |Nick Fouriezos |January 16, 2021 |Ozy 

Indeed, every teacher is expected to be a Muslim by birth or conversion. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Submission is set in a France seven years from now that is dominated by a Muslim president intent on imposing Islamic law. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

In 2009, a Pakistani Christian woman got into a religious argument with some Muslim women with whom she was harvesting berries. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

The governor of Punjab province, a Muslim man, called publicly for leniency for her. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

He sees himself as the first Muslim president of all Europe. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

From the Muslim schools of Arabia came the first teachers of modern science to Europe. Evolution of Life and Form |Annie Wood Besant 

He washed and went through the mummery of muslim prayers for the watchful mullah's sake, and climbed on to his bed. King--of the Khyber Rifles |Talbot Mundy 

A most important Muslim tradition says that Muhammad declares this sura to be equal to a third of the rest of the Koran. The Worlds Greatest Books, Volume XIII. |Various 

Said by Muslim commentators to be one of the last ten nights of Ramadhan, the seventh of those nights reckoning backwards. The Worlds Greatest Books, Volume XIII. |Various 

Muslim commentators hold that the Paraclete (Comforter) promised in John xvi. The Worlds Greatest Books, Volume XIII. |Various