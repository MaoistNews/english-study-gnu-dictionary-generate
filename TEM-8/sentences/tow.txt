Her car was ordered towed by clinic police, costing more than $200, she said. The Startling Reach and Disparate Impact of Cleveland Clinic’s Private Police Force |by David Armstrong |September 28, 2020 |ProPublica 

Ring Car Alarm plugs into your car and monitors activities like break-ins or tows and then sends an alert to the app. Amazon’s Fall 2020 products announcements include a security camera drone that flies around your house |Stan Horaczek |September 24, 2020 |Popular-Science 

It has eight reinforced foam handles, a zippered valve cover, and a reinforced tow point for a safe and secure ride. The best boating tubes |PopSci Commerce Team |September 3, 2020 |Popular-Science 

It was the longest the two of them had ever spent together, certainly the longest without their husbands in tow. Gender Is What You Make of It - Issue 88: Love & Sex |Charles King |August 5, 2020 |Nautilus 

So far, Spot’s been mostly trialed in surveying and data collection, but as this video suggests, string enough Spots together, and they could tow your car. The Robot Revolution Was Televised: Our All-Time Favorite Boston Dynamics Robot Videos |Jason Dorrier |July 19, 2020 |Singularity Hub 

Trump even gave Jackson a personal tour of the venue, with television cameras in tow. I Watched a Casino Kill Itself: The Awful Last Nights of Atlantic City’s Taj Mahal |Olivia Nuzzi |December 8, 2014 |DAILY BEAST 

The driver then got on the highway and started going "well above the speed limit," with the taxi inspector still in tow. The Ten Worst Uber Horror Stories |Olivia Nuzzi |November 19, 2014 |DAILY BEAST 

The rebels used, among other weapons, TOW missiles recently supplied by the U.S. to Harakat Hazm. The Battle for Aleppo: A Decisive Fight for ISIS, Assad, and the USA |Jamie Dettmer |October 25, 2014 |DAILY BEAST 

The police vehicles take off from the parking lot with Booker and Fulop in tow. The Ugly Truth About Cory Booker, New Jersey’s Golden Boy |Olivia Nuzzi |October 20, 2014 |DAILY BEAST 

Send Obama to the American Medical Association and the National Medical Association with Murthy in tow to make the case. Where the Hell Is the Surgeon General? |Roland S. Martin |October 9, 2014 |DAILY BEAST 

They require frequent cleaning with a long wire and a bit of tow, and in some large towns there are professional pipe-cleaners. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The boat was now hoisted out and sent ahead to tow, but we could not succeed in getting the vessel's head round. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Again it was empty except for the operator, a tow-headed kid with a Racing Form tucked in a side pocket. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Without her powerful engines to tow it to windward of the wrecks the lifeboat would be much, very much, less useful than it is. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

These matches were fuses of some slow-burning fiber, like tow, which would keep a spark for a considerable time. The Wonder Book of Knowledge |Various