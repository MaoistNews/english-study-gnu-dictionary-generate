Perovskite inks are deposited onto glass or plastic to make extremely thin films—around one hundredth of the width of a human hair—made up of metal, halide, and organic ions. How a New Solar and Lighting Technology Could Propel a Renewable Energy Transformation |Sam Stranks |September 3, 2020 |Singularity Hub 

Yamagishi and his colleagues suggest that a colony twice that thick—roughly the width of a dime—could survive up to eight years in space. Clumps of bacteria could spread life between planets |Paola Rosa-Aquino |August 27, 2020 |Popular-Science 

About the size of a small TV at 21 inches in width, this dishwasher does the job by easily connecting to your kitchen faucet. Small dishwashers that fit in almost any kitchen |PopSci Commerce Team |August 25, 2020 |Popular-Science 

Much like long bone growth, pelvis width is driven largely by estrogen levels. Males Are the Taller Sex. Estrogen, Not Fights for Mates, May Be Why. |Christie Wilcox |June 8, 2020 |Quanta Magazine 

Tiny channels about the width of a human hair collect sweat. Sweat tech alerts athletes when to rehydrate — and with what |Silke Schmidt |May 21, 2020 |Science News For Students 

Each side of the triangle will measure five miles in width, a foot in depth, and nearly 307 miles in length. The 2013 Novel of the Year Is… |Nathaniel Rich |December 30, 2013 |DAILY BEAST 

Some of us fill it out more width-wise than length-wise, so I always get ring around the cock. Condoms Aren’t Sexy. This Is How Porn Stars Would Fix Them. |Aurora Snow |November 30, 2013 |DAILY BEAST 

Two thick palm trunks lie across its width and its concrete block walls have tumbled to the ground. Typhoon Haiyan: The Philippine Village that Lost Its Men |The Telegraph |November 17, 2013 |DAILY BEAST 

The grander the occasion, the larger the width of the hoop petticoat. In Paris, a Historical Look at Lingerie at Musée des Arts Décoratifs |Sarah Moroz |July 5, 2013 |DAILY BEAST 

The width of the surrounding streets allows the Barclays Center to stand in relief as the alien presence it is. The 6 Best Quotes About the Barclays Center |William O’Connor |May 16, 2013 |DAILY BEAST 

About three o'clock, as nearly as I could tell, we dipped into a wooded creek bottom some two hundred yards in width. Raw Gold |Bertrand W. Sinclair 

It was not until 1842 when part of the Royal Hotel stables were taken down, that it was made its present width. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Small veins, rarely exceeding half an inch in width, the fibres not easily separable. Asbestos |Robert H. Jones 

The man on the girl's right seemed to overlap her possessively which could have been accounted for by the width of his shoulders. Hooded Detective, Volume III No. 2, January, 1942 |Various 

She found herself in a large saloon, which took in the whole width of the stern of the dahabeeyah. Bella Donna |Robert Hichens