Earlier this month, anti-vaccine protesters surprisingly focused on Australia’s vaccine mandate in a protest outside its consulate in New York City. U.S. Conservatives Want to Save Australia From ‘COVID Tyranny.’ Australians Aren’t Interested |Chad de Guzman |October 22, 2021 |Time 

First, in mid-March, consulates around the world shut down because of the pandemic. These Afghans Won the Visa Lottery Two Years Ago — Now They’re Stuck in Kabul and Out of Luck |by Dara Lind |August 27, 2021 |ProPublica 

Once consulates and embassies got the correct names, they rushed appointments, often giving applicants little notice. These Afghans Won the Visa Lottery Two Years Ago — Now They’re Stuck in Kabul and Out of Luck |by Dara Lind |August 27, 2021 |ProPublica 

Clients with children abroad have not been able to bring their children here with consulates closed. Center Serving Domestic Violence Victims Remains Closed, Angering Advocates |Ashly McGlone |January 6, 2021 |Voice of San Diego 

Now, consulates are “slowly allowing emergency cases, but no one is really clear what is considered an emergency,” Parr said. Center Serving Domestic Violence Victims Remains Closed, Angering Advocates |Ashly McGlone |January 6, 2021 |Voice of San Diego 

The safety of American personnel at the American consulate in Libya was undermined by a “stand down order.” Congress Debunks Congress’s Nuttiest Benghazi Theories |Ben Jacobs |November 26, 2014 |DAILY BEAST 

When there was an actual consulate in Benghazi, back in 1967, it was stormed by a mob and parts of it were set on fire. This Sexy Thriller Is Just the Document the Benghazi Commission Needs |Christopher Dickey |September 15, 2014 |DAILY BEAST 

Fortunately the Indian security guards at the consulate killed all the attackers. Nuclear Pakistan's Spies Target India—and Their Own Prime Minister |Bruce Riedel |September 4, 2014 |DAILY BEAST 

It serves as home base for the United States Consulate and other foreign offices. Fighting Back With Faith: Inside the Yezidis’ Iraqi Temple |Michael Luongo |August 21, 2014 |DAILY BEAST 

Erbil, the Iraqi Kurdish capital, is home to a U.S. consulate and thousands of U.S. citizens working in the oil industry. PKK Kurdish Terrorists Are Fighting IS Terrorists With U.S. Help |Thomas Seibert |August 16, 2014 |DAILY BEAST 

Native women were not interfered with by either party, nor were the foreigners, many of whom took refuge at the British Consulate. The Philippine Islands |John Foreman 

He was further instructed to hand over his consulate archives to the British Consul, who would take charge of American interests. The Philippine Islands |John Foreman 

The Gov.-General courteously proposed to send a large bodyguard to his consulate, but it was not necessary. The Philippine Islands |John Foreman 

Six weeks after the victory of Zurich came the 18th Brumaire, and Napoleon's accession to the consulate. Napoleon's Marshals |R. P. Dunn-Pattison 

Incessant bugle-calls from the natives added to the commotion, and thousands of Chinese crowded into the Chinese Consulate. The Philippine Islands |John Foreman