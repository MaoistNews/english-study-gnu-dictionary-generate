After he caricatured the Marx brothers, the film studio sought to restyle Groucho to look more like a humorous Hirschfeld. Al Hirschfeld: Drawing entertainers with pen and ink, elegance and wit |Michael Cavna |July 30, 2021 |Washington Post 

We described the second video as either humorous or boring, and then asked them how long they thought each video would feel like it lasted. Why Vacations Feel Like They’re Over Before They Even Start |LGBTQ-Editor |July 12, 2021 |No Straight News 

Former Mayor Jerry Sanders – or Darren Pudgil, the spokesman who delivered this quote – comes out looking sensible and humorous while reframing the whole debacle such that it’s hard to imagine this ending up any other way. 7 Quotes From or About Jason Hughes That Are, in Retrospect, Hysterical |Andrew Keatts |July 5, 2021 |Voice of San Diego 

Manansala has created just that with her debut novel, a tale full of eccentric characters, humorous situations and an oh-so-tricky mystery. Eight thrillers and mysteries to read this summer |S.A. Cosby |May 27, 2021 |Washington Post 

He’d let Schaefer vent, then calmly ask him questions or make a humorous remark. Joseph R.L. Sterne, Baltimore Sun editorial page editor, dies at 92 |Frederick N. Rasmussen |April 12, 2021 |Washington Post 

In fact, she also launched a humorous series “Ask Lena” on YouTube where she gives sage advice on feminism in episode one. Comedians and Feminism Getting Laughs |Agunda Okeyo |October 23, 2014 |DAILY BEAST 

The sniper barely missed, and Steven relayed the story as equal parts humorous and traumatic. Was U.S. Journalist Steven Sotloff a Marked Man? |Ben Taub |September 2, 2014 |DAILY BEAST 

Libyans are by and large charming, charismatic, humorous people with a Mediterranean joie de vivre. It’s Not the USA that Made Libya the Disaster it is Today |Ann Marlowe |August 3, 2014 |DAILY BEAST 

Austen, Eliot, and James sometimes complemented their essential seriousness with humorous minor characters and subplots. How to Get Laid in Brooklyn a la Adelle Waldman’s Nifty Novel of Manners |Tom LeClair |July 25, 2014 |DAILY BEAST 

Mollen, an actress, first got her literary start by way of a humorous incident, naturally. Actress Jenny Mollen Talks Hiring Prostitutes for Husband Jason Biggs and Embracing Her Crazy |Erin Cunningham |June 30, 2014 |DAILY BEAST 

"You positively convulse me, you're so very humorous," said Robinson, without a vestige of a smile. Davy and The Goblin |Charles E. Carryl 

Perhaps our comic papers have never heard of the Improvement Clubs, or find nothing in them that is humorous. Ancestors |Gertrude Atherton 

He was fond of the pathetic, but the humorous moved him most, and his lively gifts were welcome wherever we went. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Aunt Ri gazed at her with a sentiment as near to veneration as her dry, humorous, practical nature was capable of feeling. Ramona |Helen Hunt Jackson 

Her hazel eyes were very round for a moment, then they narrowed, and little humorous lines formed at the corners of her lips. St. Martin's Summer |Rafael Sabatini