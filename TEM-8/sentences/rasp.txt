Using a rasp, zest the meyer lemon into the bowl and add the chopped herbs, anchovy, capers and a few grinds of black pepper. How Top Chefs Stay Thin |Rachel Syme |December 15, 2009 |DAILY BEAST 

"I saw you as I was coming away from Kendrick's last night," he began, with a bickering rasp in his voice. The Wreckers |Francis Lynde 

His throat was like sandpaper and the words came out in a rasp. The Dark Door |Alan Edward Nourse 

Sixth, rasp off the outside edge of one end of the pipe as shown. Elements of Plumbing |Samuel Dibble 

If the branch is tapered with the rasp as shown the joint can be made very tight. Elements of Plumbing |Samuel Dibble 

Second, square the ends with the rasp, as previously explained. Elements of Plumbing |Samuel Dibble