Environments designed by the Miami-based start-up’s team of coders and trained architects include a gold-toned plenary hall, an outdoor amphitheater, and a suite of lobbies to foster chance encounters among participants. What a conference in the metaverse looks like |Anne Quito |February 25, 2022 |Quartz 

He had chosen his plenary to break the news to the unsuspecting audience, me included. E.O. Wilson Saw the World in a Wholly New Way - Issue 112: Inspiration |David Sloan Wilson |January 5, 2022 |Nautilus 

“More than 300 member organizations worldwide participated in the voting process, workshops, plenary sessions, regional and board meetings during the 8-day virtual event,” the statement notes. D.C. loses bid for 2025 World Pride to Taiwan |Lou Chibbaro Jr. |November 13, 2021 |Washington Blade 

The opening plenary brought a diverse group of leaders together to discuss the theme “Mobilizing for Impact.” The Clinton Global Initiative Kicks off With Tears, Impressions, and Fighting Words |Nina Strochlic |September 24, 2013 |DAILY BEAST 

The hottest debate in Davos on Thursday will come from a series of plenary sessions on Europe. From Capitalism to the World's Most Vulnerable |Barbie Latza Nadeau |January 26, 2012 |DAILY BEAST 

A plenary session features a keynote address on “unleashing creativity” from the founder of a promotions site called ePrize. The Perils of Borrowing Online |Gary Rivlin |October 12, 2011 |DAILY BEAST 

Your arm was stout enough in old days, and I give you plenary authority to use it as you see fit. A Thin Ghost and Others |M. R. (Montague Rhodes) James 

He also conceded a plenary indulgence to all who have made such visits three times in three distinct days. The Mysteries of All Nations |James Grant 

I, on my part, will listen to your lover's confessions and give you plenary absolution—even for listening at keyholes. Joan of the Sword Hand |S(amuel) R(utherford) Crockett 

It was called Great Friday, and on that day such as entered the sanctuary of Anis received plenary indulgence. The Life of Joan of Arc, Vol. 1 and 2 (of 2) |Anatole France 

One would suppose that she had plenary indulgences for her conduct. Court Beauties of Old Whitehall |W. R. H. Trowbridge