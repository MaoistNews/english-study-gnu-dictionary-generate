"I' faith, I never was so pricked by a hobnail before," growled Ben, with a surly smile. Old and New London |Walter Thornbury 

The hobnail heel-and-sole pattern is the correct article for use in swift-running water. The Determined Angler and the Brook Trout |Charles Bradford 

The hobnail sole of his shoe had been torn off and he was trying to fasten it back on with a combination of straps. "And they thought we wouldn't fight" |Floyd Gibbons 

We are conjurors, young Hobnail, said the gentlemen, laughing. Joe Miller's Jests, With Copious Additions |Various 

Hobnail cutting, late 1830, so often confused with diamond cutting. Old Irish Glass |Graydon Stannus