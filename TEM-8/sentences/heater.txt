The fire is thought to have started on the first floor of the townhouse and was probably caused by a “mishap with a kerosene heater,” according to Piringer. Large snake, two dogs rescued from house fire in Silver Spring |Dana Hedgpeth |February 9, 2021 |Washington Post 

Powerful and durable, these heaters can make an outdoor party feel cozy no matter where you live. Backyard party essentials: Outdoor Super Bowl party ideas for 2021 |PopSci Commerce Team |January 30, 2021 |Popular-Science 

Co-owner Michael Biddick, the author of “43 Wine Regions,” makes sure patrons drink as well as they eat and an outdoor patio beckons with individual heaters, well-spaced tables and a row of evergreens. Tom Sietsema’s 8 favorite places to eat right now |Tom Sietsema |January 26, 2021 |Washington Post 

The beauty of this heater is its versatility—it can be installed on any rig and provides warm, dry air that’ll let you camp comfortably no matter how cold it is outside. How to Add a Heater to Any Camper |Bryan Rogala and Jackson Buscher |January 25, 2021 |Outside Online 

Dirty Goose set up fire pits under tents on U Street, as well as rooftop propane heaters. Business owners adapt again as indoor dining returns to D.C. |Evan Caplan |January 25, 2021 |Washington Blade 

When I do, I can see an electric light, a Dyson vacuum cleaner, a laptop computer, a blow-heater and a mobile phone. Consider the Washing Machine |Justin Green |September 28, 2012 |DAILY BEAST 

An absence of natural gas for the water heater led her to innovate. Pakistan's Yuppies in Danger |Lorraine Adams |May 12, 2011 |DAILY BEAST 

The two were sitting in front of a heater one November afternoon when Apple had a eureka moment. The Original Apple Icon |Anthony Haden-Guest |October 6, 2010 |DAILY BEAST 

"We had bedbugs before they were cool, before they were hip," says Heater. Vigilante Bedbug Exterminators |Brian Ries |August 27, 2010 |DAILY BEAST 

Back in 2006, Brian Heater was living in an apartment in Astoria, Queens, when he began noticing welts on his skin. Vigilante Bedbug Exterminators |Brian Ries |August 27, 2010 |DAILY BEAST 

Behind this stairway on the lower floor was a fireplace (m) with a water heater. The Private Life of the Romans |Harold Whetstone Johnston 

There was no fire in the great heater, and the tables and chairs were black with dust. The Call of the Beaver Patrol |V. T. Sherman 

The bottom of the tank and the bottom of the heater are connected. Elements of Plumbing |Samuel Dibble 

The top of the heater and the top of the boiler are connected. Elements of Plumbing |Samuel Dibble 

Upon the primus heater, alone, did we rely for cooking the meals on sledging journeys. The Home of the Blizzard |Douglas Mawson