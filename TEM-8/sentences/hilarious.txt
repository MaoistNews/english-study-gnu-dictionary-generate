Browne’s hilarious, cringeworthy accounts of harvest logistical quandaries and late-night winery blunders that ruined thousands of dollars’ worth of wine are not unique, but they do illustrate the hard work involved in winemaking. Crack open the best 2020 wine books for rich history, international politics and a vintner’s lively memoir |Dave McIntyre |December 11, 2020 |Washington Post 

The packaging, which features a clipart-style image of an Aztec temple, reminded me of those hilarious, old-school hair gel tubs. This Face Mask Made Me a Skin Care Convert |Erin Riley |November 22, 2020 |Outside Online 

He was hilarious and never seemed like he wasn’t having a good time. Catching Shade From Your (and the Beastie Boys’) Favorite TV Cop |Eugene Robinson |October 14, 2020 |Ozy 

Instead, the Doobie Brothers’ lawyer scolded Murray in a short, hilarious letter that calls out the “Ghostbusters” and “Caddyshack” star for copyright infringement, but also lampoons his questionable acting decisions and fashion choices. ‘Damn ugly’ golf shirts: Bill Murray blasted by Doobie Brothers in viral copyright letter |Jeff |September 25, 2020 |Fortune 

Angie McMahon, whom you may have seen on Netflix’s hilarious baking show Nailed It, found a solution while playing the Jaxbox game Whiplash with her family on Zoom. The Best Streaming Shows Quarantine Has to Offer |Joshua Eferighe |August 14, 2020 |Ozy 

Its fearless creator opens up about the hilarious new season. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Why is the ‘Kroll Show’ ending its hilarious run after only three seasons? The Zany Shades of Nick Kroll |Abby Haglage |December 15, 2014 |DAILY BEAST 

Thanks to Funny or Die, you can see the former 30 Rock actress do her best Peter Pan to hilarious effect. Watch Jane Krakowski's Secret Peter Pan Live! Audition Tape |Jack Holmes, The Daily Beast Video |December 2, 2014 |DAILY BEAST 

The highly anticipated sixth season of the hilarious spy-spoof will premiere on FX at 10 pm EST on January 8, 2015. ‘Archer’ Season 6 Exclusive: Watch Six ‘Certified’ Videos and See the FX Series’ New Key Art |Marlow Stern |November 24, 2014 |DAILY BEAST 

How did you wrangle Nicole Kidman for her hilarious cameo in the movie? Stephen Merchant Talks ‘Hello Ladies’ movie, the Nicole Kidman Cameo, and Legacy of ‘The Office’ |Marlow Stern |November 22, 2014 |DAILY BEAST 

"A gen'leman to speak to you, Miss Betty," she announced a moment later, looking in on the hilarious girls. The Outdoor Girls in the Saddle |Laura Lee Hope 

Next morning the girls were hilarious over the mirthful episode in the train the night before. The Outdoor Girls in the Saddle |Laura Lee Hope 

Im laughing over a new joke, said Wilder, in anything but an hilarious tone. The Woman Gives |Owen Johnson 

He was humble but full of dignity, serene though distressed, cheerful but not hilarious. Beacon Lights of History, Volume I |John Lord 

Thyrsis read this, and then shut the door upon the messenger-boy, and burst into wild, hilarious laughter. Love's Pilgrimage |Upton Sinclair