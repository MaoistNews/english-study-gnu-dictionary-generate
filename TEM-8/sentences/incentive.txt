China’s reliance on foreign semiconductors is both a major incentive for and hinderance to achieving that goal. China will spend $300 billion on semiconductor imports as U.S. squeezes chip supply |eamonbarrett |August 27, 2020 |Fortune 

In their 2005 best-selling book Freakonomics, two authors explained how economics are a powerful incentive on human behavior. Sacramento may pay COVID-infected workers $1000 to stay home |Jeff |August 26, 2020 |Fortune 

Smaller chains and independent theaters will also be reopening, but discounts and other incentives are scarcer. Are these big discounts enough to get you back into a movie theater? |dzanemorris |August 20, 2020 |Fortune 

They argue that the incentives CEOs face have not changed, so their behavior won’t change. Revisiting the Business Roundtable’s ‘Stakeholder Capitalism,’ one year later |Geoffrey Colvin |August 19, 2020 |Fortune 

Excerpts of a preliminary legal review of the purchase, leaked to NBC 7, contend that by acting as a middleman in a major real estate transaction, Cisterra didn’t have an incentive to look closely at the building’s true condition. How the City Came to Lease a Lemon |Lisa Halverstadt and Jesse Marx |August 10, 2020 |Voice of San Diego 

Given the potential for a cyber tit-for-tat to escalate, Obama has even more incentive to find a diplomatic solution. Obama Could Hit China to Punish North Korea |Shane Harris, Tim Mak |December 20, 2014 |DAILY BEAST 

In addition, because House Democrats were cut out of the negotiations over the bill, they don't feel any incentive to play ball. Nancy Pelosi Plays Hardball On Cromnibus |Ben Jacobs |December 11, 2014 |DAILY BEAST 

As it stands, candidates do not have much of an incentive to come out in favor of same-sex marriage. Is Gay Marriage Going Away in 2016? |David Freedlander |December 4, 2014 |DAILY BEAST 

Until scholars and collectors stop buying, antiquities dealers have no incentive to stop selling. Dismembering History: The Shady Online Trade in Ancient Texts |Candida Moss |November 23, 2014 |DAILY BEAST 

As long as there are states willing to negotiate payments with groups like ISIS, there will be a financial incentive to kidnap. To Kill the ISIS Monster You Gotta Do More Than Cut Off Its Money |Charlie Winter |November 18, 2014 |DAILY BEAST 

Several desertions were now reported from the troops, a hostility to discipline rather than cowardice being the incentive. A Virginia Scout |Hugh Pendexter 

Above all, we had the perpetual incentive of gardening to keep our eyes toward the future. The Idyl of Twin Fires |Walter Prichard Eaton 

And to thwart Mrs. Errington would alone have been a powerful incentive with old Max. A Charming Fellow, Volume II (of 3) |Frances Eleanor Trollope 

But with the 250 apprehension of the Ideal and of the Divine law, three things follow, incentive to progress. The Life of Mazzini |Bolton King 

What a successful man, of marked force of character, has done, may be an incentive and an encouragement to others. The Bay State Monthly, Vol. II, No. 6, March, 1885 |Various