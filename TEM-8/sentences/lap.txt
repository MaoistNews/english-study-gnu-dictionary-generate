This makes it not only a great lap desk for working from a couch or coffee table—or even from the floor—but also an excellent standing desk converter for your existing work setup. Convenient laptop stands with adjustable easels |PopSci Commerce Team |September 23, 2020 |Popular-Science 

An ultra-portable lap desk with an adjustable easel helps remedy these problems, by providing that little ergonomic lift. Convenient laptop stands with adjustable easels |PopSci Commerce Team |September 23, 2020 |Popular-Science 

In order to win, a team’s autonomous car must be able to complete 20 laps—which equates to a little less than 50 miles in distance—and cross the finish line first in 25 minutes or less. Self-driving cars will hit the Indianapolis Motor Speedway in a landmark A.I. race |jonathanvanian2015 |September 19, 2020 |Fortune 

Human drivers who notice that their cars are close to overheating, for example, may decide to take the risk and finish a race if there are just a few laps left. Self-driving cars will hit the Indianapolis Motor Speedway in a landmark A.I. race |jonathanvanian2015 |September 19, 2020 |Fortune 

Hamilton’s Mercedes collided with the Red Bull driven by Alex Albon, spinning it around and taking it out of the race with less than four laps to go. How data helped keep Red Bull’s F1 team on track during the pandemic |Jeremy Kahn |September 19, 2020 |Fortune 

New Senate Majority Leader McConnell had just taken a victory lap held a press conference. The Booze That Saved America |Kevin Bleyer |November 8, 2014 |DAILY BEAST 

This is a certifiable situation in our collective nervous lap, and the time is now for. ISIS and BS |Amal Ghandour |October 15, 2014 |DAILY BEAST 

Instead of taking a victory lap, she was was dodging reporters. Koch Brothers Bail Out GOP Senate Hopeful in Oregon |Ben Jacobs |August 25, 2014 |DAILY BEAST 

A. I struck him in the back of the head while he was in my lap fighting to break free. From Ferguson Cop Embroiled in a Brutality Suit to City Councilwoman |Michael Daly |August 20, 2014 |DAILY BEAST 

Tihen had insisted he was in her lap as she struggled to handcuff him. From Ferguson Cop Embroiled in a Brutality Suit to City Councilwoman |Michael Daly |August 20, 2014 |DAILY BEAST 

At the sight, Felipe flung himself on his knees before her; he kissed the aged hands as they lay trembling in her lap. Ramona |Helen Hunt Jackson 

See the ease and grace of the lady in the sacque, who sits on the bank there, under the myrtles, with the guitar on her lap! Checkmate |Joseph Sheridan Le Fanu 

I turned round, thrust my purse into the lap of the nearest, and with a light heart led the lady back to the hotel. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

There was a picture of Madame Lebrun with Robert as a baby, seated in her lap, a round-faced infant with a fist in his mouth. The Awakening and Selected Short Stories |Kate Chopin 

Before that time we always put rope-yarn between the lap of the boiler-plates to make the seams tight. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick