The company updated its ad policies after it found that housing, employment and credit ads were being targeted based on a users’ zip code, age, gender and marital status. A Google Ads setting allowed advertisers to exclude people of ‘unknown gender’ |Carolyn Lyden |February 11, 2021 |Search Engine Land 

But, at the end of the day, which one you receive will boil down to your zip code, local health care infrastructure, and the total number of available doses. What we know about which COVID vaccine you’ll receive |Sy Mukherjee |January 12, 2021 |Fortune 

Results are shared with public-health authorities through the accompanying mobile app, with has users fill out their zip code and date of birth, the FDA said. FDA approves first at-home, over-the-counter COVID test |Verne Kopytoff |December 15, 2020 |Fortune 

One of the greatest determinants of physical and economic health is the zip code in which you were born. A Model for a Just COVID-19 Vaccination Program - Issue 93: Forerunners |Melanie Moses & Kathy Powers |November 25, 2020 |Nautilus 

Hosts post their pool and its amenities on the app, and those looking to swim can search for pools in their area by zip code and filter searches by amenities and voilà — you just book a time! No Plans This Weekend? No Problem! This App Lets You Rent Other People’s Pools |Charli Penn |September 4, 2020 |Essence.com 

Based on a true story” is film code for “this may or may not have happened, but almost certainly not in this way. Wrestler Mark Schultz Hates the ‘Sickening and Insulting Lies’ of ‘Foxcatcher’ |Rich Goldstein |December 31, 2014 |DAILY BEAST 

In Brazil people color code their underwear according to their needs. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

These addresses were used by whoever carried out the attack to control the malware and can be found in the malware code itself. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

The source code for the original “Shamoon” malware is widely known to have leaked. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

Enforcement of U.S Code, Title VII, Chapter 25A “Export Standards for Grapes and Plums” remains fully funded, thank goodness. Congress’ Gift That Keeps on Giving |P. J. O’Rourke |December 20, 2014 |DAILY BEAST 

It is no part of the present essay to attempt to detail the particulars of a code of social legislation. The Unsolved Riddle of Social Justice |Stephen Leacock 

The minimum wage law ought to form, in one fashion or another, a part of the code of every community. The Unsolved Riddle of Social Justice |Stephen Leacock 

Plato, dissatisfied with the laws of his country, wrote out a code of morals and laws which he thought much better. Gospel Philosophy |J. H. Ward 

The objectors consider it to be a most selfish doctrine without any warrant in the civilised code of morality. Third class in Indian railways |Mahatma Gandhi 

Why is a cankered tie indissoluble, notwithstanding the great maxim adopted by the code, Quicquid ligatur dissolubile est? A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire)