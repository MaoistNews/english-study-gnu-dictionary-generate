Then, this year, the Justice companies “apparently stopped paying” some of the stipulated penalties, citing the lawsuit, according to a court filing by the plaintiffs. This Billionaire Governor’s Coal Company Might Get a Big Break From His Own Regulators |by Ken Ward Jr. |September 17, 2020 |ProPublica 

The city stipulated in its long and complex terms with the university that the site could be a potential location for future groundwater extraction wells. Pursuing Independent Water Sources, San Diego Ignores One Beneath Its Feet |MacKenzie Elmer |September 14, 2020 |Voice of San Diego 

For example, it stipulates that social-media platforms must keep records of messages that are forwarded by at least five users to more than 1,000 users within 15 days. Brazil’s “fake news” bill won’t solve its misinformation problem |Amy Nordrum |September 10, 2020 |MIT Technology Review 

So far, Reliable has had to have an observer on the ground directly watching its airplane fly because of FAA rules that stipulate civilian drones must be flown within direct line of vision of a human operator. Cessna makes history by taking off and landing with no one aboard. Here’s how |Jeremy Kahn |August 26, 2020 |Fortune 

Let’s stipulate that we were not put on Earth to do anything other than to maximize the representation of our genes in future generations. Just Because It’s Natural Doesn’t Mean It’s Good - Issue 89: The Dark Side |David P. Barash |August 19, 2020 |Nautilus 

Rather, new standards stipulate that bulbs manufactured and sold in the U.S. must meet higher energy efficiency standards. Why You Should Give LED Light Bulbs for Christmas. Seriously. |Daniel Gross |December 4, 2013 |DAILY BEAST 

The new constitution does not stipulate any requirements for a vice president in the government. Egypt Constitution Passes Amid Allegations of Fraud |Vivian Salama |December 23, 2012 |DAILY BEAST 

So let's stipulate that my critics are completely gender blind, reacting only to my many faults. Why Does Everyone Hate Women? |Megan McArdle |October 24, 2012 |DAILY BEAST 

I'll stipulate that the Democrats had a good convention, in the primetime hours at least. Mark McKinnon on the Pundits’ Rush to Bury Romney-Ryan |Mark McKinnon |September 12, 2012 |DAILY BEAST 

Let's stipulate that the fate of the Republic does not turn on the state of Sally Quinn's social life. A Good Word for Sally Quinn |David Frum |June 14, 2012 |DAILY BEAST 

After a short consultation with Ronan they directed him to stipulate the conditions for Cautin's life. The Poniard's Hilt |Eugne Sue 

Neither did she ever give me any money,—or anything but my daily dinner,—nor ever stipulate that I should be paid for my services. Great Expectations |Charles Dickens 

Still, in your service, I am willing to endure even Podbury—for a strictly limited period; that I do stipulate for. The Travelling Companions |F. Anstey 

We must stipulate that the new dish effects an improvement in the economy of the working classes. The Popular Science Monthly, September, 1900 |Various 

It is always as well to stipulate something about prices beforehand. Gatherings From Spain |Richard Ford