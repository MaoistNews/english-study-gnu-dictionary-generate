Upside down and confused, the owl relaxed its wings, and I used my free arm to tuck them first against its body and then the body against me as though holding a swaddled newborn child. The quest to snare—and save—the world’s largest owl |Jonathan Slaght |August 28, 2020 |Popular-Science 

These products could confuse consumers into accidentally ingesting a potentially deadly product. FDA: Please, don’t drink hand sanitizer—even if it’s sold in… a beer can? |Beth Mole |August 27, 2020 |Ars Technica 

Now, they are swimming in confusing murky waters when it comes to making decisions. The CDC’s new COVID-19 testing guidelines could make the pandemic worse |Sara Kiley Watson |August 27, 2020 |Popular-Science 

Proponents of the change argue that being associated with China has become problematic during the coronavirus pandemic because border agents tend to confuse them with citizens of communist China where Covid-19 is believed to have originated from. A quirky passport design contest evokes Taiwan’s search for national identity |Anne Quito |August 25, 2020 |Quartz 

Even the Democratic activists and insiders I spoke with who strongly support the party’s historical role in advancing underrepresented groups emerged from the 2016 election frightened and confused by its results. How Clinton’s Loss Paved The Way For Biden |Seth Masket |August 20, 2020 |FiveThirtyEight 

Until then, men shall all wearily grow our facial hair, wear flannel, and confuse the hell out of each other out on the streets. How Straight World Stole ‘Gay’: The Last Gasp of the ‘Lumbersexual’ |Tim Teeman |November 12, 2014 |DAILY BEAST 

One gets the sense that they are wearing a mask to confuse their readers, and even to evade them. Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 

Stephen Collins is just an actor, of course, and no one should confuse him with a character he played on TV. ‘7th Heaven’ Dad Stephen Collins and the Christian Right’s Real Morality Tale |Amanda Marcotte |October 8, 2014 |DAILY BEAST 

This, in fact, is one of their big arguments and it works well to confuse base lines. Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 

The increase in recognition of autism spectrum disorders in Western countries continues to confound and confuse. No, Stem Cells Don't Cause Autism |Kent Sepkowitz |September 11, 2014 |DAILY BEAST 

I hate drums in the march,' said the king, 'they do nothing but confuse the step. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

I've been studying motor manuals and all that sort of thing ever since I commenced to drive, but diagrams always confuse me. The Everlasting Arms |Joseph Hocking 

The great number and intricacy of these objects confuse the senses and do not permit the eye to rest. Birds and All Nature, Vol. VI, No. 3, October 1899 |Various 

But at first the telectroscope only served to confuse them more. Islands of Space |John W Campbell 

Couldn't he confuse her into going off with him, at least temporarily? A World Called Crimson |Darius John Granger