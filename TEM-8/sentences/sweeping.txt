These sweeping observations are, disappointingly, the most thorough analysis Ehrlich provides on climate change as a whole. What Gretel Ehrlich Gets Wrong About Climate Change |Erin Berger |February 8, 2021 |Outside Online 

The big influencers on state privacy bills are Europe’s sweeping General Data Protection Regulation and California’s now-strengthened privacy law. Cheat sheet: What to expect in state and federal privacy regulation in 2021 |Kate Kaye |February 1, 2021 |Digiday 

It’s critical that advertisers take their exclusions seriously and understand what the implications of broad-sweeping exclusions can mean for their brands and their metrics. Facebook testing brand safety topic exclusions for advertisers |Carolyn Lyden |January 29, 2021 |Search Engine Land 

The sweeping social and economic transformation that scientists say is needed to attack the problem will require a global effort. How to stop your house’s expensive drafts — and save the planet |Sarah Kaplan |January 27, 2021 |Washington Post 

The distribution of funding for health care providers is just one example of complications with the sweeping $2 trillion CARES Act. How the CARES Act Forgot America’s Most Vulnerable Hospitals |by Brianna Bailey, The Frontier |January 26, 2021 |ProPublica 

Two years ago in Michigan, she oversaw AFP operations to help the Republican-controlled legislature pass sweeping anti-union laws. The Next Phase of the Koch Brothers’ War on Unions |Carl Deal and Tia Lessin |December 22, 2014 |DAILY BEAST 

In 1695, still under an imposed silence, she died in a plague sweeping the capital. Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 

The classic film that opens with a tornado sweeping through a Kansas farm made its debut 75 years ago in 1939. Fact-Checking the Sunday Shows: November 2 |PunditFact.com |November 2, 2014 |DAILY BEAST 

These tensions run throughout the conference, but also throughout the “mindfulness” movement that is now sweeping America. What If Meditation Isn’t Good for You? |Jay Michaelson |November 1, 2014 |DAILY BEAST 

As the Ebola epidemic began sweeping through the region, fear and mistrust of the health workers in West Point escalated. Meet the Liberian Girls Beating Ebola |Abby Haglage |October 29, 2014 |DAILY BEAST 

"Yes, Alessandro," she answered faintly, the gusts sweeping her voice like a distant echo past him. Ramona |Helen Hunt Jackson 

He stood aside, and bending from the waist he made a sweeping gesture towards the door with the hand that held his hat. St. Martin's Summer |Rafael Sabatini 

“Yes; that would be indispensible,” said the baron, whose eyes were sweeping the room from corner to corner, fiercely and swiftly. Checkmate |Joseph Sheridan Le Fanu 

She did shout for joy, as with a sweeping stroke or two she lifted her body to the surface of the water. The Awakening and Selected Short Stories |Kate Chopin 

The wave of religious fanaticism sweeping over the land might recede as rapidly as it had risen. The Red Year |Louis Tracy