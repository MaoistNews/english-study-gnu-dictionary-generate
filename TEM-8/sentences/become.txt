In the last year, her fusion exercise class has attracted a cult following and become de rigueur among the celebrity set. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

The more we appease, the more we indulge, the more emboldened the enemies of freedom become. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

Spouting off against police online has become criminalized in recent weeks. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

Many of those who have become cops in New York seem to have ceased to address such minor offenses over the past few days. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Lucas said that he himself nonetheless hopes to become a cop. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

It will be a busy session; and I want to see if I can't become a useful public man. Elster's Folly |Mrs. Henry Wood 

My son,” said Grabantak one evening to Chingatok, “if we are henceforth to live in peace, why not unite and become one nation? The Giant of the North |R.M. Ballantyne 

You will follow the suite of my daughter to Spain, and you will become the bosom Counsellor of the wife of your Prince? The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The children of sinners become children of abominations, and they that converse near the houses of the ungodly. The Bible, Douay-Rheims Version |Various 

I shall soon depart, and practise no more; and my time will become my own—still my own, by no means yours. Checkmate |Joseph Sheridan Le Fanu