These large chairs can recline and raise your legs above your chest. Best massage chair: Take relaxation to a new level right in your living room |Irena Collaku |August 13, 2021 |Popular-Science 

There are no vibration functions and it doesn’t recline, but for a simple, lightweight massage chair, this is great for apartments and small spaces. Best massage chair: Take relaxation to a new level right in your living room |Irena Collaku |August 13, 2021 |Popular-Science 

You won’t be folding yourself into your doll-sized coach seat only to have the lady in front of you recline, so that now only one of your lungs can inflate. Okay, so maybe we don’t miss everything about holiday travel |Liz Langley |December 18, 2020 |Washington Post 

I’m not sure what points I’m hitting every time I recline on its stabby array, but with a few minutes of wiggling and microadjustments, I never fail to find something that feels good. My Chronic Pain Was No Match for This Mat |Aleta Burchyski |September 4, 2020 |Outside Online 

So in order for you to recline to a position that you might want to be in, you will need to essentially either put force into it if you’re a light body type in order to hold yourself in that position. Everything I Learned in My Quest for a Better Work-From-Home Chair |Patrick Lucas Austin |July 29, 2020 |Time 

The incidents have sparked wide debate about whether passengers should opt to recline at all. Solution to Seat Rage: No More Reclining |Will Doig |September 4, 2014 |DAILY BEAST 

But of course someone always will recline her seat, like the people in the first row, or the woman in front of me, whom I hate. Should People Stop Reclining Their Seats? |Megan McArdle |February 20, 2013 |DAILY BEAST 

Round the three walls is a raised daīs called "lewan," covered with rugs or mattresses, on which the guests recline. Peeps at Many Lands: Egypt |R. Talbot Kelly 

To move, stand, or recline in an indolent or relaxed manner. Scottish Ghost Stories |Elliott O'Donnell 

Passengers will please recline in their bunks and fasten the retaining straps before the steward arrives. The Colors of Space |Marion Zimmer Bradley 

I asked him to cause her to pass out of her ecstacy, and recline on the bed. Journal in France in 1845 and 1848 with Letters from Italy in 1847 |T. W. (Thomas William) Allies 

Bruce and Emmie had the railway carriage to themselves, and the invalid was thus able to recline as on a couch. The Haunted Room |A. L. O. E.