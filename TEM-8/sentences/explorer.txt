All they discovered were water bottles from earlier explorers. A CIA spyplane crashed outside Area 51 a half-century ago. This explorer found it. |Sarah Scoles |January 5, 2021 |Popular-Science 

Instead, the land claimed the lives of four of the explorers. The Woman Who Survived Two Years Alone in the Arctic |Fiona Zublin |December 9, 2020 |Ozy 

For the second year in a row, China has landed an ambitious robotic explorer on the moon. A Chinese robot is bringing home moon rocks, and US bots will soon follow |Tim Fernholz |December 3, 2020 |Quartz 

That’s not only key to sustaining human life, but the hydrogen and oxygen in water could also be used as rocket fuel, making the moon a potential gas station in space that could help explorers reach farther into the solar system. A dollar can’t buy you a cup of coffee but that’s what NASA intends to pay for some moon rocks |Christian Davenport |December 3, 2020 |Washington Post 

No matter how many G’s the network has, no Martian explorer will ever enjoy a real time call with Earth. The moon will soon have cell service |Charlie Wood |October 22, 2020 |Popular-Science 

The first two videos are teasers featuring two favorite cartoon characters for young girls, Dora the Explorer and Tinkerbell. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

Unfortunately, dozens of pyramids are missing their peaks thanks to an overeager, gold-seeking explorer. Egypt Ain’t The Only Pyramid Show In Town |Nina Strochlic |December 11, 2014 |DAILY BEAST 

Actor and explorer Michael Palin, visiting in 2001, described one elderly bookkeeper showing off his priceless wares. The Lost Libraries of the Sahara‬ |Nina Strochlic |September 11, 2014 |DAILY BEAST 

The proposed Titan Mare Explorer (TiME), for example, would place a boat on either Ligeia Mare or Kraken Mare. A Cloud Forms Over Saturn’s Mysterious Moon |Matthew R. Francis |August 17, 2014 |DAILY BEAST 

Ironically, the first archaeologist to explore the cave had a connection to the most legendary fictional explorer. The Cave Where Mayans Sacrificed Humans Is Open for Visitors |Nina Strochlic |August 14, 2014 |DAILY BEAST 

And the brave explorer sailed safely through the dangerous strait now named for him. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

On one side loomed a huge tank, to the brink of which a rickety wooden ladder invited the explorer to ascend. Dope |Sax Rohmer 

Passage over it is often one of the most difficult feats to accomplish which the Alpine explorer has to undertake. Outlines of the Earth's History |Nathaniel Southgate Shaler 

These fractures often exist in very great numbers, and constitute a formidable barrier in the explorer's way. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Each step, which seems to bring the explorer of nature nearer to his object, only carries him to the threshold of new labyrinths. Decline of Science in England |Charles Babbage