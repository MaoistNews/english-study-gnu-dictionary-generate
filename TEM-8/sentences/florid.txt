There are Halo novels, miniseries, and reams of florid fan-fiction. David Fincher and Conan O’Brien: Halo 4’s Secret Weapons |Alex Klein |October 10, 2012 |DAILY BEAST 

The florid brushwork of a Constable gets hypertrophied in Freud, into a kind of gross exaggeration of what unleashed paint can do. Lucian Freud, the Conservative Radical |Blake Gopnik |July 21, 2011 |DAILY BEAST 

Show business, of course, provides florid conditions for contempt. Comedians Laugh as Leno Sinks |Gina Piccalo |October 24, 2010 |DAILY BEAST 

He lacks the magisterial tone of Colm Tóibín or the florid and fertile imagination of Patrick McCabe. Great Irish Adventures |Allen Barra |June 9, 2010 |DAILY BEAST 

The village organist had distinguished himself by his florid rendering of the Wedding March. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

On one occasion, a general titter arose at his florid picture of the happiness which must proceed from this event. The Book of Anecdotes and Budget of Fun; |Various 

Once he learnt, with his aunt, the exceedingly florid duet in Semiramide, and sang the soprano part admirably. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Darker grew his florid countenance; his bulging eyes looked troubled and perplexed. Mistress Wilding |Rafael Sabatini 

The last had a very brilliant career as an orator, though his orations were too florid to be read. Beacon Lights of History, Volume I |John Lord