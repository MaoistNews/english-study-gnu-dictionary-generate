Maybe it’s because of the pandemic, but this Valentine’s Day, so far, there are only three weddings scheduled. How One Small Maryland Town Became the Marriage Capital of the East Coast in the Early 20th Century |Melissa August |February 11, 2021 |Time 

Our Valentine’s Day gift is some sunshine for a change, especially in the afternoon. D.C.-area forecast: Snow and wintry mix taper off this morning, while ice concerns mount for Saturday |David Streit |February 11, 2021 |Washington Post 

This year, Valentine’s Day will be less sexy, more self-care. Brands court customers with different tactics this Covid-tinged Valentine’s Day |Erika Wheless |February 11, 2021 |Digiday 

Above all, it’s important to remain exclusive with your pod both before and after Valentine’s Day. How to celebrate Valentine’s Day in a pandemic |Sara Kiley Watson |February 10, 2021 |Popular-Science 

Your partner will think of you every time they use these Valentine’s Day gifts. The best, most practical Valentine’s Day gifts for any kind of partner |PopSci Commerce Team |February 8, 2021 |Popular-Science 

When I get older losing my hair/Many years from now/Will you still be sending me a valentine/Birthday greetings, bottle of wine? Bruce Springsteen’s Bond Moment: The Boss’ Body Beautiful at 64 |Tim Teeman |July 25, 2014 |DAILY BEAST 

Actress Ellen Page came out of the closet on Valentine's Day at a Human Rights Campaign conference in Las Vegas. How Likable Is Alec Baldwin After His ‘New York Magazine’ Confessional? |Amy Zimmerman |February 26, 2014 |DAILY BEAST 

Your choice to teach love keeps my Valentine safer from hate. My Sexy American Sikh Valentine |Jesse Bawa |February 14, 2014 |DAILY BEAST 

Insert painfully true Netflix binge/Valentine's Day joke here. 'House of Cards' Returns, R. Kelly to Release New 'Trapped in the Closet' |Culture Team |December 4, 2013 |DAILY BEAST 

The most poignant moment comes when Richard starts crying after imagining he and Adele dancing to “My Funny Valentine.” 11 Wacky, Moving, Memorable ‘Grey’s Anatomy’ Moments (VIDEO) |Chancellor Agard |October 10, 2013 |DAILY BEAST 

But the poem opens with a call from a bird to all other birds, bidding them rejoice at the return of Saint Valentine's day. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

It is true that he dreams about Saint Valentine's day, but that is quite another matter. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Behind the altar in this chapel is an internationally famous white marble, recumbent "Figure of Lee" which Valentine created. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

So all by coach to my house, where I found my Valentine with my wife, and here they drank, and then went away. Diary of Samuel Pepys, Complete |Samuel Pepys 

"Almost the very same thing that you did, monsieur," replied Valentine, despondently. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue