You dedicated part of your book to covering Zionism—the belief that Jewish people deserve a state—and the dehumanization of Palestinians. The Troubling Consequences of Seeing Muslims as a Racial Group |Sanya Mansoor |December 13, 2021 |Time 

As Theodor Herzl, the father of modern Zionism, once said, "If you will it, it is no dream." Would Pro-Israel Billionaires Adelson and Saban Really Buy the NYT? |Eli Lake |November 9, 2014 |DAILY BEAST 

It opposes global capitalism, European integration and Zionism. In Hands of Hungarian Artist, Jewish Home Movies of the ’30s a Warning of Coming Holocaust |Daniel Genis |October 25, 2014 |DAILY BEAST 

None of it has lessened my belief in Zionism or the imperative of Israel as a home and sanctuary for Jews. Don’t Accuse Israel of Apartheid |Benjamin Pogrund |July 17, 2014 |DAILY BEAST 

From the very beginning there was a substantial contradiction between Zionism and Lydda. The Triumph and Tragedy of Ari Shavit’s My Promised Land |Sol Stern |February 9, 2014 |DAILY BEAST 

By evening,” Shavit writes, “Zionism has taken the city of Lydda. The Triumph and Tragedy of Ari Shavit’s My Promised Land |Sol Stern |February 9, 2014 |DAILY BEAST 

Rather strange of Zangwill, who is himself not a realist and has gone in for Zionism, to like Ibsen so much. Discourses of Keidansky |Bernard G. Richards 

Zionism, Socialism and Anarchism come up in turn, and so many trenchant and vital things are said on these subjects. Discourses of Keidansky |Bernard G. Richards 

Zionism, as a movement, has brought to fruition much of the latent love of the young Jew for his people and his religion. A Jewish Chaplain in France |Lee J. Levinger 

Indeed, until the time of Herzl all the most prominent protagonists of Zionism were Christians. Notes on the Diplomatic History of the Jewish Question |Lucien Wolf 

With the advent of Herzl, however, Zionism was no more a matter of domestic concern only. The Jewish State |Theodor Herzl