With an early coup — the first cartoonist they signed was Garry Trudeau, then a student cartoonist for the Yale Daily News and later of “Doonesbury” fame — their operation grew into the world’s largest independent newspaper syndicate. John McMeel, newspaper syndicator who brought ‘Doonesbury’ to millions of readers, dies at 85 |Emily Langer |July 21, 2021 |Washington Post 

If the syndicate knew that it would be on the hook for lawsuits, Pearson reasoned, it would force him to cut controversial accusations and to issue retractions in response to legal threats. A top columnist who exposed corruption — and sometimes betrayed his principles |Matthew Pressman |July 9, 2021 |Washington Post 

The money went out quickly but was sometimes taken advantage of by larger companies that did not need it and criminal syndicates that escaped detection. With evictions set to begin next month, hundreds of millions in Washington-area rental aid remains unspent |Anu Narayanswamy, Jonathan O'Connell, Marissa Lang, Kyle Swenson |June 4, 2021 |Washington Post 

Nigerian founders-turned-investors are now running syndicate fundsThis round is also a big step for Future Africa. Nigeria’s Termii raises $1.4M seed led by Future Africa and Kepple Africa Ventures |Tage Kene-Okafor |March 19, 2021 |TechCrunch 

Yet investigators received tips that pointed to the syndicate. The Murder Chicago Didn’t Want to Solve |by Mick Dumke |February 25, 2021 |ProPublica 

Héctor's older brothers Arturo and Alfredo were men with the right temperament to preside over a multinational crime syndicate. Trading Dime Bags for Salvador Dali |Jason McGahan |October 19, 2014 |DAILY BEAST 

Seventeen months later, a recently awoken Kennex is obsessed with figuring out how The Syndicate planned the ambush. ‘Almost Human’ Review: A Dystopian Future That We’ve Seen Before |Chancellor Agard |November 17, 2013 |DAILY BEAST 

Mahmoud Abbas and his Fatah syndicate still wielded the political power. Stop Blaming Israel And America For Fayyad’s Fall |Abraham Katsman |April 29, 2013 |DAILY BEAST 

Newsmax pays to syndicate their columns, and their stature lends the site credibility. The Right-Wing Money Model |Justin Green |January 7, 2013 |DAILY BEAST 

It's all done on behalf of a syndicate, in which "everybody has a share." David's Bookclub: Catch-22 |David Frum |December 29, 2012 |DAILY BEAST 

Bidault was one of the syndicate that engineered the bankruptcy of Birotteau in 1819. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Girra was a powerful figure in the metropolitan pin-ball game syndicate and had a piece of the number policy racket too. Hooded Detective, Volume III No. 2, January, 1942 |Various 

He was on his way to pay the money to the heads of a syndicate in control of Chicago's gambling concession. Hooded Detective, Volume III No. 2, January, 1942 |Various 

These men agreed to form with the Burbages a syndicate to finance the erection of a new playhouse. Shakespearean Playhouses |Joseph Quincy Adams 

They talked of Crozier's land deal and syndicate as they walked slowly towards the house. You Never Know Your Luck, Complete |Gilbert Parker