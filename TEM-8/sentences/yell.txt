They were across the street from a fire station, close enough for his daughter to yell for help. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

Sometimes I want to yell, "STOP MAKING EVERYTHING ABOUT YOU." The Unbearable Whiteness of Protesting |Rawiya Kameir, Judnick Mayard |December 10, 2014 |DAILY BEAST 

Excerpted from Rebel Yell: The Violence, Passion, and Redemption of Stonewall Jackson by S.C. Gwynne. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

Really, how often would someone yell “rape,” especially when her children are in the house? Should Twitter Suspend LGBT Engineer Accused Of Raping Her Wife? |Emily Shire |October 8, 2014 |DAILY BEAST 

The House freshman from Yell County is in a dead heat with Sen. Mark Pryor in the Arkansas Senate race. The Wall Street Money Men Behind a Right-Wing Star |Patricia Murphy |September 26, 2014 |DAILY BEAST 

A terrific yell of rage burst from every one, and each hastily threw something or other at the bold intruder. Hunting the Lions |R.M. Ballantyne 

Chumru, though no fighting-man, realized that he was expected to make a row and uttered a bloodcurdling yell. The Red Year |Louis Tracy 

A female yell issued from the alley as he came up, and Mrs Rampy suddenly appeared in a state of violent self-assertion. The Garret and the Garden |R.M. Ballantyne 

Immediately the little girl set up a yell that, 195 as Burd declared, could have scarcely been equaled by a steam calliope. The Campfire Girls of Roselawn |Margaret Penrose 

A hideous yell of applause rose from the multitude, and again he plunged his saber into the carriage. Madame Roland, Makers of History |John S. C. Abbott