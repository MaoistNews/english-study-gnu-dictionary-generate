When you wear a mask – even a cloth mask – you typically are exposed to a lower dose of the coronavirus than if you didn’t. Cloth Masks Do Protect The Wearer – Breathing In Less Coronavirus Means You Get Less Sick |LGBTQ-Editor |August 20, 2020 |No Straight News 

Surgical masks are not as protective as N95s, but they do shield the wearer from droplets and fluids better than the now ubiquitous cloth masks—3% to 25% better, depending on the study. Unmade in America |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

Although all trainees and staff were required to wear cloth masks, campers were not. Coronavirus outbreak at camp infected more than 200 kids and staff |Erin Garcia de Jesus |August 4, 2020 |Science News For Students 

Many cloth masks you can buy today are made of simple cotton, but the World Health Organization recommends that non-medical fabric masks have three layers of fabric, each made of a different material to serve a different protective role. The WHO’s guide to making more effective reusable masks |Katherine Ellen Foley |July 18, 2020 |Quartz 

In addition to paper, kirigami can reshape sheets of wood, cloth or metal. Shape-shifting cuts give shoes a better grip |Carolyn Wilke |July 14, 2020 |Science News For Students 

Paper and cloth are cheap, what people are paying for is the story. Glenn Beck Is Now Selling Hipster Clothes. Really. |Ana Marie Cox |December 20, 2014 |DAILY BEAST 

The figure enters the elevator and is then seen quickly leaving the mall, black cloth flapping behind it. Middle East Murder Mystery: Who Killed an American Teacher in Abu Dhabi? |Chris Allbritton |December 3, 2014 |DAILY BEAST 

Waving a silk cloth, he declared, “Gentlemen, I will have this land just as surely as I now have this handkerchief.” Washington’s Wheeler-Dealer Patriotism |William O’Connor |October 31, 2014 |DAILY BEAST 

No piece of cloth throughout history has sparked more controversy as the veil. Saudi Activist Manal Al-Sharif on Why She Removed the Veil |Manal Al Sharif, Advancing Human Rights |October 30, 2014 |DAILY BEAST 

Novelty aside, the real question is whether these avowedly chaste men of the cloth are listening. The Vatican's Same-Sex Synod: The Bishops Hear About Reality. Do They Listen? |Barbie Latza Nadeau |October 12, 2014 |DAILY BEAST 

He thrust the Cardinal's mantle into it, and stood over the smouldering cloth, till the whole was consumed to ashes. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Her eyes were not nearly as soft as they had been, while she picked up the hanging folds of pink cloth, and went on. Rosemary in Search of a Father |C. N. Williamson 

Great had been her indignation at the want of respect shown to the Reverend John Dodd's cloth. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Ramona had covered the box with white cloth, and the lace altar-cloth thrown over it fell in folds to the floor. Ramona |Helen Hunt Jackson 

Instead of a cloth, on each table was a sheet of fine glazed paper which had the appearance of oiled silk. Our Little Korean Cousin |H. Lee M. Pike