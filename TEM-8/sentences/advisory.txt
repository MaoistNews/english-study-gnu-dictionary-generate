Calvert, meanwhile, did not disclose that he once held an unpaid position on the advisory board of Hayungs’ company, MRG Medical. The COVID-19 Charmer: How a Self-Described Felon Convinced Elected Officials to Try to Help Him Profit From the Pandemic |by Vianna Davila, Jeremy Schwartz and Lexi Churchill |September 25, 2020 |ProPublica 

A permanent advisory group was also set up by the surgeon general to provide guidance in vaccine production and testing. Here’s What Happens When You Try to Scale Up Vaccine Production Too Quickly |Fiona Zublin |September 18, 2020 |Ozy 

One was followed by a former member of Congress, Republican Tim Huelskamp of Kansas, who is on the Catholics for Trump advisory board. Pro-Trump youth group enlists teens in secretive campaign likened to a ‘troll farm,’ prompting rebuke by Facebook and Twitter |Isaac Stanley-Becker |September 15, 2020 |Washington Post 

The report also recommends establishing an international scientific advisory board to evaluate the state of the technology and consult on applications to do such heritable or germline editing. Strict new guidelines lay out a path to heritable human gene editing |Tina Hesman Saey |September 3, 2020 |Science News 

Wealth and investment banking advisory, the biggest growth areas, both posted record revenue in the first six months, driven by growth in China as markets recovered. Credit Suisse aims to double its headcount in China in 5 years |Claire Zillman, reporter |August 31, 2020 |Fortune 

Indeed, as an almost purely advisory firm, Lazard is (appropriately) barely affected by the Dodd-Frank reforms. Antonio Weiss Is Not Part of the Problem |Steve Rattner |January 7, 2015 |DAILY BEAST 

And if he is re-elected, the House advisory rules prohibiting him from voting no longer apply. The Felon Who Wouldn’t Leave Congress |Ben Jacobs, David Freedlander |December 23, 2014 |DAILY BEAST 

On Nov. 13, after years of deliberation, an advisory panel finally recommended lifting the ban, sort of. The Outrageous Celibacy Requirement for Gay Blood Donors |Jay Michaelson |November 22, 2014 |DAILY BEAST 

The whole stack was re-evaluated—a “one-time decision,” said a memo from the advisory council, due to “extraneous circumstances.” At This Creepy Libertarian Charter School, Kids Must Swear ‘to Be Obedient to Those in Authority’ |ProPublica |October 15, 2014 |DAILY BEAST 

To this end, a robust Special Operations Forces presence beyond simply a modest advisory effort should be sent to Iraq. Lessons From Fallujah, Then and Now |Dr. Daniel R. Green |July 26, 2014 |DAILY BEAST 

In connection with the advisory committee's recommendation that the depth of lifeboats shall not exceed 44 per cent. Loss of the Steamship 'Titanic' |British Government 

The advisory board, men who drove past in milk wagons, condemned it as a piece of folly. A Yankee from the West |Opie Read 

You all know something of the scheme; it has been heard and passed as feasible by the Advisory Group. Greylorn |John Keith Laumer 

The policies of the National Trades' Union instead of merely advisory were henceforth to be binding. A History of Trade Unionism in the United States |Selig Perlman 

The field of this advisory council is indicated in a series of memoranda presented to him in January, 1917. Harper's Pictorial Library of the World War, Volume XII |Various