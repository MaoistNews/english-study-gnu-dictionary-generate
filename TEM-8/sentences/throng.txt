The demonstrations were entirely different scenes from this past March, when hundreds of protesters, including children, thronged the city’s streets to call for a full reopening of schools. Back to School, Ready or Not |Eromo Egbejule |August 27, 2021 |Ozy 

Luxury cars full of families traverse the island, while throngs of others walk or ride bikes on the tree-lined streets. A vacation town promises rest and relaxation. The water knows the truth. |Nneka M. Okona |August 26, 2021 |Vox 

When the person falls — and they usually do — throngs of people who’ve helped make the social media spectacle known as the Milk Crate Challenge the latest viral phenomenon proceed to sing a chorus of “Oh!” The viral Milk Crate Challenge has left people injured. Doctors are begging them to stop. |Timothy Bella |August 24, 2021 |Washington Post 

With its loose application requirements, PUA instantly drew throngs of scammers. How Unemployment Insurance Fraud Exploded During the Pandemic |by Cezary Podkul |July 26, 2021 |ProPublica 

In a similar vein, the companies have suspended or modified activities and attractions that draw throngs of spectators. Theme parks aim to keep visitors safe — and screaming |Andrea Sachs |May 21, 2021 |Washington Post 

The throng took a collective breath before retreating back behind their office doors. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 

Cuomo shouted to the throng of elected officials gathered at the head of the parade. Andrew Cuomo Can't Ignore It Now: He's Weak Even at Home |David Freedlander |September 10, 2014 |DAILY BEAST 

She said the drug bridge drew the predictable sensationalism from a press throng that took its drug use cues from tamer festivals. A Report From the Misunderstood Gathering of the Juggalos |Steve Miller |July 28, 2014 |DAILY BEAST 

With that, he took a huff off a morning joint and moved into the throng of jovial patrons. A Report From the Misunderstood Gathering of the Juggalos |Steve Miller |July 28, 2014 |DAILY BEAST 

I threaded my way through the silent throng of spectators, but was stopped at Fourth Street by a cordon of police. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

Above, great standard electric lamps shed their white glare upon the eddying throng casting a myriad of grotesque shadows. The Joyous Adventures of Aristide Pujol |William J. Locke 

It was eleven o'clock when he went out and joined the throng of people sunning themselves on the walk beside the lake. The Wave |Algernon Blackwood 

Down in the hall a great throng of guests passed from the room into the garden and back again. The Weight of the Crown |Fred M. White 

Lamb worked his way up into the throng and got a glimpse of the other guy getting stiff on the backroom floor. Hooded Detective, Volume III No. 2, January, 1942 |Various 

A throng of young girls, gleaning, followed the reapers and raked up the ears that fell. Frdric Mistral |Charles Alfred Downer