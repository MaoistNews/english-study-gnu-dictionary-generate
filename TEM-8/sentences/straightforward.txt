The argument for defunding, however, is fairly straightforward. What ‘defund the police’ really means |Simon Balto |February 9, 2021 |Washington Post 

Keep your filenames descriptive and straightforward and you’ll be ahead of the game. Image SEO: Best practices and tips for optimization |Michael McManus |February 8, 2021 |Search Engine Watch 

Recruiting a new developer with the brief “can be based anywhere” seemed like a straightforward role to fill for marketing industry specialist Sphere Digital Recruitment. ‘We’ll get harder and faster policies around pay’: The future of remote working on big city salaries |Jessica Davies |February 8, 2021 |Digiday 

Because I couldn’t remember anything about the time before the accident, it seemed like the most straightforward answer. What happens to our lost childhood memories? Motherhood sent me looking for answers. |Missy Ryan |February 4, 2021 |Washington Post 

Talk show host Wendy Williams is known for her straightforward and unfiltered nature, especially when it comes to interviewing celebrity guests. A Look Back At Wendy Williams’ Most Talked About Interviews |Steven Psyllos |February 2, 2021 |Essence.com 

“He has always been very straightforward with me,” Obama said of his relationship with the GOP leader. In Press Conference, Obama Turns Conciliatory—Mostly |Eleanor Clift |November 6, 2014 |DAILY BEAST 

Most have been straightforward cases where the child came in with the characteristic rash. Predator Doctors Take Advantage of Patients With ‘Chronic Lyme’ Scam |Russell Saunders |September 19, 2014 |DAILY BEAST 

The purpose of the ISIS threats are less straightforward than they may seem. Don't Start a War Over a Hostage |Jacob Siegel |August 27, 2014 |DAILY BEAST 

Pretty damn straightforward: Elect, and reelect, a Democratic president. Only Eight Years of President Hillary Can Take the Supreme Court Away From Conservatives |Michael Tomasky |June 30, 2014 |DAILY BEAST 

Raeburn is a science reporter, and much of what he does in this book is straightforward reporting. Do Hands-On Dads Raise Healthier Kids? |Andy Hinds |June 15, 2014 |DAILY BEAST 

Here was a return for his frankness—his straightforward conduct—his unequalled liberality. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

“Mr. Pickwick, I thank you most heartily for all your kindness to my son,” said old Mr. Winkle, in a bluff straightforward way. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

In private life, all who knew him agreed as to his sympathetic, jovial, and straightforward character. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

This is the last will of Mr. Faversham—a plain, straightforward will in many ways, although slightly involved in others. The Everlasting Arms |Joseph Hocking 

The steam-tug had not to contend with the ordinary straightforward rush of a North Sea storm. The Floating Light of the Goodwin Sands |R.M. Ballantyne