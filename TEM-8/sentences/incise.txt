You can seem to incise lines on it, and they look for all the world like they’re real, but with a little bit of shaking up, you can make significant changes. 5 Psychology Terms You’re Probably Misusing (Ep. 334 Rebroadcast) |Stephen J. Dubner |January 9, 2020 |Freakonomics 

The usual plan is to prick or incise each lesion and press out the contents. Essentials of Diseases of the Skin |Henry Weightman Stelwagon 

Put to the doors a while there; ye can incise To a hairs breadth without defacing.Sur. The Mad Lover |Francis Beaumont 

Winding streams on plains may thus incise their meanders in solid rock as the plains are gradually uplifted. The Elements of Geology |William Harmon Norton 

Care must be taken not to let the knife slip at the end of the incision and so incise the neck tissues. A System of Operative Surgery, Volume IV (of 4) |Various 

The next step is to incise the sinus freely from above downwards towards the jugular fossa and curette out the thrombus. A System of Operative Surgery, Volume IV (of 4) |Various