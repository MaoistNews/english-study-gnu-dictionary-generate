Mank ties the reason for Sinclair’s loss to a newsreel played in movie theaters that mixes truth and fiction. The tangled history behind David Fincher’s Mank — and its link to Citizen Kane — explained |Alissa Wilkinson |December 4, 2020 |Vox 

The first stretch of the film announces Kane’s death and tells the story of his life in the style of a newsreel, the sort that would have played before a feature film in a movie theater, as was common at the time. The tangled history behind David Fincher’s Mank — and its link to Citizen Kane — explained |Alissa Wilkinson |December 4, 2020 |Vox 

The newsreel footage in Watchers of the Sky follows columns of refugees fleeing war, suitcases and small children in their arms. The Man Who Invented the Word ‘Genocide’ |Nina Strochlic |November 19, 2014 |DAILY BEAST 

Watch the newsreel below and observe a fabled minute in the life and times of Marty Reisman, who died on Friday at age 81. Marty Reisman: The Magical Hustler Who Saved a Classic Game |Harold Evans |December 10, 2012 |DAILY BEAST 

For historical shots, he relied on newsreel footage that he sepia-toned and inserting similar-toned footage of his actors. The Wonderful ‘Hemingway & Gellhorn:’ Nicole Kidman, Clive Owen, and the HBO Movie |Allen Barra |May 28, 2012 |DAILY BEAST 

Some of the best newsreel footage was shot by our greatest film directors, John Ford and John Huston. Diving Into The Pacific |Allen Barra |May 6, 2010 |DAILY BEAST 

This book is more like an early newsreel, with no search for larger historical causes. The Best of Brit Lit |Peter Stothard |September 23, 2009 |DAILY BEAST 

There were newsreel shots of V-1 and V-2 being blasted from their takeoff ramps and a montage of later experimental models. The Old Die Rich |Horace Leonard Gold 

One day I entered a motion picture house to view a newsreel of the European battlefields. Autobiography of a YOGI |Paramhansa Yogananda 

The horror of the struggle, filled with the dead and dying, far surpassed in ferocity any representation of the newsreel. Autobiography of a YOGI |Paramhansa Yogananda 

Reporters and newsreel cameramen swarmed over my quarters at Grosvenor House. Autobiography of a YOGI |Paramhansa Yogananda 

I asked him, referring to the flight he had spoken about in the newsreel. Test Pilot |David Goodger (goodger@python.org)