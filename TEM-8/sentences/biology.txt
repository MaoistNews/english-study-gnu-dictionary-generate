Gunnar and others hope to reveal more of the underlying biology behind the reboot. Puberty can repair the brain’s stress responses after hardship early in life |Esther Landhuis |August 28, 2020 |Science News 

This study also opens the door for other control measures that alter the biology of locusts themselves. A single molecule may entice normally solitary locusts to form massive swarms |Jonathan Lambert |August 12, 2020 |Science News 

It expects women and men to behave very differently, from birth forward, simply on the basis of their biology. Gender Is What You Make of It - Issue 88: Love & Sex |Charles King |August 5, 2020 |Nautilus 

Now researchers have proposed a new learning method more closely tied to biology, which they think could help us approach the brain’s unrivaled efficiency. A New Brain-Inspired Learning Method for AI Saves Memory and Energy |Edd Gent |July 27, 2020 |Singularity Hub 

One key contender is CRISPR, the fast-advancing gene-editing technology that stands to revolutionize synthetic biology and treatment of genetically linked diseases. How AI Will Make Drug Discovery Low-Cost, Ultra-Fast, and Personalized |Peter H. Diamandis, MD |July 23, 2020 |Singularity Hub 

For his tireless assault on evolutionary biology and downsizing the deity to fit within science, I give Meyer second place. 2014: Revenge of the Creationists |Karl W. Giberson |December 27, 2014 |DAILY BEAST 

Complementarity as conservative Catholics use the term, however, is more than biology. Is Pope Francis Backpedaling on Gays? |Jay Michaelson |November 19, 2014 |DAILY BEAST 

“In the long term, I am more worried about biology,” he told The Telegraph. The Other Side of Stephen Hawking: Strippers, Aliens, and Disturbing Abuse Claims |Marlow Stern |November 6, 2014 |DAILY BEAST 

Essentially he is arguing that there are functional trade-offs in developmental biology. Why Aristotle Deserves A Posthumous Nobel |Nick Romeo |October 18, 2014 |DAILY BEAST 

People are starting to recognize that depression must relate to biology, because who would give up such an outwardly gifted life? We're Talking About Depression All Wrong |Jean Kim |August 20, 2014 |DAILY BEAST 

Its backbone should be the study of biology and its substance should be the threshing out of the burning questions of our day. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

“Botany is that branch of biology which treats of plant life” has in it the same error. English: Composition and Literature |W. F. (William Franklin) Webster 

“Biology” is not so well understood as “botany,” though it is a more general term. English: Composition and Literature |W. F. (William Franklin) Webster 

It follows that biology is the foundation rather than the house, if we may use so crude a figure. Taboo and Genetics |Melvin Moses Knight, Iva Lowther Peters, and Phyllis Mary Blanchard 

It is time to abandon the notion that biology prescribes in detail how we shall run society. Taboo and Genetics |Melvin Moses Knight, Iva Lowther Peters, and Phyllis Mary Blanchard