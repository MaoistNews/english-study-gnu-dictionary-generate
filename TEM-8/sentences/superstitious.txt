For some, Jacques Le Gris became a martyr, a man sentenced to a brutal death by a backwards and superstitious legal system which demonized him. The True Story Behind The Last Duel—and History’s Attempt to Erase It |John-Paul Heil |October 15, 2021 |Time 

There are all of these various sorts of iterations that go into people’s minds, some logical, some sort of more superstitious almost. Dr. Sanjay Gupta Now Knows the World Better by Seeing How People Evaluate Risk |Pallabi Munsi |April 23, 2021 |Ozy 

We humans are notoriously unreliable, superstitious narrators, always scanning the horizon for signs that validate what our hearts have already told us. I Found a Rainbow At the End of My Hunt For a Vaccine Appointment |Susanna Schrobsdorff |March 28, 2021 |Time 

When we can barely illuminate our own world, it would be superstitious to imagine that dead men could do it for us. American Democracy Under Threat for 250 Years |Jedediah Purdy |December 28, 2014 |DAILY BEAST 

For artists, that moral sensibility, superstitious or no, ought to be cranked to 11. The Strange World of Political Assassination Fantasies |James Poulos |September 24, 2014 |DAILY BEAST 

Their marriage had begun to suffer, and memories of the polio ballet loomed over the choreographer, known to be superstitious. The Tragic Downfall of Tanaquil Le Clercq, Ballet’s Greatest Muse |Nancy Buirski |February 3, 2014 |DAILY BEAST 

These days, Greaves regards traditional religion in general as both dangerously superstitious and exclusionary. Satan Is Coming to Oklahoma |Michelle Cottle |December 10, 2013 |DAILY BEAST 

Historically, superstitious investors have feared the 10th month of the year. Washington Drama Makes October a Confusing Month for Investors |William O’Connor |October 1, 2013 |DAILY BEAST 

And yet there still remains a superstitious belief in prayer, and most surprising are some of its manifestations. God and my Neighbour |Robert Blatchford 

He told how the Korean farmer lived a simple, patient life, while at the same time he was ignorant and superstitious. Our Little Korean Cousin |H. Lee M. Pike 

They emanated from a credulous and superstitious people in an unscientific age and country. God and my Neighbour |Robert Blatchford 

"Look here, old man, this superstitious nonsense is becoming an obsession to you," it said one fine April morning. Uncanny Tales |Various 

People of limited education, born and brought up in out of the way country places, are apt to be superstitious. The World Before Them |Susanna Moodie