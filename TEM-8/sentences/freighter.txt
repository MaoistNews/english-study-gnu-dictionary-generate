At first, these so-called “cabin freighters” airlifted ventilators, masks, and other crucial medical supplies to hard-hit hospitals. Air Canada is gearing up for a new phase of the pandemic |Nicolás Rivero |February 3, 2022 |Quartz 

Cathay Pacific said the three individuals were freighter pilots who flew cargo, not passenger, planes. Delta variant dangerous during pregnancy, CDC reports say |Andrew Jeong, Adela Suliman, Laurie McGinley, Paulina Villegas |November 19, 2021 |Washington Post 

The world's maritime industry — from ferries to freighters — is trying to navigate a once-in-a-century transition away from fossil fuels to new, cleaner means of propulsion. Ships search for green fuels to keep oceans blue |Joann Muller |March 5, 2021 |Axios 

Lightning flashes are more frequent along shipping routes, where freighters emit particulates into the air, than in the surrounding ocean. Storm force |Katie McLean |February 24, 2021 |MIT Technology Review 

There are many other coastal freighter types that are similar in size and energy demand.’ This Week’s Awesome Tech Stories From Around the Web (Through February 20) |Singularity Hub Staff |February 20, 2021 |Singularity Hub 

The 7,000-ton freighter is still stocked with sake bottles and four fighter planes, and it is encircled by gray reef. A WWII Battle Frozen in Time |Nina Strochlic |May 14, 2014 |DAILY BEAST 

The red bottom-plates of a submarine freighter came rolling up behind the surge to show how futile was the might of man. Astounding Stories, May, 1931 |Various 

Jandron only means that Miss Mallen is much more comfortable on this passenger-ship than she'd be in your freighter. The Sargasso of Space |Edmond Hamilton 

Ummm—seems so—wasn't she the big freighter that disappeared many years ago? Security |Poul William Anderson 

The freighter had a long black-snake whip, and was snapping it about the ears of the leaders. Motor Matt's Daring, or, True to His Friends |Stanley R. Matthews 

The two machines glided up the slope, leaving the baffled and swearing freighter far behind. Motor Matt's Daring, or, True to His Friends |Stanley R. Matthews