Officials there told Kaper that mothers who chose anonymity at the time of relinquishing their children couldn’t be revealed, and have denied his requests. Despite mixed results, south Asian adoptees turn to DNA tests |Bhavya Dore |October 20, 2020 |Quartz 

On top of this, my ex was able to post false information about me on “liars and cheaters” forums enabled by Section 230 of the Communications Decency Act, which allows digital platforms to relinquish ultimate responsibility over user-posted content. America’s backwards privacy laws leave women vulnerable |jakemeth |October 12, 2020 |Fortune 

Trusting creators to know their audience and deliver great content means it’s OK for the brand to relinquish a degree of control. Deep Dive: How the Summer of 2020 forced brand marketing to change for the better |jim cooper |September 14, 2020 |Digiday 

Still despite some recent forced innovation, journalists in particular can be a symbolic bunch, reticent to relinquish time-honored traditions, processes and language more reminiscent of the inky fingered days when the print product reigned supreme. The closure of newsrooms is a symbolic end of a publishing era |Lara O'Reilly |August 18, 2020 |Digiday 

When an employee walks into a meeting they are relinquishing control. How to Make Meetings Less Terrible (Ep. 389 Rebroadcast) |Stephen J. Dubner |May 28, 2020 |Freakonomics 

To be a hero, you must accept your fate and relinquish control to it. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

She has yet to relinquish her anonymity and is unavailable for any comment. Alleged U.Va. Abductor Accused of Rape at Christian College |Michael Daly |September 28, 2014 |DAILY BEAST 

He would not relinquish presidential power and live to regret it, like his cousin. From The Square Deal to The New Deal: The Overlapping Political Identities of TR and FDR |John Avlon |September 9, 2014 |DAILY BEAST 

Should I relinquish my passport and book permanent passage on an infantilizing escorted “adventure” to the Mall of America next? I Can’t Shake Hawaii: An Ode to Returning to Places You’ve Been Before |Debra A. Klein |October 7, 2013 |DAILY BEAST 

But it was clear from the start that no matter how strongly he was challenged, Morsi would never relinquish the presidency. How to Take Down a President |Mike Giglio |July 17, 2013 |DAILY BEAST 

Gradually he began to feel a little sheepish, but nevertheless he did not relinquish his desire to break up the service. The Chequers |James Runciman 

Was it certain that the united force of all her neighbours would be sufficient to compel her to relinquish her prey? The History of England from the Accession of James II. |Thomas Babington Macaulay 

I resolved, rather than act further, to relinquish the handsome payments he made to me from time to time. The Doctor of Pimlico |William Le Queux 

Several instances have proved the people are determined to relinquish the barbarous system of commandoes for stealing cattle. Robert Moffat |David J. Deane 

The men left their floating prisons only to relinquish comfort and to "rough it." The British Expedition to the Crimea |William Howard Russell