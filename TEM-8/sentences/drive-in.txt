This is a guy who has his son-in-law clean his eyeglasses, for crying out loud. Will Chris Christie Regret His Cowboy Hug? |Matt Lewis |January 5, 2015 |DAILY BEAST 

Her travel clique has been known to arrive at an airport, bags packed, passport-in-hand, within hours of spotting a deal. ‘We Out Here’: Inside the New Black Travel Movement |Charlise Ferguson |January 4, 2015 |DAILY BEAST 

He used to drive her to school once he came home from the Marines. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The hope was that greater transparency about performance would drive results. The ‘No Child’ Rewrite Threatens Your Kids’ Future |Jonah Edelman |January 3, 2015 |DAILY BEAST 

You would only see it for a second, but it would drive you forward. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

Then with your victorious legions you can march south and help drive the Yankee invaders from the land. The Courier of the Ozarks |Byron A. Dunn 

Wordsworth has illustrated how an unwise and importunate demand for a reason from a child may drive him into invention. Children's Ways |James Sully 

Give not up thy heart to sadness, but drive it from thee: and remember the latter end. The Bible, Douay-Rheims Version |Various 

If they will come to our villages and drive us out a hundred at a time, what would they do to one man alone? Ramona |Helen Hunt Jackson 

Such throats are trying, are they not?In case one catches cold; Ah, yes! Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various