Under these conditions it is again hidden beneath heterogeneous dogmas, obscure comments, and factitious explanations. My Religion |Leo Tolstoy 

But before he had walked half a square, the factitious energy inspired by drink deserted him. File No. 113 |Emile Gaboriau 

The entire structure of their social life reposed on feelings, and not on a factitious conception, on a work of art. The Aesthetical Essays |Friedrich Schiller 

His mind turns towards combinations, arrangements, legal or factitious organisations. Essays on Political Economy |Frederic Bastiat 

The prices of farms and improvements vary greatly, and are influenced much by factitious and local circumstances. A New Guide for Emigrants to the West |J. M. Peck