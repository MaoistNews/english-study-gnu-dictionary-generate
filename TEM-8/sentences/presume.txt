Now, if you head over to California’s database again, you will find filings going back to 2016, which I presume is the five-year limit for the site’s search function. PSA: Most aggregate VC trend data is garbage |Danny Crichton |February 9, 2021 |TechCrunch 

So I presumed there must be a lot of micrometeorites in the dust, and I was right. What Dust From Space Tells Us About Ourselves |Natalie Wolchover |February 4, 2021 |Quanta Magazine 

Lawmakers even presumed that anyone receiving the now-celebrated Pell Grants would have to borrow to pay for what those limited scholarships did not cover. Even forgiving student loans won’t solve the higher education funding crisis |Elizabeth Shermer |January 22, 2021 |Washington Post 

His face—presuming it’s a man—is covered by a mask, and the hood of his sweatshirt obscures the rest of his head. A pair of rare Nike sneakers may be the key to catching a Washington riot suspect |Marc Bain |January 12, 2021 |Quartz 

It can’t be presumed that developers know about BundlePhobia, Lighthouse, or SEO for that matter. Google’s Lighthouse is now recommending JavaScript library alternates |Detlef Johnson |January 5, 2021 |Search Engine Land 

The next month she married the man who, it is safe to presume, was her final husband. Beware of Japan’s “Black Widows” |Jake Adelstein |November 20, 2014 |DAILY BEAST 

But it is a mistake to presume that because these voters are Obama loyalists they are Democratic Party loyalists. Is This the Beginning of the End for Blacks and Dems? |Keli Goff |November 3, 2014 |DAILY BEAST 

I presume most Republicans will be clever enough to mute impeachment talk before November. The GOP’s Audacious Impeachment Spin Job |Michael Tomasky |July 30, 2014 |DAILY BEAST 

Somebody (a monk, I presume) has put a dummy dressed in a guard's uniform inside. Pablo Escobar’s Private Prison Is Now Run by Monks for Senior Citizens |Jeff Campagna |June 7, 2014 |DAILY BEAST 

How far that may be is unclear, though we can only presume that it would involve jail time for those tracked down. Can a Tweet Put You in Prison? It Certainly Will in the UK |Michael Moynihan |January 23, 2014 |DAILY BEAST 

I presume the twenty-five or thirty miles at this end is unhealthy, even for natives, but it surely need not be so. Glances at Europe |Horace Greeley 

I presume this path does not extend many miles without meeting impediments. Glances at Europe |Horace Greeley 

I presume there will be more middling and half middling yields within twenty miles of Paris than in all Belgium. Glances at Europe |Horace Greeley 

That Lannes would have emerged superior to these trials his previous career affords strong reason to presume. Napoleon's Marshals |R. P. Dunn-Pattison 

I presume you know that Maria Theresa was a first-rate soldier; or, at least, she had the happy art of finding them. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various