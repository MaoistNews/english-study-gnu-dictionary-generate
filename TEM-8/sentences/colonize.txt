What home canning entails, in the most rudimentary of explanations, is the use of heat to create enough pressure to seal your canning jars and prevent bacteria from colonizing its contents. I found an old-school technique to conquer my jam fears. Then I tried to figure out if it’s safe. |Charlotte Druckman |August 26, 2021 |Washington Post 

These tools open up new markets and new ways to extract resources, but what the innovator sees as progress often brings unwanted change to communities colonized by imported technologies and their makers’ ambitions. The dangerous appeal of technology-driven futures |Sheila Jasanoff |June 30, 2021 |MIT Technology Review 

As inconceivable as it still sounds, the wheels have been set in motion for humans to one day reach and colonize Mars. These Mice Were Born From Sperm That Spent Almost 6 Years in Space |Vanessa Bates Ramirez |June 16, 2021 |Singularity Hub 

Humanity’s attempt to colonize the moon will give us a good sense of the challenges we might face on Mars. When Will the First Baby Be Born in Space? |Chris Impey |June 4, 2021 |Singularity Hub 

Developed countries must also boost financial support for the developing world, where billions of people in formerly-colonized countries bear the disproportionate impacts from climate-related disasters. There is No Climate Justice Without Racial Justice |Manish Bapna |May 3, 2021 |Time 

“We are not saying we need to colonize the moon or anything crazy like that,” he added. Wisconsin’s GOP Secession Panic |David Freedlander |May 2, 2014 |DAILY BEAST 

But executives have a lot to do with the larger agenda to emasculate and colonize. CeeLo and Goodie Mob on Their Comeback, Kanye West’s ‘Emotional Problems,’ More |Marlow Stern |August 13, 2013 |DAILY BEAST 

Coastal construction gives them more places for their polyp stages to colonize. Beware at the Beach, the Jellyfish Rule the Seas and It’s Our Fault |Lisa-ann Gershwin |June 20, 2013 |DAILY BEAST 

The situation will remain the same: Israel will continue to illegally colonize just as it has for decades. Levy is Right |Yousef Munayyer |July 13, 2012 |DAILY BEAST 

Her ancestors were Englishmen long ago, who upon the very British idea of colonialism left to colonize the Barbados. This Week’s Hot Reads: May 7, 2012 |Jimmy So |May 8, 2012 |DAILY BEAST 

There were over a thousand people in this expedition that was going out to colonize Oregon for the United States. Historic Adventures |Rupert S. Holland 

She cannot be assailed with success at home, and she has no need to leave her own territories in search of lands to colonize. The Crime Against Europe |Roger Casement 

Theyll have their atomic war pretty soon, and leave us a nice high-radiation planet to colonize. They Also Serve |Donald E. Westlake 

The border is an excellent place in which to colonize native or other interesting plants. The Practical Garden-Book |C. E. Hunn 

The next expedition seems to have been a project to colonize the country. Great Events in the History of North and South America |Charles A. Goodrich