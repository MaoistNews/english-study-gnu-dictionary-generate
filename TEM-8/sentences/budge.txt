The cost per transistor—which once fell at the same exponential rate that transistor density increased—hasn’t budged for more than three generations of chipmaking. Why it’s a mistake to bet against Silicon Valley |Katie McLean |February 24, 2021 |MIT Technology Review 

Several other WallStreetBets believers said they aren’t budging, regardless of the damage. As GameStop stock crumbles, newbie traders reckon with heavy losses |Drew Harwell |February 2, 2021 |Washington Post 

Temperatures have fallen into the upper 20s to near 30 and may not budge a whole lot overnight. After 2 to 4 inches of snow, an icy night ahead in D.C. area. Snow showers possible Monday. |Jason Samenow, Dan Stillman, Andrew Freedman |February 1, 2021 |Washington Post 

After months of barely budging, rates rose significantly in early January at their strongest pace since the spring. Mortgage rates dip as investors await actions of new administration |Kathy Orton |January 21, 2021 |Washington Post 

McConnell is engaging the negotiators even though he hasn’t budged. Bipartisan stimulus deal in jeopardy as McConnell refuses to back package |kdunn6 |December 8, 2020 |Fortune 

During his budge speech, Treasurer Joe Hockey proclaimed: “The age of entitlement is over.” Australia Wants to Open the Great Barrier Reef to Dumping |Kirsten Alexander |June 2, 2014 |DAILY BEAST 

Nothing, it seems, will budge them from their opposition to give-and-take of governance. Rubio's Immigration Flip-Flop |Jamelle Bouie |October 29, 2013 |DAILY BEAST 

The Democratic presidential administration refuses to budge, and the government is shut down. ‘The West Wing’ Government Shutdown Episode Is Frighteningly Familiar |Marlow Stern |October 2, 2013 |DAILY BEAST 

In this crisis, there were no hands on deck—no one willing to budge one iota from their respective ideological corners. Ex-Republican Congressman: Shutdown Is a ‘Dangerous Game’ |Steve LaTourette |September 30, 2013 |DAILY BEAST 

The administration refused to budge on calling a spade a spade. Time To Cut Off Egypt |Ali Gharib |August 14, 2013 |DAILY BEAST 

Hamilton sprang to his aid and did his utmost to effect his release; but, powerful as he was, he could not budge him. Famous Adventures And Prison Escapes of the Civil War |Various 

And before I could budge she throws her arms around my neck and told me to say it again, say it again, say it again! Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 

Budge is lambskin with the wool dressed outwards, worn on the edge of the hoods of bachelors of arts, etc. Milton's Comus |John Milton 

"Don't you budge an inch from there till I git back," shouted Shorty, as he drove away. Si Klegg, Book 2 (of 6) |John McElroy 

He could not budge it, nor could the mighty Ling, nor could all of them together. The Devil's Asteroid |Manly Wade Wellman