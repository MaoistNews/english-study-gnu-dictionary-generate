The rangy actor gave a terrific comedic turn as a minion to the top girl at a boarding school in Ghana in 1986 where cliques, beauty pageants and skin tone were the chatter of the day. Non-binary actor wins Helen Hayes Award |Patrick Folliard |September 24, 2020 |Washington Blade 

Engaging in shared, meaningful media use can lead to terrific bonding and quality time between parents and kids of all ages. These methods will make screen time more enriching for your kids |By By Colleen Russo Johnson, PhD/Working Mother |September 24, 2020 |Popular-Science 

While we’re not suggesting you binge-watch terrific TV at double speed or fast-forward through your favorite records, there’s a strong case to be made for clawing back time when you don’t have to be watching, listening, or reading too closely. Read, watch, and listen to things faster than ever before |David Nield |September 9, 2020 |Popular-Science 

We know that some people who’ve been exposed to common cold coronaviruses develop a terrific T cell response to those coronaviruses. We Don’t Have to Despair - Issue 88: Love & Sex |Robert Bazell |August 12, 2020 |Nautilus 

I mean, there’s a terrific study from Toronto showing that the walkability of whatever ZIP code equivalent you’re in in Toronto, the walkability of that neighborhood predicted who gained how much weight over 10 or 20 years. The Zero-Minute Workout (Ep. 383 Rebroadcast) |Stephen J. Dubner |January 2, 2020 |Freakonomics 

On Monday Mount calls to say he thinks the script is terrific. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Making a film that hit so close to home was nothing but a truly terrific experience. Blogger Shares and Shames Cancer in ‘Lily’ |Amy Grantham |December 9, 2014 |DAILY BEAST 

Marilyn Johnson is a writer with a terrific capacity for learning. The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 

Iyer employs a terrific combination of erudition and absurdity that calls to mind the great postmodernists. Lars Iyer’s ‘Wittgenstein Jr.’ Plumbs the Deep Fun of Philosophical Fiction |Drew Smith |October 1, 2014 |DAILY BEAST 

“Our 18 working groups are a terrific resource to elected officials, whether they are running for president or not,” said Hook. Exclusive: Romney Foreign Policy Team Is Schooling 2016's Republicans |Josh Rogin |September 8, 2014 |DAILY BEAST 

A terrific yell of rage burst from every one, and each hastily threw something or other at the bold intruder. Hunting the Lions |R.M. Ballantyne 

Giving a terrific whoop, he raised his gun and fired, the ball just missing Lawrence's head. The Courier of the Ozarks |Byron A. Dunn 

It was the early dawn of a bright summer day, succeeding a night of terrific storm and darkness. The World Before Them |Susanna Moodie 

A terrific roar followed, the canvas was instantly torn open, and the whole tent fell in dire confusion on the top of its inmates. Hunting the Lions |R.M. Ballantyne 

And it cannot do this unless it continues to use the terrific engine of taxation already fashioned in the war. The Unsolved Riddle of Social Justice |Stephen Leacock