In keeping with the facade, Williams showed himself to be dedicated preacher who “knows his scripture.” Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 

Like all good scripture, Me & Dog can be read literally as well as metaphorically. Are You There, Nobody? It’s Me, Margaret |Candida Moss |October 12, 2014 |DAILY BEAST 

Checking Scripture demonstrates quickly that this is just not so. In Texas Textbooks, Moses Is a Founding Father |Edward Countryman |September 22, 2014 |DAILY BEAST 

It is not easy work, but it calls the bluff of those who would say “we have to take scripture seriously.” Meet the Young, Evangelical, Pro-Gay Movement |Gene Robinson |September 21, 2014 |DAILY BEAST 

For evangelicals, the way forward is not around scripture, but directly through it. Meet the Young, Evangelical, Pro-Gay Movement |Gene Robinson |September 21, 2014 |DAILY BEAST 

To fix on any one stage in such an evolution, detach it, affirm it, is to wrest a true scripture to its destruction. Solomon and Solomonic Literature |Moncure Daniel Conway 

The observance, under various phases, is described in Scripture as an undisputed and indisputable reality. The Ordinance of Covenanting |John Cunningham 

Circumstances might occur, in which there would be no warrant from Scripture or providence for making a given vow. The Ordinance of Covenanting |John Cunningham 

The fact that vowing and swearing to God are a part of his service is manifest, as we have seen from sundry passages of Scripture. The Ordinance of Covenanting |John Cunningham 

And again another scripture saith, "They shall look on him whom they pierced." His Last Week |William E. Barton