Their colonies here are predicated on the notion that their forefathers discovered an unpeopled dry wilderness, which they irrigated into their own slice of Eden. Should I Move to the Southwest, Even Though There’s a Drought? |mskenazy |September 1, 2021 |Outside Online 

He is truly one of the forefathers of making reggae live in London. ‘Lovers Rock’—The Story Behind The Music In Steve McQueen’s Tribute To Reggae |cmurray |November 28, 2020 |Essence.com 

Ancestor, an′ses-tur, n. one from whom a person has descended: a forefather:—fem. Chambers's Twentieth Century Dictionary (part 1 of 4: A-D) |Various 

The oldest and best of all Sanskrit dramas, Hanuman-Natak, is ascribed to this talented forefather of ours. From the Caves and Jungles of Hindostan |Helena Pretrovna Blavatsky 

It is your own forefather, MacIan with the broken sword, bleeding without hope at Culloden. The Ball and The Cross |G.K. Chesterton 

I will not permit you, Captain Roland, to rob me of either forefather, either train of idea. The Caxtons, Complete |Edward Bulwer-Lytton 

Forefather's day was observed by the Nashville churches in the theological hall of Fisk University. The American Missionary, Volume 49, No. 3, March, 1895 |Various