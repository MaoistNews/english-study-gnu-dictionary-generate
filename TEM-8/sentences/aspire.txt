Until now, he didn’t have the awareness or the nerve to aspire to such potency. The NFL’s top QBs are waking up to their power, following Tom Brady and LeBron James |Jerry Brewer |February 11, 2021 |Washington Post 

A judge later ruled that Grindr could not be held responsible, citing Section 230 — a mistake, in Warner’s view, that his bill aspires to fix. Sen. Warner to unveil bill reining in Section 230, seeking to help users fight back against real-world harm |Tony Romm |February 5, 2021 |Washington Post 

The fact that services like these — the new generation of robocalls, as it were — can sound “lifelike”, like actual humans, has been something that consumer versions have aspired to, although that hasn’t always worked out for the best. Infinitus emerges from stealth with $21.4M for ‘voice RPA’ aimed at healthcare companies |Ingrid Lunden |February 4, 2021 |TechCrunch 

By the end of 2025, GM aspires to have 40 percent of its US fleet composed of battery-electric cars, and have zero pollutants coming out of new light duty cars’ tailpipes by 2035. GM wants its cars to be fully electric by 2035. Here’s what that could mean for auto emissions. |Ula Chrobak |February 4, 2021 |Popular-Science 

Because of that burden, they usually don’t aspire to that level of detail unless their research question absolutely requires it. The Hard Lessons of Modeling the Coronavirus Pandemic |Jordana Cepelewicz |January 28, 2021 |Quanta Magazine 

As Americans, we should absolutely aspire to more than that. The Resurrection of Kristen Stewart |Marlow Stern |October 11, 2014 |DAILY BEAST 

Cheerleaders fall in love with freaks, jocks aspire to be indie musicians, and relationships are in a constant state of flux. In Praise of ‘Awkward’: OMFG MTV, Like, Really Gets High School |Amy Zimmerman |June 20, 2014 |DAILY BEAST 

Most bands these days aspire to reproduce their recordings on stage as faithfully as possible. Is Jack White the Last True Rock Star? |Andrew Romano |June 13, 2014 |DAILY BEAST 

My deepest desire is that my work will help people aspire to the life those kinds of images evoke. The Drunken Downfall of Evangelical America's Favorite Painter |Zac Bissonnette |June 8, 2014 |DAILY BEAST 

Our research indicates they can end up having a similar effect to an 'in-group' in high school that others aspire to join. Should The Devil Sell Prada? Study Finds Snobby Salespeople Boost Sales |The Fashion Beast Team |April 30, 2014 |DAILY BEAST 

I must aspire to the agitating transports of self-devotion, in scenes of sacrifice and peril! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

To imitate finite excellence, is to aspire at excellence, even though but in part. The Ordinance of Covenanting |John Cunningham 

And why should good men claim for it the character of an ordinance of God, to which even of itself it does not aspire? The Ordinance of Covenanting |John Cunningham 

He made the profession of a teacher the most honorable calling to which a citizen could aspire. Beacon Lights of History, Volume I |John Lord 

At a time when money might aspire to everything, the millionaire's dreams had nothing very exorbitant. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various