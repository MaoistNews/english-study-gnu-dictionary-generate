I’ve always remembered my dad’s request that I stand for the anthem. Mark Cuban had the right idea: It’s time to rethink how we use the national anthem |John Feinstein |February 12, 2021 |Washington Post 

Strickland’s attorneys had not responded to requests for comment from the Associated Press, which first reported that the lawsuit had been dropped. Raptors president, sheriff’s deputy drop lawsuits over shoving incident at NBA Finals |Cindy Boren |February 12, 2021 |Washington Post 

Keller’s attorney did not respond to a request for comment Wednesday evening. Grand jury beefs up charges against Olympian Klete Keller related to Capitol riot |Rick Maese |February 11, 2021 |Washington Post 

FEMA officials did not respond to a request for comment Wednesday. Leaders in Washington region ask FEMA for help in vaccinating federal workers |Julie Zauzmer, Rachel Chason, Rebecca Tan |February 11, 2021 |Washington Post 

Last July, the Housing Commission issued another request for proposals. Vacancy Tax Study Is Giving City Officials Déjà Vu |Lisa Halverstadt and Andrew Keatts |February 10, 2021 |Voice of San Diego 

He no doubt had heard by then that some of the cops had ignored his request and turned their backs. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

Whatever happened overtook them both within a minute or so of that altitude change request, and they were never heard from again. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

Nickelodeon did not return a request for comment for this story. Yep, Korra and Asami Went in the Spirit Portal and Probably Kissed |Melissa Leon |December 25, 2014 |DAILY BEAST 

Normal procedure is that any member country can request that a document be circulated, and the UN does it pro-forma. Exclusive: Sony Emails Say Studio Exec Picked Kim Jong-Un as the Villain of ‘The Interview’ |William Boot |December 19, 2014 |DAILY BEAST 

Teague did not respond to an InsideClimate News request for comment. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

Before the outlaw can comply with this small request the horn sounds again. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

The memory of him shall not depart away, and his name shall be in request from generation to generation. The Bible, Douay-Rheims Version |Various 

Thinking it was a request for employment which he could not offer, Malcolm stuffed it carelessly into a pocket. The Red Year |Louis Tracy 

The merest acquaintance would have said something more emphatic than "I will, thank you; good-by," to such a request. The Awakening and Selected Short Stories |Kate Chopin 

I was the dark complexioned man of the party, and as a “first-footer” in great request. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow