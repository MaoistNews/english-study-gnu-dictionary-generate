On the one hand, people weary of struggling to set up not just one but two appointments to get the currently available double-dose vaccines may welcome one-stop shopping. What you need to know about J&J’s newly authorized one-shot COVID-19 vaccine |Tina Hesman Saey |February 27, 2021 |Science News 

Experts who believe that summer could be relatively normal remain cautious about the near-term as highly-transmissible variants are circulating that could cause a spring spike in cases and as pandemic-weary Americans tire of restrictions. How this summer could bring the pandemic relief we’re longing for |William Wan |February 26, 2021 |Washington Post 

Many consumers have felt weary by what can seem like an onslaught of services laying claim to the extra money lying around their desks every month. With Paramount Plus, ViacomCBS marshals new weapons in the streaming wars |Steven Zeitchik |February 25, 2021 |Washington Post 

These simple tools can get your weary muscles back into shape quickly, so you can get back to the gym. 4 mobility tools to help soothe and stretch your sore muscles |Stan Horaczek |February 5, 2021 |Popular-Science 

To curb the spread of the virus, individuals must continue to practice social distancing and masking, even if many have long since grown weary of altering their lives to protect themselves and their communities. 10 Million People Have Tested Positive for Coronavirus in the United States |Tara Law |November 9, 2020 |Time 

But the man appears so weary that I decide to skip the dull stuff and get to the heat. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Dehydrated and feeling weary, Marino lay down beside another migrant under a tree and fell asleep. Drug Smuggler Sues U.S. Over Dog Bite |Caitlin Dickson |December 10, 2014 |DAILY BEAST 

Gays observe hetero-excitement over the lumbersexual with a weary eyeroll. How Straight World Stole ‘Gay’: The Last Gasp of the ‘Lumbersexual’ |Tim Teeman |November 12, 2014 |DAILY BEAST 

Or, worse, they are contrived to sound tired, perhaps in an attempt to come off as world-weary. U2 Generously Gives Us a Lousy Album, Sucks at the Corporate Teat |Hampton Stevens |September 13, 2014 |DAILY BEAST 

But peering more closely at the photograph, taken this August, his weary brown eyes reveal a darker truth. Fighting Ebola With Nothing but Hope |Abby Haglage |August 27, 2014 |DAILY BEAST 

As small letters weary the eye most, so also the smallest affairs disturb us most. Pearls of Thought |Maturin M. Ballou 

Malcolm had selected it as a training-ground that evening, because he meant to weary and subdue his too highly spirited charger. The Red Year |Louis Tracy 

When you exalt him put forth all your strength, and be not weary: for you can never go far enough. The Bible, Douay-Rheims Version |Various 

A flock of weary sheep pattered along the road, barnward bound, heavy eyed and bleating softly. The Soldier of the Valley |Nelson Lloyd 

The crest-fallen astronomer plodded on his weary way, another example of a fool and his money soon parted. The Book of Anecdotes and Budget of Fun; |Various