I raised my arms in victory after rolling awkwardly off the bars, then swallowed that pride and climbed down twice as slowly, ashamed of the instincts that led me there. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

I don’t care how long has passed-Diane Sawyer should be ashamed of herselfFormer NBC anchor Matt Lauer was roundly chastised, too, for the footage that appears in the documentary. Britney Spears’s conservatorship is back in court — and back in the public eye |Ashley Fetters |February 11, 2021 |Washington Post 

He was a pornographer and should be measured as such, and you, sir, should be ashamed of yourself. Outing a Celeb Sex Pest and Mourning Larry Flynt … Sorta |Eugene Robinson |February 11, 2021 |Ozy 

Ask for a reversal of at least some of the fees, and also do not feel ashamed to say, “Please help me.” Hints From Heloise: Don’t get hit with pricey overdraft fees |Heloise Heloise |January 26, 2021 |Washington Post 

High-sugar, high-fat, and high-calorie foods are deemed “sinfully delicious,” an indulgence to feel a little ashamed of. There Are No Rules for Healthy Eating |Christine Byrne |January 22, 2021 |Outside Online 

Sabrine says that despite the private horror of what she was going through, she was too ashamed to tell her family. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

And though I knew I should say something, I still felt ashamed. Bill Cosby’s Long List of Accusers (So Far): 18 Alleged Sexual Assault Victims Between 1965-2004 |Marlow Stern |November 24, 2014 |DAILY BEAST 

For the first time since I put my acceptance letter in the mail, I woke up this morning ashamed of my alma mater. How UVA Is Failing Its Women |Allison McNearney |November 20, 2014 |DAILY BEAST 

Distraught, confused and ashamed, both men broke down in the courtroom, weeping like children and begging for forgiveness. Did Picasso Try to Steal the Mona Lisa? |Nick Mafi |October 23, 2014 |DAILY BEAST 

Indeed, many of the posters are, it seems, feeling ashamed about their actions. ‘The Fappening’ Perpetuators Have a J.Law Come-to-Jesus Moment and ‘Cower With Shame’ |Marlow Stern |October 8, 2014 |DAILY BEAST 

The dog stood with hanging head and tail, as if ashamed he had let so many of his enemies get away unharmed. The Courier of the Ozarks |Byron A. Dunn 

Be ashamed of upbraiding speeches before friends: and after thou hast given, upbraid not. The Bible, Douay-Rheims Version |Various 

And then Jolly Robin would feel ashamed that he had even thought of being so cruel to an infant bird, even if he was a Cowbird. The Tale of Grandfather Mole |Arthur Scott Bailey 

But she was very much ashamed of you—and so was I; and at last we all sent Captain Lovelock after you to bring you back. Confidence |Henry James 

A certain great authority once said that if he had made it he would have been ashamed of it. The Unsolved Riddle of Social Justice |Stephen Leacock