There’s no doubt this is enthralling, double-tap-worthy content, but unless you’re an athlete or a fitness professional yourself, you probably won’t be able to equal many of these feats at home. Use TikTok to build the perfect workout |Sandra Gutierrez G. |September 17, 2020 |Popular-Science 

The site is also projecting a 1-in-3 chance that the President repeats his 2016 feat of winning the election while losing the national popular vote. Trump and Biden’s election odds are virtually even for the first time in 3 months |reymashayekhi |September 2, 2020 |Fortune 

Exactly how the immune system manages that feat isn’t known. In a first, a person’s immune system fought HIV — and won |Tina Hesman Saey |August 26, 2020 |Science News 

Bats, foxes, raccoons, boars and other wildlife that harbor potential zoonotic infections tend to hide in remote places, so vaccinating enough of them to create herd immunity is not an easy feat. Can Vaccines for Wildlife Prevent Human Pandemics? |Rodrigo Pérez Ortega |August 24, 2020 |Quanta Magazine 

Achieving that ambitious feat could only happen if MediaTek’s chips are “widely adopted” by Huawei, Brady Wang, a semiconductor analyst at Counterpoint Research in Taipei, wrote in a note about the call. This chipmaker was a winner in the U.S. crackdown on Huawei. Now, it’s another victim |Grady McGregor |August 20, 2020 |Fortune 

Murders in the City of Angels have fallen by about half in the last 10 years: no small feat for such a big city. America’s 2014 Murder Capital |Brandy Zadrozny |January 3, 2015 |DAILY BEAST 

The real hackers—whoever they may prove to be—had pulled off a feat: they ruined a Hollywood fete. Sony Hack: A Dictator Move? |Kevin Bleyer |December 14, 2014 |DAILY BEAST 

The sets—which, really, were a feat of design and direction—appeared to be remnants of a Lewis Carroll fever dream. ‘Peter Pan Live!’ Review: No Amount of Clapping Brings It to Life |Kevin Fallon |December 5, 2014 |DAILY BEAST 

As great as this feat was, an equally demanding test followed: to conceal from the Nazis that Enigma had been beaten. The Castration of Alan Turing, Britain’s Code-Breaking WWII Hero |Clive Irving |November 29, 2014 |DAILY BEAST 

Luckily the duo got what they needed before they had to pack it up, which is not an easy feat when working with a drone. Anatomy of a Drone Porn: ‘Drone Boning’ Makes Sex Look Like Art |Aurora Snow |November 8, 2014 |DAILY BEAST 

Having achieved this feat he sighed again, and applied himself assiduously to the pie. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

He made a deliberate effort to put himself in Zeal's place, and after several failures accomplished the feat. Ancestors |Gertrude Atherton 

By means of this proof he confutes the cowardly rival who claims to have achieved the feat. The Three Days' Tournament |Jessie L. Weston 

During this same year, a most unusual naval feat occurred in the Revolutionary War. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Over this same highway it would now be an easy feat for a powerful car to cover the distance in three or four hours. British Highways And Byways From A Motor Car |Thomas D. Murphy