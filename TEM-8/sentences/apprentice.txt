Holly Walker, an apprentice ski guide with the Association of Canadian Mountain Guides and a former ski patroller at Whistler Blackcomb Ski Resort, opts to wear her device in-bounds, too. Know How to Use Your Avalanche Transceiver |Amelia Arvesen |March 2, 2021 |Outside Online 

Master artists taught apprentices “how to draw, make the composition and pose the colors.” In a gray, empty Paris, this corner shop’s colorful posters transport you wherever you want to go |Lily Radziemski |February 19, 2021 |Washington Post 

For instance, wage subsidies would enable employers to take on the risk of hiring apprentices, a practice that used to be common but is now rare in the United States. How to fix what the innovation economy broke about America |Katie McLean |February 17, 2021 |MIT Technology Review 

Easily the story’s most fascinating character, Lydia has plenty to say about fate but thrives by imposing her will on others—including Anna, whom she manipulates into becoming her apprentice. In Starz's Enchanting The Luminaries, Fate and Free Will Battle Amid New Zealand's Gold Rush |Judy Berman |February 12, 2021 |Time 

Without being overbearing or assertive, Gerace had gently taken us under his wing for our full education, both within science and without, as the master painters did with their apprentices in the 17th and 18th centuries. Five Scientists on the Heroes Who Changed Their Lives - Issue 93: Forerunners |Alan Lightman, Hope Jahren, Robert Sapolsky, |December 2, 2020 |Nautilus 

Whatever the excuse, in 2008 we were all subjected to Celebrity Apprentice. Donald Trump Fires Woman For Not Calling Bill Cosby |Jack Holmes, The Daily Beast Video |January 5, 2015 |DAILY BEAST 

Cocker, for his part, worked briefly as an apprentice gasfitter but decided to take the plunge into the world of commercial music. The Greatest Rock Voice of All Time Belonged to Joe Cocker |Ted Gioia |December 23, 2014 |DAILY BEAST 

The former apprentice engineer retained a lifelong interest in the way things worked. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Now, the onetime Lloyd Kaufman/Troma apprentice is the toast of Tinseltown. ‘Guardians of the Galaxy’ Filmmaker James Gunn on His Glorious Space Opera and Rise to the A-List |Marlow Stern |August 3, 2014 |DAILY BEAST 

At one point, the host of Celebrity Apprentice that Tea Partiers need to stop supporting doomed candidates. Donald Trump Is Still A Birther |David Freedlander |May 30, 2014 |DAILY BEAST 

The young apprentice was of middle height, very well built, amazingly active, and able to bear the utmost fatigue. Napoleon's Marshals |R. P. Dunn-Pattison 

If she lasts a couple of hours, I shall be surprised, said the apothecarys apprentice, intent upon the toothpicks point. Oliver Twist, Vol. II (of 3) |Charles Dickens 

He was an apprentice to a wig and curl maker, when Whitefield attracted his attention, and he became a methodist preacher. The Every Day Book of History and Chronology |Joel Munsell 

Shortly after this the youngest apprentice went below, and found the ill-used lad standing on a locker, and gibbering fearfully. The Chequers |James Runciman 

An apprentice who is qualifying himself to operate an elevator is an employee within the Minnesota Act. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles