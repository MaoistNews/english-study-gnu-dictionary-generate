This capability was restored, she discovered, when the yeasts acquired bacterial versions of those genes via horizontal gene transfer. By Losing Genes, Life Often Evolved More Complexity |Viviane Callier |September 1, 2020 |Quanta Magazine 

The miter saw is fully adjustable with easy to read markers and a 14-Inch expanded horizontal cutting capacity. Make every project a breeze with the right miter saw |PopSci Commerce Team |August 26, 2020 |Popular-Science 

As a matter of physics, rotation is required to produce horizontal or physical movement on pitches. What Really Gives Left-Handed Pitchers Their Edge? |Guy Molyneux |August 17, 2020 |FiveThirtyEight 

It also appears that horizontal gene exchange isn’t limited to prokaryotes. How Life Could Continue to Evolve - Issue 88: Love & Sex |Caleb Scharf |August 12, 2020 |Nautilus 

Interstellar horizontal gene transfer need not involve physical movement, but could enhance the fitness of any participating species. How Life Could Continue to Evolve - Issue 88: Love & Sex |Caleb Scharf |August 12, 2020 |Nautilus 

Like countless other boys in Massapequa, he had a crew cut and wore Keds sneakers and T-shirts with horizontal stripes. Why Comedians Still Think Bill Cosby Is a Genius |Mark Whitaker |October 5, 2014 |DAILY BEAST 

Daily activity is rendered in parallel horizontal timelines, making it very easy to compare one day's activity to the next. The Best Quantified Self Site You Haven’t Heard Of |Jamie Todd Rubin |August 5, 2014 |DAILY BEAST 

Name some aspect of the horizontal polka and it has probably been discussed endlessly among human beings with two X chromosomes. C’mon, Ladies, Masturbation Isn’t Just for Bad Girls |Emily Shire |June 19, 2014 |DAILY BEAST 

You see, the spine is a row of vertebrae that was designed to be horizontal. Why Is Louis C.K. So Funny? He Uses Humor as a Moral Compass. |Andrew Romano |May 2, 2014 |DAILY BEAST 

Like the wings, the tail surfaces—horizontal and vertical—easily break away from the fuselage and float. Mysterious Debris Near Australia Looks like MH370’s Wing |Clive Irving |March 20, 2014 |DAILY BEAST 

In the diagram the horizontal arrows represent such mere banking operations, not true circulation. Readings in Money and Banking |Chester Arthur Phillips 

It had worked at Tavistock; it was a horizontal high-pressure pole puffer. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Carlson was a little above medium height, dark complexioned, his brow a washboard of horizontal wrinkles. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The cliffs of Red Point partake of a reddish tinge and appear to be disposed nearly in horizontal strata. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

The tubes are horizontal over the fire, the water circulating through them. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick