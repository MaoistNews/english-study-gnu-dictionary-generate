But along with the cartoon funk is an all-too-real story of police brutality embodied by a horde of evil Pigs. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

The benefits of incumbency are quite potent, especially in the all-important area of raising campaign funds. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

The building used to be an all-girls school, and when it was initially purchased by Fortune it was dilapidated. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

This led to the formation of a Christian militant group to counter the rebels, and all-out sectarian violence exploded. The Year’s Most Forgotten Humanitarian Crisis |Nina Strochlic |January 1, 2015 |DAILY BEAST 

Starting in the 1970s, then MPAA president Jack Valenti began what was to become a decades-long fight against the quota system. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

Strathland would bundle me out in ten minutes if anything happened to Jack. Ancestors |Gertrude Atherton 

He had discovered that the all-glorious boast of Spain was not exempt from the infirmities of common men. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

She is immensely rich, one of the ablest political women in London, and Jack is desperately in love with her. Ancestors |Gertrude Atherton 

Naturally the conversation fell on the all-absorbing topic of the day and the object of his mission. The Philippine Islands |John Foreman 

How on earth can Jack find time to think about women with the immense amount of work he gets through? Ancestors |Gertrude Atherton