Unless there is a court decision that changes our law, we are OK. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

It is grandstanding for a right rarely protected unless under immediate attack. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

They had rarely seen their own fathers carry small children unless their mothers were ill. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

Which is impossible unless people talk publicly rather than letting each crime be its own isolated incident. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

In some ways I never got out of that unless realizing… well, that kind of seals it off. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

For instance, the Limestone Polypody is not happy unless there is a certain amount of lime present in the soil. How to Know the Ferns |S. Leonard Bastin 

This was why we resolved, at the time of our arrival, not to baptize any adults unless they were previously well catechized. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

However great the power of Revival, there is no memory unless there was a First Impression. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

If we set him adrift the poor child would starve—unless the cat got him. The Tale of Grandfather Mole |Arthur Scott Bailey 

The intensity of his sensations seemed inexplicable, unless some reality, some truth, lay behind them. The Wave |Algernon Blackwood