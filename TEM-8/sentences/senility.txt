Whether it was actual ignorance, senility, or some obscure test, it's hard to know. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

With senility's fingers at his throat, it was clear that no more movies were going to be made. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Adrift in senility and depression, Hitchcock is dismantling his life, putting it away. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

His father was near senility, his mother was overbearing, and the household was in financial straits. The Professor and the Doomsday Clock: ‘A Confederacy of Dunces’ & Signs of John Kennedy Toole’s Suicide |Cory MacLauchlin |December 17, 2012 |DAILY BEAST 

Like senility, there is no way to stop sebaceous hyperplasia from advancing, either. My Odyssey Into Extreme Dermatology |Elizabeth Hayt |April 15, 2009 |DAILY BEAST 

We, fools as we are, know how to pay the proper respect that is due to senility and second-childishness. Punch, or the London Charivari, Vol. 108, April 6, 1895 |Various 

It would seem that we lawyers have taken insufficient account of the characteristics of senility. Criminal Psychology |Hans Gross 

I should like to add to what precedes, that senility presents fact and judgment together. Criminal Psychology |Hans Gross 

The deep red hue of a frosty and vigorous senility still coloured their unwrinkled faces. The Mirror of Literature, Amusement, and Instruction |Various 

Those who have clung so hard to their bodies, must galvanise them again with rheumatism and senility and mortgage-ridden minds. The Hive |Will Levington Comfort