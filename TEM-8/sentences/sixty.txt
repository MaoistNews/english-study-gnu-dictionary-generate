Sixty-seven Republicans voted against it, a margin in line with estimates of many conservatives from earlier in the day. ‘Cromnibus’ Passes, But Did Anyone Win? |Ben Jacobs |December 12, 2014 |DAILY BEAST 

Requests received more than sixty (60) days after January 31st, 2015, will not be honored. The Daily Beast Company LLC The New Alphas Sweepstakes Official Rules | |December 9, 2014 |DAILY BEAST 

Sixty vaccinators have been killed in the area in the last few years. Powdered Measles Vaccine Could Be Huge for Developing World |Kent Sepkowitz |December 2, 2014 |DAILY BEAST 

But a recently purchased automated bottling line has increased their output to sixty cases per hour. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

“Sixty-five years altogether,” she pointed out with a laugh. Freedom From Fear for Dreamer Kids |Mike Barnicle |November 24, 2014 |DAILY BEAST 

But what might have been very practicable for eight hundred and sixty men, was impossible for three hundred and sixty. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

At that time, the postage on letters from that region was very high, sometimes as much as fifty or sixty cents, or even a dollar. The Boarded-Up House |Augusta Huiell Seaman 

Fujiyama, the noted volcano of Japan, is twelve thousand three hundred and sixty-five feet high. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Madame Coquereau, in spite of her sixty-five years trudged along with springing step. The Joyous Adventures of Aristide Pujol |William J. Locke 

We are going to send our butler to the sale to-morrow, to pick up some of that sixty-four. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens