It wasn’t just a question of how the restaurant was going to pay rent month-to-month, but also how they could afford to pay what would amount to more than $30,000 in missed rent at the end of the year. Is the Government Just Going to Watch the Restaurant Industry Die? |Elazar Sontag |August 28, 2020 |Eater 

Sherry told the Blade she and other tenants paid their rent by the week. New Orleans shelter to be ‘forever home’ for homeless trans people |Michael K. Lavers |August 27, 2020 |Washington Blade 

In addition to offering vans for rent, it’s open to members who already have their own vans. The New Camper Companies Redefining Road Travel |Alex Temblador |August 27, 2020 |Outside Online 

If a housing authority brings in fewer dollars from rent payments, it doesn’t get more money. She Was Sued Over Rent She Didn’t Owe. It Took Seven Court Dates to Prove She Was Right. |by Danielle Ohl, Capital Gazette, and Talia Buford and Beena Raghavendran, ProPublica |August 25, 2020 |ProPublica 

Struggling restaurants say it’s a lifeline, letting them rehire bartenders, pay rent and reestablish relationships with customers. Number of states allowing to-go cocktails has surged from 2 to 33 during coronavirus |Lee Clifford |August 24, 2020 |Fortune 

The first 30 years of his life, he helped his father build and then rent out Rockefeller Center at a difficult time. This Republican Loved Taxes & Modern Art |Scott Porch |November 19, 2014 |DAILY BEAST 

And actual vote-buying is a pretty low-rent form of corruption anyway. Undo Citizens United? We’d Only Scratch the Surface |Jedediah Purdy |November 12, 2014 |DAILY BEAST 

The winter air is rent with cries from thousands of puffed up lips, begging to be let in. Russia’s Gold Digger Academy |Peter Pomerantsev |November 11, 2014 |DAILY BEAST 

Squeezing what rent he could from the tenants, Washington moved on. Washington’s Wheeler-Dealer Patriotism |William O’Connor |October 31, 2014 |DAILY BEAST 

The journey began well, as Washington managed to collect some rent from war-ravaged tenants in Cumberland. Washington’s Wheeler-Dealer Patriotism |William O’Connor |October 31, 2014 |DAILY BEAST 

Rent, the share of the land-owner, offered to the classicist a rather peculiar case. The Unsolved Riddle of Social Justice |Stephen Leacock 

A fourth lives upon rent, dozing in his chair, and neither toils nor spins. The Unsolved Riddle of Social Justice |Stephen Leacock 

You may have similar qualms over rent and the rightness and wrongness of it. The Unsolved Riddle of Social Justice |Stephen Leacock 

He wishes to cultivate it still, and offers to renew the lease for any number of years, and pay the rent punctually. Glances at Europe |Horace Greeley 

The high rent of a Broadway store, says the economist, does not add a single cent to the price of the things sold in it. The Unsolved Riddle of Social Justice |Stephen Leacock