Now, with real inflation for the first time in more than a generation, the dormant fears have been roused, and fear has a way of gripping not just our sense of the present but foreboding about the future. Inflation May Already Have Peaked. Overreacting Brings Its Own Risks |Zachary Karabell |May 13, 2022 |Time 

The reality of the crisis isn’t as foreboding as Milman initially makes it seem. Why you should care about ‘The Insect Crisis’ |Allie Wilkinson |April 26, 2022 |Science News 

Amid a sea of blinking green and blue lights, an entire rack of computers suddenly scintillates yellow, and then, after a few seconds, a foreboding red. Inside the physical footprint of the Cloud |Steven Gonzales Monserrate/The MIT Press Reader |February 14, 2022 |Popular-Science 

Having each court changing schedules and advancing a larger volume of cases to trial has filled him with a feeling of foreboding. Amid Calls for Reform, Maine’s Criminal Defense System Reaches a “Breaking Point” |by Samantha Hogan, The Maine Monitor |June 8, 2021 |ProPublica 

They all seem to represent doors or portals of some kind—dark, monumental, and foreboding. How These Rothkos Were Restored Without Touching the Canvas |Adam Rogers |May 30, 2021 |The Daily Beast 

The color has also been used to forebode an omen or a threat. On St. Patrick’s Day, Beware Those Sly Green Eyes |Laura Dimon |March 17, 2013 |DAILY BEAST 

The storm raised over King Christian's letter was such as to forebode no other settlement than by arms. A History of the Nineteenth Century, Year by Year |Edwin Emerson 

From her history the shadow of the Horde, one is tempted to forebode, in the words of Poe, shall be lifted nevermore. The Rise of the Russian Empire |Hector H. Munro 

The menaces of my persecutor seemed to forebode the inevitable interruption of this system. Caleb Williams |William Godwin 

This profound tranquility excited the suspicions of the Black Bear; it seemed to forebode an impending storm. The Tiger-Slayer |Gustave Aimard 

Perhaps—but her tone did not forebode a cheerful conversation. Mrs. Maxon Protests |Anthony Hope