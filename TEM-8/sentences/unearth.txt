The earliest studies of this sort all involved very small populations, but there are now a couple that have unearthed reasons for optimism, suggesting that immunity will last at least a year, and perhaps longer. The persistence of memory in B cells: Hints of stability in COVID immunity |John Timmer |January 20, 2021 |Ars Technica 

As the two dug through sand and bones, Eriksen recalls, “We unearthed this mass of plastic.” Camels have been dying after mistaking plastic for food |Asher Jones |January 15, 2021 |Science News For Students 

We tend to unearth deeply unsettling and upsetting truths, which was especially hard in a deeply unsettling and upsetting year. ProPublica’s Top 25 Stories of 2020 |by Karim Doumar |January 11, 2021 |ProPublica 

Owners often rush out of competitive pressure, focusing on publicly popular candidates rather than meticulously determining what they want and unearthing coaches who fit. The NFL has tried to address its minority hiring problem. Will this year be different? |Adam Kilgore |December 17, 2020 |Washington Post 

All kinds of big scores have been unearthed in the woods around Amandola over the years. ‘The Mozart of fungi’: For ages, truffle hunting has been one of the most challenging pursuits on earth. Then the pandemic hit |Bernhard Warner |December 12, 2020 |Fortune 

They contain dozens of blurred out media photos that Bartiromo wants to unearth. He Bullies Kids and Calls It News |Brandy Zadrozny |June 26, 2014 |DAILY BEAST 

She will unearth more than their remains in a quest that becomes a journey of baleful discovery and painful self-discovery. Holocaust Horrors Haunt the Films ‘Ida’ And ‘The German Doctor’ |Jack Schwartz |May 12, 2014 |DAILY BEAST 

Harris is not the first to unearth this history, although no one else has done it so thoroughly. WWII Lies of Hollywood's Greats |Caryn James |February 22, 2014 |DAILY BEAST 

Locals quickly joined the effort to help unearth the lumps and unveil first corners, and then entire slabs, of tombstones. Uncovering Jamaica’s Jewish Past |Debra A. Klein |December 1, 2013 |DAILY BEAST 

To unearth what you need, put a minus sign in front of any terms you want excluded from your search. 13 Hacks to Improve Your Google Search |Nina Strochlic |September 15, 2013 |DAILY BEAST 

He also managed to unearth his father's old writing-desk, and had it set up in its old place in the "office." Dry Fish and Wet |Anthon Bernhard Elias Nilsen 

To dig up de profundis a shoehorn that you need is a more remarkable achievement than to unearth a new Pompeii. Bizarre |Lawton Mackall 

Its nice to unearth a reserve fund of silk stockings under a sofa pillow. I, Mary MacLane |Mary MacLane 

If Thorpe were really innocent, he would welcome the clever sleuthing that would be likely to unearth the truth. The Come Back |Carolyn Wells 

Jenkins, go around back and see if you can unearth the butler. The Mystery of the Hidden Room |Marion Harvey