They freeze well, so make a big batch and defrost one at a time, so it’s ready for your kid when they get home. Back-to-school recipes, including breakfasts, lunches and snacks, for students of all ages |Kari Sonde |August 25, 2021 |Washington Post 

The air fryer wicks moisture from the food as it defrosts and cooks, so you end up with a crispy chimichanga-like burrito, not a soggy wrap. The Complete Guide to Air Fryers |Christine Byrne |April 7, 2021 |Outside Online 

This has resulted in a number of cases where vaccines were wasted or pharmacists and others offered the vaccine to anyone who would take it, in order not to waste their defrosted supply after the day’s vaccinations were done. To get vaccines to more people, the priority system needs to loosen up |Kat Eschner |January 21, 2021 |Popular-Science 

The vaccine clinic has also managed to further reduce potential waste by getting appointment confirmations and defrosting vaccine vials close to appointment times, she said. How Many Vaccine Shots Go to Waste? Several States Aren’t Counting. |by Ryan Gabrielson, Caroline Chen and Mollie Simon |January 21, 2021 |ProPublica 

That means pharmacists need to be on top of how many vaccines they will be giving, and ensure that they defrost the right number at the right time. How the First COVID-19 Vaccinations Rolled Out at Hospitals Across the U.S. |W.J. Hennigan |December 15, 2020 |Time 

Defrost overnight in the refrigerator (if frozen) and bake before serving. Make These Barefoot Contessa Chicken Pot Pies |Ina Garten |November 29, 2014 |DAILY BEAST 

“It takes a day to defrost and then it takes a day to take out,” Lupoi says on the recording. Mafia’s Cocaine-in-a-Can Bust |Michael Daly |February 12, 2014 |DAILY BEAST 

It would seem Bennett has decided to defrost a security perspective that went into deep-freeze in the 1970s. Naftali Bennett's Security Strategy is Frozen in the 1970s |Gen.(res.) Nati Sharoni |November 18, 2013 |DAILY BEAST 

He opened the refrigerator and examined the control, then turned it to "defrost." The Flying Stingaree |Harold Leland Goodwin