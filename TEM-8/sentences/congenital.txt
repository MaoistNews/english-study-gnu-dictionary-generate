Techshot also envisions someday using artificial tissue and organs to help treat diseases, and even congenital defects. NASA inches closer to printing artificial organs in space |Tatyana Woodall |June 18, 2021 |MIT Technology Review 

Despite having been diagnosed with a congenital heart condition,“she worked day and night—walked in the heat to different centers as and when assigned,” Dinesh says. 'Our Lives Don't Matter.' India's Female Community Health Workers Say the Government Is Failing to Protect Them From COVID-19 |Nilanjana Bhowmick / New Delhi |May 5, 2021 |Time 

Cunningham-Rundles treats many patients with congenital immune system deficiencies. What immunosuppressed patients should know about the coronavirus vaccines |Lindsey Bever |April 23, 2021 |Washington Post 

Early on, my vet back home delivered the crushing news that Scout had a congenital kidney disease. I have the pandemic to thank for this precious time with my old hound, Teddy |Diana Nyad |December 21, 2020 |Washington Post 

Mabry’s ex-wife worked in a hospital, so two of their kids, including one with a congenital condition, moved in with him. As COVID-19 Ravaged This Iowa City, Officials Discovered Meatpacking Executives Were the Ones in Charge |by Michael Grabell and Bernice Yeung |December 21, 2020 |ProPublica