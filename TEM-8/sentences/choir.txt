Singing and talking, particularly when done loudly, are risky activities, as researchers learned from the Washington choir practice that became a Covid-19 superspreader event. As theater innovates worldwide, Broadway’s lights will stay off |Alexandra Ossola |October 9, 2020 |Quartz 

Experts have pointed to the spread of the virus in choirs, buses, fitness classes and other poorly ventilated spaces. CDC says airborne transmission plays a role in coronavirus spread in a long-awaited update after a website error last month |Lena H. Sun, Ben Guarino |October 5, 2020 |Washington Post 

If you look at superspreading events, for example the Washington choir case, it is impossible they are being spread by droplets. This scientist made a Google Doc to educate the public about airborne coronavirus transmission |Niall Firth |October 2, 2020 |MIT Technology Review 

In one notorious case, a single person at a choir practice in Washington state infected 52 others, leading to two deaths. Why Vienna opera singers are ready to risk their lives to perform in a pandemic |Julia Belluz |September 30, 2020 |Vox 

A database of Covid-19 superspreading events around the world lists numerous choir practices and a few concerts as sources of contagion. Why Vienna opera singers are ready to risk their lives to perform in a pandemic |Julia Belluz |September 30, 2020 |Vox 

I know the verse because Mrs. Bertalan used to have us do it in ninth-grade choir. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

Here it is being performed by the Westminster Cathedral Choir. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

But I was a choir geek, and then got frustrated and took an acting class and realized that was the thing for me. Michael C. Hall on Going Drag for ‘Hedwig and the Angry Inch’ and Exorcising ‘Dexter’ |Marlow Stern |December 4, 2014 |DAILY BEAST 

It was in the vestry where the choir was putting on its garments. Joseph Campbell on the Roots of Halloween |Joseph Campbell |October 31, 2014 |DAILY BEAST 

Then, as I sat here on this “throne,” this beautiful choir struck my ears and senses. Joseph Campbell on the Roots of Halloween |Joseph Campbell |October 31, 2014 |DAILY BEAST 

The steady use of the organ for an hour-and-a-half's choir rehearsal would exhaust the batteries. The Recent Revolution in Organ Building |George Laing Miller 

When fifteen he became voluntary organist and choir-master to the Birkenhead School Chapel. The Recent Revolution in Organ Building |George Laing Miller 

B'lieves in candles and vestures; got Tim into the choir one Sunday, and now you can't keep him out of it. The Cromptons |Mary J. Holmes 

The body is an octagon of thirty-two feet diameter; and the choir, of the same shape, is twenty-one feet in diameter. Journal of a Voyage to Brazil |Maria Graham 

This is also a good point from which to study the clerestory as seen in choir and crossing. Bell's Cathedrals: A Short Account of Romsey Abbey |Thomas Perkins