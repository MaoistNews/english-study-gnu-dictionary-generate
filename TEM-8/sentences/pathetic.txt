There are some mattresses on the ground and a pathetic amount of necessities. The ballad of the Chowchilla bus kidnapping |Kaleb Horton |July 23, 2021 |Vox 

Part of the fun of declaring yourself being based is getting to label the other side as weak, wrong and pathetic — and, well, cringe. The Trailer: Based or cringe? A new way of explaining the same old political brawls |David Weigel |July 13, 2021 |Washington Post 

This concept was just as sexist, but it at least recognized that women were powerful rather than pathetic. Myanmar's Women Are Fighting for a New Future After a Long History of Military Oppression |MiMi Aye |June 1, 2021 |Time 

The story Dobbs tells is, by turns, hilarious, pathetic and infuriating. Nixon knew better, but he couldn’t save himself |Joe Klein |May 28, 2021 |Washington Post 

In the show, the joke is meant to be at Michael’s expense — look at this pathetic man, newly single and so desperate for love that he would settle for a woman he can’t even identify. Asian women say Hollywood has failed them for decades. They’re ready for meaningful change. |Sonia Rao |March 26, 2021 |Washington Post 

And we can listen to the pathetic, creepy bravado of a former vice president, wrong on nearly every decision he made. Dick Cheney vs. ‘Unbroken’ |Mike Barnicle |December 15, 2014 |DAILY BEAST 

But this had to be one of the most pathetic presidential wardrobes in American history. From Auschwitz to the White House: One Tailor’s American Tale |Martin Greenfield |December 5, 2014 |DAILY BEAST 

Telling people that you knew her when would just be pathetic. Is Bigger Better for St. Vincent? |David Yaffe |December 4, 2014 |DAILY BEAST 

Fortunately, they are drawn from a pathetic preterite far beneath the contempt of our cultural elite. The FBI’s Bogus ISIS Bust |James Poulos |November 21, 2014 |DAILY BEAST 

Conning people into buying a book to prepare for an "Ebola apocalypse" is not just irresponsible, it's pathetic. The Sham, Scaremongering Guide to Ebola |Abby Haglage |November 20, 2014 |DAILY BEAST 

Tressan fell suddenly to groaning and wringing his hands a pathetic figure had it been less absurd. St. Martin's Summer |Rafael Sabatini 

The observer might well remain perplexed at the pathetic discord between human work and human wants. The Unsolved Riddle of Social Justice |Stephen Leacock 

It is curious to note children's first manifestations of a sense of the pathetic and the comic as represented in art. Children's Ways |James Sully 

He was fond of the pathetic, but the humorous moved him most, and his lively gifts were welcome wherever we went. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

In spite of his brilliant career, Bernadotte must ever remain one of the most pathetic figures in history. Napoleon's Marshals |R. P. Dunn-Pattison