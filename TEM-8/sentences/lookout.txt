This an exclusive community of curious people and lovers of OZY’s The Carlos Watson Show who are always on the lookout for real and meaningful conversations that inspire, teach and surprise you. The Carlos Watson Show Ambassadors |Sandya Kola |September 28, 2020 |Ozy 

And, during a tough year, finding ways to turn those losses into wins is key to those advertisers on the lookout for new ways to work their dollars harder. ‘Necessary, but insufficient:’ Advertisers are starting to question the value of low exchange fees |Seb Joseph |September 23, 2020 |Digiday 

Some clubs like Liverpool, Manchester City and Tottenham Hotspur experimented with archive short-form content at a time when those advertisers that were still spending were on the lookout for pre-roll video ad inventory. ‘An unprecedented period of Darwinian experimentation’: As sports return, Twitter eyes ad boost |Seb Joseph |September 18, 2020 |Digiday 

We will be on the lookout for any problems that prevent people from voting — such as mail ballot delivery problems, changed voting locations, long lines, registration problems, purged voter rolls, broken machines and voter intimidation. Please Tell Us If You Have Any Trouble Voting This Year |by Rachel Glickhouse |September 8, 2020 |ProPublica 

Not only does this mean Lookout didn’t sacrifice much U-pick business during the initial wave of the pandemic, but it also gave Mofenson and his team a chance to reconfigure the entire operation ahead of the anticipated fall crowds. Pick Your Poison |Nick Mancall-Bitel |September 3, 2020 |Eater 

The lookout guy seems a bit younger and is also described as a black man, wearing a red baseball cap. How to Get Away With Stealing $2 Million in Jewelry in the Heart of New York |John Surico |November 13, 2014 |DAILY BEAST 

In August 2010, a formal “Be on the Lookout” list was created instructing staff to flag applications of tea party groups. Fact-Checking the Sunday Shows: June 22 |PunditFact.com |June 22, 2014 |DAILY BEAST 

And later it came out that the word “progressive” was also used to flag applications on another IRS “Be on the Lookout” list. Fact-Checking the Sunday Shows: June 22 |PunditFact.com |June 22, 2014 |DAILY BEAST 

Kyle was said to have served only as a lookout while the others jumped. Hero or Criminal? James Brady, the WTC Ironworker Who Jumped Off the Building |Michael Daly |March 25, 2014 |DAILY BEAST 

Sometime afterward he got in his Jeep and drove the winding road to Hanapepe lookout. Doug Kenney: The Odd Comic Genius Behind ‘Animal House’ and National Lampoon |Robert Sam Anson |March 1, 2014 |DAILY BEAST 

They appreciated by now that he was not the kind to give up without a fight, therefore they were on the lookout. The Homesteader |Oscar Micheaux 

He must keep a reasonably careful lookout for other travelers in order to avoid collision; also for defects in the highway. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Be on the lookout for blue Buick sedan, nineteen thirty-nine model, red wheels, being driven by Raymond Delancy. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The continuation of the shoal between the islands and Point Lookout was not clearly ascertained. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Keeping a sharp lookout he soon came to a road that ran in the direction he wished to go. The Courier of the Ozarks |Byron A. Dunn