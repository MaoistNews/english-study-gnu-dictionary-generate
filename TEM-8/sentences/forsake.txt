Long-term settlement of Earth orbit, the moon, Mars, and beyond requires explorers forsake Earth’s gravity—the steady downward force every Earthly animal has evolved to navigate over billions of years. Japan Proposes a Wild Concept for Making Artificial Gravity on the Moon |Jason Dorrier |July 11, 2022 |Singularity Hub 

It is appalling that we have now forsaken both the political spirit and the essential public policy machinery that was deployed so effectively just over a decade ago. We Must Learn From the Failed Global Response to COVID-19 |Kevin Rudd |December 29, 2021 |Time 

You forsake most notions of fashion for the efficiency of functionality. Why We Love ‘Hiking with Rappers’ |awise |October 28, 2021 |Outside Online 

Houston, like TCU, was one of the forsaken former SWC teams that didn’t make the cut for the Big 12 when it was created. Five teams to watch in Week 2 of college football (plus the rise of Central Florida) |Patrick Stevens |September 10, 2021 |Washington Post 

One of you may have to forsake the toaster oven you’ve enjoyed for the past decade in favor of the newer, smaller model your future roommate owns. How to decide what items stay and what goes when combining homes |Nicole Anzia |September 9, 2021 |Washington Post 

Every hour, the anthem is played, followed by Orthodox priests intoning prayers and beseeching God not to forsake Ukraine. EuroMaidan Protesters: We Want U.S. Protection |Jamie Dettmer |March 4, 2014 |DAILY BEAST 

But will he be willing to forsake his lucrative gig at Fox News to grind it out on the campaign trail? Who Does the GOP Really Have To Run Against Hillary in 2016? |Myra Adams |January 14, 2014 |DAILY BEAST 

Muhammad assumed this risk because he refused to forsake any opportunity for peace. Prophet Muhammad’s Rules of War |Qasim Rashid |November 20, 2012 |DAILY BEAST 

The Kremlin will have little choice but to forsake its mega-projects. Hydraulic Fracking's Putting the Screws to Vladimir and Friends |Justin Green |September 28, 2012 |DAILY BEAST 

He understood that to be leisurely is to forsake possibilities, even lives. Christopher Hitchens Eulogized by Roya Hakakian |Roya Hakakian |December 16, 2011 |DAILY BEAST 

From it I learned that, if I would gain heaven, I must forsake sin and live a pure life. The value of a praying mother |Isabel C. Byrum 

His many failures caused his friends to forsake him and he was put in prison for not paying his debts. The Wonder Book of Knowledge |Various 

May he hear your prayers, and be reconciled unto you, and never forsake you in the evil time. The Bible, Douay-Rheims Version |Various 

As favour and riches forsake a man, we discover in him the foolishness they concealed, and which no one perceived before. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

He hasn't the nerve to forsake his native heath and roam the wide world, a free and independent gentleman. Prison Memoirs of an Anarchist |Alexander Berkman