If they have symptoms like chest pain, shortness of breath or an abnormal heart beat, he says, they should see a doctor. College athletes show signs of possible heart injury after COVID-19 |Aimee Cunningham |September 11, 2020 |Science News 

She told the Blade she only sleeps a few hours a night, she has no strength and walking a few feet leaves her out of breath. Cuban doctor contracts coronavirus in ICE custody |Yariel Valdés González |September 9, 2020 |Washington Blade 

Although one of the major problems they have to deal with is rescuing people who refuse to evacuate, under their breath they thanked us for saving these homes. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 

For example, people with type 2 diabetes are often overweight and have shortness of breath. How special relativity can help AI predict the future |Will Heaven |August 28, 2020 |MIT Technology Review 

Alternative living, in my case vanlife, is a financial breath of fresh air. The New Camper Companies Redefining Road Travel |Alex Temblador |August 27, 2020 |Outside Online 

In the next breath, however, he is decrying the press misinterpretation of his Diana script. Harry’s Daddy, and Diana’s ‘Murder’: Royal Rumors In a New Play |Tom Sykes |January 4, 2015 |DAILY BEAST 

Throughout Christmas eve and day, the world is monitoring with bated breath. Sweden’s Burning Christmas Goat |Nina Strochlic |December 25, 2014 |DAILY BEAST 

He died in July after being grabbed around the throat by a cop and wrestled to ground where the breath flew out of him. The Wildly Peaceful, Human, Almost Boring, Ultimately Great New York City Protests for Eric Garner |Mike Barnicle |December 8, 2014 |DAILY BEAST 

“Every time you see me, you want to mess with me,” Garner exclaimed, short of breath. Before Eric Garner, There Was Michael Stewart: The Tragic Story of the Real-Life Radio Raheem |Marlow Stern |December 4, 2014 |DAILY BEAST 

His breath became so strained that he was forced to quit his job as a horticulturalist for the parks department. Before Eric Garner, There Was Michael Stewart: The Tragic Story of the Real-Life Radio Raheem |Marlow Stern |December 4, 2014 |DAILY BEAST 

It was one of those long moments that makes a fellow draw his breath sharp when he thinks about it afterward. Raw Gold |Bertrand W. Sinclair 

While you were admiring the long roll of the wave, a sudden spray would be dashed over you, and make you catch your breath! Music-Study in Germany |Amy Fay 

Cease ye therefore from the man, whose breath is in his nostrils, for he is reputed high. The Bible, Douay-Rheims Version |Various 

He caught his breath, he paused, then stepped within on tiptoe, and the hush of four thousand years closed after him. The Wave |Algernon Blackwood 

He is on the violin what Liszt is on the piano, and is the only artist worthy to be mentioned in the same breath with him. Music-Study in Germany |Amy Fay