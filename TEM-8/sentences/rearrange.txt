Is there any intervention that would do more than help to rearrange the rubble? Only Iraq Can Save Itself From Chaos |Jay Parini |June 26, 2014 |DAILY BEAST 

And so if the government could just get to the kitchen, rearrange some things, we could certainly party with the Haitians. ‘Clueless’: How the Greatest Clique of the ‘90s Transformed Into A Shakespearean Tragedy |Marlow Stern |May 30, 2014 |DAILY BEAST 

When Jackson asks if he can rearrange the seats before we start, she belts out sweetly: “Go nuts for cowboy butts!” There’s Something About Mary Lambert |Abby Haglage |September 19, 2013 |DAILY BEAST 

We rearrange our schedules so that we can spend more time catering to its every whim. The Economics of Puppy Management |Megan McArdle |February 22, 2013 |DAILY BEAST 

ARIES Incoming: You are bombarded with information now as plans change, rearrange, and caveats and amendments abound. Horoscopes July 3-9, 2011 |Starsky + Cox |July 2, 2011 |DAILY BEAST 

But it was a long time before he found any one who was willing to attempt to rearrange his scribbled thoughts. The Homesteader |Oscar Micheaux 

This means not alone that he shall remember them all, but there is a more serious trouble: he must often rearrange them. English: Composition and Literature |W. F. (William Franklin) Webster 

A reader creates nothing new; 77 all he does is to rearrange in his own mind the images already familiar. English: Composition and Literature |W. F. (William Franklin) Webster 

Finally he had to stop, undo the bundle, and rearrange every article in it, before he could induce it to “carry” smoothly. The Backwoodsmen |Charles G. D. Roberts 

If it is found necessary to rearrange any of the fronds, it may be done by means of a wet camel-hair brush. The Sea Shore |William S. Furneaux