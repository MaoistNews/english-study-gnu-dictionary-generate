Now, it appears that internet access is emerging as a new and troublesome determinant of health. One big hiccup in US efforts to distribute COVID-19 vaccines? Poor internet access. |By Tamra Burns Loeb, Et Al./The Conversation |February 10, 2021 |Popular-Science 

That worry has already prompted some vaccine makers to look for ways to tweak their shots to keep up with these troublesome newcomers. How coronavirus variants may pose challenges for COVID-19 vaccines |Erin Garcia de Jesus |January 27, 2021 |Science News 

It only feels like there’s something more imminently troublesome going on. How to keep your anxiety from spiraling out of control |Sara Chodosh |January 15, 2021 |Popular-Science 

After all, teeth are a notoriously troublesome aspect of human anatomy, and dental troubles don’t wait for a pandemic to end before they strike. Inside American dentistry’s identity crisis |Rachel Schallom |October 27, 2020 |Fortune 

Over the last few months, industries in the US have had to contend with a troublesome carbon dioxide shortage. US carbon dioxide supply is a bottleneck for Covid-19 vaccine distribution |Katherine Ellen Foley |September 12, 2020 |Quartz 

But this approach can be troublesome for a variety of reasons. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

Why the government is so vehemently eager to delist the grizzly remains a troublesome question. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

Reputations are constantly being disinterred, re-examined, re-purposed for use in the troublesome present. The Gods of Punk Are Back in New York City |Anthony Haden-Guest |September 27, 2014 |DAILY BEAST 

Yes, Moscow and Tehran are troublesome today for a variety of good reasons. Here's How to Dig Out of This 'Stupid Sh*t' U.S. Foreign Policy |Leslie H. Gelb |August 13, 2014 |DAILY BEAST 

The regime has, to date, never definitively weighed in on whether these troublesome critters had imperialist or fascist ties. Who Will Maduro Blame for Venezuela’s Blackout This Time? |Daniel Lansberg-Rodríguez |June 28, 2014 |DAILY BEAST 

My soul hateth your new moons, and your solemnities: they are become troublesome to me, I am weary of bearing them. The Bible, Douay-Rheims Version |Various 

He shall not be sad, nor troublesome, till he set judgment in the earth, and the islands shall wait for his law. The Bible, Douay-Rheims Version |Various 

As already reported in my telegram, fire from the Asiatic shore is at times troublesome, but I am taking steps to deal with it. Gallipoli Diary, Volume I |Ian Hamilton 

The first method is the more troublesome, but, without comparison, the better one. A Woman's Journey Round the World |Ida Pfeiffer 

I caught a violent cold the very same day: it has settled in my eyes, I believe, for they have been troublesome to me ever since. The Battle of Hexham; |George Colman