Yet space, to many, feels more frivolous, and thus gets hit harder by critics. Four Civilian Astronauts. Three Days in Orbit. One Giant Leap. Meet the Inspiration4 Crew |Jeffrey Kluger |August 10, 2021 |Time 

The lawyers for the Sandy Hook relatives have asked the judge in the case to rule that the proposed deposition of Clinton would be “unnecessary and frivolous.” Alex Jones’ Last Ditch Defense in Sandy Hook Lawsuit: Depose Hillary Clinton |Will Sommer |July 23, 2021 |The Daily Beast 

These days, AC is starting to sound like a less frivolous choice. In America’s least air-conditioned cities, brutal heat changes some people’s minds |Marc Fisher, Carissa Wolf, Michael Hingston |July 22, 2021 |Washington Post 

At its core, Cusack’s argument is that prosecutor-less prosecutions erode one of the major safeguards that protects people — often but not exclusively homeless people — against frivolous criminal cases. City Attorney’s Hands-Off Approach to Infractions Means Cops Act as Prosecutors |Jesse Marx |July 19, 2021 |Voice of San Diego 

During those interviews, the two brothers flitted between frivolous family chatter and serious discussion of coronavirus policy. Now CNN’s Chris Cuomo has a full-blown scandal on his hands |Erik Wemple |May 20, 2021 |Washington Post 

In Northanger Abbey, Jane Austen defends the novel against critics who dismiss it as frivolous and feminine. The Birth of the Novel |Nick Romeo |November 27, 2014 |DAILY BEAST 

The 1996 filing (which you can check out here) was, naturally, as silly and frivolous as the boycott push that came before it. When the Religious Right Attacked ‘The Little Mermaid’ |Asawin Suebsaeng |November 20, 2014 |DAILY BEAST 

Mizell called the Justice Department “arrogant” for bringing this frivolous case to the court. Honoring The Late John Doar, A Nearly Forgotten Hero Of The Civil Rights Era |Gary May |November 15, 2014 |DAILY BEAST 

“This is silly in its context but serious in its purpose, it is not frivolous,” Pelosi says. Pelosi to Boehner: I Quashed Impeachment, and So Can You |Eleanor Clift |August 1, 2014 |DAILY BEAST 

Blogging is a lifestyle agenda that can pay the bills, it becomes a serious business of frivolous things. Tavi Gevinson: From Teen Fashion Queen to Broadway Star |Arabelle Sicardi |July 12, 2014 |DAILY BEAST 

Many denounce the system of morning calls as silly, frivolous, and a waste of time. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Romarino paid a little court to her in his frivolous way; but she did not observe it, or, at all events, took no notice of it. Skipper Worse |Alexander Lange Kielland 

If the invention be frivolous or pernicious, the inventor cannot secure for it legal protection. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

I am going to be frivolous, coquette, and imagine myself a girl of the old Southern Set, when there were no new people. Ancestors |Gertrude Atherton 

Scarcely anything has been written against the French Academy, except frivolous and insipid pleasantries. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire)