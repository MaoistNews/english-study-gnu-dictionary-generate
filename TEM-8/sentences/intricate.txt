Montalto recalls the Naples of her youth with intricate paintings that often include a volcano. In the galleries: Rejuvenating the obsolete into unconventional art |Mark Jenkins |February 12, 2021 |Washington Post 

The natural consequence of a young quarterback’s growth is that teams throw ever more intricate schemes at him, disguises and unscouted looks. Patrick Mahomes has unmatched physical gifts. His intellect might be what sets him apart. |Sally Jenkins |February 5, 2021 |Washington Post 

You can also keep it basic by using one color and the simplest stitches in the book, or you can build on it—add different cables, intricate stitches, or a jacquard pattern to make all your senatorial-meme fantasies come true. How to knit your own Bernie mittens |Sandra Gutierrez G. |January 26, 2021 |Popular-Science 

While the women’s version has a more intricate design and color options, the men’s version is available in neutrals. Compression socks to ease aches and support travel |PopSci Commerce Team |January 20, 2021 |Popular-Science 

Many cordless trimmers will carry at least one hour of power on a single charge, so unless you’re doing something particularly intricate, you should make it out without extra whiskers holding on. The best beard trimmer: Shape your facial hair with ease |Carsen Joenk |January 19, 2021 |Popular-Science 

It features on its front and back covers an intricate “original” design, embossed in gold. Rand Paul’s Many Leather-Bound Books |Olivia Nuzzi |November 27, 2014 |DAILY BEAST 

It must be the compelling characters, the memorable lines, the intricate plots. Book Bag: 5 Novels Shakespeare Sort of Wrote |Lois Leveen |October 10, 2014 |DAILY BEAST 

The Mayans are lauded for their achievements—their art & architecture, intricate calendar, and developed culture. The Cave Where Mayans Sacrificed Humans Is Open for Visitors |Nina Strochlic |August 14, 2014 |DAILY BEAST 

These tents form an intricate retail ecosystem that can be loosely divided into two parts: barracas and ambulantes. The Girl From Ipanema Is Not Alone: Rio’s Famous Beach Is A Rich, Cultural Kaleidoscope |Brandon Presser |June 23, 2014 |DAILY BEAST 

Pac may not be as verbose as other rappers of his time, but his flow is intricate, and complicated to replicate. Broadway’s Rebel, Tellin’ You to Hear It: A Portrait of Saul Williams |Alex Suskind |June 17, 2014 |DAILY BEAST 

The intricate perforations of the lamp were inset with colored glass, and the result was a subdued and warm illumination. Dope |Sax Rohmer 

Afterwards we sounded its channel, and found a deep passage, but too narrow and intricate to be preferred to the eastern channel. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

This entrance to the bay is very intricate, and useless, since that to the south of the islands is so much better. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

The corridors are long, narrow and intricate passages, forming a complete underground net-work. The Catacombs of Rome |William Henry Withrow 

When I saw him he was coming down the steps; his feet, his finger and his lips moving in time to some intricate measure. Marguerite |Anatole France