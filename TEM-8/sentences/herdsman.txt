A couple of seemingly frustrated herdsmen were trying to steer cattle and goats through the narrow patches of pasture that remained. Zimbabwe’s climate migration is a sign of what’s to come |Andrew Mambondiyani |December 17, 2021 |MIT Technology Review 

She teams up with a herdsman who has been wrongfully accused of terrorism, and the two embark on a page-turning journey where they must do what it takes to survive. Here Are the 10 New Books You Should Read in November |Annabel Gutterman |November 1, 2021 |Time 

To get that wildlife-friendly tag, herdsmen need to clear multiple steps — from predator-proofing pens and reporting poaching, to removing shangdongs and freeing up land for wildlife reserves. Where Shepherds Save Wolves — Thanks to Buddhism |Charu Kasturi |March 10, 2021 |Ozy 

The years like Great Black Oxen tread the world And God the herdsman goads them on behind. American Dreams, 1923: Black Oxen by Gertrude Atherton |Nathaniel Rich |March 28, 2013 |DAILY BEAST 

The fresh imprint of a tiger's paw upon the pathway gives the same sort of feel to the Indian herdsman. Gallipoli Diary, Volume I |Ian Hamilton 

Galerius, an Illyrian herdsman, but exhibited more conspicuously upon the throne of empire the native barbarity of his character. The Catacombs of Rome |William Henry Withrow 

If a man has hired a herdsman for the cows or a shepherd for the sheep, he shall give him eight gur of corn per annum. The Oldest Code of Laws in the World |Hammurabi, King of Babylon 

Just about sundown the stately herdsman again appeared with his motley following. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

One of these stones, fifteen inches in diameter, broke through the roof of a cottage, and killed a herdsman and a bullock. The Rain Cloud |Anonymous