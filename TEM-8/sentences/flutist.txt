Expect an art market, dance classes, grooving DJ sets from the likes of Trilla Kay and morning yoga, soundtracked by a live flutist. The best things to do in the D.C. area the week of Aug. 26-Sept. 1 |Anying Guo, Hau Chu, Fritz Hahn, Kelsey Ables |August 26, 2021 |Washington Post 

Not only did Palin show off her skills as a flutist as well, but she was awarded the title of Miss Congeniality. 15 Hilarious Pageant Moments | |June 18, 2013 |DAILY BEAST 

He came of a musical family, his father being a flutist, while his mother played the piano and sang. Woman's Work in Music |Arthur Elson 

He would not go to Karrosch, or any of the large, important orchestras; none of the small ones wished a flutist. The Old Flute-Player |Edward Marshall and Charles T. Dazey 

An old German musician who used to play with him in the Orchestra told me that Lanier was the finest flutist he had ever heard. Literary Hearthstones of Dixie |La Salle Corbell Pickett 

He was a good flutist, gave four or five lessons a week, and played in the small orchestras of second-rate theatres. The Patriot |Antonio Fogazzaro 

The flutist was wild, but presently he calmed down, and played us a nice little tune. The Patriot |Antonio Fogazzaro