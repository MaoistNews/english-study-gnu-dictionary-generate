There was no way to test blood for HIV, and excluding gays was a prudent move. The Outrageous Celibacy Requirement for Gay Blood Donors |Jay Michaelson |November 22, 2014 |DAILY BEAST 

Excluding gays from secular marriage does not raise a federal question. All The Wrong Reasons to Ban Gay Unions |Jay Michaelson |November 7, 2014 |DAILY BEAST 

There have been certain exceptions, but the policy is tilted heavily to excluding Sikhs because of their beards and turbans. New York’s Sikhs Need Their Own Al Sharpton |Dean Obeidallah |August 8, 2014 |DAILY BEAST 

As Rajdeep Singh noted, excluding Sikhs from the NYPD is counterproductive to the very idea of community-based policing. New York’s Sikhs Need Their Own Al Sharpton |Dean Obeidallah |August 8, 2014 |DAILY BEAST 

As Israelis, we can testify that Hillel is increasingly excluding more of us, whose nation Hillel purports to embrace. How Hillel Is Losing Touch With the People It Purports to Embrace |Israeli Opposition Network |December 16, 2013 |DAILY BEAST 

Clay (excluding that type used in the manufacture of pottery) ranks fifth in financial value. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

This is gained by darkening the house, and excluding all persons, except when their presence is indispensable. Elements of Agricultural Chemistry |Thomas Anderson 

She sought to spare him pain by excluding from her manner all suggestion that things were other than she desired. Mistress Wilding |Rafael Sabatini 

Therefore, Socialism, excluding competition inspired by self-interest would obliterate the social dividend. The Inhumanity of Socialism |Edward F. Adams 

He has been able to prevent the annual attacks by using cold baths and excluding meat, tea, coffee and alcohol from the diet. The Treatment of Hay Fever |George Frederick Laidlaw