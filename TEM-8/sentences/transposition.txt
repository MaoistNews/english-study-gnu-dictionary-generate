Wherever references of this kind do occur, they as often assume the shape of some amusing transposition. A Cursory History of Swearing |Julian Sharman 

The necessity for this insertion shows that Westphal's transposition is not in itself an easy one. The Modes of Ancient Greek Music |David Binning Monro 

Another means to acquire technical mastery is through transposition. Piano Mastery |Harriette Brower 

When by an unhappy transposition, any of the hinder parts of the body first present themselves. The Works of Aristotle the Famous Philosopher |Anonymous 

It seems to me that a transposition, vice vers, of the admonitions would be equally salutary and just. George Eliot's Life, Vol. I (of 3) |George Eliot