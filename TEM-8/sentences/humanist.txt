I used dépaysement when, as a humanist in a land of engineers, I asked unpopular questions about the emotional effects of digital technology. The Pandemic Made Us Strangers to Ourselves. Will We Have Learned Anything When It's Over? |Sherry Turkle |March 16, 2021 |Time 

Scotland, for example, had more humanist weddings than Christian ones in 2019. Can Uganda Help Africa Break the Church’s Grip on Weddings? |Charu Kasturi |December 14, 2020 |Ozy 

Giovanni Gaetani, membership engagement manager at the London-based Humanists International, which is funding projects to encourage humanist weddings and funerals in Uganda and Lithuania, says the popularity of such ceremonies is growing globally. Can Uganda Help Africa Break the Church’s Grip on Weddings? |Charu Kasturi |December 14, 2020 |Ozy 

Kato Mukasa, a lawyer who leads the AHCN and is the UHASSO chair, says a survey conducted between February 2018 and February 2020 shows the country has more than 3,000 humanists. Can Uganda Help Africa Break the Church’s Grip on Weddings? |Charu Kasturi |December 14, 2020 |Ozy 

You have a bunch of humanists sort of generating hypotheses, but they don’t know exactly what’s possible. Netflix’s Reed Hastings on Rejecting Brilliant Jerks, the Power of Big Vacations, and Spending $15 Billion on Content |Eben Shapiro |September 13, 2020 |Time 

“You almost saw a humanist as well as an absolute demonic side,” she says now. Westgate's Chilling Security Video Reveals Shopping Mall Bloodbath |Nina Strochlic |September 15, 2014 |DAILY BEAST 

“There should be humanist alternatives to church in basic training,” he said. U.S. Air Force: Swear to God—or Get Out |Dave Majumdar |September 8, 2014 |DAILY BEAST 

Once, the humanist idea used to animate the very core of the university. Can Higher Education Really Save Our Humanity? |James Poulos |February 1, 2014 |DAILY BEAST 

Instead, liberals were attacked for their “secular humanist” ideology as opposed to their philosophy of governing. Ted Cruz and Donald Trump a Sideshow at Fire and Brimstone Rally |Ben Jacobs |August 12, 2013 |DAILY BEAST 

But if the religious Zionist youth movements are any indication, those values will be anything but universal or humanist. Racism and Religious Zionist Youth Movements: Own Up |Dr. Assaf David |May 30, 2013 |DAILY BEAST 

But if I were asked to describe myself in a single word, I should call myself a Humanist. God and my Neighbour |Robert Blatchford 

The Humanist remedy is to remove the causes which lead or drive men into crime, and so to prevent the manufacture of "sinners." God and my Neighbour |Robert Blatchford 

An anthology of the literature of social protest, with an introduction by Jack London, who calls it "this humanist Holy-book." The Book of Life: Vol. I Mind and Body; Vol. II Love and Society |Upton Sinclair 

Or, to quote the famous humanist creed of Protagoras, as Schiller is so fond of doing, "Man is the measure of all things." The Behavior of Crowds |Everett Dean Martin 

The humanist method must be extended to the whole subject-matter of education, even to a revaluation of knowing itself. The Behavior of Crowds |Everett Dean Martin