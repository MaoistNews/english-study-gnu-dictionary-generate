Two have been charged with counts listed as federal crimes of terrorism, relating to destruction of government property to intimidate or coerce the government. Five more charged in Capitol riot allegedly teamed with Kansas City Proud Boys |Spencer Hsu, Rachel Weiner |February 11, 2021 |Washington Post 

Pregnant people have historically been considered a “vulnerable” population—a designation also applied to groups, such as children and the incarcerated, who may be coerced into participating in research. With Little Data to Guide Them, Pregnant Health Care Workers Are Stepping Up to Get Vaccinated Against COVID-19 |Jamie Ducharme |January 21, 2021 |Time 

Once you find the ghost’s favorite room, you may need to coerce them to come out since some are camera shy. Are you a ‘Phasmophobia’ pro? Here are some alternate rules to keep the scares fresh. |Elise Favis |January 11, 2021 |Washington Post 

Any organization that feels coerced into paying a ransom should, at a minimum, analyze the potential risks of sanctions, especially if Bitcoin payments eventually find their way to a terrorist organization. The hacker ‘ceasefire’ with hospitals is over—and that should terrify us |jakemeth |December 9, 2020 |Fortune 

She moved with her three kids to a new state and connected with a nonprofit that supports survivors of coerced debt and domestic violence. The coming war on the hidden algorithms that trap people in poverty |Karen Hao |December 4, 2020 |MIT Technology Review 

A fellow justice also accused McCaffery of attempting to coerce him into opposing Castille. Judges Behaving Badly: A Great American Tradition |Asawin Suebsaeng |October 30, 2014 |DAILY BEAST 

Once again Russia brandishes the threat of a gas cutoff to squeeze Kiev and coerce Europe. In Ukraine, Winter Is Coming |Anna Nemtsova |September 23, 2014 |DAILY BEAST 

Alito opened the door by questioning the “anomalous” Abood precedent, which lets states coerce union members into paying dues. The Conservative Case for Unions After the Harris v. Quinn Decision |James Poulos |July 2, 2014 |DAILY BEAST 

In other instances, CIA recruiters used thinly veiled threats to coerce their cooperation. The CIA Tried Hard to Recruit Spies Among the Al Qaeda Prisoners at Gitmo |Daniel Klaidman |November 28, 2013 |DAILY BEAST 

Religious liberty, Jefferson argued, denies the majority any right to coerce a dissenting minority, even one hostile to religion. Thomas Jefferson’s Quran: How Islam Shaped the Founders |R.B. Bernstein |September 29, 2013 |DAILY BEAST 

This Constitution does not attempt to coerce sovereign bodies, States, in their political capacity. Select Speeches of Daniel Webster |Daniel Webster 

It guides, it need not coerce or necessitate, though it may. The Contemporary Review, January 1883 |Various 

To coerce them into a reluctant self-denial could be no possible object to him either of wish or hope. Expositor's Bible: The Second Epistle to the Corinthians |James Denney 

Hopeless to search further among empty swamps and forests, to grope at large in this hushed wilderness, to coerce a jungle. Where the Pavement Ends |John Russell 

Hume, though we have found him censuring the conduct of Franklin, was opposed to any attempt to coerce America. Life and Correspondence of David Hume, Volume II (of 2) |John Hill Burton