At one point, we all crawled to a central point equidistant from our respective holes. My Worst Hike: I Got Food Poisoning on the John Muir Trail |cobrien |January 6, 2022 |Outside Online 

After work one quiet Sunday night I spotted “The World” on the shelf in the “Essays” section, equidistant from volumes by James Baldwin and Virginia Woolf. World class: Remembering legendary travel writer Jan Morris |Liza Weisstuch |December 10, 2020 |Washington Post 

Meanwhile, respondents put Obama at 3.9 and Romney at 6.1—each of them equidistant (1.1 points) from the center. Michael Tomasky on Mitt Romney and the Magical Powers of a Lie |Michael Tomasky |April 10, 2012 |DAILY BEAST 

The Chester road enters the valley at a point about equidistant from either end. Famous Adventures And Prison Escapes of the Civil War |Various 

They are almost equidistant from each other, and the next to the lowest one is the longest. Pharaoh's Broker |Ellsworth Douglass 

The windows of this apartment were two in number, and, equidistant from the doors, were considerably elevated above the floor. Hardscrabble |John Richardson 

Some Swastikas have three dots placed equidistant around each of the four ends (fig. 12c). The Swastika |Thomas Wilson 

Parallel lines are those equidistant one from the other throughout their length, as in Figure 42. Mechanical Drawing Self-Taught |Joshua Rose