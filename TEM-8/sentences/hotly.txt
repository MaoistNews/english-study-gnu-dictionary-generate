It’s not critical, but if you’re able to warm the punch cups or mugs in advance by filling them with hot water as the drink warms, your drinks will stay warm longer once you serve them. This cocktail can keep you warm around the fire pit — and you can make it there, too |M. Carrie Allan |November 20, 2020 |Washington Post 

When I was a kid, they sold hot dogs and coffee from carts because lift lines could last an hour. How COVID-19 Could Actually Be Good for Skiing |Marc Peruzzi |November 20, 2020 |Outside Online 

While many countries now have national apps, there hasn’t been a federal effort in the US—which happens to be the world’s coronavirus hot spot. Do digital contact tracing apps work? Here’s what you need to know. |Cat Ferguson |November 20, 2020 |MIT Technology Review 

It typically has mild winters, and very hot and humid summers, with pleasant fall and spring seasons that include vibrant wildflowers and changing leaves. Dallas : LGBTQ Living in the Lone Star State |LGBTQ-Editor |November 19, 2020 |No Straight News 

The area is also known as a geothermal hot spot because of its roughly 10,000 geysers, hot springs, mud pots, and steam vents, the greatest concentration in the world. Yellowstone National Park Is a Geyser Wonderland |Emily Pennington |November 18, 2020 |Outside Online 

While medications have improved, the use of preventative drugs like Truvada remains hotly debated. Victor Mooney’s Epic Adventure for His Dead Brother |Justin Jones |October 19, 2014 |DAILY BEAST 

One of the most hotly anticipated films of the year is Birdman. Exclusive: Watch a Clip From ‘Birdman,’ Featuring an Award-Worthy Turn by Michael Keaton |Marlow Stern |October 1, 2014 |DAILY BEAST 

Disney and Lucasfilm released the hotly anticipated news, along with a photo of the cast doing a table read in London. Lupita Nyong’o Joins ‘Star Wars: Episode VII’ Cast |Marlow Stern |June 2, 2014 |DAILY BEAST 

The Common Core Standards Initiative has been hotly debated since it was first introduced in 2009. The Wingnut War On Common Core Is A Plot To Destroy Public Schools |Caitlin Dickson |May 7, 2014 |DAILY BEAST 

First, Hannah visits Adam in his dressing room prior to his hotly anticipated opening night turn in Major Barbara. The Excellent Season 3 Finale of ‘Girls’ Caps Off Its Best Season Yet |Marlow Stern |March 24, 2014 |DAILY BEAST 

On the fall of Comyn, his followers pressed forward and blows were hotly exchanged. King Robert the Bruce |A. F. Murison 

There was something in his glance that caused the queen to lower her eyes and her face to flush hotly. The Weight of the Crown |Fred M. White 

Daphne's jealousy made him ridiculous; he resented it hotly; yet he knew he was not altogether blameless. Marriage la mode |Mrs. Humphry Ward 

“A woman who does her duty is not to be accused of misusing anything,” cried Miss Temperley hotly. The Daughters of Danaus |Mona Caird 

For three hours the three big frigates hotly chased the Constitution and Levant, but let the Cyane go. Stories of Our Naval Heroes |Various