Lawmakers are expected to be in Washington until the start of October, after which they are scheduled to return to their home districts for the last weeks of the campaign season. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

She is expected to continue in her role next year, if Democrats retain control the House. Trump moves closer to Pelosi in economic aid talks, and House speaker must decide next move |Rachael Bade, Erica Werner |September 17, 2020 |Washington Post 

Biden’s team is going after a population that is generally more averse to in-person interactions and more of his supporters are expected to cast ballots over the next month, either by mail or through early voting. Trump and his campaign try to allay concerns about trailing Biden in television ads |Michael Scherer, Josh Dawsey |September 17, 2020 |Washington Post 

Even in states that haven’t made absentee voting easier, the number of ballot requests is still expected to spike. Vote by mail: Which states allow absentee voting |Kate Rabinowitz, Brittany Mayes |September 17, 2020 |Washington Post 

Redfield told the panel he expected a vaccine to start being available in November or December. Top health official says states need about $6 billion from Congress to distribute coronavirus vaccine |Lena H. Sun |September 16, 2020 |Washington Post 

The resources were what you might expect: Dining room, a media center, a library, a TV room, a meeting room, a computer room. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

Plus, expect outside players to take actions related to the conflict. In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

The relationships, and motivations of their chief participants, are as tangled and shady as you expect of the super-rich. The Real-Life ‘Downton’ Millionairesses Who Changed Britain |Tim Teeman |December 31, 2014 |DAILY BEAST 

You expect soldiers of all ranks to understand the need to respect the chain of command, regardless of personal feelings. We Need Our Police to Be Better Than This |Nick Gillespie |December 31, 2014 |DAILY BEAST 

The name that most Republicans seem both to expect and dread to consider running is Vito Fossella. The Felon Who Wouldn’t Leave Congress |Ben Jacobs, David Freedlander |December 23, 2014 |DAILY BEAST 

You speak with about as little reflection as we might expect from one of those children down there playing in the sand. The Awakening and Selected Short Stories |Kate Chopin 

Why expect that extraordinary virtues should be in one person united, when one virtue makes a man extraordinary? Pearls of Thought |Maturin M. Ballou 

Or, if I escaped these dangers for a day or two, what could I expect but a miserable death of cold and hunger? Gulliver's Travels |Jonathan Swift 

He wrote a letter to Sir Hugh Wheeler warning the gallant old general that he might expect to be attacked forthwith. The Red Year |Louis Tracy 

I did not find the Aristocracy so remarkable for physical perfection and beauty as I had been taught to expect. Glances at Europe |Horace Greeley