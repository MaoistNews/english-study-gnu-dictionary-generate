Then they intended to bury her, but she looked more alive than dead, and she still had such pretty red cheeks. In New Brothers Grimm 'Snow White', The Prince Doesn't Save Her |The Brothers Grimm |November 30, 2014 |DAILY BEAST 

In the special, Workman plays the old man who, as a cabin boy, watched the pirates bury their treasure. Garfield Television: The Cat Who Saved Primetime Cartoons |Rich Goldstein |November 5, 2014 |DAILY BEAST 

Us is me and Gus, driving our bus across the land; when we die, just bury us together, hand in hand. Well, La Ti Da: Stephin Merritt’s Winning Little Words of Scrabble |David Bukszpan |October 11, 2014 |DAILY BEAST 

The landscape looks something like the marsh behind the Toys ‘R’ Us where Tony Soprano might bury a body in Jersey. Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 

Just days before, the officer told him, 19 bodies of Ebola victims were left lying outside with few men to bury them. CDC: 'Window Is Closing' on Containing Ebola |Abby Haglage |September 2, 2014 |DAILY BEAST 

If they are still Moderns and alive, I defy you to bury them if you are discussing living questions in a full and honest way. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

The swift breeze seemed to Edna to bury the sting of it into the pores of her face and hands. The Awakening and Selected Short Stories |Kate Chopin 

Two years before her death Mrs. Otis was glad to bury her mortification and misery in Rosewater. Ancestors |Gertrude Atherton 

Meanwhile Benny looked on with great delight as Watch tried to bury his bone with only one paw to dig with. The Box-Car Children |Gertrude Chandler Warner 

Since the number of casualties was extremely high during this battle, Jackson allowed Banks to bury his dead the following day. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey