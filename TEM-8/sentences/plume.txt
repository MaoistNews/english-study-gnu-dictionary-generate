The first was a plume of moisture from Tropical Storm Fausto, far to the south, which managed to travel north to California on the wind and provide just enough moisture to form clouds. What’s behind August 2020’s extreme weather? Climate change and bad luck |Carolyn Gramling |August 27, 2020 |Science News 

The extent to which fecal aerosol plumes are infecting people with the SARS-CoV-2 virus isn’t known, said Queensland’s Morawska. Scientists found coronavirus in a long-vacant apartment. A possible spreader? ‘Fecal aerosol plumes’ |Claire Zillman, reporter |August 27, 2020 |Fortune 

It was one of the largest wildfire smoke plumes ever seen in the stratosphere, says Jessica Smith. Australian wildfires pumped smoke to record heights |Maria Temming |July 27, 2020 |Science News For Students 

The rising plume lifted up record amounts of water and carbon monoxide. Australian wildfires pumped smoke to record heights |Maria Temming |July 27, 2020 |Science News For Students 

Those winds have never been observed around similar plumes, researchers report online May 30 in Geophysical Research Letters. Smoke from Australian fires rose higher into the ozone layer than ever before |Maria Temming |June 15, 2020 |Science News 

For one month in 2013, a British street artist known by the nom de plume of Banksy hypnotized the city of New York. Catch Him If You Can: Reliving Banksy’s New York Invasion |Alex Suskind |November 14, 2014 |DAILY BEAST 

Borgman has escaped through a tunnel, leaving a plume of smoke in his wake. The Twisted Sadism of ‘Borgman’ |Alex Suskind |June 28, 2014 |DAILY BEAST 

Reprinted with the permission of Plume, a member of the Penguin Group. Rick Moody: Why I Write |Rick Moody |February 1, 2013 |DAILY BEAST 

Mark Owen (the nom de plume for Matt Bissonnette) carefully recounts his life and career as a member of SEAL Team 6. From ‘Zero Dark Thirty’ to ‘Medal of Honor’ Video Game: The SEAL Team Six Gift Guide |Benjamin Schor |December 19, 2012 |DAILY BEAST 

As ocean currents head eastward across the Pacific, the plume is expected eventually to hit the West Coast of the United States. Japanese Debris Plume From Tsunami Migrating Across Pacific Ocean |Daniel Stone |March 9, 2012 |DAILY BEAST 

It mounted straight as a plume for a little way, until it met the cool air of evening which was beginning to fall. The Bondboy |George W. (George Washington) Ogden 

Each tiny tree was a plume of leaves; the rows stretched out to the hilltop, and over. The Bondboy |George W. (George Washington) Ogden 

The sun-kissed flowers plume the day with colour, and fling incense to the winds. Charles Baudelaire, His Life |Thophile Gautier 

Corn Plume looked down at the Squash Maiden sitting on her blanket at his feet. Stories the Iroquois Tell Their Children |Mabel Powers 

Her heart sang, when she heard the voice of Corn Plume, for she knew that he was calling her. Stories the Iroquois Tell Their Children |Mabel Powers