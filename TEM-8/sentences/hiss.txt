The cascading raindrops, the hiss of cars on glistening streets, the splashes of pedestrians striding through puddles, all showed up. After many dry days, Saturday was wet, or at least fairly wet |Martin Weil |October 17, 2021 |Washington Post 

He stumbled upon a hiss coming from somewhere in the constellation Sagittarius, in the direction of the center of the galaxy. How radio astronomy put new eyes on the cosmos |Christopher Crockett |August 31, 2021 |Science News 

Arno Penzias and Robert Wilson, engineers at Bell Labs, were stymied by a persistent hiss in the house-sized, horn-like antenna they were repurposing for radio astronomy. How radio astronomy put new eyes on the cosmos |Christopher Crockett |August 31, 2021 |Science News 

The highs sound a little crunchy and there’s a very slight hiss when no sound is happening. Jaybird Run wireless earbud review: solid sound for your sweat and swole sessions |empire |June 24, 2021 |Popular-Science 

The contention was that a sellout was taking place led by, of all people, Richard Nixon, who originally exposed Alger Hiss. The End of the Illusion: America Finally Learns Its Limits |Jacob Heilbrunn |April 30, 2013 |DAILY BEAST 

Others hiss at reporters they disagree with, creating a toxic and tense environment. Knox Case Nears Its Gaudy End |Barbie Latza Nadeau |September 29, 2011 |DAILY BEAST 

When the hiss of reptiles turns to words, you hear something that you have never heard and will never forget. They Saw It Coming |Stanley Crouch |December 30, 2008 |DAILY BEAST 

Back in 2005, for instance, he argued that the U.S. housing bubble was starting to hiss badly. The Five Financial Gurus You Can Still Trust |Dave Kansas |October 19, 2008 |DAILY BEAST 

Smith's method usually gives good results, as does also the more simple method of Hiss (p. 263). A Manual of Clinical Diagnosis |James Campbell Todd 

When it is inconvenient to stain before the smears have dried, capsules can be shown by the method of Hiss. A Manual of Clinical Diagnosis |James Campbell Todd 

With an explosive hiss, gray jets of live steam erupted from pipes around the edge of the room. Hooded Detective, Volume III No. 2, January, 1942 |Various 

And Edom shall be desolate: every one that shall pass by it, shall be astonished, and shall hiss at all its plagues. The Bible, Douay-Rheims Version |Various 

As the leader finished her remarks Mrs. Haight brought her teeth together with a snap and shot through them a little hiss. Ancestors |Gertrude Atherton