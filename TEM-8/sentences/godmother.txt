Rusty Warren, a 1960s comedian, called a godmother of the sexual revolution, died on May 25 at 91 from chronic obstructive pulmonary disease at a caregiver’s home in Lagura Hills, Calif. In memoriam: Remembering queer lives lost in 2021 |Kathi Wolfe |December 30, 2021 |Washington Blade 

His mother died when he was 8 years old, and his godmother, who helped raise him, also has died. Family of man killed by D.C. police demand answers about what prompted officer to shoot |Peter Hermann, Clarence Williams |September 2, 2021 |Washington Post 

The Queen became godmother to Duleep Singh’s children, including Sophia Duleep Singh, who would later become a suffragette. The Little-Known History Behind the People of Color Who Joined the Royal Family Long Before Meghan |Suyin Haynes |March 12, 2021 |Time 

I have been honored and privileged to be designated as godmother of four people — all children of various close friends. Miss Manners: How inclusive should I be in my obit? |Judith Martin, Nicholas Martin, Jacobina Martin |March 12, 2021 |Washington Post 

Virtual Kim swans in every so often to dish out advice like a buxom fairy godmother. Inside ‘Kim Kardashian: Hollywood’: The Reality Star’s Aspirational App and Vainest Project Yet |Frances McInnis |June 23, 2014 |DAILY BEAST 

The boy had been back at the playground with his godmother early Sunday evening when he decided he wanted an ice. This Brooklyn 6-Year-Old’s Murderer Is Still on the Loose |Michael Daly |June 3, 2014 |DAILY BEAST 

The monster was still beyond imagining when Prince, known as P.J., told his godmother that he wanted an Italian ice. This Brooklyn 6-Year-Old’s Murderer Is Still on the Loose |Michael Daly |June 3, 2014 |DAILY BEAST 

Singled out for opprobrium was the planned exhibit on Margaret Sanger, birth-control crusader and godmother of Planned Parenthood. Michele Bachmann’s Crazy War on Women’s History |Michelle Cottle |May 8, 2014 |DAILY BEAST 

A former legislative director of Ohio Right to Life, Porter is in many ways the godmother of the heartbeat movement. Janet Folger Porter, Abortion Warrior, on Her Heartbeat Crusade |Michelle Cottle |July 7, 2013 |DAILY BEAST 

Belle was to be godmother and had to be got down; which was impossible, as the jester Euclid says. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

Her godmother had twice written to her, and Babette was now hoping to see her and her daughters in Interlaken. Rudy and Babette |Hans Christian Andersen 

Half-way up stood the74 boarding-house where the godmother was living. Rudy and Babette |Hans Christian Andersen 

Two thousand pounds a year left you by your godmother; the very same you told us you know. Memoirs of Mr. Charles J. Yellowplush |William Makepeace Thackeray 

I was up at the castle a great deal, because the Baroness Maximiliana of Wallersttten was my godmother. Maezli |Johanna Spyri