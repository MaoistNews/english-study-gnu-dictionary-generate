The final margin was a throwback to the disappointing Super Bowls of the 1980s. That really was one of the least enjoyable Super Bowls of all time |Neil Greenberg |February 9, 2021 |Washington Post 

It might be disappointing, but you may have to build up your tolerance so you can finally play titles like Halo or Apex Legends. Video games can cause motion sickness—here’s how to fight it |Sandra Gutierrez G. |February 2, 2021 |Popular-Science 

In September, the company posted disappointing earnings, which Gill lampooned as a “Chernobyl experience.” The GameStop stock craze is about a populist uprising against Wall Street. But it’s more complicated than that. |David J. Lynch |February 1, 2021 |Washington Post 

It’s disappointing that this year, the academic medicine community seems to have shrugged its shoulders. The pandemic’s lasting effects on young medical workers |Terry Nguyen |February 1, 2021 |Vox 

Wizards forward Davis Bertans had yet another disappointing shooting night, going 3 for 12 from three-point range, but he scored a critical 11 points. Wizards stun Nets with wild sequence in the final seconds |Ava Wallace |February 1, 2021 |Washington Post 

Yes, it was a fairly disappointing year in music—one devoid of Goth teen prodigies, Yeezy, and galvanizing rock anthems. The 14 Best Songs of 2014: Bobby Shmurda, Future Islands, Drake, and More |Marlow Stern |December 31, 2014 |DAILY BEAST 

Two years later, further tests were done, but this time to a disappointing conclusion. The Chinese Town Descended From Romans? |Nina Strochlic |December 4, 2014 |DAILY BEAST 

On the other hand, what broken and disappointing ground it is. A Reminder: Our Justices are Politicians in Robes |Jedediah Purdy |November 13, 2014 |DAILY BEAST 

The response the Foleys received was, for the most part, beyond disappointing—little more than a “pat on the head,” John said. Foley Family to White House: You Saved Bergdahl. Why Not Our Son? |Eli Lake |October 24, 2014 |DAILY BEAST 

Williamson stands by his record of rarely disappointing a customer when they are at a loss as to what suits them. The Harlem Hat Shop You Have to Visit |Justin Jones |October 24, 2014 |DAILY BEAST 

Many of the pneumatic actions made to-day, however, are disappointing in these particulars. The Recent Revolution in Organ Building |George Laing Miller 

It is without tower or spire of considerable height and somewhat disappointing when viewed from the exterior. British Highways And Byways From A Motor Car |Thomas D. Murphy 

This was very disappointing to him, as he wanted to make inquiries concerning the manner of their escape. The Everlasting Arms |Joseph Hocking 

They were to me disappointing efforts, if they were meant to be demonstrations of the success of co-operation. Third class in Indian railways |Mahatma Gandhi 

It was rather disappointing to them to see his face so melancholy, when they expected him to be full of animation and pleasure. Eric, or Little by Little |Frederic W. Farrar