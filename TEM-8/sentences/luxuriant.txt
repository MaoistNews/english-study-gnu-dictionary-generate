If water be present on the surface of Venus and if oxygen be a constituent of its atmosphere, we might expect to find in that planet a luxuriant tropical life. Hope for life on Venus survives for centuries against all odds |Tom Siegfried |September 25, 2020 |Science News 

Anderson is covering her breasts, water spritzing at her face, Williams is shot in luxuriant side-profile. Prince Fielder’s Demi Moore Moment: World Loses It Over Athlete Without Six-Pack |Tim Teeman |July 10, 2014 |DAILY BEAST 

In between, luxuriant stretches of time pass in which there is no action at all. Death Imitates Art |Michael Tomasky |July 20, 2012 |DAILY BEAST 

Rather, he seeks recognition for his luxuriant beard and colorful costumes, and perhaps for his romantic success. Let Them Say F--k |Reihan Salam |August 1, 2010 |DAILY BEAST 

The mountain ranges continue drawing nearer and nearer to each other, and the woods become thicker and more luxuriant. A Woman's Journey Round the World |Ida Pfeiffer 

But here vegetation is so luxuriant, that even the pruned and grafted tree springs up like the native of the forest. Journal of a Voyage to Brazil |Maria Graham 

The clove-tree is somewhat smaller, and cannot boast of such luxuriant foliage, or such fine large leaves as the nutmeg-tree. A Woman's Journey Round the World |Ida Pfeiffer 

At any rate, this pit is a very lovely one, abounding in the most luxuriant vegetation. A Woman's Journey Round the World |Ida Pfeiffer 

This rustic porch, overhung with luxuriant vines, evidently served as the family sitting-room. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue