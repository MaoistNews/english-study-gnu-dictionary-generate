Later in those same exercises, the grenade, still unexploded in this exercise, flies back for a landing, and the Marine catches it by a tether. The US Marines are testing flying, remote-controlled grenades |Rob Verger |July 13, 2021 |Popular-Science 

Without it, the franchise loses some crucial tether to reality. The rise and rise and rise of the Fast & Furious franchise |Emily VanDerWerff |June 25, 2021 |Vox 

There’s a loose overlay of drivers doing crimes — and breaking up crimes — but they attempt to maintain a vague tether to reality. The rise and rise and rise of the Fast & Furious franchise |Emily VanDerWerff |June 25, 2021 |Vox 

For kids unable to see friends, options such as messaging apps and video games gave them an essential tether to their old lives. After pandemic free-for-all, parents struggle to reinstate screen-time rules |Heather Kelly |June 24, 2021 |Washington Post 

So I suspect that Xi Jinping will try to maintain a tether on any North Korea initiative so that we don’t drift too far away. 'This Is a Window of Opportunity.' Ret. General Vincent K. Brooks on Why Things Might Be Moving Again With North Korea |Charlie Campbell / Shanghai |June 24, 2021 |Time 

We would lack a human face as our symbol; we would exist in the ether of ideas with no concrete stake in the ground to tether us. 128 Years Old and Still a Looker: Happy Birthday to Lady Liberty |Elizabeth Mitchell |October 28, 2014 |DAILY BEAST 

In this conversation, Rick realizes that to survive, he must tether himself to the present—to these people. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

Power for the sensors flows up the tether and data flows down. Why Old-School Airships Now Rule Our Warzones |Bill Sweetman |June 30, 2014 |DAILY BEAST 

Even the always-energetic Atti looked like he was at the end of his tether. My Attack of Model Jealousy |Anonymous |February 9, 2014 |DAILY BEAST 

Jane was constantly at the end of her emotional tether, which manifested brilliantly on stage. Jane Fonda’s New Biography: 14 Juicy Bits |Lizzie Crocker |August 23, 2011 |DAILY BEAST 

It indicated that things were going very badly indeed; that Laura was at the end of her tether. The Creators |May Sinclair 

And I have to stand by and see you at the end of your tether, hurt and frightened, and to know that I can do nothing for you. The Creators |May Sinclair 

The arrow-swift horses of a Persian trader slept in one stall; a tall dromedary shook his tether in another. God Wills It! |William Stearns Davis 

And when your really scientific ragger sinks to this, he is nearing the end of his tether. The Gold Bat |P. G. Wodehouse 

Pursuing such an inquiry with regard to Frederick Chopin, we find ourselves, however, soon at the end of our tether. Frederick Chopin as a Man and Musician |Frederick Niecks