By Thursday morning, nine of the 13 closed Metro stations were open, and the National Park Service had reopened the Lincoln Memorial and walkways along the Reflecting Pool. The fortress around downtown D.C. is being dismantled. But heightened security may remain. |Emily Davies, Michael Brice-Saddler, Peter Hermann |January 21, 2021 |Washington Post 

In addition to the requisite waterslides, the park has plank walkways from which visitors can see the water’s colorful mineral deposits. In Big Sky country, a pandemic-era fly-fishing getaway |Carl Fincke |January 21, 2021 |Washington Post 

On the other side of the walkway was a three-tier conveyor belt system. A Temp Worker Died on the Job After FedEx Didn’t Fix a Known Hazard. The Fine: $7,000. |by Wendi C. Thomas, MLK50: Justice Through Journalism |December 23, 2020 |ProPublica 

It’s like the whole floor is a crack, and there’s a tiny little walkway you have to cross to get care. Pregnant in the pandemic? It helps to have good Wi-Fi. |Cat Ferguson |December 14, 2020 |MIT Technology Review 

That means bagging your leaves, shoveling snow off your walkway or hiring someone to do it for you. Hints From Heloise: Not so neighborly neighbors |Heloise Heloise |November 30, 2020 |Washington Post 

But he loses his backpack in the process and it stays with the cops as he flees down the walkway toward Brooklyn. Protesters Slimed This Good Samaritan Cop |Michael Daly |December 16, 2014 |DAILY BEAST 

A number of bottles and other debris came down upon the demonstrators and cops on the roadway from the pedestrian walkway above. Protesters Slimed This Good Samaritan Cop |Michael Daly |December 16, 2014 |DAILY BEAST 

He had reassembled the weapon in a bathroom and stepped out onto a fourth-floor walkway overlooking an atrium. Inside the Washington Navy Yard’s Building 197 During Alexis’s Rampage |Michael Daly |September 19, 2013 |DAILY BEAST 

Its familiar spires reach into the sky, the arched walkway hovering high above. Harry Potter and the Plotless Theme Park |Andy Dehnart |July 15, 2010 |DAILY BEAST 

When they stood upon the walkway and had started toward home, her father paused. The Homesteader |Oscar Micheaux 

As they went up the walkway, the large front doors parted, and a handsome elderly woman came forth. The Homesteader |Oscar Micheaux 

He had her arm—held it close, as they passed through the station and crossed the walkway to where an inclosed auto stood. The Homesteader |Oscar Micheaux 

They had just crossed the broad plaza at Fifty-ninth Street and entered the walkway that leads to the Mall. The Foolish Virgin |Thomas Dixon 

He rammed it under a drum of gasoline and ran it to the walkway nearest to the floating plane. Astounding Stories of Super-Science, August 1930 |Various