So astronomers only had a brief window of time to gather as much data as they could about the object before it went on its merry way. The debate continues: ‘Oumuamua could be remnant of Pluto-like planet |Jennifer Ouellette |March 18, 2021 |Ars Technica 

We don’t want to wait months or years to find that merry band of outsiders who love the same odd little movie we do. Will the Pandemic Spawn a New Genre of Cult Classics? Barb and Star Suggests Yes |Stephanie Zacharek |March 6, 2021 |Time 

Early in the pandemic, a lot of people said things like, let’s just require the people who are the most vulnerable to stay at home, and the rest of us should be able to go about our merry way. “Vaccine passports could further erode trust” |Lindsay Muscato |December 22, 2020 |MIT Technology Review 

We may admire colorful light displays from the safety of our cars, or watch the Rat King leap across our television screens, but there is no shortage of opportunities to feel merry and bright. Today in D.C.: Headlines to start your Thursday in D.C., Maryland and Virginia |Dana Hedgpeth, Teddy Amenabar |December 10, 2020 |Washington Post 

After a while I saw that there was just a merry-go-round of abused women coming in and coming out, none of whom were healing. Aileen Wuornos Killed. I Knew Her. And I Know Why |Eugene Robinson |November 10, 2020 |Ozy 

Bohac said the bill does not require anyone to say “Merry Christmas” if they are not up for it. A Field General in the War on Christmas |David Freedlander |December 24, 2014 |DAILY BEAST 

Because we all grew up initially thinking it was “God Rest Ye, Merry Gentlemen.” Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

But asked if he would say “Merry Christmas” to someone who he knew did not celebrate the holiday, he paused for several seconds. A Field General in the War on Christmas |David Freedlander |December 24, 2014 |DAILY BEAST 

Deck your halls instead with boughs of holly, shouting “Merry Christmas” (or “Happy Hanukkah”) well into the night. A Field General in the War on Christmas |David Freedlander |December 24, 2014 |DAILY BEAST 

In his last tweet, he wished his followers a “Merry Christmas.” Rand Paul Has a Few Festivus Grievances |Ben Jacobs |December 23, 2014 |DAILY BEAST 

We were in the midst of it, and having a merry time, when the door suddenly opened and Liszt appeared. Music-Study in Germany |Amy Fay 

There was a host of friends and acquaintances around the little home, making merry and admiring the baby. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

The comical little pig and the merry monkey hid under the bush and ate acorns as they watched the circus procession go past. Squinty the Comical Pig |Richard Barnum 

The vintage hath mourned, the vine hath languished away, all the merry have sighed. The Bible, Douay-Rheims Version |Various 

How gracefully she led off the merry dance whilst clogs were on her spirits, weighing upon every movement. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various