When the researchers experimentally disabled a host’s FT gene, dodders no longer flowered. This parasitic plant eavesdrops on its host to know when to flower |Jonathan Lambert |September 4, 2020 |Science News 

Power down your laptop, Socrates urges, disable Slack, and talk. The business advice Socrates would give if he wrote a management book today |jakemeth |August 25, 2020 |Fortune 

For starters, disabling NANOS2 does not definitively prevent the surrogate bull from producing some of its own sperm. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

The bacteria then use Cas9 to cut the DNA apart, which disables the virus. A Renaissance of Genomics and Drugs Is Extending Human Longevity |Peter H. Diamandis, MD |June 26, 2020 |Singularity Hub 

Even though disabling the Google search box might sound like a bizarre idea, I recommend considering this in particular cases. Eight great tips for internal site search optimization |Marco Bonomo |June 18, 2020 |Search Engine Watch 

This would seem to refute the new report that the captain alone would have been able to disable all the systems. Who Gagged the Search for MH370? |Clive Irving |June 22, 2014 |DAILY BEAST 

Her first move was to disable the “retweet cartels,” a setup that automatically retweets from preprogrammed users or hashtags. The Champagne Tranarchist Who Hijacked Occupy’s Twitter Feed |Nina Strochlic |February 15, 2014 |DAILY BEAST 

In Yemen, Somalia, Iraq, and Mali, we have to keep working with partners to disrupt and disable these networks. Obama’s War on Terror Muddle |Eli Lake |January 29, 2014 |DAILY BEAST 

Once you enslave a computer to do what you want, you disable it for real love. Can Robots Fall in Love, and Why Would They? |Jimmy So |December 31, 2013 |DAILY BEAST 

It's true that if Republicans continue to disable themselves, Democrats have more freedom to indulge their progressive instincts. The Dems’ Fork in the Road |David Frum |November 12, 2013 |DAILY BEAST 

No doubt the legion had suffered a defeat; but not such as to disable their continuance of the contest. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

The main thing is to disable one's antagonist as quickly as possible, and Festing knew that Wilkinson would not be scrupulous. The Girl From Keller's |Harold Bindloss 

But how do you disable a smooth-surfaced turtle-backed machine? The Status Civilization |Robert Sheckley 

To disable the pilot of the opposing aeroplane will be the first object. Aviation in Peace and War |Sir Frederick Hugh Sykes 

This extra fatigue may possibly disable his horse, so that the animal cannot proceed further. Travels in Peru, on the Coast, in the Sierra, Across the Cordilleras and the Andes, into the Primeval Forests |J. J. von Tschudi