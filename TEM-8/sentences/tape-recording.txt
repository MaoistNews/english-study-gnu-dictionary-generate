What is evident are the many parallels between Kafuku’s circumstances and those found in Uncle Vanya, which he’s preparing to star in by running lines in his car opposite a tape recording of Oto playing the role of his scene partner. ‘Drive My Car,’ a Japanese Study of Grief, Is the Most Entrancing Film of the Year |Nick Schager |November 16, 2021 |The Daily Beast 

My agent at the time sent that tape to SNL and then they asked me to come in for an audition. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

I stood with a tape recorder, listening to men denounce the liberal media controlled by Jews. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

Is there any better Beyoncé lyric to use in response to the most shocking celebrity tape this side of One Night in Paris? Yoncé Said Knock You Out: The Solange and Jay Z Story |Kevin O’Keeffe |December 29, 2014 |DAILY BEAST 

This is an extraordinary recording that deserves to be much better known. The Best Albums of 2014 |Ted Gioia |December 13, 2014 |DAILY BEAST 

I watched Garner die on tape and wondered why I was crying so hard when I am not that much of a cryer at all. The Day I Used Eric Garner’s Voice |Joshua DuBois |December 5, 2014 |DAILY BEAST 

He gets out and does things while these fatheads stay in quarters and untangle red tape. Raw Gold |Bertrand W. Sinclair 

A handkerchief, once red, with polka spots, contained a ragged flannel shirt and a stocking-heel tied with a piece of tape. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward 

As it had columns for recording statistics of the fair for a period of years, it was instructive as well as ornamental. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Black Hood knew what it was to be a policeman with hands bound by red tape or political intrigue. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The following are either essential, or useful in various degrees, for obtaining and recording observations. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King