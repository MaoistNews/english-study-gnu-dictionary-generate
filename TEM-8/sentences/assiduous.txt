Yang also ran up the score in conservative Jewish precincts of Brooklyn, after an assiduous effort to win Orthodox votes. The Trailer: "The new Democratic Party": What we learned (and what we don't know yet) from New York's primary |David Weigel |June 24, 2021 |Washington Post 

Any life, despite the most assiduous biographer’s investigations, remains somewhat of a mystery. Two new books celebrate Old Hollywood glory |Kathi Wolfe |June 12, 2021 |Washington Blade 

There has been assiduous woo-ing of the political establishment too. Camilla Will Never Be Queen: Prince Charles Capitulates On Website FAQ |Tom Sykes |November 12, 2012 |DAILY BEAST 

Webb was never wild about some of the nitty-gritty aspects of elected office and was not a very assiduous fundraiser. Olympia Snowe, Jim Webb, Ben Nelson, More Moderate Retiring Senators |Ben Jacobs |February 29, 2012 |DAILY BEAST 

As an intern at The Nation in 1989, he was an amiable and assiduous fact-checker of my copy. Britain's New Political Dynasty |Norman Birnbaum |October 1, 2010 |DAILY BEAST 

But despite her allure, her assiduous assault on New York produced no results. How Desiree Landed Her Gig |Sandra McElwaine |August 11, 2010 |DAILY BEAST 

His assiduous reporting and his smart, passionate commentary have impressed colleagues and readers alike. Introducing Beast Books |Tina Brown |January 22, 2010 |DAILY BEAST 

She soon gave birth to a daughter, her only child, whom she nurtured with the most assiduous care. Madame Roland, Makers of History |John S. C. Abbott 

He was not seen again, at least in the flesh, but he became one of the most assiduous frequenters of De Quincey's visions. Charles Baudelaire, His Life |Thophile Gautier 

Nobody is a greater slave than an assiduous courtier, unless it be a courtier who is more assiduous. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

Only by an assiduous devotion to the contents of the daily newspapers in their reports of the doings of the socially elect. Mrs. Raffles |John Kendrick Bangs 

She had been an assiduous student of the eighteenth century philosophers, and on the whole was a lady of considerable culture. Frederick Chopin as a Man and Musician |Frederick Niecks