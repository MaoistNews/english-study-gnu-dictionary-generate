Yes, he did manage to veto a tripling of saloon license fees. Politics Report: The 101 Ash St. Scandal Isn’t Going Away |Andrew Keatts |July 3, 2021 |Voice of San Diego 

Sold primarily at saloons and all-night lunch carts that catered to factory shift workers, the dish, at least in the imagination of polite society, seemed to have “something of the night” about it. Who invented the hamburger? Biting into the messy history of America’s iconic sandwich. |Erik Ofgang |May 28, 2021 |Washington Post 

After the story ran, a reader named Thomas Pieragostini emailed me a link to a series of ads that appeared in the Shiner Gazette in Texas in the spring of 1894 that advertised “hamburger steak sandwiches” being served at a local saloon. Who invented the hamburger? Biting into the messy history of America’s iconic sandwich. |Erik Ofgang |May 28, 2021 |Washington Post 

She loved wild women, hosted saloons for 60 years and founded a women’s academy. Best of OZY: Badass Women of History |Sean Culligan |May 28, 2021 |Ozy 

The adobe-walled Starlight Theatre restaurant and saloon in the nearby ghost town of Terlingua fits the bill, too. A Cross-Country Guide to the Best Adventure Bars |Graham Averill |April 14, 2021 |Outside Online 

The Horse You Came in On Saloon, Baltimore Horse-themed bars must be bad luck for famous authors. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

Along this sky-high route, nearly every 19th-century saloon or historic hotel has a ghost story to tell. The U.S. Road Trips You Should Really Take |Lonely Planet |April 26, 2014 |DAILY BEAST 

The former Secretary of Culture, David Mellor, used to say that they were drinking in “the last chance saloon.” David Cameron’s Credibility Problem |Alastair Campbell |July 8, 2011 |DAILY BEAST 

Look, this is a late-night saloon and occasionally it's necessary to assert myself when things get a little out of hand. Hostess to the Stars |A. E. Hotchner |December 3, 2010 |DAILY BEAST 

When that happened, it was like Frank Sinatra telling a saloon singer he was good. Dominick Dunne: The Ultimate Reporters' Reporter |Allan Dodds Frank |August 28, 2009 |DAILY BEAST 

There was a moment's pause, and Doa Inez returned into the saloon, which was now beginning rapidly to fill. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

The cheerful sound of music came from the deck of a big saloon steamer, bearing its crowd of noisy tourists. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

In ordinary times fares ranged from ₱50 saloon accommodation to ₱8 a deck passage. The Philippine Islands |John Foreman 

He apologized for interrupting their tête-à-tête, but said he had no choice, as the saloon was completely full. Bella Donna |Robert Hichens 

Longcluse had made up his mind promptly on the night of the billiard-match played in the Saloon Tavern. Checkmate |Joseph Sheridan Le Fanu