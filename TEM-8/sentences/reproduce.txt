There are 74 individuals left, mostly males and non-reproducing females. Is It Too Late for the Southern Resident Orcas? |Catherine DeNardo |February 3, 2021 |Outside Online 

A test may not be able to detect the virus early in its courseEarly in an infection, the virus may not have reproduced enough to be detectable. Three reasons a negative coronavirus test doesn’t necessarily mean you’re not infected |Marisa Iati |January 14, 2021 |Washington Post 

The vast majority of animal species reproduce sexually, but females of some species are able to produce eggs containing all the genetic material required for reproduction. Parthenogenesis: How females from some species can reproduce without males |Ars Contributors |December 26, 2020 |Ars Technica 

They are strengthening the viability of that child to go on and reproduce by nurturing it. Outwitting the Grim Reaper - Issue 94: Evolving |Kevin Berger |December 23, 2020 |Nautilus 

We wanted to share our code so others can reproduce and explore it on their own. Brand reputation and the impact of Google SERP selections |JR Oakes |December 23, 2020 |Search Engine Land 

Airline pilots are now slowly, too slowly, being given access to flight simulators able to reproduce sudden and unexpected upsets. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 

Asked if the ability to reproduce should be a human right, Spar said she would leave that for the philosophers to think about. Want Blue Eyes With That Baby?: The Strange New World of Human Reproduction |Eleanor Clift |November 24, 2014 |DAILY BEAST 

You may have noticed that roughly 100 percent of higher animals reproduce sexually, requiring a male and female partner to do so. Is Pope Francis Backpedaling on Gays? |Jay Michaelson |November 19, 2014 |DAILY BEAST 

Most bands these days aspire to reproduce their recordings on stage as faithfully as possible. Is Jack White the Last True Rock Star? |Andrew Romano |June 13, 2014 |DAILY BEAST 

The program “evolves” solutions that computer scientists cannot readily reproduce. This is What Happens When You Teach Machines the Power of Natural Selection |James Barrat |February 1, 2014 |DAILY BEAST 

To reproduce the impulse born of the thought—this is the aim of a psychological method. Expressive Voice Culture |Jessie Eldridge Southwick 

And so this is why the clever performer cannot reproduce the effect of a speech of Demosthenes or Daniel Webster. Expressive Voice Culture |Jessie Eldridge Southwick 

This is quite apart from their failure to reproduce the master touch in other branches of the liutaro's art. Antonio Stradivari |Horace William Petherick 

And the conversations I can reproduce almost verbatim, for, according to my invariable habit, I kept full notes of all he said. Three More John Silence Stories |Algernon Blackwood 

A mono-cell, the amoeba, was able to reproduce itself by the simple stratagem of sub-division. Hooded Detective, Volume III No. 2, January, 1942 |Various