That phrase later became shorthand for nasty statements about refugees and other migrants. How to fight online hate before it leads to violence |Kathiann Kowalski |February 4, 2021 |Science News For Students 

So unless you’re in a specific group, like migrant workers or refugees or Palestinians in the occupied territories, you’re insured, and you’re part of the system. “Everyone is impressed by Israeli vaccination, but I don’t think we’re a success story” |Lindsay Muscato |January 22, 2021 |MIT Technology Review 

This comes just before the Lunar New Year festivities, when migrant workers all over China expect to head home to celebrate the holiday with their families. China Marks the Wuhan Lockdown Anniversary Amid Spiraling COVID-19 Cases and With Risky Holiday Travel Looming |Charlie Campbell / Changsha and Wuhan |January 22, 2021 |Time 

As we saw with the migrant workers, the year 2020 impacted India’s most vulnerable severely. The Covid-19 pandemic hasn’t been a “great equaliser” for India’s queer population |Neel Sengupta |January 22, 2021 |Quartz 

It has since grown from an international focus to include assistance for migrants along the southern border of the United States. How your unneeded travel rewards can make a difference |Hugh Biggar |January 14, 2021 |Washington Post 

Dehydrated and feeling weary, Marino lay down beside another migrant under a tree and fell asleep. Drug Smuggler Sues U.S. Over Dog Bite |Caitlin Dickson |December 10, 2014 |DAILY BEAST 

With the help of a Jamaican migrant worker, he returns to his homeland to find his father. Life After ‘Winter’s Bone’: Debra Granik on Finding J. Law and the Plight of the Female Director |Marlow Stern |October 24, 2014 |DAILY BEAST 

The actor transformed from a sensual Marilyn Monroe to a whimsical Salvador Dali to a starving migrant mother, seamlessly. The Man Behind Marilyn Malkovich |Justin Jones |October 2, 2014 |DAILY BEAST 

While migrant ship tragedies at sea happen all too often, the latest sinking appears to have been no accident. Hundreds of Migrants are Reported Drowned by Traffickers Near Malta |Barbie Latza Nadeau |September 15, 2014 |DAILY BEAST 

“GNRD has zealously fought to eliminate abuse of Nepalese migrant workers in Qatar,” the organization said in a statement. World Cup Watchdogs Go Missing in Qatar |Nina Strochlic |September 5, 2014 |DAILY BEAST 

Common migrant; breeds in the North; a few may winter in the State. Birds of the Rockies |Leander Sylvester Keyser 

Migrant; rare, if not accidental; only one specimen, and that a female. Birds of the Rockies |Leander Sylvester Keyser 

Migrant; occasionally winter resident; not known to breed in State. Birds of the Rockies |Leander Sylvester Keyser 

Rare migrant; range extends only to foothills; no record of its breeding. Birds of the Rockies |Leander Sylvester Keyser 

Common migrant; occurs from the plains to the great height of 13,000 feet. Birds of the Rockies |Leander Sylvester Keyser