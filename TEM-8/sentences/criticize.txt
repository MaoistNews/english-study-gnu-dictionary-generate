The committee criticized Boeing and the FAA for not mentioning the system’s name. Boeing crashes were the “horrific culmination” of multiple mistakes, House report says |kdunn6 |September 16, 2020 |Fortune 

The president frequently claims he took bold action that was criticized. Trump’s ABC News town hall: Four Pinocchios, over and over again |Glenn Kessler |September 16, 2020 |Washington Post 

Additionally, a recent National Institutes of Health panel criticized the FDA for its EUA of convalescent plasma without having performed randomized tests. 6 questions that must be answered in the race for a vaccine |jakemeth |September 15, 2020 |Fortune 

Even his defense, which used to be criticized relentlessly, has improved considerably. Everything Should Be On The Table For The Houston Rockets. Even James Harden’s Future. |Chris Herring (chris.herring@fivethirtyeight.com) |September 14, 2020 |FiveThirtyEight 

A draft city report also criticizes the department’s data collection practices. Morning Report: Oceanside Reboots Top Cop Search |Voice of San Diego |September 14, 2020 |Voice of San Diego 

Better, she says, to use local Muslim leaders and others in civil society to criticize militant arguments. ISIS Has a Message. Do We? |Jamie Dettmer |December 8, 2014 |DAILY BEAST 

Ever notice that "classy" adults never criticize teenagers for not being "classy"? GOP Flack Throws Shade at First Teens |Olivia Nuzzi |November 29, 2014 |DAILY BEAST 

Uber recently threatened to use the personal data of a tech journalist to destroy her because she dared criticize them. The Ten Worst Uber Horror Stories |Olivia Nuzzi |November 19, 2014 |DAILY BEAST 

She refused to criticize the group by name or clarify whether she believed that ethnic Koreans had special privileges. For Top Pols In Japan Crime Doesn’t Pay, But Hate Crime Does |Jake Adelstein, Angela Erika Kubo |September 26, 2014 |DAILY BEAST 

It increases the number of people who think, criticize, discuss. Mexico City: Francisco Goldman’s Other Lost Love |Jason Berry |September 25, 2014 |DAILY BEAST 

They began to criticize all sorts of things which they had believed in and reverenced before. Stories That Words Tell Us |Elizabeth O'Neill 

"You've got no call to criticize," he went on, as Tony did not answer. Colonial Born |G. Firth Scott 

If she sees you want to learn and not to criticize she will become the most delighted, flattering teacher you ever dreamed of. The Library of Work and Play: Housekeeping |Elizabeth Hale Gilman 

Their attitude toward the Public was one which you did not criticize, for it seemed to you to be reasonable. Humanly Speaking |Samuel McChord Crothers 

The burgess's daughter was refining to an appreciation of the exquisite so rapidly that she could criticize patricians. Lord Ormont and his Aminta, Complete |George Meredith