Dollar General has found it could cast its net wider by created a distinct brand from its namesake stores, even with items are are still inexpensive. Dollar General takes aim at more affluent shoppers with a new chain called ‘popshelf’ |Phil Wahba |October 8, 2020 |Fortune 

The high-end department store, whose own house brands have not included fur for a number of years, said the ban would extend to its namesake stores, its discount Rack chain, and its website. Nordstrom to stop selling fur and exotic skin products |Phil Wahba |September 29, 2020 |Fortune 

A lucky Massachusetts town in the 18th century decided to name itself after Benjamin Franklin and then ask its namesake for a gift. One of the great enemies of the public library is departing |Aaron Pressman |September 18, 2020 |Fortune 

On Anchor’s own app, the problem is even worse, with the namesake show rarely appearing at the top of results. ‘Game of whack-a-mole’: Spotify has a counterfeit podcast problem |Max Willens |August 21, 2020 |Digiday 

DNA origami, like its namesake, is now living out one of its endless possibilities. How Fake Viruses Can Help Us Make the Best Possible Vaccines |Shelly Fan |July 7, 2020 |Singularity Hub 

I remind Deen that his namesake died in an infamously horrible car crash, so he may want to cool it on texting and driving. My Bizarre Night With James Deen, Libertarian Porn Star |Emily Shire |November 12, 2014 |DAILY BEAST 

This was even before the nonprofit group hired a Boston law firm to investigate its namesake as Newsweek was preparing its story. Who’s Telling The Truth About Somaly Mam? A Smashed Icon, A Media Brawl—and a Comeback |Lloyd Grove |September 19, 2014 |DAILY BEAST 

The cap stayed only because it referenced Civil War-era California state lawmaker Abner Weed, the namesake of the town. Meet the Beer Bottle Dictator |Tim Mak |August 12, 2014 |DAILY BEAST 

An absentee father is loitering about with his son and namesake, Mason Jr., at a deserted concert hall. The Making of ‘Boyhood’: Richard Linklater’s 12-Year Journey to Create An American Masterpiece |Marlow Stern |July 10, 2014 |DAILY BEAST 

The sand on its namesake beach turns gray as the sun dips behind the Two Brothers, twin granite spires at the far side of the bay. Rio’s Real-Life Slumdog Millionaires |Brandon Presser |June 18, 2014 |DAILY BEAST 

A silver cup is the usual present, with your little namesake's initials, or full name, engraved upon it. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Your letter to Foster is not forgot: nor was the visit to his namesake of Orchard Street. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon 

He left his Idrone property to his nephew and namesake, who was also continued in the government of Leighlin. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell 

Of course, it was not admirable conduct, but Peter could understand and sympathize with the motives of his namesake. The Boy Grew Older |Heywood Broun 

Little Grace never saw her grandmama, nor her lovely aunt that was her namesake, nor consciously her mama. Blackwood's Edinburgh Magazine, Vol 58, No. 357, July 1845 |Various