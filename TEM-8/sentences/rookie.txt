The rookie has allowed three or more goals in seven of his nine starts. In the home of ‘The King,’ the Capitals get served with a second straight loss |Samantha Pell |February 5, 2021 |Washington Post 

It’s hard especially when you’re a rookie in your first year and you want to work out, want to improve and it can’t happen. Rui Hachimura and Deni Avdija are regaining their footing after missing so much time |Ava Wallace |February 4, 2021 |Washington Post 

Patience is in order all around given the many challenges facing rookies during this unusual season, including their shortened post-draft acclimation. The front-runners for NBA awards aren’t the usual suspects |Ben Golliver |February 1, 2021 |Washington Post 

To narrow the pool further, only seven first-round quarterbacks in that span have won a playoff game and none of them did it as a rookie. With Matthew Stafford out of the running, Washington’s quarterback options are limited |Nicki Jhabvala, Sam Fortier |February 1, 2021 |Washington Post 

Haskins started seven games as a rookie and showed improvement late in the year. Former Washington QB Dwayne Haskins signs one-year contract with Steelers |Nicki Jhabvala |January 21, 2021 |Washington Post 

The Lion Air captain had left his rookie copilot to make the landing until he realized he was in trouble. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

They castigated the captain, a 48-year-old Indonesian, and his rookie copilot, a 24-year-old Indian. Who Will Get AsiaAir 8501’s Black Boxes? |Clive Irving |December 30, 2014 |DAILY BEAST 

One of the rookie officers, Peter Liang, was walking with a flashlight in one hand and his gun in the other. New York's Next Killer-Cop Grand Jury |Jacob Siegel |December 6, 2014 |DAILY BEAST 

A 28-year-old gunned down in a dark, New York City hallway by a rookie cop who apparently made a fatal mistake. Raging Protesters Set Ferguson on Fire |Justin Glawe |November 25, 2014 |DAILY BEAST 

Still, he was locked into his rookie contract, and had to ride out two more dysfunctional seasons of the show. Ben McKenzie’s Journey From Reluctant Teen Idol on ‘The O.C.’ to Sheriff of ‘Gotham’ |Marlow Stern |November 4, 2014 |DAILY BEAST 

What's all that nonsense Ferrers gives us about the old days when he was such a rookie from civil life? Uncle Sam's Boys as Lieutenants |H. Irving Hancock 

A Reservist is a dug-out, a recruit a rookie, and a veteran an old sweat. Anecdotes of the Great War |Carleton Britton Case 

No, admitted the rookie reluctantly, yo nevah tol me nuthin. Funny Stories Told By The Soldiers |Carleton B. Case 

Some of the happier memories of my rookie days die with him. Biltmore Oswald |J. Thorne Smith, Jr. 

McRae had taken him when he was a raw rookie and given him his chance with the Giants to show what he could do. Baseball Joe, Captain of the Team |Lester Chadwick