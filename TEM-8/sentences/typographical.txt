Excerpted from Shady Characters: The Secret Life of Punctuation, Symbols, and Other Typographical Marks by Keith Houston. The Rise and Fall of the Infamous SarcMark |Keith Houston |September 24, 2013 |DAILY BEAST 

Only one typographical error was noted in the conversion of the printed document to digital format. A Synopsis of the American Bats of the Genus Pipistrellus |Walter W. Dalquest 

Simple typographical errors were corrected, in some cases by referring to other editions of this book. Urania |Camille Flammarion 

Changes to the text (correction of typographical errors) are listed at the end of the book. Domestic Animals |Richard L. Allen 

Transcriber's Notes: No corrections of typographical or other errors have been made to this text. The Raid of John Brown at Harper's Ferry as I Saw It |Rev. Samuel Vanderlip Leech 

Obvious typographical errors in spelling and punctuation have been corrected without comment. Dwarf Fruit Trees |F. A. Waugh