Kim is not thought to be broke just yet, but he will be soon, as movie studios seek to recoup $100m in copyright fees. The Zillionaires Who Lost Everything |Tom Sykes |October 26, 2014 |DAILY BEAST 

They might front the money if they believe in a new model and think she will be able to recoup the costs. Many Models Get Paid in Clothes, Not Cash, but That Might Be Changing |Misty White Sidell |March 11, 2013 |DAILY BEAST 

“In the most extreme cases, the abusive party is able to recoup irrespective of the cost,” he says. Domestic Violence Among the Wealthy Hides Behind ‘Veil of Silence’ |Eliza Shapiro |February 28, 2013 |DAILY BEAST 

According to the Washington Post, the government was looking to recoup $385 million of Katrina, Rita, and Wilma aid money. From Katrina To Sandy, FEMA Rumors and Failures Keep Swirling |Michael Moynihan |November 9, 2012 |DAILY BEAST 

Surely then we can safely raise their taxes, and count on them to work harder to recoup their losses? Answering a Murray Defender |David Frum |February 8, 2012 |DAILY BEAST 

How I was going to recoup myself for the double cost afterwards I didn't know. In Accordance with the Evidence |Oliver Onions 

But you dont mention your game of cards with Chevalier Desfleurets; did you recoup your losses? Brother Jacques (Novels of Paul de Kock, Volume XVII) |Charles Paul de Kock 

Here was a chance to recoup himself, in some small part, for the loss of his cabin and supplies. The Backwoodsmen |Charles G. D. Roberts 

With that amount, Otto, you know I can recoup all these terrible losses, and in less than a year all will be repaid. The Empty House And Other Ghost Stories |Algernon Blackwood 

These losses, though deplorable, were not vital while the Russian armies still retained power to retaliate and recoup. The New Gresham Encyclopedia |Various