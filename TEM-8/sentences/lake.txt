It’s the only moon in the solar system with a dense nitrogen-rich atmosphere like Earth’s, and the only place besides Earth where there’s been clear evidence of lakes on the surface. The 5 best places to explore in the solar system—besides Mars |Neel Patel |August 17, 2020 |MIT Technology Review 

The Erta Ale stands out as one of the world’s most active volcanoes and one of the only eight and possibly the longest-existing lava lakes in the world. Scientists say a new ocean will form in Africa as the continent continues to split into two |Uwagbale Edward-Ekpu |August 13, 2020 |Quartz 

Some climate models have come to the same conclusion, he says, counter to the prevailing image of ancient Mars as a planet covered in oceans and lakes. Mars may not have been the warm, wet planet we thought it was |Neel Patel |August 7, 2020 |MIT Technology Review 

The lake’s water has been receding for thousands of years, so there are spots near the ancient shoreline where the present-day lake is invisible. To rehearse Perseverance’s mission, scientists pretended to be a Mars rover |Lisa Grossman |July 29, 2020 |Science News 

The landing site in Jezero crater, just north of the Martian equator, contains an ancient river delta that looks like it once carried water and silt into a long-lived lake. NASA’s Perseverance rover will seek signs of past life on Mars |Lisa Grossman |July 28, 2020 |Science News 

The “waters of Lake Minnetonka” may have been purifying, but they were also freezing. Speed Read: The Juiciest Bits From the History of ‘Purple Rain’ |Jennie Yabroff |January 1, 2015 |DAILY BEAST 

I am reminded of the story of Senator Bernie Sanders (Independent, VT) walking along the shores of Lake Champlain. Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

People are extremely anxious about the next generation,” Lake said, “and it unites America. How Will Cuba Play In Peoria? |Eleanor Clift |December 21, 2014 |DAILY BEAST 

Williams said he went to the lake to take a stroll “because of his heart.” Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 

This video remedies that injustice, showcasing an owl doing a butterfly stroke in Lake Michigan. Swimming Owls, Jane Krakowski’s Peter Pan Live! Audition, and More Viral Videos |The Daily Beast Video |December 7, 2014 |DAILY BEAST 

Ages back—let musty geologists tell us how long ago—'twas a lake, larger than the Lake of Geneva. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

They also seized the lake gunboats, took an entire Spanish garrison prisoner, and captured a large quantity of stores. The Philippine Islands |John Foreman 

A small but beautiful river debouches from the lake at its west end, and the town is grouped around this outlet. Glances at Europe |Horace Greeley 

They gardened, they drove out, they rowed and sailed upon the lake, but they declined all acquaintances. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

A similar circumstance has occurred on the seashore at Hoy Lake, Cheshire, where several "fairy pipes" have been found. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.