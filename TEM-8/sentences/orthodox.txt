I think the minute you say something even slightly off accepted orthodox, they want you destroyed. Andrew Sullivan doesn’t care what you think |Chris Johnson |September 22, 2021 |Washington Blade 

I write with Sacha and write ordinary scripted narrative comedy films as well, and it’s insane the amount of effort you put into writing a scene for an “orthodox” movie. Sacha Baron Cohen’s Partner in Crime Seizes the Spotlight |Marlow Stern |July 30, 2021 |The Daily Beast 

According to orthodox quantum theory, the cat is neither dead nor alive until we open the box and observe the system. Is Reality a Game of Quantum Mirrors? A New Theory Suggests It Might Be |Peter Evans |June 30, 2021 |Singularity Hub 

This new form of populist, semi-authoritarian right is the political camp that has backed Netanyahu, along with ultra-orthodox parties. Even if the Ceasefire Holds, the Far-Right Will Dominate Israel's Future |Dahlia Scheindlin |May 21, 2021 |Time 

The gentleman was listed as Orthodox and kosher, which is way too religious for my friend whose JSwipe account I was test-driving. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

Ben is not Orthodox or particularly committed to adhering to traditional Jewish laws. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

I mean my background weighed heavily, because I was brought up in this orthodox way. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

Certainly, other communities—ultra-Orthodox Jews, for example—are fretting about members who go online, and then astray. God vs. the Internet. And the Winner is… |Michael Schulson |November 16, 2014 |DAILY BEAST 

Most Allentown residents of Syrian heritage are Orthodox Christians from the Wadi-al-Nasara region in western Homs province. Welcome to Assadville, USA |Christopher Moraff |November 11, 2014 |DAILY BEAST 

A burning controversy between the Averroists and the orthodox schoolmen. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

Lady Maude sat alone in her room; the white robes upon her, the orthodox veil, meant to shade her fair face thrown back from it. Elster's Folly |Mrs. Henry Wood 

Each essays to think, appear and speak as nearly according to the orthodox standard of Womanhood as possible. Glances at Europe |Horace Greeley 

Or were they merely orthodox through a more uneven balancing of their qualities, the animal in abeyance? Ancestors |Gertrude Atherton 

I rather fear, major, that your method of comforting me is not what the missionary would call orthodox. Hunting the Lions |R.M. Ballantyne