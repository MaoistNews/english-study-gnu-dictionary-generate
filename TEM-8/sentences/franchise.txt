If the only impression you have of Deadpool is Ryan Reynolds mouthing off in the movie franchise, do yourself a favor and pick up this 2012 series by Croatian comic genius Dalibor Talajic. This Weekend: You’ll Never Bathe the Same Way Again |Joshua Eferighe |August 21, 2020 |Ozy 

City spokespeople also couldn’t tell reporter MacKenzie Elmer what’s next for the franchise fee agreement immediately after the special Council meeting. Morning Report: City Walks a Fine Line With Franchise Fee Deal |Voice of San Diego |August 7, 2020 |Voice of San Diego 

The Council won’t get to see the final franchise agreement before it goes out to market anyway, Council President Georgette Gómez said during the meeting. The City Is Walking a Fine Line in Demanding Millions From Its Next Power Provider |MacKenzie Elmer |August 7, 2020 |Voice of San Diego 

The franchise agreement is considered San Diego’s most powerful leverage point against investor-owned utilities, and there’s really no standard for what the city can or can’t request. What Power San Diego Has Over Its Power Company |MacKenzie Elmer |August 4, 2020 |Voice of San Diego 

“We’re definitely focused on creating franchises,” co-CEO Reed Hastings said on a call with investors last week. Can Netflix create the next James Bond? |Adam Epstein |July 20, 2020 |Quartz 

Then, under the bold headline “Rebooting Spider-Man,” Robinov describes a broad vision for the future of the franchise. Exclusive: Sony Hack Reveals Studio's Detailed Plans For Another ‘Spider-Man’ Reboot |William Boot |December 13, 2014 |DAILY BEAST 

Another angle Robinov suggests as a possibility for Peter Parker/Spider-Man is a franchise reboot tackling Spidey as… an adult. Exclusive: Sony Hack Reveals Studio's Detailed Plans For Another ‘Spider-Man’ Reboot |William Boot |December 13, 2014 |DAILY BEAST 

The NFL cares about only one thing: protecting the 32 franchise owners. Roger Goodell and the NFL’s Path to Power |Robert Silverman |December 11, 2014 |DAILY BEAST 

More than the books, and they are greatly so, this is an action franchise. Team Peeta or Team Gale: Why the ‘Hunger Games’ Love Triangle Ruins ‘Mockingjay – Part 1’ |Kevin Fallon |November 28, 2014 |DAILY BEAST 

The Hunger Games franchise is already a deeply political saga, chronicling a growing rebellion against a tyrannical regime. ‘The Hunger Games’ Stars Silent on Thai Protesters |Asawin Suebsaeng |November 21, 2014 |DAILY BEAST 

I am prepared to respect the franchise, to give substantially, although not nominally, equality. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Likewise, he owned the stage line and franchise, controlling the only right of way by which a railroad could reach up the valley. Scattergood Baines |Clarence Budington Kelland 

Mr. O'Connell moved that it should be an instruction to the committee to restore the franchise to these freeholders. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

In regard to the qualification of electors, he said it had been determined not to adhere to the parliamentary franchise. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

On the 4th of July, however, the house went into committee upon clause twenty, which referred to the value of the franchise. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan