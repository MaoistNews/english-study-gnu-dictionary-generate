Cassini’s instruments also detected the presence of silica, which can get mixed with water in undersea volcanoes. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

It also includes active volcanoes such as the Ol Doinyo Lengai in Tanzania, and the DallaFilla and Erta Ale in Ethiopia. Scientists say a new ocean will form in Africa as the continent continues to split into two |Uwagbale Edward-Ekpu |August 13, 2020 |Quartz 

The Natural History Museum in London, England offers a guide to making your own model volcano at home. Let’s learn about volcanoes |Bethany Brookshire |August 5, 2020 |Science News For Students 

In one of the most poignant scenes in her book, she is hiking on Mauna Kea — the next volcano over from Greene’s Mars habitat — and finds a fern growing amid the volcanic desolation. Two new books explore Mars — and what it means to be human |Lisa Grossman |July 15, 2020 |Science News 

He particularly likes this approach that linked cycles of pressure inside the volcano with weather conditions. Did rain put the Kilauea volcano’s lava-making into overdrive? |Megan Sever |May 15, 2020 |Science News For Students 

And as we left, spears of sunlight painted the top of the nearby volcano Galeras. Spirit Tripping With Colombian Shamans |Chris Allbritton |August 24, 2014 |DAILY BEAST 

Standing on the edge of the Burfell volcano, you realize what a fragile construct modern civilization is. Want to Write a Book? Go to Iceland |Adam LeBor |May 26, 2014 |DAILY BEAST 

It ends with Godzilla lured away from Tokyo with a bird call and trapped in a volcano. A Comprehensive History of Toho’s Original Kaiju (and Atomic Allegory) Godzilla |Rich Goldstein |May 18, 2014 |DAILY BEAST 

When the volcano blew its top, thousands perished, immolated by fire, boiling magma, and ash. The Volcano That Rewrote History |William O’Connor |May 5, 2014 |DAILY BEAST 

This week they got Mike Tyson and Razor Ruddock over at the Mirage, where the fake volcano blows up every twenty minutes. The Stacks: Harold Conrad Was Many Things, But He Was Never, Ever Dull |Mark Jacobson |March 8, 2014 |DAILY BEAST 

A volcano broke out in the island of St. George, one of the Azores. The Every Day Book of History and Chronology |Joel Munsell 

Fujiyama, the noted volcano of Japan, is twelve thousand three hundred and sixty-five feet high. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

And what was a mere laughing, crying child of a man like Aristide Pujol in front of a Ducksmith volcano? The Joyous Adventures of Aristide Pujol |William J. Locke 

Torrents of lava poured over the sides of the volcano and destroyed whole villages on the shores of the lake. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

Outwardly cold, Sir Henry seemed to his youthful observer, who now knew him better, to resemble a volcano coated with ice. The Red Year |Louis Tracy