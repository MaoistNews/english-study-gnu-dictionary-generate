My favorite kitchen appliance, which I use twice a day sometimes, is my rice cooker. 20 Days of Turkey |Meghan McCarron |December 23, 2020 |Eater 

After the cooker reaches high pressure, which takes 5 to 10 minutes, cook for 35 minutes. Instant Pot delivers the silkiest bread pudding we’ve ever tried |Jessie Sheehan |December 11, 2020 |Washington Post 

Slow cookers don’t normally come in sizes tailored exclusively for small amounts of food, but air fryers are available in a larger range of sizes, which makes some models suitable for cooking for one to two people. Best air fryer: Five things to consider |PopSci Commerce Team |December 4, 2020 |Popular-Science 

Walk into a Mi store and you’ll encounter rice cookers, electric scooters, weighing scales, and robot vacuums—an entire ecosystem of interconnected smart devices that orbit the Xiaomi mobile phone. How Chinese phonemaker Xiaomi conquered India—and outperformed Apple |eamonbarrett |December 3, 2020 |Fortune 

Solo Stove’s 22-inch-wide cooker draws air in through a ring of 70 holes around its base, so it travels below smoldering coals, heats up, rises, and circulates to cook your burgers and corn. The 100 greatest innovations of 2020 |Popular Science Staff |December 2, 2020 |Popular-Science 

Anova Precision Cooker is the perfect little sous-vide gadget for the budding gastronomist. The Daily Beast’s 2014 Holiday Gift Guide: For the Richard Hendriks in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

Nobody really gets out in this story, which only ups the pressure-cooker quality. Book Bag: Gritty Stories From the Real Montana |Carrie La Seur |October 2, 2014 |DAILY BEAST 

It is worth remembering that the Boston Marathon bombers armed their pressure cooker bombs with the insides of fireworks. Pennsylvania Student Proves You Could Buy Ingredients for a WMD on Amazon |Michael Daly |January 28, 2014 |DAILY BEAST 

Maybe a sales clerk who sold the pressure cooker left female DNA on what would become a Boston bomb. Katherine Russell Under Scrutiny After Female DNA Found on Boston Bomb |Michael Daly |April 30, 2013 |DAILY BEAST 

The pressure-cooker bombs are also used by insurgents in Afghanistan, Pakistan, and India. The Sheikh Who May Have Influenced Boston’s Tsarnaev Brothers |Eli Lake |April 19, 2013 |DAILY BEAST 

Paris blossomed out with what I thought was an American invention, the fireless cooker. Paris Vistas |Helen Davenport Gibbons 

Henceforth the cooking operations were simplified, for previously a sledging-cooker had been used. The Home of the Blizzard |Douglas Mawson 

This is most readily done by lowering too quickly the outside cover over the rest of the cooker. The Home of the Blizzard |Douglas Mawson 

In the "Nansen Cooker," which we used, a maximum result is secured from the heat of the primus. The Home of the Blizzard |Douglas Mawson 

The bottle had stood on the top of the cooker while the meal was being prepared, but the wine was still as solid as ever. The Home of the Blizzard |Douglas Mawson