Showings will include a special message from Seth Rogen that will play beforehand. The Inside Story of How Sony’s ‘The Interview’ Finally Made It to Theaters |Marlow Stern |December 23, 2014 |DAILY BEAST 

A call made to police beforehand described Rice as “a guy with a pistol” on a swing set, but said it was “probably fake.” The 14 Teens Killed by Cops Since Michael Brown |Nina Strochlic |November 25, 2014 |DAILY BEAST 

On the fashion shoot beforehand, he was puppyish energy and charm—no diva-ishness, just fast, funny, and co-operative. Daniel Radcliffe: I’m Richer Than One Direction |Tim Teeman |October 24, 2014 |DAILY BEAST 

The only other option is to use laser-guided bombs, but even then the target has to be correctly indentified beforehand. Air Force Pilots Say They're Flying Blind Against ISIS |Dave Majumdar |October 10, 2014 |DAILY BEAST 

Are you a writer who sits down and outlines things beforehand? A Trailblazer in YA Dystopian Fiction: An Interview With 'The Giver' Author Lois Lowry |Marianne Hayes |August 12, 2014 |DAILY BEAST 

They had read the placards, they wished to see what the placards had announced, and to make their choice beforehand. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

Once or twice she took a quiet dinner there alone, having instructed Celestine beforehand to prepare no dinner at home. The Awakening and Selected Short Stories |Kate Chopin 

When they are ready they hasten in a crowd to the warehouse, where they have entered into a contract beforehand. Skipper Worse |Alexander Lange Kielland 

On this occasion he was well beforehand with the work, and sent in the cantata to the committee by the 1st of April. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Unless the accounts are made up beforehand, parsons can't avail much at the twelfth hour. Elster's Folly |Mrs. Henry Wood