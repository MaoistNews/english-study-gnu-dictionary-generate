He stopped to pick up a wrapper near our resting spot, explaining that’s why his alias is Trash. How a Plus-Size Hiker Found Her Footing on the Trail |syadron |August 22, 2021 |Outside Online 

In that spirit, consider email aliases, a useful feature found in popular services like Gmail, Microsoft Outlook, and Apple Mail. Use email aliases to organize your inbox—and stay private |David Nield |August 22, 2021 |Popular-Science 

More than 130 workers also attached their email aliases to the post. Amazon pledges to investigate discrimination after internal petition wins backing of hundreds of employees |Jay Greene |July 23, 2021 |Washington Post 

They employed aliases and cover stories to relay money, intelligence and threats. Operation Fox Hunt: How China Exports Repression Using a Network of Spies Hidden in Plain Sight |by Sebastian Rotella and Kirsten Berg |July 22, 2021 |ProPublica 

The indictment was read out in court as “the state of Texas versus Jack Rubenstein, alias Jack Ruby,” leading Belli to shout, “He answers to the name of Jack Ruby.” The trial of the man who killed the man who killed Kennedy |Thurston Clarke |July 9, 2021 |Washington Post 

He was living under the alias Alonso Rivera Muñoz as a middling real estate developer and art collector in Querétaro. Trading Dime Bags for Salvador Dali |Jason McGahan |October 19, 2014 |DAILY BEAST 

Part of the alias and the disguise is masking racism with the mechanisms of large, diverse institutions. How We Got to Ferguson—a Reading List |David Masciotra |August 23, 2014 |DAILY BEAST 

Miller, who also uses the alias Frazier Glenn Cross Jr., has an extensive résumé of hate. Hate—and Hitler—in the Heartland: The Arrest of Frazier Glenn Miller |John Avlon, Caitlin Dickson |April 14, 2014 |DAILY BEAST 

As far as I could tell, only one of my friends had managed to ditch her adolescent alias along the way—and with good reason. We're All Still Secretly Using Our 1990s AOL Screen Names. Why? |Andrew Romano |January 23, 2014 |DAILY BEAST 

An older person interested in history, I would suggest Alias Grace or The Blind Assassin. How I Write: Margaret Atwood |Noah Charney |October 10, 2013 |DAILY BEAST 

Billy Towler—alias Walleye—looked after him with an air of uncertainty. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

I've asked them and their friends to come down here to headquarters for the unveiling of Black Hood, alias the Eye. Hooded Detective, Volume III No. 2, January, 1942 |Various 

"You might spare me that 'alias, the Eye' business," Black Hood said, some of his old-time banter returning. Hooded Detective, Volume III No. 2, January, 1942 |Various 

And up sprang the little black figure of Pizotti, alias Plornish, and the next moment he had leaped to the ice! The Girls of Central High on the Stage |Gertrude W. Morrison 

Widow of a workman named Pierre alias Bougival; she was usually designated by the latter name. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe