Fauci continued in 2012 and beyond to marshal support for NIH’s handling of the experiments with the H5N1 strain and for other gain-of-function projects. A science in the shadows |David Willman, Madison Muller |August 26, 2021 |Washington Post 

Since at least 2014, the DOJ’s asset forfeiture program, which is run by the marshals, has taken the same approach with cryptocurrency and opened up the stores of crypto it seizes to bids from the public. Feds are seizing cryptocurrency from criminals. Now they have to figure out what to do with it. |Rebecca Heilweil |July 30, 2021 |Vox 

“With his shoulder insignia changed, it appears Pak was demoted from marshal of the Korean People’s Army, the highest military rank under Kim Jong Un, to vice-marshal,” according to NK News, an independent website in Seoul. New Kim Jong Un Purge Suggests North Korea Is in Deep Shit |Donald Kirk |July 9, 2021 |The Daily Beast 

His uniform would have displayed his rank as a marshal above a chest full of military medals. New Kim Jong Un Purge Suggests North Korea Is in Deep Shit |Donald Kirk |July 9, 2021 |The Daily Beast 

Robert Brandt, the chief deputy marshal at the courthouse, said officials are waiting for a determination on whether Tuesday’s fire was intentionally set or was an accident, possibly due to faulty wiring. U.S. Marshals Service vehicle damaged by fire outside D.C. courthouse |Peter Hermann |July 7, 2021 |Washington Post 

It has a presence, it remains potentially destructive, but all we can do is attempt to marshal it. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

The earl was killed in battle and Marshal captured, but he would later be ransomed by the queen herself. England’s Greatest Knight Puts ‘Game of Thrones’ to Shame |William O’Connor |December 9, 2014 |DAILY BEAST 

With Marshal at his side, Richard crushed Philip and his armies. England’s Greatest Knight Puts ‘Game of Thrones’ to Shame |William O’Connor |December 9, 2014 |DAILY BEAST 

Marshal appears in many of the sources regarding these rulers, and therefore, it seems, much can be verified. England’s Greatest Knight Puts ‘Game of Thrones’ to Shame |William O’Connor |December 9, 2014 |DAILY BEAST 

Before dying in 1219, Marshal would begin the task of rebuilding England after decades of war. England’s Greatest Knight Puts ‘Game of Thrones’ to Shame |William O’Connor |December 9, 2014 |DAILY BEAST 

He distinguished himself in several campaigns, especially in the Peninsular war, and was raised to the rank of field marshal. The Every Day Book of History and Chronology |Joel Munsell 

A mushir (marshal) would find it derogatory to his dignity to smoke out of a stem less than two yards in length. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Never had the genius of the Marshal stood higher than in this difficult retirement from Portugal. Napoleon's Marshals |R. P. Dunn-Pattison 

An incident of the campaign of 1806 gave the Marshal's enemies an excellent opening for showing their dislike. Napoleon's Marshals |R. P. Dunn-Pattison 

While secretly countenancing every attack on the Marshal, the Emperor, for family reasons, was loth to come to an open breach. Napoleon's Marshals |R. P. Dunn-Pattison