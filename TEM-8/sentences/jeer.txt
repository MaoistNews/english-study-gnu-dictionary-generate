When either was mentioned, boos and jeers filled the ballroom. Conservatives turn to a new message: ‘God bless the Ukrainian people’ |David Weigel |February 27, 2022 |Washington Post 

A mighty shock drowned the boy's words at this moment, and seemed to jeer at them. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward 

One of the first canons of criticism is never to attempt the feat yourself; jeer rather at others. The Daughters of Danaus |Mona Caird 

But men also humiliate us, degrade us, jeer at, ridicule the miseries that they and their society entail upon us. The Daughters of Danaus |Mona Caird 

The driver had turned to jeer at his companion when he found himself seized in a grip there was no fighting against. The Light That Lures |Percy Brebner 

Of course the boys of the town were attracted by the stranger's singular movements, and began to hoot and jeer. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng