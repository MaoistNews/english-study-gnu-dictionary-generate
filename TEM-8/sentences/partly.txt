The discrepancy is partly due to many unemployed Americans being misclassified as employed, Powell said during a virtual speech at the Economic Club of New York. Fed chair: Unemployment rate was closer to 10 percent, not 6.3 percent, in January |Rachel Siegel |February 10, 2021 |Washington Post 

The answer lies partly in the science of concussions and partly in helmet testing and certification. The Trek WaveCel Helmet Lawsuit, Explained |Joe Lindsey |February 10, 2021 |Outside Online 

Thousands of years later, it turned out Aristotle was partly wrong there as well. Undergraduates Hunt for Special Tetrahedra That Fit Together |Kevin Hartnett |February 9, 2021 |Quanta Magazine 

Friday night looks partly to mostly cloudy and cold with lows in the 20s. D.C.-area forecast: Cloudy and milder today before winter storm threats arrive |Matt Rogers |February 9, 2021 |Washington Post 

For the rest of the day, we should have partly cloudy skies and highs in the mid-40s to lower 50s. PM Update: Patchy, light freezing rain is a risk late tonight into early Tuesday |Ian Livingston |February 8, 2021 |Washington Post 

DuVernay has partly succeeded in presenting a more human King, warts and all. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

That is partly because it can be said that this is the book that saved Christmas. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

On Sunday morning, the meeting continued in his house, partly a debating forum, partly a steering committee. How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

Whitaker also had Obama partly to thank for his job with the DPH. Obama’s Golf Buddy May Be a ‘Hostile Witness’ in Chicago Corruption Case |Ben Jacobs |December 3, 2014 |DAILY BEAST 

Granted, partly this is a problem of sources the author identifies in the introduction. How Clausewitz Invented Modern War |James A. Warren |November 24, 2014 |DAILY BEAST 

Sometimes the stems are quite bare; on other occasions they are partly branched; in any case the branches are short. How to Know the Ferns |S. Leonard Bastin 

The churchyard was partly surrounded by houses, and in 1781 "iron pallisadoes" were affixed to the wall. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

A few years back it was partly turned into a depot for American meat, but is now simply used for warehouses. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

This intimacy arose partly from association while fishing for Cod, which abound in these waters, and partly from trading in furs. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

A great shout—partly, no doubt, of disappointment—was given when the lions escaped. Hunting the Lions |R.M. Ballantyne