Congress should pass legislation allowing for an expedient and merciful resettlement of Afghan refugees into the country. The U.S. Offered My Family a Home After the Vietnam War. It Must Do the Same for Those Fleeing Afghanistan |Aimee Phan |August 17, 2021 |Time 

In some uncomplicated health-care situations, professional medical knowledge is sufficient to find an expedient solution, so decisions are straightforward. Holistic decision-making in a digitized health-care environment |Siemens Healthineers |June 16, 2021 |MIT Technology Review 

After all, they reasoned, Burr was an expedient politician who would defect to the party that thrust him into power. The election that foreshadowed 2020 |James Morone |January 11, 2021 |Washington Post 

Machine learning provides an equitable, precise, and expedient capability to allocate our precious vaccine supplies. The COVID-19 vaccine rollout is dangerously flawed. Science and data could fix it |matthewheimer |December 18, 2020 |Fortune 

Walling America off—whether physically, economically, or digitally—is expedient, but it is the ultimate self-defeating move for a 21st-century power that relies on international interconnectedness. Trump’s TikTok ban isn’t ‘tough on China’—it’s actually quite the opposite |jakemeth |September 9, 2020 |Fortune 

It was the result of a chain of good decisions—wise, prudent, long-sighted, or, at the least, expedient choices. Why Does the USA Depend on Russian Rockets to Get Us Into Space? |P. J. O’Rourke |June 22, 2014 |DAILY BEAST 

So in Florida, backing Medicaid expansion may become the politically expedient thing for the Republican. States Edge Closer to Medicaid Expansion: Who’ll Go First? |Michael Tomasky |January 8, 2014 |DAILY BEAST 

Obama noted Thursday that both sides in the conflict blame the U.S., a popular and expedient political tactic in Egypt. How Obama Lost His Influence in Egypt |Josh Rogin |August 16, 2013 |DAILY BEAST 

And because “it is very tempting to a minister to employ such an expedient…the practice will…be abused, in every government.” Austerity’s Scottish Ghosts Haunt the Modern Economic Mind |Mark Blyth |May 12, 2013 |DAILY BEAST 

The egalitarian rule-follower is merely expedient, but the loyal person will go to the wall for you. Why Favoritism Is Virtuous: The Case Against Fairness |Stephen T. Asma |December 7, 2012 |DAILY BEAST 

I beseech your Majesty to be pleased to have executed immediately what is most expedient for the royal service in this matter. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

But, after all, perhaps it might be easier and more expedient if he were to appear to accept the Seneschal's statement. St. Martin's Summer |Rafael Sabatini 

Probably his retreat would be cut off by some similar device, so the bolder expedient of an advance offered the better chance. The Red Year |Louis Tracy 

He trusted that what might be done in this matter be most expedient for the service of the king our sovereign. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Yet all this was beside the main point, which was that the action of Congress, whether expedient or not, was illegal. The Eve of the Revolution |Carl Becker