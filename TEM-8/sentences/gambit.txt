Regular visitors to local art spaces will recognize some artists’ familiar gambits in new editions. In the galleries: A wide array of media carry election-year messages |Mark Jenkins |October 30, 2020 |Washington Post 

It’s a calculated gambit, primed to prod advertisers into thinking they’re missing out on the next big thing in social media. ‘We’re at the crux of it’: How TikTok rival Triller is brashly pitching advertisers |Seb Joseph |October 12, 2020 |Digiday 

Charles Schwab’s namesake brokerage, which slashed brokerage commissions in the 1970s, had a similar marketing gambit. Robinhood’s speedy rise is shaking up the brokerage market |John Detrixhe |September 19, 2020 |Quartz 

I guess Falwell folded on that gambit when he realized, as I’ve said here many a time, video is forever. Who Among Us Has Not Seduced the Pool Boy? |Eugene Robinson |August 30, 2020 |Ozy 

As publishers look for pockets of audience engagement wherever they can find them, limited-run educational newsletters are a gambit gaining some traction. ‘The second wave’: Publishers see the value of providing education through newsletter courses |Kayleigh Barber |August 27, 2020 |Digiday 

This gambit means Aereo is avoiding paying anything to broadcasters or the middlemen it is replacing. What the Aereo Decision Means for You |Kyle Chayka |June 25, 2014 |DAILY BEAST 

For these other news organizations, their hunt for the ex-prisoners was a multi-thousand dollar high-risk gambit. My Search for the Taliban Five |Josh Rogin |June 15, 2014 |DAILY BEAST 

How did you arrive at him as your Gambit, and why did you decide to move away from Taylor Kitsch? Simon Kinberg on ‘X-Men: Days of Future Past,’ ‘The Fantastic Four’ Reboot, and Black Superheroes |Marlow Stern |May 25, 2014 |DAILY BEAST 

Jensen is in a different political place than Grimes, and her more aggressive stance may indeed be a savvy strategic gambit. Meet the Kentucky Dem Running On, Not From, Obamacare |Jonathan Miller |April 22, 2014 |DAILY BEAST 

One gambit is to require photo identification, a reasonable-sounding provision that 34 states have now adopted. The ID Whose Time Has Come |Eleanor Clift |April 16, 2014 |DAILY BEAST 

Whatever kind of gambit is being played here, it is bigger than any of its parts or pieces. Highways in Hiding |George Oliver Smith 

When the rolling was very bad, Mrs. Gambit clutched me with one hand and her right hand neighbour with the other. The Chaplain of the Fleet |Walter Besant and James Rice 

Nor could I understand why Mrs. Gambit spoke scornfully of this act of kindness, which was entirely unexpected by me. The Chaplain of the Fleet |Walter Besant and James Rice 

Supper ended, Mr. Gambit lit a pipe of tobacco and began to smoke, begging me not to mind him. The Chaplain of the Fleet |Walter Besant and James Rice 

When he had finished, Mrs. Gambit dropped her chin and returned to practical business. The Chaplain of the Fleet |Walter Besant and James Rice