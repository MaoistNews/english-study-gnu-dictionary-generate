If using the gloves for driving, the fit should be snug as well but also allow for a bit of stretch as well as grip to make handling the steering wheel comfortable and safe. Best winter gloves: Our picks for touch screen gloves, ski gloves, and more |PopSci Commerce Team |February 2, 2021 |Popular-Science 

A steering wheel replaced the traditional stick for better control. Chuck Yeager has died at 97, but the legacy of his record-breaking flight lives on |Rob Verger |December 8, 2020 |Popular-Science 

Only remove hands from the steering wheel when in a Hands-Free Zone. How the autonomous vehicle engineers at Ford are helping to make it safer to drive |By Ford Built for America |December 3, 2020 |Popular-Science 

This detailed toy—with 2,573 pieces—has a working steering wheel, four-speed gearbox, all-wheel drive, forward-folding seats and more. Holiday gifts for gearheads |Joe Phillips |November 23, 2020 |Washington Blade 

“Oh my gosh,” whispers the driver, whose hands hover above the steering wheel as the car moves on its own. Who’s liable when a self-driving car collides with another vehicle? |dzanemorris |November 21, 2020 |Fortune 

They were racing toward the corner of Tompkins and Myrtle avenues with Johnson at the wheel when another call came over the radio. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

Kim Jong Un is changing role models, steering Pyongyang away from Chinese autocrats toward the ultra-aggressive Vladimir Putin. Kim Jong Un’s Kid Gloves Are Now Off |Gordon G. Chang |December 17, 2014 |DAILY BEAST 

On Sunday morning, the meeting continued in his house, partly a debating forum, partly a steering committee. How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

“They think Putin is the only evil in Russia and dream about getting rid of him,” he said, tightening his grip on the wheel. Think Putin’s Bad? Wait for the Next Guy |Anna Nemtsova |November 14, 2014 |DAILY BEAST 

Maybe the wheel will turn again, and heterosexuality will come to seem edgy. How Straight World Stole ‘Gay’: The Last Gasp of the ‘Lumbersexual’ |Tim Teeman |November 12, 2014 |DAILY BEAST 

Never again would he sit behind that wheel rejoicing in the insolence of speed. The Joyous Adventures of Aristide Pujol |William J. Locke 

He deposited it on the vacant seat, clambered up behind the wheel, and started. The Joyous Adventures of Aristide Pujol |William J. Locke 

The non-elastic character of water made it unsuitable for a machine requiring a fly-wheel. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

To quote Mrs. Kaye, 'A Liberal peer is as useful as a fifth wheel to a coach, and as ornamental as whitewash.' Ancestors |Gertrude Atherton 

I think 6½ feet diameter for the fly, and 9½ inches diameter for the small wheel, will give speed enough to the drum. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick