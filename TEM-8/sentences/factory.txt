Elon Musk called an attempted cyberattack against Tesla “serious,” a comment that confirms the company was the target of a foiled ransomware attempt at its massive factory near Reno, Nevada. Elon Musk confirms Tesla was target of foiled ransomware attack |Kirsten Korosec |August 28, 2020 |TechCrunch 

Not only replaceable, but guaranteed replaceable—and for reasonably technical end users, user-replaceable—with easily purchased parts from the factory. Fairphone users can buy new camera without replacing the phone itself |Jim Salter |August 27, 2020 |Ars Technica 

The lion’s share of the US’s imported exercise equipment comes from China—some 65% in 2019, according to the International Trade Centre—where factories were shutting down at the beginning of 2020 due to Covid-19. Why the US can’t stock dumbbells fast enough |Jenni Avins |August 26, 2020 |Quartz 

Anything could be built in industrial areas – standalone homes, apartments, businesses or factories. Single-Family Zoning’s Century of Supremacy in San Diego |Andrew Keatts |August 26, 2020 |Voice of San Diego 

The IT sector offers an alternate way forward in an economy still dominated by Soviet-style collective farms and state-run factories. Belarus’s crackdown could squash Minsk’s budding tech sector |Nicolás Rivero |August 20, 2020 |Quartz 

The cameraman was reporting on the factory catching fire when the inevitable happened. Fireworks Factory Explodes in Colorful Burst |The Daily Beast Video |January 6, 2015 |DAILY BEAST 

A plastic factory, a hardware supplier, and shipping–and-receiving giants like Fed-Ex and DHL are neighboring businesses. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

Not just a candy factory but a candy store, and everything in it free. ‘Asteroids’ & The Dawn of the Gamer Age |David Owen |November 29, 2014 |DAILY BEAST 

By day, she cares for her children in a bombed-out milk factory that hosts her orphanage, Okutiuka. Death Metal Angola: Heavy Metal in War-Torn Africa |Nina Strochlic |November 21, 2014 |DAILY BEAST 

Have we become a nation of Veruca Salts from “Willie Wonka and The Chocolate Factory,” who are unbearably demanding? We've Been on the Wrong Track Since 1972 |Dean Obeidallah |November 7, 2014 |DAILY BEAST 

It was afterwards used as a schoolroom in connection with Winfield's factory. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

A Lowell factory-girl would consider this entirely out of character, and a New-York milliner would be shocked at the idea of it. Glances at Europe |Horace Greeley 

Think of this, ye who talk, not always without reason, of "factory slaves" and the meagre rewards of labor in America. Glances at Europe |Horace Greeley 

Gallinas, the noted slave factory on the west of Africa, purchased by the Liberian republic. The Every Day Book of History and Chronology |Joel Munsell 

Seven o'clock in the morning is too early for any rational human being to be herded into a factory at the call of a steam whistle. The Unsolved Riddle of Social Justice |Stephen Leacock