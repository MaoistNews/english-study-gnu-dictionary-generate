Even Mahomes’s improvisational theater came to suggest desperation more than imagination. NFL defenses no longer dominate, making Bucs’ shutdown of Chiefs even more impressive |Chuck Culpepper |February 8, 2021 |Washington Post 

A campaign so simple, it might seem, could have only come out of desperation. Celebrities have always done endorsements. Now they sell their fast food orders. |Jameson Rich |February 5, 2021 |Vox 

So then he started on a series of efforts, in desperation, to get the items in hand. Contractor admits he lied to get nearly $40 million in N95 contracts from federal government |Rachel Weiner |February 4, 2021 |Washington Post 

Plays on which win probability was below 20 percent were removed on the assumption that they were desperation calls. Some Coaches Shy Away From Risk In The Playoffs. Not Andy Reid. |Josh Hermsmeyer |January 22, 2021 |FiveThirtyEight 

At this point, the desperation to get out of the industry is an effort to protect their mental health, they said. ‘Always on trauma machine’: Social media managers grapple with burnout, leaving the industry |Kimeko McCoy |January 13, 2021 |Digiday 

Stories of war, death, fear and desperation do not have happy endings. Inside the Smuggling Networks Flooding Europe with Refugees |Barbie Latza Nadeau |December 15, 2014 |DAILY BEAST 

What does our desperation to get a nuclear deal at all costs say to the modern-day Iranian Solzhenitsyns rotting in Evin prison? Iran’s Horrific Human-Rights Record |Sen. Mark Kirk, Sen. Marco Rubio |November 7, 2014 |DAILY BEAST 

They were shouting with a mixture of fury and desperation about their families in Kobani, under siege just across the line. When ISIS Is An Excuse For A Police State |Jamie Dettmer |October 18, 2014 |DAILY BEAST 

Few bragged about online dating, often keeping the whole ordeal secret, as it reeked of dating desperation. Swipe Right For Sex: Mixxxer Is Tinder for the Porn Star Set |Aurora Snow |October 4, 2014 |DAILY BEAST 

Perhaps, instead, they had reached a desperation we can't quite fathom. The Fear That Killed Eight Ebola Workers |Abby Haglage |September 20, 2014 |DAILY BEAST 

Faith and hope had left her; and as to love, she knew that she loved one man only, and loved him to desperation. Skipper Worse |Alexander Lange Kielland 

At last in desperation, I tried my only remaining tune, not being very proficient on the flute. Kari the Elephant |Dhan Gopal Mukerji 

And so step by step the devil thrust him into desperation, and strove thereby to clinch the hopelessness of his estate. Julian Home |Dean Frederic W. Farrar 

Then Farnham took down a shutter, and in desperation threw open the windows to let some fresh air in. Digby Heathcote |W.H.G. Kingston 

He was in a state of fury, full of plottings of desperation, swearing to himself that he would show no mercy. Overland |John William De Forest