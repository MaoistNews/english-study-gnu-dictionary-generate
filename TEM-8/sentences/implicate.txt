Shining lights into dark and secret worlds and speaking truth to power comes with great risk, and here it was an even greater risk—because it directly implicated him. Inside a secret running program at Nike and a win-at-all-costs corporate culture |Rachel King |October 6, 2020 |Fortune 

The lawsuit also says 48 cases of sexual harassment had been reported over the previous two years, including 13 that implicated senior managers or executives. Alphabet to fund $310 million diversity initiative to settle sexual misconduct lawsuit from shareholders |Danielle Abril |September 25, 2020 |Fortune 

The article directly implicated Google Local results and the ease of how unsavory people game them to commit fraud. A new era has arrived in local search: Google’s Local Trust Pack |Justin Sanger |September 18, 2020 |Search Engine Land 

Fleer is hardly the first sheriff’s employee to be implicated in sexual misconduct. Morning Report: Local Pension Funds Scraped by in the Pandemic |Voice of San Diego |July 24, 2020 |Voice of San Diego 

While systole activated inhibitory brain regions, it also activated the amygdala, an area implicated in the experience of fear. How Your Heart Influences What You Perceive and Fear |Jordana Cepelewicz |July 6, 2020 |Quanta Magazine 

Though there are many claims that implicate it in improved brain function, the evidence in support of this finding is tenuous. Fish Oil, Turmeric, and Ginseng, Oh My! Are ‘Brain Foods’ B.S.? |Dr. Anand Veeravagu, MD |October 10, 2014 |DAILY BEAST 

In Illinois, which houses some of the tougher DUI laws in the nation, even smoking a joint a week before can implicate you. The Truth About Driving While Stoned |Abby Haglage |June 12, 2014 |DAILY BEAST 

Several lines of scientific evidence have begun to implicate genes that control dopamine. Are We Killing Our Sports Gene? |David Epstein |August 4, 2013 |DAILY BEAST 

Because misogynist monsters always implicate themselves in crimes to protect women. Prosecuted for Standing Her Ground |Kirsten Powers |July 19, 2013 |DAILY BEAST 

Pointing fingers, he stated “the people that I trusted to run it” are the ones to implicate. Murdoch Hearing's 8 Best Moments | |July 19, 2011 |DAILY BEAST 

She was trying to find some explanation that would clear the boss, and perhaps implicate the Hatch crowd. The Wreckers |Francis Lynde 

He knew that the whole complex machinery of Scotland Yard was working, and working at top speed, to implicate him in the tragedy. The Daffodil Mystery |Edgar Wallace 

You might have done me the service of not excusing yourself to the squire when he came here, in such a way as to implicate me. Rhoda Fleming, Complete |George Meredith 

If I were to expose Flemming, it would implicate Thornton, and that seemed too much of a retaliation. Frank Merriwell's Races |Burt L. Standish 

It would be a dangerous document in case he should be searched; for its contents would expose him, and implicate others. Down the Rhine |Oliver Optic