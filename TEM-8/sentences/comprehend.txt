Awe, the emotional response and feelings of wonder people experience when seeing things that they can’t fully comprehend, has long been associated with better mental and physical health. Regular doses of awe can do wonders for emotional health |Kat Eschner |September 22, 2020 |Popular-Science 

We in the media unfortunately spent a couple of decades abetting this by deifying young tech founders who, it was often implied, simply know things we mere mortals can never comprehend. HBO’s new sex cult doc has big lessons for investors |dzanemorris |September 2, 2020 |Fortune 

In any case, once science began to comprehend the subatomic world, no force could stop the eventual revelation of the atom’s power. How understanding nature made the atomic bomb inevitable |Tom Siegfried |August 6, 2020 |Science News 

Make Googlebot do as little leg-work as possible in order to comprehend your content. JavaScript rendering and the problems for SEO in 2020 |Anthony Lavall |May 6, 2020 |Search Engine Watch 

I saw a few pictures in there that looked pretty but couldn’t comprehend what I was reading. Lack of diversity in his field has troubled this mathematician |Esther Landhuis |April 14, 2020 |Science News For Students 

I wouldn't, but I also wouldn't be surprised if Patriots fans didn't properly comprehend the mechanics of sex, either. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

White rappers are always difficult to comprehend, difficult to deal with. The Cultural Crimes of Iggy Azalea |Amy Zimmerman |December 29, 2014 |DAILY BEAST 

It is very difficult to sit by helplessly while a friend is imprisoned for a crime that is too implausible to comprehend. Behind Bars for the Holidays: 11 Political Prisoners We Want to See Free In 2015 |Movements.Org |December 25, 2014 |DAILY BEAST 

I could not comprehend the fact that Kristen and Lorne and Bill and all these people are singing “Happy Birthday” to me. How Aidy Bryant Stealthily Became Your Favorite ‘Saturday Night Live’ Star |Kevin Fallon |October 31, 2014 |DAILY BEAST 

Indeed, when you read it, you comprehend how, almost inevitably, his oratorical gifts eclipsed his skill as a writer. Martin Luther King’s Nobel Speech Is an Often Ignored Masterpiece |Malcolm Jones |October 16, 2014 |DAILY BEAST 

We cannot bring our minds to comprehend that power; but let us raise our thoughts and try to understand something concerning it. Gospel Philosophy |J. H. Ward 

These cherished plants seemed to comprehend and requite unaffected love. Madame Roland, Makers of History |John S. C. Abbott 

Nor hast thou ever known, Aspasia,Or couldst thou comprehend the thoughts that onceThou didst inspire in me. The Poems of Giacomo Leopardi |Giacomo Leopardi 

Not long after this talk Bessie had an experience in school that helped her to comprehend her mother's words. The value of a praying mother |Isabel C. Byrum 

It is very important they should comprehend my act quite clearly, and he is the very man to do this great service to humanity. Prison Memoirs of an Anarchist |Alexander Berkman