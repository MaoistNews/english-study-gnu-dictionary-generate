In place of “tomato-soup mystery cake” was “reduced-fat chocolate mousse cake.” Maria Guarnaschelli, influential cookbook editor, dies at 79 |Emily Langer |February 11, 2021 |Washington Post 

That means you may feel sluggish after eating it, as fats tend to sit heavier in the stomach and digest slower than carbs and protein. These Are Our Editors' Go-To Ski Lunches |Ula Chrobak |February 9, 2021 |Outside Online 

As Rachel lets her mother’s messages go unanswered, she craves forbidden food, the kind with salt and fat and sugar. Melissa Broder’s ‘Milk Fed’ is a delectable exploration of physical and emotional hunger |Bethanne Patrick |February 9, 2021 |Washington Post 

Big, fat fluffy flakes that fall from the sky and pile up in freezing mounds. Let’s learn about snow |Bethany Brookshire |February 9, 2021 |Science News For Students 

In the wintertime, fat-tire bikes are allowed at select parks, including on ungroomed trails within Acadia National Park. 7 Unique Ways to Experience National Parks |Megan Michelson |February 6, 2021 |Outside Online 

“One of the big misconceptions is that eating fat makes you fat, because it has more calories,” Asprey says. Bulletproof Coffee and the Case for Butter as a Health Food |DailyBurn |December 27, 2014 |DAILY BEAST 

Roll the pork over the stuffing, like a jelly roll, until the seam is facing down and the fat back is on top. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

Lay the butterflied pork loin on the cutting board with the fat cap facing down. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

He wasn't crazy about being fat, but he saw his body as a tool to use in the making of his career. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

About our Eric Garners—too fat, too scared, too noncompliant, too many kids—there are always, as Flagg knows well, excuses. McConaughey’s ‘Stand’—And Ours |James Poulos |December 5, 2014 |DAILY BEAST 

Give a sweet savour, and a memorial of fine flour, and make a fat offering, and then give place to the physician. The Bible, Douay-Rheims Version |Various 

Then a fat, untidy old man appeared in the doorway of a cubicle within the shop, and Edwin Clayhanger blushed. Hilda Lessways |Arnold Bennett 

He wished her mother had not been quite such an appalling person, fat and painted. Rosemary in Search of a Father |C. N. Williamson 

It was a spring day, and the fat buds of the chestnuts were bursting into magnificent green plumes. Children's Ways |James Sully 

He controlled himself betimes, bethinking him that, after all, there might be some reason in what this fat fellow said. St. Martin's Summer |Rafael Sabatini