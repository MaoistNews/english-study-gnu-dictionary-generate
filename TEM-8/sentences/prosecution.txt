If they do resist it, under the existing legal powers, they would arguably be liable for criminal prosecution under an offense for which jail terms on conviction extend towards seven years. Why Twitter Blocked Accounts Linked to Farmers' Protests in India—Only to Reverse Course |Billy Perrigo |February 1, 2021 |Time 

Ethridge also noted that most soldiers who are flagged for extremist behavior face administrative sanctions — including counseling or retraining — rather than criminal prosecution. The Boogaloo Bois Have Guns, Criminal Records and Military Training. Now They Want to Overthrow the Government. |by A.C. Thompson, ProPublica, and Lila Hassan and Karim Hajj, FRONTLINE |February 1, 2021 |ProPublica 

Markman said there are typically three outcomes following prosecutions of violations of Government Code 1090. How a Volunteer Helped Get the City Into Its Biggest Real Estate Debacle |Lisa Halverstadt |January 29, 2021 |Voice of San Diego 

Voice of San Diego’s Ashly McGlone dove into the data, and found domestic violence prosecutions dropped 15 percent between 2019 and 2020, though the number of cases actually reported only dropped by about 13 percent. Morning Report: City Attorney Prosecuting Fewer Domestic Violence Cases |Voice of San Diego |January 7, 2021 |Voice of San Diego 

Elliott spokeswoman Hilary Nemchik said 257 domestic violence cases referred for prosecution in 2020 are still under review, and prosecutors have one to five years to file charges, depending on the crime. Domestic Violence Prosecutions Have Dropped Under City Attorney Mara Elliott |Ashly McGlone |January 7, 2021 |Voice of San Diego 

It was unclear whether he was speaking on behalf of the Foreign Ministry or the Prosecution. Canadians in Egyptian Jail Might Be Under Suspicion For Film Footage |Jesse Rosenfeld |October 3, 2013 |DAILY BEAST 

Jim Doyle was the uncommunicative spokesman for Archibald Cox and Leon Jaworski at the Watergate Special Prosecution Force. Newsweek’s Watergate Legacy |Jim Doyle |December 29, 2012 |DAILY BEAST 

State appeals Olmert acquittal - Prosecution appeals former PM's acquittal in Rishon Tours and Talansky affairs. Ring, Ring |Orly Halpern |November 8, 2012 |DAILY BEAST 

The Harris County D.A.'s office won the Innocence Project of Texas' “Honesty and Integrity in Prosecution Award” last fall. Pat Lykos: Texas' Capital Punishment Avenger |Ben Crair |April 4, 2011 |DAILY BEAST 

The Non Prosecution Agreement stipulated that they would not be charged. The Billionaire Pedophile's Sex Den |Conchita Sarnoff |July 22, 2010 |DAILY BEAST 

The Prosecution hoped to show that this chain was the one which I had said had been stolen. My Memoirs |Marguerite Steinheil 

And now the Graft Prosecution was to learn by public vote how many of the people stood behind it. Port O' Gold |Louis John Stellman 

Enemies of Prosecution, backed by an enormous fund, were setting innumerable obstacles in their way. Port O' Gold |Louis John Stellman 

Most of the San Francisco papers heaped abuse upon the Prosecution, its attorneys and its judges. Port O' Gold |Louis John Stellman 

But before a jury was empanelled the November ballot gave the Prosecution its "coup de grace." Port O' Gold |Louis John Stellman