They could have had trouble raising new capital, it could have been problematical. Barney Frank on JPMorgan’s Contradictions, Mitt Romney’s Mistakes |Eleanor Clift |May 26, 2012 |DAILY BEAST 

As main-character material, Wittgenstein likewise was highly problematical. When Your Hero’s an SOB |Bruce Duffy |August 1, 2011 |DAILY BEAST 

This attitude of the scientific world toward these problematical occurrences is quite comprehensible. Man And His Ancestor |Charles Morris 

Accordingly, merchants prefer a representative currency, even though its representative character be somewhat problematical. The Atlantic Monthly, Volume 14, No. 81, July, 1864 |Various 

Probably for this reason the "delivery" of the labor vote by the Federation has ever been so largely problematical. A History of Trade Unionism in the United States |Selig Perlman 

Wallace's plays are more or less problematical and Maud has invested a good deal of her money in this. The Beauty |Mrs. Wilson Woodrow 

During part of the march up rations had been short, and for a number of days were very problematical. War in the Garden of Eden |Kermit Roosevelt