It was daring, sculptural, colourful and free – and like nothing else that had gone before. What Makes a Masterpiece of a City? |David Frum |December 11, 2012 |DAILY BEAST 

What is the point of this thick, colourful book, except as a sort of cultural tea bag for the American market? Pippa Middleton Book is a "Cultural Tea Bag", says Telegraph's Christopher Howse |Tom Sykes |November 2, 2012 |DAILY BEAST 

Artists have colourful lives, and this fascinating biography reveals the interesting times of this most amusing painter. Alexander McCall Smith’s Art Book Bag |Alexander McCall Smith |September 4, 2012 |DAILY BEAST 

Almost the first thing which impressed us was the colourful quality of the winter landscape. The Idyl of Twin Fires |Walter Prichard Eaton 

It is not hard to make for oneself a colourful picture of a typical Sunday congregation in these dead and gone days. Greenwich Village |Anna Alice Chapin 

The colourful and graphic pictures make these histories beloved by all children whether they read the text or not. The Story of Mankind |Hendrik van Loon 

Few dare visit the colourful forest of evil or the treacherous river, for strange and unholy things dwell therein. The Fantasy Fan, October 1933 |Various 

In the course of their career, few women could have cut a wider swath, or one more colourful and glamorous. The Magnificent Montez |Horace Wyndham