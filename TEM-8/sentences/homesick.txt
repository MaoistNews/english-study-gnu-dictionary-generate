So, when he moved away from Southern California for work and grew homesick, tinga was the first recipe he learned how to cook. This quick jackfruit or chicken tinga recipe is all about the sauce |Daniela Galarza |November 18, 2021 |Washington Post 

In May 1965, Brian Robson was miserably homesick after working nearly a year in Melbourne, Australia, but he couldn’t afford a plane ticket home to Wales. Man seeks pals he once persuaded to ship him around the world in a crate |Cathy Free |April 16, 2021 |Washington Post 

There’s the distance he feels from his lonely, homesick-for-the-old-world mother. Alfred Kazin’s “A Walker in the City” charts an intellectual awakening, block by block |Liza Weisstuch |March 4, 2021 |Washington Post 

When Shawn was on the Olympic circuit, she says coffee was an easy cure for her homesick blues. Why an Olympic gold medalist and former NFL player cofounded a subscription coffee brand |Rachel King |September 29, 2020 |Fortune 

The soldiers smiled back at the friendly locals—they all felt homesick and could not wait to see their families. Ukrainian Troops Surrender to Unarmed Pro-Russian Protesters |Anna Nemtsova |April 17, 2014 |DAILY BEAST 

I also found that Hofbräu-Festzelt is the tent of choice for homesick English-speakers. My Two-Day Oktoberfest Bender |Kristyn Ostman |October 5, 2013 |DAILY BEAST 

If she is at all uncomfortable or homesick, Lamb promises to take her to the nearest airport and send her back to her mom. Great Weekend Reads |Malcolm Jones, Lucy Scholes, Jacob Silverman, Drew Toal |September 18, 2011 |DAILY BEAST 

She was placed in a dorm with other first-year students, and for her entire first week of school she was very homesick. The Next Meryl Streep |Marlow Stern |August 10, 2011 |DAILY BEAST 

Who it helps: Homesick troops, many of them on their second, third, or even fourth deployment. Don't Be a Scrooge! |The Daily Beast |December 16, 2009 |DAILY BEAST 

He never returned, but died in England on June 3, 1780, an unhappy and a homesick exile from the country which he loved. The Eve of the Revolution |Carl Becker 

Would you ever dream that four children could be homesick in such a beautiful house as Mr. Cordyce's? The Box-Car Children |Gertrude Chandler Warner 

The letters that found their way across the sea were not homesick in these days, and Ikey's mother ceased to worry about him. The Story of the Big Front Door |Mary Finley Leonard 

My heart reaches out in homesick yearning for the notes of our dear Northern songsters. Gardens of the Caribbees, v. 1/2 |Ida May Hill Starr 

Kit could only think of a lost, homesick dog begging for the scent of the trail to his own kennel. The Treasure Trail |Marah Ellis Ryan