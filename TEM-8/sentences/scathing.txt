In the summer of 2018, the city auditor’s office published a scathing report outlining the city’s history of ignoring its growing stormwater needs. San Diego’s Infrastructure Deficit Is Really a Stormwater Deficit |Andrew Keatts |February 8, 2021 |Voice of San Diego 

Metro on Friday disputed several claims made in a scathing audit of its Rail Operations Control Center, saying its own outside review of the allegations showed many to be untrue while not denying the need to improve workplace culture. Metro disputes audit that claimed rail control center was ‘toxic workplace’ environment |Justin George |December 11, 2020 |Washington Post 

Just days later, a scathing report by short-selling Hindenburg Research alleged a variety of misrepresentations by Nikola of its technology. Why today’s GM news is devastating for Nikola |dzanemorris |November 30, 2020 |Fortune 

As a scathing new profile of Facebook in the New Yorker observes, “The company’s strategy has never been to manage the problem of dangerous content, but rather to manage the public’s perception of the problem.” Facebook’s new tool to stop fake news is a game changer—if the company would only use it |Jeff |October 18, 2020 |Fortune 

Celgene’s cancer drug Revlimid saw massive price hikes year-over-year in order to reach sales targets, according to a scathing Congressional probe on Wednesday. Bristol Myers Squibb and Celgene got a beating in Congress on drug prices—but only Congress can fix the problem |Sy Mukherjee |October 1, 2020 |Fortune 

Scathing sentences already took shape in his brain, but deeper investigation would be necessary before he could write anything. We Two |Edna Lyall 

Scathing letters are all right, but they should be directed and stamped, then burned just before they are trusted to the mails. Little Journeys to the Homes of the Great Philosophers, Volume 8 |Elbert Hubbard 

Scathing comments by Franklin on Thomas Penn's meanness, 138. Benjamin Franklin; Self-Revealed, Volume I (of 2) |Wiliam Cabell Bruce 

Scathing as some of the portraits are, the writer is by no means merely cynical. The Marriage of William Ashe |Mrs. Humphry Ward