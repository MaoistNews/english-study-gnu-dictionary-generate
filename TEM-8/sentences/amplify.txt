“Core to what we do at Live Nation is helping amplify voices onstage around the world, and supporting voting is another important way we want to continue making voices heard,” said Michael Rapino, the company’s president and CEO, in a statement. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

In removing accounts Tuesday, Twitter pointed to policies specifying, “You can’t artificially amplify or disrupt conversations through the use of multiple accounts.” Pro-Trump youth group enlists teens in secretive campaign likened to a ‘troll farm,’ prompting rebuke by Facebook and Twitter |Isaac Stanley-Becker |September 15, 2020 |Washington Post 

It will accelerate rapid, perhaps chaotic, urbanization of cities ill-equipped for the burden, testing their capacity to provide basic services and amplifying existing inequities. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

Instead, they use it to amplify other projects that help them make money. What’s Oracle? TikTok users react to proposed Oracle deal |Danielle Abril |September 15, 2020 |Fortune 

Our priority at our events is to amplify the voices that have been silenced by the majority. The 2019 TechCrunch Include Report |Henry Pickavet |September 11, 2020 |TechCrunch 

Actress, activist and African American: Danai Gurira believes in the power of using her voice to amplify African storytellers. Walking Dead’s Danai Gurira Vs. Boko Haram |Kristi York Wooten |November 30, 2014 |DAILY BEAST 

These tools of expression are meant to create and amplify energy. DJ Steve Aoki: To Cake or Not To Cake |Steve Aoki |August 8, 2014 |DAILY BEAST 

She was determined—for her voice to be heard, and for it to amplify the voices of women throughout Libya. Libyan Activist Pays Tribute To Slain Spiritual Sister |Anonymous |June 27, 2014 |DAILY BEAST 

Computerized trading programs react to trends in the market and then amplify those trends. Let’s All Please Stop Overreacting to Bernanke’s Remarks |Daniel Gross |June 20, 2013 |DAILY BEAST 

Society doesn't create these disorders, but it can amplify them, and by extension, it can diminish them. I Was Adam Lanza, Part 2 |David Frum |December 22, 2012 |DAILY BEAST 

He took the word from Bob and made no attempt to alter or to amplify it. Tristram of Blent |Anthony Hope 

But we must first amplify the definition of perspectives and biographies. The Analysis of Mind |Bertrand Russell 

Attempts are frequently made to amplify this description, but the results are always very vague. The Psychology of Singing |David C. Taylor 

Encouraged by his attention, the girl proceeded to amplify her story. Remarkable Rogues |Charles Kingston 

It is impossible now to amplify the thought, but I wish in connection with it to name three particulars. The American Missionary -- Volume 33, No. 02, February, 1879 |Various