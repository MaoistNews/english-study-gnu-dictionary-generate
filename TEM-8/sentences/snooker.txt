The worst attack occurred after nightfall on Thursday at a snooker club in Quetta in an area frequented by Hazara Shia Muslims. Pakistan’s Deadliest Day |Jahanzeb Aslam |January 11, 2013 |DAILY BEAST 

In some rooms it is considered fair and part of the game to snooker an opponent deliberately; in others the practice is condemned. Encyclopaedia Britannica, 11th Edition, Volume 3, Slice 7 |Various 

A four-handed game of snooker is in as rapid progress as is reasonably possible. Punch, or the London Charivari, Vol. 153, July 25, 1917 |Various 

But Snooker, as usual, tried to sneak away, his tail between his legs. Bliss, and Other Stories |Katherine Mansfield 

My dear Jimmie, have I changed so much, then, since last we played snooker together in the club? The Message |Louis Tracy 

A year ago young Snooker had done a month for one of those very trout. Mr. Britling Sees It Through |H. G. Wells