After the death of Tess’s grandmother, the mother and daughter rummage through grandma’s belongings, finding hints of “deep-rooted family secrets” about burglaries and lost treasure near the Canadian border. The making of ‘Open Roads,’ a game about mothers, daughters and road trips |Elise Favis |February 4, 2021 |Washington Post 

Then 29, she began a 20-year sentence for burglary and car theft as part of a crime ring that stashed hundreds of stolen items in a suburban Colorado storage unit. On surviving—and leaving—prison during a pandemic |Sarah Scoles |January 21, 2021 |Popular-Science 

The girl speaks about her grandmother and a mysterious past, explaining that she’s found secrets about her grandma, like the mention of a burglary ring and “some sort of lost fortune.” ‘The Last of Us Part II’ wins game of the year at The Game Awards, alongside new ‘Mass Effect’ and ‘Among Us’ reveals |Elise Favis, Gene Park, Mikhail Klimentov |December 11, 2020 |Washington Post 

He was released from prison three months ago — he served 20 years for armed burglary — then met Winestock. ‘It’s going to take a village’: Community group prepares to deliver 3,000 meals before Thanksgiving |Lauren Lumpkin |November 22, 2020 |Washington Post 

In statistical models that did identify a significant relationship between undocumented immigration and crime, we found undocumented immigration reduces property crimes, such as burglary. Undocumented Immigrants May Actually Make American Communities Safer – Not More Dangerous – New Study Finds |LGBTQ-Editor |October 27, 2020 |No Straight News 

The more accomplished students took classes in safe-cracking, burglary, blackmail, and confidence games. Meet 'The Queen of Thieves' Marm Mandelbaum, New York City's First Mob Boss |J. North Conway |September 7, 2014 |DAILY BEAST 

Identified only as Darius, the teen has been arrested for burglary several times in recent weeks. France Decries Attack That Left Roma Teen in a Coma |Lizzie Crocker |June 18, 2014 |DAILY BEAST 

The trip was bankrolled by low-level Miami gangster Raul Pacheco, himself already on probation for attempted armed burglary. MLB’s Next Headache: Cartels, Gangsters, and Their Cuban Superstars |Peter C. Bjarkman |April 18, 2014 |DAILY BEAST 

They were arraigned in Manhattan criminal court on charges of burglary, reckless endangerment and jumping from a structure. Hero or Criminal? James Brady, the WTC Ironworker Who Jumped Off the Building |Michael Daly |March 25, 2014 |DAILY BEAST 

Burglary, something he hated, seemed to be the only way to find evidence. The Domestic Spying of Hoover’s FBI Is an Eerie Prequel to the NSA’s Snooping Today |Betty Medsger |March 23, 2014 |DAILY BEAST 

As he was relating an account of a desperate burglary, I asked him what he would have done if the man of the house had awakened. The value of a praying mother |Isabel C. Byrum 

There was a burglary at Countess Saens' house, and the thief took nothing but papers. The Weight of the Crown |Fred M. White 

She left here in a violent hurry on her maid coming to say that there had been a burglary at her house. The Weight of the Crown |Fred M. White 

He wisely concluded that it is not sufficient to commit a successful burglary, if you are afterwards found out. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Even burglary becomes a bore when you have to wait too long idle out in the cold. It Is Never Too Late to Mend |Charles Reade