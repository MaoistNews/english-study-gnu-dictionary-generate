She reveled in watching him learn and grow, and shared stories with friends of how he liked playing baseball, took up bass guitar lessons and was interested in history. Tiffany Shackelford, 46, was known as a unique, fun ‘force’ to friends, family |Dana Hedgpeth |February 11, 2021 |Washington Post 

They attended soccer, hockey, baseball and basketball games, saw movies and went to the circus. Date Lab: One of our setups is still going strong two years later. We caught up with Willie and Renee for an update. |Marin Cogan |February 11, 2021 |Washington Post 

Only the catcher with the fourth-most WAR in baseball history, Carlton Fisk. Plenty Of Catchers Peak After 30. Will The Mets Get Lucky With James McCann? |Howard Megdal |February 8, 2021 |FiveThirtyEight 

He moved to the Arizona Republic in 1997 and then in 2003 to ESPN, where he became a well-known face in the network’s baseball coverage. Pedro Gomez, ESPN baseball reporter, dies at 58 |Ben Strauss |February 8, 2021 |Washington Post 

Simon, 74, said her husband enjoyed playing and watching sports, including basketball, soccer and baseball when he was younger. Frank Anderson, 87, cared passionately about feeding the homeless in D.C. |Dana Hedgpeth |February 5, 2021 |Washington Post 

Tim Russert and I are driving back to the Albany airport after taking our kids to the baseball Hall of Fame in Cooperstown. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 

The kids had a gift for him too, a tee shirt with ‘Baseball Spoken Here’ stenciled across the front. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 

This would take time since, under current law, even scouting Cuban baseball players on the island is illegal. Is Major League Baseball Ready For Cuba’s Players? |Ben Jacobs |December 19, 2014 |DAILY BEAST 

The end of the embargo and resumption of diplomatic relations with Cuba could transform Major League Baseball. Is Major League Baseball Ready For Cuba’s Players? |Ben Jacobs |December 19, 2014 |DAILY BEAST 

Baseball has long been the most popular sport in Cuba and the island has long been a hotbed of baseball talent. Is Major League Baseball Ready For Cuba’s Players? |Ben Jacobs |December 19, 2014 |DAILY BEAST 

A kindly young man, with a rather wide face and hands disfigured as to fingers by much early baseball. The Amazing Interlude |Mary Roberts Rinehart 

The sittings in the row do not seem to have been marked off any more than they are now in the "bleachers" at our baseball grounds. The Private Life of the Romans |Harold Whetstone Johnston 

This is a new indoor game which follows out in principle the regular baseball play. The Boy Mechanic, Book 2 |Various 

If ducks could only be taught to play baseball they would beat Ty Cobb at stealing bases. The Red Cow and Her Friends |Peter McArthur 

Living with her was like watching a baseball game with the bases always full and two strikes on the batter. The Boy Grew Older |Heywood Broun