Andrew Keatts wrote last week about the origins of single-family zoning in San Diego, which account for 70 percent of residential land and will be untouched under Mayor Kevin Faulconer’s push for housing reform. Morning Report: MTS Rejects Many Who Applied for Disabled Fare Reductions |Voice of San Diego |August 31, 2020 |Voice of San Diego 

Short North is a business district with a collection of residential neighborhoods. Columbus: A Rich History of LGBTQ Diversity |LGBTQ-Editor |August 30, 2020 |No Straight News 

Even near transit stations, more than 50 percent of area zoned for residential use is restricted to single-family homes. Morning Report: A Century of Single Family Home Supremacy |Voice of San Diego |August 27, 2020 |Voice of San Diego 

That leaves roughly half of all residential land in so-called Transit Priority Areas untouched by the new program, according to data provided by the city’s planning department. Single-Family Zoning’s Century of Supremacy in San Diego |Andrew Keatts |August 26, 2020 |Voice of San Diego 

The county lab from mid-May to mid-June processed 2,100 test kits from residential care facilities, which includes assisted living facilities, more than four times the prior month. COVID-19 Testing Plans Leave Out Assisted Living Facilities |Jared Whitlock |June 26, 2020 |Voice of San Diego 

A Colorado-based full- service residential mortgage banking company. The 26 Next Hobby Lobbys |Abby Haglage |December 17, 2014 |DAILY BEAST 

We gazed on a residential area of box-like homes stacked on top of one another on a steep hillside. Mass Murder in the Holy City |Michael Tomasky |November 18, 2014 |DAILY BEAST 

The name of his equestrian club and residential land development derives from the Otomí native people of central Mexico. Trading Dime Bags for Salvador Dali |Jason McGahan |October 19, 2014 |DAILY BEAST 

Three stray dogs saunter out of our way as we turn into a residential area lined with trailers and graffiti-sprayed stop signs. A Shooting on a Tribal Land Uncovers Feds Running Wild |Caitlin Dickson |August 26, 2014 |DAILY BEAST 

Yes, Hamas is firing rockets from residential areas and storing weapons caches in schools. Why I’m Against Hamas, Against What Israel Is Doing, and For Judaism |Sally Kohn |July 25, 2014 |DAILY BEAST 

They had seen enough to be convinced that there were parts of India much preferable to Delhi for residential purposes. The Red Year |Louis Tracy 

Dropped off Williams on that residential street around the corner from the bank. Hooded Detective, Volume III No. 2, January, 1942 |Various 

They traversed the new residential area characterized by larger grounds and a higher average of architecture. A Hoosier Chronicle |Meredith Nicholson 

It was chiefly in the seventeenth century that what we now know as the West End became a residential quarter. Stories That Words Tell Us |Elizabeth O'Neill 

Charlestown may claim to be the port of St. Austell, and is becoming also a popular residential suburb. The Cornwall Coast |Arthur L. Salmon