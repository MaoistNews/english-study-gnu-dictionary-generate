I have always been fascinated by enmeshed relationships, I think is not an unfair way of putting it. How Gideon the Ninth author Tamsyn Muir queers the space opera |Constance Grady |February 5, 2021 |Vox 

After some 400 million years of evolution, spiders’ forms and uses of silk fascinate researchers like Greco, who are looking for new materials. How a tiny spider uses silk to lift prey 50 times its own weight |Susan Milius |February 3, 2021 |Science News 

Another topic that’s long fascinated me, also having to do with the mind, is memory. “We Get All Our Great Stuff from Europe — Including Witch Hunting.” (Ep. 446) |Steven D. Levitt |January 7, 2021 |Freakonomics 

Krans’s interest in aviation goes back to the 1980s, when his dad, a machinist fascinated by engineering and innovative planes, would sometimes bring home jet models. A CIA spyplane crashed outside Area 51 a half-century ago. This explorer found it. |Sarah Scoles |January 5, 2021 |Popular-Science 

I’m also fascinated by Honey’s story of managing a chronic pain condition in the midst of a pandemic, especially when so many of their symptoms present similarly to Covid-19 symptoms. The Lost Year: Chronic pain, an unusual love story, and reassurance via pickle |Emily VanDerWerff |December 27, 2020 |Vox 

His conservatism, which is more of a cultural than political kidney, seems to fascinate, delight or detract critics. Whit Stillman on the 20th Anniversary of ‘Barcelona’, His New Amazon Series, and the Myth of the Ugly Expat |Michael Weiss |August 10, 2014 |DAILY BEAST 

The performances that shook Kansas City's underground culture decades ago still continues to fascinate onlookers today. Private Birthday Party: A Look at Kansas City’s Long Lost Drag Queens |Erin Cunningham |April 9, 2014 |DAILY BEAST 

The region continues to fascinate me and the number of interesting producers keeps growing and growing. What to Drink in 2014: 13 Chefs and Critics Picks |Jordan Salcito |January 11, 2014 |DAILY BEAST 

“Partial disclosures of male closeness fascinate me,” he says. Speed Read: 13 Juiciest Bits From Morrissey’s ‘Autobiography’ |Nico Hines |October 17, 2013 |DAILY BEAST 

What does fascinate Jünger, and about which he has the most interesting things to say, is the issue of physical courage. David's Book Club: Storm of Steel |David Frum |July 18, 2012 |DAILY BEAST 

This man was indubitably clever, and to a less educated eye than Gwynne's his face would appeal and fascinate. Ancestors |Gertrude Atherton 

Och, and the girls whose poor hearts you deracinate, Whirl and bewilder and flutter and fascinate! The Book of Humorous Verse |Various 

The picture seemed to fascinate her as though it were the reflection of some stranger. The Beach of Dreams |H. De Vere Stacpoole 

Brave as the young man was, he could not support the cold and sea-green eye of the hideous bird, which appeared to fascinate him. The Flying Horseman |Gustave Aimard 

You would fascinate the great ones of the earth, and they would tell you tales of State that would help the great cause. Polly the Pagan |Isabel Anderson