The depressing results come not during a time of sharply lower aggregate venture capital results, notably. Funding for female founders falls to 2017 levels as pandemic shakes up the VC market |Alex Wilhelm |October 9, 2020 |TechCrunch 

Still, McConaghy brings verve and a sense of adventure to an otherwise depressing tale of a doomed dream. 3 New Novels Show a Natural World in Peril |Erin Berger |October 3, 2020 |Outside Online 

That’s unlikely to change any time soon, depressing, very likely, the profits for Big Oil for the foreseeable future. Investors look to buck a four-week losing streak, sending global stocks higher |Bernhard Warner |September 28, 2020 |Fortune 

I don’t care if Monday’s blueTuesday’s grey and Wednesday tooThursday, I don’t care about youIt’s Friday, I’m in loveThe news of late has ranged from dreary and depressing to downright alarming. Believe it or not, the house drone is not Amazon’s most interesting new feature |Aaron Pressman |September 25, 2020 |Fortune 

In the beginning, I might be sometimes not really wanting to look at news because I’m like, “Oh, this is so depressing.” Reasons to Be Cheerful (Ep. 417) |Stephen J. Dubner |May 7, 2020 |Freakonomics 

I remember that after the movie, people were saying how depressing it was, and I started an argument with them. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Judy, as depressing as she sounds in this song, just wants your holiday season to be happy. The Most Confusing Christmas Music Lyrics Explained (VIDEO) |Kevin Fallon |December 24, 2014 |DAILY BEAST 

Depressing is really what Cuba has become—repression, bureaucracy, and crippling poverty. The Five Best Books on Cuba |William O’Connor, Malcolm Jones |December 17, 2014 |DAILY BEAST 

They logged every incident and released depressing day-by-day accounts of the carnage. ISIS Fighters Are Killing Faster than Statisticians Can Count |Peter Schwartzstein |December 5, 2014 |DAILY BEAST 

Surely, for anyone with a vested interest in science, reason, and the idea of secular politics, this is deeply depressing news. Extreme Weather? Blame the End Times |Jay Michaelson |November 28, 2014 |DAILY BEAST 

It was depressing to think of going to bed in such circumstances with the yelling of an Arctic storm for a lullaby. The Giant of the North |R.M. Ballantyne 

In the French Mustel reed organ the first touch is operated by depressing the keys about a sixteenth part of an inch. The Recent Revolution in Organ Building |George Laing Miller 

Peter Ivanovich Jurgenson was born at Reval in 1836, and his childhood was spent in very poor and depressing circumstances. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

The severe atmosphere of that sombre apartment, wherein sat the chief of the police of the Republic, was depressing. The Doctor of Pimlico |William Le Queux 

The seriousness of the situation assumed an even more depressing aspect. The Rival Campers |Ruel Perley Smith