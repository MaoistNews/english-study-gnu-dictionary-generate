While many loved the elegant art piece, made all the more sublime by the artist’s invisibility, others condemned the incursion of any man-made object in pristine desert. Who's Really to Blame for the Monolith Shitshow |Mark Sundeen |December 4, 2020 |Outside Online 

On the other hand, single conifers have a capacity to dazzle as sublime specimens or to drag a landscape down. Winter wonders: Conifers revive the sleeping garden. But remember, less is more. |Adrian Higgins |December 2, 2020 |Washington Post 

That made the ice in the mix sublime — move directly from a solid to a gas. ‘Frozen smoke’ could protect electronics from annoying static |Sid Perkins |October 28, 2020 |Science News For Students 

First, there’s the sublime look of joy on Luke’s face as Jess says, “I was attacked by a swan.” Every episode of Gilmore Girls, ranked |Constance Grady |October 6, 2020 |Vox 

The essential kindness and sublime silliness of one terrific franchise. The essential kindness of Bill and Ted |Alissa Wilkinson |August 28, 2020 |Vox 

From this louche improbable source pours music of sublime beauty without one false note. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

“I have thought that no incident in the life of Jackson was more truly sublime than this,” wrote Hill. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

The decision brings the Military more in line with its veterans who have seen combat and found nothing sublime in war. There Are Only Atheists in Fox Holes |Michael Carson |October 5, 2014 |DAILY BEAST 

“It seems like volunteers for ISIS are surfing for the sublime,” Atran wrote to me on Sunday. ISIS, Hip-Hop Jihadists and the Man Who Killed James Foley |Christopher Dickey |August 25, 2014 |DAILY BEAST 

We did it by knowing that the writing is really sublime and just opening yourself to it. Inside Orange Is the New Black’s Terrifying Showdown Between Red and Vee |Kevin Fallon |June 25, 2014 |DAILY BEAST 

On such occasions his unfaltering impudence reached heights truly sublime. The Joyous Adventures of Aristide Pujol |William J. Locke 

Byron wrote dashingly about 'sublime Tobacco,' but I do not think he carried the practice to excess. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

To fall from the sublime to the ridiculous is especially awkward, and results in becoming very particularly ridiculous yourself. Violins and Violin Makers |Joseph Pearce 

It was thronged with motorists who generally dashed along in sublime disregard of the speed limits. British Highways And Byways From A Motor Car |Thomas D. Murphy 

It is to him that we are indebted for all knowledge of the sublime scenes enacted at the last supper of the Girondists. Madame Roland, Makers of History |John S. C. Abbott