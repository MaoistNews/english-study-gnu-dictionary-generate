The advertisements also seemed to target women and working mothers with fat-free snacks to be eaten at work, on the go, and while “kickin’ back.” The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

You cannot think different in a nation where you cannot breathe free. Trump’s most popular YouTube ad is a stew of manipulated video |Glenn Kessler, Meg Kelly |September 17, 2020 |Washington Post 

So, in some sense, the Swedes can have this very harmonious redistributive society because they’re free-riding off the cutthroat society. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

Singapore’s government launched a similar initiative in 2019 when it partnered with Fitbit to provide Singapore residents with free Fitbit fitness trackers if they bought a premium subscription to the company’s coaching program. One country is now paying citizens to exercise with their Apple Watch |Naomi Xu Elegant |September 16, 2020 |Fortune 

Time’s Up, an organization that advocates for harassment-free workplaces for women, denounced the host’s use of the phrase. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

In other words, the free thinker defending freedom of thought. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Tend to your own garden, to quote the great sage of free speech, Voltaire, and invite people to follow your example. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

The simple, awful truth is that free speech has never been particularly popular in America. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

Cambodia, with its seemingly free press, is also a haven for foreign journalists. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 

In other words, the free speech exhibited by the folks at Charlie Hebdo was not virtuous—until there was a body count. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

What need to look to right or left when you are swallowing up free mile after mile of dizzying road? The Joyous Adventures of Aristide Pujol |William J. Locke 

It seemed to free her of a responsibility which she had blindly assumed and for which Fate had not fitted her. The Awakening and Selected Short Stories |Kate Chopin 

If we can free this State of Yankees, we will accomplish more than your armies down south have. The Courier of the Ozarks |Byron A. Dunn 

The voice of the orator peculiarly should be free from studied effects, and responsive to motive. Expressive Voice Culture |Jessie Eldridge Southwick 

Above all, he was amazed to hear me talk of a mercenary standing army in the midst of peace and among a free people. Gulliver's Travels |Jonathan Swift