The Nets agreed to send Caris LeVert, Rodions Kurucs, first-round picks in 2022, 2024 and 2026 and swaps in 2021, 2023, 2025 and 2027 to Houston. James Harden is a winner and a loser of the Nets’ blockbuster trade |Ben Golliver |January 14, 2021 |Washington Post 

The Nets will send Caris LeVert, Rodion Kurucs, first-round picks in 2022, 2024 and 2026 and swaps in 2021, 2023, 2025 and 2027 to Houston. James Harden deal shows Nets are all in, both for a title and as a personality experiment |Ben Golliver |January 14, 2021 |Washington Post 

That way, if the vaccine virus was able to ditch that modification by way of a swap, it would lose this necessary string of genetic letters too, and die out. A new polio vaccine joins the fight to vanquish the paralyzing disease |Aimee Cunningham |January 8, 2021 |Science News 

However, the monetary value of the shareholder’s stock should remain roughly equal before and after the swap. The NYSE is set to delist 3 Chinese companies—but U.S. investors can still own shares |eamonbarrett |January 4, 2021 |Fortune 

Seed swaps and projects like Open Source Ecology encourage people to grow, build, and print the world they want to see around them. Solarpunk Is Growing a Gorgeous New World in the Cracks of the Old One |Carin Ism |September 6, 2020 |Singularity Hub 

Asprey was inspired to swap out butter for traditional coffee creamers while on a trip to Nepal. Bulletproof Coffee and the Case for Butter as a Health Food |DailyBurn |December 27, 2014 |DAILY BEAST 

The U.S. got a relatively good deal—three Cuban spies were returned to their country in the swap. Up To Speed: The Cuba Embargo |Nina Strochlic |December 18, 2014 |DAILY BEAST 

Swap out that blond with the A-cup for a busty redhead in an instant. Welcome to Oculus XXX: In-Your-Face 3D is the Future of Porn |Aurora Snow |October 18, 2014 |DAILY BEAST 

A webcam girl could offer a “hands on” interactive session without ever having to leave the bedroom or swap bodily fluids. Welcome to Oculus XXX: In-Your-Face 3D is the Future of Porn |Aurora Snow |October 18, 2014 |DAILY BEAST 

Eventually, Kiev agreed to swap Kulygina for 17 captured officials, including Budik. A Torture Survivor on Ukraine's Tortured Ceasefire |Anna Nemtsova |September 11, 2014 |DAILY BEAST 

If the Turks are given time to swap troops in the middle of fighting, we can't really tell how we stand. Gallipoli Diary, Volume I |Ian Hamilton 

You never knowed a sound dodge around so, and swap places so quick and so much. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 

It wasn't a month before your little local stockholders began to get together and swap stock and sell it. The Wreckers |Francis Lynde 

It was a great treat to me to be able to “swap lies” with so many people, after two months almost wholly alone! Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury 

I'd swap all my education in a minute for the mighty body and the healthy and lusty living that you enjoy. The Iron Puddler |James J. Davis