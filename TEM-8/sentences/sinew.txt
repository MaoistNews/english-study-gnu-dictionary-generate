Its frame is still thick with sinew, and there’s some kind of membrane around its chest cavity, which lights up. Huge skeletons are just part of how we live now |Maura Judkis |October 25, 2021 |Washington Post 

But I personally started to feel disrespected, that that caused me—because of my heart, my sinew—to overreact. Rick Sanchez Licks His Wounds |Adam Hanft |January 9, 2011 |DAILY BEAST 

The butcher sawed excruciatingly slowly through bone and sinew. From Bullets to Ballet |Sebastian Rich |October 16, 2010 |DAILY BEAST 

For I knew that thou art stubborn, and thy neck is as an iron sinew, and thy forehead as brass. The Bible, Douay-Rheims Version |Various 

These arrow-heads have generally a shoulder where the arrow was set into the shaft, there to be bound tightly with sinew or fiber. The Wonder Book of Knowledge |Various 

It is the nerve that accompanies the sinew, and Howell Gruffydd now receives and despatches telegrams. Mushroom Town |Oliver Onions 

The diaphragm is peculiar in that it is somewhat circular in shape and is more or less tendinous or sinew-like in the middle. Voice Production in Singing and Speaking |Wesley Mills 

She roused all her energies; strained every sinew, and put forth all her remaining strength. Rookwood |William Harrison Ainsworth