We implore parents and caretakers to get vaccinated themselves to protect vulnerable children from infection. Kids Can't Get COVID-19 Vaccines Yet. But We Do Have Ways to Protect Them |Lael M. Yonker and Anthony J. Fischer |August 17, 2021 |Time 

When she awoke, she rushed to the computer and implored him in an email to respond right away. Ruth Pearl, seeker of justice for her murdered journalist son, Daniel, dies at 85 |Emily Langer |July 30, 2021 |Washington Post 

I sent him a missive, imploring him to brave this perilous journey with me. I Went Camping as My Dungeons & Dragons Character |smurguia |July 15, 2021 |Outside Online 

He calls another, imploring him to act now, because he can’t imagine the Ohtani will last long. On the hunt for his next sports-card score |Kent Babb |July 14, 2021 |Washington Post 

In her letter, Finkelstein implored the organization for an increase in screening and access to at-home fetal monitoring. What umbilical cord tests can and can’t tell us about stillbirths |Purbita Saha |July 8, 2021 |Popular-Science 

Probably his actual words from the Bible, which implore Christians to care for the poor. Just Imagine Hologram Jesus Telling Paul Ryan and Pat Robertson Off |Dean Obeidallah |May 24, 2014 |DAILY BEAST 

We know the risk he and his wife are taking for all of us, and we implore you, O good and great God, to keep him safe. How to Pray in the Public Square |Gene Robinson |May 11, 2014 |DAILY BEAST 

Dear fellow Jews—I implore you: Please, please stop trying to make people shut up. Dear Jews: Stop Trying to Make People Shut Up |Emily L. Hauser |December 9, 2013 |DAILY BEAST 

Now, as they enter the fourth estate and seek its reinvention, I implore them: be bold. A Challenge to New Media Moguls Pierre Omidyar and Jeff Bezos |Alexander Busansky |November 4, 2013 |DAILY BEAST 

If you are reading this, Bill Keller's psychiatrist, I implore you to work this through with him. Message to Bill Keller's Psychiatrist |Michael Tomasky |May 22, 2013 |DAILY BEAST 

Were you a free man at this moment, and went down on your knees to implore me to give you Anne, I would not do it. Elster's Folly |Mrs. Henry Wood 

"I implore you, yield to these wise words," now put in the archdeacon addressing Gaudry. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 

If I loved you, it would be entirely different; so once more, and for the last time, I implore you to let us part as friends.' The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 

Therefore when we hear the sound of the cornet we should implore God to rebuild the Temple. Hebraic Literature; Translations from the Talmud, Midrashim and Kabbala |Various 

O you who are quickly ready to help, I implore you for wealth whereby we may overshadow all men, like the sky. Sacred Books of the East |Various