Today, more than two decades after I arrived here in that rental van, this allegiance manifests as knee-jerk defensiveness when others take shots at California. In defense of California |James Temple |September 4, 2020 |MIT Technology Review 

In a container, liquid can be levitated over a layer of gas by shaking the container up and down because the repeated, upward jerking motion keeps fluid from dripping into the air below. Toy boats float upside down underneath a layer of levitated liquid |Maria Temming |September 2, 2020 |Science News 

That would be a heartbreaking experience for anyone to go through, and I’m so sorry that this jerk betrayed your trust like that. Social Media Feels Increasingly Toxic. What Do I Do? |Blair Braverman |August 29, 2020 |Outside Online 

Make the jerk bleed for your bike and they’re far less likely to bother at all. The most secure ways to lock up your bike |By Michael Frank/Cycle Volta |August 26, 2020 |Popular-Science 

Bill Gates was widely considered to come off like an evasive jerk in his testimony at the time … Which means he is now perfectly positioned to help Jeff Bezos learn from his mistakes in the Amazon titan’s first appearance before Congress. Advice for Jeff Bezos on testifying before Congress from me, the totally real Bill Gates |Sarah Todd |July 27, 2020 |Quartz 

You write a lot about how you were a jerk or a snob when it came to comedy or film. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

Emetophobia tends to compromise my relationships, turning me into a selfish jerk. Why My Norovirus Panic Makes Me Sick |Lizzie Crocker |January 5, 2015 |DAILY BEAST 

What they found was that most people preferred to work with the lovable fool rather than the competent jerk. The Case Against In-Your-Face Atheism |Steve Neumann |January 4, 2015 |DAILY BEAST 

“Either this or stay home and jerk off,” said one guy when I asked why he came tonight. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

It is not a knee-jerk response to a sudden perceived threat. To Stop ISIS, Britain Is Set to Stop Free Speech |Nico Hines |November 25, 2014 |DAILY BEAST 

A twist, a sudden jerk, and it was Black Hood who had the signal device now. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Here the little dogs sit and bark and jerk, ready to dodge into their hole in a moment. The Nursery, August 1873, Vol. XIV. No. 2 |Various 

It made the skin jerk and pull as if he were trying to get rid of an itch without using his hand. Hooded Detective, Volume III No. 2, January, 1942 |Various 

He said nothing, however, but he went over and gave the bell cord a violent jerk. The Amazing Interlude |Mary Roberts Rinehart 

When you tug a boat, you must not jerk at the rope but pull it gently, so I urged Kari to pull it smoothly. Kari the Elephant |Dhan Gopal Mukerji