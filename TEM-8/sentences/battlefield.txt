Treating PTSD requires re-creating battlefield aromas, for example, while firefighters might train in smoke-scented simulations. Aromas can evoke beloved journeys — or voyages not yet taken |Jen Rose Smith |February 11, 2021 |Washington Post 

Especially on the battlefield, that kind of thing is just absolutely bread and butter. How Gideon the Ninth author Tamsyn Muir queers the space opera |Constance Grady |February 5, 2021 |Vox 

In other words, our immune response depends on how many soldiers the vaccine brings to the battlefield. Will the vaccines work against the South African variant of COVID-19? |Daniel Bentley |January 8, 2021 |Fortune 

MacMillan synthesizes a vast body of literature about war, from battlefield accounts to theories of war, and she shows how new technologies and weaponry have repeatedly changed the course of history. Humans Have Gotten Nicer and Better at Making War - Issue 94: Evolving |Steve Paulson |January 6, 2021 |Nautilus 

The Park Service also protects a large number of historic battlefields, historic sites, and military parks, leaving no shortage of places where you can explore and pay tribute on this day. Every Day that the National Parks Are Free in 2021 |Emily Pennington |December 30, 2020 |Outside Online 

So however detailed the statistics of the battlefield are, they cannot achieve the goal. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

The “tactical” side of artillery—its use on a battlefield—was something Jackson was not called upon to explain. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

For 50 years, this city that once graced postcards has been a battlefield. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

Despite its recent gains on the battlefield, the fight against rebel brigades has taken a significant toll on the government army. Local Truces Are Syria’s Sad Little Pieces of Peace |Joshua Hersh |November 18, 2014 |DAILY BEAST 

By one account, it aided U.S. troops in capturing or killing at least ten of those senior leaders from the battlefield. How the NSA Became a Killing Machine |Shane Harris |November 9, 2014 |DAILY BEAST 

Rapidity of action and a self-confidence which on the battlefield never felt itself beaten were the cause of Murat's success. Napoleon's Marshals |R. P. Dunn-Pattison 

Pultusk and Eylau bore witness to his bravery and address on the battlefield, and Napoleon began to relent. Napoleon's Marshals |R. P. Dunn-Pattison 

The first thought of a soldier as he sinks dying or wounded on the battlefield is of home and the loved ones. The Courier of the Ozarks |Byron A. Dunn 

The town itself was used as a battlefield and many of the individual houses were completely destroyed. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Because of its recumbent position, symbolic of General Lee resting on a battlefield cot, this statue is considered most unique. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey