Nestled in the foothills of the Sandia Mountains, the city’s skyline to the east is framed by 10,000-foot peaks. 72 Hours in Albuquerque |kklein |July 6, 2021 |Outside Online 

In the 1960s, American architects were preoccupied with erecting iconic buildings along city skylines. The legacy of “inside-out” office design |Heather Landy |May 19, 2021 |Quartz 

Juul’s June 2015 launch party was held at Jack Studios, a giant industrial loft space in Manhattan often used for fashion photo shoots, with striking views of the city skyline and the Hudson River below. How Juul Got Vaporized |Jamie Ducharme |May 17, 2021 |Time 

Filling those offices with paying tenants would be necessary to keep the new skyline from becoming a symbol of failure. The Empire State Building Opened During the Great Depression. Its Survival Story Holds a Lesson for Today |Olivia B. Waxman |April 30, 2021 |Time 

Artist Stephen Wiltshire is famous for his panoramic skyline views, which he draws entirely from memory. Do You Remember? |Sean Culligan |April 13, 2021 |Ozy 

Chicago retains its brilliant skyline, great cultural institutions, powerful political influence, and a strong business community. Battle of the Upstarts: Houston vs. San Francisco Bay |Joel Kotkin |October 5, 2014 |DAILY BEAST 

The absence of the Twin Towers in the skyline was jarring, as was the sight of tanks and humvees posted along Park Row. The Resilient City: New York After 9/11 |John Avlon |September 11, 2014 |DAILY BEAST 

In the distance the fractured skyline of the city stood out like shards of glass. Real Life Lazarus: When Patients Rise From the Dead |Sandeep Jauhar |August 21, 2014 |DAILY BEAST 

Day and night, the downtown skyline is gorgeous to gaze at from ground level. It’s Tampa Time This Summer |Ray Roa |July 10, 2014 |DAILY BEAST 

Now I was content to sit next to Dad on his bed and look out the window at the orange light bouncing off the New York skyline. Almost Famous: A Father's Day Story |Alex Belth |June 15, 2014 |DAILY BEAST 

Only, he argued, British soldiers would stand against the skyline during a general action. Gallipoli Diary, Volume I |Ian Hamilton 

The last thing—against the skyline—a little column of French soldiers of the line charging back upwards towards the lost redoubt. Gallipoli Diary, Volume I |Ian Hamilton 

Matt studied the skyline in advance, wondering how far away the two ruffians were and what their designs could be. Motor Matt's "Century" Run |Stanley R. Matthews 

When the last of them had passed I observed with great alarm a thinning out of the darkness along the eastern skyline. A Virginia Scout |Hugh Pendexter 

Beyond rose tier on tier of hills, ending on the skyline in snow-clad mountain peaks. In the Ranks of the C.I.V. |Erskine Childers