Divers exploring an underwater portion of the lake’s K’akaya reef found a ritual offering deposited by the Inca, say archaeologists Christophe Delaere of the University of Oxford and José Capriles of Penn State. A submerged Inca offering hints at Lake Titicaca’s sacred role |Bruce Bower |August 3, 2020 |Science News 

Coral reefs provide spots of brilliant color under the waves. Let’s learn about coral reefs |Bethany Brookshire |July 22, 2020 |Science News For Students 

A bleached reef can spell trouble for the many species that call it home. Let’s learn about coral reefs |Bethany Brookshire |July 22, 2020 |Science News For Students 

Scientists are working to figure out how to help bleached corals and plant new coral reefs. Let’s learn about coral reefs |Bethany Brookshire |July 22, 2020 |Science News For Students 

They’re also planting tiny bits of coral in new places, trying to help new reefs get a good start. Let’s learn about coral reefs |Bethany Brookshire |July 22, 2020 |Science News For Students 

I was out, maybe in the Great Barrier Reef catching black marlin. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

And this in turn affects the fish, whales, dolphins, turtles, dugongs and seabirds that live within the Reef. Australia Wants to Open the Great Barrier Reef to Dumping |Kirsten Alexander |June 2, 2014 |DAILY BEAST 

The Reef has lost more than half its coral cover since 1985, with two-thirds of the loss occurring after 1998. Australia Wants to Open the Great Barrier Reef to Dumping |Kirsten Alexander |June 2, 2014 |DAILY BEAST 

The 7,000-ton freighter is still stocked with sake bottles and four fighter planes, and it is encircled by gray reef. A WWII Battle Frozen in Time |Nina Strochlic |May 14, 2014 |DAILY BEAST 

It also takes place near a minefield of rocks, is a hangout spot for sharks, and breaks on a reef. Now That Everest Is Closed, Check Out These Other Extreme Adventures |Nina Strochlic |May 8, 2014 |DAILY BEAST 

The few birds that frequented the reef were very shy, and flew away at our approach: they were principally pelicans and terns. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

REEF k, in latitude 14 degrees 47 minutes, has a dry sand upon it: its sub-marine extent was not ascertained. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

All the islands are low and wooded, and surrounded by a coral reef of small extent. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

The Mermaid was nearly lost in attempting to cross the latter reef. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

A considerable reef projects off the east end for more than a mile. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King