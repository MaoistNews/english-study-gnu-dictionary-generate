High temperatures may once again stay below freezing, in the upper 20s to low 30s. D.C.-area forecast: Raw today. Iciness threat grows Saturday into early Sunday. |A. Camden Walker |February 12, 2021 |Washington Post 

Temperatures tonight reach the low 20s well north to the upper 20s well south. PM Update: Light snow mainly passes south of us tonight; cloudy and cold Friday |Ian Livingston |February 12, 2021 |Washington Post 

In the upper two thirds of the oven, there’s still a lot of room to play around. Don’t Fear the Broiler |Elazar Sontag |February 11, 2021 |Eater 

Temperatures still struggle to warm much, with highs in the upper 30s. D.C.-area forecast: Snow and wintry mix taper off this morning, while ice concerns mount for Saturday |David Streit |February 11, 2021 |Washington Post 

The total amount of heat stored in the upper oceans in 2020 was higher than any other year on record dating back to the 1950s. 2020 was warmest year on record for Earth’s oceans |Maria Temming |February 10, 2021 |Science News For Students 

Kanye refuses to stomach any rejection, no matter how upper crust. Kanye West and Kim Kardashian’s Balmain Campaign: High Fashion Meets Low Culture |Amy Zimmerman |December 23, 2014 |DAILY BEAST 

If only Sulzberger had managed to keep a zipped upper lip while leaving the dirty work to anonymous underlings. The Bloodiest Media Coups of 2014 |Lloyd Grove |December 22, 2014 |DAILY BEAST 

White, upper-middle-class, Ivy-League educated white men, however Great they are, are falling out of power. A Few Great Men Too Many: Aaron Sorkin Doesn’t Think You Can Handle the Truth |Arthur Chu |December 21, 2014 |DAILY BEAST 

When I came to New York after college I was taken with the world of Jewish intellectuals and the Upper West Side. Meghan Daum On Tackling The Unspeakable Parts Of Life |David Yaffe |December 6, 2014 |DAILY BEAST 

Sure, the Red Coats had the upper hand in terms of transportation, supplies and training. The British Royals Reinvade Brooklyn: William and Kate Come Watch Basketball on Historic Battle Site |Justin Jones |December 6, 2014 |DAILY BEAST 

“Perhaps you do not speak my language,” she said in Urdu, the tongue most frequently heard in Upper India. The Red Year |Louis Tracy 

On the upper part of the stem the whorls are very close together, but they are more widely separated at the lower portion. How to Know the Ferns |S. Leonard Bastin 

It is to be remembered, however, that a few of these bacteria may reach the sputum from the upper air-passages. A Manual of Clinical Diagnosis |James Campbell Todd 

The upper part of the stem is usually unbranched, but whorls of branches occur towards the base. How to Know the Ferns |S. Leonard Bastin 

The eyebrows were low and thick, the upper lip was sensitive, quivering sometimes as she talked, but the lower was firm and full. Ancestors |Gertrude Atherton