In the dens where dragons dwelt before, shall rise up the verdure of the reed and the bulrush. The Bible, Douay-Rheims Version |Various 

The verdure was everywhere astonishing, and we fancied we saw many golden Cuyps as we passed by these quiet pastures. Little Travels and Roadside Sketches |William Makepeace Thackeray 

On the other side was a narrow margin, and then a sheer wall of hills in exquisite verdure. Mrs. Falchion, Complete |Gilbert Parker 

Along the banks of the Loire, France has meadows, the verdure of which will not sink in comparison with those of England. Travels through the South of France and the Interior of Provinces of Provence and Languedoc in the Years 1807 and 1808 |Lt-Col. Pinkney 

Only here and there, separated by vast intervals of barrenness, could be seen minute streaks of verdure. Overland |John William De Forest