She was brave and defiant, but also funny and stubborn like the time she accepted Dad’s challenge to quit smoking by listening to the hypnosis tapes he had given her for her birthday. The Memories That Sustain Me on Mother's Day |Nadja Halilbegovich |May 5, 2022 |Time 

Like meditation practice, many people are capable of doing hypnosis on their own, Spiegel says. How Hypnosis Works, According to Science |Eleanor Cummins |April 28, 2022 |Time 

Faubion notes that hypnosis and cognitive behavioral therapy have both been shown to be promising, probably because they reduce the anxiety associated with some menopause symptoms. To Ease Menopause Symptoms, Add a Little Melody |Galadriel Watson |February 15, 2022 |Time 

Collin had another plan — she made a virtual appointment with her therapist, scheduled a hypnosis session and deleted Twitter from her phone. Managing stress: Company leaders face pressure, burnout just as employees do |Jessica Davies |July 12, 2021 |Digiday 

Using research from cognitive neuropsychology and hypnosis, our recent paper argues in favor of the latter position, even though this seems to undermine the compelling sense of authorship we have over our consciousness. Is It Time to Give Up on Consciousness as ‘the Ghost in the Machine’? |Peter Halligan |June 17, 2021 |Singularity Hub 

In a deposition given under hypnosis two years later, he remembered that he “woke up, saw my death, and looked around.” The Stacks: The Judas Priest Teen Suicide Trial |Ivan Solotaroff |June 28, 2014 |DAILY BEAST 

In addition, Bleckwenn used hypnosis to try to achieve a similar state. Would Truth Serum Work on James Holmes in the Aurora Shooting Trial? |Kent Sepkowitz |March 14, 2013 |DAILY BEAST 

I believe that creativity requires a form of auto-hypnosis in order to work. Justin Cronin: How I Write |Noah Charney |October 10, 2012 |DAILY BEAST 

It was enough to feel, as one model came down the runway after another, a state of hypnosis coming on. Marc Jacobs' Spring Summer 2013 Show: Walk The Line |Isabel Wilkinson |September 12, 2012 |DAILY BEAST 

The government declined her offer to undergo hypnosis to see if she could recall any additional information. Did McVeigh Have Another Accomplice? |Gerald Posner |October 4, 2009 |DAILY BEAST 

Originally, he endeavoured to reawaken the memory of the sexual trauma by means of the induction of profound hypnosis. The Sexual Life of the Child |Albert Moll 

What is meant by rapport in the group may be illustrated by a somewhat similar phenomenon which occurs in hypnosis. Introduction to the Science of Sociology |Robert E. Park 

It must be traced in the literature of automatisms, hypnosis, divided personality, and the "subliminal." The Letters of William James, Vol. 1 |William James 

Repeated advertising of a tooth brush or a box of crackers is mild mental suggestion—hypnosis, if you will. Astounding Stories of Super-Science, November, 1930 |Various 

"Very well," I answered, feeling myself in profound hypnosis. Amazing Grace |Kate Trimble Sharber