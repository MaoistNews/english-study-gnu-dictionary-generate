In actuality, the software was using generative adversarial networks, the algorithm behind deepfakes, to swap the women’s clothes for highly realistic nude bodies. A deepfake bot is being used to “undress” underage girls |Karen Hao |October 20, 2020 |MIT Technology Review 

Vargas said she believes the board did its due diligence when it came to a professor who kept a trove of nude photos and sex videos with students on his work computer. Morning Report: The Height Limit Issue No One’s Talking About |Voice of San Diego |October 20, 2020 |Voice of San Diego 

Blue jeans originated as durable workwear for miners, but a good deal of runway fashion performs only the bare-minimum job of keeping the wearer from being nude—and occasionally not even that. Fashion is struggling to rise to the creative challenge of Covid-19 |Marc Bain |October 14, 2020 |Quartz 

It’s a weird sort of victim blaming that suggests that if someone chooses such a “superficial” profession—and especially if they pose for nude or provocative photos—they deserve whatever they get. Emily Ratajkowski and the question of why society looks down on models |kristenlbellstrom |September 17, 2020 |Fortune 

She has, Lane writes, a moose head who “once saw Jack Nicholson nude.” ‘A Star is Bored’ a delicious work of fiction |Kathi Wolfe |August 13, 2020 |Washington Blade 

The Oscar-winning actress put nude photo thieves in their place with one perfect statement. Jennifer Lawrence’s Righteous Fury Says Everything We Wanted to Say |Kevin O’Keeffe |December 29, 2014 |DAILY BEAST 

In November 2002, a detainee who had been held partially nude and chained to the floor died, apparently from hypothermia. The Most Gruesome Moments in the CIA ‘Torture Report’ |Shane Harris, Tim Mak |December 9, 2014 |DAILY BEAST 

Nude prisoners were kept in a central area, and walked around as a form of humiliation. Inside the CIA’s Sadistic Dungeon |Tim Mak |December 9, 2014 |DAILY BEAST 

Actually, Brown lost the Senate race to Democrat incumbent Jean Shaheen because Scott once posed nude for Cosmo. The GOP Senate: A New Utopia Dawns |P. J. O’Rourke |November 8, 2014 |DAILY BEAST 

There's a scene in which a nude Amy Elliott-Dunne, played with committed gusto by Rosamund Pike, is washing off in the shower. Yes, Ben Affleck Goes Full-Frontal in ‘Gone Girl,’ Confronting One of Cinema’s Last Taboos |Marlow Stern |October 1, 2014 |DAILY BEAST 

I stood up, for the exciting near-nude body of the woman who had caused Nokomee's outburst was too close, too intimately relaxed. Valley of the Croen |Lee Tarbell 

Onto this preparation the studies drawn from the nude model are "squared up," and the drawing corrected again from the nude model. The Painter in Oil |Daniel Burleigh Parkhurst 

The prince and I were left alone with the two Jivros, who stood beside the nude figure of the alien Croen. Valley of the Croen |Lee Tarbell 

But to Bierce's mind, "noble and nude and antique," this mid-Victorian draping and bedecking of "unpleasant truths" was abhorrent. The Letters of Ambrose Bierce |Ambrose Bierce 

It is the first nude statue of the Renaissance made for Cosimo de' Medici before his exile. Florence and Northern Tuscany with Genoa |Edward Hutton