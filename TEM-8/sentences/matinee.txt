In the old days of fame, matinee-idol actors and pin-up picture actresses may have been dim bulbs. Welcome to Showbiz Sharia Law |P. J. O’Rourke |May 4, 2014 |DAILY BEAST 

The crowd at the matinee showing I went to of Of Mice and Men was comprised of many young kids—mostly female. James Franco Uncensored: The Actor on Broadway, NYT Hate, and That Half-Naked Instagram |Marlow Stern |May 4, 2014 |DAILY BEAST 

At this point, Franco says an abrupt “goodbye”  and rushes off to the theater for a matinee performance. James Franco Uncensored: The Actor on Broadway, NYT Hate, and That Half-Naked Instagram |Marlow Stern |May 4, 2014 |DAILY BEAST 

While the humans are entertaining enough in this Provencal matinee, the bird is the star. One Perfect Summer Day in Virginia Woolf, Saul Bellow and Others |Matt Seidel |September 25, 2013 |DAILY BEAST 

So we did a matinee Sunday afternoon and then went straight from the matinee to the awards. John Gallagher Jr., Star of ‘Newsroom’ and ‘Short Term 12,’ Is Hollywood’s Nicest Guy |Kevin Fallon |August 23, 2013 |DAILY BEAST 

There was still money in her purse, and her next temptation presented itself in the shape of a matinee poster. The Awakening and Selected Short Stories |Kate Chopin 

From him I learned that the occasion was neither a full-dress ball nor a chance gathering of a jour fixe, but a musical matinee. Frederick Chopin as a Man and Musician |Frederick Niecks 

Franchomme, whom I questioned about the matinee at the Marquis de Custine's, had no recollection of it. Frederick Chopin as a Man and Musician |Frederick Niecks 

You see mamma and I met Judith Blount one afternoon at a matinee just before college opened. Molly Brown's Sophomore Days |Nell Speed 

Not long after this matinee experience—perhaps a month—Mrs. Vance invited Carrie to an evening at the theatre with them. Sister Carrie |Theodore Dreiser