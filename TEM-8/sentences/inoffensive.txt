He is as submissive as a truck, as inoffensive as a bulldozer, as gentle as a power-driven lawnmower. Tarzan Wasn’t for Her - Issue 100: Outsiders |Erika Lorraine Milam |June 2, 2021 |Nautilus 

Not terrible, not brilliant, not good, but amiable, uneventful, inoffensive. Yes, ‘Looking’ Is Boring. It’s the Drama Gays Deserve. |Tim Teeman |January 24, 2014 |DAILY BEAST 

It was a short, inoffensive video message which simply stated “thanks for the follow!” Snapchatting With Senator Rand Paul |Gideon Resnick |January 17, 2014 |DAILY BEAST 

Is the word Redskin somehow uniquely inoffensive among racial and ethnic slurs? The ‘Tennessee Crackers’ Would Have Never Happened |Michael Tomasky |November 30, 2013 |DAILY BEAST 

In other words, most of these women were not bland, inoffensive “sweater gays”; they were RuPaul. Media Ignores Rash of Assaults on Transgender Women |Jay Michaelson |June 6, 2012 |DAILY BEAST 

If you want your singers grateful, bland, and forever inoffensive, this is not the show for you. Why 'X Factor' Trounces 'American Idol' |Richard Rushfield |December 1, 2011 |DAILY BEAST 

Their discipline is admirable, but their natural disposition is likewise quiet and inoffensive. Glances at Europe |Horace Greeley 

The city hell hounds sprang to meet them and the slaughter of inoffensive Europeans began in Darya Gunj. The Red Year |Louis Tracy 

Margaret laughed outright and her laughter was so inoffensive and so musical that the Chief Inspector laughed also. Dope |Sax Rohmer 

Mr. Bailey was a quiet, inoffensive man, very free with his money to everybody around. The Doctor of Pimlico |William Le Queux 

You should never venture on the most innocent and inoffensive joke, unless it be with people of culture or intelligence. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre