It measures 71-feet long, and its propulsion is provided by three GE -made J85-15 engines that together provide 12,000 lbs of thrust. Here’s your first look at Boom Supersonic’s faster-than-sound XB-1 demonstrator aircraft |Darrell Etherington |October 7, 2020 |TechCrunch 

Both move air to create thrust for the plane, but turboprops are more efficient at lower speeds because the propeller moves more air with a smaller turbine. Airbus Just Unveiled Three New Zero-Emission Concept Aircraft |Vanessa Bates Ramirez |September 23, 2020 |Singularity Hub 

The middle portion of the blade is the sweet spot, generating enough excess thrust to keep the rotor spinning against the drags of the tip and the root. The science behind how an aircraft glides |By Peter Garrison/Flying Mag |September 3, 2020 |Popular-Science 

It would be more precise, however, to say that only part of each blade glides because only part of each blade has the right combination of speed and angle of attack to achieve the proper balance between thrust and drag. The science behind how an aircraft glides |By Peter Garrison/Flying Mag |September 3, 2020 |Popular-Science 

It’s a large, stable kayak designed as a platform for a saltwater-grade through-hull Minn Kota GPS-enabled trolling motor with 45 pounds of thrust. This motorized kayak can drive itself |By Nate Matthews/Outdoor Life |August 28, 2020 |Popular-Science 

But with the outbreak of hostilities in mid-2011, all festivities were thrust into the deep freeze. In One Corner of Syria, Christmas Spirit Somehow Manages to Survive |Peter Schwartzstein |December 25, 2014 |DAILY BEAST 

The central thrust of the sequence derives from historical fact. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Suddenly, a  tall, curly haired white man appeared and thrust himself in the middle of the opposing forces. Honoring The Late John Doar, A Nearly Forgotten Hero Of The Civil Rights Era |Gary May |November 15, 2014 |DAILY BEAST 

Thrust into a world of seemingly supernatural monsters, his adventure begins. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

Presidents must act at least as much as they react; they must seize the initiative and thrust their enemies on the defensive. This Really Is Obama's Moment of Truth |Jonathan Alter |September 4, 2014 |DAILY BEAST 

He thrust his tiny tuft of beard between his teeth—a trick he had when perplexed or thoughtful. St. Martin's Summer |Rafael Sabatini 

Great was the surprise of Alf at the honour and labour thus thrust upon him, but he did not shrink from it. The Giant of the North |R.M. Ballantyne 

He thrust the Cardinal's mantle into it, and stood over the smouldering cloth, till the whole was consumed to ashes. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

I only saw the glitter of a bayonet which a Mexican thrust into his shoulder, at the very moment he was helping me up. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

I turned round, thrust my purse into the lap of the nearest, and with a light heart led the lady back to the hotel. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various