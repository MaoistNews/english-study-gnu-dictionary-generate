People often start setting goals with a little too much gusto, trying to overhaul many aspects of their life at once. How to Set Goals You’ll Actually Achieve |Amanda Loudin |January 4, 2021 |Time 

The character exudes gusto and sex appeal, and a churning, forward-leaning drive. Zorro at 100: Why the original swashbuckler is still the quintessential American action hero |Michael Sragow |January 1, 2021 |Washington Post 

Ultimately, you may be disgusted with yourself — as I was — for devouring this morally repugnant tale with such gusto, but reading, like eating is a hard activity to regulate once the appetites are aroused. Sure, you may be grossed out, but ‘A Certain Hunger’ is a naughty pleasure |Maureen Corrigan |December 11, 2020 |Washington Post 

Fossil shark teeth got people hooked on the Meg long before paleontology took off in the early 19th century, when scientists started cataloging fossils with gusto. Could an ancient megashark still lurk in the deep seas? |By Riley Black |October 15, 2020 |Popular-Science 

He ignored red flags—like Salazar’s rumored testing of testosterone or the flippancy with which he passed out prescription drugs—with the typical gusto of a new Nike employee. Inside a secret running program at Nike and a win-at-all-costs corporate culture |Rachel King |October 6, 2020 |Fortune 

There's a scene in which a nude Amy Elliott-Dunne, played with committed gusto by Rosamund Pike, is washing off in the shower. Yes, Ben Affleck Goes Full-Frontal in ‘Gone Girl,’ Confronting One of Cinema’s Last Taboos |Marlow Stern |October 1, 2014 |DAILY BEAST 

As admirable as the U.S. fightback against Belgium was the pride and gusto of their fans. Home of the (Footballing) Brave: The U.S. Bested Britain in World Cup Spirit |Emma Woolf |July 7, 2014 |DAILY BEAST 

Of course, the young people on the progressive side of the hall supported my cause with gusto. We Are Radicals at Heart: A New History Gets America Wrong |Harvey J. Kaye |December 5, 2013 |DAILY BEAST 

For her other performance, she also belted “Wrecking Ball” with all the gusto of a young Linda Blair having an exorcism. Miley Cyrus Twerks Out a Stellar ‘Saturday Night Live’ Hosting Stint |Kevin Fallon |October 6, 2013 |DAILY BEAST 

After he said, “go ahead,” she “took a handful and swallowed them with gusto and no dramatics.” Richard Burton’s Sexy Diaries: 13 Juiciest Bits |Kevin Fallon |October 20, 2012 |DAILY BEAST 

Huxley quotes with satirical gusto Dr. Wace's declaration as to the word "Infidel." God and my Neighbour |Robert Blatchford 

Then Mr. Blackbird selected a good many choice tidbits here and there, which he bolted with gusto. The Tale of Grandfather Mole |Arthur Scott Bailey 

Ben Jonson loved the 'durne weed,' and describes its every accident with the gusto of a connoisseur. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The four shook hands solemnly with their new neighbor, then, with even a greater gusto, drank his health. Ancestors |Gertrude Atherton 

He would retail his exciting experiences as a pugilist and a drunkard with much gusto. The Pit Town Coronet, Volume II (of 3) |Charles James Wills