Note: These are only a handful of many aborted could-be classics. Doomed Passion Projects of Hollywood: The Lost Classics of Stanley Kubrick, Alfred Hitchcock, and More |Marlow Stern |March 28, 2014 |DAILY BEAST 

Many men (and women) evaluated Sarah Palin, the could-have-been GOP vice president from Alaska, based on her appearance. Everyday Sexism Creator Laura Bates on Helping Women Speak Out |Anna Klassen |April 9, 2013 |DAILY BEAST 

Is this year's Sundance sales frenzy a direct result of last year's little-movie-that-could? The 'Winter's Bone' Effect |Nicole LaPorte |January 29, 2011 |DAILY BEAST 

Jerry Brown, California's once and could-be future governor, is a master of reinvention, just like the pop icon. The Madonna of Politics |Lloyd Grove |March 15, 2010 |DAILY BEAST 

That ugly fellow who swore at me the day before was in the boat, and I c-could understand him. Ben Comee |M. J. (Michael Joseph) Canavan 

And Glora and Alan—in our present size-could doubtless disembark safely. Astounding Stories, March, 1931 |Various 

If only she could summon him from the moonlight out there; if only she were a witch-could see him, know where he was, what doing! Saint's Progress |John Galsworthy 

He was all innuendo and strange hints and whispered secrets, and I-could-if-I-woulds. Nights |Elizabeth Robins Pennell 

Anthony said you-could hear Emmy's tongue striking the roof of her-mouth all thee time. The Tree of Heaven |May Sinclair