A lot of us are so quick to deify Anthony Fauci, to condemn anyone still wearing a mask, to dismiss other people’s choices as inherently harmful, stupid, and, well, evil. The feud between a weed influencer and scientist over puking stoners |Amanda Chicago Lewis |July 20, 2022 |MIT Technology Review 

A movie that deified him AND gave me wacky cartoons was always going to win over my pea-sized brain. Debating How Space Jam: A New Legacy Stacks Up Against the Original |Cady Lang |July 19, 2021 |Time 

To do so is to deify a celebrity for being what we need them to be, while willfully ignoring who they really are. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

This on a continent where most people nearly deify the Clintons, and almost all American presidents. Hillary's Gutsy Mission |Eliza Griswold |August 7, 2009 |DAILY BEAST 

The latter wished to deify Alexander; but Timaeus exalts Timoleon above the most venerable gods. The Histories of Polybius, Vol. II (of 2) |Polybius 

Not satisfied with three Gods, they must canonize and deify men and make saints and demi-gods. Abraham Lincoln: Was He A Christian? |John B. Remsburg 

It is a ghastly thing to study these religions, and to see what dark and revolting qualities ignorance can deify. The Spanish Pioneers |Charles F. Lummis 

It is not clear, then, how they can 'deify' classes of things, if they have no notion of deity. Social Origins and Primal Law |Andrew Lang 

His choices free or fetter, elevate or debase, deify or demonize his humanity. Tablets |Amos Bronson Alcott