At this early point, the United States could have focused on scaling up testing, encouraged university laboratories and private companies to develop additional diagnostic tests, and broadened its criteria for who could be tested, Adalja says. Trump downplayed the threat from COVID-19. Here’s how we could’ve fought back harder. |Kate Baggaley |September 11, 2020 |Popular-Science 

However, broadened 5G devices and networks alone won’t solve the current consumer need to stay connected. Why the coronavirus pandemic has made 5G more essential than ever |jakemeth |September 7, 2020 |Fortune 

The move seemed mystifying, but Walmart said owning the short-video app could drive e-commerce sales by broadening its third-party marketplace—though it may be more effective at boosting the company’s growing advertising business. America’s largest retailer is taking on Amazon Prime |Marc Bain |September 4, 2020 |Quartz 

Then the legacy magazine brand pivoted to video and broadcast television as a way to not only broaden its audience but deepen the brand deals associated with the franchise. ‘A blueprint for what is going to happen’: Time’s Time 100 franchise on track to double revenue in 2020 |Kayleigh Barber |September 3, 2020 |Digiday 

The company points to more than 300 peer-reviewed or independent studies that support its technologies and techniques, and has worked to broaden access to its solution during the pandemic. 5 companies that want to track your emotions |jakemeth |August 22, 2020 |Fortune 

You broaden your base, you broaden your message, it shows that you really want to get things done. Return of the Northeastern Republican |David Freedlander |November 4, 2014 |DAILY BEAST 

On the other hand, this prize does afford us a chance to broaden our horizons beyond the borders of whatever country we live in. Who the Hell Is Patrick Modiano? |Malcolm Jones |October 9, 2014 |DAILY BEAST 

Johnson urged conservatives to tone down their rhetoric to try to broaden the base. Conservative Senator Kicks Tea Party to the Curb |David Freedlander |May 31, 2014 |DAILY BEAST 

But while Paul may broaden his rhetoric, his outreach is cramped. Who to Blame for Donald Sterling and Cliven Bundy |Robert Shrum |April 30, 2014 |DAILY BEAST 

You could, I suppose, broaden the definition of “runner-up” to include Romney. The Most Annoying Myth in Politics |Jamelle Bouie |February 24, 2014 |DAILY BEAST 

Friendship with such cultured, interesting people would broaden the horizon of my whole life. Mildred's Inheritance |Annie Fellows Johnston 

A humble man himself, he had made all his sacrifices to broaden the chances for his children. The Iron Puddler |James J. Davis 

A foreign business training may broaden a man in some ways, but it leaves his muscles flabby for real home work here in America. Flamsted quarries |Mary E. Waller 

And we have to broaden the field to cover the habitual order of reflective preferences in the community to which we belong. Essays in Experimental Logic |John Dewey 

I merely hope you will broaden your interests enough to include the Red Cross work before it is too late. Ruth Fielding In the Red Cross |Alice B. Emerson