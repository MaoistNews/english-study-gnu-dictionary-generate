Scientists can look for that wobbling, a method called radial velocity measurement. Analyze This: Some 5,000 planets orbit stars other than our sun |Carolyn Wilke |May 10, 2022 |Science News For Students 

Other telescopes—both space-based and Earth-based—use what’s known as the radial-velocity method. Looking for a New Planet? How About 5,000 of Them? |Jeffrey Kluger |March 25, 2022 |Time 

This meant significant progress over measuring the orbiting stars’ velocity in two dimensions by providing the “critical third dimension” of their radial, or 3D, motion. Skywatcher |Anne-Marie Corley, SM ’09 |October 27, 2021 |MIT Technology Review 

The team then tested the rats’ memory by having them swim through a radial arm water maze containing six arms, including one with a hidden platform. Swimming is the ultimate brain exercise. Here’s why. |Purbita Saha |July 28, 2021 |Popular-Science 

His instrument used a clever new way to hunt for other worlds, called the radial velocity technique. A century of astronomy revealed Earth’s place in the universe |Lisa Grossman |July 21, 2021 |Science News 

A quadrant of a circle is one quarter of the same, being bounded on two of its sides by two radial lines, as in Figure 49. Mechanical Drawing Self-Taught |Joshua Rose 

In kangaroo rats, which are saltators, the index rises to more than 100 owing to the lengthening of the radial component. Speciation in the Kangaroo Rat, Dipodomys ordii |Henry W. Setzer 

They were in a section of the passage now that corresponded roughly to the statue's radial artery. The Jewels of Aptor |Samuel R. Delany 

The lower bars are probably meant for teeth; what the radial lines on the crown are supposed to be is again conjecture. In Search Of Gravestones Old And Curious |W.T. (William Thomas) Vincent 

The hand of an emaciated individual may be readily injected, both arteries and veins, by fixing the pipe in the radial artery. History of Embalming |J. N. Gannal