That’s higher than depression, higher than phobia, higher than panic attacks and alcoholism. Why do we procrastinate, and how can we stop? Experts have answers. |Angela Haupt |July 9, 2021 |Washington Post 

In some cases, exposure therapy—in which phobia sufferers willingly and gradually increase their exposure to their trigger—can work, too. 'Needle Phobia' May Be Keeping Some From Getting Their COVID-19 Vaccine. Here's How to Cope |Tara Law |May 7, 2021 |Time 

Friends and family can also help loved ones cope with needle phobia, McMurtry says. 'Needle Phobia' May Be Keeping Some From Getting Their COVID-19 Vaccine. Here's How to Cope |Tara Law |May 7, 2021 |Time 

No one likes needles, though some patients have true phobias that may dissuade them from getting vaccines at all. Scientists have spent nearly 100 years searching for a better way to give vaccines |Katherine Ellen Foley |April 5, 2021 |Quartz 

Reading has also been shown to alleviate common mental health problems such as anxiety, depression, phobias and eating disorders. Stop doomscrolling on social media and read a book |matthewheimer |October 25, 2020 |Fortune 

The idea of “research” settings in our popular culture tap into this phobia. Why Smart People Are Dumb Patients |Jean Kim |July 14, 2014 |DAILY BEAST 

Contemporary society has gifted us with a phobia potentially as strong as acrophobia or the fear of flying: smartphone anxiety. Do You Suffer From Smartphone Anxiety? (And if So, What the Hell’s Your Problem?) |Melissa Fares |April 30, 2014 |DAILY BEAST 

The guy who took the cake suffered from a Vagina Dentata phobia, with attendant castration anxiety. HONOR THIS! |Moral Courage |February 3, 2014 |DAILY BEAST 

So there you have it: another outbreak fueled by irrational vaccine phobia. A Maddening Case of the Measles in Orthodox Jewish Brooklyn |Kent Sepkowitz |June 13, 2013 |DAILY BEAST 

He discusses his strange phobia, what makes him cry, and what he and Gore Vidal have in common. Lev Grossman’s Weird Phobia: ‘How I Write’ Interview |Noah Charney |June 8, 2012 |DAILY BEAST 

Maybe he had forced himself to go with her and the power of his lifelong phobia had wiped it from his memory. The Memory of Mars |Raymond F. Jones 

No matter how much overlay you pile on top of such a phobia to suppress it, it will continue to haunt you. The Memory of Mars |Raymond F. Jones 

Unless the fear of sleeplessness becomes a full grown phobia, no anxiety need be felt. Think |Col. Wm. C. Hunter 

Nervous breakdowns are increasing as a result of the American worry phobia. Evening Round Up |William Crosbie Hunter 

Anton Varcek won't be interested, one way or another; he has what amounts to a pathological phobia about firearms of any sort. Murder in the Gunroom |Henry Beam Piper