The creek near my home is modest compared to some of the waterways I’ve loved in the past. 11 Great Microadventures You Can Do Now |The Editors |October 1, 2020 |Outside Online 

The creek, which in the 1800s ran miles from the Temblor Mountains to the then-vast Buena Vista Lake, is now dry most of the year. Oil Companies Are Profiting From Illegal Spills. And California Lets Them. |by Janet Wilson, The Desert Sun, and Lylla Younes, ProPublica |September 18, 2020 |ProPublica 

Under state laws, it’s illegal to discharge any hazardous substance into a creek or streambed, dry or not. Oil Companies Are Profiting From Illegal Spills. And California Lets Them. |by Janet Wilson, The Desert Sun, and Lylla Younes, ProPublica |September 18, 2020 |ProPublica 

With CalGEM’s approval, companies turned these into de facto — but permanent — production sites, even in creeks and ravines supposedly protected by environmental laws. Oil Companies Are Profiting From Illegal Spills. And California Lets Them. |by Janet Wilson, The Desert Sun, and Lylla Younes, ProPublica |September 18, 2020 |ProPublica 

Despite the site’s long and dirty history, the water Kinder Morgan treats at its facility in Murray Canyon is good enough to throw back into the local creek – part of its remediation plan approved by the local water quality board. Pursuing Independent Water Sources, San Diego Ignores One Beneath Its Feet |MacKenzie Elmer |September 14, 2020 |Voice of San Diego 

One cold October day in 1968, I climbed out of a warm creek on the Yellowstone Plateau and came face to face with a huge grizzly. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

The third eaglet was never found despite a search by the Flint Creek volunteers and the landowner. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

But he had later received a call from the Cherry Creek School District saying she was not in class. How ISIS’s Colorado Girls Were Caught |Michael Daly |October 22, 2014 |DAILY BEAST 

The hills of the remote Green Valley Creek near the Russian River reminded Giuseppe of home. Napa’s Earthquake Is Not The Only Thing Shaking The Vineyards |Clive Irving |August 31, 2014 |DAILY BEAST 

West Dry Creek Road has a bunch of good Zinfandel vineyards, of which Rafanelli is, in my experience, the best. Napa’s Earthquake Is Not The Only Thing Shaking The Vineyards |Clive Irving |August 31, 2014 |DAILY BEAST 

In a minute Bruce was back with his hat full of water from the creek that whimpered just beyond the willow patch. Raw Gold |Bertrand W. Sinclair 

About three o'clock, as nearly as I could tell, we dipped into a wooded creek bottom some two hundred yards in width. Raw Gold |Bertrand W. Sinclair 

I swung down from my horse on the brink of the creek, cinched the saddle afresh, and rolled a cigarette. Raw Gold |Bertrand W. Sinclair 

Near noon I found a place where they'd cached two extra horses in the brush on Sage Creek. Raw Gold |Bertrand W. Sinclair 

At times the creek was as wide as an ordinary river, at others so contracted that one could gather grass on either side. Ancestors |Gertrude Atherton