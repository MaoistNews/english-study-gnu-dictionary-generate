Because they have not gone through the same extensive breeding as corn, soy, and wheat, the orphans have more untapped potential. New Veggies for a Warming Planet - Issue 92: Frontiers |Viviane Callier |November 4, 2020 |Nautilus 

The recent discoveries suggest that focusing on this handful of genes could accelerate the improvement of orphan crops, or even enable the de novo domestication of wild plants with crop potential. New Veggies for a Warming Planet - Issue 92: Frontiers |Viviane Callier |November 4, 2020 |Nautilus 

Like the Romanian orphans, these kids had lower cortisol levels than nonadopted children who had no behavioral problems. Puberty can repair the brain’s stress responses after hardship early in life |Esther Landhuis |August 28, 2020 |Science News 

Each held a sample of saliva from one of the 2- and 3-year-old orphans. Puberty may reboot the brain and behaviors |Esther Landhuis |August 27, 2020 |Science News For Students 

This confirmed Gunnar’s previous research on Romanian orphans and international adoptees living in the United States. Puberty may reboot the brain and behaviors |Esther Landhuis |August 27, 2020 |Science News For Students 

He gave us a little orphan girl in a red wig belting “Tomorrow.” Out of the Birdcage: How Mike Nichols Made Gay Culture Mainstream |Kevin Fallon |November 20, 2014 |DAILY BEAST 

She described how “in certain circles [it] is the moral equivalent of eating a baby orphan.” Let’s Get Rid of ‘Plus-Size’ for Good |Emily Shire |November 12, 2014 |DAILY BEAST 

Scribbling notes in the classroom next door is Justine, a shy 17-year-old orphan who is the only female in her class. Victims No More: Congo’s Badass Women Mechanics |Nina Strochlic |June 6, 2014 |DAILY BEAST 

Metronomic therapy is a quintessential financial orphan, Vikas Sukhatme says. How Big Pharma Holds Back in the War on Cancer |ProPublica |April 23, 2014 |DAILY BEAST 

The financial orphan problem points to a deeper issue with the way cancer drugs are developed. How Big Pharma Holds Back in the War on Cancer |ProPublica |April 23, 2014 |DAILY BEAST 

You love her, your beautiful Finnish orphan brought up in France and romantically met in London, with the adorable name? The Joyous Adventures of Aristide Pujol |William J. Locke 

She wore no denim uniform, such as Amy had mentioned on a previous occasion as being the mark of the usual “orphan.” The Campfire Girls of Roselawn |Margaret Penrose 

It was the home that had sheltered her orphan childhood; she had never slept a night from under its moss-grown roof. The World Before Them |Susanna Moodie 

George Gordon: Scotch birth, so far as can learn; left an orphan; lived mostly in London. Elster's Folly |Mrs. Henry Wood 

You shall not be the loser, Mrs. Martin, by the attention you may pay to this poor orphan girl. The World Before Them |Susanna Moodie