Squealing and growling with malevolent delight, Dano’s villain is a hybrid creation with his own distinctive lunacy, and he proves a worthy rival to Pattinson’s demented Dark Knight. ‘The Batman’ Is the Best Batman Movie Since ‘The Dark Knight’ |Nick Schager |February 28, 2022 |The Daily Beast 

At 27 feet tall and built of polyurethane coated with supposedly graffiti-proof paint, it appeared to have been crafted by a cross between a demented adolescent, a lover of brutalist Hitlerian “art,” and an unhinged cartoonist on a bad LSD trip. The Hideous People Behind a ‘Heinously Ugly’ Confederate Statue |Mark Potok |December 19, 2021 |The Daily Beast 

A brain scan showed such deterioration that the neuroradiologist said it looked like the MRI of a demented 85-year-old. A stay in the ICU can be terrifying. It might not get better after release. |Katie Hafner |October 29, 2021 |Washington Post 

Perhaps those numbers are overstated, but that’s still a lot of people willing to publicly express confidence in one of the more demented ideas that’s ever emerged. Elevating doubt is the point |Philip Bump |May 27, 2021 |Washington Post 

Did playing such a demented character like Dexter for so long mess with your head? Michael C. Hall on Going Drag for ‘Hedwig and the Angry Inch’ and Exorcising ‘Dexter’ |Marlow Stern |December 4, 2014 |DAILY BEAST 

It was unpleasant and discombobulating: a simulation of hostage-taking, mental asylum and demented dreamscape all rolled into one. Sex, Blood, and Screaming: Blackout’s Dark Frights |Tim Teeman |October 7, 2014 |DAILY BEAST 

I warmed to him immediately—it was impossible not to—unless you are a demented ideologue who has lost normal human reactions. The James Foley I Knew in the ISIS War Zone |Jamie Dettmer |August 20, 2014 |DAILY BEAST 

The simple answer, as the demented circus of Twitter and the Internet demonstrate every day, is: durr, no. Atheist King Richard Dawkins’ Rape Fantasy |Tim Teeman |July 31, 2014 |DAILY BEAST 

She is the mother from hell; a demented cross between Cersei and Tywin Lannister. Inside ‘Orange Is the New Black’ S2, Eps. 6-12: About That Shocking Incest Scene |Kevin Fallon, Marlow Stern |June 20, 2014 |DAILY BEAST 

She paused when she was beside the bed, and stood looking down upon him in that demented fashion. The Homesteader |Oscar Micheaux 

She made no reply, trembled, put her hair and her clothes straight again with the fingers of a woman demented. The Nabob |Alphonse Daudet 

The demented girl seemed not to hear, she did not even raise her eyes, and continued swaying to and fro and to laugh. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 

Ah, I see you think I am demented because I say that, but my husband is alive. Mary Louise and Josie O'Gorman |Emma Speed Sampson 

Letter has just come, and I am demented about my new godchild! A Confederate Girl's Diary |Sarah Margan Dawson