Images of thousands of Afghans desperately trying to flee their country following a hasty US withdrawal have provoked an international outcry. Where do Afghan refugees go? |Tazreena Sajjad |August 24, 2021 |Quartz 

So the agency turned to better-equipped academic institutes and private medical centers, including at Baylor College of Medicine, Texas A&M, and Houston Methodist, a hasty stopgap pattern seen in state after state last year. What We Learned About Genetic Sequencing During COVID-19 Could Revolutionize Public Health |Alice Park |June 11, 2021 |Time 

One of the women abruptly becomes a love interest, as if in a hasty attempt to make us feel something for her underdeveloped character seconds before her doom. Why Isn’t Sean Spicer Mauled by Zombies in Zack Snyder’s ‘Army of the Dead?’ |Melissa Leon |May 21, 2021 |The Daily Beast 

The board’s decision has become fodder for Bottoms’ critics, who said it represented her hasty and inconsistent leadership style. Atlanta Mayor Keisha Lance Bottoms says she won’t seek reelection |Tim Craig, Vanessa Williams, John Wagner |May 7, 2021 |Washington Post 

That left the Free State, Maryland, as the closest place for many couples to obtain a hasty marriage license. How One Small Maryland Town Became the Marriage Capital of the East Coast in the Early 20th Century |Melissa August |February 11, 2021 |Time 

Benjamin Franklin warned against making any hasty conclusions on such “a point of great importance.” Why We Can’t Quit Calling Presidents ‘Kings’ |Kevin Bleyer |November 22, 2014 |DAILY BEAST 

But the thrill was already gone when Fox rushed out a hasty follow-up later that year, The Next Joe Millionaire. You Really Don't Want to Watch Fox’s ‘I Wanna Marry “Harry”’ |Jason Lynch |May 20, 2014 |DAILY BEAST 

One of my greatest phobias is getting rocketed while showering or using the bathroom, so I keep those trips hasty. Dodging Rockets in Afghanistan as the Taliban’s Fighting Season Begins |Nick Willard |May 14, 2014 |DAILY BEAST 

This course of action is what the Constitution envisions and also slows down the hasty rush to war. Why Obama Should Be Applauded for Consulting Congress on Syria |Aaron Magid |September 9, 2013 |DAILY BEAST 

Haddad flew back immediately to find his city rising up, and is now beating a hasty retreat on the bus-fare increase. The Problem With Cities |Janine di Giovanni |June 24, 2013 |DAILY BEAST 

This is, of course, possible, but it cannot be more than speculation; the final Dunciad does show evidence of hasty revision. A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

And saying a hasty good afternoon, he popped through his doorway and vanished at Billy Woodchuck's feet. The Tale of Grandfather Mole |Arthur Scott Bailey 

Gilbert's "O shame, father," and Mrs. Rushmere's "God bless the dear child," following her hasty retreat. The World Before Them |Susanna Moodie 

Stunned by the reception they received, those who had not been killed or wounded beat a hasty retreat. The Courier of the Ozarks |Byron A. Dunn 

Sir Ralph, Mr. Findlay (who was helping us) and I, had our hasty lunch together. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow