Easy-going guy who enjoys nights out on the weekends and documentaries after work on the weeknights — and a guy who likes my eclectic taste in music and who prefers the beach to anywhere else. Meet D.C.’s Most Eligible LGBTQ Singles |Staff reports |February 11, 2021 |Washington Blade 

A final revelation from the filmmaker touches on what elevates “Two of Us” beyond the sum of its eclectic set of parts. Lesbian love story becomes thriller in ‘Two of Us’ |John Paul King |February 4, 2021 |Washington Blade 

I had a ton of fun with this eclectic and distinct theme, especially when building séance rooms, though not all of it fit my Tanglewood project. I recreated ‘Phasmophobia’ in ‘The Sims 4’ with the game’s new paranormal pack |Elise Favis |January 26, 2021 |Washington Post 

The platform’s product list is eclectic, from clothing to honey. Can Blockchain Save ‘Made in the USA’? |Daniel Malloy |January 8, 2021 |Ozy 

More than meets the eye, Belize’s tapestry is also woven with exotic cultures paired with an eclectic food scene. Your Personalized Guide to Belize's Best Adventures |Outside Editors |December 21, 2020 |Outside Online 

I think this is just much more natural and eclectic than a lot of films. ‘Boyhood’ Star Ellar Coltrane: An Astonishing Debut 12 Years in the Making |Kevin Fallon |July 11, 2014 |DAILY BEAST 

But these days, Starbucks features an eclectic mix of music with a very heavy dose of jazz. Jazz (The Music of Coffee and Donuts) Has Respect, But It Needs Love |Ted Gioia |June 15, 2014 |DAILY BEAST 

Forty-eight works from 37 artists, including 18 women, are on display, and the selection is eclectic. Pharrell’s ‘GIRLS’ Exhibit Stars...Pharrell? |Liza Foreman |May 29, 2014 |DAILY BEAST 

The occasion was a live taping for KCRW's long-running Morning Becomes Eclectic radio program. Beck’s Musical Time Machine: This Wasn’t a Concert. It was a Spectacular Party. |Andrew Romano |April 18, 2014 |DAILY BEAST 

He also posted the tips videos on YouTube, where his eclectic likes suggest the very opposite of a narrow-minded fanatic. Don’t Turn This Malaysia Airlines Pilot Into Flight 370’s Richard Jewell |Michael Daly |March 17, 2014 |DAILY BEAST 

But Alexander was only a religious eclectic, honouring what he thought best in the current systems of belief. The Catacombs of Rome |William Henry Withrow 

In consequence of its symbolical purpose this hieratic series is rather eclectic than cyclopdic in its character. The Catacombs of Rome |William Henry Withrow 

Shy in boyhood and eclectic in the matter of associates, he had the genius for real friendships. Historic Fredericksburg |John T. Goolrick 

It was during this period that Clemens formulated his eclectic therapeutic doctrine. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 

In its actual procedure school work must always be thus eclectic. The Montessori Method |Maria Montessori