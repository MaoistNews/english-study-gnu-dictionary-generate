A couple hundred years ago, of course, the church or state could have just crushed such a heterodox movement. America’s Fastest Growing Death Holiday Is From Mexico |Michael Schulson |November 1, 2014 |DAILY BEAST 

McGibney is the first to acknowledge that his anti-bullying approach is heterodox. The Bully Waging War Against Bullies |Lizzie Crocker |October 10, 2013 |DAILY BEAST 

For example, Stephens is very opposed to the state tolerating lots of little heterodox churches. Freedom Of Religion? Let's Not Get Carried Away |David Frum |August 15, 2012 |DAILY BEAST 

More than many, he understood the electoral attractions of a heterodox Democratic party. America's Prime Minister |Tunku Varadarajan |September 28, 2010 |DAILY BEAST 

He reminds me a bit of George Allen, but more moderate, cosmopolitan, and heterodox. Scarborough for President |Mark McKinnon |September 8, 2009 |DAILY BEAST 

This heterodox opinion brought upon him a crowd of antagonistic replies, and amongst them the Rfutation of Bodin. Notes and Queries, Number 177, March 19, 1853 |Various 

But we perceive nothing, on Christian theories, heterodox in the general position. The Indian in his Wigwam |Henry R. Schoolcraft 

Should add that Mayor was exceedingly popular politician of heterodox views. Punch, or the London Charivari, Vol. 109, August 3, 1895 |Various 

It is apparent that Grotius had not sufficiently examined this subject, since he speaks of it in a manner so heterodox. The Life of the Truly Eminent and Learned Hugo Grotius |Jean Lvesque de Burigny 

His heterodox brother—in the eighteenth century they both usually belonged to one family—leaves it out. Critical Miscellanies (Vol. 2 of 3) |John Morley