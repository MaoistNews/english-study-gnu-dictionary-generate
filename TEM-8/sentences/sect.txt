Fasting is also possible throughout the year by individuals, but this celebration is the common communal embracing of fasting across sects. Fasting May Have Become a Health Fad, But Religious Communities Have Been Doing It For Millennia |LGBTQ-Editor |July 30, 2021 |No Straight News 

Yesterday, Twitter deleted tweets and videos of President Muhammadu Buhari making threats of punishment to a sect called IPOB in the South-Eastern part of the country after he blamed them for attacks on government buildings. Nigeria suspends Twitter operations, says platform ‘undermines its corporate existence’ |Tage Kene-Okafor |June 4, 2021 |TechCrunch 

For a certain jet-setting sect, wide-open spaces with views, few Covid-19 cases, and the freedom to go maskless are all the rage. Welcome to the “New” Issue of The Highlight |Vox Staff |February 1, 2021 |Vox 

She specifically focused on the Satmar community, the sect Feldman was once part of. Inside ‘Unorthodox’: How the Netflix series lined up its cast and costume design |radmarya |August 30, 2020 |Fortune 

It also resembles those of a 1st-century sect leader in Palestine who was also popular with the poor. Is India’s Fallen ‘God-Man’ So Different From a Megachurch Pastor? |Jay Michaelson |November 21, 2014 |DAILY BEAST 

On a 2,813-acre tract roughly 30 miles west, Washington found a Calvinist sect called the Seceders squatting on his land. Washington’s Wheeler-Dealer Patriotism |William O’Connor |October 31, 2014 |DAILY BEAST 

He was an Ahmadi, a minority Muslim sect that Pakistan has declared un-Islamic and against which it discriminates horribly. Why So Many Pakistanis Hate Their Nobel Peace Prize Winner |Chris Allbritton |October 10, 2014 |DAILY BEAST 

Perl loved all kinds of music, including rock and roll, growing up in the Chabad community, a sect of Hasidic Judaism. The Sisterhood of Bulletproof Stockings: It’s Ladies’ Night for Hasidic Rockers |Emily Shire |September 30, 2014 |DAILY BEAST 

The Yazidis, members of an ancient religious sect, fled when the Islamic State overran their homes. Will U.S. Troops Stand By While ISIS Starves Thousands? |Jacob Siegel |August 7, 2014 |DAILY BEAST 

In this she differed from others of her sect, who strove to convey the idea of humility both outwardly and inwardly. Skipper Worse |Alexander Lange Kielland 

He was the chief of the seven sages of Greece, and founder of the Ionic sect of philosophers. The Every Day Book of History and Chronology |Joel Munsell 

The reader may give three lines to both, if he pleases; see note to sect. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Certainly the most cultivated and aristocratic sect--the Sadducees--repudiated it altogether; while the Pharisees held to it. Beacon Lights of History, Volume I |John Lord 

At the close of the last century there existed a religious sect who were in favour of abandoning the use of clothing. A Cursory History of Swearing |Julian Sharman