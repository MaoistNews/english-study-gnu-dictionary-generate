While the inquest has yet to fully start, one theory centers on the team’s age. Tokyo Surprises: Expect the Unexpected |Toyloy Brown III |August 6, 2021 |Ozy 

She was surprised to hear Belton’s voice, according to testimony from her and Belton during a coroner’s inquest. The Murder Chicago Didn’t Want to Solve |by Mick Dumke |February 25, 2021 |ProPublica 

No doubt, there will be an inquest into what went so wrong that a mob was able to march up the same steps where female lawmakers formed an honor guard as the late Supreme Court Justice Ruth Bader Ginsburg’s body left the building last year. The Breach of the Capitol Spooked Us — As It Should Have |Philip Elliott |January 7, 2021 |Time 

That is a fact recorded by the doctor in charge of the ambulance at the inquest. Harry’s Daddy, and Diana’s ‘Murder’: Royal Rumors In a New Play |Tom Sykes |January 4, 2015 |DAILY BEAST 

At last month's inquest, Coroner Fiona Wilcox concluded Mrs Saldanha had taken her own life. Kate Prank Call DJ Tells of Death Threats |Tom Sykes |October 13, 2014 |DAILY BEAST 

He told senior officials that an inquest was not powerful enough to handle the kind of allegations he needed to explore. Brits Investigate Assassination of the Spy Who Warned Us About Putin |Nico Hines |July 22, 2014 |DAILY BEAST 

At an inquest in May, DCI Paul Fotheringham of Kent Police said recent heroin use was likely to have played a role in her death. Peaches Geldof's Tragic Last Interview: "I’m going to die like my mother." |Tom Sykes |June 20, 2014 |DAILY BEAST 

Peaches Geldof died of a heroin overdose,an inquest heard today. Peaches Geldof Died Of Heroin Overdose |Tom Sykes |May 1, 2014 |DAILY BEAST 

The fortune was proving quite as large as he had expected, and not even an inquest had been held upon the dead man. Uncanny Tales |Various 

When I was about twenty I held a sort of inquest upon it and found out a number of things. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

"One of the most extraordinary cases I have ever met with," the doctor told the coroner at the inquest. Uncanny Tales |Various 

Couldn't say much at inquest, or didn't; was asked if he witnessed accident; said 'No,' but some still think he did. Elster's Folly |Mrs. Henry Wood 

A woman having died suddenly at Waterford, the Coroner had, according to law, ordered an inquest. Friend Mac Donald |Max O'Rell