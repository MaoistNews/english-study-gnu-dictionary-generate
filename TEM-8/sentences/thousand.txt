This third-party delivery ecosystem has caused death by a thousand cuts for restaurants for the past two decades. The True Cost of Convenience |Deepti Sharma |January 22, 2021 |Eater 

There are all of these hundreds of thousands or millions of posts that might not get that many interactions but collectively make up a lot of misinformation. Why Fights Over The COVID-19 Vaccine Are Everywhere On Facebook |Kaleigh Rogers |January 22, 2021 |FiveThirtyEight 

Then an unexpectedly large wave basically broke on me, and the wall of water felt as if a thousand tiny needles had been shot into my face. How I Learned to Surf in Middle Age |Tom Vanderbilt |January 5, 2021 |Outside Online 

Remember, though, that it will be supporting your upper half for thousands of hours, so it’s worth investing a little time and energy into choosing the right one. The best pillow: Sleep better in any position |Carsen Joenk |December 11, 2020 |Popular-Science 

Two-hundred forty-five thousand, when you’re still more than 9 million in the hole. The economy added 245,000 jobs in November, the slowest month of growth since recovery began |Eli Rosenberg |December 4, 2020 |Washington Post 

Well over a thousand holes in, I average less than four strokes per hole. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

It cost several thousand dollars and a high-powered former district attorney to get the charges dropped. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 

Neither could her three-week, multi-thousand dollar stay, which was supposed to be a recovery period. The Insurance Company Promised a Gender Reassignment. Then They Made a Mistake. |James Joiner |December 29, 2014 |DAILY BEAST 

One person who dialed in has “a pretty big Twitter following,” Goff said, “several thousand.” ‘Ready for Romney’ Is Amateur Hour |Tim Mak |December 23, 2014 |DAILY BEAST 

Two years into an Arctic expedition, they were forced to abandon ship a thousand miles north of Siberia. The Best Nonfiction Books of 2014 |William O’Connor |December 14, 2014 |DAILY BEAST 

It contains above eighty thousand houses, and about six hundred thousand inhabitants. Gulliver's Travels |Jonathan Swift 

The garrison of the town and fortress was nearly three thousand strong. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

There were two battalions, together about a thousand men; and they brought a field-piece with them. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

And it was no light task, then, for six hundred men to keep the peace on a thousand miles of frontier. Raw Gold |Bertrand W. Sinclair 

Ten thousand of the best troops in Mexico entered Texas and were shortly to be followed by ten thousand more. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various