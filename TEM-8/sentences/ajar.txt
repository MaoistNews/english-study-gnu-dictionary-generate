Freeman said that during the gap in the video, Prather was pounding on the door so violently it popped ajar. After escalating for years, a neighborhood feud culminates in killing caught on video |Justin Jouvenal |October 7, 2021 |Washington Post 

When the police officers arrived minutes later, they pushed the door ajar and found Trawick, a personal trainer and dancer, standing near his stove, holding the knife and stick. What Police Impunity Looks Like: “There Was No Discipline as No Wrongdoing Was Found” |by Eric Umansky |April 20, 2021 |ProPublica 

The evidence suggested that the killer or killers had probably entered the building through the back door, which had been found ajar. The Murder Chicago Didn’t Want to Solve |by Mick Dumke |February 25, 2021 |ProPublica 

Despite the high drama of shutting down news, Facebook still has sizable payment offers on publishers’ tables and there they remain with doors ajar. How Australia May Have Just Saved Journalism From Big Tech |Robert Whitehead |February 22, 2021 |Time 

Every day, we are all forced to recalculate the wisdom of reentering a world that is, if far from completely reopened, at least left cautiously ajar. How Does Your COVID-19 Risk Tolerance Compare to Others? |Chris Wilson |October 19, 2020 |Time 

A guard is manning the door, which is always kept ajar so she can be monitored. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 

Her door stands ajar, halving the room with a beam of light. After the Genocide, Rwanda’s Widows Aging Alone |Nina Strochlic |August 31, 2014 |DAILY BEAST 

By the time the No Smoking sign flashes off, both of the Allmans are fast asleep, their mouths characteristically ajar. Stacks: Hitting the Note with the Allman Brothers Band |Grover Lewis |March 15, 2014 |DAILY BEAST 

An “exit” door left ajar welcomed leering lingerers backstage. Eight Minutes at Macaulay Culkin’s Pizza Party |Caitlin Dickson |December 14, 2013 |DAILY BEAST 

Postscript: According to his Instagram and Facebook feed, Ajar is still living large. The Real Bling Ring: Where Are They Now? |Tricia Romano |May 21, 2013 |DAILY BEAST 

It was ajar, and Kerry, taking an electric torch from his overall pocket, flashed the light upon the name-plate. Dope |Sax Rohmer 

Edna had discovered it accidentally one day when the high-board gate stood ajar. The Awakening and Selected Short Stories |Kate Chopin 

He and Queeker stood in the passage and saw the bed, the invalid, and the watcher through an inner door which stood ajar. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

I went to the stranger's room, and listened at his door, which was slightly ajar. A Connecticut Yankee in King Arthur's Court, Complete |Mark Twain (Samuel Clemens) 

With our door ajar we watched the ghastly struggle between the faithful mongrel and the assassin. A Virginia Scout |Hugh Pendexter