To stave off death you’ll have to confront a witch who likes to replace her subjects’ heads with crockery, a Napoleon-like frog with a string of titles that proclaim his glory and a mountain-dwelling yeti named Betty. ‘Death’s Door’: A clever and fun Zelda homage |Christopher Byrd |July 30, 2021 |Washington Post 

The threat is more immediate in Manuela Viera Gallo’s jagged necklaces, made of broken crockery and other trashed household objects. In the galleries: A painful, political take on the art of cruel shoes |Mark Jenkins |April 9, 2021 |Washington Post 

There's the "finale" we should be seeking after the broken crockery from the Tea Party tantrum is cleared away. What Comes After the Tea Party? |David Frum |February 21, 2013 |DAILY BEAST 

He also could be maddening to work with, dominating any situation and breaking a lot of crockery in the process. U.N. Ambassador Susan Rice’s Blunt Style Unusual, But Effective on Libya |Eleanor Clift |January 19, 2012 |DAILY BEAST 

Some bits of broken crockery furnished his table, a board wedged against the rock. Dorothy at Skyrie |Evelyn Raymond 

He drew loads of boards from the saw-mill, and loads of crockery from the various village stores. David Fleming's Forgiveness |Margaret Murray Robertson 

I was sawing up a few more sticks from the orchard, when the express man drove up with the beds, the crockery, and so on. The Idyl of Twin Fires |Walter Prichard Eaton 

It was in trying to grab something that she lost control, and fell, barge and all after her crockery into the sea. The Romance of His Life |Mary Cholmondeley 

Always an unruly fellow, and dangerous to trust among crockery. History Of Friedrich II. of Prussia, Vol. I. (of XXI.) |Thomas Carlyle