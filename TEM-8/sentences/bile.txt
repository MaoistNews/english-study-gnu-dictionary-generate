It can be green or yellow because it contains a lot of bile. Why Is My Poop Brown? |LGBTQ-Editor |December 13, 2021 |No Straight News 

If you have heard of bile, you may know that it comes from the gallbladder. Your liver does more than you give it credit for |By Marie-Pierre Hasne/The Conversation |March 17, 2021 |Popular-Science 

Fresh beef blood and bile can appear in a warm, spicy beef salad with a riot of dried herbs and chilies. Blood is a respected ingredient around the world, but less so in the U.S. A new book aims to change that. |Mayukh Sen |February 26, 2021 |Washington Post 

If they want to call on Fox News figures like Bret Baier and Chris Wallace to denounce the bile flowing from the so-called “opinion” shows, glorious. Hey, Democrats: Hands off Fox News’s cable carriers |Erik Wemple |February 24, 2021 |Washington Post 

Indeed, few filmmakers get as much bile and vitriol spewed their way as the man behind Madea. Gone Girl’s Biggest Twist Is the Superb Tyler Perry |Alex Suskind |October 6, 2014 |DAILY BEAST 

So why does Electronic Intifada direct so much more bile toward the former than the latter? The Pro-Palestinian Left's Hamas Blindspot |Peter Beinart |March 8, 2013 |DAILY BEAST 

Jesse Singal reports on the latest attempts to stem the flow of Internet bile. Most Comments Are Horrible—Sites Look for Ways to Make Them Better |Jesse Singal |July 16, 2012 |DAILY BEAST 

By now, American rage has refined itself—like wine mixed with bile—into various categories and degrees. From the JetBlue Pilot to Robert Bales, Cultural Road Rage Is Everywhere |Lee Siegel |March 28, 2012 |DAILY BEAST 

The surgery removes the right side of the pancreas, the gallbladder, and parts of the stomach, bile duct, and small intestine. Jobs’s Unorthodox Treatment |Sharon Begley |October 6, 2011 |DAILY BEAST 

This, however, should not be accepted as proving the presence of bile without further tests. A Manual of Clinical Diagnosis |James Campbell Todd 

It depends upon the fact that bile acids lower surface tension. A Manual of Clinical Diagnosis |James Campbell Todd 

A trace of bile may be present as a result of excessive straining while the tube is in the stomach. A Manual of Clinical Diagnosis |James Campbell Todd 

Putty-colored or "acholic" stools occur when bile is deficient, either from obstruction to outflow or from deficient secretion. A Manual of Clinical Diagnosis |James Campbell Todd 

The shell is thick, and is surrounded by an uneven gelatinous envelop which is often stained with bile. A Manual of Clinical Diagnosis |James Campbell Todd