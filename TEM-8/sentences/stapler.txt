Post-itUp there with pens and staplers as one of the most popular office products of all time, these sticky notes are also the result of a serendipitous moment. Accidental Genius: 10 Surprising Inventions |Emma Foster |July 30, 2021 |Ozy 

The thing is, you just want to join some files together, not spend precious time researching the privacy practices of a virtual stapler. How to combine PDF files for free without sketchy online software |John Kennedy |June 29, 2021 |Popular-Science 

Methinks Cuthbert will need no hint from me to despise the home of the honest wool stapler. The Lost Treasure of Trevlyn |Evelyn Everett-Green 

And so it is to London thou wilt go--to the worthy wool stapler on the Bridge? The Lost Treasure of Trevlyn |Evelyn Everett-Green 

Captain Stapler, who was no card-player, looked on and replenished the long glasses of Scotch that stood at each man's right hand. A Son Of The Sun |Jack London 

His father was a wool-stapler, holding a good position in the district. My Experiences as an Executioner |James Berry 

His father was a wool stapler who lost his fortune in the panic of 1847. The Quarterly of the Oregon Historical Society, Vol. IV |Various