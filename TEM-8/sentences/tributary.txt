That figure suggests how much rain may have fallen elsewhere over the 11,560 square miles of field and forest and city and town that drain into the tributaries that empty into the Potomac before it arrives at Little Falls. Our second-coldest day of 2020 came on Saturday |Martin Weil |December 27, 2020 |Washington Post 

Twomey says that dyslexic readers, in their struggle to learn to read, are calling on more of the tributaries in the brain to overcome their difficulties of whichever script they are being taught. Reading, That Strange and Uniquely Human Thing - Issue 94: Evolving |Lydia Wilson |December 23, 2020 |Nautilus 

As we learn more about the mysterious tributaries activated in reading, perhaps there are more teaching strategies to be discovered, helping those who do not find it a natural activity, or for those around the world who miss out on early education. Reading, That Strange and Uniquely Human Thing - Issue 94: Evolving |Lydia Wilson |December 23, 2020 |Nautilus 

A four-acre sinkhole there had opened at the plant’s coal ash pond years earlier, releasing over 2 million gallons of arsenic-laced ash and water into a tributary of the Etowah River. A Power Company’s Quiet Land-Buying Spree Could Shield It From Coal Ash Cleanup Costs |by Max Blau for Georgia Health News |November 24, 2020 |ProPublica 

From Rig to Flip and American Rivers, Rio Rica introduces us to Rica Fulton and the Little Snake River, a lesser known tributary of the West’s renowned Yampa River. Exploring the Little Snake River |Outside Editors |October 5, 2020 |Outside Online 

The first pioneer to reach the riparian tributary where Kansas City now shimmers was, in fact, on the lam himself. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

The village sits along a narrowing vein of the Rio Negro, a tributary of the mighty Amazon. Bye Bye Latté, Hello Guayusa: Why The Amazon Holds the Secret to a Cleaner, Healthier Caffeine |Brandon Presser |August 29, 2014 |DAILY BEAST 

But I had to find my way into another passion, and with the art, it was a river flowing into a tributary. Painting for Pleasure |Rachel Syme |May 6, 2010 |DAILY BEAST 

Those in the middle have arisen from the union of the moraines formed in two or more tributary valleys. Outlines of the Earth's History |Nathaniel Southgate Shaler 

From there we proceeded up the Gury, a large tributary stream flowing into the Ganges below Rumpurbolea. A Woman's Journey Round the World |Ida Pfeiffer 

He gestured with his arm to the valley below where a cool stream of water dashed over rocks on its way to join a bigger tributary. The Adventure Girls at K Bar O |Clair Blank 

At various points tributary gorges, the graves of fluvial gods who had perished long ago, opened into the main cañon. Overland |John William De Forest 

This fish (but seldom taken any great weight) abounds in the Tees and its tributary streams. The Teesdale Angler |R Lakeland