Wilson, too, doesn’t offer any miracle cures—because he’s not full of crap. How to Avoid a Mid-Race Bathroom Stop |mmirhashem |August 1, 2021 |Outside Online 

Maybe then, as the seas rise, ice sheets melt and disasters-of-a-millennia crowd up like Space Mountain thrill-seekers, it’s time to cut the crap. Climate Disasters Are Making It Hard to Enjoy the Olympics. And I’m Not Sure I Want to, Anyway |Alejandro de la Garza |July 29, 2021 |Time 

That’s because reality battles with memory and kicks the crap out of it. Gene Weingarten: New York, from the bizarre to the truly bizarre |Gene Weingarten |June 3, 2021 |Washington Post 

You’d park a car in a lot in the morning and then return after sunset, when all the yellow-tinted mercury vapor lights are lit, and the car would be all but unrecognizable—except for its shape, license plate, and the crap in the back seat, of course. How These Rothkos Were Restored Without Touching the Canvas |Adam Rogers |May 30, 2021 |The Daily Beast 

The pile of crap got so huge that it dripped down a meter length of the insulator and caused a short triggering an automatic shutdown. Should Traffic Lights Be Abolished? (Ep. 454) |Stephen J. Dubner |March 11, 2021 |Freakonomics 

She said, "Crap," and moved away, acknowledging that I'd gotten her. Little Brother |Cory Doctorow 

I haven't had time to tell Maragon the boys on the Crap Patrol were wrong. Card Trick |Walter Bupp AKA Randall Garrett 

Each word is repeated to a man; and when the leader comes to “Crap in,” the man specified draws in his foot. The Traditional Games of England, Scotland, and Ireland (Vol II of II) |Alice Bertha Gomme