When he has called the police in the past, they have not responded, or responded “mad late.” Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

From righteous fury to faux indignation, everything we got mad about in 2014—and how outrage has taken over our lives. The Daily Beast’s Best Longreads, Dec 15-21, 2014 |William Boot |December 21, 2014 |DAILY BEAST 

The house decays around Amelia and Samuel, their world narrows and becomes mad, undealable with. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

We fight over their ownership and control, as if reality were a resource as scarce as the water and oil in Mad Max. On Torture, Chuck Johnson & Sondheim |James Poulos |December 13, 2014 |DAILY BEAST 

This year's shockers: no Amy Poehler, nothing for 'Mad Men,' and a whole lot of love for virgins and transgenders. 15 Enraging Golden Globe TV Snubs and Surprises: Amy Poehler, 'Mad Men' & More |Kevin Fallon |December 11, 2014 |DAILY BEAST 

Then she won, and went half mad with the joy and excitement, but the joy didn't last long. Rosemary in Search of a Father |C. N. Williamson 

Mankind, mad with the energy of activity, would be seen to pursue the fleeing phantom of insatiable desire. The Unsolved Riddle of Social Justice |Stephen Leacock 

Irene's been down to the train to meet you three times and she's sure fighting mad by this time. The Homesteader |Oscar Micheaux 

Your mad career generally ended in a crowd and a free fight of confetti. The Joyous Adventures of Aristide Pujol |William J. Locke 

Some who would face a mad bull coolly enough spring with disgust from a cockroach or a centipede. Hunting the Lions |R.M. Ballantyne