Whatever becomes of the quest for artificial general intelligence, it seems there’s still plenty of room to run for its more vocational forerunners. DeepMind’s Vibrant New Virtual World Trains Flexible AI With Endless Play |Jason Dorrier |August 1, 2021 |Singularity Hub 

They’re most interesting as forerunners to other, better Taylor Swift songs. Taylor Swift’s new take on ‘Fearless’ piles on the nostalgia, along with some revenge |Allison Stewart |April 12, 2021 |Washington Post 

How it evolved, and where its forerunners came from, is up for debate. Your ancestors might have been Martians |Charlie Wood |February 12, 2021 |Popular-Science 

The EU, a forerunner on climate policy, has pledged to make the bloc’s economic recovery “green.” When it comes to climate change, says Mark Carney, this financial crisis is different—and maybe better |kdunn6 |October 27, 2020 |Fortune 

The researchers showed that, elaborate as that chemical mechanism is in cells today, nearly all the ingredients for a potential forerunner to it could have formed easily from just two simple organic compounds reacting in water. New Clues to Chemical Origins of Metabolism at Dawn of Life |John Rennie |October 12, 2020 |Quanta Magazine 

Melchior is the forerunner of the aunt who always gave me socks. Keep Christmas Commercialized! |P. J. O’Rourke |December 6, 2014 |DAILY BEAST 

Turing conceived and built a computer, the forerunner of all digital computations, that cracked the code. The Castration of Alan Turing, Britain’s Code-Breaking WWII Hero |Clive Irving |November 29, 2014 |DAILY BEAST 

By contrast, Ashraf Ghani, the current forerunner in the partial second round results, got record support in Pashtun areas. Kerry Must Let the Afghan Voters Choose Their Next President |Zardasht Shams |July 11, 2014 |DAILY BEAST 

The organization started by Norquist is a forerunner to the Tea Party. Vicious Rift on the Right |Eli Lake, Ben Jacobs |October 25, 2013 |DAILY BEAST 

I see Dickens as the forerunner to people like Chaplin and Woody Allen, really. Happy Birthday, Charles Dickens! ‘Lost,’ ‘NCIS,’ ‘Big Love,’ ‘Veep’ Writers on His Legacy |Jace Lacob |February 7, 2012 |DAILY BEAST 

It may be a forerunner or successor, the cause or consequence, or a contemporaneous fact, etc. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Poniatowski's campaign against Austria, glorious as it was for the Poles, was in reality the forerunner of disaster. Napoleon's Marshals |R. P. Dunn-Pattison 

Forerunner of the many first-aid classes to come was that hour of Mabel's, and made memorable by one thing she said. The Amazing Interlude |Mary Roberts Rinehart 

Nobody dreamed at that time that the little tool was the forerunner of a great change. The Later Cave-Men |Katharine Elizabeth Dopp 

This is the first attempt at an anthology of Yorkshire poetry, and the forerunner of many other anthologies. Yorkshire Dialect Poems |F.W. Moorman