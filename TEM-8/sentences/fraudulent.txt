He telegraphed over and over that he planned to cast mail-in ballots as fraudulent, and, over and over, The Post and others pointed out that there was no evidence to support the claim. The lie that lingers: 3 in 10 Americans falsely believe the election was riddled with fraud |Philip Bump |January 19, 2021 |Washington Post 

Some followers of the QAnon conspiracy theory had seized on the mathematical theory as proof that the November election was fraudulent, and changed the Wikipedia page accordingly. On its 20th birthday, Wikipedia might be safest place online |Heather Kelly |January 15, 2021 |Washington Post 

So if the dominant position in the Republican Party is that the only free and fair elections are those where Republicans win, and anything else is “stolen” and fraudulent, then we’re on the precipice of not having a democracy. How Much Danger Is American Democracy In? |Sarah Frostenson (sarah.frostenson@abc.com) |January 12, 2021 |FiveThirtyEight 

She doesn’t understand why some Republican lawmakers continue to repeat baseless claims that the election was fraudulent, further angering the masses. Americans across the political spectrum fear what the Capitol attack portends |Annie Gowen, Jenna Johnson, Holly Bailey |January 12, 2021 |Washington Post 

At least millions of dollars of previous aid went to fraudulent applicants, according to public and private estimates. U.S. suspends 25% tariff on French goods as it widens review of digital taxes | |January 7, 2021 |Washington Post 

You better take it before your filthy fraudulent self is bared to the nation. Why the FBI’s Suicide Note to MLK Still Matters |Nick Gillespie |November 15, 2014 |DAILY BEAST 

The investigation stems from Dec. 2013 charges that 25 Russian diplomats allegedly tried to obtain fraudulent Medicaid benefits. Obamacare Covers Foreign Diplomats |Tim Mak |October 29, 2014 |DAILY BEAST 

And when a fraudulent work hits the marketplace, it tends to circulate. Why eBay Is an Art Forger’s Paradise |Lizzie Crocker |August 19, 2014 |DAILY BEAST 

Once submitted, a detailed review is initiated and vetted by our teams, and listings deemed fraudulent will be removed promptly. Why eBay Is an Art Forger’s Paradise |Lizzie Crocker |August 19, 2014 |DAILY BEAST 

And in April, a Florida pastor named Kevin Sutherland was convicted of trying to sell fraudulent Damien Hirst paintings. Why eBay Is an Art Forger’s Paradise |Lizzie Crocker |August 19, 2014 |DAILY BEAST 

The transfers of property covered by the act are those which the common law regards as fraudulent. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

And thirdly he knew that his adversary would cheat if he could and that his adversary suspected him of fraudulent designs. The Joyous Adventures of Aristide Pujol |William J. Locke 

A different rule applies to a minor who makes a fraudulent contract. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Bribery and fraudulent voting gained ground, and an attempt was again made to meet them by increasing the severity of the law. A short history of Rhode Island |George Washington Greene 

The nub of the whole matter is: The claims made for Sanatogen are unwarranted, misleading and fraudulent. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various