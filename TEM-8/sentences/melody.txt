The Google Magenta team, which makes machine-learning tools for the creative process, has made models that help you compose melodies, and tools that help you sketch cats. Create your own moody quarantine music with Google’s AI |Karen Hao |September 4, 2020 |MIT Technology Review 

I’d get a melody in my head and I would wonder if I could turn it into a song. Inside an FKT Attempt on the Appalachian Trail |Martin Fritz Huber |September 3, 2020 |Outside Online 

Without an idea of how neural circuits work and in what sequences, zapping the brain with electricity—no matter how cool the device itself is—is akin to banging on all the keys of a piano at once, rather than composing a beautiful melody. Neuralink’s Wildly Anticipated New Brain Implant: the Hype vs. the Science |Shelly Fan |September 1, 2020 |Singularity Hub 

Similar to a piano melody, a single wrong note may not ruin the entire song—or in the case of glomeruli, the perception of a smell. A Highway to Smell: How Scientists Used Light to Incept Smell in Mice |Shelly Fan |July 1, 2020 |Singularity Hub 

Together, the individual spots the researchers stimulated built the perception of the odor, just as a string of notes makes a melody. How to make a mouse smell a smell that doesn’t actually exist |Laura Sanders |June 18, 2020 |Science News 

A department store piano melody plays in the background while he admires everything he can see. Kirk Cameron Saves Christmas from Abominable Killjoys (Other Christians) |Brandy Zadrozny |November 14, 2014 |DAILY BEAST 

I had to play melody while simultaneously playing harmony with him. The Stacks: John Coltrane’s Mighty Musical Quest |Nat Hentoff |October 18, 2014 |DAILY BEAST 

Arriving at the Melody Ballroom, the atmosphere was a frenzy of joy, jubilation and holy bedlam. The Battle for LGBT Equality Isn’t Over Yet |Gene Robinson |May 25, 2014 |DAILY BEAST 

He probably heard the song during a Brazilian tour, and the melody simply stayed in his head. Did Led Zeppelin Steal ‘Stairway to Heaven’? |Ted Gioia |May 25, 2014 |DAILY BEAST 

When Paul McCartney came up with the melody to “Yesterday,” he initially feared that it was an old song that he was recalling. Did Led Zeppelin Steal ‘Stairway to Heaven’? |Ted Gioia |May 25, 2014 |DAILY BEAST 

The flute and the psaltery make a sweet melody, but a pleasant tongue is above them both. The Bible, Douay-Rheims Version |Various 

And the singers lifted up their voices, and in the great house the sound of sweet melody was increased. The Bible, Douay-Rheims Version |Various 

Don't you remember my saying that Liszt had such an extraordinary way of playing a melody? Music-Study in Germany |Amy Fay 

The mirth of timbrels hath ceased, the noise of them that rejoice is ended, the melody of the harp is silent. The Bible, Douay-Rheims Version |Various 

The melody or tune is played on one of the pipes furnished with holes for the purpose, while the other three give a drone, bass. The Recent Revolution in Organ Building |George Laing Miller