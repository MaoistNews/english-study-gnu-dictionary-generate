“It’s like treating an individual patient while rebuilding the entire health-care system,” said Alfred Sommer, former dean of the Johns Hopkins Bloomberg School of Public Health, who was part of the team that vanquished smallpox four decades ago. Now comes the hardest part: Getting a coronavirus vaccine from loading dock to upper arm |Lena H. Sun, Frances Stead Sellers |November 23, 2020 |Washington Post 

It was the final chapter of a very polarizing time for the city attorney’s office, and he came out the vanquished. Your San Diego Election Questions, Answered |Megan Wood |October 20, 2020 |Voice of San Diego 

Genetic analysis, which was not available during the original research, later confirmed that during the 1994 invasion, the aggressors didn’t just evict the vanquished. Naked mole-rats invade neighboring colonies and steal babies |Jake Buehler |October 20, 2020 |Science News 

It posited that microbes, not “miasma,” caused sickness and death, and that you could cure illnesses by vanquishing the offending microbe. When Evolution Is Infectious - Issue 90: Something Green |Moises Velasquez-Manoff |September 30, 2020 |Nautilus 

This time around, the Bolts made quick work of the Jackets in the first round, vanquishing their old foe in five games. The Dallas Stars Are Putting Their Regular-Season Shortcomings Behind Them |Terrence Doyle |September 21, 2020 |FiveThirtyEight 

But there is a strong resolve to vote despite the risk, a fierce determination to vanquish the Vandals. With Poroshenko on the Ukraine Campaign Trail |Bernard-Henri Lévy |May 23, 2014 |DAILY BEAST 

In an attempt to vanquish the monster, the superheroes step in and pummel him, knocking him to the ground. The Rise of Superhero Therapy: Comic Books as Psychological Treatment |Alex Suskind |February 17, 2014 |DAILY BEAST 

To be competitive here three years from now, Rand knows he needs to vanquish Ron's long shadow. Rand Paul: I’m Not My Dad |David Catanese |June 29, 2013 |DAILY BEAST 

Visco says she believes the way to vanquish the disease is through science. Bill Clinton’s New Gig: Curing Breast Cancer |Abigail Pesta |November 13, 2012 |DAILY BEAST 

She is credited with helping to end the war and vanquish dictator Charles Taylor in 2003, opening the door for Sirleaf. A Nobel Smackdown in Liberia: Leymah Gbowee vs. Ellen Johnson Sirleaf |Abigail Pesta |October 10, 2012 |DAILY BEAST 

The whole history of the Old Testament displays nothing but the vain efforts of God to vanquish the obstinacy of his people. Letters To Eugenia |Paul Henri Thiry Holbach 

Lieut. Wilkinson and party appear in good spirits, and show a disposition which must vanquish every difficulty. The Expeditions of Zebulon Montgomery Pike, Volume II (of 3) |Elliott Coues 

Jerry, who had been told of the trouble, was ready to descend upon the entire college and vanquish it single-handed. Marjorie Dean College Freshman |Pauline Lester 

After all, it was more important that she should vanquish her enemies than prove to a mere man that they really were her enemies. The Backwoodsmen |Charles G. D. Roberts 

He had met at last that which must vanquish all his resolutions, and turn all his desperate efforts into vanity. Greifenstein |F. Marion Crawford