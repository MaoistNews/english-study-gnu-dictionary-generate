Serrations are designed to cut rope or cord but also reduce the portion of a blade that can be used to perform other cutting tasks, and they’re difficult to sharpen. Three Questions to Ask Yourself Before Buying a Knife |Wes Siler |September 3, 2020 |Outside Online 

In traditional cultures, this might involve things like making rope, gathering and processing food, constructing houses, and firing pottery. No, Animals Do Not Have Genders - Facts So Romantic |Cailin O'Connor |August 26, 2020 |Nautilus 

If you’re looking for indoor activities, think about streaming a yoga class or finding a jump rope. How to cope as COVID-19 imposes social distancing |Sheila Mulrooney Eldred |March 23, 2020 |Science News For Students 

If you shake it up and down, you create a wave, with the rope as your medium. Explainer: Understanding waves and wavelengths |Jennifer Look |March 5, 2020 |Science News For Students 

The piece of rope touching your hand doesn’t move away from your hand. Explainer: Understanding waves and wavelengths |Jennifer Look |March 5, 2020 |Science News For Students 

She tugged on the black rope that wrapped around his thighs and torso, her leather gloves creaking with each adjustment. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

“I like decorating my slaves,” she said, referencing the rope, her thin, crimson-coated lips peeling off her front teeth. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

From the roof of the barn is a long loop of rope, through this the turkey is suspended by its legs. Confessions of a Turkey Killer |Tom Sykes |November 26, 2014 |DAILY BEAST 

Clinton, meanwhile, spent several minutes greeting audience members along the rope line and posing for cellphone selfies. Hillary Clinton Basks in Labor’s Love: ‘This Is Like a Homecoming!’ |David Freedlander |September 16, 2014 |DAILY BEAST 

After all, what politician blurts out a major life decision while working a rope line? Bill Clinton's McConnell Attack May Be What We'll Remember From the Steak Fry |Ben Jacobs |September 15, 2014 |DAILY BEAST 

It occurred to him then, for the first time, that a third resource was open—he might cut the rope, and let the kite go free! The Giant of the North |R.M. Ballantyne 

Once the rope got tangled around Squinty's foot, and he jumped over it to get free. Squinty the Comical Pig |Richard Barnum 

All this while Squinty was chewing on the apple which he had picked up from the ground after he had jumped over the rope. Squinty the Comical Pig |Richard Barnum 

Every few days after that the boy took Squinty out of his pen, and let him do the rope-jumping and the acorn-hunting tricks. Squinty the Comical Pig |Richard Barnum 

And it did not take Squinty long to learn to jump the rope when there was no apple on the other side. Squinty the Comical Pig |Richard Barnum