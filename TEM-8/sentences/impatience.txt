It’s not like we’re unaware of how absurdly fortunate we are even as we slog through this second pandemic year with weary impatience. Superficial Empathy and Watching the Afghan Tragedy On the Little Screen |Susanna Schrobsdorff |August 22, 2021 |Time 

I regret putting other climbers at risk, and I regret the impatience I felt. A Free Soloist Remembers His Yosemite Free Fall |Kevin Johnson |June 29, 2021 |Outside Online 

Maybe it’s the cold and snow blanketing so much more of the country than usual, or maybe it’s our collective impatience with a slow vaccine rollout as we approach the once-unthinkable milestone of a year under varying degrees of lockdown. The 5 Best New Shows Our TV Critic Watched in February 2021 |Judy Berman |February 26, 2021 |Time 

I know that impatience with corporate voice systems is a tiresome, hackneyed gripe. Gene Weingarten: Your call is unimportant to us |Gene Weingarten |January 7, 2021 |Washington Post 

This time, the impatience was evident on both sides of the aisle. Hogan’s first batch of coronavirus tests from South Korea were flawed, never used |Steve Thompson |November 20, 2020 |Washington Post 

American women expressed their support and impatience when fighting puritanism and conservatism using Femen tactics. Femen's Topless Sextremists Invade the US |Lizzie Crocker |February 23, 2014 |DAILY BEAST 

I called to her, but she slipped away with a tormenting smile at my helpless hands, and I followed her with some impatience. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

I waited and waited, closing my eyes with fear and impatience, but all was silent as the grave. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

Similarly, how little time Shostakovich spent on his work elucidates the fever and impatience of his mind. What Do Great Artists’ Routines Reveal? |Alexander Aciman |May 9, 2013 |DAILY BEAST 

It was characterized by apocalyptic and incendiary rhetoric, anger, impatience, and revolutionary zeal. Good Luck With That, Peter Wehner |Michael Tomasky |January 25, 2013 |DAILY BEAST 

I waited three months more, in great impatience, then sent him back to the same post, to see if there might be a reply. The Boarded-Up House |Augusta Huiell Seaman 

Liszt looked at it, and to her fright and dismay cried out in a fit of impatience, "No, I won't hear it!" Music-Study in Germany |Amy Fay 

Felipe was so full of impatience to continue his search, that he hardly listened to the Father's words. Ramona |Helen Hunt Jackson 

But he could not bear the reflection, and with fevered impatience, he hurried through the business of the morning. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Perhaps their course is wiser than that which hot impatience would prompt—nay, I believe it is. Glances at Europe |Horace Greeley