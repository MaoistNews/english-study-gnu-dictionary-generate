He had to wait until he was in college, when he landed a research position that required him to trap star-nosed moles in Pennsylvania’s wetlands. ‘Great Adaptations’ unravels mysteries of amazing animal abilities |Erin Garcia de Jesus |October 12, 2020 |Science News 

I had only a vague understanding of all this when I set out for Pennsylvania in a rented cargo van with a sleeping bag and a cooler full of hundreds of earthworms to feed hungry moles. How to hunt for star-nosed moles (and their holes) |Kenneth Catania |September 15, 2020 |Popular-Science 

The supposed mole who stole nuclear secrets has never been found. A brief history of US-China espionage entanglements |Konstantin Kakaes |September 3, 2020 |MIT Technology Review 

About a year earlier, veterinarians at Texas A&M extracted DNA from one of Chance’s moles and used the sample to create a genetic double. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

A new mole popping up is just a new mole, because we don’t consider it to be a potential threat of cancer. Black May Not Crack, But It Definitely Will Burn |Shirley Carswell |May 14, 2019 |TruthBeTold.news 

To his detractors, he was a half-mad paranoiac who nearly destroyed the CIA in his obsessive search for a Soviet mole. The Bizarre Tale of Ben Bradlee, JFK, and the Master Spy |Will Rahn |October 22, 2014 |DAILY BEAST 

Last season was definitely challenging, because we were not allowed to mention them or allude to a mole of any kind. The Leaner, Meaner Season 2 of ‘Marvel’s Agents of S.H.I.E.L.D.’ |Jason Lynch |September 22, 2014 |DAILY BEAST 

Alasania said it was important to focus the mole hunt on the army. What to Do When Russia Invades Your Country |Eli Lake |May 7, 2014 |DAILY BEAST 

She was briefly outed by Red as a mole and then allowed to recede into the background again. ‘The Blacklist’ Is Dead Without the Psychotic Red |Jason Lynch |March 31, 2014 |DAILY BEAST 

Rocket teamed up with the Incredible Hulk to overthrow Judson Jakes, a devious mole. 11 Things to Know About Bradley Cooper’s Rocket Raccoon in Guardians of the Galaxy |Marina Watts |February 19, 2014 |DAILY BEAST 

He grew very restless, for it was a long time before Grandfather Mole appeared. The Tale of Grandfather Mole |Arthur Scott Bailey 

But she told Grandfather Mole that it was all right—that she knew a person of his age ought not to go without his breakfast. The Tale of Grandfather Mole |Arthur Scott Bailey 

And he had waited so long for Grandfather Mole that he had begun to feel hungry again. The Tale of Grandfather Mole |Arthur Scott Bailey 

One thing was certain: Grandfather Mole could travel much faster through the water than he could underground. The Tale of Grandfather Mole |Arthur Scott Bailey 

Then Jimmy remembered suddenly that he had to meet Grandfather Mole over there. The Tale of Grandfather Mole |Arthur Scott Bailey