Jason Motlagh, who’s been reporting in Afghanistan off and on for the past 15 years, wrote an unforgettable Outside story in 2017 about the wild and dangerous horseback sport called buzkashi. Operation Afghanistan Rescue |aheard |August 21, 2021 |Outside Online 

People formed a search posse on horseback, and it was just a fear that we were going to find bodies. The ballad of the Chowchilla bus kidnapping |Kaleb Horton |July 23, 2021 |Vox 

Two hundred years later, another British doctor, Edward Tilt, suggested that young women could irritate their ovaries by horseback riding or traveling by train while menstruating — especially if they were nervous or had long eyelashes. How medicine sought to control women’s bodies while ignoring their symptoms |Susan Okie |July 2, 2021 |Washington Post 

In addition to shuttle services, many destination resorts near national parks offer activities like horseback riding, cycling, fishing, hiking, and more, along with nicer dining facilities than you’re likely to find inside a park’s boundaries. What National Parks’ Record Summer Means for Your Visit |Wes Siler |June 25, 2021 |Outside Online 

My horseback teacher’s mom got sick, so we were meant to be away. 'A Year Full of Emotions.' What Kids Learned From the COVID-19 Pandemic |Allison Singer |June 12, 2021 |Time 

The brand logo turned out to feature a graceful archer on horseback, in a Tatar national costume, poised to shoot his arrow. Rebranding The Land of Mongol Warriors & Ivan The Terrible |Anna Nemtsova |December 25, 2014 |DAILY BEAST 

This music video is an ode to his one true love, complete with romantic rides on horseback. Swimming Owls, Jane Krakowski’s Peter Pan Live! Audition, and More Viral Videos |The Daily Beast Video |December 7, 2014 |DAILY BEAST 

Instead of statues of generals, we put up monuments to ordinary soldiers, lists of names rather than men on horseback. Why Some Americans Are More Equal Than Others |Jedediah Purdy |September 2, 2014 |DAILY BEAST 

Two wranglers on horseback chased 20 bulls from a holding pen. Chicago’s Running of the Bulls |Hampton Stevens |July 26, 2014 |DAILY BEAST 

Through the years he became just as adept at politics as he was on horseback. The Cowboy Sheriff of Las Vegas Rides Into ‘Mob Museum’ |John L. Smith |June 8, 2014 |DAILY BEAST 

The troopers slashed at the men on foot and the sepoys fired indiscriminately at any one on horseback. The Red Year |Louis Tracy 

“Steed”—Jefferson rode on horseback to the Capitol to take his oath of office as President. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

The party was made up of six men on horseback, two tame buffaloes, and a pack of immense dogs used to hunting. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

Let my litter be prepared, and send men on horseback to provide relays of carriers every ten miles. The Red Year |Louis Tracy 

If God put a beggar on horseback, would the horse be blamable for galloping to Monte Carlo? God and my Neighbour |Robert Blatchford