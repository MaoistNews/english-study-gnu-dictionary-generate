Wherever you go, the Talon will help turn your car camping site into a true base camp. Three Family-Friendly Adventures to Try This Fall |Outside Editors |September 17, 2020 |Outside Online 

In backcountry areas where bruins and other animals want your food, hanging it in a tree downwind of camp will keep it out of reach and reduce nighttime animal prowlers around your tent. This essential survival tool can save your life 10 different ways |By Tim MacWelch/Outdoor Life |September 15, 2020 |Popular-Science 

Overall, athletes had successful camps in 46 of the 82 cases, for a hit rate of 56 percent. Why Altitude Training Helps Some but Not Others |Alex Hutchinson |September 11, 2020 |Outside Online 

This summer, consumer financial services company Synchrony introduced Synchrony summer camps, virtual summer camp experiences for 3,700 children of employees. The best back-to-school benefits companies are offering their employees |ehinchliffe |September 10, 2020 |Fortune 

At the beginning of camp, students often reported that struggling with math was a sign you weren’t doing well. A secret of science: Mistakes boost understanding |Rachel Kehoe |September 10, 2020 |Science News For Students 

Pitchfork called him a “a rap-obsessed misfit from a summer camp who freestyles poorly” who is “ridiculous without knowing it.” The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

Within a concentration camp, would someone make a joke about the number, the tattooed number? Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

For now, Sabrine continues her daily routine of visits to the protest camp, to political leaders and taking care of the twins. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

In the summer of 2014, they both were sentenced to 4-1/2 years in a labor camp. Behind Bars for the Holidays: 11 Political Prisoners We Want to See Free In 2015 |Movements.Org |December 25, 2014 |DAILY BEAST 

The prison camp island nation known as Cuba erupted in celebration. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

"A camp-fire would hardly flash and die out like that, Sarge," he answered thoughtfully. Raw Gold |Bertrand W. Sinclair 

In the most perfect stillness, we arrived within two hundred paces of the enemy's camp. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The camp grew still, except for the rough and ready cook pottering about the fire, boiling buffalo-meat and mixing biscuit-dough. Raw Gold |Bertrand W. Sinclair 

Meanwhile, he had been selected as aide-de-camp by General d'Ure de Molans. Napoleon's Marshals |R. P. Dunn-Pattison 

Thus the whole State became one vast armed camp, nearly forty thousand men on a side, arrayed against each other. The Courier of the Ozarks |Byron A. Dunn