Finally ready for retirement, the couple built a farmhouse in central California. How a Literary Road Trip Helped Me Cope with Infertility |smurguia |August 17, 2021 |Outside Online 

The neighboring farmhouse has two three-bedroom units, perfect for a family or group of friends. Our Favorite New Cycling Base Camps |eriley |July 27, 2021 |Outside Online 

End an excellent day in the Hill Country with a farmhouse ale from the 100 percent philanthropic Save the World Brewing Co. The 5 Best Water Adventures in Texas |kklein |July 8, 2021 |Outside Online 

The survival of these farmhouses is so concerning that the World Monuments Fund has placed them on its watch list, while helping to raise funds for renovations, advocacy and owner support. How to Save the Structures We Love Most |Stephen Starr |June 28, 2021 |Ozy 

Last week, the kids were on Easter holiday from school and we went and stayed at an old farmhouse in the middle of nowhere. The Monk Who’s Helping the World Navigate Meditation, One Day at a Time |Pallabi Munsi |April 30, 2021 |Ozy 

A local Kurdish farmer later took me to a border spot near his farmhouse. Turkish President Kisses Off Kurds Under Siege By ISIS |Jamie Dettmer |October 7, 2014 |DAILY BEAST 

We stood on the rooftop of a derelict farmhouse meters away from a Turkish tank and a razor wire fence marking the end of Turkey. Impotent U.S. Airstrikes, Passive Turks and an ISIS Triumph |Jamie Dettmer |October 3, 2014 |DAILY BEAST 

My sister found it in the back of one of the farmhouse closets. How Gettysburg Did Not Unlock the Past |Laird Hunt |September 21, 2014 |DAILY BEAST 

They are both in the study of my old farmhouse, in a room that has three nice sized windows, each with a lovely, bucolic view. How I Write: Scott Spencer |Noah Charney |March 13, 2014 |DAILY BEAST 

The $5 million property as described by the New York Times, has “rolling fields and a farmhouse once owned by a Vanderbilt.” Sean Eldridge, Husband of Facebook Mogul Chris Hughes, Running For Congress |Ben Jacobs |September 23, 2013 |DAILY BEAST 

What might be of a disturbing nature in the old farmhouse could not, she thought, be as fearsome as the approaching tempest. The Campfire Girls of Roselawn |Margaret Penrose 

Just as Mr. Darwood drove around to the door with his sleigh Andy came back to the farmhouse. The Mystery at Putnam Hall |Arthur M. Winfield 

The Darwood farmhouse set back from the road, among some cedar trees. The Mystery at Putnam Hall |Arthur M. Winfield 

Here and there was a farmhouse or a country home on a slope, set in the midst of fields just turning green. Ancestors |Gertrude Atherton 

When Willy was two years old, he lived in a red farmhouse with a yard in front of it. The Nursery, August 1873, Vol. XIV. No. 2 |Various