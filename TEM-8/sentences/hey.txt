Everywhere I go, ‘Hey Cartman, you must like Family Guy, right?’ Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

Hey, whatever keeps those lecherous freaks from sexually assaulting humans is fine by us. Zebra Finches, Dolphins, Elephants, and More Animals Under the Influence |Bill Schulz |December 31, 2014 |DAILY BEAST 

Then, last year, she received a call from Anderson: “Hey, do you remember when we met over ten years ago?” Jena Malone’s Long, Strange Trip From Homelessness to Hollywood Stardom |Marlow Stern |December 22, 2014 |DAILY BEAST 

But hey, maybe anti-regime posters wrote those, this being one place where it is open to being cyber-slimed. Inside the ‘Surprisingly Great’ North Korean Hacker Hotel |Michael Daly |December 20, 2014 |DAILY BEAST 

On the one hand, these results say: Hey, there is real support for Democrats in the South. Seriously, Democrats: You’re Done in Dixie |Michael Tomasky |December 10, 2014 |DAILY BEAST 

Und you vos go owid on der blains und catch some counterfeiters, hey? The Rover Boys on the Farm |Arthur M. Winfield (AKA Edward Stratemeyer) 

By the way, I've just found out why you was so anxious to get into this house, hey? Wayside Courtships |Hamlin Garland 

He stopped in his dressing to say, "We've struck a great boarding place, hey?" Wayside Courtships |Hamlin Garland 

Welting a man on the head with a whip-stock ain't anything, hey? Wayside Courtships |Hamlin Garland 

But the land is an enemy to be feared, while the Frenchman is not—hey! The Two Admirals |J. Fenimore Cooper