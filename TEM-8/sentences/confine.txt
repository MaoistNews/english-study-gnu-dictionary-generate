For starters, more people will probably watch the game within the confines of their homes. How CBS will expand Super Bowl streaming viewership this year |Tim Peterson |February 1, 2021 |Digiday 

From his confines in a Melbourne hotel room, the world’s 70th-ranked men’s tennis player is staging a clinic on the sport’s mental game. A tennis player has turned his Australian Open quarantine into a party of one |Liz Clarke |January 21, 2021 |Washington Post 

Due to close confines, too many deaths among inmates and correction officers have occurred nationally, and family and friend visitations are canceled. Hints From Heloise: Send a letter to your future self |Heloise Heloise |January 21, 2021 |Washington Post 

The trick is still mostly experimental, but it’s an example of thinking outside the artificial confines of a particular AI domain. 2021 Could Be a Banner Year for AI—If We Solve These 4 Problems |Shelly Fan |January 5, 2021 |Singularity Hub 

Experts in psychology and decision-making say hostility toward wearing masks, even within the shared confines of a passenger jet, has been fueled by politicization — but also by skewed incentives and inconsistent messaging. Sneezed on, cussed at, ignored: Airline workers battle mask resistance with scant government backup |Michael Laris |January 1, 2021 |Washington Post 

It is naïve to imagine that a militarized police will confine itself to surgical strikes in crime-ridden areas. What’s Next, Police With Tanks? |James Poulos |June 28, 2014 |DAILY BEAST 

These questions simply will not confine themselves to quiet rooms. Paul Begala: The Strangely Silent Jan. 23 Debate in Tampa |Paul Begala |January 24, 2012 |DAILY BEAST 

In war, for instance, we certainly mean to confine our aspirations for life to ourselves and our allies. Let the Death Penalty Die |Thomas Cahill |March 10, 2009 |DAILY BEAST 

Miss Gould will confine her lectures this week in English to the discussion of plays and play-making. The Girls of Central High on the Stage |Gertrude W. Morrison 

No third mode of origin can be conceived, and we may safely confine ourselves to a review of these two claims. Man And His Ancestor |Charles Morris 

If he confine that slave in his house, and afterwards the slave has been seized in his hand, that man shall be put to death. The Oldest Code of Laws in the World |Hammurabi, King of Babylon 

So, we will name no chemicals or poisons but confine ourselves to effects and processes. Hooded Detective, Volume III No. 2, January, 1942 |Various 

We cannot confine the first refrain to one line only, as there is no stop at the end of l. 14. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer