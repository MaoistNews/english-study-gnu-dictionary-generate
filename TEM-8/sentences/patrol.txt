Brooks recounts with vivid detail her experiences in the police academy and as an officer on patrol. A Georgetown professor trades her classroom for a police beat |Ronald S. Sullivan Jr. |February 12, 2021 |Washington Post 

High-paying “extra duty” jobs — like sitting in a patrol car monitoring traffic at a road construction site — are also protected by the contracts. How the Police Bank Millions Through Their Union Contracts |by Andrew Ford, Asbury Park Press, and Agnes Chang, Jeff Kao and Agnel Philip, ProPublica |February 8, 2021 |ProPublica 

Anderson says the 9-year-old started screaming, “I want my dad,” and resisted the officers’ efforts to get her into the patrol car. Rochester Police Suspended After Handcuffing and Pepper Spraying 9-Year-Old Girl |Shani Parrish |February 2, 2021 |Essence.com 

Two days later, Emery ordered the agency to study the issue of chokeholds in a bid to understand why they were the focus of so many complaints against police officers despite the patrol guide’s ban. Still Can’t Breathe |by Topher Sanders, ProPublica, and Yoav Gonen, THE CITY, video by Lucas Waldron, ProPublica |January 21, 2021 |ProPublica 

To date, none of the patrol unions have ever gone on strike. Why Ski Patrollers Are Picketing at Two Vail Resorts |Scott Yorko |January 20, 2021 |Outside Online 

Then they came up against a police patrol on mountain bicycles, which again led to more shooting, without injuries. Police Hunt for Paris Massacre Suspects |Tracy McNicoll, Christopher Dickey |January 7, 2015 |DAILY BEAST 

“They just walk around, they ride in their patrol cars, and they just pass by,” he said. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

With the midterm elections safely in the rearview mirror, Obama is on legacy patrol. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

Brinsley stepped up to the passenger side of the patrol car, raised a silver Taurus semi-automatic pistol and began firing. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

Hundreds of cops saluting as the bodies were rolled out with a full escort by highway patrol. Anger at The Cop Killer - And The Police |M.L. Nestel |December 21, 2014 |DAILY BEAST 

"Hon'lable p'lice patrol come 'long plenty soon," murmured Sin Sin Wa. Dope |Sax Rohmer 

They mewed like cats at the approach of the patrol, and crowed like cocks when a likely victim approached. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

He was not of the Allied Patrol nor of any branch of the police force that encircled the world in its operations. Astounding Stories, May, 1931 |Various 

Nor did the voluble and sulphurous orders to halt that a patrol-ship flashed north. Astounding Stories, May, 1931 |Various 

The patrol-ship was on station; she was lost far astern before she could gather speed for pursuit. Astounding Stories, May, 1931 |Various