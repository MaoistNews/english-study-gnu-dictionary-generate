It was just the anonymous artist and his group of pranksters using New York as their unofficial playground. Catch Him If You Can: Reliving Banksy’s New York Invasion |Alex Suskind |November 14, 2014 |DAILY BEAST 

The video was also posted on the Twitter account of “a3maq news,” which acts as an unofficial media arm of ISIS. ISIS Video: America’s Air Dropped Weapons Now in Our Hands |Josh Rogin |October 21, 2014 |DAILY BEAST 

The story of their visit quickly spread among the ‘Gringo Trail’ and McFadden lucked into a business as the unofficial tour guide. Cocaine, Politicians and Wives: Inside the World’s Most Bizarre Prison |Jason Batansky |October 12, 2014 |DAILY BEAST 

TEL AVIV – If hell were a travel destination, Qusay Omran would be its unofficial tour guide. Beating Cancer & Dodging Israel's Bombs |Itay Hod |September 1, 2014 |DAILY BEAST 

Labor Day marks the unofficial end of summer and the unofficial start of the general election. Just Tuning Into The Mid-Terms? Here are Five, Weird, Unpredictable Senate Races to Watch |Ben Jacobs |September 1, 2014 |DAILY BEAST 

There are also executive and legislative councils, unofficial nominated members serving on the last-named council. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

He is assisted by an executive council of three official and three unofficial members. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

Congress, though it refused to sanction any unofficial negotiations, sent commissioners from its own body to confer with him. The Political History of England - Vol. X. |William Hunt 

For the truth is, no unofficial eye can see the Crown-Prince, or know what state he is in. History Of Friedrich II. of Prussia, Vol. VII. (of XXI.) |Thomas Carlyle 

The musicians played energetically, switching now from the hymn to their unofficial little ditty. Pagan Passions |Gordon Randall Garrett