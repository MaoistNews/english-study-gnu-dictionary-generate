The three-page letter arrived at GameStop’s Texas headquarters 18 months before the company unexpectedly emerged as the hottest stock on Wall Street and the latest symbol of a widening populist revolt against entrenched elites. The GameStop stock craze is about a populist uprising against Wall Street. But it’s more complicated than that. |David J. Lynch |February 1, 2021 |Washington Post 

Humans were doomed in the play even before Radius led the revolt. The first ‘robots’ were made of flesh and bone |By John M. Jordan/MIT Press Reader |January 26, 2021 |Popular-Science 

Chekheria graduated college in Tbilisi with a law degree just a year before another revolt, the Rose Revolution of late 2003. A Pandemic Tourism Pivot From Cool to Wellness |Dan Peleschuk |August 11, 2020 |Ozy 

In 1794, George Washington himself led a militia of 13,000 men into Pennsylvania to put down an anti-tax revolt. What Happened In Portland Shows Just How Fragile Our Democracy Is |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |August 5, 2020 |FiveThirtyEight 

The post Facebook in the age of revolt appeared first on Digiday. Facebook in the age of revolt |Brian Morrissey |July 10, 2020 |Digiday 

The ISI came to the CIA for assistance in fostering a revolt that had developed in the Afghan countryside against Communist rule. CIA Agents Assess: How Real Is ‘Homeland’? |Chuck Cogan, John MacGaffin |December 15, 2014 |DAILY BEAST 

A political leader told us parliament won't do anything unless people revolt. Will the Swiss Quit Cooking their Kittens and Puppies? |Barbie Latza Nadeau |November 30, 2014 |DAILY BEAST 

Initially, Truth Revolt printed that Dunham was 17 when this event occurred (she was 7). Welcome to Generation Overshare: Lena Dunham, Taylor Swift, and the Politics of Self-Disclosure |Marlow Stern |November 6, 2014 |DAILY BEAST 

Fallin reversed course on that as well, but not before a mini-revolt grew among suburban parents. The Democrats’ Great Plains Firewall: Can Joe Dorman Take the Oklahoma Statehouse? |David Freedlander |October 3, 2014 |DAILY BEAST 

A problem far more pressing for the dynasty was the Taiping revolt, which ran from 1850 to 1864 and left tens of millions dead. We're Still Fighting the Opium Wars |William O’Connor |August 28, 2014 |DAILY BEAST 

The rapid spread of the revolt was not a whit less marvelous than its lack of method or cohesion. The Red Year |Louis Tracy 

Yet, so curiously constituted is the native mind, the blowing-up of the magazine was the final tocsin of revolt. The Red Year |Louis Tracy 

The news of Bruce's revolt and the death of Comyn roused Edward into full martial vigour. King Robert the Bruce |A. F. Murison 

General Pio del Pilar slept in the city every night, ready to give the rocket-signal for revolt. The Philippine Islands |John Foreman 

In vain he warned the King that this was not a revolt but a revolution; the counsels of Polignac were all powerful. Napoleon's Marshals |R. P. Dunn-Pattison