Attempts to promote new fashions, harness the “propulsive power of envy,” and boost sales multiplied in Britain in the late 18th century. The making of our consumer culture |Kerryn Higgs |January 23, 2021 |Quartz 

“We are committed to making our rail operations control center a safety standard-bearer and envy of the transit industry,” Wiedefeld said. Metro disputes audit that claimed rail control center was ‘toxic workplace’ environment |Justin George |December 11, 2020 |Washington Post 

Disney has experimented with the premium digital release of “Mulan” and the upcoming Pixar release “Soul,” but its box-office might has been the envy of Hollywood. Disney plans to stream a galaxy of Star Wars, Marvel series |Verne Kopytoff |December 10, 2020 |Fortune 

Find people to spend your life with who have similar money views as you and it will save you a lot of unnecessary stress, envy and wasteful spending. The 20 most important personal finance laws to live by |Ben Carlson |December 8, 2020 |Fortune 

Seeing a new group arrive from the city, with all their toes intact, shiny hair, “fat and delicious-looking,” Agnes drools—maybe with envy, maybe because they so appetizingly resemble healthy livestock in her world of jerky and morning mush. 3 New Novels Show a Natural World in Peril |Erin Berger |October 3, 2020 |Outside Online 

I envy my refusenik friends their steadfast commitments to stay in, and contentment in doing so. The Refuseniks Hiding From ‘Happy New Year’ |Lizzie Crocker |December 31, 2014 |DAILY BEAST 

Europeans seem to find them exotic, an odd case of culture-envy in reverse. Beer Countries vs. Wine Countries |Clive Irving |December 7, 2014 |DAILY BEAST 

I don’t think that happened in the ‘90s and the ‘80s and I wonder if there’s a purity to that that I envy sometimes. How Aidy Bryant Stealthily Became Your Favorite ‘Saturday Night Live’ Star |Kevin Fallon |October 31, 2014 |DAILY BEAST 

I had found the one and only thing in the entire universe for which Ben Bradlee might envy me: my age. Ben Bradlee Was the Last of the Newspaper Giants |Tom Shales |October 22, 2014 |DAILY BEAST 

I envy Muslims their practice of regular and genuine prayer. Can We Lose the Violent Muslim Cliché? |Jay Parini |October 12, 2014 |DAILY BEAST 

It is then we make him our friend, which sets us above the envy and contempt of wicked men. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Consult not with him that layeth a snare for thee, and hide thy counsel from them that envy thee. The Bible, Douay-Rheims Version |Various 

She expatiated on his father's character; on the envy of his rivals; and dated his fall to their ambition alone. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

She had a graceful figure, and the slender foot below her white piqué skirt was at once the envy and admiration of Aix-les-Bains. The Joyous Adventures of Aristide Pujol |William J. Locke 

Like his father, he had to bear all that Spanish envy and Spanish malignity could inflict. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.