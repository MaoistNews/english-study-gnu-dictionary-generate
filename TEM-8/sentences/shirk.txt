And to shirk that part of our human responsibility is, well, a sin. A Catholic Ex-Banker on Pope Francis’s Radical Views |Chris Lowney |December 15, 2013 |DAILY BEAST 

Employers can't shirk the fines as easily as individual purchasers can. The Obamacare Death Spiral |David Frum |November 4, 2013 |DAILY BEAST 

Should this happen it would be doubly disastrous were we to shirk the challenge now. Margaret Thatcher Sounded the Alarm on Climate Change |David Frum |April 8, 2013 |DAILY BEAST 

They abuse drinks or drugs, abuse spouses and loved ones, shirk sleep and plummet into crippling depression. War Photographer Trains Her Lens on Military Suicide at Home |Allison Yarrow |September 4, 2012 |DAILY BEAST 

“Congress must not shirk its responsibilities,” the White House said in a statement. 'Do-Nothing' Congress a Target for Obama in 2012 |Patricia Murphy |November 12, 2011 |DAILY BEAST 

Tom held to his strange belief to 'Let it all come,' he would not try to prevent; he would neither shirk nor dodge. The Wave |Algernon Blackwood 

You'll work your passage back to fitness quicker than an arm ever did before, you pale-faced shirk! Red Pepper Burns |Grace S. Richmond 

He patted Dorothy on the shoulder and would not permit her to shirk his praise. Hidden Gold |Wilder Anthony 

The situation was a critical one, but Steve's was not a nature to shirk responsibilities or shun sacrifices. The Gentle Art of Cooking Wives |Elizabeth Strong Worthington 

Evelyn was fairly good at school; it was not, she considered, worth her while any longer to 322 shirk her lessons. A Very Naughty Girl |L. T. Meade