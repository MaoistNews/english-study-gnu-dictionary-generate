She did not feel safe, she said, so she moved into her mother’s three-bedroom apartment with her two children, her brother, her sister and her niece. Domestic violence survivors say they were kicked out by the housing program that promised to help them |Rachel Chason, Katie Mettler |October 30, 2020 |Washington Post 

If your nieces and nephews like camping in the back yard, or you have friends who enjoy traveling light, this single-height mattress is a great option. Air mattresses for people who love to entertain and travel |PopSci Commerce Team |September 30, 2020 |Popular-Science 

His former lawyer is set to release a tell-all book about him and his niece already has. Trump’s convention: liars spreading hate |Peter Rosenstein |August 26, 2020 |Washington Blade 

However, it is more like your niece pointing to the rabbits while also understanding that rabbits are animals. Animals That Can Do Math Understand More Language Than We Think |Erik Nelson |June 14, 2020 |Singularity Hub 

By contrast, the desire of his niece, Bethany, to become transhuman through tech implants and bio-hacking is met with anger, fear, and revulsion. Transgender, Transhuman: Technological Advances Offer Increased Choices But Also Create New Prejudices |LGBTQ-Editor |April 9, 2020 |No Straight News 

He is said to have had many affairs, chief among them with Jean Gordon, his half-niece. The Price of Being a Patton: Wrestling With the Legacy of America’s Most Famous General |Tim Teeman |May 26, 2014 |DAILY BEAST 

He hugs his sister, Athelgra, hugs his daughter, Ernestine, hugs his niece Arthel. The Stacks: The Neville Brothers Stake Their Claim as Bards of the Bayou |John Ed Bradley |April 27, 2014 |DAILY BEAST 

Negron was dating Mayor Delle Donna's niece, Rita Perito, and their relationship allegedly turned physically abusive. Did Christie Go Easy on a Human Trafficker Just to Bust a Small-Time Pol? |Olivia Nuzzi |March 17, 2014 |DAILY BEAST 

My ultimate aim is that by the time my niece is eight - she's three now - there won't be a risk of FGM. Camilla Backs FGM Eradication Campaign |Tom Sykes |February 28, 2014 |DAILY BEAST 

I have a 13-year-old niece who is a Belieber (aka big fan of Justin). Justin Bieber's Spiritual Crisis |Joshua DuBois |January 26, 2014 |DAILY BEAST 

One of her humours was to unite the son of her minister, with a niece of the widowed Queen of Saint Germain's. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

It was evident to the German doctor that his patron looked forward to his great-niece's visit with pleasure. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Miss Barrington shook off her anger, and rising, laid a gentle hand on her niece's shoulder. Winston of the Prairie |Harold Bindloss 

A niece is so safe—however good you are at statistics, you can't really prove anything. First Plays |A. A. Milne 

(A little awkwardly) Darling one, I wonder if you'd mind—just at first—being introduced as my niece. First Plays |A. A. Milne