Last year, when California’s utilities first began carrying out widespread blackouts like this, some homes and businesses were left in the dark for days. California faces widespread power cuts after weeks of destructive wildfires |kdunn6 |September 8, 2020 |Fortune 

India has imposed hundreds of internet blackouts in different parts of the country over the past few years, including cutting off connectivity throughout the disputed state of Kashmir for six months. Podcast: How a 135-year-old law lets India shutdown the internet |Anthony Green |September 2, 2020 |MIT Technology Review 

Home to over 12 million people, the region has suffered tremendously as a result—unemployment has spiked and over $1 billion in economic losses have been attributed to the blackout. Podcast: How a 135-year-old law lets India shutdown the internet |Anthony Green |September 2, 2020 |MIT Technology Review 

In the past few years, India has imposed hundreds of internet blackouts in different parts of the country, sometimes just for hours, sometimes for months. Podcast: How a 135-year-old law lets India shutdown the internet |Anthony Green |September 2, 2020 |MIT Technology Review 

Although dawn was yet to break, she immediately set to thinking about what a blackout would mean for her and her work. How India became the world’s leader in internet shutdowns |Katie McLean |August 19, 2020 |MIT Technology Review 

And so as Friday dawned, the Great CBS Blackout of 2013 entered its eighth day. Why Time Warner Cable Can’t Cave to CBS’s Demands |Daniel Gross |August 10, 2013 |DAILY BEAST 

The Great Blackout of 2011 gridlocked traffic, closed schools and canceled flights. It’s the Electric Grid, Stupid |Daniel Stone |September 10, 2011 |DAILY BEAST 

Well, he certainly wasn't much of a perceptive, or he would have been able to handle the Blackout himself. Vigorish |Gordon Randall Garrett 

My Blackout victim was reaching out, trying to find something he could use to raise himself to his feet. Vigorish |Gordon Randall Garrett 

I picked her up in my arms and carried her to the same sawdust-strewn private dining room where I'd given Barney the Blackout. Vigorish |Gordon Randall Garrett 

A Blackout is quite effective—it's hard to hit what you can't see. Vigorish |Gordon Randall Garrett 

But Barney, the stick-man who'd felt my Blackout, caught on a lot quicker. Vigorish |Gordon Randall Garrett