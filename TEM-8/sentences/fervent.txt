This despite the fact that many have been possibly identified by others online in a fervent effort to bring the perpetrators to justice. A Week After the U.S. Capitol Attack, Many Involved Are Still Walking Free Despite Online Efforts to Identify Them |Megan McCluskey |January 13, 2021 |Time 

That’s a popular passage among liberal Christians who find many conservatives’ fervent support for the military to be in conflict with some Christian teaching. What you need to know about Raphael Warnock |Eugene Scott |January 6, 2021 |Washington Post 

One longs, in this particular moment, to see data on how the most fervent fans of “Jeopardy!” In the dumbest and darkest of times, Alex Trebek maintained a safe space for intelligence |Hank Stuever |November 8, 2020 |Washington Post 

Among Us’s simplicity is reminiscent of Animal Crossing, which similarly has a fervent fan base dating back to the early days of the pandemic, when its explosion in popularity led to a worldwide shortage of Nintendo Switches. AOC’s Among Us livestream hints at Twitch’s political power |Tanya Basu |October 21, 2020 |MIT Technology Review 

It is incumbent upon us not to drop the ball this time, and channel all these fervent sentiments into real action and change. The long path to inclusivity |Katie McLean |October 20, 2020 |MIT Technology Review 

Ramos was a fervent Mets fan and he would often talk to the students about sports. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

So to hear such fervent anti-Ortega sentiment from previously devoted campesinos and compañeros is unprecedented. China’s Nicaragua Canal Could Spark a New Central America Revolution |Nina Lakhani |November 30, 2014 |DAILY BEAST 

And when those final fervent lines— One way or another One way or another One way or another; This darkness got to give. The Stacks: Grateful Dead I Have Known |Ed McClanahan |August 30, 2014 |DAILY BEAST 

I know as much as anyone how much her most fervent supporters want Hillary Clinton to run for president. Don’t Run for President, Hillary. Become a ‘Post-President’ Instead |Tina Brown |May 2, 2014 |DAILY BEAST 

The political world and her most fervent fans may be exercised about a presidential bid. Don’t Run for President, Hillary. Become a ‘Post-President’ Instead |Tina Brown |May 2, 2014 |DAILY BEAST 

Ronald bent and kissed the speaker, with a fervent hope that everything would end well. The Weight of the Crown |Fred M. White 

How fervent their prayers for their companions in tribulation, when they themselves stood in jeopardy every hour! The Catacombs of Rome |William Henry Withrow 

Much patience, devotion to the child, and fervent prayer will be needful to accomplish anything worth while. The value of a praying mother |Isabel C. Byrum 

Phinees, our father, by being fervent in the zeal of God, received the covenant of an everlasting priesthood. The Bible, Douay-Rheims Version |Various 

Enid's true, overburdened heart was only too ready to respond to his fervent appeal. The Doctor of Pimlico |William Le Queux