Citizen science is a big part of park ecology science, she says, so if visitors see and photograph tracks or scat, or participate in surveys like with the Cascades Wolverine Project, reports that ecologists can follow up on. Mount Rainier’s first wolverine mama in a century is a sign of the species’ comeback |Hannah Seo |August 28, 2020 |Popular-Science 

In the early ’80s, I remember writing a proposal to study the interactions and ecology of microorganisms. He Found ‘Islands of Fertility’ Beneath Antarctica’s Ice |Steve Nadis |July 20, 2020 |Quanta Magazine 

He also has studied hornet ecology at the Taiwan Forestry Research Institute. What you need to know about ‘murder hornets’ |Susan Milius |July 20, 2020 |Science News For Students 

She’s a biologist who studies evolution and ecology at the Center for Ecological Research in Debrecen, Hungary. Pooping ducks can shed the live eggs of fish |Carolyn Wilke |July 17, 2020 |Science News For Students 

The idea has provoked debate across the fields of ecology and animal behavior for more than two decades. Random Search Wired Into Animals May Help Them Hunt |Liam Drew |June 11, 2020 |Quanta Magazine 

The gym—a fragile collective of human ecology at the best of times—has suddenly become even more tense. How to Survive the New Year ‘Gympocalypse’ |Tim Teeman |January 6, 2015 |DAILY BEAST 

He could be remade into a defender of the environment, a preserver of habitats and champion of rainforest ecology. Can Tarzan of the Apes Survive in a Post-Colonial World? |Ted Gioia |November 23, 2014 |DAILY BEAST 

In the case of great apes at least, a new study in the Journal of Animal Ecology suggests the answer is yes. How Gorillas Are Outsmarting Ebola |Melissa Leon |September 1, 2014 |DAILY BEAST 

And of course one needs to convince the French Ministries of Culture and of Ecology that the application makes sense. The Next UNESCO World Heritage Site: Burgundy’s Pinot Noir Country? |Jordan Salcito |May 31, 2014 |DAILY BEAST 

“I was pleasantly surprised,” Rachel McQueen, assistant human ecology professor at the university, said. Levi’s CEO Chip Bergh Hasn’t Washed His Jeans in a Year—And That’s Perfectly Fine! |Erin Cunningham |May 21, 2014 |DAILY BEAST 

They were well informed in plant and animal ecology, and in knowledge of range of species. Prairie Smoke (Second Edition, Revised) |Melvin Randolph Gilmore 

The youngest branch of Botany is Ecology or the study of vegetation in relation to habitat—particularly soil in its widest sense. Makers of British Botany; a collection of biographies by living botanists |Various 

I wonder if modern ecologists know of these records made long before ecology was invented? Makers of British Botany; a collection of biographies by living botanists |Various 

Variation, distribution, and ecology of the Mexican teiid lizard Cnemidophorus calidipes. The Amphibians and Reptiles of Michoacn, Mxico |William E. Duellman 

Notes on the variation, distribution, and ecology of the iguanid lizard Enyaliosaurus clarki. The Amphibians and Reptiles of Michoacn, Mxico |William E. Duellman