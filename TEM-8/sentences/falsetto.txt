One section that gets the blood pumping more than any other is the second half of the chorus, in which the low-toned voices of rappers singing “baby hold on” are woven together with vocalists’ light falsettos in a dynamic contrast of vocal timbres. The Best K-Pop Songs and Albums of 2021 |Kat Moon |December 8, 2021 |Time 

You might laugh at first at his ridiculous falsetto, and then you might blast the song 20 times in a row due to its relentless exuberance. The 10 Best Songs of 2021 |Cady Lang |December 5, 2021 |Time 

While many of IU’s tracks are sung in a weightier fashion, her singing here is airy and delicate —whether in the soft falsetto or the whispery ooh’s that fill the post-chorus. The Best K-Pop Songs of 2021 So Far |Kat Moon |July 1, 2021 |Time 

He made his first digital splashes back in 2011 as a mystery man, refusing to assign a name or face to his voice — a floating, yearning falsetto that sounded like it was trying to escape the loneliness of living inside the body that had produced it. At the weirdest Super Bowl, the Weeknd came to play |Chris Richards |February 8, 2021 |Washington Post 

Fans of Earth, Wind & Fire need only hear a few notes of Philip Bailey’s iconic falsetto before they start to swoon, sway and sing along. Earth, Wind & Fire’s Philip Bailey Hosts Virtual Holiday Gala |cmurray |December 3, 2020 |Essence.com 

They wrote big songs with big falsetto choruses designed for big sing-alongs in big stadiums. Gwyneth Paltrow Haunts Coldplay’s Self-Conscious Breakup Album ‘Ghost Stories’ |Andrew Romano |May 20, 2014 |DAILY BEAST 

And he starts to sing it, filling the room with his sweet, fragile tenor, shifting to a falsetto that cuts straight to the heart. The Stacks: The Neville Brothers Stake Their Claim as Bards of the Bayou |John Ed Bradley |April 27, 2014 |DAILY BEAST 

Unlike his falsetto and his "cool dad" penchant for fedoras, this rumored affair is just so not cute. Marry, Screw, Kill: Lindsay Lohan Sex List Scandal Edition |Amy Zimmerman |March 12, 2014 |DAILY BEAST 

His three-octave falsetto was also used to good advantage in Mars Attacks! The Deaths You Missed This Year |Malcolm Jones, Jimmy So, Michael Moynihan, Caitlin Dickson |December 30, 2013 |DAILY BEAST 

That high falsetto is frighteningly convincing, which raises the question: could a man really have pulled it off? Listen Here: Voicemails From Manti Te’o’s ‘Girlfriend’ |Lizzie Crocker |January 24, 2013 |DAILY BEAST 

"Well, if it ain't ole Turkeyneck in person," he called in a high falsetto voice, as the two entered. David Lannarck, Midget |George S. Harney 

At last his companion had got into the habit of looking up at him whenever he cried in a falsetto voice, "Mignonne." A Passion in the Desert |Honore de Balzac 

The trolley-wire, lifting a whole city home to supper, is a giant with a falsetto voice. The Voice of the Machines |Gerald Stanley Lee 

After the squalling falsetto had implored for a long time, the assailant at last gave over the exercise. The Landloper |Holman Day 

The employment of the Falsetto at any time, either in speaking or reading, is of doubtful taste. The Ontario Readers: The High School Reader, 1886 |Ministry of Education