So I started to think about anything in my life that would be worth people giving it any amount of time. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

The number of diplomats was pitiful (45 appointees in 1860), as was the amount of money allocated to them. U.S. Embassies Have Always Been for Sale |William O’Connor |January 2, 2015 |DAILY BEAST 

The amount of vanished bitcoins was 650,000 BTC (or 24.7 billion yen). Japanese Bitcoin Heist ‘an Inside Job,’ Not Hackers Alone |Nathalie-Kyoko Stucky, Jake Adelstein |January 1, 2015 |DAILY BEAST 

They are afflicted with “progressive spiritual emptiness,” he said, which no amount of academic honors and degrees can fill. Pope Francis Denounces the Vatican Elite’s 'Spiritual Alzheimer’s' |Barbie Latza Nadeau |December 23, 2014 |DAILY BEAST 

We may never know the full amount that the Kochs or other outside spenders donate to advance anti-union legislation. The Next Phase of the Koch Brothers’ War on Unions |Carl Deal and Tia Lessin |December 22, 2014 |DAILY BEAST 

Other factors being equal, the amount of urea indicates the activity of metabolism. A Manual of Clinical Diagnosis |James Campbell Todd 

After about the forty-fifth year it becomes gradually less; after seventy-five years it is about one-half the amount given. A Manual of Clinical Diagnosis |James Campbell Todd 

In disease, the amount of solids depends mainly upon the activity of metabolism and the ability of the kidneys to excrete. A Manual of Clinical Diagnosis |James Campbell Todd 

The amount of the other purin bodies together is about one-tenth that of uric acid. A Manual of Clinical Diagnosis |James Campbell Todd 

For instance, the Limestone Polypody is not happy unless there is a certain amount of lime present in the soil. How to Know the Ferns |S. Leonard Bastin