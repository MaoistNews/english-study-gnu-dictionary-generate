Well, that’s the contract we have with each other, that people will be honest with you. “People want to believe”: How Love Fraud builds an absorbing docuseries around a romantic con man |Alissa Wilkinson |September 4, 2020 |Vox 

Really, if I’m really honest with you, the main reason I didn’t want the job, the main reason I tried to quit, is because the pressure was super, super intense. Tan France Goes Deep on Racism and When He Almost Quit ‘Queer Eye’ |Eugene Robinson |September 3, 2020 |Ozy 

When I hear you say that, if I’m 100% honest, that sounds like talking points. Full Transcript: Tomi Lahren on ‘The Carlos Watson Show’ |Daniel Malloy |August 31, 2020 |Ozy 

New personalities entering the crypto world—from Paul Tudor Jones to William Shatner to Olympian Christie Rampone—are helping initiate an honest conversation about whether our financial systems are helping or hurting us all. Why 2020 might be the year cryptocurrency goes mainstream |jakemeth |August 24, 2020 |Fortune 

Because it’s time for the brands to build honest and transparent relationships with consumers, which is going to lead to stronger trust in advertising. Five great display and video advertising tactics to increase relevance and revenue in a cookie-less world |Anastasia-Yvoni Spiliopoulou |August 24, 2020 |Search Engine Watch 

What matters is being honest, humble, and a faithful and loyal friend, father and member of your community. Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 

The Times of Israel even applauded Netanyahu for finally being honest about his views on the issue of Palestine. In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

To be honest, I think a lot of good essay writing comes out of that. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

There is a brutally honest section of the book about how you fell out of love with your wife, and essentially chose soccer. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 

So I’m sitting with my daughter and all of her friends—who are 13—and she says ‘Dad, can I be honest with you? Jamie Foxx: Get Over the Black ‘Annie’ |Stereo Williams |December 20, 2014 |DAILY BEAST 

With childlike confidence he follows the advice of some more or less honest dealer. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Sometimes necessity makes an honest man a knave: and a rich man a honest man, because he has no occasion to be a knave. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

They will reach you by the hands of Mr. Mackenzie, a worldly-minded Scotch merchant, but honest as to earthly things. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

If they are still Moderns and alive, I defy you to bury them if you are discussing living questions in a full and honest way. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

A world that has known five years of fighting has lost its taste for the honest drudgery of work. The Unsolved Riddle of Social Justice |Stephen Leacock