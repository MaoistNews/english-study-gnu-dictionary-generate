In 2016, her group used a chemical method to turn on human beige fat. Gene editing can alter body fat and may fight diabetes |Silke Schmidt |October 23, 2020 |Science News For Students 

How much brown and beige fat adults host is unknown, but it’s a lot less than in mice. Gene editing can alter body fat and may fight diabetes |Silke Schmidt |October 23, 2020 |Science News For Students 

They are a natural beige color and an easy-to-wash cotton fiber. These hanging planters bring life to your space |PopSci Commerce Team |October 8, 2020 |Popular-Science 

Subcutaneous fat tissue can even develop specialised “beige” fat cells that are able to burn fat. Belly Fat Linked To Higher Risk Of Premature Death, Regardless Of Your Weight |LGBTQ-Editor |October 5, 2020 |No Straight News 

During a tour with GHN and ProPublica on a sweltering morning in early August, Rollins unlocked the doors of a low-slung beige building a short drive north of Macon. He Wanted to Fix Rural America’s Broken Nursing Homes. Now, Taxpayers May Be on the Hook for $76 Million. |by Max Blau for Georgia Health News |September 22, 2020 |ProPublica 

Before the race, he had entered the grandstands waving his beige Stetson in the air, as if already on a victory lap. Why California Chrome’s Fairy Tale Didn’t End Happily Ever After |Michael Fensom |June 8, 2014 |DAILY BEAST 

The Republican candidate for President of the United States is casually lounging on a cheap beige couch. Inside ‘Mitt,’ Netflix’s All-Access Mitt Romney Documentary |Marlow Stern |January 17, 2014 |DAILY BEAST 

He removed her beige Roger Vivier pumps and white lab coat to reveal a denuded ballerina-pink slip dress. Tilda Swinton and Oliver Saillard Perform the Creation of Fashion in ‘Eternity Dress’ |Sarah Moroz |November 21, 2013 |DAILY BEAST 

In beige socks and black flip-flops, the man accused of bombing two U.S. embassies faced a judge in Manhattan. Gripping His Koran, Anas al-Liby Has His Day in Court |Michael Daly |October 16, 2013 |DAILY BEAST 

Dressed entirely in beige, working the A-1 cash register, it was clear that Walt no longer wanted to be Heisenberg. Latest ‘Breaking Bad’ Episode, ‘Ozymandias,’ Is Most Action-Packed Yet |Andrew Romano |September 16, 2013 |DAILY BEAST 

Harry noticed she was wearing a beige knit suit with a neckline that spoke volumes. The Observers |G. L. Vandenburg 

I would say he was about your size, and he had a light-beige jacket, and was lightweight. Warren Commission (6 of 26): Hearings Vol. VI (of 15) |The President's Commission on the Assassination of President Kennedy 

It was of excellent material, a sort of beige, but it bore unmistakable signs of having been worn before. Adventures of Sherlock Holmes |A. Conan Doyle 

She was daintily dressed in some sort of beige chiffon with pearls about her neck, and had easy, pleasant manners. Diplomatic Days |Edith O'Shaughnessy 

She wore a regardless beige gown, with Paris written all over it, and beautifully put on over a lovely, small-hipped figure. Diplomatic Days |Edith O'Shaughnessy