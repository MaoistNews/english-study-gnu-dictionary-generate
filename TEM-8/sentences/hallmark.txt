The concept of evidence-based medicine, for so long a hallmark of trustworthiness, has been deeply corrupted by drug and device manufacturers whose deep pockets can sway the results and the reporting. My cancer might be back—and I wonder if unnecessary radiation caused it in the first place |jakemeth |September 22, 2020 |Fortune 

A sleek design is a hallmark of Moleskine, and makes this a great notebook for well beyond the classroom. Notable notebooks for writing and drawing |PopSci Commerce Team |September 17, 2020 |Popular-Science 

Diversity was also a hallmark of “Vida,” which ran for three seasons on STARZ. FROM THE VAULTS: The opposite of binge-watching |Brian T. Carney |September 4, 2020 |Washington Blade 

In fact, the lack of reliable, quickly updating data has been a hallmark of this crisis, whether it be public health or economic data. How The Experts Are Measuring The Economic Recovery |Neil Paine (neil.paine@fivethirtyeight.com) |July 15, 2020 |FiveThirtyEight 

Just as scientists are beginning to understand how this microbiome supports human health, hallmarks of modern life such as antibiotics and processed foods may be pushing many of our microbial residents toward extinction. Scientists want to build a Noah’s Ark for the human microbiome |Carolyn Beans |June 11, 2020 |Science News 

The idea that January 1st initiates a period of new beginning is not a flash of Hallmark brilliance. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

They apparently took that as a sign of suspicious activity, even though that can be a hallmark of people on the autism spectrum. Worse Than Eric Garner: Cops Who Got Away With Killing Autistic Men and Little Girls |Emily Shire |December 4, 2014 |DAILY BEAST 

Split-ticket voting in general elections, the hallmark of so-called independents, is relatively rare. Reality Check: There Are No Swing Voters |Goldie Taylor |November 13, 2014 |DAILY BEAST 

Fast food and personified death: not exactly Hallmark material. America’s Fastest Growing Death Holiday Is From Mexico |Michael Schulson |November 1, 2014 |DAILY BEAST 

This is the hallmark of a successful multiplatform communication strategy. ISIS Is Winning the Online Jihad Against the West |Ali Fisher, Nico Prucha |October 1, 2014 |DAILY BEAST 

What (p. 310)they meant was that it was unconventional, was without the dignity of tradition to give it its hallmark. The Building of a Book |Various 

In all cases success starts with innovative intelligence products, which has not been a hallmark of United States operations. Shock and Awe |Harlan K. Ullman 

There was imposed upon it the unmistakable hallmark of spirituality that has always identified it in the throng of the nations. Jewish History |S. M. Dubnow 

Nature does not make duplicates; her creative hallmark is upon every leaf and bee; upon every cliff and cloud and star. Child and Country |Will Levington Comfort 

The reverse is fitted with two studs and a hook and bears the hallmark of "W. Pinchin, Philada." American Military Insignia 1800-1851 |J. Duncan Campbell and Edgar M. Howell.