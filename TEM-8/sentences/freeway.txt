Some consider trucking to be an attractive market for a self-driving startup because freeways are a relatively simple environment for software to understand. Toyota partners with startup Aurora to develop self-driving taxis |Timothy B. Lee |February 9, 2021 |Ars Technica 

Before the SkyGuardian was rerouted to the desert, General Atomics had planned to use its technologies to detect speeders on the freeway. Lessons From the Year in Surveillance Tech Debacles |Jesse Marx |December 28, 2020 |Voice of San Diego 

At night, after his wife, Gail, fell asleep, he sneaked out of the house with his teenage son to drive the empty freeways and cruise by the shuttered brick downtown, letting the cold wind hit his skin. For Years, JaMarcus Crews Tried to Get a New Kidney, but Corporate Healthcare Stood in the Way |by Lizzie Presser |December 15, 2020 |ProPublica 

Their daily commutes can also be a nightmare, even before they reach the freeway. What the Census Taught Me About the NIMBY vs. YIMBY Debate |Paul Krueger |December 14, 2020 |Voice of San Diego 

I grew up associating SANDAG with the signs on the side of the freeway. A Sixth Big Move for SANDAG’s New Transportation Vision |Joe Bettles |November 30, 2020 |Voice of San Diego 

He took a secluded road, telling me that avoiding the freeway would save me the tolls. ‘Brave Miss World’: Linor Abargil on Her Journey From Rape Victim to Beauty Queen to Activist |Linor Abargil |May 29, 2014 |DAILY BEAST 

The word evaporados appears on a wall adjacent to a freeway in a collection of photographs by Eduardo Villanes. In ‘Urbes Mutantes,’ Scenes From Latin America’s Transformations |Justin Jones |May 19, 2014 |DAILY BEAST 

He was coming off of a freeway, and I was hurt pretty badly from somebody driving really fast. Amy Heckerling’s Three Favorite Scenes in ‘Clueless’ on Its 18th Birthday |Erin Cunningham |July 19, 2013 |DAILY BEAST 

In 1998, a man named Daniel Jones shot himself in the head on live television on a Los Angeles freeway interchange. ‘Horribly Wrong’—Fox’s Live Suicide and the Thrill of the Police Chase |Eric Nusbaum |September 30, 2012 |DAILY BEAST 

He pulled the stolen sedan off the freeway in Tonopah, about 50 miles west of Phoenix. ‘Horribly Wrong’—Fox’s Live Suicide and the Thrill of the Police Chase |Eric Nusbaum |September 30, 2012 |DAILY BEAST 

I folded there, and the next thing I knew, I was on my side in the rubble under the freeway, holding myself and crying. Little Brother |Cory Doctorow 

Traffic roared up the first short block of Wisconsin from under the high steel freeway down to their left. Mr. Wicker's Window |Carley Dawson 

The last flicker of light scudded across the steel sides of the freeway to pick out the lettering above the shop window. Mr. Wicker's Window |Carley Dawson 

Here is the railroad overpass, and here is the freeway overpass. Warren Commission (6 of 26): Hearings Vol. VI (of 15) |The President's Commission on the Assassination of President Kennedy 

That's right; in other words, Stemmons Freeway and the service road both go under the underpass. Warren Commission (6 of 26): Hearings Vol. VI (of 15) |The President's Commission on the Assassination of President Kennedy