Turkey Trots are set to return on Thanksgiving after a year hiatus, and at least one runner is ready to take his trot to the next level. What You Missed: Drone Helps California Crews Find Hiker |Fred Dreier |November 18, 2021 |Outside Online 

Other athletes can get digestive issues, but they are usually much different than runner’s trots and are not as common. Why do marathon runners get the runs? |Ellen Airhart |November 7, 2021 |Popular-Science 

The whole point of being a pageant queen is to trot around in your bikini to be ogled at while feigning sexual naiveté. Miss America Hypocrisy: The Vanessa Williams Nude Photo Shaming |Amanda Marcotte |July 23, 2014 |DAILY BEAST 

And, of course, they trot out the Constitution to justify their actions, much as the slave holders did 150 years earlier. The South Has Indeed Risen Again and It’s Called the Tea Party |Jack Schwartz |December 8, 2013 |DAILY BEAST 

He had to urge his horse to a trot, and he went tagging alongside the funnel to see what it would do. Benjamin Franklin, America’s First Storm Chaser |Lee Sandlin |April 14, 2013 |DAILY BEAST 

Then I just have to “retrieve payload” from Coinapult, trot on back over to Blockchain and BAM! My Bitcoin (Mis)adventure |Winston Ross |April 4, 2013 |DAILY BEAST 

Oscar forecasters like to trot out old statistics when deciding who will win which awards. Oscar’s Best Director: Steven Spielberg vs. David O. Russell |Ramin Setoodeh |February 7, 2013 |DAILY BEAST 

The truth is, it is not safe to trot down such mountains and hardly to ride down them at all. Glances at Europe |Horace Greeley 

But I have some more foul way to trot through still, in your Epistles and Satyrs, &c. A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

Soon he begins to trot, and, when he thinks himself out of sight, bounds off like a greyhound. Hunting the Lions |R.M. Ballantyne 

To the left of us a horse snorted nervously; we heard him trot with high, springy strides to the end of his rope, and snort again. Raw Gold |Bertrand W. Sinclair 

I don't want to go back into my life, I don't want to trot out the old 'more sinned against than sinning' cliché. Bella Donna |Robert Hichens