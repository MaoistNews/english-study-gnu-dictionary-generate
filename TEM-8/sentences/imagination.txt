It takes us as finite beings and gives us almost infinite capacity to create new worlds of imagination. Talking Is Throwing Fictional Worlds at One Another - Issue 89: The Dark Side |Kevin Berger |September 9, 2020 |Nautilus 

The process of decolonizing one’s imagination is the first step to envisioning a future where pleasures are more evenly distributed than was the case in our past. Solarpunk Is Growing a Gorgeous New World in the Cracks of the Old One |Carin Ism |September 6, 2020 |Singularity Hub 

Now, with a little imagination, there are days where I can embrace it as a New Age-y sweat-based regimen among the skittering rodents. The Sublime Agony of Hot-Weather Running |Martin Fritz Huber |August 27, 2020 |Outside Online 

People, he says, are too limited by their own experience and imaginations. Can an A.I. hedge fund beat the market? |Jeremy Kahn |August 25, 2020 |Fortune 

They have little imagination about how to engage in social change, and even less imagination about the alternative world they would build if they could. Generation Z is ‘traumatized’ by climate change—and they’re the key to fighting it |matthewheimer |August 19, 2020 |Fortune 

Does each character have one in real life that inspired it, or are they from your imagination? The Zany Shades of Nick Kroll |Abby Haglage |December 15, 2014 |DAILY BEAST 

She found instead a show of imagination, artistry, and above all, really happy people—and she quickly fell in love. A Backstage Love Affair With Cirque du Soleil |Allison McNearney |December 1, 2014 |DAILY BEAST 

This little nook is the perfect spot for some quiet reading time or to let your imagination run wild. The Daily Beast’s 2014 Holiday Gift Guide: For the Blue Ivy in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

So after that initial inspiration, my imagination created the rest. Amanda Knox: A Mother’s Obsession |Nina Darnton |November 26, 2014 |DAILY BEAST 

Today, Turkey in the German imagination has mostly to do with immigration, assimilation, and EU membership. The 20th-Century Dictator Most Idolized by Hitler |William O’Connor |November 24, 2014 |DAILY BEAST 

Distance, the uncertain light, and imagination, magnified it to a high wall; high as the wall of China. The Giant of the North |R.M. Ballantyne 

I could have sworn I heard a cry, and one of my men spoke in a tone that assured me my imagination had not been playing a trick. Raw Gold |Bertrand W. Sinclair 

It was when the face and figure of a great tragedian began to haunt her imagination and stir her senses. The Awakening and Selected Short Stories |Kate Chopin 

How much of the imagination, how much of the intellect, evaporates and is lost while we seek to embody it in words! Pearls of Thought |Maturin M. Ballou 

When she heard it there came before her imagination the figure of a man standing beside a desolate rock on the seashore. The Awakening and Selected Short Stories |Kate Chopin