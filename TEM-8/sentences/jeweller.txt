Presently Harney found that his watch had stopped, and turned in at a small jeweller's shop which chanced to still be open. Summer |Edith Wharton 

"God bless me, crony; we ought to do these fine things and yet stay Catholics," cried the jeweller. Catherine de' Medici |Honore de Balzac 

These words were so alarming to the jeweller and the two women that they were followed by a dead silence. Catherine de' Medici |Honore de Balzac 

It has been enlarged with soft solder, as though Essex had only trusted it to a jeweller working in his presence. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon 

"A Moslem gentleman of good breeding, but perhaps decayed family," was the estimate of the jeweller. God Wills It! |William Stearns Davis