Deputy health commissioner Anderson disagrees about whether lotteries are useful, saying the city is pushing to improve equity through community partnerships and outreach instead. Chicago thinks Zocdoc can help solve its vaccine chaos |Lindsay Muscato |February 12, 2021 |MIT Technology Review 

Buyers will be selected by lottery, as usual, with designations for every team. What you need to know about Super Bowl LV |Sam Fortier |February 7, 2021 |Washington Post 

In most states, lotteries accounted for about 2 percent of total revenue, a significant sum, to be sure, but hardly enough to offset a tax reduction and meaningfully bolster government expenditures. Sports gambling could be the pandemic’s biggest winner |Jonathan D. Cohen |February 5, 2021 |Washington Post 

Worst of all, Dallas owes New York its 2021 first-rounder, which could land in the lottery barring a reversal of fortune this season. How Luka Doncic’s Mavericks lost their joyful swagger and how they can get it back |Ben Golliver |February 3, 2021 |Washington Post 

Other proposals included implementing a lottery rather than the first-come, first-served registration scramble for appointments. D.C. releases early data on vaccine recipients amid push for equitable distribution |Michael Brice-Saddler, Julie Zauzmer, Lola Fadulu |February 1, 2021 |Washington Post 

You might be a lesbian, in which case you have won the sexual lottery. Was 2014 the Year Science Discovered The Female Orgasm? |Samantha Allen |December 6, 2014 |DAILY BEAST 

The winning lottery numbers and foretold riches never arrived. The Multimillion ‘Clairvoyance by Mail’ Scam |Jacob Siegel |November 21, 2014 |DAILY BEAST 

Selecting legislators by lottery was good enough for the ancient Athenians. Is It Time to Take a Chance on Random Representatives? |Michael Schulson |November 8, 2014 |DAILY BEAST 

No matter how much money the Koch brothers or Tom Steyer spend, they cannot convince a lottery to choose one person over another. Is It Time to Take a Chance on Random Representatives? |Michael Schulson |November 8, 2014 |DAILY BEAST 

Pathways offers employment services no matter the intensiveness of the disability (they have a lottery system). Hiring People With Disabilities Isn’t Just the Right Thing to Do—It’s Good for Business |Elizabeth Picciuto |October 27, 2014 |DAILY BEAST 

Malicious persons in the town even declared that the lamented Torvestad had got his wife in a lottery at Christiansfeldt. Skipper Worse |Alexander Lange Kielland 

A lottery drawn in London for the benefit of the Virginia plantations, the profits of which amounted to nearly 30,000. The Every Day Book of History and Chronology |Joel Munsell 

She ran the household, but had likewise a decided mania for lottery, and always for the same numbers; she "nursed a trey." Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

The lottery had taken a strong hold upon the innate love of chance. A short history of Rhode Island |George Washington Greene 

The new county was required to build a court house at its own expense, which was partly done by lottery. A short history of Rhode Island |George Washington Greene