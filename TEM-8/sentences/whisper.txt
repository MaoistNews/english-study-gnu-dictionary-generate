The whispers have played out alongside long-running discussions about the lack of clear leadership on park issues, duplication of efforts and overhead among various Balboa Park groups and struggles to execute major park initiatives and projects. Two Balboa Park Groups Are in Talks to Merge |Lisa Halverstadt |October 10, 2020 |Voice of San Diego 

During the summer, there were whispers among Parisian food circles and on social media of assault allegations involving Sekine. Chef Behind Parisian New-Wave Hit Dersou Dies |Eileen W. Cho |September 29, 2020 |Eater 

There are whispers of McDonald’s locations that have breakfast buffets. Fast-Food Buffets Are a Thing of the Past. Some Doubt They Ever Even Existed. |MM Carrigan |September 29, 2020 |Eater 

The whisper number, for example, calls for just 670,000 new jobs, about half of what economists expect. Jittery investors eye today’s big jobs report as markets rebound from an epic sell-off |Bernhard Warner |September 4, 2020 |Fortune 

Once Greene and Lobb moved it into the symplectic world, the problem gave way with a whisper. New Geometric Perspective Cracks Old Problem About Rectangles |Kevin Hartnett |June 25, 2020 |Quanta Magazine 

He speaks in a whisper, flanked by the two locals who set up the meeting. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

The substitute nurse says to him in a stage whisper, “You know, the doctor says no vodka.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Some residents took to the secret-sharing app Whisper to disclose their biggest fears. ‘I’m Flipping Out’: Dallas Residents’ Worst Ebola Fears |The Daily Beast |October 15, 2014 |DAILY BEAST 

Supporters of the Putin regime were pointed out in a whisper. From Moscow to Queens, Down Sergei Dovlatov Way |Daniel Genis |September 15, 2014 |DAILY BEAST 

Darren was the kind of man who'd lug a sofa bed up three flights of stairs without a whisper of complaint. Let Us Now Praise Famous Rednecks and Their Unjustly Unsung Kin |Allison Glock |August 23, 2014 |DAILY BEAST 

These words were uttered in a guarded whisper by a boy about seventeen years of age, to a great dog that stood by his side. The Courier of the Ozarks |Byron A. Dunn 

The voice died out in a broken whisper, and two hot tears fell on Black Sheep's forehead. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

He saw the tips of the fir trees shimmer, and heard them whisper as the breeze turned their needles towards the light. Three More John Silence Stories |Algernon Blackwood 

"The angels don't whisper such blessed dreams to me," returned Dorothy, sadly. The World Before Them |Susanna Moodie 

A voice came from somewhere, a voice asking somebody also in a whisper to put the lights out. The Weight of the Crown |Fred M. White