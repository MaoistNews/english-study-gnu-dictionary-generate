This son of an artist-painter father and occupational therapist mother initially wanted to be a chef, thanks to his discerning palate. When Dead Beer Walks: A Dane in Vietnam Turns Strange Brew Into Craft Gin |Eugene Robinson |February 12, 2021 |Ozy 

Choosing a mix from a single brand may allow you to discern differences more consistently than randomly selecting styles from a variety of brands. Beginner-friendly golf balls that will help you fall in love with the game |PopSci Commerce Team |February 5, 2021 |Popular-Science 

It is not with some discerning eye on my part that I wound up on a show that still somehow manages to be culturally relevant 20 years later. How ‘Freaks and Geeks’ went from misfit dramedy to cult classic, as told by its cast and creators: ‘People just like it so much that it thrusts itself from the grave’ |Sonia Rao |January 27, 2021 |Washington Post 

Designed as fun teaching tools, they’re meant to help both budding news consumers and veteran news junkies learn how to discern factually sound, vetted reporting from malicious misinformation. The Latest Weapon Against Fake News? Video Games |Charu Kasturi |January 27, 2021 |Ozy 

This allows them to smell with higher sensitivity, while being able to minutely discern what they’re smelling—blood or Deet. Scientists Made a Biohybrid Nose Using Cells From Mosquitoes |Shelly Fan |January 26, 2021 |Singularity Hub 

But, as far as I can discern, they do focused, pragmatic work. COEXIST’s Bonehead Bumper-Sticker Politics |Michael Schulson |December 21, 2014 |DAILY BEAST 

The addicting ditty “One of These Things” was used to help children learn to compare and discern differences. ‘Sesame Street’ Is Middle-Aged and Awesome |Emily Shire |November 10, 2014 |DAILY BEAST 

Meaning, one was left to discern, that religion is self-evidently a coercive force for ill. Karen Armstrong’s New Rule: Religion Isn’t Responsible for Violence |Patricia Pearson |October 29, 2014 |DAILY BEAST 

Lately though it seems the line between spin and reality is harder to discern. When Campaign Spin Becomes Fact |Stuart Stevens |March 21, 2014 |DAILY BEAST 

Rational people, they contend, would take the time to discern whether the person behind the weapon was an actual threat. The Scare Campaign of Open Carry Activists |Brandy Zadrozny |November 18, 2013 |DAILY BEAST 

It did not amuse me, nor, so far as I could discern, was Monsieur de Tressan greatly taken with it. St. Martin's Summer |Rafael Sabatini 

At 1720 to 1725 a close student of his work of hand may discern some signs of what was to follow, it might be said naturally. Antonio Stradivari |Horace William Petherick 

Not once, by word or deed, can we discern that he cherished any idea of future renown. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

In coasting along the shore, you will discern the summits which are marked on the chart. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Few minds could discern distinctly the path of truth and duty through the clouds and vapors of those stormy times. Madame Roland, Makers of History |John S. C. Abbott