It’s hard to even imagine school without a classic Mead Composition Book. Notable notebooks for writing and drawing |PopSci Commerce Team |September 17, 2020 |Popular-Science 

Just imagine if the more than 200,000 people who voted in Jefferson County, Kentucky, during the primary had actually shown up at the county’s only polling location. Why you should vote as early as possible (and how to do it) |John Kennedy |September 17, 2020 |Popular-Science 

“It’s actually not possible to imagine an AP class being shut down abruptly at Scripps Ranch High or many others,” Lewis writes. Morning Report: Lincoln Abruptly Canceled AP Class |Voice of San Diego |September 17, 2020 |Voice of San Diego 

It’s actually not possible to imagine an AP class being shut down abruptly at Scripps Ranch High or many others. When a Calculus Class Abruptly Became Ceramics at Lincoln High |Scott Lewis |September 16, 2020 |Voice of San Diego 

Now imagine that you want to insert a new edge connecting two nodes in a planar graph, say nodes 1 and 6 in the example below. A New Algorithm for Graph Crossings, Hiding in Plain Sight |Stephanie DeMarco |September 15, 2020 |Quanta Magazine 

“You can imagine the sound of that gun on a Bronx street,” Chief of Detectives Robert Boyce says. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Imagine waking up to find a guy who looks like a tech startup employee eating your charred crispy leg. The Red Viper, Zoe Barnes, and the Best Fictional Deaths of 2014 |Melissa Leon |January 1, 2015 |DAILY BEAST 

If you think divorce between two people is messy and traumatic, imagine divorcing yourself. Why Singles Should Say ‘I Don’t’ to The Self-Marriage Movement |Tim Teeman |December 30, 2014 |DAILY BEAST 

Imagine living 28 years—your whole life—trapped inside the wrong body. The Insurance Company Promised a Gender Reassignment. Then They Made a Mistake. |James Joiner |December 29, 2014 |DAILY BEAST 

Now imagine that one day hope appeared, an unexpected opportunity to free yourself, to finally be yourself. The Insurance Company Promised a Gender Reassignment. Then They Made a Mistake. |James Joiner |December 29, 2014 |DAILY BEAST 

I would ask you to imagine it translated into every language, a common material of understanding throughout all the world. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

And furthermore, I imagine something else about this—quite unlike the old Bible—I imagine all of it periodically revised. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

You may imagine the effect this missive produced upon the proud, high-minded doctor of divinity. Elster's Folly |Mrs. Henry Wood 

We can imagine that, as soon as a printed book ceased to be a great rarity, it became an object of great abhorrence. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

I thought you said Meadowville, and never havin' been there, I didn't see how I could imagine the station. The Soldier of the Valley |Nelson Lloyd