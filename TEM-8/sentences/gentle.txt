Our spacious villa looked directly onto the gentle curve of Polo Beach. A Maui vacation in three acts |Alex Pulaski |February 12, 2021 |Washington Post 

Afterward, you can expect to wash the blanket on your machine’s delicate settings with a gentle detergent. Best heated throw blanket: Bundle up with these electric blankets |PopSci Commerce Team |February 11, 2021 |Popular-Science 

Be sure to wash your heated vest with cool water with the machine’s gentle setting. Best heated vest: Beat the cold weather with the right winter gear |PopSci Commerce Team |February 9, 2021 |Popular-Science 

Gorilla Glue told TMZ that rubbing alcohol, a gentle combing and a delicate shampoo would likely be the best solution for Brown. She used Gorilla Glue as hairspray. After 15 washes and a trip to the ER, it still won’t budge. |Lateshia Beachum |February 8, 2021 |Washington Post 

That gentle evolution gives astronomers a unique opportunity to use TRAPPIST-1 and TOI-178 as testbeds for planetary theory. Two exoplanet families redefine what planetary systems can look like |Lisa Grossman |February 5, 2021 |Science News 

But he was always uncommonly gracious, a truly gentle man, willing to dispense wisdom and perspective when asked. Ed Brooke: The Senate's Civil Rights Pioneer and Prophet of a Post-Racial America |John Avlon |January 4, 2015 |DAILY BEAST 

She is incapable of responding to kindness and enquiry, even very gentle flirting on the part of a co-worker. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

The only justice sought by those folks involved a conviction against Wilson for killing the “gentle giant” teen. Justice Was Served in Ferguson—This Isn’t Jim Crow America |Ron Christie |November 25, 2014 |DAILY BEAST 

They act silly, and for a moment we see how gentle and innocent they are. ‘Girlhood’: Coming of Age in France’s Projects |Molly Hannon |November 25, 2014 |DAILY BEAST 

I thought he was very intelligent, very gentle, soft-spoken, precise. All Eyes on Anjelica Huston: The Legendary Actress on Love, Abuse, and Jack Nicholson |Alex Suskind |November 10, 2014 |DAILY BEAST 

He turned to the gentle accents of his sweet Alice, breathed in a letter which had been wet with her grateful tears. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The action was at first a little confusing to Edna, but she soon lent herself readily to the Creole's gentle caress. The Awakening and Selected Short Stories |Kate Chopin 

He reached down inside my shirt, with a none too gentle hand, and relieved me of the belt that held the money. Raw Gold |Bertrand W. Sinclair 

Are you quite sure you have never suffered from this rather common disorder, gentle reader, at least, if you be of the male sex? The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

And it might be a good idea for you to give your men a gentle hint to keep their mouths closed about this affair—all of it. Raw Gold |Bertrand W. Sinclair