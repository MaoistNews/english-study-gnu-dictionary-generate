There is no technical or regulatory obstacle to launching a constellation of ultra-bright satellites that might render many or most astronomy programs impossible. Satellite mega-constellations risk ruining astronomy forever |Neel Patel |September 2, 2020 |MIT Technology Review 

The true threat these mega-constellations pose to the astronomy community is only just beginning to be understood. Satellite mega-constellations risk ruining astronomy forever |Neel Patel |September 2, 2020 |MIT Technology Review 

The new LEO satellites don’t affect every astronomy program the same way. Satellite mega-constellations risk ruining astronomy forever |Neel Patel |September 2, 2020 |MIT Technology Review 

The team used the tool to confirm 50 new potential planets, a first for artificial intelligence as applied to astronomy. 50 new planets, including one as big as Neptune, are identified using A.I. |rhhackettfortune |August 26, 2020 |Fortune 

Gathering data on glass plates and steering a telescope by eye may sound primitive, but these observations led to some of the most groundbreaking discoveries in astronomy. Social Distancing From the Stars |Emily Levesque |August 11, 2020 |Quanta Magazine 

The authors took care to eliminate the possibility of other sources of polarization, which is always a concern in astronomy. The Black Hole Tango |Matthew R. Francis |November 24, 2014 |DAILY BEAST 

Muslims made many discoveries in mathematics, chemistry, physics, medicine, astronomy and psychology. ‘Gods of Suburbia’: Dina Goldstein’s Arresting Photo Series on Religion vs. Consumerism |Dina Goldstein |November 8, 2014 |DAILY BEAST 

Nearly everything we know about dark matter so far comes from astronomy. Still No Dark Matter from Space Station Experiment |Matthew R. Francis |September 21, 2014 |DAILY BEAST 

One of the big challenges in astronomy involves determining when the first galaxies formed, and what they looked like. Some of the First Galaxies Were Big Babies |Matthew R. Francis |September 14, 2014 |DAILY BEAST 

Cosmic ray observations are more challenging than many other forms of astronomy. The Hottest Spot for Cosmic Rays |Matthew R. Francis |July 13, 2014 |DAILY BEAST 

A learned Professor declared that no person unacquainted with astronomy could correlate “Moon” to “Omnibus.” Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

First of all comes astronomy, including the phenomena exhibited in the heavens, beyond the limits of the earth's atmosphere. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Science began with astronomy, and the first instruments which men contrived for the purpose of investigation were astronomical. Outlines of the Earth's History |Nathaniel Southgate Shaler 

As stated in the chapter on astronomy, some trace of the triangular form appears in the land masses of the planet Mars. Outlines of the Earth's History |Nathaniel Southgate Shaler 

He talked until late into the night about astronomy and its latest discoveries. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky