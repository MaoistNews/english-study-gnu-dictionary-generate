“You must stop baring your midriff,” scolded her headmistress. The 15 Most Cringeworthy Bits From Leandra Medine’s New Memoir |Emilia Petrarca |September 10, 2013 |DAILY BEAST 

Her clothes are sold at Barneys, all over Europe and Asia, and are quite fitted, with provocative names like "HeadMistress Dress." The Rise and Rise of L'Wren Scott |Merle Ginsberg |March 18, 2009 |DAILY BEAST 

At the door she came face to face with Miss Bennet, the headmistress. Fifty-Two Stories For Girls |Various 

She was tall and thin, angular and severe, a typical headmistress, stern and unyielding. The Imaginary Marriage |Henry St. John Cooper 

Foremost amongst these was Mrs. Liang, mother of Ling Ai, the headmistress of the girls' school. The Fulfilment of a Dream of Pastor Hsi's |A. Mildred Cable 

Her position here is one of the best of its kind, for she is practically headmistress. Tom and Some Other Girls |Mrs. George de Horne Vaizey 

It is said she was a stern headmistress, but she stood for all that was fine, and meant a great deal to Georgetown. A Portrait of Old George Town |Grace Dunlop Ecker