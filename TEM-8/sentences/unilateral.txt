In practice, just as the decision to enter was regarded as a unilateral right by the United States, articulated in policy like the Eisenhower doctrine, so was the decision to withdraw — even if that left Afghanistan to the brutal Taliban. Afghanistan’s collapse exposes the truth about U.S. military invasions in the region |Suzanne Enzerink |August 25, 2021 |Washington Post 

He said her unilateral overreach would leave a scar on the House’s integrity. Bipartisan House probe of Jan. 6 insurrection falls apart after Pelosi blocks two GOP members |Marianna Sotomayor, Jacqueline Alemany, Karoun Demirjian |July 22, 2021 |Washington Post 

Even if your employer thought less visibility and a less challenging position would mean less strain on your mental health, that’s not the employer’s unilateral call to make. My old job was erased when I was on medical leave. Doesn’t the job I return to have to be the same? |Karla Miller |April 15, 2021 |Washington Post 

Take New Hampshire, where state law gives Secretary of State Bill Gardner unilateral power to move the primary date as necessary to protect the state’s distinction of hosting the cycle’s first presidential primary. Many Democrats Are Sick Of Iowa And New Hampshire Going First, But The Primary Calendar Is Unlikely To Change |Geoffrey Skelley (geoffrey.skelley@abc.com) |April 8, 2021 |FiveThirtyEight 

Statehood would be a unilateral gift to Democrats in a Senate that is at the moment evenly divided and a House that is within reach for the GOP next year. Sorry. D.C. Statehood Isn’t Likely |Philip Elliott |March 22, 2021 |Time 

Under these circumstances, the kind of unilateral executive action Obama is undertaking will become more and more common. Why Did Obama Flip-Flop on Immigration? |Paul Campos |November 21, 2014 |DAILY BEAST 

At the same time, Democrats lost ground, now maintaining unilateral control in just six states, down from 11 before the midterms. GOP States’ Hitlist: Abortion, Unions & Hillary |Nancy Kaffer |November 18, 2014 |DAILY BEAST 

To be clear, unilateral Democratic control is just as problematic as the reverse. GOP States’ Hitlist: Abortion, Unions & Hillary |Nancy Kaffer |November 18, 2014 |DAILY BEAST 

If the president takes unilateral action to legalize millions of people, King said, “it will create a constitutional crisis.” Steve King: GOP Congress Should Keep Repealing Obamacare |Ben Jacobs |October 29, 2014 |DAILY BEAST 

He spoke to Putin on June 17 and came away offering a “unilateral cease-fire.” Putin Is Just Getting Started in Ukraine |Michael Weiss |June 19, 2014 |DAILY BEAST 

Your State Department has been distributing judicious hints that a unilateral policy toward Franco will upset the apple cart. The Five Arrows |Allan Chase 

The body of the rules of this law can be altered by common consent only, not by a unilateral declaration on the part of one State. International Law. A Treatise. Volume I (of 2) |Lassa Francis Oppenheim 

The object of treaties is always an obligation, whether mutual between all the parties or unilateral on the part of one only. International Law. A Treatise. Volume I (of 2) |Lassa Francis Oppenheim 

In some cases of unilateral tubal abortion the operator has cleared out the tubal mole and clot, and left the tube. A System of Operative Surgery, Volume IV (of 4) |Various 

Picture to yourself the unilateral development, the imminent danger of a spinal curvature. The Valley of Fear |Sir Arthur Conan Doyle