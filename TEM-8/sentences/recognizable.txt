A few studies measuring antibodies — key proteins that recognize and bind to germs — suggest that their levels do wane with time. A Hong Kong man got the new coronavirus twice |Erin Garcia de Jesus |August 26, 2020 |Science News For Students 

A few studies measuring antibodies — key immune proteins that recognize and bind to pathogens — suggest that antibody levels do wane over time. A man in Hong Kong is the first confirmed case of coronavirus reinfection |Erin Garcia de Jesus |August 24, 2020 |Science News 

The game’s scenarios, while simplified, recognize real-world problems for these animals. The board game Endangered shows just how hard conservation can be |Sarah Zielinski |August 21, 2020 |Science News 

That finding gives astronomers more confidence that they will be able to recognize potentially habitable exoplanets. Hubble watched a lunar eclipse to see Earth from an alien’s perspective |Maria Temming |August 17, 2020 |Science News 

Now researchers have discovered a set of supersensing cells in the taste buds of mice that can detect four of the five flavors that the buds recognize. Newly discovered cells in mice can sense four of the five tastes |Carolyn Wilke |August 13, 2020 |Science News 

But unlike any other director, he was an identifiable public figure, as recognizable as any president or movie star. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Is there a more celebrated--and recognizable— 20th century movie director? Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Warren makes one clearly recognizable gesture to the center, and the centrists make one recognizable gesture to the left. Staving Off a Democratic Civil War |Michael Tomasky |December 2, 2014 |DAILY BEAST 

And part of what makes it weird is that he stands alone as a recognizable figure. Kevin Spacey Stars as a Frank Underwood-like Warmonger in ‘Call of Duty: Advanced Warfare’ |Alec Kubas-Meyer |November 8, 2014 |DAILY BEAST 

He is probably the most recognizable calavera artist in the world. New Orleans’ Carnivalesque Day of the Dead |Jason Berry |November 1, 2014 |DAILY BEAST 

When she lowered her eyes and looked out again across the sands, the figure had approached so close as to be recognizable. Dope |Sax Rohmer 

Some of them were so worn with hunger and fatigue as to be hardly recognizable, and all were utterly discouraged. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

I knew that in twenty-four hours there wouldn't be a recognizable body left, and in a week there wouldn't be anything at all. In the Dark |Ronal Kayser 

Some appeared smashed almost out of all recognizable shape, while others were, to all appearances unharmed. The Sargasso of Space |Edmond Hamilton 

The two were Verinder and Captain Kilmeny, though at that distance they were not recognizable. The Highgrader |William MacLeod Raine