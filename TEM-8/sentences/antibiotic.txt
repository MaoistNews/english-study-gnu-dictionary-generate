These medications are a type of antimicrobial drug—a category that also includes antibiotics. These are the best COVID-19 treatments right now |Claire Maldarelli |October 5, 2020 |Popular-Science 

That’s why people who took antibiotics could, paradoxically, develop new infections after treatment. When Evolution Is Infectious - Issue 90: Something Green |Moises Velasquez-Manoff |September 30, 2020 |Nautilus 

Researchers will need to study how the short-term benefits of antibiotics stack up against potential risks, she says, but such research may also lead to therapies for pregnant women who need antibiotics. A mother mouse’s gut microbes help wire her pup’s brain |Carolyn Wilke |September 23, 2020 |Science News 

Unlike many bacterial invaders that are thwarted by antibiotics, there aren’t many pharmaceuticals that help us beat viral infections. All the pros—and cons—of convalescent plasma therapy for COVID-19 |Claire Maldarelli |September 1, 2020 |Popular-Science 

He expected the California Department of Public Health to be his advocate when he alleged Palomar Vista Healthcare Center in Escondido had failed to deliver adequate antibiotic treatments for an infection that left him at risk. Confirmed Nursing Home Complaints Plummet During Pandemic |Jared Whitlock |August 25, 2020 |Voice of San Diego 

In the future, antibiotic resistance could have catastrophic consequences. Without Education, Antibiotic Resistance Will Be Our Greatest Health Crisis |Russell Saunders |December 19, 2014 |DAILY BEAST 

A fair number explicitly ask for an antibiotic, to cover the possibility that it is “bronchitis” or a “sinus infection.” Without Education, Antibiotic Resistance Will Be Our Greatest Health Crisis |Russell Saunders |December 19, 2014 |DAILY BEAST 

A plague outbreak in Madagascar has killed 40 people so far, and due to antibiotic resistance, it could kill many more. Bubonic Plague Is Back (but It Never Really Left) |Kent Sepkowitz |November 27, 2014 |DAILY BEAST 

The report also noted that “the inherent risks of long-term antibiotic therapy were not justified by clinical benefit.” Predator Doctors Take Advantage of Patients With ‘Chronic Lyme’ Scam |Russell Saunders |September 19, 2014 |DAILY BEAST 

Reeve passed away after experiencing an adverse reaction to an antibiotic on Oct. 10, 2004. Robin Williams and Christopher Reeve's Epic Friendship and the Greatest Williams Story Ever Told |Marlow Stern |August 12, 2014 |DAILY BEAST 

But the discovery of what substance should be added to what antibiotic was largely one of trial and error. Bolden's Pets |F. L. Wallace 

I sat back while he pondered and watched the Quack, who was swallowing another antibiotic capsule. Problem on Balak |Roger D. Aycock 

I wondered often what could have been in that stuff to make it such a powerful antibiotic. Jack of No Trades |Charles Cottrell 

The books started with the specifications for antibiotic growth equipment for colonies with problems in local bacteria. Sand Doom |William Fitzgerald Jenkins 

The administration of certain antibiotic drugs has produced cockroaches very nearly free of bacteroids. The Biotic Associations of Cockroaches |Louis M. Roth