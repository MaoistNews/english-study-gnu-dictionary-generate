I could not help at the meeting threatening to horsewhip J. Price for the falsehoods that he with the others had reported. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Seizing him by the collar, and threatening him with the horsewhip. Two Men of Sandy Bar |Bret Harte 

The horsewhip and revolver were as necessary to its conduct as the pen and inkpot. American Sketches |Charles Whibley 

I only wonder he did not horsewhip Philip round the quadrangle. The Heir of Redclyffe |Charlotte M. Yonge 

I am told she lifted her horsewhip on a gentleman once, and then put her horse at him and rode him down. Lord Ormont and his Aminta, Complete |George Meredith