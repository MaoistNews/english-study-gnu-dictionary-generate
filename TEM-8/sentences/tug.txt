The backward tug of this material’s gravity then causes the cluster to plunge closer and closer to the galactic center. The Milky Way’s most massive star cluster may have eaten a smaller cluster |Ken Croswell |September 29, 2020 |Science News 

For designers, though, possibly the most intriguing aspect of the Tik Tok tug-of-war is the report, first floated in the New York Times, that the leading candidate for CEO of the restructured entity is Instagram co-founder Kevin Systrom. How would Instagram co-founder Kevin Systrom redesign TikTok as CEO? |claychandler |September 22, 2020 |Fortune 

Foundational theories of morality have been locked in a perennial tug of war between the supernaturalism of Plato and the naturalism of his opponents. The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 

Foundational theories of morality have been locked in a perennial tug of war. The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 

Each star tends to keep the disk aligned with itself, and the tug-of-war warped and sheared the disk, and twisted the inner ring even further. A weirdly warped planet-forming disk circles a distant trio of stars |Lisa Grossman |September 3, 2020 |Science News 

These men and women will no doubt cause a tug a war in time. Up To Speed: The Cuba Embargo |Nina Strochlic |December 18, 2014 |DAILY BEAST 

The White House has been mediating the tug of war between the CIA and the committee over the redaction process. You're About to See What Obama Calls 'Torture' |Josh Rogin, Eli Lake |August 1, 2014 |DAILY BEAST 

But since then it has been subject to a tug of war with the CIA. Ex-CIA Top Lawyer: Release the ‘Black Site’ Report |Eli Lake |March 14, 2014 |DAILY BEAST 

After this Euro-Russian tug of war, the sparks ignited in Kiev and Moscow. Cut the Baloney on Ukraine |Leslie H. Gelb |March 9, 2014 |DAILY BEAST 

Throughout the day, you would overhear chit-chat about the ongoing tug-of-war between libertarians and social conservatives. CPAC: Come for the Crazy, Stay for the Party |Michelle Cottle |March 7, 2014 |DAILY BEAST 

He was given no reply save a muttered curse, a command to hold his tongue, and an angry tug at his tied arms. The Red Year |Louis Tracy 

In fact, the true tug only begins when we try to carry the second line and the flanking machine guns. Gallipoli Diary, Volume I |Ian Hamilton 

On this occasion, however, the tug appeared somewhat late on the scene, and hailed the Gull. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

The drama, as far as the Gull-Light was concerned, ended that night with the disappearance of the tug and lifeboat. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

Tug service for sailing ships or vessels without motive power is at the rate of $15 per hour. The Wonder Book of Knowledge |Various