Those who transmigrate into the bodies of the less fortunate are accused of tourism, voyeurism. The first murder |Katie McLean |August 19, 2020 |MIT Technology Review 

The pollution closes beaches that are vital to San Diego’s tourism economy. No One Is Actually in Charge of Solving the Border Sewage Crisis |Dianne Feinstein |August 12, 2020 |Voice of San Diego 

Covid-19 has brought tourism from China to the UK to a standstill. The UK is facing the prospect of a future with no Chinese tourists |Annabelle Timsit |August 6, 2020 |Quartz 

Even as most domestic air travel routes have opened now, aviation and tourism experts believe that the country is unlikely to resume international commercial flights before October. India’s Covid-19 tourism slump is putting over 85 million jobs at risk |Niharika Sharma |July 29, 2020 |Quartz 

Growth is now expected to collapse in many countries especially those dependent on tourism and resources, such as oil and mineral exporters. Africa’s pathway to economic recovery post-Covid is looking much more murky |Yinka Adegoke |July 6, 2020 |Quartz 

Slowly, two were opened up, and in 2010 the regional government opened all four Brogpa villages in a push for tourism. The Himalayas’ Hidden Aryans |Nina Strochlic |January 3, 2015 |DAILY BEAST 

Religious profiteering has spread beyond the tourism industry. The Buddhist Business of Poaching Animals for Good Karma |Brendon Hong |December 28, 2014 |DAILY BEAST 

Apparently tourism in the country had jumped tenfold since the film hit theaters. When Countries Lose Their Shit Over American Movies |Asawin Suebsaeng |December 17, 2014 |DAILY BEAST 

In recent years news outlets have documented the rise of so-called “birth tourism” here in America. The Progressive Case Against Birthright Citizenship |Keli Goff |December 15, 2014 |DAILY BEAST 

But the site has seen little of the decimation from heavy tourism that has plagued the northern pyramids of Giza in Egypt. Egypt Ain’t The Only Pyramid Show In Town |Nina Strochlic |December 11, 2014 |DAILY BEAST 

For years too few doctors have seen clearly that gymnastic tourism and sport do more for health than all doctors taken together. Readings on Fascism and National Socialism |Various 

Increasing tourism has resulted in special problems in resort areas. Area Handbook for Bulgaria |Eugene K. Keefe, Violeta D. Baluyut, William Giloane, Anne K. Long, James M. Moore, and Neda A. Walpole