His exchanges with Rob are terse to the point of being hostile. In Pig, Nicolas Cage Plays a Grouchy, Meditative Hermit—and Gives His Best Performance in Years |Stephanie Zacharek |July 15, 2021 |Time 

The language in the email is terse, but it appears to say that the attacks target devices that are exposed to the Internet. Hackers are using unknown user accounts to target Zyxel firewalls and VPNs |Dan Goodin |June 24, 2021 |Ars Technica 

“In the same way Steve Jobs could be cutthroat and terse and explosive, Elon is the same way,” said the former employee, who worked at the companies under both men. Apple locked users into its ecosystem. Tesla wants to do the same with its ‘iPhone on wheels.’ |Faiz Siddiqui |May 14, 2021 |Washington Post 

In a terse new letter that’s been leaked to the press, Secretary of State Antony Blinken has urged Afghan President Ashraf Ghani to work toward a power-sharing deal between his democratically elected government and the Taliban. What About Afghanistan’s Democracy, Joe? |Charu Kasturi |March 10, 2021 |Ozy 

Much terser is Clara Cornelius’s handsome setting of “Measure Twice, Cut Once,” worthy advice for craftspeople of all sorts. In the galleries: A focus on the intersection of art and movement |Mark Jenkins |February 26, 2021 |Washington Post 

But Brinsley is unconvinced and the two trade terse responses. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

The comparison prompted Kelly to release the terse statement: “Martha McSally is no Gabby Giffords.” The Battle Is On in Gabby Giffords’s Old District |Eleanor Clift |October 9, 2014 |DAILY BEAST 

It is a mighty tough slog, I will have to give them that, written in terse and exclusive science-ese. No, Stem Cells Don't Cause Autism |Kent Sepkowitz |September 11, 2014 |DAILY BEAST 

She had earlier offered a terse description of how her asthmatic brother had come to die. The Gentle Giant Cut Down by Cops |Michael Daly |July 24, 2014 |DAILY BEAST 

My response to poisonous emails is a terse “thank you for contacting me” and nothing more. Pediatrician: Don’t Make Your Kid’s Healthcare a Proxy in Your Divorce Battles |Russell Saunders |February 14, 2014 |DAILY BEAST 

A dozen of these terse but meaningless sayings now dance before our recollection, for who has not heard them, even to loathing? The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

"Hit's the pore house fer a cow hand," was his terse aphorism on the subject, and Landy had never seen a "fitten" poor house. David Lannarck, Midget |George S. Harney 

Ripley briefed the general situation as it stood on the night of the engine theft in a few terse sentences. The Wreckers |Francis Lynde 

Bill Hayes gave a terse account of his stewardship during Hollister's absence. The Hidden Places |Bertrand W. Sinclair 

Bill, to use a terse but slangy term, proceeded to go up in the air. Radio Boys Loyalty |Wayne Whipple