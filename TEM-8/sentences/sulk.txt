The cop swiftly rebukes Bieber, who then goes off to cross his arms and sulk petulantly in a corner. Watch Justin Bieber Try to Walk in a Straight Line After His DUI Arrest |Amy Zimmerman |February 28, 2014 |DAILY BEAST 

Every interaction with her was fraught lest she would throw a sulk or sink into a pout. Karl Taro Greenfeld on His Novel “Triburbia,” Con Men, and Literary Success |Nick McDonell |August 9, 2012 |DAILY BEAST 

If the Supreme Court overturns the health-care law, Democrats will be tempted to sulk and feel sorry for themselves. Michael Tomasky: Democrats Should Come Out Swinging Against the Court |Michael Tomasky |June 24, 2012 |DAILY BEAST 

Some will go off and sulk, and their followers will stay home on Election Day. The Tea Party vs. the GOP |Adam Clymer |January 26, 2010 |DAILY BEAST 

Common prudence forced them to sulk all day in the stall of the khan, while Musa went forth to make his discoveries. God Wills It! |William Stearns Davis 

They sulk and pout, worse than humans, if one act wins more applause than another. David Lannarck, Midget |George S. Harney 

Sadie did not often sulk, and he was grateful because she said nothing about what had happened on the previous night. The Girl From Keller's |Harold Bindloss 

He used to sulk for a week afterwards, avoiding Alan in the 'quarter' and ostentatiously burying himself in a group of boarders. Sinister Street, vol. 1 |Compton Mackenzie 

"Elvira can sulk for the rest of eternity, if she wants to," Mea said now without the slightest trace of sadness. Maezli |Johanna Spyri