Bush and Renz have been working together for about a dozen years, producing social justice–driven advertising, short films, and music videos, which means they were very used to collaborating by the time they made the movie. ‘Antebellum’ tackles the past head on in an effort to ‘move forward’ |radmarya |September 17, 2020 |Fortune 

Bush and Renz are both excited to see how the world receives their movie. ‘Antebellum’ explores truths of our ugly past |Brian T. Carney |September 16, 2020 |Washington Blade 

Even though President Obama and President Bush campaigned on bi-partisanship and bringing people together, they failed. America’s Hidden Duopoly (Ep. 356 Rebroadcast) |Stephen J. Dubner |September 3, 2020 |Freakonomics 

Likewise, in 1988, Bush ultimately won the nomination, but not before coming in third in the Iowa caucuses and having some tense debates with Senate Minority Leader Bob Dole. Biden Had To Fight For The Presidential Nomination. But Most VPs Have To. |Julia Azari |August 20, 2020 |FiveThirtyEight 

The Supreme Court eventually intervened to let stand a preliminary count that awarded Bush the state. Here’s what could happen to stock markets if the Trump-Biden election results are contested |Anne Sraders |August 18, 2020 |Fortune 

“The institution of marraige [sic] is under attack in our society and it needs to be strengthened,” Bush wrote. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

Meanwhile, in Florida, Bush was flooded with questions about whether gay marriage could possibly come to the Sunshine State. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

A few days later, Bush replied, “We will uphold the law in Florida.” Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

Think back to the Bush-Kerry race of 2004, the Thrilla in Vanilla. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

To those who agreed with him, Bush pledged that the law against same-sex marriage would remain intact. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

I saw every crook in the fence, every rut in the road, every bush and tree long before we came to it. The Soldier of the Valley |Nelson Lloyd 

Not having completed the loading of his gun, Tom hastily rode behind a dense bush, and concealed himself as well as he could. Hunting the Lions |R.M. Ballantyne 

About an hour after resuming their walk, the major went off in hot pursuit of an enormous bee, which he saw humming round a bush. Hunting the Lions |R.M. Ballantyne 

Squinty heard one man cry, and then the comical little pig dodged under a bush, and kept on running. Squinty the Comical Pig |Richard Barnum 

The hairy animal, with the long tail, came straight for the bush behind which Squinty was hiding, and crawled through. Squinty the Comical Pig |Richard Barnum