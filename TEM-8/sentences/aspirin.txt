If you’ve got tense, sore, or injured muscles, before you reach for aspirin, try some heat therapy. Best heating pad for chronic pain and tense muscles |Irena Collaku |August 22, 2021 |Popular-Science 

Elderly people or those who regularly take other medications, including low-dose aspirin, may also have a greater chance of developing ulcers and suffering from complications, Stevoff said. Ibuprofen and ulcers: What Rep. Debbie Dingell’s emergency surgery can teach us |Allyson Chiu |May 26, 2021 |Washington Post 

I brought myself to such a totally relaxed state, without an aspirin or anything. Marie Mongan, champion of hypnobirthing, dies at 86 |Olesia Plokhii |February 11, 2021 |Washington Post 

I always carry provisions in my backpack—water, aspirin, Tylenol, power bars. Sen. Tammy Duckworth’s experiences as a soldier and wheelchair user shaped her response to the Capitol riots |Emma Hinchliffe |January 14, 2021 |Fortune 

I did it to breathe better, to stop the peculiar aches, to chase the kids longer and play basketball without needing aspirin. Tallying up a year of loss: A lot of pounds, too many loved ones, countless connections |Jerry Brewer |December 27, 2020 |Washington Post 

I assume he turned something else into aspirin and black coffee the next morning. Keep Christmas Commercialized! |P. J. O’Rourke |December 6, 2014 |DAILY BEAST 

It held aspirin, Sal Hepatica, cigarette papers and a Mason jar full of tobacco. Stanley Booth on the Life and Hard Times of Blues Genius Furry Lewis |Stanley Booth |June 7, 2014 |DAILY BEAST 

So, did he take over enough aspirin for hundreds of thousands of people? ‘Mission Congo’ Alleges Pat Robertson Exploited Post-Genocide Rwandans For Diamonds |Marlow Stern |September 7, 2013 |DAILY BEAST 

What the film does allege is that OBI may have purchased mass quantities of aspirin to ship to victims at Goma. ‘Mission Congo’ Alleges Pat Robertson Exploited Post-Genocide Rwandans For Diamonds |Marlow Stern |September 7, 2013 |DAILY BEAST 

If it's not covered by insurance, maybe you'll stay home, take aspirin, and live longer. How the Oregon Study Should Change Our Thinking |Megan McArdle |May 3, 2013 |DAILY BEAST 

Unable to sleep the next morning, I left John to his snoring and went for an aspirin and black coffee. The Holes and John Smith |Edward W. Ludwig 

I do have a little headache; would you happen to have any aspirin aboard? The Penal Cluster |Ivar Jorgensen (AKA Randall Garrett) 

Jo loaded up with aspirin to deaden a toothache which was worrying her. The Luck of Thirteen |Jan Gordon 

"Another aspirin is going to turn my luck," she thought, and therewith swallowed surreptitiously her last tabloid of the panacea. Alone |Norman Douglas 

She swallowed dose after dose of aspirin, until finally, with the first gray streaks of dawn, she at last fell asleep. The Mystery of the Fires |Edith Lavell