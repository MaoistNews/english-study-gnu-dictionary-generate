They were looking for “electorally generated veto points” — that is to say, elected bodies that could block change. The definitive case for ending the filibuster |Ezra Klein |October 1, 2020 |Vox 

It took about a year, but they changed that golden-share, that veto power over major transactions into what they called the Public Interest Foundation. Podcast: How Russia’s everything company works with the Kremlin |Anthony Green |September 30, 2020 |MIT Technology Review 

A state law passed just before Ikrata’s arrival gave the city of San Diego an effective veto at SANDAG. Politics Report: Audit Day at SANDAG |Scott Lewis and Andrew Keatts |September 19, 2020 |Voice of San Diego 

If reformers hope to succeed in curbing overpolicing, they will first have to overcome the challenge of underpolicing, which has often allowed officers to exercise an effective veto on reform. What Can Mayors Do When the Police Stop Doing Their Jobs? |by Alec MacGillis |September 3, 2020 |ProPublica 

San Diego needs support from just two other cities to exercise a veto. Politics Report: Who Will Get the Midway Rose? |Scott Lewis and Andrew Keatts |August 15, 2020 |Voice of San Diego 

Immediately, there was a national groundswell of voices calling for Arizona Governor Jan Brewer to veto the bill. Corporations Are No Longer Silent on LGBT Issues | |December 24, 2014 |DAILY BEAST 

By giving an artistic veto to a madman, we submit to the mindset of a slave. The Sony Hack and America’s Craven Capitulation To Terror |David Keyes |December 19, 2014 |DAILY BEAST 

In his veto message, Christie also chided Democratic lawmakers for “using their lawmaking authority to play politics.” Christie Bows to Iowa’s Pork Kings on Gestation Crates |Olivia Nuzzi |November 29, 2014 |DAILY BEAST 

With the second veto on Friday, however, all bets seemed to be off. Christie Bows to Iowa’s Pork Kings on Gestation Crates |Olivia Nuzzi |November 29, 2014 |DAILY BEAST 

In fact, because the House never voted, he never got the chance to sign or veto anything. SNL Parodies Schoolhouse Rock Hilariously, Gets A Lot Wrong |Jack Holmes, The Daily Beast Video |November 24, 2014 |DAILY BEAST 

The worthy knight not being now alive to veto the project, a figure of him has been placed opposite the College in Edmund Street. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

It made me furious, too, to see my ambition nipped with the frost of a possible veto from Miss Smawl. In Search of the Unknown |Robert W. Chambers 

This protection was exercised mainly through the use of the veto power given to the tribunes. The Two Great Republics: Rome and the United States |James Hamilton Lewis 

And this repeal is demanded because a single State interposes her veto, and threatens resistance! Select Speeches of Daniel Webster |Daniel Webster 

To make it possible for the tribunes to give such protection, the veto had been granted to them. The Two Great Republics: Rome and the United States |James Hamilton Lewis