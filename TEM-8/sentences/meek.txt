The show needed a moment for Nathan Shelley, the meek kit man played by Nick Mohammed, to draw up a play savvy enough to showcase his dormant coaching expertise. ‘Ted Lasso,’ built on charm and empathy, won over America. It won over the soccer world, too. |Thomas Floyd |July 22, 2021 |Washington Post 

I learned to shield my meek older brother, crying out for her to stop as her fists beat the self-worth out of me. The Few, the Proud, the Suicidal |Eugene Robinson |May 31, 2021 |Ozy 

Then Gwinnett argued that Uzuegbunam’s discussion of gentle Jesus meek and mild was “contentious” language with “a tendency to incite hostility,” and hence constituted “fighting words” unprotected by the Constitution. Campus speech rules are hurting students. They deserve recompense — even if it’s just $1. |George Will |March 12, 2021 |Washington Post 

While Clare sits head held high and regal, her blonde hair perfectly coiffed, Irene is meek, hoping to go unnoticed using the brim of her cloche hat to cover her eyes. Colorism Is Just The Tip Of The Iceberg In ‘Passing’ |Brande Victorian |February 1, 2021 |Essence.com 

This version is accessible to a 12-and-up audience and corrects the myths that Parks was a meek, passive player in American history. Calendar: Jan. 29-Feb. 4 |Philip Van Slooten |January 28, 2021 |Washington Blade 

Outside of that one encounter, however, Moses is pretty meek. Christian Bale: One Man's Moses Is Another Man's Terrorist |Candida Moss, Joel Baden |December 7, 2014 |DAILY BEAST 

But Kasich will perform some meek tap dance about repeal and replace, leaving the good parts. John Kasich’s Unforgivable Truth About Obamacare |Michael Tomasky |October 21, 2014 |DAILY BEAST 

You must celebrate the peacemakers, the poor at heart, the meek. What Jesus Really Means |Jay Parini |January 12, 2014 |DAILY BEAST 

He was not a very nice guy, and maybe Linda was very meek … Nobody would touch Marilyn. The True Life of Marilyn Chambers, the ‘Other’ Queen of Porn |Christine Pelisek |August 19, 2013 |DAILY BEAST 

From hip hop to electronic and indie rock and featuring artists like Meek Mill and Hem, see which music videos are becoming viral. Adam Levine, Grizzly Bear & More of the Best Music Videos of the Week (VIDEO) |Jean Trinh |February 1, 2013 |DAILY BEAST 

Take my yoke upon you, and learn of me; for I am meek and lowly of heart: and ye shall find rest unto your souls. Solomon and Solomonic Literature |Moncure Daniel Conway 

“He was worth saving,” remarked Stanley, stooping to pat the meek head of the dog. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

There was such pathos in her meek withdrawal into that little north room, that Brodrick hadn't the heart to keep her in it. The Creators |May Sinclair 

Then the meek-eyed woman reappeared, removed the dishes, returned once more, and looked fixedly at Thurstane's bloody sleeve. Overland |John William De Forest 

They sent a shiver of feeling through the village, and turned the meek white horse into a charger of war. When Valmond Came to Pontiac, Complete |Gilbert Parker