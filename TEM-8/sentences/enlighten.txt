This guide is meant to enlighten your search for the best night-vision goggles. Best night-vision goggles: How to see the galaxy when the Sun goes down |Irena Collaku |August 15, 2021 |Popular-Science 

Chastened by the realities laid bare by the pandemic, a lot of companies are embracing fresh-start affirmations and promising newly enlightened, gentler, more equitable workplaces. How to run a feminist company |Lila MacLellan |June 10, 2021 |Quartz 

Gradually, Momo grows enlightened to the oppression of androids, connecting the dots between a surgery she had as a child and the disappearance of her childhood best friend. Politics and the pandemic have changed how we imagine cities |Joanne McNeil |April 28, 2021 |MIT Technology Review 

If you care even a little bit about New York City — and possibly even if you don’t — this book will enlighten and quite possibly thrill you. How Does New York City Keep Reinventing Itself? (Bonus) |Kurt Andersen |March 21, 2021 |Freakonomics 

The shame and sorrow these young women suffer in the 1890s is not so different from what women trying to get pregnant — or end a pregnancy — endure in our own supposedly enlightened era. In Anna North’s riveting ‘Outlawed,’ there’s nothing more dangerous than a childless woman |Ron Charles |January 7, 2021 |Washington Post 

Maybe some of you Brits who followed me over from the dear old Graun could enlighten. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

Creator Rod Serling was compelled by the need “not to just entertain but to enlighten.” How a War-Weary Vet Created ‘The Twilight Zone’ |Rich Goldstein |November 13, 2014 |DAILY BEAST 

With Sinclair as our non-judgmental guide, we see that these revelations are not meant to shock us but, perhaps, enlighten us. ‘High Maintenance,’ Like a Good High, Is Funny and Sometimes Unsettling |Caitlin Dickson |November 11, 2014 |DAILY BEAST 

They're supposed to give audiences hope and enlighten them while promoting positive emotions. Inside ‘Leviathan’: Russian Filmmaker Andrey Zvyagintsev’s Award-Winning Anti-Putin Cannes Film |Richard Porton |May 27, 2014 |DAILY BEAST 

Can someone please enlighten me: is Girls supposed to be serious? Why ‘Girls’ Is Bad for Women |Emma Woolf |March 31, 2014 |DAILY BEAST 

We certainly do intend to deal fairly with Liberia, and give the reader every information that may tend to enlighten them. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

Jack shivered as he recalled the Rev. Charles Mason's picture of that home, but he would not enlighten her. The Cromptons |Mary J. Holmes 

Juana, too late aware of our laws and habits and administrative customs, did not enlighten her husband soon enough. Juana |Honore de Balzac 

Sensible and peaceable people enlighten themselves; their light spreads itself gradually, and in time reaches the people. Superstition In All Ages (1732) |Jean Meslier 

Man is so little enlightened to-day only because we had the precaution or the good fortune to enlighten him little by little. Superstition In All Ages (1732) |Jean Meslier