She must toil on, sometimes far into the night, to satisfy the wants of her growing family. America forgot how to make proper pie. Can we remember before it’s too late? |Megan McArdle |July 1, 2021 |Washington Post 

I'm concerned that so many people experienced complications, for want of a more inclusive word. The Trailer: A Texas free-for-all, as twenty-three candidates battle for a swing House seat |David Weigel |April 22, 2021 |Washington Post 

In his research, he and his colleagues have found that when faced with decisions, CEOs rarely give weight to the wants and needs of stakeholders, largely because there is little value or profit incentive to do so. Stakeholder capitalism: Is it working, or is it all talk? |McKenna Moore |November 3, 2020 |Fortune 

Every buyer has their own list of wants, needs, and must not have’s in their head. What are the factors that make us move? |Joseph Hudson |September 4, 2020 |Washington Blade 

You can paint a spoon any color you want and go crazy with experimental patterns. Turn an old spoon into a fishing lure that’ll catch almost anything |By Joe Cermele/Field & Stream |June 25, 2020 |Popular-Science 

So it might be me projecting my desires onto Archer to want to just get away from work for a few weeks. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Models in Israel will have to maintain a BMI of 18.5 or higher if they want to stay employed. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

Why would “they” want to crush him just for attempting to buy something twenty years ago? Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

One wants speech to be free, but one doesn't actually want to hear it. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

No one seems to know who that is—or why they would want to do such a thing. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

Now first we shall want our pupil to understand, speak, read and write the mother tongue well. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

The sailors sometimes use it to fry their meat, for want of butter, and find it agreeable enough. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

It will be a busy session; and I want to see if I can't become a useful public man. Elster's Folly |Mrs. Henry Wood 

Do you want the marriage of your daughter with the rich and Honourable Harry broken? The Joyous Adventures of Aristide Pujol |William J. Locke 

I want to see the sort of thing happening to schools that has already happened to many sorts of retail shops. The Salvaging Of Civilisation |H. G. (Herbert George) Wells