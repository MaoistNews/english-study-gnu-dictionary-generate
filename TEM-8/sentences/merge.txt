Electric car companies Arrival, Canoo, ChargePoint, Fisker, Lordstown Motors, Proterra and The Lion Electric Company are some of the companies that have merged with SPACs — or announced plans to — in the past year. With a reported deal in the wings for Joby Aviation, electric aircraft soars to $10B business |Jonathan Shieber |February 12, 2021 |TechCrunch 

Whether Neuralink will eventually merge brains and Teslas is beside the point. Can privacy coexist with technology that reads and changes brain activity? |Laura Sanders |February 11, 2021 |Science News 

The origin is somewhat unclear, but it came shortly after the established National Football League merged with the upstart American Football League in 1966. What you need to know about Super Bowl LV |Sam Fortier |February 7, 2021 |Washington Post 

Florida SB 48 aims to merge and expand the multiple voucher programs that already exist into two programs. Betsy DeVos is gone — but ‘DeVosism’ sure isn’t. Look at what Florida, New Hampshire and other states are doing. |Valerie Strauss |February 5, 2021 |Washington Post 

The bill would also put the onus on merging companies to prove that they don’t pose a risk of reducing competition, taking that burden off of the government in specific cases. New antitrust reform bill charts one possible path for regulating big tech |Taylor Hatmaker |February 4, 2021 |TechCrunch 

It is his ability to merge moral sentiment, theological passion, and policy prescription that lights the fire of his rhetoric. The Unsung Heroism of Jesse Jackson |David Masciotra |September 7, 2014 |DAILY BEAST 

So there we have it: as smaller galaxies merge, so do their black holes. Black Hole Pair Caught in Feeding Frenzy |Matthew R. Francis |April 27, 2014 |DAILY BEAST 

The individual components merge with each other, creating news forms and images. Franck de las Mercedes Lost Everything in a Fire…Except His Faberge Egg |Justin Jones |April 8, 2014 |DAILY BEAST 

Eventually, all things merge into one, and a river runs through it. Pete Dexter’s Indelible Portrait of Author Norman Maclean |Pete Dexter |March 23, 2014 |DAILY BEAST 

In a poll conducted last month by KIIS, only 41 percent of Crimeans wanted to merge with Russia. Why America Must Stop Comparing Ukraine To World War II |Will Cathcart |March 10, 2014 |DAILY BEAST 

Yet out of the whole discussion of the matter some few things begin to merge into the clearness of certain day. The Unsolved Riddle of Social Justice |Stephen Leacock 

Whenever the political parties of a country merge their differences of opinion in one common cause, the end may be foreseen. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

They went reluctantly inside, to merge with the darkness of the interior. Space Prison |Tom Godwin 

There it narrowed abruptly, to merge into the sheer wall of the canyon. Space Prison |Tom Godwin 

The many societies of Earth began to merge into a single superstate. The Status Civilization |Robert Sheckley