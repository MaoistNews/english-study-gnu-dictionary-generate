These changes make obesity, diabetes, heart disease and other health issues more likely. Healthy screen time is one challenge of distance learning |Kathryn Hulick |September 11, 2020 |Science News For Students 

The analysis controlled for a long list of variables, including population density, income, race and age, as well as community health indicators such as prevalence of smokers, adult obesity, preventable hospital stays and physical inactivity. New Research Shows Disproportionate Rate of Coronavirus Deaths in Polluted Areas |by Lylla Younes, ProPublica, and Sara Sneath |September 11, 2020 |ProPublica 

That mechanism may explain part of why animal-based foods like red meat are associated with higher risk of obesity and diabetes, though it’s not yet clear how much it matters. BCAA supplements can enhance your workout, but should you take them? |Amy Schellenbaum |September 10, 2020 |Popular-Science 

Underlying conditions like severe obesity or high blood pressure were linked to more serious illness or death. A sobering breakdown of severe COVID-19 cases shows young adults can’t dismiss it |Erin Garcia de Jesus |September 9, 2020 |Science News 

GLP-1 drugs include Novo’s Ozempic for diabetes and Saxenda for obesity. Weight loss and obesity drugs could help fight COVID-19, Novo Nordisk says |kdunn6 |September 4, 2020 |Fortune 

Nor do these studies address the structural and systematic issues that contribute to obesity, such as poverty and stress. Why Your New Year’s Diet Will Fail |Carrie Arnold |December 30, 2014 |DAILY BEAST 

The NFL, for instance, has a fitness campaign designed to address childhood obesity. College Football Fattens Players Up and Then Abandons Them |Evin Demirel |October 4, 2014 |DAILY BEAST 

The show could feature various obesity researchers who might highlight the latest thinking in long-term weight management. ‘The Biggest Loser’ Could Be TV’s Most Important Show Ever |Daniela Drake |September 26, 2014 |DAILY BEAST 

After all, here we are, in the middle of a global obesity crisis; and there they are running a metabolic lab on television. ‘The Biggest Loser’ Could Be TV’s Most Important Show Ever |Daniela Drake |September 26, 2014 |DAILY BEAST 

If The Biggest Loser could correct this misconception, it would do a lot to reduce anti-obesity prejudice. ‘The Biggest Loser’ Could Be TV’s Most Important Show Ever |Daniela Drake |September 26, 2014 |DAILY BEAST 

He was a tall man, which conveniently helped conceal a tendency toward obesity. The Envoy, Her |Horace Brown Fyfe 

Thou, cowardly and treacherous comrade, who hidest thy sick pasha-like obesity in the corner of thy stage-box! The Nabob |Alphonse Daudet 

In a letter to Sir Harry Erskine, in 1756, he complains of this tendency to obesity. Life and Correspondence of David Hume, Volume II (of 2) |John Hill Burton 

In any medical paper to-day you are almost sure to see an article on neurasthenia and obesity. Nervous Breakdowns and How to Avoid Them |Charles David Musgrove 

She was another of those wonders of obesity, unable to stand excepting on all fours. The Discovery of the Source of the Nile |John Hanning Speke