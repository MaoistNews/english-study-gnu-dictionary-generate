The facade of the building is covered with rows of glowing plastic carolers, a snowman, drummer boys and candy canes, and decked with garlands of evergreens. Savor the spirits of the season at these outdoor holiday-themed bars |Fritz Hahn |December 11, 2020 |Washington Post 

A 140-year-old building in Brussels has been refurbished with insulation on the facades and the roof and improved ventilation. Authenticity Versus Longevity for Preservation: Which Matters More? |Nick Fouriezos |December 11, 2020 |Ozy 

Leonsis draped a facade of Verizon Center with Wall’s image. John Wall never let down Southeast D.C. even if the basketball was frustrating |Kevin Blackistone |December 3, 2020 |Washington Post 

Her silvery voice scaled the facade of the building as the setting sun seemed to stall awhile to listen. WNO pop-up opera truck delivers music to the public, but it’s not a perfect fit |Michael Andor Brodeur |October 28, 2020 |Washington Post 

Since El-Waylly came forward, the everything-is-perfect facade of the famous test kitchen has crumbled. Bon Appétit Announces Dawn Davis, Publishing Heavyweight, as New Editor-in-Chief |Elazar Sontag |August 27, 2020 |Eater 

In keeping with the facade, Williams showed himself to be dedicated preacher who “knows his scripture.” Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 

In an ancient stone carving, warriors brandishing shields and swords swarm over the columned facade of a grand temple. Iraq’s Long-Lost Mythical Temple Has Been Found…and Is In Danger of Disappearing Again |Nina Strochlic |July 24, 2014 |DAILY BEAST 

We, like his various conquests, were seduced by his facade of invincibility and haunted past. What's Happened to Don Draper? Why Everyone’s Favorite ‘Mad Men’ Stud Needs His Mojo Back |Lizzie Crocker |April 16, 2014 |DAILY BEAST 

Another clock, outside on the 42nd St. facade, is the world's largest example of Tiffany glass. Grand Central Terminal: 100 Years, 100 Facts |Sarah Begley |February 1, 2013 |DAILY BEAST 

He also has a deep, creamy voice and calm demeanor that completes his fallen-hero facade. ‘The Hobbit’: 19 Changes from J.R.R. Tolkien’s Novel to Peter Jackson’s Movie |Anna Klassen |December 14, 2012 |DAILY BEAST 

In vain he sought support against one of the porphyry columns at the facade of the mosque. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 

So one morning she made her way towards the grotesque facade of the humble, silent home where she had spent her childhood. At the Sign of the Cat and Racket |Honore de Balzac 

It has a large brick church with a decidedly Flemish facade, and a detached pagoda-like belfry. The Head Hunters of Northern Luzon From Ifugao to Kalinga |Cornelis De Witt Willcox 

Silhouettes of tall poplars loomed against the blackness; occasionally a lamp revealed the milky blue facade of a house. A Traveller in War-Time |Winston Churchill 

One turns back to the still old house, and sees a grey and lichenous facade with a very finely arched entrance. Tono Bungay |H. G. Wells