You might recall that McCarthy famously exulted in 2015 that GOP Benghazi investigations were a success because Hillary Clinton’s “numbers are dropping.” GOP reaction to troop deaths shows what a Republican House would really mean |Greg Sargent |August 27, 2021 |Washington Post 

Bell, one of the Nationals’ highest-profile offseason acquisitions, pumped his fist and exulted as the ball exited for his fourth homer of the season. A team meeting, Patrick Corbin’s strong start and Josh Bell’s blast lift Nats over Phillies |Gene Wang |May 13, 2021 |Washington Post 

I would like to see the College of Fine Arts not only reestablished, but see it exulted. Howard University names actress and alumna Phylicia Rashad as dean of College of Fine Arts |Keith L. Alexander |May 12, 2021 |Washington Post 

“This is a shot in the arm, no pun intended,” exulted Jay Kornegay, vice president of the SuperBook at Westgate Las Vegas, the city’s largest sportsbook. Signs of life amid signs of concern in the second year of the coronavirus pandemic |Joel Achenbach, Ryan Slattery, Kayla Ruble, Caren Chesler |March 25, 2021 |Washington Post 

Picture a Senate where nothing gets through unless it gets through me, and where I exult in thwarting the agenda of the opposing party at every turn, even for wafer-thin reasons. McConnell threatens to hold the Senate hostage unless he can keep holding the Senate hostage |Alexandra Petri |March 19, 2021 |Washington Post 

Since his videos exult in the killing of innocent civilians, any cross-examination would have emphasised his inhumanity. Bin Laden Should Have Been Captured, Not Killed |Geoffrey Robertson |May 3, 2011 |DAILY BEAST 

Could she obtain a triumphant acquittal, through the force of her own integrity, she would greatly exult. Madame Roland, Makers of History |John S. C. Abbott 

It caused her to exult in the face of the great golden October sunset piled high in the west. The Creators |May Sinclair 

They heard but saw nothing, only the savage heart of brutus found time to exult—his enemies were perishing. It Is Never Too Late to Mend |Charles Reade 

They are desperate, then, and seem to exult in devilry of all kinds. A Final Reckoning |G. A. Henty 

We went on to exult in the noble independence of the American character in all classes, at some length. A Traveler from Altruria: Romance |William Dean Howells