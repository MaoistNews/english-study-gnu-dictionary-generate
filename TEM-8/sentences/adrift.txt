I was engaged, and when that engagement was called off, I felt adrift. Tips for visiting U.S. national parks from someone who visited them all |Andrea Sachs |December 17, 2020 |Washington Post 

So it’s reasonable to conclude that these buoys mimic how well ancient watercraft set adrift in the same area might have fared, the researchers say. Ancient humans may have deliberately voyaged to Japan’s Ryukyu Islands |Charles Choi |December 3, 2020 |Science News 

After that hard-hitting game, they will face a Cowboys team that seems adrift. NFL games on TV: Games will look different; Tom Brady vs. Drew Brees, Part 2 |Cindy Boren |November 8, 2020 |Washington Post 

Ultimately, she finds that her motherland is a place of perpetual migration, and at long last, she feels less adrift. A Writer Retraces Her Family's Past in Taiwan |Frances Nguyen |October 15, 2020 |Outside Online 

In her view, now is a great time for the ritually and spiritually adrift to shop around for their ritual fit. Why do we miss the rituals put on hold by the COVID-19 pandemic? |Sujata Gupta |August 14, 2020 |Science News 

Once we were discussing Lifeboat, a Hitchcock film that takes place almost entirely in a small boat adrift at sea. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Adrift in senility and depression, Hitchcock is dismantling his life, putting it away. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Still, “They were my island of misfit toys,” she says, adrift. Best Career Arc Ever: From Burlesque To Bartending |Anne Berry |September 13, 2014 |DAILY BEAST 

The reason Price of Fame ultimately becomes tiresome is our increasing awareness of how adrift the woman at its center is. Clare Boothe Luce's Vapid Second Act |Wendy Smith |July 5, 2014 |DAILY BEAST 

Each experience—like so many others in her life—left her wounded, weary, adrift. Maya Angelou Knew How To Inspire As A Writer, Teacher, and Great Human Being |Joshua DuBois |May 28, 2014 |DAILY BEAST 

If we set him adrift the poor child would starve—unless the cat got him. The Tale of Grandfather Mole |Arthur Scott Bailey 

They encountered a score of ruffians who had cut themselves adrift from the Gwalior contingent. The Red Year |Louis Tracy 

Joe was out on the boom, getting the reef-earrings adrift, when the first of the chapter of accidents came. The Chequers |James Runciman 

The boats no longer looked as if cutting their way through the lands, but adrift on a great lake. Ancestors |Gertrude Atherton 

He had made and set adrift those powder kegs, fixing them so that they would explode on touching anything. Stories of Our Naval Heroes |Various