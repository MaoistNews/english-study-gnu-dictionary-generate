So sometimes I think I would overreact over little things just because I’m hypercritical of myself. Vincent And Briana Of ‘Married At First Sight’ Explain The Perks Of Marrying A Stranger And Finding Love On TV |Victoria Uwumarogie |June 17, 2021 |Essence.com 

Her mother ran the household with a hypercritical eye, although she could stand only for short periods. Cecilia Chiang, grand dame of Chinese cooking in America, dies at 100 |Tim Carman |October 28, 2020 |Washington Post 

Quite a number of Marxian critics find themselves in the same position as the hypercritical French sculptor. The life and teaching of Karl Marx |M. Beer 

To be hypercritical, I might suggest that perhaps occasionally the version is rather too literal. Punch, Or The London Charivari, Vol. 100., February 7, 1891 |Various 

Be not hypercritical, for Heart's sake, against a man whose aim it is to help the cause of Heart. Heart |Martin Farquhar Tupper 

The public ear has become dainty, fastidious, hypercritical; hence the Ballad-Singer languishes and dies. The History of the Catnach Press |Charles Hindley 

The rest of the evening I could enjoy to my heart's content with no hypercritical glances following me around. Vacation with the Tucker Twins |Nell Speed