They might have been the most ingratiating band in the world. Gwyneth Paltrow Haunts Coldplay’s Self-Conscious Breakup Album ‘Ghost Stories’ |Andrew Romano |May 20, 2014 |DAILY BEAST 

But the little blooper that resulted made Fallon seem more ingratiating than ever. Can Jimmy Fallon Be Himself on ‘The Tonight Show’? |Tom Shales |February 17, 2014 |DAILY BEAST 

Though David was criticized for tossing softball or ingratiating questions, he, in fact, knew what he was doing. The Private David Frost |John M. Florescu |September 3, 2013 |DAILY BEAST 

He could be petty and mean-spirited to subordinates, ingratiating and sycophantic to bosses and celebrities. The Only Sportscaster That Mattered: New Biography of Howard Cosell |Robert Lipsyte |November 20, 2011 |DAILY BEAST 

Man gets divorced late in life and copes by ingratiating himself with unsuspecting Brooklynites. Bill Murray Crashes Karaoke and More Crazy Moments |The Daily Beast |January 7, 2011 |DAILY BEAST 

But in May, 1819, he was recalled to France, and soon found means of ingratiating himself with the Bourbons. Napoleon's Marshals |R. P. Dunn-Pattison 

I have looked in,” said Aristide, with his ingratiating smile, “to see whether you are ready to go to the Madeleine. The Joyous Adventures of Aristide Pujol |William J. Locke 

"Can't bear to stay near a man that mentions so much money in a breath," said Scattergood, with his most ingratiating grin. Scattergood Baines |Clarence Budington Kelland 

This is a thing I would despise in anybody else; but he is so jolly insidious 240 and ingratiating! The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

To Buckingham and Company the idea of governing such a "simpleton," and thus ingratiating themselves with the King, was enticing. Court Beauties of Old Whitehall |W. R. H. Trowbridge