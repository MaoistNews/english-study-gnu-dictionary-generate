People seem intuitively to understand that the virus doesn’t respect identity or nationality, that it is a symptom of a larger, transnational condition. The virus caused more than a pandemic. It set us all ablaze. |Philip Kennicott |February 5, 2021 |Washington Post 

The United Nations estimates that perhaps more than 10 million people throughout the world are stateless — meaning they have no true nationality and are citizens of nowhere. The New Meaning of Citizenship |Daniel Malloy |December 13, 2020 |Ozy 

Participants who had positive feelings about people of other races, nationalities, and ethnicities estimated cities in Mexico and Canada as being closer than those who held negative or neutral feelings. How border walls trick the human brain and psyche |Jessica Wapner |October 2, 2020 |Popular-Science 

When we look at everyone’s nationalities, it becomes clear that we’re all more similar in how we experience loneliness than we might expect. Letter-writing staved off lockdown loneliness. Now it’s getting out the vote. |Tanya Basu |September 18, 2020 |MIT Technology Review 

They may even unintentionally favor players of certain nationalities, races, ages or backgrounds. A robot referee can really keep its ‘eye’ on the ball |Kathryn Hulick |August 20, 2020 |Science News For Students 

Hossein Darakhshan, born in Tehran on January 7, 1975, has dual Canadian-Iranian nationality. Iran’s Blogfather Walks Free After Six Years in Jail |IranWire |November 21, 2014 |DAILY BEAST 

She did, however, choose to surmise that “The nationality of the shooter, it sounds Hispanic, Latino,” based on his name. The Ugliest, Creepiest Responses to the Fort Hood Shooting |Caitlin Dickson |April 3, 2014 |DAILY BEAST 

Is falafel Israeli and can fried chickpeas have a nationality? Watching Anthony Bourdain in Palestine |Maysoon Zayid |September 19, 2013 |DAILY BEAST 

VVD, the biggest party in Dutch government, is even in favor of stripping them of Dutch nationality. Seduced by War, Europeans Join the Fight in Syria |Nadette De Visser |June 11, 2013 |DAILY BEAST 

Israel is guilty of blurring the line between nationality and faith, too. No, Most Palestinians Aren't Anti-Semites |Maysoon Zayid |May 9, 2013 |DAILY BEAST 

It was a fatal error, for though the Spanish people might despise their King, they were intensely proud of their nationality. Napoleon's Marshals |R. P. Dunn-Pattison 

Foreign families of neutral nationality sought more tranquil asylum far beyond the suburbs or on ships lying in the harbour. The Philippine Islands |John Foreman 

An American woman therefore who marries an alien takes the nationality of her husband. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

But if he was unconcerned as to family descent, he was far from indifferent as to nationality. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

They have no nationality, and are officially described as “Filipinos under the protection of the United States.” The Philippine Islands |John Foreman