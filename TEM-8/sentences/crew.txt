Although I’m a little self-conscious, I’m proud of my work on it— shot on a hot summer day with a safe and socially distant crew in my garage. Fortune launches a new community for leaders who want to change business for good |Ellen McGirt |September 15, 2020 |Fortune 

There’s still a lot to learn about how every team is going to perform this season, but the entire Hot Takedown crew is united in hoping that Washington permanently adopts the clunky, quaint “Football Team” nickname. Reading The Right Amount Into The NFL’s Week 1 |Sarah Shachat |September 15, 2020 |FiveThirtyEight 

Military chopper pilots and crews have rescued another 78 people who had been trapped on the mountain amid the flames. A California mountain community loses its heart |Mike Silverstein |September 10, 2020 |Washington Blade 

In this week’s FiveThirtyEight Politics podcast, the crew discuss which states could be competitive in the 2020 election. States That Are Not Normally Competitive Are Competitive In 2020 |Galen Druke |September 9, 2020 |FiveThirtyEight 

In this installment of the FiveThirtyEight Politics podcast, the crew discusses how those trends are playing out and what it means for the 2020 electoral map. Politics Podcast: The 2020 Electoral Map Could Get Weird |Galen Druke |September 8, 2020 |FiveThirtyEight 

The brokers then scout out potential “crew members” who can earn substantial discounts for working the journey. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

Specifically, what briefing did the flight crew receive before they went to the airplane? Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Carlisle writes that the Air Force would want a crew ratio of 10 to one for each drone orbit during normal everyday operations. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

J Crew did not give back the money it incidentally made off of Mrs. Obama. One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

Crew members had to cut through the ice on the streets to get shots. Speed Read: The Juiciest Bits From the History of ‘Purple Rain’ |Jennie Yabroff |January 1, 2015 |DAILY BEAST 

I asked whether he or the crew had seen any prodigious birds in the air about the time he first discovered me? Gulliver's Travels |Jonathan Swift 

It was well that Monsieur de Biancourt was wiser than many of his crew, whose sole cry was to kill them all. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Three days after we left the port, a discovery was made of another addition to the number of the crew. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

On the following day they were again seen, and fired upon by the boat's crew of the Dick. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Peroo, well known to the crew, had possessed himself of the wheel, and was taking the launch craftily up-stream. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling