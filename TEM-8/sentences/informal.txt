SafeBoda has made a name for itself as a hassle-free alternative to the informal motorcycle taxis. Can Motorbike Taxis Fix Africa’s E-commerce Problem? |Charu Kasturi |February 10, 2021 |Ozy 

The Sundance rabbit hole was no less real for being streamed instead of screened, as my informal tally of 24 films over six days suggested. Even in a world turned upside-down and an all-virtual Sundance, the movies survived |Ann Hornaday |February 5, 2021 |Washington Post 

Last year Woods and the chief executive of Chevron, Mike Wirth, had informal talks about a merger, the Wall Street Journal reported. Oil companies’ losses in 2020 were staggering. And that was before the government focused on climate change. |Will Englund |February 4, 2021 |Washington Post 

Both surprises were the result of an informal alliance between left-wing activists and business titans. The Secret History of the Shadow Campaign That Saved the 2020 Election |Molly Ball |February 4, 2021 |Time 

In the meantime, Stephanie has been part of the informal economy in Columbia, working as a babysitter and cleaning houses. A Maryland couple opened their home to a Honduran mother and son. They ended up sharing more than space. |Stephanie García |January 30, 2021 |Washington Post 

And in informal talks, Chinese leaders have compared hackers on both sides to unruly children who can only barely be controlled. Obama Could Hit China to Punish North Korea |Shane Harris, Tim Mak |December 20, 2014 |DAILY BEAST 

Burt is part of an informal, unpaid foreign policy team who regularly briefs Paul on international issues. Rand Paul Gives War a Chance |Olivia Nuzzi |August 18, 2014 |DAILY BEAST 

To the Peggy Noonans among us who cringe when Obama talks “down”: This is a deeply informal country. For a President Today, Talkin' Down Is Speaking American |John McWhorter |August 7, 2014 |DAILY BEAST 

Israelis have also waged a psy-war on Hamas, albeit more informal and spontaneous. Israel, Hamas, WhatsApp and Hacked Phones in the Gaza Psy-War |Itay Hod |July 26, 2014 |DAILY BEAST 

But trying to impose such order by chasing away informal commerce and culture is myopic. Great Cities are Born Filthy |Will Doig |July 13, 2014 |DAILY BEAST 

Dinner occurred in the middle of the day, and about nine in the evening was an informal but copious supper. Hilda Lessways |Arnold Bennett 

The system had then been in existence, in a more or less informal way, for about eight years. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

This is hardly a function—parties even in the big political country-houses are more or less informal. Ancestors |Gertrude Atherton 

A sort of informal council took place occasionally in the little house. The Amazing Interlude |Mary Roberts Rinehart 

It was impossible to feel a stranger to the Professor, in these circumstances of frequent and informal meeting. The Daughters of Danaus |Mona Caird