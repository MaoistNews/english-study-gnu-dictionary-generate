Rub this cream inside your chamois or bike shorts to help keep things lubricated, which prevents chafing and hot spots. The Gear That Got Me Through My First Bike Tour |agintzler |November 23, 2021 |Outside Online 

There could also be fluids that lubricate the fault, decreasing friction and making it easier for the plates to slip past each other. What can ‘silent earthquakes’ teach us about the next Big One? |JoAnna Wendel |November 11, 2021 |Science News For Students 

First and foremost, it’s important to keep your belt lubricated to keep it functioning at optimal use and stave off repairs. The best treadmills for walking and running all year round |Chelsea Frank |October 13, 2021 |Popular-Science 

Enthusiasts often build their own boards and nerd out over things like lubricating the switches for a perfectly silky feel. The best keyboards to upgrade your computer setup |Jasmine Harding |October 1, 2021 |Popular-Science 

Even a well lubricated chain-drive model will be louder than a belt-drive or wall-mounted garage door opener. Best garage door opener for your home |Irena Collaku |August 23, 2021 |Popular-Science 

Beauty, fame, It Girl status, and old money (never new) all lubricate the entry process. Inside London’s Hottest Celebrity Haunt—But How Long Will Chiltern Firehouse Burn? |Lizzie Crocker |June 9, 2014 |DAILY BEAST 

This oil was used to lubricate the anemometer and other instruments exposed outside. The Home of the Blizzard |Douglas Mawson 

Lubricate the embossed cams in the cam housing with a thin film of vaseline every fifty hours of actual running. Aviation Engines |Victor Wilfred Pag 

Lubricate the stopper and insert it in the mouth of the jar, with the handle in a line with the two side tubes. The Elements of Bacteriological Technique |John William Henry Eyre 

When the pressure on the pin or any bearing is over 800 pounds per square inch, oil is no longer able to lubricate it properly. Farm Engines and How to Run Them |James H. Stephenson 

The next operation after moulding the bullets is to lubricate them. Pistol and Revolver Shooting |A. L. A. Himmelwright