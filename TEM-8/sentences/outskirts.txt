The next day, after driving to Putney on the outskirts of London, we start the end of our journey. Biking With the Bard |Kara Cutruzzula |December 28, 2014 |DAILY BEAST 

Artillery and mortar duels all around the outskirts of Donetsk rumble angrily every day. Should the U.S. Arm Ukraine’s Militias? |Jamie Dettmer |November 24, 2014 |DAILY BEAST 

By pure chance I had been posted to Supreme Headquarters Allied Powers in Europe, SHAPE, on the outskirts of Paris. I Saw Nuclear Armageddon Sitting on My Desk |Clive Irving |November 10, 2014 |DAILY BEAST 

He blames Ukrainian officials for violating the ceasefire agreement and shelling the outskirts of Donetsk city. Ukraine Could Explode in the Next 48 Hours |Anna Nemtsova |November 10, 2014 |DAILY BEAST 

We are sitting in a ragged park behind a McDonalds restaurant on the outskirts of the Ukrainian capital. Corruption Eats Away at Ukraine Military |Charles McPhedran |October 21, 2014 |DAILY BEAST 

In the outskirts of the city, skirmishes between Spanish troops and rebels were of frequent occurrence. The Philippine Islands |John Foreman 

It was one of these rebel detachments that passed the four fugitives from Cawnpore on the outskirts of Bunnee. The Red Year |Louis Tracy 

The thatched roof farmhouse where he was born is still standing on the outskirts of the village. British Highways And Byways From A Motor Car |Thomas D. Murphy 

It was near the town, and Sarah, who rarely went beyond the neighbouring streets, now began to take long walks into the outskirts. Skipper Worse |Alexander Lange Kielland 

And that motor parts plant on the outskirts with its heavy back-log of defense orders that had compelled a doubling of its help. Hooded Detective, Volume III No. 2, January, 1942 |Various