It’s just a shame that these shows are often made in a style that is so tired by this point, it even does a disservice to the Dickens and Austen tales for which it was originally developed. The Long Song Is a Brilliant Tale of Slavery’s End in Jamaica, Frustratingly Told |Judy Berman |January 29, 2021 |Time 

Montgomery’s head of emergency management, Earl Stoddard, said those who have leaked the registration link did “a great disservice” to the community by complicating efforts to quickly inoculate the county’s top-priority group. D.C. will resume indoor dining Friday as coronavirus cases hover at elevated levels |Julie Zauzmer, Rebecca Tan, Erin Cox, Gregory S. Schneider |January 22, 2021 |Washington Post 

That would do any open world Star Wars game a major disservice. What we want from Ubisoft Massive’s open world Star Wars game |Mike Hume, Gene Park, Elise Favis |January 13, 2021 |Washington Post 

Unintentionally, the film’s opening also does a disservice to the subtlety of some of the performances, particularly Kirby’s. Pieces of a Woman Features a Superb Performance From Vanessa Kirby, If You Can Bear to Watch It |Stephanie Zacharek |January 8, 2021 |Time 

Increasing that walled garden power would be a disservice to users and to the digital economy, especially to small and upstart businesses. How A.I. can make digital advertising less creepy |jakemeth |December 17, 2020 |Fortune 

To call Wild an emotional film would be an egregious disservice to its astounding journey to screen. Crying With Laura Dern: The Star on Her Oscar-Worthy ‘Wild’ Turn |Kevin Fallon |December 3, 2014 |DAILY BEAST 

The lack of discussion and attention are a major disservice to women. Should Twitter Suspend LGBT Engineer Accused Of Raping Her Wife? |Emily Shire |October 8, 2014 |DAILY BEAST 

And, in doing so, are we ultimately doing them, as well as the feminist movement, a disservice? Lana Del Rey and the Fault in Our ‘Feminist’ Stars |Amy Zimmerman |June 11, 2014 |DAILY BEAST 

Maybe they help sell church services, but do they do a disservice to the gospel? Can’t Fill the House On Easter? Try Handing Out Gadgets |Matthew Paul Turner |April 20, 2014 |DAILY BEAST 

Not to deal with this and the way we have done it would have been a disservice to the series. Kentucky’s Finest Antihero: Walton Goggins on Justified’s Chameleon Villain |Allen Barra |February 11, 2014 |DAILY BEAST 

Well has Ennius said, "Kindnesses misplaced are nothing but a curse and disservice." Dictionary of Quotations from Ancient and Modern, English and Foreign Sources |James Wood 

It required six months of judicial labor to bring forth this result, which was of "infinite disservice to the crown." The Trial of Theodore Parker |Theodore Parker 

At least they have been of some service to our cause and of some disservice to yours, and that, I take it, is the purpose of war. In Hostile Red |Joseph Altsheler 

The author of those lines, on another occasion, rendered Mr. Bird a serious disservice. Mr. Punch's History of Modern England Vol. IV of IV. |Charles L. Graves 

To represent a Person fairly and without disservice to his Reputation, two Things are to be observ'd. A Short View of the Immorality, and Profaneness of the English Stage |Jeremy Collier