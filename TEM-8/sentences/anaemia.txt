Stricken by an incurable anaemia, he would remain for weeks without leaving his house, without doing any work. The Nabob |Alphonse Daudet 

Here occur fever, anaemia, neuralgic pains, and the eruptions on the skin and the mucous membranes. Essays In Pastoral Medicine |Austin Malley 

Anaemia and nervousness exist in an especially marked degree among women. Woman and Socialism |August Bebel 

Anaemia is a frequent result of overstrain, not to mention the constant colds and rheumatism due to overheated rooms. Women in Modern Industry |B. L. Hutchins 

Later effects have been sleeplessness and nervousness, tending in extreme cases to insanity; anaemia, digestive troubles. Encyclopaedia Britannica, 11th Edition, Volume 6, Slice 5 |Various