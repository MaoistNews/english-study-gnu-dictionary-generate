I’ve also found that it resists stains and cleans up easily, which is a major plus for road trips and winter camping. The Best Winter Car-Camping Upgrades |Bryan Rogala |November 24, 2020 |Outside Online 

Plus, the sudsing left stains on my lenses from the water I couldn’t shake off. The best ways to stop a mask from fogging up your glasses, ranked |Sandra Gutierrez G. |November 18, 2020 |Popular-Science 

Soil stains hint that there may have been such structures inside and that people were living there, she said. Former Jesuit plantation may hold quarters of the enslaved |Michael Ruane |November 9, 2020 |Washington Post 

You wrote that it “left a stain on their reputations, their department and the country.” Q&A with ‘Anonymous’: Miles Taylor on his secret, why he didn’t resign and more |Philip Rucker, Carol D. Leonnig |October 30, 2020 |Washington Post 

They can sop up moisture and help tackle stains on other surfaces, including rugs and carpets. Dish towels to tackle almost any mess |PopSci Commerce Team |September 10, 2020 |Popular-Science 

As mentioned, Yahoo has a black stain on its collaboration and severe breach of privacy. Alibaba’s Dark Side: Censoring Customers |Brendon Hong |November 18, 2014 |DAILY BEAST 

We ask our celebrities to pour their hearts out, and then chastise them if they stain our buttoned-up shirts. Welcome to Generation Overshare: Lena Dunham, Taylor Swift, and the Politics of Self-Disclosure |Marlow Stern |November 6, 2014 |DAILY BEAST 

While that is unlikely to happen, the very fact that it can is a stain on the American judicial system. 10-Year-Old Murder Defendant Shows Failure of U.S. Juvenile Justice System |Christopher Moraff |October 18, 2014 |DAILY BEAST 

About “developers in bed with reviewers,” and the stain this leaves on the “integrity of games journalism.” It's Dangerous to Go Alone: Why Are Gamers So Angry? |Arthur Chu |August 28, 2014 |DAILY BEAST 

I put my hands behind my head, lay back and looked at a water stain on the ceiling. Almost Famous: A Father's Day Story |Alex Belth |June 15, 2014 |DAILY BEAST 

Beginners must be warned against mistaking the edges of cells, or particles which have retained the red stain, for bacilli. A Manual of Clinical Diagnosis |James Campbell Todd 

There are a number of bacilli, called acid-fast bacilli, which stain in the same way as the tubercle bacillus. A Manual of Clinical Diagnosis |James Campbell Todd 

Louis pressed his father's hand to his lips; that hand which was hardly washed from the stain of Wharton's blood! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

There was still visible on it the stain where he had wiped his hand, and this stain seemed certainly blood. Uncanny Tales |Various 

He did not think of the matter again till just as he was getting into bed, when he noticed a red stain upon his handkerchief. Uncanny Tales |Various