“She heard the ruckus, and then when she opened the door and looked down, he was being attacked by the alligator,” he said. Ida’s death toll reaches 4; officials urge those who evacuated not to return |Paulina Firozi, Tim Craig |August 31, 2021 |Washington Post 

I’m a bro’s bro at the end of the day, drinking beers, causing some ruckus. Jake Paul’s Going to Save Combat Sports? Bank On It |Eugene Robinson |June 2, 2021 |Ozy 

She’d raised eight children and two grandkids when Damion and his twin brother, Trayvion, were born, filling her home with a thrilling ruckus. The Child Care Industry Was Collapsing. Mrs. Jackie Bet Everything on an Impossible Dream to Save It. |by Lizzie Presser |May 1, 2021 |ProPublica 

Coming out of the bathroom, Acosta, Barron’s husband, wondered if perhaps an animal had gotten inside, causing a ruckus. A nationwide horror: Witnesses, police paint a picture of a murderous rampage that took 8 lives |Tim Craig, Mark Berman, Hannah Knowles, Marc Fisher |March 18, 2021 |Washington Post 

Core Web Vitals were part of Page Experience update, and, by far, caused the biggest ruckus. Google ranking factors to change search in 2021: Core Web Vitals, E-A-T, or AMP? |Aleh Barysevich |September 16, 2020 |Search Engine Watch 

This is not about saving Christmas from ‘them,’ but rather from ‘us’…I expect a pretty big ruckus. Kirk Cameron Saves Christmas from Abominable Killjoys (Other Christians) |Brandy Zadrozny |November 14, 2014 |DAILY BEAST 

This latest ruckus in the CRC brings the challenge of dealing with evolution into bold relief. The Christian Reformed Church Still Won’t Stand Up For Science |Karl W. Giberson |June 29, 2014 |DAILY BEAST 

To hear Ruckus soliloquize is to be a fly on the wall in a country club locker room. Aaron McGruder’s ‘The Boondocks’ Returns Without Aaron McGruder |Rich Goldstein |April 21, 2014 |DAILY BEAST 

Bratton and his companions followed to make sure the two did not cause a ruckus on the platform. My Patrol With the NYPD’s Bill Bratton |Michael Daly |March 14, 2014 |DAILY BEAST 

Hearing the ruckus, people came running from all over the building. Mel Brooks Is Always Funny and Often Wise in This 1975 Playboy Interview |Alex Belth |February 16, 2014 |DAILY BEAST 

The rest of their clan showed up at the foot of the stairs and made a great ruckus while I finished welding the door shut. The Repairman |Harry Harrison 

Mebbe sometime I meets up wid a army whut starts a ruckus wid me. Lady Luck |Hugh Wiley 

I suppose you'd feel happier if you weren't locked up in your cabin during the ruckus? Legacy |James H Schmitz 

"If those mikes pick up any unusual ruckus—any sharp increase in the noise level—come running," Malone said. Out Like a Light |Gordon Randall Garrett 

And all Ive got to say is that youre going to have a lovely ruckus! The Ranchman |Charles Alden Seltzer