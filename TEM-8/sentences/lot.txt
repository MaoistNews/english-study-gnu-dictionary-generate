There will undoubtedly be lots of changes, but as of press time, here’s a glimpse at some of the cinematic delights waiting for LGBTQ audiences. COVID complicates fall film releases |Brian T. Carney |September 17, 2020 |Washington Blade 

So both of these companies may lose a lot of their overseas exposure. The U.S. has nailed Chinese companies this year. Why hasn’t Beijing retaliated? |Veta Chan |September 17, 2020 |Fortune 

I think this year has been unprecedented in a lot of ways, and when things are very different than what they have been, people are fearful. Mobilizing the National Guard Doesn’t Mean Your State Is Under Martial Law. Usually. |by Logan Jaffe |September 17, 2020 |ProPublica 

If you’ve got a lot of that going for you, then striking oil can be a blessing. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

“It helped me a lot having to run for a while,” Davies said. A Canadian Teenager Is One Of The Fastest Soccer Players In The World |Julian McKenzie |September 16, 2020 |FiveThirtyEight 

I think a lot of it has to do with the attitude and the energy behind it and the honesty. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

There was a lot of positive feedback from people interested in non-gender binary people. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

If anything the work the two cops and the maintenance guy were doing deserves more respect and probably helped a lot more people. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

A lot of people ring in the New Year with vows to lose weight and exercise. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

And extortion makes a lot more sense before a story hits the news wire, not after. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

If the "Y" Beach lot press their advantage they may cut off the enemy troops on the toe of the Peninsula. Gallipoli Diary, Volume I |Ian Hamilton 

In the old world, poverty seemed, and poverty was, the natural and inevitable lot of the greater portion of mankind. The Unsolved Riddle of Social Justice |Stephen Leacock 

Seen thus poverty became rather a blessing than a curse, or at least a dispensation prescribing the proper lot of man. The Unsolved Riddle of Social Justice |Stephen Leacock 

Then the enemy's howitzers and field guns had it all their own way, forcing attack to yield a lot of ground. Gallipoli Diary, Volume I |Ian Hamilton 

I have seen a lot of Bolshevik propaganda and it is not very convincing stuff. The Salvaging Of Civilisation |H. G. (Herbert George) Wells