McCain said he wanted Obama to consult with his top generals about what to do on Iraq before offering his own advice. U.S. Aircraft Could Strike Iraq Tomorrow |Eli Lake, Tim Mak |June 13, 2014 |DAILY BEAST 

The White House did not even consult or inform Congress until after the prisoner release had begun. How Obama Convinced His Spies to Support the Taliban Prisoner Release |Eli Lake |June 3, 2014 |DAILY BEAST 

Consult your doctor to determine a healthy range of sodium for you. The Truth About Salt: Should You Shake the Habit? |DailyBurn |May 5, 2014 |DAILY BEAST 

He cannot consult Mr. Ed, but he could take some guidance from a horse that shares the same ancestry. Central Park’s Carriages Saved This Horse |Michael Daly |April 24, 2014 |DAILY BEAST 

(For those who insist on a more lawyerly consideration, consult the indispensible Skepdic on the subject). Introducing ‘Breatharianism,’ The Dumbest Diet of All Time |Kent Sepkowitz |March 2, 2014 |DAILY BEAST 

Consult not with him that layeth a snare for thee, and hide thy counsel from them that envy thee. The Bible, Douay-Rheims Version |Various 

Mrs. Irvin has decided to consult a palmist or a hypnotist or some such occult authority before dining with you this evening. Dope |Sax Rohmer 

I came here this afternoon on purpose to consult you, though I knew what a busy time it was with you during the hay harvest. The World Before Them |Susanna Moodie 

Tell ye, and come, and consult together: who hath declared this from the beginning, who hath foretold this from that time? The Bible, Douay-Rheims Version |Various 

Then, too, they wanted to fasten the porcelain insulators just right and had to consult one of the books several times. The Campfire Girls of Roselawn |Margaret Penrose