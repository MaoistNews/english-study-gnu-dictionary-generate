“I think that Joe Biden will be the person to protect Anthony and those with preexisting conditions,” the father says. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

Six unknowing months before the start of the Great Depression, my father arrived. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

While DeFazio and other lawmakers haven’t called for a permanent grounding of the jet, the father of a woman who died in the Ethiopia crash said the report raised questions about the plane’s return to service. Boeing crashes were the “horrific culmination” of multiple mistakes, House report says |kdunn6 |September 16, 2020 |Fortune 

I have been yelled at basically my entire life, from my father, my family, to high school. Momofuku’s David Chang on the big changes the restaurant industry needs to make to survive |Beth Kowitt |September 14, 2020 |Fortune 

Anthony Mallott said that no one asked his father to resign, and that he did so voluntarily. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

What matters is being honest, humble, and a faithful and loyal friend, father and member of your community. Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 

Father Joel Román Salazar died in a car crash in 2013; his death was ruled an accident, but the suspicion of foul play persists. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

Charles “Father” Coughlin, a raving anti-Semite, was one of the most popular radio hosts in the country. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

You will have your beloved father back sooner than you think, and you can visit and communicate with him all the while. Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 

“There is a heavy security presence but nothing has changed,” agrees Father Javier. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

There was a rumor that Alessandro and his father had both died; but no one knew anything certainly. Ramona |Helen Hunt Jackson 

And he was gone, and out of sight on the swift galloping Benito, before Father Gaspara bethought himself. Ramona |Helen Hunt Jackson 

At this same time they seized in Nangasaqui a servant of the father provincial, Matheo Couros, who was washing his clothes. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

He wanted to tell her that if she called her father, it would mean the end of everything for them, but he withheld this. The Homesteader |Oscar Micheaux 

The Father had been in sore straits of mind, as month after month had passed without tidings of his "blessed child." Ramona |Helen Hunt Jackson