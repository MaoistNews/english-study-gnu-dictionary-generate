The team identified genes possibly involved in the tuatara’s biological quirks including their long lives, which are the longest of any other reptiles besides tortoises. How tuatara live so long and can withstand cool weather |Jake Buehler |August 5, 2020 |Science News 

Once this sum exceeded 10, the tortoise was guaranteed to have finished the race. Are You A Pinball Wizard? |Zach Wissner-Gross |July 24, 2020 |FiveThirtyEight 

At this point, you just needed to determine when the tortoise had finished 20 percent of the race. Are You A Pinball Wizard? |Zach Wissner-Gross |July 24, 2020 |FiveThirtyEight 

Last week, the tortoise and the hare were about to begin a 10-mile race along a “stretch” of road. Are You A Pinball Wizard? |Zach Wissner-Gross |July 24, 2020 |FiveThirtyEight 

To better understand this, let’s take a closer look at the tortoise over time. Are You A Pinball Wizard? |Zach Wissner-Gross |July 24, 2020 |FiveThirtyEight 

Tortoise disqualified for technical reasons, first place awarded to Sputnik hare. Why Does the USA Depend on Russian Rockets to Get Us Into Space? |P. J. O’Rourke |June 22, 2014 |DAILY BEAST 

The same look of an unruly child behind his tortoise-shell glasses. Bernard-Henri Lévy on Ali Zeidan: Our Friend in Libya |Bernard-Henri Lévy |February 21, 2013 |DAILY BEAST 

“That was bizarre,” he said, his brown eyes getting wide behind his tortoise-shell glasses. Jeff Goldblum on Theresa Rebeck’s ‘Seminar,’ Celebrity Death Hoaxes & More |Lorenza Muñoz |November 8, 2012 |DAILY BEAST 

His art collection includes a live tortoise covered in gems and a "flavor organ" on which he can play gustatory fugues. Where Beauty Meets Decay |Blake Gopnik |May 23, 2012 |DAILY BEAST 

The tortoise Hollande, early on in his bid to become the Socialist nominee, had only two reporters on his beat. Against All Odds, Can Sarkozy Pull Out an Election Win vs. Hollande? |Tracy McNicoll |May 4, 2012 |DAILY BEAST 

The legs and arms were carved or made of costly woods, or inlaid or plated with tortoise-shell or the precious metals. The Private Life of the Romans |Harold Whetstone Johnston 

As he glanced through the window he saw an Englishman in the shop holding a tortoise, which he was turning about in his hands. The Animal Story Book |Various 

The sheath itself was hardly less remarkable, made of a single piece of tortoise shell, studded with golden bees. Napoleon's Young Neighbor |Helen Leah Reed 

The tortoise is found sculptured on some of the ruins at Uxmal; it was also stamped upon the coins of Grecian Thebes and gina. The Works of Hubert Howe Bancroft, Volume 5 |Hubert Howe Bancroft 

If you've spectacles, don't have a tortoise-shell rim, And don't go near the water—unless you can swim. The Book of Humorous Verse |Various