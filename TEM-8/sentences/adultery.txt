Additionally, adultery is not, in the grand scheme of things, the worst sin to have ever been committed. America’s strange obsession with marital affairs |Isaac Amend |January 29, 2022 |Washington Blade 

The story revolves around an associate pastor at an active Protestant church in suburban Chicago who’s troubled by his own envy and adultery. The 10 best books of 2021 |Washington Post Editors and Reviewers |November 18, 2021 |Washington Post 

It would have been unthinkable under the Taliban, which stoned people for adultery. Afghanistan: What’s at Stake? |Kate Bartlett |June 1, 2021 |Ozy 

Although Prince Charles had admitted to adultery in a prior television interview, this was the first time a senior royal had broken free of the conventions surrounding the palace and shone a light onto its inner workings. Meghan and Harry’s Interview Won’t End the Monarchy. But a Reckoning Is Coming |Dan Stewart |March 9, 2021 |Time 

The Army Reserve has opened an investigation into whether Cunningham engaged improperly with a subordinate’s family, in possible violation of the rules concerning adultery under the Uniform Code of Military Justice. In N.C., voters call Cunningham’s infidelity reckless, shocking. They still vote for the Democrat. |Rachael Bade, Pam Kelley |October 21, 2020 |Washington Post 

It also contains some clunky passages of adultery, temptations of the flesh, and general sexual awkwardness. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

But I say onto you, That whosoever looketh on a woman to lust after her hath committed adultery with her already in his heart. Keep Christmas Commercialized! |P. J. O’Rourke |December 6, 2014 |DAILY BEAST 

In fact, Rampal preached against adultery and “vulgar singing and dancing.” Is India’s Fallen ‘God-Man’ So Different From a Megachurch Pastor? |Jay Michaelson |November 21, 2014 |DAILY BEAST 

Pounding is charged with one count each of assault, adultery, and conduct unbecoming an officer. Commando Colonel Accused of Exposing his Lover to HIV |Jacob Siegel |November 19, 2014 |DAILY BEAST 

Typically, adultery charges are added to cases where there have been other offenses. Commando Colonel Accused of Exposing his Lover to HIV |Jacob Siegel |November 19, 2014 |DAILY BEAST 

Adultery is a cause recognized in all of them, for which an absolute divorce can be granted. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

We owe it neither to the Syriac tongue nor to the Hebrew, a jargon of the Syriac, in which adultery is called niuph. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

The women of Lacedæmon, we are told, knew neither confession nor adultery. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

Adultery is an evil only inasmuch as it is a theft; but we do not steal that which is given to us. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

The Lacedæmonians, therefore, had good reason for saying that adultery was impossible among them. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire)