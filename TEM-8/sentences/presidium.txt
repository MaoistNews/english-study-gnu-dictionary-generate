The deputy chairman of the presidium of the Donetsk Republic, Denis Pushilin, also predicts “good results.” Putin’s People Stage Their Bogus Vote |Jamie Dettmer |May 11, 2014 |DAILY BEAST 

During his absence, the outfit seems to have been run by a sort of presidium of the senior officers. Rebel Raider |H. Beam Piper 

More importantly, the presidium assumed the powers and functions of the National Assembly when the latter was not in session. Area Handbook for Bulgaria |Eugene K. Keefe, Violeta D. Baluyut, William Giloane, Anne K. Long, James M. Moore, and Neda A. Walpole 

The memophone began giving him the names of the Presidium and of the Chiefs of Managements. A Slave is a Slave |Henry Beam Piper 

In practice, the Assembly listens to the reading of bills drawn up by its Presidium and then votes unanimous approval. Area Handbook for Albania |Eugene K. Keefe 

The Presidium also designates ministry jurisdiction over various enterprises according to the recommendations of the premier. Area Handbook for Albania |Eugene K. Keefe