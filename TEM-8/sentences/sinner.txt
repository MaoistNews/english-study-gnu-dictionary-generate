Taken together, if swinging away on a 3-0 pitch in a blowout is a baseball sin, a lot of hitters are sinners. Baseball’s Most Disrespectful Home Runs |Alex Kirshner |June 14, 2021 |FiveThirtyEight 

Because of that high and holy standard, the most loving thing I can do as a fellow sinner is to speak the truth in love. Va. House of Delegates candidate defends anti-transgender comments |Michael K. Lavers |April 13, 2021 |Washington Blade 

We were always taught, you know, love the sinner, hate the sin. In Arkansas, Trans Youth Face the Country's Strictest Laws Yet |Raisa Bruner |April 9, 2021 |Time 

Essentially, Pope Francis is urging Christians to “love the sinner, but hate the sin.” Is Pope Francis Backpedaling on Gays? |Jay Michaelson |November 19, 2014 |DAILY BEAST 

As part of the “love the sinner, hate the sin” culture, many townspeople are still polite and cordial to their faces. Mississippi Is Hell for These Lesbians |Emily Shire |August 8, 2014 |DAILY BEAST 

Leila Hatami pecked the director of the Cannes Film Festival and was quickly denounced as a sinner by hardliners at home. The Kiss That Sent Iran Crazy and an Actress to Be Flogged in Public |IranWire |May 23, 2014 |DAILY BEAST 

I never was one,” he insisted—“even on the basis of an earthly definition of a saint as a sinner who keeps trying. Mandela: The Miracle Maker |Sam Seibert |December 5, 2013 |DAILY BEAST 

The third reason I think Richard Dawkins is a secret believer is because he, like me, is a sinner. Richard Dawkins May Be a Christian in Disguise, Even to Himself |Joshua DuBois |November 3, 2013 |DAILY BEAST 

Good is set against evil, and life against death: so also is the sinner against a just man. The Bible, Douay-Rheims Version |Various 

His holy book says: There is more joy over one sinner that repenteth than over ninety and nine just men. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

He which converteth the sinner from the error of his way shall save a soul from death, and shall hide a multitude of sins. The Ordinance of Covenanting |John Cunningham 

And in like manner, no sinner can say that he had a claim upon the Creator to be brought into being free from the curse. The Ordinance of Covenanting |John Cunningham 

By that the personal identity of the sinner is not altered; for it is the same being that sinned who is saved. The Ordinance of Covenanting |John Cunningham