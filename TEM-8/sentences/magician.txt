Wynn also paid $18 million to create an 88-acre animal habitat for the magicians outside Las Vegas. Siegfried Fischbacher, half of large-animal magic team Siegfried & Roy, dies at 81 |Matt Schudel |January 14, 2021 |Washington Post 

Matthew’s word Magi is a vague clue, since it can mean astronomers, wise men or magicians and was applied to people from all over. Here's What History Can Tell Us About the Magi |Olivia B. Waxman |December 29, 2020 |Time 

In the backroom, behind a curtain, masters of the craft such as Tony Slydini and Dai Vernon conducted private demonstrations for aspiring magicians. Al Cohen, D.C. magic shop proprietor who knew all the tricks, dies at 94 |Matt Schudel |December 18, 2020 |Washington Post 

They started calling it Al’s Magic Shop, and then a lot of professional magicians started hanging around, and before I knew it, I was in the magic business. Al Cohen, D.C. magic shop proprietor who knew all the tricks, dies at 94 |Matt Schudel |December 18, 2020 |Washington Post 

Commercial execs at social video app Triller are like those magicians. ‘We’re at the crux of it’: How TikTok rival Triller is brashly pitching advertisers |Seb Joseph |October 12, 2020 |Digiday 

He was a magician, an invisible teller of tales with the power to make my sides ache without telling a single joke. When Your Comic Hero Is an Alleged Rapist |Doug McIntyre |November 18, 2014 |DAILY BEAST 

But I remember the first week when Nigel called me the magician of dance. 'So You Think You Can Dance' Winner Ricky Ubeda Is Adorable, and Tired |Kevin Fallon |September 4, 2014 |DAILY BEAST 

After Fajuri acquired the box last year, he began gathering other memorabilia of the late, great magician. Get a Piece of Houdini Before He Disappears |Nina Strochlic |August 22, 2014 |DAILY BEAST 

“There was a time when he was just sort of another dead magician,” Culliton says. Get a Piece of Houdini Before He Disappears |Nina Strochlic |August 22, 2014 |DAILY BEAST 

Sara was pregnant, and the magician had to decide whether it was time to lay his cards down. Bob Dylan and the Writing of ‘Blonde on Blonde’ at the Chelsea Hotel |Sherill Tippins |December 3, 2013 |DAILY BEAST 

He made me think of an old time magician more than anything, and I felt that with a touch of his wand he could transform us all. Music-Study in Germany |Amy Fay 

As if a magician's wand had touched him, the garland of roses transformed him into a vision of Oriental beauty. The Awakening and Selected Short Stories |Kate Chopin 

Oh, sorcery of the most wonderful magician of letters the world has seen since Shakespeare! Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Is it quite true that Simon, called the Magician, was adored among the Romans? A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

Such was the Roman divinity which for so many ages was taken for Simon the Magician. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire)