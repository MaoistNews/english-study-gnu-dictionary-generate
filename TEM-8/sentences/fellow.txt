Alexander Golding is an MBA fellow at the George Washington University School of Business. Opportunity Zones haven’t fully reached their potential, but don’t write them off yet |jakemeth |September 16, 2020 |Fortune 

He even got fellow Canadian Drake to follow him on Instagram. A Canadian Teenager Is One Of The Fastest Soccer Players In The World |Julian McKenzie |September 16, 2020 |FiveThirtyEight 

Yelena Ionova is a postdoctoral fellow in quality of Medical Products, University of California, San Francisco. The ‘inactive' ingredients in your pills could harm you |By Yelena Ionova/The Conversation |September 15, 2020 |Popular-Science 

Varun Sivaram, visiting senior fellow at Columbia University’s Center on Global Energy Policy, is the former CTO of ReNew Power, India’s largest renewable-energy company. To confront the climate crisis, the US should launch a National Energy Innovation Mission |Amy Nordrum |September 15, 2020 |MIT Technology Review 

This Tuesday, I will be asking my fellow Board members to allow businesses to open up, if they can operate safely and NOT to enforce the State rules. Morning Report: San Diego Is Ignoring an Untapped Water Source |Voice of San Diego |September 15, 2020 |Voice of San Diego 

On Dec. 22, 1799, Sands told her cousins that she would be leaving to elope with a fellow boarder named Levi Weeks that night. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

He has even joked about how his fellow Republicans attack him. Why This Liberal Hearts Huckabee |Sally Kohn |January 6, 2015 |DAILY BEAST 

At least 29 fellow Republicans must vote against Boehner for a second ballot to be reached, and that seems very unlikely. The YOLO Caucus' New Cry for Attention |Ben Jacobs |January 4, 2015 |DAILY BEAST 

Instead, I spend much of my time criticizing my fellow atheists. The Case Against In-Your-Face Atheism |Steve Neumann |January 4, 2015 |DAILY BEAST 

An atheist counsels his fellow non-believers on how not to talk to people of faith. The Case Against In-Your-Face Atheism |Steve Neumann |January 4, 2015 |DAILY BEAST 

It was one of those long moments that makes a fellow draw his breath sharp when he thinks about it afterward. Raw Gold |Bertrand W. Sinclair 

He controlled himself betimes, bethinking him that, after all, there might be some reason in what this fat fellow said. St. Martin's Summer |Rafael Sabatini 

Man's enthusiasm in praise of a fellow mortal, is soon damped by the original sin of his nature—rebellious pride! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

It beats punching cows, though—that is, when a fellow discovers that he isn't a successful cowpuncher. Raw Gold |Bertrand W. Sinclair 

Don't chaff, Shirtings; you're a very good fellow, you know, but I'm not in a laughing humour. The Pit Town Coronet, Volume I (of 3) |Charles James Wills