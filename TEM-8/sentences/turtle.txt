The reptiles have long been of scientific interest because of their unclear evolutionary relationship with other reptiles, as they share traits with lizards and turtles as well as birds. How tuatara live so long and can withstand cool weather |Jake Buehler |August 5, 2020 |Science News 

Based on the genetic analyses, the researchers confirmed that the tuatara is more closely related to snakes and lizards than to crocodilians, birds or turtles. How tuatara live so long and can withstand cool weather |Jake Buehler |August 5, 2020 |Science News 

The earliest dinosaur eggs were more like leathery turtle eggs than hard bird’s eggs. Early dinosaurs may have laid soft-shelled eggs |Jack J. Lee |August 3, 2020 |Science News For Students 

Explore coral reef restoration, shipwrecks, sea turtles and more. Let’s learn about coral reefs |Bethany Brookshire |July 22, 2020 |Science News For Students 

These ecosystems are wildly diverse — home to everything from crabs to sea turtles. Let’s learn about coral reefs |Bethany Brookshire |July 22, 2020 |Science News For Students 

This Israel-Hamas war feels different, neither turtle nor scorpion even pretending anymore about seeking peace. Is It Just Me or Is the World Exploding? So Why Isn’t Obama Doing More? |Michael Tomasky |July 28, 2014 |DAILY BEAST 

A non-Sioux nation in the state, Turtle Mountain Band of Chippewa Indians, also said no. The Native Americans Who Voted for ‘The Fighting Sioux’ |Evan Weiner |June 26, 2014 |DAILY BEAST 

The commercial then shifts to a split screen of a cartoon turtle and Senate Minority Leader Mitch McConnell. Tea Party Candidate In Texas Makes Viral "Turtle Soup" Campaign Ad |Ben Jacobs |February 18, 2014 |DAILY BEAST 

In the meantime, Stovall has learned some political lessons in light of his turtle soup success. Tea Party Candidate In Texas Makes Viral "Turtle Soup" Campaign Ad |Ben Jacobs |February 18, 2014 |DAILY BEAST 

He was filming other television spots in January and his campaign manager and the producer wanted to do the turtle soup spot. Tea Party Candidate In Texas Makes Viral "Turtle Soup" Campaign Ad |Ben Jacobs |February 18, 2014 |DAILY BEAST 

To these we gave boiled rice, and with it turtle and manatee boiled. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

She careened so badly that the girls shrieked and Malcolm himself thought she would turn turtle. The Red Year |Louis Tracy 

For our use, besides bread-fruit and fish, there was a turtle weighing perhaps more than twenty pounds. A Woman's Journey Round the World |Ida Pfeiffer 

I shall give 'em real turtle from Birch's, and as for fizz, they shall swim in it if they like. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Thomas Simpson, companion of Mr. Dean in the discovery of the north-west passage, died by his own hand at Turtle river, aged 32. The Every Day Book of History and Chronology |Joel Munsell