The capsule did safely return to Earth, however, and provided valuable test data on the way. Boeing and NASA target December for second try at uncrewed orbital demonstration flight |Darrell Etherington |August 28, 2020 |TechCrunch 

That portion is based on Lockheed Martin’s Orion crew capsule that will be used to take humans into space on other missions. Here’s what NASA’s next moon lander may look like |Aaron Pressman |August 21, 2020 |Fortune 

That would make it a time capsule of the early solar system. In a first, astronomers spotted a space rock turning into a comet |Lisa Grossman |August 18, 2020 |Science News 

“In terms of a small capsule project, we haven’t done those kinds of numbers before,” he told Quartz. The Covid-19 tie-dye boom is real |Jenni Avins |August 10, 2020 |Quartz 

Later assessments by those with a firmer grasp of physics estimated the capsule would have hit the water at a bone-mulching 180 mph. The Story Behind the Eiffel Tower’s Forgotten Competitors |Fiona Zublin |August 10, 2020 |Ozy 

What waited was a capsule that was just as majestic as the celebration. New York’s Century-Old Time Capsule Is a Dud |Justin Jones |October 8, 2014 |DAILY BEAST 

A 1914 time capsule forgotten for years was finally opened at the New York Historical Society. New York’s Century-Old Time Capsule Is a Dud |Justin Jones |October 8, 2014 |DAILY BEAST 

The New York Historical Society plans to seal a new time capsule this week that is more reflective of our current pop culture. New York’s Century-Old Time Capsule Is a Dud |Justin Jones |October 8, 2014 |DAILY BEAST 

The treasures found within the capsule were mostly records that reflected those immediately involved with its planning. New York’s Century-Old Time Capsule Is a Dud |Justin Jones |October 8, 2014 |DAILY BEAST 

In addition to acting and singing, the British model is now set to launch her first capsule collection with DKNY this November. Kate Hudson Defends Butt Implants; Cara Delevingne Is Designing For DKNY |The Fashion Beast Team |June 10, 2014 |DAILY BEAST 

Each is surrounded by a gelatinous capsule, which is its distinctive feature (Fig. 9). A Manual of Clinical Diagnosis |James Campbell Todd 

Recognition of the pneumococcus depends upon its morphology, the fact that it is Gram-staining, and the presence of a capsule. A Manual of Clinical Diagnosis |James Campbell Todd 

When they form short chains, demonstration of the capsule is necessary to distinguish them from streptococci. A Manual of Clinical Diagnosis |James Campbell Todd 

During the time it is red, the outer shell is soft, but ultimately becomes perfectly hard, and resembles a wooden capsule. A Woman's Journey Round the World |Ida Pfeiffer 

It has a flower of pale green color and its fruit is a capsule containing three small brown seeds, with patches of black. The Wonder Book of Knowledge |Various