This is especially striking, though hardly surprising, in the case of Priebus, Mr. Outreach. Today’s GOP: Still Cool With Racist Pandering? |Michael Tomasky |January 7, 2015 |DAILY BEAST 

Mr. Huckabee far overshadows his kinder, gentler Gov. Huckabee. The Devil in Mike Huckabee |Dean Obeidallah |January 6, 2015 |DAILY BEAST 

Mr. Bachner found it by wandering through the market and identified a craftsmen here who works in a tiny booth. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

Mr. Bachner said it had been hard to introduce his work ethic and share his vision with the locals and his team. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

Mr. Bachner stayed because he realized the city is filled with artisans and the possibilities fascinated him. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

As long as may be necessary, Sam,” replied Mr. Pickwick, “you have my full permission to remain. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Hilda, trembling at the door, more than half expected Mr. Orgreave to say: "You mean, she's invited herself." Hilda Lessways |Arnold Bennett 

After all, may not even John Burns be human; may not Mr. Chamberlain himself have a heart that can feel for another? God and my Neighbour |Robert Blatchford 

It was with a feeling of relief on both sides that the arrival of Mr. Haggard, of the Home Office, was announced. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

“This is a distressing predicament for these young people,” thought Mr. Pickwick, as he dressed himself next morning. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens