That latter fear crossed my mind more than once during the evening. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

In both of these latter cases, their eyes show more focus than fun, like tonight is a job. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

The former is controlled by the Palestinian Authority, the latter by the government of Israel. Inside Hebron, Israel’s Heart of Darkness |Michael Tomasky |November 21, 2014 |DAILY BEAST 

The former believed in the role of the state as a provider, while the latter favored an iron fist approach to governance. How WWI Produced the Holocaust |J.P. O’Malley |November 21, 2014 |DAILY BEAST 

The trouble was, he alienated Pope Pius VI and Pius VII—the latter he actually arrested. Napoleon Was a Dynamite Dictator |J.P. O’Malley |November 7, 2014 |DAILY BEAST 

Mrs. Woodbury paints in oils and water-colors; the latter are genre scenes, and among them are several Dutch subjects. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Without the former quality, knowledge of the past is uninstructive; without the latter, it is deceptive. Pearls of Thought |Maturin M. Ballou 

The latter trod on the toes of the former, whereupon the former threatened to "kick out of the cabin" the latter. The Book of Anecdotes and Budget of Fun; |Various 

Give not up thy heart to sadness, but drive it from thee: and remember the latter end. The Bible, Douay-Rheims Version |Various 

He passed the latter part of his life in poverty, and towards the close of it, was confined in a madhouse. The Every Day Book of History and Chronology |Joel Munsell