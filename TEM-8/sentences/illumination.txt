The illumination of the darkness of this case is something we talked about. Four police officers shot Amadou Diallo 19 times. A new photography project names them. |Geoff Edgers |December 3, 2020 |Washington Post 

Each individual pixel is a diode that provides its own illumination, so the screen doesn’t need a backlight. The best entertainment tech of 2020 |Stan Horaczek |December 3, 2020 |Popular-Science 

The adolescent squid is transparent, but beneath its skin are light-reflecting pigments that, during its brief illumination, seemed to turn it to gold. Check out the breathtaking winners of the 2020 Wildlife Photographer of the Year contest |Rachael Zisk |October 20, 2020 |Popular-Science 

As scholar Adam Potkay noted in his 2007 book The Story of Joy, “joy is an illumination,” the ability to see beyond to something more. How to find joy during tough times |By Angela Gorrell/The Conversation |October 9, 2020 |Popular-Science 

The illumination comes not from the sun but from the infrared energy of the planet and its lower atmosphere. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

The book is fascinating partly for its illumination of the origins of current conventions. How the News Business Found Its Footing |Nick Romeo |June 22, 2014 |DAILY BEAST 

Rather than selective filtering for purposes of justification, information is instead used as the fuel for illumination. What Hockey Players Can Teach our Toothless Politicians |Dave Maney |April 29, 2014 |DAILY BEAST 

An affair that blossoms in his 40s and is quickly squelched by a departmental rival serves as his single true illumination. Must-Read College Novels: From “Lucky Jim” to “Pnin” |Sam Munson |August 13, 2012 |DAILY BEAST 

No matter the situation, the knowledge of handlers and the experts pales in comparison to her inner illumination. 10 Juicy Bits from Christine O’Donnell’s ‘Troublemaker’ |David Sessions |August 12, 2011 |DAILY BEAST 

Consider the opening of the first illumination, “After the Flood:” When Your Hero’s an SOB |Bruce Duffy |August 1, 2011 |DAILY BEAST 

The occupants of the room had been too absorbed with their own affairs to notice the gradual dimming of the illumination. The Boarded-Up House |Augusta Huiell Seaman 

An illumination broke over his whole face when he saw her and joined her under the orange tree. The Awakening and Selected Short Stories |Kate Chopin 

The intricate perforations of the lamp were inset with colored glass, and the result was a subdued and warm illumination. Dope |Sax Rohmer 

The iris of the human eye dilates and contracts with every shift of illumination, and the Time Observatory had an iris too. The Man from Time |Frank Belknap Long 

Nothing can be more beautiful of the kind than such an illumination seen from the ship. Journal of a Voyage to Brazil |Maria Graham