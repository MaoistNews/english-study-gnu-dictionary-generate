As such, Americans, and Republicans in particular, are becoming more interventionist in their foreign policy preferences. Rand Paul vs. the Real World |James Kirchick |September 10, 2014 |DAILY BEAST 

But the logic of the anti-interventionist left is built today around the same moral flaw that it was during the Cold War. Why Liberals Should Back Iraq Intervention |Michael Tomasky |August 11, 2014 |DAILY BEAST 

No Republican presidential candidate in recent memory has won the nomination on a dovish or non-interventionist platform. Is Rand Paul a Secret Hawk? Or Maybe Not a Total Dove? |James Kirchick |May 9, 2014 |DAILY BEAST 

Rand Paul will bring his interesting mix of libertarian non-interventionist policy to the dance. GOP’s Strong Field Has No Frontrunner for 2016 |Mark McKinnon |March 10, 2014 |DAILY BEAST 

Then he said that in fact, the danger is not that the United States is these days too aggressively interventionist or imperialist. Obama’s U.N. Speech |Michael Tomasky |September 24, 2013 |DAILY BEAST 

Palmerston was inclined to be "interventionist," but was restrained by his colleagues and the influence of the Queen. Mr. Punch's History of Modern England Vol. II (of IV),--1857-1874 |Charles L. Graves 

One type is market oriented – and the other, interventionist. After the Rain |Sam Vaknin 

In the 'seventies Punch, as we have seen, was decidedly non-interventionist. Mr. Punch's History of Modern England Vol. III of IV |Charles L. Graves 

In their hands the external policy of the Republic, conducted with no lack of skill, was of necessity non-interventionist. History of Holland |George Edmundson 

Using modern terms one may say that charity is “interventionist.” Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 8 |Various