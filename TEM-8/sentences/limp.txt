Sometimes you just need a contraption that will quickly turn limp pieces of bread into crunchy, warm slices of perfection. Nine kitchen gifts that do one thing really well |Sara Chodosh |November 26, 2020 |Popular-Science 

Last week, he was limping noticeably after a hit by the Steelers. AFC South thrives and the Steelers stay unbeaten in NFL Week 11 |Cindy Boren, Mark Maske, Des Bieler |November 23, 2020 |Washington Post 

Let the cabbage sit like this until limp and pliable, 30 minutes. This bo ssam dream is within reach, and it starts with your Instant Pot |Eric Kim |October 28, 2020 |Washington Post 

Except they often have a slight limp on the other side of their body from where their hemisphere was removed. Your Brain Makes You a Different Person Every Day - Issue 91: The Amazing Brain |Steve Paulson |October 14, 2020 |Nautilus 

I’ve plunged several limp inflatable mats into my bathtub in search of streams of tiny bubbles, slapped synthetic patches over the holes, and yet still woken up in the middle of the night on my next trip with a rock jabbing me in the kidney. How to Repair Your Sleeping Pad at Home |Maren Larsen |October 4, 2020 |Outside Online 

He felt his body grow limp (like one of those high-speed films of a flower wilting). Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

Clean-shaven and balding, Saleem is in his forties and walks with a limp. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

To compound it, Rice then treats her limp form with what appears to be cold contempt. Ray Rice Should Have Remembered His 'Kindness' Anti-Bullying Wristband |Michael Daly |September 10, 2014 |DAILY BEAST 

The charts now featured the likes of Limp Bizkit, a rap-metal band whose misogyny was so overt as to be comical. Beyoncé Is Our Indigo Girl: The Halcyon '90s and Feminism's Resurgence in Pop Music |Amanda Marcotte |August 26, 2014 |DAILY BEAST 

One leg of his green pajamas hung limp, empty below the knee. Stanley Booth on the Life and Hard Times of Blues Genius Furry Lewis |Stanley Booth |June 7, 2014 |DAILY BEAST 

His head fell back limp on MacRae's arm, and the rest of the message went with the game old Dutchman across the big divide. Raw Gold |Bertrand W. Sinclair 

She had been walking alone with her arms hanging limp, letting her white skirts trail along the dewy path. The Awakening and Selected Short Stories |Kate Chopin 

Very hot; very limp with the prevalent disease but greatly cheered up by the news of yesterday evening's battle at Helles. Gallipoli Diary, Volume I |Ian Hamilton 

Before we were entirely out of the crush of the city, the engine began to limp and shortly came to a stop. British Highways And Byways From A Motor Car |Thomas D. Murphy 

The rider seemed as weak as the horse, for he swayed in the saddle as he rode, and the bridle reins hung limp in his hands. The Courier of the Ozarks |Byron A. Dunn