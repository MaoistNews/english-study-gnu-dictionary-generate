Dayforward is aiming to modernize the insurance industry by cutting out the middleman and allowing customers to buy directly from the site. ‘Crafting the brand’: How former Huge CEO is pivoting from agency background in service of DTC life insurance startup |Kimeko McCoy |February 3, 2021 |Digiday 

No Google Analytics means no middleman between Ekstra Bladet and data from its site, whether its views or subscription conversions. ‘We had to take full ownership of data’: Why Denmark’s biggest news site cut reliance on Google’s tech |Seb Joseph |January 27, 2021 |Digiday 

Extracting fees from publishers, advertisers, and as the middleman. Google accused of colluding with Facebook and abusing monopoly power in new lawsuit |George Nguyen |December 17, 2020 |Search Engine Land 

In many cases, brands are not getting much ad inventory for their money because an increasingly large portion of the spending is going to middlemen. How A.I. can make digital advertising less creepy |jakemeth |December 17, 2020 |Fortune 

Specializing in sleep products, Coop Home Goods cannot be found in stores but thrives online, cutting out the middleman during sales to better guarantee customer satisfaction. The best pillow: Sleep better in any position |Carsen Joenk |December 11, 2020 |Popular-Science 

A more recent wrinkle is the doctor who prescribes from his own office, cutting out the middleman (read: pharmacist). New Study Says Doctors Can’t “Just Say No” to Their Patients |Kent Sepkowitz |March 31, 2014 |DAILY BEAST 

But it turns out that the manager had received no such recording, due to some middleman botching the exchange. The Real Life of Llewyn Davis |Nicholas Mancusi |November 7, 2013 |DAILY BEAST 

It is a highly efficient, fair, reliable, technologically advanced, and cheap-enough middleman. Meet the Servicey New Dow |Daniel Gross |September 10, 2013 |DAILY BEAST 

Warby Parker's reinvention of the high-priced optical industry relied on one main tactic: cutting out the middleman. Warby Parker Thrives by Giving Away Glasses Whenever It Sells a Pair |Nina Strochlic |May 7, 2013 |DAILY BEAST 

As is the case with all secondary markets, however, the middleman usually takes a piece. What to Do With Unwanted Gift Cards |Nina Strochlic |December 26, 2012 |DAILY BEAST 

If he happens also to be a Captain of Industry, which usually he is not, it is merely one middleman cut out. The Inhumanity of Socialism |Edward F. Adams 

The important thing for you is that he is the middleman on whom you depend for the disease. By the Christmas Fire |Samuel McChord Crothers 

The Law was handed down by a being even inferior to the angels, by a middleman named Moses. Commentary on the Epistle to the Galatians |Martin Luther 

They endeavored to cut off the profits of the middleman by establishing cooperative grocery stores, meat markets, and coal yards. A History of Trade Unionism in the United States |Selig Perlman 

But the manufacturer's emancipation from the middleman need not always lead to trade agreements. A History of Trade Unionism in the United States |Selig Perlman