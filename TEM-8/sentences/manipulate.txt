On the advertising side, Google said it already has policies that prohibit advertisers from using doctored or manipulated media or false claims that could undermine voter participation or trust in the election. How Google, Facebook, and Twitter plan to handle misinformation surrounding 2020 presidential election results |Danielle Abril |September 10, 2020 |Fortune 

This ambiguity leaves it to the state to decide what kind of content is considered false or potentially harmful, and could allow those in power to manipulate the definition for political gain. Brazil’s “fake news” bill won’t solve its misinformation problem |Amy Nordrum |September 10, 2020 |MIT Technology Review 

The court’s ruling was informed, in part, by tens of thousands of alternative maps demonstrating that the district boundaries had very likely been manipulated for political gain, the very definition of gerrymandering. How next-gen computer generated maps detect partisan gerrymandering |Sujata Gupta |September 7, 2020 |Science News 

The Authenticator analyzes videos or images and tells users the percentage chance that they’ve been artificially manipulated. Microsoft’s New Deepfake Detector Puts Reality to the Test |Vanessa Bates Ramirez |September 4, 2020 |Singularity Hub 

After manipulating the positronium atoms with a laser to put them in the appropriate energy level, the team hit them with microwave radiation to induce some of them to jump to another energy level. A measurement of positronium’s energy levels confounds scientists |Emily Conover |August 24, 2020 |Science News 

And from their power structure within the prisons they manipulate and control events on the streets. The Mexican Mafia Is the Daddy of All Street Gangs |Seth Ferranti |December 11, 2014 |DAILY BEAST 

He studied our use of language and the way that words are manipulated to manipulate the populace. Why George Carlin Deserves His Own Street |Kevin Bartini |October 21, 2014 |DAILY BEAST 

They are actually using the media around the galaxy to manipulate events and make people feel they have more power than they do. ‘Star Wars Rebels’ Explores the Jedi’s Lost Years Between the Prequels and the Original Trilogy |Annaliza Savage |August 2, 2014 |DAILY BEAST 

But tech geeks, with their superhuman ability to manipulate ones and zeroes, do. Occupying the Throne: Justine Tunney, Neoreactionaries, and the New 1% |Arthur Chu |August 1, 2014 |DAILY BEAST 

Porn is really the same as any other media product: People manipulate it to their purposes more than they are manipulated by it. Is Bondage Porn to Blame for a Murder? |Amanda Marcotte |July 9, 2014 |DAILY BEAST 

Cleverly though they manipulate, cleanliness is not their besetting weakness. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Of course they have made up their minds you are erratic, and have not the least doubt that they can manipulate that loose screw. Ancestors |Gertrude Atherton 

I don't want to let him manipulate that light enough to send any signals to possible Mexican watchers, Overton. Uncle Sam's Boys as Lieutenants |H. Irving Hancock 

He began to manipulate the cards, lying cramped on his side, and in doing so dropped two or three. Blazed Trail Stories |Stewart Edward White 

They felled the great trees and dragged them aside with powerful donkey engines to manipulate their gear. The Hidden Places |Bertrand W. Sinclair