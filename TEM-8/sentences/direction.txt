Now, barely a week later, Epstein's home in North Hollywood, California, has AT&T fiber service with unlimited data and advertised speeds of 300Mbps in both directions. AT&T scrambles to install fiber for 90-year-old after his viral WSJ ad |Jon Brodkin |February 12, 2021 |Ars Technica 

Light winds pick up just a tad from the northeasterly direction before dawn. D.C.-area forecast: Raw today. Iciness threat grows Saturday into early Sunday. |A. Camden Walker |February 12, 2021 |Washington Post 

Those fans can take today’s hearing as a step, however small, in the direction of Spears’s increased autonomy. Britney Spears’s conservatorship is back in court — and back in the public eye |Ashley Fetters |February 11, 2021 |Washington Post 

In every direction, seawater creeps higher and higher, slopping across the floor of nearby buildings. Aromas can evoke beloved journeys — or voyages not yet taken |Jen Rose Smith |February 11, 2021 |Washington Post 

Either of those words alone point in the direction of filling, comfort-food satisfaction, but together they are practically a guarantee. Chili-tinged sweet potatoes are loaded in all the right ways - flavor, nutrition and satisfaction |Ellie Krieger |February 11, 2021 |Washington Post 

Pleasure shoots magically in every direction like an explosion of sparks. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

Absent a body, no one can say with absolute certainty whether Castro is dead, even if all signs point in that direction. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

An arrow appears indicating the direction you will launch your ball. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

They had hoped the autopsy would show Brinsley had eaten something that would point them in the right direction. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

The ATSB has been impressive in the way it has taken over the direction of the search for Flight 370. Who Will Get AsiaAir 8501’s Black Boxes? |Clive Irving |December 30, 2014 |DAILY BEAST 

A lateen sail was visible in the direction of Cat Island, and others to the south seemed almost motionless in the far distance. The Awakening and Selected Short Stories |Kate Chopin 

After a bit of waiting, Mac decided that the smoke was floating from a certain direction, and we began to edge carefully that way. Raw Gold |Bertrand W. Sinclair 

Even genius, however, needs direction and adjustment to secure the most perfect and reliable results. Expressive Voice Culture |Jessie Eldridge Southwick 

Now many people think that we are moving in the direction of world socialism to-day. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

His superior talents and untiring industry were under the direction of philanthropic and Christian impulses. The Every Day Book of History and Chronology |Joel Munsell