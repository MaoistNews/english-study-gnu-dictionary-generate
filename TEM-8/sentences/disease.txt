When the number drops too low it means an epidemic is likely out of control, since officials can no longer keep up with the demand for testing and see where new pockets of disease are spreading. The new Covid-19 case surge in Europe, explained |Julia Belluz |September 17, 2020 |Vox 

To that end, his lab infected the brains of mice, chimpanzees, and monkeys with the virus that causes the disease. Synthetic biologists have created a slow-growing version of the coronavirus to give as a vaccine |David Rotman |September 16, 2020 |MIT Technology Review 

It found vaccination levels for all diseases globally have dropped to the lowest in 25 years because of pandemic lockdowns and economic destabilization. ‘There’s been some cracks:’ Bill Gates questions FDA’s credibility on a COVID-19 vaccine |Claire Zillman, reporter |September 16, 2020 |Fortune 

According to the commission’s calculations, only 80 or so couples in the US have both partners carry mutations for sickle-cell disease. A CRISPR Baby Future? New Report Outlines Path to Human Germline Editing |Shelly Fan |September 15, 2020 |Singularity Hub 

“It was thought for a long time that this is the only way in which an immune response remembers an infection, by these memory lymphocytes,” said Mihai Netea, a clinician and infectious disease specialist at Radboud University in the Netherlands. ‘Trained Immunity’ Offers Hope in Fight Against Coronavirus |Esther Landhuis |September 14, 2020 |Quanta Magazine 

Without it, they say, the disease would surely kill her within two years. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 

He beat his illness twice, wrote about his battles with the disease, and continued broadcasting even as his health was failing. Remembering ESPN’s Sly, Cocky, and Cool Anchor Stuart Scott |Stereo Williams |January 4, 2015 |DAILY BEAST 

It was a reminder that, as Beyoncé once sang, “Perfection is the disease of a nation,” and her family is hardly flawless. Yoncé Said Knock You Out: The Solange and Jay Z Story |Kevin O’Keeffe |December 29, 2014 |DAILY BEAST 

World peace, religious tolerance, and an end to global poverty, hunger, and disease. Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

Despite the obvious ongoing problems with disease and access to basics, the future of Africa is bright. Silicon Valley Sets Its Sights on Africa |Christian Borys |December 22, 2014 |DAILY BEAST 

In disease, the amount of solids depends mainly upon the activity of metabolism and the ability of the kidneys to excrete. A Manual of Clinical Diagnosis |James Campbell Todd 

Men's lives are as thoroughly blended with each other as the air they breathe: evil spreads as necessarily as disease. Pearls of Thought |Maturin M. Ballou 

As a rule, however, persistent glycosuria is diagnostic of diabetes mellitus, of which disease it is the essential symptom. A Manual of Clinical Diagnosis |James Campbell Todd 

Here they are seldom abundant, but their constant presence is the most reliable urinary sign of the disease. A Manual of Clinical Diagnosis |James Campbell Todd 

The darkness, or rather the general misapprehension, which prevails on this subject, is a frightful source of disease and misery. Glances at Europe |Horace Greeley