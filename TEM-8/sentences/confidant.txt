Some of Giuliani’s closest allies and confidants have not been shy about attacking other conservative leaders for refusing to swoop in during the attorney’s time of need. Rudy Giuliani’s Legal Fund Was a Bust. Now, Its Donation Page Has Disappeared. |Adam Rawnsley, Asawin Suebsaeng |July 8, 2021 |The Daily Beast 

Chris Cuomo has long been a key confidant of his brother, regularly advising him about politics, according to people who know both men. Chris Cuomo took part in strategy calls advising his brother, the New York governor, on how to respond to sexual harassment allegations |Josh Dawsey, Sarah Ellison |May 21, 2021 |Washington Post 

Abdullah’s regime said Hamzah was in cahoots with Bassem Awadallah, a former finance minister and longtime confidant of the king, and Sharif Hassan bin Zaid, another royal family figure, to target “the security and stability of the nation.” Jordan’s royal family feud and alleged coup plot, explained |Alex Ward |April 9, 2021 |Vox 

The Viennese psychoanalyst reported his discovery in a breathless letter to his colleague and confidant, Wilhelm Fliess. A brain researcher on what Freud got right |Jess Keiser |February 26, 2021 |Washington Post 

We have gathered there as hopeful advocates and confidants when “the boss” was elsewhere. Pete Buttigieg as Cabinet secretary is more than a ‘first’ |Charles Francis |December 19, 2020 |Washington Blade 

When a top Mobutu confidant named Colonel Alphonse Bangala purchased the island, Lometcha bought shares. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

A longtime confidant of and lawyer for Lyndon Johnson, Fortas remained a close advisor after joining the Court. Ruth Bader Ginsburg Levels With Us on Why She’s Not Retiring |Jeff Greenfield |September 25, 2014 |DAILY BEAST 

Inside was Mandelbaum, her twenty-four-year-old son Julius, and her most trusted confidant, Herman Stoude. Meet 'The Queen of Thieves' Marm Mandelbaum, New York City's First Mob Boss |J. North Conway |September 7, 2014 |DAILY BEAST 

Otis Moss, Jr., the noted African-American civil rights leader and confidant of Dr. Martin Luther King, Jr., concurred. Religious Leaders Hail Obama’s New Ambassador |Joshua DuBois |July 29, 2014 |DAILY BEAST 

If the President went through with the appointment, he was counting on his confidant to bust open those particular X-Files. President 42 Talks Up Area 51 |Kevin Bleyer |April 6, 2014 |DAILY BEAST 

He was at once banker and confidant of most of the Indians who were getting ahead in agriculture and stock-raising. Mystery Ranch |Arthur Chapman 

When M. George Spero was announced, she felt that an unknown friend, almost a confidant, had arrived. Urania |Camille Flammarion 

The king and his confidant sprang ashore and walked quickly in the direction of the Pre-aux-Clercs. Catherine de' Medici |Honore de Balzac 

She had wanted to make a confidant of her relative, and had decided that nothing could be more unwise. Overland |John William De Forest 

"I was well advised in not making you my confidant sooner, if this is how you take it," cried Miss Chressham angrily. The Rake's Progress |Marjorie Bowen