Rosina, when she saw me cooling, had no such merciful contraption ready. Owning Up to Possession’s Downside |Samantha Harvey |December 14, 2014 |DAILY BEAST 

Why is Putin feeling so merciful all of a sudden to his political enemies? Why Is Putin Pardoning His Political Enemies? |Anna Nemtsova |December 19, 2013 |DAILY BEAST 

The death carried an all-the-more-powerful message because Julius Caesar was famously merciful to defeated enemies. Who Was the Real Cato? |David Frum |December 20, 2012 |DAILY BEAST 

All Muslims assert that God is most merciful, most compassionate. My Meetings With the Man Behind the Mosque |Brad Gooch |August 19, 2010 |DAILY BEAST 

I'd been so irrationally certain of merciful miracles; he was supposed to live forever. My Martha's Vineyard Close Encounter |Patricia J. Williams |August 29, 2009 |DAILY BEAST 

But with no Devil the belief in a merciful and loving Heavenly Father becomes impossible. God and my Neighbour |Robert Blatchford 

But this theory of a merciful, and loving Heavenly Father is vital to the Christian religion. God and my Neighbour |Robert Blatchford 

He longed for death with a full and yearning desire, and he could kiss the hand that would be merciful and give the fatal blow. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Her face was grey as the one from which she drew the merciful coverings, but her eyes went fearlessly to that which she sought. Uncanny Tales |Various 

"It has been a most merciful escape," Maloney said, his pulpit voice struggling with his emotion. Three More John Silence Stories |Algernon Blackwood