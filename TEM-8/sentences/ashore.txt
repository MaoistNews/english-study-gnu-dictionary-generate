With the Lombardi Trophy safely ashore, they crowded onto a stage to dance. Tampa Bay Buccaneers celebrate Super Bowl LV victory in boat parade |Cindy Boren, Glynn A. Hill |February 10, 2021 |Washington Post 

Wind is easy to bring ashore to the 8 million people in the Big Apple. At New York City’s biggest power plant, a switch to clean energy will help a neighborhood breathe easier |Andrew Blum |February 2, 2021 |Popular-Science 

The crew of the Donaldson came ashore expecting to find the five explorers. The Woman Who Survived Two Years Alone in the Arctic |Fiona Zublin |December 9, 2020 |Ozy 

Galloway is trailing, but if there’s a blue wave coming, she could be washed ashore as a surprise winner. Political Tremors: How to Survive Election Day Drama |Nick Fouriezos |November 1, 2020 |Ozy 

Researchers say that the true death toll is likely far larger since most whales struck by ships sink to the bottom of the ocean rather than wash ashore. Whale ‘roadkill’ is on the rise off California. A new detection system could help. |Erik Olsen |September 29, 2020 |Popular-Science 

On June 6, 1944, the greatest amphibious force ever assembled began to fight its way ashore. The Deadly Trap Behind D-Day’s Beaches |Clive Irving |June 5, 2014 |DAILY BEAST 

"U.S.," he said next, pointing to where we stood, shaking his head to indicate that he wouldn't step ashore. Big-Sky West Texas: A Road Trip Through Hidden America |Condé Nast Traveler |March 18, 2014 |DAILY BEAST 

My novel starts in New Salem, as Lincoln is washed ashore as a young man who has yet to define himself. Making Lincoln Sexy: Jerome Charyn’s Fictional President |Tom LeClair |March 6, 2014 |DAILY BEAST 

Here he is describing the state of the body when it is hauled ashore: “Its humanity had been lost to the ravages of nature.” This Week’s Hot Reads: March 3, 2014 |Nicholas Mancusi |March 3, 2014 |DAILY BEAST 

Edward had crossed the Channel to put an army ashore in Flanders. The Day the Sea Ran Red: The Battle of Sluys |Dan Jones |May 6, 2013 |DAILY BEAST 

This vessel, loaded with supplies, went ashore and was lost; and one hundred and twenty Japanese and three Dutchmen were drowned. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

He has been ashore at Kum Kale and reports violent fighting and, for the time being, victory. Gallipoli Diary, Volume I |Ian Hamilton 

A few moments afterward he was seen dragging his own trunk ashore, while Mr. Hitchcock finished his story on the boiler deck. The Book of Anecdotes and Budget of Fun; |Various 

I was right, so it seems, about getting ashore before the enemy could see to shoot out to sea. Gallipoli Diary, Volume I |Ian Hamilton 

There were machine guns here which wiped out the landing parties whenever they tried to get ashore North of the present line. Gallipoli Diary, Volume I |Ian Hamilton