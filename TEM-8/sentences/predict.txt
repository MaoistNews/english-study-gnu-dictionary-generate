Though the veteran politician is still predicted to win reelection in the deep-red state, Harrison poses a serious threat to Graham’s campaign. Jaime Harrison Says He Feels ‘A Little Sad’ For Lindsey Graham |Hope Wright |September 17, 2020 |Essence.com 

Centers for Disease Control and Prevention Director Robert Redfield predicted Wednesday that most of the American public will not have access to a vaccine against the novel coronavirus until late spring or summer of next year. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

With the mechanism behind mantling unmasked, a third partner—Orion Genomics, a private startup founded by Martienssen—was able to develop a simple DNA test that predicts whether a designer seedling will bear robust or withered fruit. The Environmental Headache in Your Shampoo - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |September 16, 2020 |Nautilus 

Typically, water utilities set water prices a year in advance by trying to predict how much people will need. Environment Report: State Throws Cold Water on Pricing Scheme |MacKenzie Elmer |September 14, 2020 |Voice of San Diego 

Another thankless task for brands is predicting where people are going to be spending their days. Deep Dive: How the Summer of 2020 forced brand marketing to change for the better |jim cooper |September 14, 2020 |Digiday 

But so-called jungle primaries are notoriously hard to predict or poll. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

A successful trend-maker might be able to steer a conversation, but virality remains extremely difficult to predict. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

They predict the government of President Petro Poroshenko may not last another three months. Ukraine Militias Warn of Anti-Kiev Coup |Jamie Dettmer |November 28, 2014 |DAILY BEAST 

Experts [predict] that over a million people in the region need food aid to allay shortages. Liberia’s Ebola Famine |Abby Haglage, Nina Strochlic |November 13, 2014 |DAILY BEAST 

Now, several reports predict the coming months could be devastating. Liberia’s Ebola Famine |Abby Haglage, Nina Strochlic |November 13, 2014 |DAILY BEAST 

I predict that, in one month from the date of this letter, there will not be an Austrian or Prussian cartridge found in France. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

I found it on all occasions extremely sensible, and very often to predict a change of wind much sooner than the barometer. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Spain is a country of surprises and of contradictions; even her own people seem unable to predict what may happen on the morrow. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

If everything came out it did not require any great effort of prophecy to predict what the result would be. The Gold Bat |P. G. Wodehouse 

I venture to predict in advance, that the degree of success is mainly within their control. Thoughts on Educational Topics and Institutions |George S. Boutwell