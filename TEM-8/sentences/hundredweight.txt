We found, however, no difficulty in procuring more, some of which weighed four hundredweight. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Spitzhase felt as if the pipe were about a hundredweight heavier, and that it had grown suddenly stiff. Black Diamonds |Mr Jkai 

Yes, full a hundredweight; half the mass was quartz, but four-fifths of the weight they knew must be gold. It Is Never Too Late to Mend |Charles Reade 

He was powerfully developed physically, and at eighteen could lift ten hundredweight. The Cornwall Coast |Arthur L. Salmon 

Each chest, therefore, if all contained the same precious metal, would represent the value of sixteen hundredweight of silver. The Cruise of the "Esmeralda" |Harry Collingwood