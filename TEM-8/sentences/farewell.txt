The result was not exactly an offensive masterpiece putting the rest of the NFC on notice that the Saints are poised for a Super Bowl run in what could be Brees’s farewell postseason. Saints advance to set up a Drew Brees-Tom Brady showdown in second round |Mark Maske |January 11, 2021 |Washington Post 

In our farewell, my boss said he was sending a gift to me to recognize both. Miss Manners: How to ask indirectly about a missing gift |Judith Martin, Nicholas Martin, Jacobina Martin |January 7, 2021 |Washington Post 

The cartoonist bid farewell knowing his strip was at its aesthetic pinnacle. ‘Calvin and Hobbes’ said goodbye 25 years ago. Here’s why Bill Watterson’s masterwork enchants us still. |Michael Cavna |December 31, 2020 |Washington Post 

Three days before flying with his mother and manager to his new life in Washington, the Wizards’ 19-year-old first-round draft pick is bidding farewell to the floor that made him famous by draining one three-pointer after another. Deni Avdija is ‘the greatest talent in Israeli basketball history.’ But is he ready for the NBA? |Steve Hendrix |November 30, 2020 |Washington Post 

All of the Core Four were still producing for the Yankees into the 2010s, years after Williams had played his last game in pinstripes, which kept him from participating in the farewell narrative with the rest of the dynasty’s cornerstones. Bernie Williams Deserves More Credit For Making The Yankees A Dynasty |Neil Paine (neil.paine@fivethirtyeight.com) |July 14, 2020 |FiveThirtyEight 

There was something cathartic about deleting this 2,500-word monster of a farewell, and resolving to live. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

Get ready to bid farewell to the spitfire Bobby Bottleservice and big booty player Ref Jeff. The Zany Shades of Nick Kroll |Abby Haglage |December 15, 2014 |DAILY BEAST 

“If you see their car, you know some poor soul is about to die,” Noor says, at his farewell party. Obama’s Deadly Informants: The Drone Spotters of Pakistan |Umar Farooq, Syed Fakhar Kakakhel |November 12, 2014 |DAILY BEAST 

A Chinese restaurateur ran to the window as I wolfed down roast duck for my farewell dinner, as shots were heard across the way. No Movie Stars, No Red Carpet, But Off-Season Cannes Is Still Magic |Liza Foreman |September 15, 2014 |DAILY BEAST 

Will voters shower Breaking Bad with farewell trophies, or is the enthusiasm for True Detective impossible to beat? What's TV's Best Drama? 'True Detective,' 'Game of Thrones,' and Our Emmy Predictions |Kevin Fallon |August 22, 2014 |DAILY BEAST 

I often recall the farewell lunch we had together at the Restaurant de Paris, in the Escolta. The Philippine Islands |John Foreman 

Uncle David nodded, and waved his hand, as on entering the door he gave them a farewell smile over his shoulder. Checkmate |Joseph Sheridan Le Fanu 

They were just about to celebrate tabagie, or a solemn feast, over his last farewell. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

The farewell and the mourning are finished by the slaughter of dogs, that the dying man may have forerunners in the other world. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

In the evening I received General d'Amade who had come over to pay his farewell visit. Gallipoli Diary, Volume I |Ian Hamilton