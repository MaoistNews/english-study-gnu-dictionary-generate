The play ultimately failed, but the Chiefs managed only one first down on the ensuing series, and Tommy Townsend shanked the punt for just 29 yards. Yes, Tom Brady Is The GOAT. But He Didn’t Do It Alone. |Josh Hermsmeyer |February 8, 2021 |FiveThirtyEight 

Unlike their opening possession, the Bucs were able to get a first down, when running back Ronald Jones rumbled for 13 yards, but an ensuing end-around by wide receiver Scotty Miller lost three yards. Super Bowl highlights: Bucs celebrate championship, Tom Brady wins MVP |Des Bieler, Mark Maske, Chuck Culpepper |February 8, 2021 |Washington Post 

Jeremiah Robinson-Earl hit a pair of free throws, and while Georgetown was able to force a missed three-pointer on the ensuing extra possession, Villanova’s Collin Gillespie secured the offensive rebound and was fouled. Georgetown continues to play well but falls short in the end against Villanova |Kareem Copeland |February 7, 2021 |Washington Post 

Success ensues and he becomes a notable virus hunter, does Ted Talks, receives grants and publishes papers and, in 2011, his bestselling book, “The Viral Storm.” ‘Catastrophist’ offers lessons from a pre-COVID plague |Patrick Folliard |February 5, 2021 |Washington Blade 

Aluma had a layup on the ensuing inbound play but missed, and the Panthers got Au’Diese Toney’s layup and three-pointer for a 68-55 lead. Pitt pulls away from Virginia Tech in second half |Gene Wang |February 4, 2021 |Washington Post 

She wants a “hagiography,” and the conflicts and confusions that ensue provide The Last Word with its comic momentum. A Novel About a Novelist ‘Like’ Naipaul |Edward Platt |November 6, 2014 |DAILY BEAST 

There would be no end to the chaos that would ensue, making the country virtually ungovernable. Religious Freedom, or a License to Discriminate? |Gene Robinson |March 23, 2014 |DAILY BEAST 

That was six years before the so-called biography law, but the row over Garrincha set the tone for the battles that would ensue. Brazil’s Rich Ban Biographies Via Arcane Law |Mac Margolis |November 21, 2013 |DAILY BEAST 

These were the sorts of measures that the other negotiators expected to ensue at future stages of the normalization process. Why France Is to Blame for Blocking the Iran Nuclear Agreement |Christopher Dickey |November 10, 2013 |DAILY BEAST 

Erdogan could be either a spoiler or catalyst if significant Israeli-Palestinian negotiations ensue. Obama's Turkish Coup In Israel |Sam Shube |March 25, 2013 |DAILY BEAST 

If there is everywhere complete economic freedom, then there will ensue in consequence a régime of social justice. The Unsolved Riddle of Social Justice |Stephen Leacock 

In their view, if only this were accomplished blessings innumerable would ensue and all complaints would for ever cease. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

These symptoms may be present in a variety of degrees, and in advanced cases even imbecility or paralysis may ensue. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

If she should ask her mother, a string of questions would ensue, with "No" for a snapper. A Forest Hearth: A Romance of Indiana in the Thirties |Charles Major 

It is obvious that between the ascetics of the monastery and the Classicists direct friction must ensue. The History of Modern Painting, Volume 1 (of 4) |Richard Muther