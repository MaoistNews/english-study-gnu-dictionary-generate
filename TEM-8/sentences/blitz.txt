The application arrives as public health experts gird for a blitz of coronavirus cases seeded by holiday travels and gatherings — a surge coming so soon that no vaccine can blunt it. Moderna to ask the FDA to greenlight its coronavirus vaccine |Carolyn Y. Johnson |November 30, 2020 |Washington Post 

Washington’s defense gave its offense one more chance after a third-down sack by rookie safety Kam Curl on a blitz. Washington loses Kyle Allen to injury, and Alex Smith’s comeback bid vs. Giants falls short |Nicki Jhabvala |November 8, 2020 |Washington Post 

Technology platforms have released a blitz of new rules to curb misinformation after mounting momentum for movements such as QAnon. YouTube follows Facebook in banning QAnon, but with caveats |Verne Kopytoff |October 15, 2020 |Fortune 

Epic has engaged in a full-scale, pre-planned media blitz surrounding its decision to breach its agreement with Apple, creating ad campaigns around the effort that continue to this day. Apple says Epic is acting as ‘a saboteur, not a martyr’ in app store challenge |radmarya |September 16, 2020 |Fortune 

The center snapped the ball and the Bulldogs sent a blitz, rushing Tagovailoa with two extra players, forcing him to throw the ball out of bounds. NFL Decision to Stick to Draft Date May Penalize Some ‘Diamonds in the Rough’ |Shirley Carswell |April 23, 2020 |TruthBeTold.news 

It reminds me of an uncle of mine who said the London Blitz was irritating. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

That fall, soon after the German blitz on London began, Kennedy headed back to the U.S. Blood and War: The Hard Truth About ‘Boots on the Ground’ |Clive Irving |September 22, 2014 |DAILY BEAST 

I collected bits of them, but my blitz was safely vicarious. Life Under Air Strikes: Children Under Fire Will Never Forget — or Forgive |Clive Irving |August 3, 2014 |DAILY BEAST 

Now the time for the bombing blitz and commando raids appears to be approaching. The ISIS Caliphate’s Coming Blitz of Baghdad |Jamie Dettmer |July 28, 2014 |DAILY BEAST 

In one ad blitz, former employees at a closed steel mill call Romney and Bain job destroyers and economic vampires. Jeb Bush’s Risky Business |Patricia Murphy |July 24, 2014 |DAILY BEAST 

Dollmann introduced us, calling him Commander von Brning, in command of the torpedo gunboat Blitz. The Riddle of the Sands |Erskine Childers 

His news was that the Blitz's steam-cutter had come in on the morning tide, and he had met von Brning when marketing at the inn. The Riddle of the Sands |Erskine Childers 

The hull of the Blitz loomed up, and a minute later our kedge was splashing overboard and the launch was backing alongside. The Riddle of the Sands |Erskine Childers 

Then I understood—only men-of-war sound bugles—the Blitz was here then; and very natural, too, I thought, and strode on. The Riddle of the Sands |Erskine Childers 

I shall be going back to the Blitz on the evening tide, but you'll be busy then with your own boat.' The Riddle of the Sands |Erskine Childers