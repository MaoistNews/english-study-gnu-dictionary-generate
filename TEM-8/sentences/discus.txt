If one of the other guests had opened their door that afternoon while I was running by, I would have been sent flying like a discus off the ceiling. Sleepless Nights, Hotel Room Sprints, So Much 7-Eleven: What It's Like to Cover the Tokyo Olympics |Sean Gregory/Tokyo |August 7, 2021 |Time 

Press won gold in the shot put and silver in the discus at the 1960 Rome Olympics, and gold in both events at the 1964 Tokyo Games. Tamara Press, Soviet Olympic champion whose gender was questioned, dies at 83 |News Services |May 7, 2021 |Washington Post 

So far as I know, no one has ever done this to an Olympic discus thrower. Why Americans Should Love the World Cup |Sean Wilsey |June 12, 2014 |DAILY BEAST 

Jeremy Hunt has introduced a new sport to the Games, to go with the discus, shot put, javelin. 20 Reasons to Feel Good About the 2012 Olympics in London |The Telegraph |July 30, 2012 |DAILY BEAST 

The Apollo and the Discobolus are engaged in the same purpose—the one watching the effect of his arrow, the other of his discus. Blackwood's Edinburgh Magazine, No. CCCXXVIII. February, 1843. Vol. LIII. |Various 

Who shall meet the helméd Arjun in the gory field of war, Krishna with his fiery discus mounted on his battle-car? Maha-bharata |Anonymous 

It was not till the discus throw that the Scout team suffered an overwhelming reverse. The Boy Scouts of Lakeville High |Leslie W. Quirk 

And after the town of Saubha had fallen, the discus came back into my hands. Mahabharata of Krishna-Dwaipayana Vyasa Bk. 3 Pt. 1 |Krishna-Dwaipayana Vyasa 

The discus then cleft Salwa in twain who in that fierce conflict was at the point of hurling a heavy mace. Mahabharata of Krishna-Dwaipayana Vyasa Bk. 3 Pt. 1 |Krishna-Dwaipayana Vyasa