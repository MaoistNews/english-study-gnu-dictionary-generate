When Shoemaker was 18, she saw her peers make games within a few weeks that went viral and became financially lucrative through Roblox. Roblox, the game company made wildly popular by kids, goes public with $41 billion valuation |Shannon Liao |March 11, 2021 |Washington Post 

Last weekend, models Sarah Ziff and Alice Shoemaker addressed their experience with Richardson on HuffPost Live. Anonymous Model Goes Public with Allegations Against Terry Richardson |Justin Jones |March 12, 2014 |DAILY BEAST 

Shoemaker spent much of the past week trying to dissuade other reporters from printing the comment. Senator to Students: I Don't Give a 'Tick's Shit' |Lloyd Grove |September 6, 2010 |DAILY BEAST 

At Shoemaker High School, for instance, 80 percent of the students have at least one parent in the military. Fort Hood's Bleak World |Mimi Swartz |November 5, 2009 |DAILY BEAST 

He was the son of a shoemaker, and sometime engaged in the same business himself. The Every Day Book of History and Chronology |Joel Munsell 

Thence by coach to my shoemaker's and paid all there, and gave something to the boys' box against Christmas. Diary of Samuel Pepys, Complete |Samuel Pepys 

And if such sayings got abroad, they would not be soothing to the feelings of a respectable shoemaker, would they now? A Charming Fellow, Volume II (of 3) |Frances Eleanor Trollope 

By-and-by I beat the shoemaker on metres and the son in the back yard, and then I left 'em, for they was no more use to me. The Belted Seas |Arthur Colton 

There was a little knot of Wesleyans assembled in the house of Mr. Gladwish, the shoemaker. A Charming Fellow, Volume II (of 3) |Frances Eleanor Trollope