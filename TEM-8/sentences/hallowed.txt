The celebrated 31-year-old American ultrarunner is known the world over and loves the camaraderie he feels among friends and fellow runners when he’s in this hallowed mountain village. Jim Walmsley’s Plan for UTMB? A Little Pre-Race R&R. |jversteegh |August 26, 2021 |Outside Online 

Such drops are needed because the hallowed grid-parity goal is misleading—the real question is at what point utilities will actually abandon existing coal plants and switch to solar, rather than merely avoid adding new coal capacity. Cheaper solar PV is key to addressing climate change |Gernot Wagner |June 30, 2021 |MIT Technology Review 

Before the 2019 Remembrance Ceremony, Governor DeSantis stood on hallowed ground, steps from where I escaped the building in 2016, and promised me that he would always support those of us impacted by the Pulse nightclub shooting. Pride Month Florida Day 2-DeSantis yanks LGBTQ funding |Brody Levesque |June 2, 2021 |Washington Blade 

It’s also playing out in the hallowed halls of global diplomacy. How George Floyd’s Murder Sparked Change |Toyloy Brown III |May 26, 2021 |Ozy 

The father and son promised one another they would be there to watch if the other reached that hallowed final weekend of the NCAA tournament. When Houston and Baylor meet in the Final Four, it will be father vs. son on the sidelines |Emily Giambalvo |April 1, 2021 |Washington Post 

The largest mass grave in America existed uneasily as both hallowed ground and deconstruction site. The Resilient City: New York After 9/11 |John Avlon |September 11, 2014 |DAILY BEAST 

That we find so hallowed and important, but also so asinine and silly? Breaking: The Oscars Might Not Suck This Year |Kevin Fallon |February 27, 2014 |DAILY BEAST 

The director is following his Wolf of Wall Street success with a documentary about the hallowed New York publication. Justin Bieber Faces Deportation; NBC Moves Emmy Awards to Monday |Culture Team |January 29, 2014 |DAILY BEAST 

And what could be more honorable than rounding up your besties to pay homage to a hallowed pop deity? Miley Cyrus, Walter White, Oprah: Your Pop Culture Halloween Costume Guide |Kevin Fallon |October 21, 2013 |DAILY BEAST 

Jackson is more interested in the wide-open early days of the movement than in its hallowed end. Best Year Ever: How 1922 Birthed Modernism |Mark Braude |September 14, 2013 |DAILY BEAST 

Next morning that glorious garrison quitted the shot-torn plain they had hallowed by their deeds. The Red Year |Louis Tracy 

Thus therefore shall you pray: Our Father who art in heaven, hallowed be thy name. The Bible, Douay-Rheims Version |Various 

In raising the lowest classes he will have hallowed a principle unknown before his time. Balsamo, The Magician |Alexander Dumas 

There were some at all events, to whom Rome, hallowed by a great ideal and noble rule, had become as the city of God. The Life of Mazzini |Bolton King 

And as we stand upon this hallowed ground, let us bury all animosities engendered by the war. Reminiscences of the Guilford Grays, Co. B., 27th N.C. Regiment |John A. Sloan