When they finally meet up in the elevator, they look like they can’t believe it themselves, and you feel almost stupid with relief. What floor? |Sophia Nguyen |February 12, 2021 |Washington Post 

Vance saw a pre-screening of the film and expressed relief that people wouldn’t see those scenes. The very real, very painful reasons the autistic community demanded two restraint scenes be removed from Sia’s new film ‘Music’ |Theresa Vargas |February 10, 2021 |Washington Post 

If you’re an athlete, you can use them for additional relief on sore muscles. Best hand warmers: Block the chill during your favorite winter activities |PopSci Commerce Team |February 10, 2021 |Popular-Science 

As arguments about the size and scope of covid relief demonstrate, political debate alone does not pose an existential threat to the Republic. Want Unity For Real? Then America Needs to Get Back to Facts |Samar Ali, Bill Haslam and Jon Meacham |February 8, 2021 |Time 

Both executives expressed relief that the merger with Outbrain fell through last year. Publishers worry Taboola’s SPAC funding could make them more dependent on its ad revenue |Sara Guaglione |February 8, 2021 |Digiday 

In January, an appeal hearing will determine whether he qualifies for post-conviction relief. The Deal With Serial’s Jay? He’s Pissed Off, Mucks Up Our Timeline |Emily Shire |December 31, 2014 |DAILY BEAST 

And, every election, we have our quasi-comic-relief candidates, your Al Haigs and Gary Bauers and Bill Richardsons. Be the Smarter Bush Brother, Jeb: Don’t Run! |Michael Tomasky |December 17, 2014 |DAILY BEAST 

Take responsibility for an endless stream of people, even as our own suffer, and struggle to get policy relief from Washington. The Progressive Case Against Birthright Citizenship |Keli Goff |December 15, 2014 |DAILY BEAST 

Adnan has an appeal hearing for post-conviction relief set for January. The Scoop on ‘Serial’: Making Sense of The Nisha Call, Asia's Letters, and Our Obsession |Emily Shire |December 11, 2014 |DAILY BEAST 

Rob Marshall lets a sigh of relief erupt so loud it could be heard by giants in the sky. Rob Marshall Defends ‘Into the Woods’ |Kevin Fallon |December 9, 2014 |DAILY BEAST 

It was with a feeling of relief on both sides that the arrival of Mr. Haggard, of the Home Office, was announced. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Several suggestions for the relief of the country bank have come to their notice. Readings in Money and Banking |Chester Arthur Phillips 

But she kept the same tone, and its tender archness only gave a greater sweetness to his sense of relief. Confidence |Henry James 

And now everybody turned out with a feeling of intense relief to witness the rejoicings on the village green. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

So with its completion, he wrapped it carefully, and sent it to a Chicago publisher, while he sighed with relief. The Homesteader |Oscar Micheaux