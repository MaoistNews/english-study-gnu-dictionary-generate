The marine ecologist at Cornell University is listening to a humpback whale song, her fingers bobbing like a conductor’s to each otherworldly croak and whine. ‘Fathom’ seeks to unravel humpback whales’ soulful songs |Jake Buehler |June 22, 2021 |Science News 

This dip in sensitivity falls just between the two most prominent frequencies of a male green tree frog’s croak, suggesting that inflated lungs don’t affect a female’s ability to hear her own species. Female green tree frogs have noise-canceling lungs that help them hear mates |Jonathan Lambert |March 4, 2021 |Science News 

When Tony takes over Danny's body, his voice begins to croak and he shakes his pointer finger for emphasis. Imaginary Friends in Movies |Marlow Stern |May 5, 2011 |DAILY BEAST 

And then I sit on a bench facing the grave and a raven says something in a croak a few steps from me. Robert Bolaño's European Adventures |Roberto Bolano |April 28, 2011 |DAILY BEAST 

But dey can't hang me, no sah, dey can't, 'cause mah man croak two weeks later. Prison Memoirs of an Anarchist |Alexander Berkman 

He will awake to drive away the crows which croak around the mountain. The Story Of The Duchess Of Cicogne And Of Monsieur De Boulingrin |Anatole France 

By my sword, the Sweetheart of the Faith, never did frogs at a mid-summer drought croak more frightfully than those scamps. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 

Suppose we'd croak this man in th' hot par-rt av th' p'litical fight; what happens? The Wreckers |Francis Lynde 

From a small almost black lake rose, like a mysterious wail, the plaintive croak of tiny frogs. Dream Tales and Prose Poems |Ivan Turgenev