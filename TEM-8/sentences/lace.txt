You remember that doll’s dress as green instead of blue, because when you were that age your mother had a green dress with the same kind of lace collar as the doll’s. Dark spaces on the map |Katie McLean |December 18, 2020 |MIT Technology Review 

So in response, we meticulously designed the Clove sneaker with fluid-repellent laces and uppers made from liquid-repellent and stain-resistant Clarino fabric, which can be cleaned easily with the same antibacterial wipes used in the hospital. This new sneaker brand is meant for health care workers |Rachel King |December 6, 2020 |Fortune 

They also have a sneaker that has no laces which I love when I really just need to slip something on. UGG Taps Zuri Marley For Ready-To-Wear Campaign |Nandi Howard |November 23, 2020 |Essence.com 

The speed laces cinch tight for a secure fit and don’t flap around when I’m swimming, which is a problem with some similar models. The Gear That Lets Me Enjoy the Last Days of Summer |Graham Averill |September 15, 2020 |Outside Online 

Parents have declared unequivocally that they want more than higher test scores and yet all funds to public schools are tied to test scores like laces on shoes. Our Public Schools Have a Customer Service Problem |Thomas Courtney |September 10, 2020 |Voice of San Diego 

They lifted her up, and when they saw that she was laced too tightly, they cut the stay lace in two. In New Brothers Grimm 'Snow White', The Prince Doesn't Save Her |The Brothers Grimm |November 30, 2014 |DAILY BEAST 

More items came in time, as funding was available, like a slit-and-lace-up jacket and a line of boxer underwear. Look Who’s Wearing The Pants: Haute Butch’s Gender-Blending Style |Nina Strochlic |October 24, 2014 |DAILY BEAST 

And no amount of fancy lace could ever have competed with the joy of sleep. Dita Von Teese, Keep Your Hands Off Our Boobs |Emma Mahony |August 6, 2014 |DAILY BEAST 

One 1918 example, in wispy silk chiffon and lace is even trimmed in mink! What Lies Beneath: How Lingerie Got Sexy |Raquel Laneri |June 5, 2014 |DAILY BEAST 

In the Forum Club, there is taffeta and lace, leather and gold. Shaq, Year One |Charles P. Pierce |May 24, 2014 |DAILY BEAST 

On his head was the second-hand hat of some parvenu's coachman, gold lace, cockade and all. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

C was a Captain, all covered with lace; D was a drunkard, and had a red face. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

Ramona had covered the box with white cloth, and the lace altar-cloth thrown over it fell in folds to the floor. Ramona |Helen Hunt Jackson 

She rose impatiently and bathed her eyes before ringing for the maid to lace her for dinner—it was long past tea-time. Ancestors |Gertrude Atherton 

She still wore the shabby lace and the artificial bunch of violets on the side of her head. The Awakening and Selected Short Stories |Kate Chopin