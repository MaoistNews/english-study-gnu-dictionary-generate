There is even a link between the malady of negative partisanship and America’s ongoing struggle with vaccine hesitancy. Why Republicans Are So Determined to Distort the Truth About the Capitol Attack |David French |July 29, 2021 |Time 

Doctors have reported an increase in all sorts of foot maladies — in part because so many of us have been spending so much time barefoot. These Shoes Are Killing Me! (Ep. 296 Rebroadcast) |Stephen J. Dubner |May 20, 2021 |Freakonomics 

In Europe, doctors prescribed sour milk to treat intestinal maladies. The Man Who Drank Cholera and Launched the Yogurt Craze - Issue 100: Outsiders |Lina Zeldovich |May 19, 2021 |Nautilus 

In a Politico op-ed adapted from the book last week, Boehner describes how birtherism and other maladies infected his party, and in audio leaked from his audiobook recording sessions he directs vulgar insults at Cruz. In new book, John Boehner says today’s GOP is unrecognizable to traditional conservatives and dishes on his time in politics |Paul Kane, Colby Itkowitz, Aaron Blake |April 9, 2021 |Washington Post 

That formula is approved for tropical maladies and requires a prescription. Supporters tout anti-parasite drug as covid-19 treatment, but skeptics call it the ‘new hydroxychloroquine’ |Laurie McGinley |April 8, 2021 |Washington Post 

But as one deadly malady loses steam, another may be exploding: hunger. Liberia’s Ebola Famine |Abby Haglage, Nina Strochlic |November 13, 2014 |DAILY BEAST 

Some places (like Syria) are prone to this malady, and others (like America) are not. Who Shrunk America? |Tunku Varadarajan |September 5, 2013 |DAILY BEAST 

Nobody would hold up the single outpost of Moo Cluck Moo as a cure for the malady of low wages. A Fast-Food Joint Thrives, Even by Paying $12 an Hour |Daniel Gross |August 2, 2013 |DAILY BEAST 

Cystic Fibrosis, a genetic malady, dooms its sufferers to a short and burdened life. NHS Turns Down Free Lifesaving Drug |Megan McArdle |October 31, 2012 |DAILY BEAST 

But the burnout described in this and so many other articles is not really a malady. Study Says Doctors More Burned Out Than Others, But It’s Not Really a Malady |Kent Sepkowitz |August 23, 2012 |DAILY BEAST 

Rheumatism was the one malady that sometimes affected mother Martha's health. Dorothy at Skyrie |Evelyn Raymond 

In all of these the clinical facts, as well as the progress of the malady, were carefully studied and recorded. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

He as well as Brisset was called in consultation regarding a very serious malady afflicting Raphael de Valentin. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

All this time, however, his malady was gaining ground; he slept badly, and his appetite failed him. Skipper Worse |Alexander Lange Kielland 

"But I do not observe any symptoms of that malady developing themselves at present," added the doctor. Elster's Folly |Mrs. Henry Wood