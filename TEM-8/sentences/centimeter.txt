The nurses were all coming in because they would not believe how calm and relaxed I was, and the centimeters were just going up. Marie Mongan, champion of hypnobirthing, dies at 86 |Olesia Plokhii |February 11, 2021 |Washington Post 

Moving at rates between 2 and 10 centimeters each year, some plates collide, some diverge and some grind past one another. How the Earth-shaking theory of plate tectonics was born |Carolyn Gramling |January 13, 2021 |Science News 

A practical item with many different uses, this ruler offers both inches and centimeters and comes in a variety of sizes from 12″ to 36″ long. The best rulers for home, office, and school |PopSci Commerce Team |January 8, 2021 |Popular-Science 

As astonishing as it may seem, many experimental AR cloud concepts, even entirely mapped cities, are location specific down to the centimeter. How a Software Map of the Entire Planet Could Change the World Forever |Aaron Frank |December 27, 2020 |Singularity Hub 

It seems when OSIRIS-REx touched down on Bennu’s surface, the collection head went 24 to 48 centimeters deep, which would explain how it recovered so much material. OSIRIS-REx collected too much asteroid material and now some is floating away |Neel Patel |October 23, 2020 |MIT Technology Review 

The daughter had also been exposed and was comatose before she and her mother each received A 250 cubic centimeter transfusion. Infected Ebola Doctor Kent Brantly Is an Endangered Hero |Michael Daly |August 3, 2014 |DAILY BEAST 

She grew increasingly ill despite a 450 cubic centimeter transfusion and became comatose after five days. Infected Ebola Doctor Kent Brantly Is an Endangered Hero |Michael Daly |August 3, 2014 |DAILY BEAST 

True, this wasn't an outpost set up by Israeli settlers to prevent a centimeter of retreat from the Whole Land. Bibi Grows a Backbone |Gershom Gorenberg |January 14, 2013 |DAILY BEAST 

So small and so numerous are these corpuscles that over five million are found in a cubic centimeter of normal blood. A Civic Biology |George William Hunter 

One barn is equal to 10-24 square centimeter, which is approximately the cross-sectional area of a typical atomic nucleus. The Atomic Fingerprint |Bernard Keisch 

With the highest known magnifying power we could distinguish the forty-thousandth part of a centimeter. Scientific American Supplement, No. 613, October 1, 1887 |Various 

As the angle of the wedge is about one tenth, the intervals between these divisions are about one centimeter. Scientific American Supplement, No. 363, December 16, 1882 |Various 

Having done this, bring the goblet to within about a centimeter of the pipe stem. Scientific American Supplement, No. 363, December 16, 1882 |Various