Donald Rabin had scoured train after train in Chicago for four hours. ‘FLUTE EMERGENCY’: A musician forgot his $22,000 instrument on a Chicago train. A homeless man found it. |Andrea Salcedo |February 5, 2021 |Washington Post 

She scoured literature, art, philosophy and psychology to describe how society often attaches a moral dimension to illness. The virus caused more than a pandemic. It set us all ablaze. |Philip Kennicott |February 5, 2021 |Washington Post 

Families have spent hours on the hotlines, scoured appointments on platforms like the ticket sales site Eventbrite, and posted desperately on local news sites like Patch and Nextdoor. People are fed up with broken vaccine appointment tools — so they’re building their own |Tanya Basu |February 1, 2021 |MIT Technology Review 

That said, the model still predicts some snow in the storm’s initial phase early Sunday before the cold air is scoured out and precipitation changes to a wintry mix and rain, not unlike the storm system Monday night. Washington receives first measurable snow in 372 days, with another threat looming Sunday |Jason Samenow, Wes Junker |January 26, 2021 |Washington Post 

Lately, though, “Polly thought her mind was a river, constantly scouring and pooling, constantly disappearing, filling with details that glinted and vanished.” In ‘The Center of Everything,’ a woman with a brain injury tries to make sense of her thoughts — and her past |Ellen Akins |January 20, 2021 |Washington Post 

A delightful cast battles over a will and a stolen painting as a horde of pseudo-Nazis scour the mountains for fugitives. ‘The Grand Budapest Hotel’ Review: Wes Anderson’s Best Ever Is a Whimsical Crime Caper |Nico Hines |February 6, 2014 |DAILY BEAST 

Immediately after the attack, Philip used his searchlights to scour the ocean for survivors to rescue. The Grand Old Duke of Edinburgh: Why Everybody Loves Phil |Tom Sykes |November 23, 2012 |DAILY BEAST 

Every day, the two men, part of a 25-person outreach force, scour the streets looking for people everyone else wants to ignore. Advocates Reached Out to Ronald Poppo Before He Was Face-Eating Victim |Aram Roston |June 4, 2012 |DAILY BEAST 

Immediately after the attack, Philip used his searchlights to scour the ocean for survivors. 12 Juicy Bits From the Prince Philip Biography |Tom Sykes |November 14, 2011 |DAILY BEAST 

Geophysical Research Letters: Bottom scour observed under Hurricane Ivan. The Gulf's Next Disaster |Dominique Browning |June 11, 2010 |DAILY BEAST 

And it seems likely that the King loved him all the more because he could cook and scour for his sake. The Library of Work and Play: Housekeeping |Elizabeth Hale Gilman 

It isn't that the floor is not scoured, for you cannot scour dry mud into anything but wet mud. The Kellys and the O'Kellys |Anthony Trollope 

He sent off the old man to scour the pantry for a supper for me, and then pushed open the door and led me into the room. The Yeoman Adventurer |George W. Gough 

But Felix was thinking about "Scour, mop, and dry it," as he looked at the snow-covered patch of land. Little Folks |Various 

Can you not turn for one look in your enemy's face, ere you scour away before him like a herd of frightened deer? Sarchedon |G. J. (George John) Whyte-Melville