“The existence of epigenetic heredity is of paramount biological relevance, but the extent to which it happens in mammals remains largely unknown,” said Drs. These Mice Pups Inherited Immunity From Their Parents—But Not Through DNA |Shelly Fan |November 2, 2021 |Singularity Hub 

Lewontin devoted nearly his entire academic career to the study of genes, the unit of heredity by which traits are passed from parent to offspring. Richard Lewontin, a preeminent geneticist of his era, dies at 92 |Emily Langer |July 8, 2021 |Washington Post 

Sometimes the intended insights have relied on simplified scenarios that shed light on scientific questions, such as Bell’s theorem or heredity, or on philosophical questions, such as elegance in mathematics or randomness. Zen and the Art of Puzzle Solving |Pradeep Mutalik |February 10, 2021 |Quanta Magazine 

“We as a species need to maintain the flexibility, in the face of future threats, to take control over our own heredity,” George Daley, the dean of Harvard Medical School, told an audience in Hong Kong in 2018. Gene editing has made pigs immune to a deadly epidemic |Katie McLean |December 11, 2020 |MIT Technology Review 

Yes, heredity is indeed far more subtle than we’d guess from the simple Mendelian mathematical rules we learned in school. How to Design (or at Least Model) Mixed Dog Breeds |Pradeep Mutalik |July 31, 2020 |Quanta Magazine 

Not surprisingly, then, this is a book about heredity, about fathers and sons and their awkward relationships. Favorite Historical Novels |Stella Tillyard |November 9, 2011 |DAILY BEAST 

A frequently touching domestic drama about an academic Chicago family, it mulls Big Themes: war, faith, heredity. The Squid and the Whale of a Book |Taylor Antrim |June 14, 2009 |DAILY BEAST 

Heredity decides how a man shall be bred; environment regulates what he shall learn. God and my Neighbour |Robert Blatchford 

Take away from a man all that heredity and environment have given him, and there will be nothing left. God and my Neighbour |Robert Blatchford 

As all men are what heredity and environment have made them, no man deserves punishment nor reward. God and my Neighbour |Robert Blatchford 

I doubt if most people, although they would call that a platitude, realize that heredity is anything more than a telling word. Ancestors |Gertrude Atherton 

As might be expected in this severer form of mental disturbance, heredity plays an especially important part in circular insanity. Essays In Pastoral Medicine |Austin Malley