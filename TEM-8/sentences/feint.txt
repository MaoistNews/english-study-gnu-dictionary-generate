While some may see a cynical feint—float in the Dead Sea and watch the Jewish vote pour in! Carly's Schlep to Israel |Samuel P. Jacobs |September 8, 2010 |DAILY BEAST 

Netanyahu had no choice but to at least feint in this direction. Bibi's Bait-and-Switch |Eric Alterman |June 14, 2009 |DAILY BEAST 

It increased in volume, this firing, until I feared that what had been started as a feint was being pushed forward to a victory. A Virginia Scout |Hugh Pendexter 

On the evening of the 15th his advance made a feint demonstration against Petersburg, and on the 16th made his attack in force. Reminiscences of the Guilford Grays, Co. B., 27th N.C. Regiment |John A. Sloan 

The boatswain swore with wicked words,Enough to shock a saint, That though she did seem in a fit,'Twas nothing but a feint. The Book of Humorous Verse |Various 

She is jesting: I have it here; Monsieur, the feint is useless. Amphitryon |Moliere 

But Clairon considers that this is a feint on his part, and that he will arrive sooner, in order to surprise her. Queens of the French Stage |H. Noel Williams