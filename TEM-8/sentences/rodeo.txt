It was nothing related to the rodeo, Todd and Sacha were just not the best creative fit for each other. Sacha Baron Cohen’s Partner in Crime Seizes the Spotlight |Marlow Stern |July 30, 2021 |The Daily Beast 

The previous weekend the city had hosted the annual World Famous Miles City Bucking Horse Sale, a rodeo that brings concert stages to Main Street and hundreds of revelers into the Montana Bar. Navigating the shifting protocols of a not-quite-post-pandemic world |Tom Haines |June 17, 2021 |Washington Post 

Now Abbott has thrust Texans back into the reopen rodeo show, and so here we go again. Living in Texas right now feels like an exercise in survival |Karen Attiah |March 5, 2021 |Washington Post 

It’s subtle on many skis, but rocker is that three-dimensional shaping reminiscent of the upturned nose of a surfboard or the hull of a rodeo kayak. Long-Term Review: The Best Skis, Period |Heather Schultz and Marc Peruzzi |March 4, 2021 |Outside Online 

Local regulations in Omaha allow for 75 percent capacity at CHI Health Center, which is usually home to the Creighton University basketball teams but also hosts concerts, rodeos and professional wrestling events. USA Swimming shrinks Olympic trials as safety precaution |Rick Maese |January 26, 2021 |Washington Post 

When Brooks went into his second song, an old hit called “Rodeo,” the cheers grew so loud it was hard to hear him sing. I'm Not Country or Pop. I'm Just Pure Garth Brooks. |David Masciotra |September 10, 2014 |DAILY BEAST 

The Gossip Girl actress has followed Gwyneth Paltrow to the blogging ‘n’ selling online rodeo. Blake Lively Gets Her GOOP On |Tim Teeman |July 23, 2014 |DAILY BEAST 

In the movie, Matthew McConaughey plays a rodeo cowboy diagnosed with AIDS. Meet the Governor-Turned-Ganjapreneur |Olivia Nuzzi |July 3, 2014 |DAILY BEAST 

This was the rodeo equivalent of pulling the sword out of the stone. The Death of a Rodeo Cowboy |Peter Richmond |May 11, 2014 |DAILY BEAST 

He resisted the lures of the buckle bunnies who linger late in a rodeo arena, looking to sidle up against the winners. The Death of a Rodeo Cowboy |Peter Richmond |May 11, 2014 |DAILY BEAST 

Ten years from now, thar won't be a cow hand ner a gun outside a dude ranch er a rodeo. David Lannarck, Midget |George S. Harney 

Anza refers to only one, in addition to the one on Rodeo Creek, viz., the large village at Tormey. The Aboriginal Population of Alameda and Contra Costa Counties, California |S. F. Cook 

We may tentatively draw their boundary between Rodeo and Crockett. The Aboriginal Population of Alameda and Contra Costa Counties, California |S. F. Cook 

Life during the rodeo was a combined circus and school-day vacation when off duty with the herd. The Long Dim Trail |Forrestine C. Hooker 

We was workin' the rodeo back of Dos Cabezas when we come across a seven-year ol' black horse that was an outlaw. The Long Dim Trail |Forrestine C. Hooker