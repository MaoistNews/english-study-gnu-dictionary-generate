Mom and Barbara would later become fast friends and political allies, and they are who I have in my heart as we contemplate this bittersweet centennial. Melinda Gates, Ai-jen Poo, and 9 more women on what the 19th Amendment’s 100th anniversary means to them |ehinchliffe |August 18, 2020 |Fortune 

No one in the EU is contemplating an association agreement with Belarus. Why You Should Care About Belarus |Tracy Moran |August 17, 2020 |Ozy 

ChilledCow’s soft hip-hop jams are just what you need to keep you mellow while studying, running errands or lying on the floor contemplating eternity. This Weekend: Mix Your Coffee With … Lemonade? |Fiona Zublin |August 14, 2020 |Ozy 

The normalcy I used to feel within the campus “bubble” no longer exists, and I’ve been forced to contemplate what kind of world I may be graduating into next year. Come COVID or High Water, College Is … Happening? |Joy Nesbitt |August 9, 2020 |Ozy 

By contemplating the end times, we can refine our understanding of the universe, but we can’t change its fate. ‘The End of Everything’ explores the ways the universe could perish |Emily Conover |August 4, 2020 |Science News 

“The idea of Mitch McConnell as the Majority Leader is too bizarre and dark to contemplate [right now],” she said. Meet the Hollywood Power Couple Who Bet Big on the Midterms—and Lost |Asawin Suebsaeng |November 6, 2014 |DAILY BEAST 

But the consequences of a nuclear exchange are almost too horrible to contemplate. ICYMI: India-Pakistan Head for Nuke War |Bruce Riedel |October 20, 2014 |DAILY BEAST 

As the Cofán shaman blew strongly over the cup, I took those few seconds to contemplate how I had managed to find myself here. Spirit Tripping With Colombian Shamans |Chris Allbritton |August 24, 2014 |DAILY BEAST 

“As the day of the operation drew closer, it became more and more painful and frightening to contemplate,” wrote Reeve. Robin Williams and Christopher Reeve's Epic Friendship and the Greatest Williams Story Ever Told |Marlow Stern |August 12, 2014 |DAILY BEAST 

When I contemplate God among the dead I find only emptiness and silence. How Losing My Daughter Changed My Faith |Kyle Cupp |June 15, 2014 |DAILY BEAST 

The working man was incited to contemplate the beauty of the night's rest that followed on the exhaustion of the day. The Unsolved Riddle of Social Justice |Stephen Leacock 

One step might spoil everything and lead to an exposure, the consequences of which were altogether too terrible to contemplate. The Weight of the Crown |Fred M. White 

And when we contemplate aright the exercise as sanctioned by the procedure of God, how distinctly are these brought before us! The Ordinance of Covenanting |John Cunningham 

It seemed that even the country gentlemen must begin to contemplate the probability of an alarming crisis. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Let us often contemplate this sorrow, and excite our hearts to a tender compassion with the Mother of Sorrows. Mary, Help of Christians |Various