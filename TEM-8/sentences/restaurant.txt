The awards might be off, but the volunteer committee that orchestrates the restaurant and chef awards has made known to the foundation’s chairman that their “faith in the Foundation has been shaken.” The Mess That Is the 2020 James Beard Awards, Explained |Elazar Sontag |September 11, 2020 |Eater 

Which is just one more reason restaurants should be getting a bailout instead of having to risk people’s health to stay afloat. CDC Finds Adults With COVID-19 Were Twice as Likely to Have Been to a Restaurant |Jaya Saxena |September 11, 2020 |Eater 

The pandemic has upended industry norms, pushing restaurants to add service charges and raise wages Three D.C. Restaurants Explain Why They’re Eliminating Tipping |Eater Staff |September 10, 2020 |Eater 

Spice companies like Penzeys and Spicewalla, as well as restaurants, have been selling more mixes than ever. The Redemption of the Spice Blend |Jaya Saxena |September 10, 2020 |Eater 

Everyone knows restaurants are struggling—Tom Colicchio tweeted that with outdoor dining, his restaurants are doing just 20 percent of their usual business. People Are Dining Out, They Just Don’t Want Anyone to Know About It |Jaya Saxena |September 3, 2020 |Eater 

The gunman then burst from the restaurant and fled down the street with the other man. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Any restaurant with a sustained fame ends up becoming a set, of sorts, and on that front, Sotto Sotto cinched it. The Fiery Death of Sotto Sotto, Toronto’s Celebrity Hotspot |Shinan Govani |December 30, 2014 |DAILY BEAST 

When I saw the fire in the restaurant, I ran down to the floor below, where I was trapped between flames above and below. ‘We’re Going to Die’: Survivors Recount Greek Ferry Fire Horror |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

It's nothing for someone to walk up to me in the store or at a restaurant and ask for an autograph or speak to me. Porn Stars on the Year in Porn: Drone Erotica, Belle Knox, and Wild Sex |Aurora Snow |December 27, 2014 |DAILY BEAST 

I learned that he was working and living in the Lower East Side, delivering orders for an Italian restaurant and raising two kids. Cuban Hip-Hop Was Born in Alamar |Daniel Levin |December 26, 2014 |DAILY BEAST 

I often recall the farewell lunch we had together at the Restaurant de Paris, in the Escolta. The Philippine Islands |John Foreman 

There was much movement, life, laughter, carriages in the street driving from restaurant to theatre. Ancestors |Gertrude Atherton 

He remembers the good dinners at the little restaurant near his studio, where they dined among the old crowd. The Real Latin Quarter |F. Berkeley Smith 

Below it is the café and restaurant de la Rotonde, a very well-built looking place, with its rounding façade on the corner. The Real Latin Quarter |F. Berkeley Smith 

She will hunt for some small restaurant, sacred in its exclusiveness and known only to a dozen bon camarades of the Quarter. The Real Latin Quarter |F. Berkeley Smith