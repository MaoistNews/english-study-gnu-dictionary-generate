These revisions come on top of revenue losses in FY2020, when the shuttering of businesses began. COVID-19 has another long-term side effect: A shrinking tax base |cleaf2013 |August 31, 2020 |Fortune 

That helped establish a model for revisions to an old piece of legislation, from 1948, that would eventually become known as the Clean Water Act. 51 Years Later, the Cuyahoga River Burns Again |Wes Siler |August 28, 2020 |Outside Online 

In the 1960s, after Paschini’s death, the church relented, authorizing publication — but only after revisions that bowdlerized the original version to portray the church in a more favorable light. A new Galileo biography draws parallels to today’s science denialism |Tom Siegfried |August 11, 2020 |Science News 

We urge the city to slow down the approval process to allow for the sharing of any revisions and refinements made by city staff prior to seeking decision-makers’ approvals. New Plan for City Parks Misses the Point |Deborah Sharpe, Howard Greenstein and Jeff Harkness |July 24, 2020 |Voice of San Diego 

If you’re having to track revisions and control permissions in other software, you’re wasting valuable time. How SEO Friendly CMS can support your digital goals now and in future |Jim Yu |July 16, 2020 |Search Engine Watch 

At its most simplified, the revision allows big players more access to insured deposits while making certain types of bets. How Naive is Elizabeth Warren? |Nick Gillespie |December 18, 2014 |DAILY BEAST 

Despite this revision, the Bosworth Field Visitor Center will remain in the village where it is, closer to the wrong site. Three Dicks: Cheney, Nixon, Richard III and the Art of Reputation Rehab |Clive Irving |July 27, 2014 |DAILY BEAST 

That differential accounted for about 60 percent of the downward revision. How Obamacare Helped Crash the Economy |Daniel Gross |June 25, 2014 |DAILY BEAST 

But it represented a very substantial revision from what the government had originally reported. How Obamacare Helped Crash the Economy |Daniel Gross |June 25, 2014 |DAILY BEAST 

At first blush, the substantial downward revision is something of a mystery. How Obamacare Helped Crash the Economy |Daniel Gross |June 25, 2014 |DAILY BEAST 

This is, of course, possible, but it cannot be more than speculation; the final Dunciad does show evidence of hasty revision. A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

He achieved his highest fame from his connection with the revision of the statutes of New York. The Every Day Book of History and Chronology |Joel Munsell 

I have not been able to read these pages, and have been compelled to entrust their revision to other eyes and other hands. The Poems of Giacomo Leopardi |Giacomo Leopardi 

Later, however, progress became a little more rapid and the revision was completed on 11 November, 1880. A History of the Cambridge University Press |S. C. Roberts 

The laws which govern the collection of the tithe-composition in Ireland require revision and amendment. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan