Sandwiched in between was a “Potbelly Punch” of SoCal Fruit Punch and mandarin vodka. Wash ‘Pulp Fiction’ Down With a Tasty Beverage |Rich Goldstein |October 17, 2014 |DAILY BEAST 

It is a safe bet that more Chinese are learning English than foreigners are studying Mandarin. The Person of 2013 Is… Xi Jinping |Jonathan Fenby |December 31, 2013 |DAILY BEAST 

In the original Marvel comic books, the Mandarin was born and bred in China, a descendant of Genghis Khan no less. Did Hollywood Collaborate With Hitler? A New Book Makes Bold Claims. |Christopher Bray |September 9, 2013 |DAILY BEAST 

Because he does not speak Mandarin very well, he would be autonomous and “secretly unfriendable.” The Gpistolary Novel: Tao Lin’s ‘Taipei’ |Emily Witt |June 18, 2013 |DAILY BEAST 

But there is one important thing they do not know, which is what it is like to be anyone except a mandarin. America’s New Mandarins |Megan McArdle |February 21, 2013 |DAILY BEAST 

At the middle altar was the mandarin, piously engaged in prayer, while two stood beside him, fanning him with large fans. A Woman's Journey Round the World |Ida Pfeiffer 

They pay down a certain sum, as caution-money, to some mandarin, and the latter answers for them. A Woman's Journey Round the World |Ida Pfeiffer 

A number of peacocks, silver-pheasants, mandarin-ducks, and deer are preserved in their gardens. A Woman's Journey Round the World |Ida Pfeiffer 

She was at once seized by the guards, but was released at the intercession of a certain mandarin. The Science of Fairy Tales |Edwin Sidney Hartland 

I promised the blue mandarin to Darthea Wynne because he always nodded yes to her when she wanted advice to her liking. The Red City |S. Weir Mitchell