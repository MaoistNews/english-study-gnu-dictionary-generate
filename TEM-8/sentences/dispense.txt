But he was always uncommonly gracious, a truly gentle man, willing to dispense wisdom and perspective when asked. Ed Brooke: The Senate's Civil Rights Pioneer and Prophet of a Post-Racial America |John Avlon |January 4, 2015 |DAILY BEAST 

The question is not whether they are right or wrong but why they feel an impulse to dispense their advice in the first place. America’s Meddlers Are Our Worst Enemies |Stefan Beck |October 3, 2014 |DAILY BEAST 

Instead of the eight flavors a typical fountain could dispense, they might want 15 or 20. Font of Invention | |September 18, 2014 |DAILY BEAST 

Let's hope they dispense with the slow courtship and start to dance. How Iran and America Can Beat ISIS Together |Ben Van Heuvelen |June 21, 2014 |DAILY BEAST 

As nice as it would have been to dispense with the fairy tale tropes altogether, they are necessary here. Disney’s Sublimely Subversive ‘Frozen’ Isn’t Your Typical Princess Movie |Melissa Leon |November 29, 2013 |DAILY BEAST 

With his pipe he would not dispense, and he always took two or three puffs, at least, before undertaking anything. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Divinely precious and infinitely perfect as it is, there is no part of it with which he can dispense. The Ordinance of Covenanting |John Cunningham 

She belonged to that ultra-modern school which scorns to sue masculine admiration, but which cannot dispense with it nevertheless. Dope |Sax Rohmer 

There were very few days during our entire summer's tour when one could dispense with cloaks and overcoats. British Highways And Byways From A Motor Car |Thomas D. Murphy 

At no time in the summer were we able to dispense for any length of time with heavy wraps and robes while on the road. British Highways And Byways From A Motor Car |Thomas D. Murphy