Later, Melenda would send me off with a warm slice of her homemade rum cake wrapped in aluminum foil. Until I Can Go Back to My Favorite Restaurant, This Jerk Paste Is the Next Best Thing |Elazar Sontag |September 25, 2020 |Eater 

Typically, they’re refreshing, tart, and often physically chilled, making an ideal foil to the highly spiced meat. Turn a cheap chicken dinner into a Turkish street-food getaway |SAVEUR Editors |September 25, 2020 |Popular-Science 

The new design is “tabless,” which means the rolled-up foils inside each cell won’t need a metal tab running its length in order to enable charging and discharging. Tesla’s new battery tech promises a road to a cheap self-driving electric car |Stan Horaczek |September 24, 2020 |Popular-Science 

He’s turned the group into a boogeyman of sorts, and it serves as a perfect foil for a president and a conservative movement looking to cast the overwhelmingly peaceful participants in protests over police brutality as a group of violent thugs. What to make of the DHS whistleblower’s shocking complaint |Alex Ward |September 11, 2020 |Vox 

Line an open box with aluminum foil, and use it to reflect sunlight into the box. Let’s learn about the sun |Bethany Brookshire |June 24, 2020 |Science News For Students 

Frank no longer has his convenient foil from The Lay of the Land, his Tibetan associate Mike Mahoney. Richard Ford’s Artful Survivalist Guide: The Return of Frank Bascombe |Tom LeClair |November 4, 2014 |DAILY BEAST 

But his lumbering lurch toward the Ted Cruz tin-foil-hat convention should instead be an object lesson for Republicans to come. Will the GOP Get the Message in Kansas? |Ana Marie Cox |October 24, 2014 |DAILY BEAST 

If McConnell really thinks that, then why not foil their scheme by voting yes? The Gender-Pay Gap: It’s Real, and Yes, It’s Sexism |Monica Potts |September 27, 2014 |DAILY BEAST 

French and Crown one rib rack and season with salt and pepper, cover exposed bones with foil, cook in oven at 350 for 2.5 hours. Epic Meal Empire’s Meat Monstrosities: From the Bacon Spider to the Cinnabattleship |Harley Morenstein |July 26, 2014 |DAILY BEAST 

The image really serves as a foil to the victim on the tape. True Detective’s Red Herring: Actress Erin Moriarty, Who Plays Marty’s Daughter, Tells All |Marlow Stern |March 10, 2014 |DAILY BEAST 

A small palm tree was set in the midst of the arena,—the trunk bronze, the leaves one sheen of gold-foil. God Wills It! |William Stearns Davis 

Out of the panic at Big Shanty two men emerged, determined, if possible, to foil the unknown captors of their train. Famous Adventures And Prison Escapes of the Civil War |Various 

General Hancock's head-quarters' flag,—the tree-foil of the Second Corps,—was waving on the ridge southwest of the house. The Boys of '61 |Charles Carleton Coffin. 

For two ounces of liquid I should recommend a sheet of stout silver foil, about two inches long and half an inch broad. Notes and Queries, Number 218, December 31, 1853 |Various 

"You bet they're the real thing," said Tough McCarty, slipping off the foil. The Varmint |Owen Johnson