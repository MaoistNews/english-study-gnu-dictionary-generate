A Family Grieves After the Buffalo ShootingAs a nation, we may consider repair, but repentance means to “turn away.” America Insists It Is Great. It Should Work on Being Decent |Taylor Harris |July 2, 2022 |Time 

Muslims believe that fasting develops submission to God, empathy with the poor and repentance and gives time for spiritual introspection. Fasting May Have Become a Health Fad, But Religious Communities Have Been Doing It For Millennia |LGBTQ-Editor |July 30, 2021 |No Straight News 

They carried megaphones and signs about repentance and damnation, but once the restaurant’s staff blasted Gaga’s “Born This Way” on the patio speakers the crowd began to disperse. Our national anthem ends with a question. Lady Gaga answered it as best she could. |Chris Richards |January 20, 2021 |Washington Post 

Only much later, after Anne Sullivan had taught to her to sign using English, had Keller “realized what I had done, and for the first time I felt repentance and sorrow.” Five Scientists on the Heroes Who Changed Their Lives - Issue 93: Forerunners |Alan Lightman, Hope Jahren, Robert Sapolsky, |December 2, 2020 |Nautilus 

By 2018, he was eager to demonstrate his repentance, while Daines, two years away from his own reelection bid, was eager to help him. Want to Win in Montana? It's the Environment, Stupid. |Elliott D. Woods |October 28, 2020 |Outside Online 

And if the volunteer was formerly a member of the state security forces, he can only join a year after declaring repentance. ISIS’s Newest Recruit Is 10 Years Old |Niqash |June 30, 2014 |DAILY BEAST 

The Yom Kippur repentance ritual demands that we reconcile with our fellow human beings before we reconcile with God. Celebrating An Open Jerusalem |Gil Troy |September 25, 2012 |DAILY BEAST 

“They felt a certain repentance after what they had done,” the judge wrote. New Hope for Amanda Knox |Barbie Latza Nadeau |March 4, 2010 |DAILY BEAST 

From mere regrets he was passing now, through dismay, into utter repentance of his promise. St. Martin's Summer |Rafael Sabatini 

Henoch pleased God, and was translated into paradise, that he may give repentance to the nations. The Bible, Douay-Rheims Version |Various 

And remorse without one grain of honest repentance pierced his heart. It Is Never Too Late to Mend |Charles Reade 

I had not prayed openly before, now when I was nearing death it was no time for a hurried repentance and a stammered prayer. Private Peat |Harold R. Peat 

His grandfather had repented, but who was to preach repentance unto these? The Heir of Redclyffe |Charlotte M. Yonge