He’s no match to the original C-3PO’s fluency in 6 million forms of communication, but he’s got full linguistic mastery and can learn from humans like humans do—from observation and imitation, whether it’s using sarcasm or sticking dishes into slots. Why AI Needs a Genome - Issue 108: Change |Lina Zeldovich |December 8, 2021 |Nautilus 

Of course, when a student posts a split-second video of the gesture, that sarcasm is lost amid viral context collapse. Netflix’s Sharp Satire The Chair Throws Sandra Oh Into the Politicized Powder Keg of Higher Ed |Judy Berman |August 19, 2021 |Time 

There are also plenty of mom accounts drenched in sarcasm, illustrating what a hot mess the maternal experience can be. The many emotions of motherhood — and how to manage them |Ilyse Dobrow DiMarco |June 25, 2021 |Washington Post 

So I’ve tried not to be sarcastic because sarcasm and humor do not translate. Ron Rivera tries to keep Washington focused amid increasing coronavirus disruptions |Nicki Jhabvala |November 30, 2020 |Washington Post 

We try to always keep the self-deprecation in it because that optimism we feel about people does not have room for sarcasm or negativity. Is It Moral to Have Facebook as a Client? Colleen DeCourcy, Wieden+Kennedy Co-President, Isn’t So Sure |Eben Shapiro |October 4, 2020 |Time 

Lou's valedictory has been acclaimed, without sarcasm, as baseball's Gettysburg Address. The Stacks: The Day Lou Gehrig Delivered Baseball’s Gettysburg Address |Ray Robinson |July 4, 2014 |DAILY BEAST 

Friedman is acutely aware of the thin line between soap opera and sarcasm. Lifetime’s ‘Witches of East End’ Is the Ultimate Witch Show |Anna Brand |November 21, 2013 |DAILY BEAST 

A spectre is haunting the internet—the spectre of Open Sarcasm. The Rise and Fall of the Infamous SarcMark |Keith Houston |September 24, 2013 |DAILY BEAST 

But it is the quest of a father and son to invent a symbol for sarcasm that will live in infamy. The Rise and Fall of the Infamous SarcMark |Keith Houston |September 24, 2013 |DAILY BEAST 

The written word has question marks and exclamation points to document those thoughts, BUT sarcasm has NOTHING! The Rise and Fall of the Infamous SarcMark |Keith Houston |September 24, 2013 |DAILY BEAST 

A vein of shrewd and humorous sarcasm, together with an under-current of quiet selfishness, made him a very pleasant companion. ' Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He had often been floored by argument and coughed down by contempt, but he seemed alike insensible to sarcasm and to insult. Madame Roland, Makers of History |John S. C. Abbott 

His manner disconcerts me; but the sarcasm of his words and the offensive tone rouse my resentment. Prison Memoirs of an Anarchist |Alexander Berkman 

Avoid sarcasm; it will, unconsciously to yourself, degenerate into pertness, and often downright rudeness. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

I see you were not, said the stranger, an expression of quiet sarcasm playing about his mouth, or you would have known my name. Oliver Twist, Vol. II (of 3) |Charles Dickens