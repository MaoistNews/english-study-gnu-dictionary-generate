If someone clicks on a search at 3 AM, Google will find the electricity to power that query from a battery, wind turbine, solar panel, hydroelectric dam, or some other carbon-free technology at that precise moment. Google made clean energy cool for corporations, and it’s about to do the same for batteries |Michael J. Coren |September 17, 2020 |Quartz 

Already, droughts regularly threaten food crops across the West, while destructive floods inundate towns and fields from the Dakotas to Maryland, collapsing dams in Michigan and raising the shorelines of the Great Lakes. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

McKay was drawn to this region because of an impending dam project, but rather than focus on conservation alone, he explores a person’s spiritual connection to a resource. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

As dams form ponds, more liquid water can come in contact with ice in the ground. Busy beavers may be speeding thaw of Arctic permafrost |Kathiann Kowalski |August 31, 2020 |Science News For Students 

In fact, the dam has helped to shift longstanding power relationships and could pave the way for more cooperation among all the countries that depend on the Nile. How to understand the Grand Renaissance Dam tensions between Egypt, Ethiopia and Sudan |Ashok Swain |July 31, 2020 |Quartz 

A dam now in place on the Thai side of the line prevents the railway from being reconstructed in its entirety, he explains. Riding Thailand’s WWII Death Railway |Liza Foreman |December 21, 2014 |DAILY BEAST 

When the family was fine, or when a cruel employee at the dam was behind the flood, God was left out of the explanation. Why Are Millennials Unfriending Organized Religion? |Vlad Chituc |November 9, 2014 |DAILY BEAST 

For example, he added, many highly valuable Western hostages are held in an ISIS prison beneath a dam near Raqqa. Obama Is Just 'Tickling' ISIS, Syrian Rebels Say |Josh Rogin |August 25, 2014 |DAILY BEAST 

The Arizona senator described a provision inserted into the debt deal appropriating $2 billion for a Kentucky dam as "disgusting." McCain Trashes Part of Debt Deal |Ben Jacobs |October 17, 2013 |DAILY BEAST 

No single joke can topple a tyrant, but each one is a small stone cast at an already fractured dam. Celebrate Dictator Appreciation Month |David Keyes |June 20, 2013 |DAILY BEAST 

They heard how in the early spring in the meadow by the mill-dam Tim and I had stopped our ploughs to draw lots and he had lost. The Soldier of the Valley |Nelson Lloyd 

For that matter, he said, he didn't care a tinker's dam if we were; he had grub and bedding and we were welcome to both. Raw Gold |Bertrand W. Sinclair 

The great Dam at Assouan was just completed and we traversed its entire length on a trolley propelled by natives. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The dam was completed, booms and cribbing placed, ledges blasted out well within the six months' period set for those operations. Scattergood Baines |Clarence Budington Kelland 

Then if each end of the log is on the upper side of the trees, the harder the water pounds the tighter the dam gets. The Box-Car Children |Gertrude Chandler Warner