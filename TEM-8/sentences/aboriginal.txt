The day just gets more upsetting when we hear about aboriginal rights in Canada. Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 

The cover story is about how over 1,000 aboriginal women have gone missing since 1980. Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 

“At that time, Aboriginal people were only just granted citizenship,” Briggs tells the Daily Beast. ‘The Sapphires’: Tony Briggs and His Mum on Racism in Australia, Music & More |Jean Trinh |March 22, 2013 |DAILY BEAST 

One Sapphire, Kay, falls for a black soldier, even as she struggles with her Aboriginal identity. ‘The Sapphires’: Tony Briggs and His Mum on Racism in Australia, Music & More |Jean Trinh |March 22, 2013 |DAILY BEAST 

An all-girl Aboriginal quartet sings in war-torn Vietnam in ‘The Sapphires,’ out today. ‘The Sapphires’: Tony Briggs and His Mum on Racism in Australia, Music & More |Jean Trinh |March 22, 2013 |DAILY BEAST 

MacRae's seat, stone-marker, and aboriginal spearhead; the three lined up like the sights of a modern rifle. Raw Gold |Bertrand W. Sinclair 

Your school years are ended, and a life of quiet, amid scenes of aboriginal romance, awaits you here. Mystery Ranch |Arthur Chapman 

On the whole, the new comers succeeded in making friends of the aboriginal race. The History of England from the Accession of James II. |Thomas Babington Macaulay 

The aboriginal races include American Indians of the mainland and Caribs. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

The aboriginal tribes of Australia are of a low-class black race, but generally peaceful and inoffensive in their habits. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various