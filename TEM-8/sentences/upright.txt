Think upright, eye level with your computer, and with your back fully supported. Best desk chair for any home office |PopSci Commerce Team |February 11, 2021 |Popular-Science 

It will usually be a classic upright, without the ability to transform to a handheld, and with very few additional tools or features. Best cordless vacuum: Suck up debris without getting yourself tangled |Charlotte Marcus |January 19, 2021 |Popular-Science 

Very often back problems are caused by a lack of core stability—your core being the muscles around your abdomen that keep you upright. Best office chair: Get comfy, stay productive with our office furniture picks |PopSci Commerce Team |January 11, 2021 |Popular-Science 

It provides a base to transfer power between the upper and lower body and gives you the stability to move swiftly and stay upright in unpredictable terrain. This 10-Move Core Workout Will Kick Your Ass |Hayden Carpenter |December 21, 2020 |Outside Online 

Instead of lugging a heavy upright vacuum out of the closet, you just pop this one off its wall charger and go to town. 7 gifts to take the stress out of stress-cleaning |Jessica Boddy |December 16, 2020 |Popular-Science 

As I tried to get upright, I realized with horror that the blood was my own. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

He slept in an upright position in a custom armchair, so the reasons for his lying down to sleep are open to speculation. The True Story of ‘The Elephant Man’ |Russell Saunders |November 3, 2014 |DAILY BEAST 

They look like evil corn silos or upright storm sewers or a trio of escaped steroidal church organ pipes wearing party hats. Up to a Point: A 'Space Corvette' in Every Garage |P. J. O’Rourke |September 6, 2014 |DAILY BEAST 

So why not keep all passengers in the ‘upright position’ when they fly? Solution to Seat Rage: No More Reclining |Will Doig |September 4, 2014 |DAILY BEAST 

In a dim backroom of a mud hut in Save, 82-year-old Teresa Nyirabutunda sits propped upright in bed by her daughter, Francine. After the Genocide, Rwanda’s Widows Aging Alone |Nina Strochlic |August 31, 2014 |DAILY BEAST 

In shape it is curved, like those things for candles attached to upright pianos, but with a weighted foot to hold it firm. Music-Study in Germany |Amy Fay 

Mrs. Ducksmith, who had sat with overwhelmed head in her hands, started bolt upright, and looked at him like one thunderstruck. The Joyous Adventures of Aristide Pujol |William J. Locke 

He had a flashlight gun in his right hand, and a photographer's tripod was propped upright between his knees. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The sick woman had raised herself upright, and was stretching her arms towards them. Oliver Twist, Vol. II (of 3) |Charles Dickens 

The boiler was of six wrought-iron upright tubes, one within the other. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick