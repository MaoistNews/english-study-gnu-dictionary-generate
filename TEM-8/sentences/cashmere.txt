Among what seems like millions of grandpa sweaters might be a vintage cashmere lurking, or a pair of Prada heels unassumingly tossed in a pile of Payless flats. Thrift shopping is an environmental and ethical trap |Sara Kiley Watson |February 26, 2021 |Popular-Science 

Buy nowIf you want to take advantage of recycled cashmere on a smaller budget, here’s a project I’ve been making in various capacities since I was a teenager. Blair Braverman's Favorite Soft Winter Gear |Blair Braverman |January 31, 2021 |Outside Online 

Soft cashmere adds a touch of luxury to your life, so enjoy. Stylish and sophisticated scarves that everyone will love |PopSci Commerce Team |January 13, 2021 |Popular-Science 

Made from Yak wool, which is almost as soft as cashmere but significantly more durable, it’s immediately cozy and keeps me warm on cold days when I don’t feel like running the heater. 7 Work-from-Home Upgrades That Boost Productivity |Jakob Schiller |December 1, 2020 |Outside Online 

Naadam sells ethically sourced cashmere items starting at just $75. How to Layer for Outdoor Dining This Fall and Winter |Wes Siler |October 14, 2020 |Outside Online 

It was a cashmere year for movies, so why are we cheering polyester? ‘American Hustle’ Is Overrated |Kevin Fallon |January 28, 2014 |DAILY BEAST 

Forget furs, cashmere, even fires—the best way to beat the cold? What to Drink When it’s Cold? The Glory of Austrian Schnaps |Jordan Salcito |January 25, 2014 |DAILY BEAST 

But it feels so much better to: Blow the whistle you have until now been concealing underneath your cashmere sweater. Crushing Christmas: How to Win Every Argument |Kelly Williams Brown |December 24, 2013 |DAILY BEAST 

Think $69 smocked bodice jumpsuits or $250 cashmere ones from French designers like Jacadi and Bonpoint. How Different Is Raising the Royal Baby From a Typical American Child? |Kevin Fallon, Lizzie Crocker |July 23, 2013 |DAILY BEAST 

Atop the mattress and box spring, Hughes reveals two “toppers,” stuffed with cashmere, horsehair, and lambswool. Savoir Beds’ Royal State Bed: Just Perfect, If You Have $175,000 |Daniel Gross |June 27, 2013 |DAILY BEAST 

Here, you,” he growled, “was aught said to thee whereby thou hast a scruple to tell me how many guns defend the Cashmere Gate? The Red Year |Louis Tracy 

The tissues woven in gold, the gold and silk embroideries and Cashmere shawls, are of the highest degree of perfection. A Woman's Journey Round the World |Ida Pfeiffer 

It was tied up in a cashmere shawl, which her maid recognized as belonging to the lost actress. Black Diamonds |Mr Jkai 

In the first place, the painter had removed the two pictures; and then Madame Guillaume had lost her cashmere shawl. At the Sign of the Cat and Racket |Honore de Balzac 

Madame Lebas had a cashmere shawl over her shoulders, of which the value bore witness to her husband's generosity to her. At the Sign of the Cat and Racket |Honore de Balzac