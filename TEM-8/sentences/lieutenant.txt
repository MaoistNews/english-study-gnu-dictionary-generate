The allegations detailed by her and two other detainees in that filing also involved a lieutenant who detainees said was promoted even after women complained. ICE Deported a Woman Who Accused Guards of Sexual Assault While the Feds Were Still Investigating the Incident |by Lomi Kriel |September 15, 2020 |ProPublica 

Giannandrea does not have a clear top lieutenant, so if he were to depart Apple, it’s unclear who would replace him or if Apple would fold the unit back into the Software Engineering department. Apple’s leadership evolves ahead of a post-Tim Cook era |radmarya |September 12, 2020 |Fortune 

Maybe, she thought, the governor and lieutenant governor planned to offer her a job within state government. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

Afterward, Mallott’s emergency replacement as lieutenant governor, Davidson, gave the keynote address. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

As lieutenant governor, Mallott still lived in Juneau and had long used the luxury hotel for extended stays and as a second office while in Anchorage, friends said. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

He was a young Army Air Force lieutenant whose plane crashed in the Pacific in May 1943. Dick Cheney vs. ‘Unbroken’ |Mike Barnicle |December 15, 2014 |DAILY BEAST 

Neary had held the rank of lieutenant since 1983 and received multiple commendations during nearly four decades on the job. The Mystery Death Of A Female Firefighter |Christopher Moraff |December 13, 2014 |DAILY BEAST 

Why has Michael Bloomberg replaced his longtime lieutenant with the editor-in-chief of The Economist? Inside Bloomberg News’ Shock Shake-Up |Lloyd Grove |December 9, 2014 |DAILY BEAST 

In 2010, he finished second with 39 percent in the race for lieutenant governor. How to Run a Statewide Campaign on $38 |Michael Ames |November 12, 2014 |DAILY BEAST 

He ran on a serious one-issue platform: eliminate the office of lieutenant governor. How to Run a Statewide Campaign on $38 |Michael Ames |November 12, 2014 |DAILY BEAST 

At the end of the campaign the Emperor justly rewarded his lieutenant by creating him Prince of Wagram. Napoleon's Marshals |R. P. Dunn-Pattison 

He was a member of the first provincial congress, and eighteen years lieutenant governor of the state of New York. The Every Day Book of History and Chronology |Joel Munsell 

The tailor of the fairy tale with his "seven at a blow" is not in it with the gunnery Lieutenant of a battleship. Gallipoli Diary, Volume I |Ian Hamilton 

The Lieutenant rode off highly elated over the fact that Colonel Guitar agreed with his views. The Courier of the Ozarks |Byron A. Dunn 

"I guess that is straight enough for Guitar to believe, instead of that upstart lieutenant," said Harry. The Courier of the Ozarks |Byron A. Dunn