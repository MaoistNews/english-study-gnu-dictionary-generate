The team’s aerodynamics experts would normally have been on the site of the wind tunnel, which is in the English town of Bedford, during the test. How data helped keep Red Bull’s F1 team on track during the pandemic |Jeremy Kahn |September 19, 2020 |Fortune 

Spoiler: it has something to do with the aerodynamics of the ball. Can You Answer These 10 Oddball Job Interview Questions Asked at America’s Top Tech Companies? |Brian Ries |January 17, 2014 |DAILY BEAST 

Americans built a fighter, the P-51, with aerodynamics that seemed to defy the laws of physics. The Nerds Who Won World War II |Marc Wortman |February 7, 2013 |DAILY BEAST 

After all, aerodynamics and electrodynamics are just theories, too. Ask the Big Questions |Deepak Chopra, Leonard Mlodinow |October 26, 2011 |DAILY BEAST 

He knew very little about the laws of aerodynamics, about stress and strain and factors of safety. The Romance of Aircraft |Lawrence Yard Smith 

The development of a new forage harvester based on principles of aerodynamics uncovered by missile engineers is another example. The Practical Values of Space Exploration |Committee on Science and Astronautics 

We had classes everyday on engines, aerodynamics, and air craft identification. The Biography of a Rabbit |Roy Benson 

It was about this time that he began his experiments in "aerodynamics." The Age of Invention |Holland Thompson