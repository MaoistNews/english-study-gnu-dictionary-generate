At sunset that day, the refinery’s rooftop will darken, but only for four months. Domino Sugar is replacing its massive neon landmark in Baltimore — and hopes no one will notice a difference |Colin Campbell |February 7, 2021 |Washington Post 

The on-court clouds that have darkened Toronto’s early season could be clearing. The Toronto Raptors Can’t Seem To Finish Their Drives |Louis Zatzman |January 27, 2021 |FiveThirtyEight 

The bots flag “restatement” as a negative word, which darkens their outlook on a company’s prospects. A 2011 dictionary is reshaping the language of corporate reporting |Samanth Subramanian |January 24, 2021 |Quartz 

The racism he faced in the run up to the record might have darkened Aaron’s joy, but, for him, bitterness never lingered. Baseball Legend Hank Aaron Has Passed Away, But His Home Run Record Remains a Lesson in Courage and Commitment |Sean Gregory |January 22, 2021 |Time 

That phenomenon occurs when the Moon is farthest away from the Earth to completely cover the Sun, resulting in a ring of light around the darkened moon. Mars missions, a super-powered telescope, and more exciting 2021 space events |Paola Rosa-Aquino |January 7, 2021 |Popular-Science 

The camera would pull in, the background would darken, and you would give a meta-commentary on the events. Voting for Slavery? Jim Wheeler Gets Into Hot Water |Jamelle Bouie |October 30, 2013 |DAILY BEAST 

If you only vote on how a person personally feels about abortion, you will never want her to darken your door. Sarah Palin's a Brainiac |Elaine Lafferty |October 27, 2008 |DAILY BEAST 

When the sky began to darken, however, they desisted for a time, and set about making preparations for the coming storm. Hunting the Lions |R.M. Ballantyne 

Gwynne's mind seemed to darken until only one luminous point confronted it, the visible tormented soul of his kinsman. Ancestors |Gertrude Atherton 

But you must know, she said, looking at him with her sea-blue eyes, that in moments of tense emotion seemed to widen and darken. The Woman Gives |Owen Johnson 

Over her shoulder peered her cousin Dave, and June saw his face darken while she looked. The Trail of the Lonesome Pine |John Fox, Jr. 

His hair was a little less primrose coloured than it had been (pomatum does darken hair a little), but his eyes had not altered. Mushroom Town |Oliver Onions