For context, 17 unicorns were minted in the United States during Q3 2020. With 5 new unicorns in first week of 2021, are we in for a stampede this year? |Alex Wilhelm |January 7, 2021 |TechCrunch 

For now it’ll only be available in vanilla, though chocolate, mint, and pistachio versions have been available over the years. Viennetta, the Fanciest Dessert of the ’90s, Is Back |Jaya Saxena |January 7, 2021 |Eater 

Instead, 2017’s bull run was largely fueled by a wave of newly-minted “alternative” cryptocurrencies that made big promises. A brief history of Bitcoin bubbles |dzanemorris |January 2, 2021 |Fortune 

The thought of sending our newly minted 5-year-old into a virtual learning environment at a new school broke our hearts. The money we didn’t spend in 2020 |Vox Staff |December 31, 2020 |Vox 

With a newly minted degree in literature, it was pretty much the only job I was qualified for. World class: Remembering legendary travel writer Jan Morris |Liza Weisstuch |December 10, 2020 |Washington Post 

Zied suggests popping a breath strip, sucking on a strong mint, or reapplying your lip gloss. 12 Thanksgiving Weight Loss Tips That Actually Work |DailyBurn |November 27, 2014 |DAILY BEAST 

By the way, a mint condition 1958 Cadillac Eldorado Biarritz convertible can now sell for as much as $350,000. Nationalism on Four Wheels |Clive Irving |October 18, 2014 |DAILY BEAST 

Then, it went big and ultimately became just another breath mint. How Nutella Conquered America |Emily Shire |May 21, 2014 |DAILY BEAST 

The true origins of the mint julep, however, stretch back considerably further. The Storied Origins of the Classic Mint Julep |Dane Huckelbridge |May 3, 2014 |DAILY BEAST 

So when you take that first frosty sip of your mint julep in celebration of the Kentucky Derby, enjoy it. The Storied Origins of the Classic Mint Julep |Dane Huckelbridge |May 3, 2014 |DAILY BEAST 

The last-named engine was intended for the coinage operations in the Mint at Lima. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The Mint belongs to our engine concern, and now coins about five millions per year. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

It was founded in 1723, and has since become a place of considerable size, for in it is the chief mint of the empire. Michael Strogoff |Jules Verne 

We are also told that the mint master was allowed a certain number of these coins as pay. The Story of the Thirteen Colonies |H. A. (Hlne Adeline) Guerber 

The word "money" is derived from the temple of Jupiter Moneta, where the Roman mint was established. The Bay State Monthly, Vol. II, No. 6, March, 1885 |Various