Endangered humpback dolphins have been spotted in Abu Dhabi and other varieties off the coast of Lagos. Is Maritime Piracy Back from the Dead? |Eromo Egbejule |August 25, 2020 |Ozy 

Michael had sold the place and moved to the coast a few years earlier. Investors ride the Big Tech rally even as COVID cases and unemployment spike |Bernhard Warner |August 21, 2020 |Fortune 

Ice near the heart of the continent today creeps coastward at less than 10 meters per year, while ice close to the coast picks up the pace, traveling up to a few kilometers per year. 50 years ago, scientists clocked the speed of Antarctic ice |Maria Temming |August 20, 2020 |Science News 

Most of the funding goes to the coasts, as opposed to the center of the country. A Power Couple With a $50M Venture Fund for the Overlooked |Nick Fouriezos |August 14, 2020 |Ozy 

Back east, where he grew up, the highways weren't this bad, but that's what you pay to live on the best coast, as his grom friends always say. Is There A Difference Between East Coast And West Coast Slang? |Candice Bradley |August 7, 2020 |Everything After Z 

Groups like the Crips and MS-13 have spread from coast to coast, and even abroad. The Daily Beast’s Best Longreads, Dec 29-Jan 4, 2014 |William Boot |January 4, 2015 |DAILY BEAST 

This approach would greatly limit his appeal beyond the Northeast and the west coast. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

There are still large tracts of the island, particularly on the north coast, that are undeveloped. Goodbye, Bahamas. Hello, Havana! |Clive Irving |December 18, 2014 |DAILY BEAST 

So too does Inherent Vice, which is something like a love letter written in pot smoke to the Gold Coast. Paul Thomas Anderson: The West Coast’s Scorsese |Nick Schager |December 13, 2014 |DAILY BEAST 

White King Soap sponsored the show on the West Coast, and Beech-Nut Gum in the East. The Flying Sorcery of Dr. Strange: Benedict Cumberbatch Is Marvel's Most Bizarre Magician |Rich Goldstein |December 8, 2014 |DAILY BEAST 

Two Battalions racing due North along the coast and foothills with levelled bayonets. Gallipoli Diary, Volume I |Ian Hamilton 

Every day they are gaining more strength, as is seen by the presence of so many of them on this coast. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

New France has an exceedingly varied sea-coast, indented by bays and rivers, broken and irregular. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

A traveler coming, wet and cold, into a country ale-house on the coast of Kent, found the fire completely blockaded. The Book of Anecdotes and Budget of Fun; |Various 

Their territory extended 400 miles on the Atlantic coast, and "from the Atlantic westward to the South sea." The Every Day Book of History and Chronology |Joel Munsell