You know, we had the typical lemonade stands and selling cinnamon sticks and things like that. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

OZY has reported that 70 percent of Gen Zers believe their lives need to make a difference in the world and 65 percent say it’s important for companies to take a stand on social issues. Reclaiming My Gen Z Identity |Shaan Merchant |September 7, 2020 |Ozy 

This engine, situated off-center, powered the vehicle at a slight angle into the sky, where it moved several dozen meters laterally before descending and coming to rest near the launch stand. SpaceX hops a full-scale Starship prototype for the second time |Eric Berger |September 3, 2020 |Ars Technica 

One of the reasons I joined Levi Strauss is that this company has had for its entire 167 years a practice where the CEO is expected to take stands on important issues of the day. CEOs aren’t promoting stakeholder capitalism for the publicity |Alan Murray |September 1, 2020 |Fortune 

We took a stand on it because it’s ripping the country apart. Levi Strauss’s Chip Bergh on why he’s taking his most direct stance yet against structural racism |Ellen McGirt |September 1, 2020 |Fortune 

To be a liberal, you have to stand up for liberal principles. Bill Maher: Hundreds of Millions of Muslims Support Attack on ‘Charlie Hebdo’ |Lloyd Grove |January 8, 2015 |DAILY BEAST 

And with stand-ups, I remember liking George Carlin and Steve Martin. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Those opposing same-sex marriage are on their heels, and increasingly unwilling or unable to make a stand against it. ‘Only God’ Can Stop Gay Marriage |Tim Mak |January 6, 2015 |DAILY BEAST 

Spencer, 27,  is variously described as a writer and a stand-up comic. Meet Stephen Fry’s Future Husband (Who Is Less Than Half His Age) |Tom Sykes |January 6, 2015 |DAILY BEAST 

Another read: “We need leaders who will stand against Common Core.” Rand Paul’s Passive-Aggressive Trolling Campaign |Olivia Nuzzi |January 6, 2015 |DAILY BEAST 

She stood, in her young purity, at one end of the chain of years, and Mrs. Chepstow—did she really stand at the other? Bella Donna |Robert Hichens 

But the liberal soul deviseth liberal things, and by liberal things shall he stand. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

All bribery, and injustice shall be blotted out, and fidelity shall stand for ever. The Bible, Douay-Rheims Version |Various 

It is only necessary to have a zinc, or a galvanized tray on which to stand the glass in an inverted position. How to Know the Ferns |S. Leonard Bastin 

Gold and silver make the feet stand sure: but wise counsel is above them both. The Bible, Douay-Rheims Version |Various