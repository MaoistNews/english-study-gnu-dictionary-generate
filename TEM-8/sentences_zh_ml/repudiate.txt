我们认为，共和党领导人必须在向公众传达的信息中明确否定选举阴谋和暴力，以结束这一切。 我们是否进入了一个政治暴力的新时代? 肖恩·林2021年1月7日|Vox 
 “我们拒绝、谴责和谴责古巴政府机构在对话和承认异议、活动人士自治、赋予少数群体权力以及尊重人权和公民权利方面的无能，”文件说。 圣伊西德罗的绝食抗议，不让哈瓦那睡觉|Maykel González Vivero | 2020年11月30日|华盛顿刀工 
 这可能会影响民主党人推行更激进议程的程度，以及共和党人在多大程度上认为特朗普主义已被否定。 在竞选的最后15天里保持清醒的8个建议|内特·西尔弗(nrsilver@fivethirtyeight.com) | 2020年10月18日|FiveThirtyEight 
 ProPublica的responseProspect误导性地暗示，这个“一个消息来源”否定了我们文章中所有归因于他的多条声明。 一家连锁医院说我们的文章不准确。 它不是。 | 2020年10月12日|ProPublica 
 如果这样的政策或做法存在，它违反了抗议者的第四修正案和正当程序权利，应该立即废除和否定。 晨报:巴里奥斯的另一个潜在违规|Sara Libby | 2020年9月25日|圣地亚哥之声 
 证词包括两名被告，萨拉姆和怀斯，他们出庭否认自己的供词。 中央公园五人组的神话爱德华·康伦| 2014年10月19日|每日野兽 
 但美国可以与叶利钦合作，尽管他选择了一个会“否定他的遗产”的继任者。 “弗拉基米尔·普京都怪这只喝醉的熊|埃莉诺·克利夫特| 2014年4月22日|每日野兽 
 他们会做出一项勇敢的行为，推翻德国60年来的战争罪行判决吗? 如何尝试纳粹|理查德·拉什克| 2013年9月6日|每日野兽 
 加拿大必须否定冲突双方的极端主义。 加拿大如何应对僵尸威胁? 大卫·弗拉姆2013年2月16日|DAILY BEAST 
 我不仅不把自己归为“出生论者”，我还特别用两本书和这部电影否定了“出生论者”的论点。 2012年9月4日，梅根·麦凯恩与“2016:奥巴马的美国”导演迪尼希·德索萨交谈 
 我的老师——《瑞典宪法》的作者——理想的革命家——他会谴责我，用行动否定宣传吗? 无政府主义者监狱回忆录|亚历山大·伯克曼 
 但是你，我的普罗旺斯啊，不要为那些不认你和否定你的言论的儿子们而烦恼。 查尔斯·阿尔弗雷德·唐纳 
 他不是被呼召去否定偶像崇拜，而是奉神的命令去一个未知的国度。 历史灯塔之光，卷二|约翰·洛德 
 经常发生的情况是，人民太容易跟随煽动家，拒绝和嘲笑诚实的改革者。 两大共和国:罗马和美国|詹姆斯·汉密尔顿·刘易斯 
 布鲁克纳姆太太没有否认这个幻象，这似乎就足够了，她的客人高兴地又跳了一跳。 尴尬时代|亨利·詹姆斯