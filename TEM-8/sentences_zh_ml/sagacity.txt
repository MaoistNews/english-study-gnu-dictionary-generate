演讲者会联想起几个世纪以来的集体智慧，把自己与永恒的、无可争辩的善联系在一起。 政客们只爱死了的记者卢克·奥尼尔| 2015年1月8日|每日野兽 
 杰伦·拉尼尔(Jaron Lanier)非常睿智地写过和谈论过这个问题。 书的未来|Sam Harris | 2011年9月27日|DAILY BEAST 
 他们宣布，新搜索引擎的名字将是:必应(Bing)。 微软窃取了我的身份! 2009年5月28日|DAILY BEAST 
 罗兰夫人的迅速回答显示出她的聪明才智。 罗兰夫人，历史的创造者|约翰S. C.阿伯特 
 他对罗兰夫人的才华、精力和智慧表示了最深切的钦佩。 罗兰夫人，历史的创造者|约翰S. C.阿伯特 
 卡尔密特的判断力和洞察力是人所共知的，他说她可能是他的侄女。 《哲学词典》第一卷(10卷)|弗朗索瓦-玛丽·阿鲁埃(伏尔泰) 
 人类的智慧无法解释今天存在的这些事实，更不能在三千年前预言它们。 福音哲学| J。 h·沃德 
 因此，对智慧的模仿变成了自命不凡的矫揉造作。 Jean de La Bruyre的“人物”|Jean de La Bruyre