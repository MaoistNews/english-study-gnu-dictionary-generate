情绪使大脑以一种更强大、更持久的方式储存记忆，用粗体字和斜体字记录下来。 你不能完全相信你的记忆|大卫·林登| 2020年9月30日|大众科学 
 注意，文本格式(粗体或斜体)在本卷中具有语义意义。 选自早期中古英语1130-1250:第二部分:注释|各种 
 还有人讨厌破折号、冒号或引号，还有人不愿在他的作品中使用斜体字。 书的构建|多种多样 
 永远不变的命题，满足路德教的斜体。 大改革史，第四卷|J。 h·默尔D 'Aubign 
 奎利诺斯(Quirinus)，名词，一个意大利的神，与被神化的罗穆卢斯(Romulus)一致。 钱伯斯的《二十世纪词典》(第四部分:N-R 
 在北极有一个小时圈，上面刻着“索引霍尔:斜体”。 天地地球仪第二卷|爱德华·卢瑟·史蒂文森