例如，那些被切割的矩形可以很容易地拼接在一起，形成二维的马赛克。 科学家发现地质的普遍几何|约书亚·索科尔|020年11月19日|量子杂志 
 我认为，这些社交媒体图像的传播在很大程度上要对大胆图案的瓷砖马赛克和水泥地板成为家居设计中日益增长的趋势负责。 被社交媒体上大胆的瓷砖迷住了吗? 以下是在选择这种造型之前需要考虑的事项。 2020年10月29日|华盛顿邮报 
 你面对的不是地图，而是看似随机而复杂的信号马赛克。 我们令人难以置信的嗅觉-第91期:神奇的大脑|Ann-Sophie Barwich | 2020年10月14日|鹦鹉螺 
 想要获得更大的控制，不妨试试Mosaic——你可以免费使用它一周，之后将花费你13美元。 充分利用您的双显示器或超宽显示器设置|David Nield | 2020年10月1日| 
 它可以让你为你的程序绘制一个特定的网格区域，你有更多的空间玩，马赛克变得更有用。 充分利用您的双显示器或超宽显示器设置|David Nield | 2020年10月1日| 
 海军部的列侬墙是一幅便利贴的马赛克，每一方格都写着一个愿望。 占领香港纪念碑人|Brendon Hong | 2014年12月4日|DAILY BEAST 
 在她看来，这幅马赛克也更像是公元前1世纪的作品，而不是公元前4世纪的作品。两栖波利斯墓有惊人的发现，但谜团挥之不去 
 结果是光谱的马赛克，覆盖了多达60个不同的区域。 SAMI就像宇宙的地球|Matthew R. Francis | 2014年7月27日|DAILY BEAST 
 像THEMIS马赛克这样的现代全球调查是我们了解一个陌生世界的一种方式。 迄今为止最好的火星地图|Matthew R. Francis | 2014年7月20日|每日野兽 
 这就是我们今天所知道的马赛克情节，这在一千年前就已经存在了。 下一个联合国教科文组织世界遗产:勃艮第的黑比诺之乡? Jordan Salcito 2014年5月31日|DAILY BEAST 
 在摩西的分配结束之前，他一生的义务不能减少。 契约条例|约翰·坎宁安 
 主入口是一个开放的房间，最初是拱形的，地板是黑白马赛克和砖石墙。 罗马地下墓穴|威廉·亨利·威斯罗 
 高拱的庙宇，中心立着石棺，镶嵌着稀有石头的马赛克作品。 一个女人的环球旅行|艾达·菲佛 
 现在星星成千上万地出现了，金色的马赛克镶嵌在高高的紫色圆顶上。 龙画家玛丽·麦克尼尔·菲诺洛萨 
 罗马马赛克是用各种颜色的大理石方块压在花柱上形成的。 大英百科全书，第11版，第4卷，第3部分|各种