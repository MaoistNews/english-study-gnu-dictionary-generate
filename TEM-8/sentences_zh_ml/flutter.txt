然而，把响亮和缓慢放在一起，结果往往会在我们的脑海中感到沉重和过度，即使我们只是在体验无重力的空气颤动。 Eyehategod的高重力金属可以连接你到天堂|克里斯·理查兹| 2021年3月12日|华盛顿邮报 
 在大都会博物馆的舞台上可能会有幽灵在飞舞。 《当斯大林遇见麦克白夫人》|Brian Moynahan | 2014年11月9日|每日野兽 
 法罗微笑着，蝴蝶飞舞，星星划过夜空。 不要违抗罗南·法罗的力量撅嘴|蒂姆·蒂曼| 2014年2月25日|每日野兽 
 事情一开始很小:这里有一点烦恼，那里有一点怀疑。 快杀了贝茨先生! 如何拯救《唐顿庄园》|Andrew Romano | 2014年2月20日|DAILY BEAST 
 那里没有红地毯，你只会在看电影时飘飘然。 凯瑞·穆ligan，《醉梦民谣》主演，在科恩兄弟、超级男孩粉丝圈、洛德和更多|Marlow Stern | 2013年12月3日|DAILY BEAST 
 突然间，她走了，留下一阵红窗帘的飘动。 尼泊尔新旧:加德满都谷地的皇家城市焕然一新|Condé纳斯特旅行者| 2013年8月19日|每日野兽 
 它们通常在任何物体的最高点摆动两到三分钟，然后消失。 一个女人的环球旅行|艾达·菲佛 
 “哦，我已经把它拿出来了，在它后面摸了摸，”卡灵顿小姐催促道，这时她心里一阵慌乱。 舞台上的中央高中女孩|格特鲁德·w·莫里森 
 她的声音中有一种绝对的真诚，夹杂着恐惧，他张开双臂，让她飞走了。 轮子上的蝴蝶|西里尔·阿瑟·爱德华游骑兵海鸥 
 这就是为什么它们有黑色的翅膀和尾巴，为什么它们如此高兴地扑腾，为什么它们永远唱不完它们的歌。 易洛魁人给孩子讲的故事|梅布尔·鲍尔斯 
 他几乎以为一张支票会飘飘荡荡地掉在地上; 但是，可惜的是，没有一点动静。 爱的朝圣|厄普顿辛克莱