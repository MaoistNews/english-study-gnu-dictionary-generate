硅谷有权利的员工的抱怨亚当·拉辛斯基，《财富》杂志，2020年9月8日 
 目瞪口呆，不知所措，捶打得说不出话来。 俚语词典|约翰·卡姆登·霍顿 
 他对印加的事务如此熟悉，连老男孩都吓呆了。 无线电男孩寻找印加宝藏|杰拉尔德·布雷坎里奇《大英词典》对dumbfound的解释:dumbfound dumbfound / (dʌm faʊnd) /动词(tr)使目瞪口呆; dumbfound C17的词源:from dumb + (con) found Collins English Dictionary - Complete & unridedded2012数字版©William Collins Sons & Co. Ltd. 1979,1986©HarperCollins Publishers 1998,2000,2003,2005,2006, 2007,2009, 2012