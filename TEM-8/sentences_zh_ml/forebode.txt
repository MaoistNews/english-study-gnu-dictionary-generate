现在，随着一代人多来首次出现真正的通货膨胀，潜伏的恐惧被唤醒了，恐惧不仅抓住了我们的现实感，还抓住了我们对未来的预感。 通胀可能已经见顶。 过度反应会带来风险扎卡里·卡拉贝尔，2022年5月13日 
 危机的现实并不像米尔曼最初描述的那么不祥。 为什么你应该关心“昆虫危机”|艾丽·威尔金森| 2022年4月26日|科学新闻 
 在一片闪烁的绿色和蓝色灯光的海洋中，一整架电脑突然闪起了黄色，然后，几秒钟后，不祥的红色出现了。 云的物理足迹内部|Steven Gonzales Monserrate/ MIT Press Reader | 2022年2月14日|大众科学 
 每家法院都在改变日程安排，将大量案件推进到审判阶段，这让他有一种不祥的预感。 在改革呼声中，缅因州的刑事辩护系统达到了“突破点”，萨曼莎·霍根，《缅因州观察家报》，2021年6月8日，| 
 它们似乎都代表着某种门或入口——黑暗的、不朽的、不祥的。 这些罗斯科的作品是如何在没有碰过画布的情况下被修复的 
 这种颜色也被用来预示预兆或威胁。 在圣帕特里克节，小心那些狡猾的绿色眼睛|劳拉·戴蒙| 2013年3月17日|每日野兽 
 克里斯蒂安国王的信引起了轩然大波，这预示着除了武力解决外，没有别的办法。 19世纪的历史，年复一年|埃德温·爱默生 
 用坡的话说，从她的历史中，部落的阴影将永远不会消失。 俄国帝国的崛起|赫克托·h·门罗 
 迫害我的人的威胁似乎预示着这个系统将不可避免地中断。 凯莱布·威廉姆斯威廉·戈德温 
 这种深沉的宁静引起了黑熊的怀疑; 这似乎预示着一场暴风雨即将来临。 《杀虎人》古斯塔夫·艾马尔 
 也许吧——但她的语气并不预示着会有愉快的谈话。 马克森夫人抗议安东尼·霍普