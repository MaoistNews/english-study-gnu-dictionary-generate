The insidious servant becomes a tyrannical master; the housebreaker is innocent, the horse thief guiltless in comparison. Etidorhpa or the End of Earth. |John Uri Lloyd 
"Take that burly housebreaker, bind him, and put him to the test," were the instructions they received. The Mysteries of All Nations |James Grant 
The darkness of the upper hallway offered no obstacle to this familiar housebreaker. The Oakdale Affair |Edgar Rice Burroughs 
Six pairs of eyes, blear or foxy, were riveted upon the boyish figure of the housebreaker. The Oakdale Affair |Edgar Rice Burroughs 
But surely madame la princesse must appreciate the police might be at a loss to know which housebreaker to arrest. Red Masquerade |Louis Joseph Vance