That, America, is the quintessence of naturally occurring British-cute. UK’s No 1 Churchman Doubts Existence of God: The Archbishop of Canterbury Thinks Deep When Running With His Dog |Tim Teeman |September 18, 2014 |DAILY BEAST 
The actor is the quintessence of smooth, first as Remington Steele, then James Bond. Pierce Brosnan’s Life After Bond: From Action Hero to Losing His Daughter to Cancer |Tim Teeman |July 2, 2014 |DAILY BEAST 
In recent years, Stoner entered a category of which it soon became the quintessence. Famous for Not Being Famous: Enough About ‘Stoner’ |Drew Smith |October 31, 2013 |DAILY BEAST 
Yes, Holmes was the quintessence of the Victorian rationalism, “the most perfect and reasoning machine that the world had seen.” How Sherlock Holmes Took on the Capitalists |Ian Klaus |December 21, 2011 |DAILY BEAST 
“Innocence is the quintessence of the snapshot,” Lisette Model would write. Robert Frank's America |Philip Gefter |September 17, 2009 |DAILY BEAST 
Preserve this precious quintessence of mercury, which is exceedingly volatile, in a well-closed vessel for further use. Witch, Warlock, and Magician |William Henry Davenport Adams 
This drama is the quintessence of all that Gorky has, up to this time, written on the "ex-man," whom he has thoroughly "explored." Contemporary Russian Novelists |Serge Persky 
Only as a subscriber, possessing a numbered set of a limited edition, could one obtain the quintessence of literature. Bizarre |Lawton Mackall 
They are the quaint quintessence of conservatism, and will occupy youthful minds menaced by modernism. Bizarre |Lawton Mackall 
Ten beds, and only one occupied, by a freckled, tousled quintessence of fractiousness in a blue wrapper. Pippin; A Wandering Flame |Laura E. Richards