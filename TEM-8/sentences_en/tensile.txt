Others worried about the tensile strength of all that glass. The metaverse is a new word for an old idea |Genevieve Bell |February 8, 2022 |MIT Technology Review 
It knocks the wind out of you even before it makes you think—though it does makes you think, too, especially about the tensile fierceness of refusing victimhood. Promising Young Woman Starts with a Cathartic Blast. Then It Gets Bogged Down With Cynicism |Stephanie Zacharek |January 15, 2021 |Time 
Its average length is about twenty inches, and its tensile strength is superior to that of cotton. Textiles |William H. Dooley 
It lacks the tensile strength and elasticity, and is of higher specific gravity than true silk. Textiles |William H. Dooley 
What is very high tensile strength in boiler iron apt to go with? Farm Engines and How to Run Them |James H. Stephenson 
In the first place, it gives the steel a greater tensile strength. United States Steel |Arundel Cotter 
Nickel is about ten per cent heavier than steel, and has a tensile strength of 90,000 pounds per square inch. Oxy-Acetylene Welding and Cutting |Harold P. Manly