It wasn’t just his having mentored with Roy Cohn, the infamous Mafia lawyer, or rubbed shoulders throughout his career with guys like Anthony “Fat Tony” Salerno. What My Mobster Grandfather Understood About American Capitalism |Russell Shorto |March 17, 2021 |Time 
If they aren’t exposed and discredited, we may witness the rise of a new Mafia. What My Mobster Grandfather Understood About American Capitalism |Russell Shorto |March 17, 2021 |Time 
Magashule does have a bit of a dirty reputation, with the book Gangster State comparing him to a Mafia don. OZY Highlights: Tool Up for a Better Tomorrow |Tracy Moran |November 22, 2020 |Ozy 
Like the Mafia Dons of old, they will be brought down by the IRS for their financial crimes. My Dad served in WWII — he was a hero, not a loser |Peter Rosenstein |September 10, 2020 |Washington Blade 
For those who have forgotten their Mafia history, that’s the Racketeer Influenced and Corrupt Organizations Act. When Did Gavin McInnes Lose His Mind? |Eugene Robinson |September 6, 2020 |Ozy 
In the Latino community the legacy of the Mexican Mafia spans multiple generations. The Mexican Mafia Is the Daddy of All Street Gangs |Seth Ferranti |December 11, 2014 |DAILY BEAST 
The Mexican Mafia run their empire from the penitentiary to the streets of LA. The Mexican Mafia Is the Daddy of All Street Gangs |Seth Ferranti |December 11, 2014 |DAILY BEAST 
Robert Morrill, a former gang detective and author of The Mexican Mafia/The Story, said there is little the authorities can do. The Mexican Mafia Is the Daddy of All Street Gangs |Seth Ferranti |December 11, 2014 |DAILY BEAST 
Much like the TV show Breaking Bad, the Mexican Mafia are real life methamphetamine kingpins. The Mexican Mafia Is the Daddy of All Street Gangs |Seth Ferranti |December 11, 2014 |DAILY BEAST 
As described by its inventor, there is an Honest team and a Mafia team that compete against each other. Where Chechens Go to Escape Their Surreal Past—and Risky Present |Anna Nemtsova |December 9, 2014 |DAILY BEAST 
The Mafia is more ancient and has a direct ancestry for nearly a thousand years. Italian Highways and Byways from a Motor Car |Francis Miltoun 
Anyhow, he defied the Mafia, laid in a stock of revolvers and rifles, and rallied his friends around him. Courts and Criminals |Arthur Train 
The Mafia might almost be called a universal conspiracy against law and order. Chambers's Journal of Popular Literature, Science, and Art |Various 
Either the Cammoristi, or the Mafia, or some such organisation. A Cabinet Secret |Guy Boothby 
Don't act this way with these people, because they are all of the (Mala-vita) Mafia and will do you harm in an instant. The Barrel Mystery |William J. (William James) Flynn