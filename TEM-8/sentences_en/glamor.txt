She grew up in New York City, a fan of old Hollywood glamor and fashion, which eventually led to her professional career. Barbara Tfank: The Red Carpet Radical |Erin Cunningham |March 2, 2014 |DAILY BEAST 
The glamor couple won't be without things to keep them busy. The Cheating German Baron |Stefan Theil |March 2, 2011 |DAILY BEAST 
The glamor of the situation, with his father as the recognized champion of labor, fitted smoothly into his own rebellious dreams. Mountain |Clement Wood 
It came upon him suddenly that the sweet witchery, the glamor falling over him was—love. Jolly Sally Pendleton |Laura Jean Libbey 
Little in touch with the true spirit of Christianity, it was easily led by the glamor of resounding phrases and classical figures. The War Upon Religion |Rev. Francis A. Cunningham 
She saw all in the clear light of reason, not in the glamor of love, and her judgment condemned them both. Marion Arleigh's Penance |Charlotte M. Braeme 
Strange is a Celtic landscape, far more moving, disturbing than the lovely glamor of Italy and Greece. Sea and Sardinia |D. H. Lawrence