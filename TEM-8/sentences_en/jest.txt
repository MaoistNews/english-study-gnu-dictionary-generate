Moves like these, even if mentioned in jest, could hurt the company’s reputation and culture, analysts say, and will likely make it harder for Twitter’s board to sell to him. Here’s What Wall Street Thinks About Elon Musk’s Bid to Buy Twitter |Nik Popli |April 15, 2022 |Time 
Sometimes when backpacking, we’d yip in jest, and she’d throw her head back and join in with a heart-rattling howl. I Was a Bad Dog Owner. Don’t Be Like Me. |Kate Siber |May 11, 2021 |Outside Online 
Yes, some Losers have complained to me — only sometimes in jest — about being forced to read enormous amounts of news coverage for 11 straight days. Style Conversational Week 1433: Pranks for asking |Pat Myers |April 22, 2021 |Washington Post 
This is the experience that some people—not I, mind you, but some people—have when reading Infinite Jest, for instance. Lars Iyer’s ‘Wittgenstein Jr.’ Plumbs the Deep Fun of Philosophical Fiction |Drew Smith |October 1, 2014 |DAILY BEAST 
After a while, he began to jest that there were snipers across the street trying to get him. Doug Kenney: The Odd Comic Genius Behind ‘Animal House’ and National Lampoon |Robert Sam Anson |March 1, 2014 |DAILY BEAST 
Rawcus has to provide a solid argument in his rhetoric to make such an accusation, even in jest. Rawcus Is the Rapper Behind the Viral ‘White People Crazy’ Video |Rich Goldstein |January 29, 2014 |DAILY BEAST 
And this explosion was fictional and in jest, so you can call it a pretty big hoot! ‘American Hustle’ Is Overrated |Kevin Fallon |January 28, 2014 |DAILY BEAST 
It may have been said in jest, but a legion of Hollywood actresses would have nodded ruefully in unison. Why Meryl Is So Special |Tim Teeman |January 16, 2014 |DAILY BEAST 
I've tried to teach lots of folks; an' sum learns quick, an' some don't never learn; it's jest 's 't strikes 'em. Ramona |Helen Hunt Jackson 
I allow we shan't never set eyes on ter her, Jos. I've got jest thet feelin' abaout it. Ramona |Helen Hunt Jackson 
I never see sech hosses; 'n' they're jest like kittens; they've ben drefful pets, I allow. Ramona |Helen Hunt Jackson 
I ain't a gwine ter back daown naow; but I dew jest wish Jeff Hyer wuz along. Ramona |Helen Hunt Jackson 
But I allow they'd flatten us all aout in jest abaout a minnit, if they wuz to set aout tew! Ramona |Helen Hunt Jackson