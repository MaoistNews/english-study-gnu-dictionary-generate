The more antibiotics are used inappropriately, the greater the risk of bacteria growing resistant to them. Without Education, Antibiotic Resistance Will Be Our Greatest Health Crisis |Russell Saunders |December 19, 2014 |DAILY BEAST 
What specific bacteria and viruses can be detected in the sewage? The Secret to Tracking Ebola, MERS, and Flu? Sewers |Wudan Yan |November 29, 2014 |DAILY BEAST 
If all animals vanished, most bacteria would still live on, but if all bacteria disappeared, we would die quickly. Why Did It Take So Long For Complex Life To Evolve On Earth? Blame Oxygen. |Matthew R. Francis |November 2, 2014 |DAILY BEAST 
A new book focuses on gut bacteria as the key to a healthy weight. ‘Good Poop’ Diet Is the Next Big Thing |Daniela Drake |October 7, 2014 |DAILY BEAST 
But consider that when we eat, the bacteria are actually fed first. ‘Good Poop’ Diet Is the Next Big Thing |Daniela Drake |October 7, 2014 |DAILY BEAST 
Bacteria, when present in great numbers, give a uniform cloud which cannot be removed by ordinary filtration. A Manual of Clinical Diagnosis |James Campbell Todd 
It is to be remembered, however, that a few of these bacteria may reach the sputum from the upper air-passages. A Manual of Clinical Diagnosis |James Campbell Todd 
Bacteria of various kinds, especially staphylococci, are usually numerous. A Manual of Clinical Diagnosis |James Campbell Todd 
They are able to migrate readily from place to place and to ingest small bodies, as bacteria. A Manual of Clinical Diagnosis |James Campbell Todd 
Undiluted normal blood can agglutinate most bacteria, but loses this power when diluted to any considerable degree. A Manual of Clinical Diagnosis |James Campbell Todd