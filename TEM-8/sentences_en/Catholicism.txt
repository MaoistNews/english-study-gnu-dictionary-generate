An excellent argument against Roman Catholicism I would say, and Greene might well stare in blank incomprehension at my pages. The Thief of Words: Starling Lawrence |Jill Bialosky |October 11, 2013 |DAILY BEAST 
Such revelations seem like the dream come true of know-nothing anti-Catholicism. The Vatican's Sex Cover-up |James Carroll |March 11, 2010 |DAILY BEAST 
As the saying goes, anti-Catholicism is the anti-Semitism of the intellectual class. The Audacity of Poping |Christopher Buckley |March 26, 2009 |DAILY BEAST 
That's another story, and Doubt limns the particular joylessness of Irish Catholicism. We Miserable Catholics |Frank McCourt |December 19, 2008 |DAILY BEAST 
Roman Catholicism in England has shown a tendency to advance, especially among the upper and upper-middle classes. Encyclopaedia Britannica, 11th Edition, Volume 9, Slice 4 |Various 
The oaths were based on a real fear which identified Roman Catholicism with treason. Virginia Under Charles I And Cromwell, 1625-1660 |Wilcomb E. Washburn 
Wolken of Ratisbon, a convert to Roman Catholicism in the second half of the fifteenth century. Some Jewish Witnesses For Christ |Rev. A. Bernstein, B.D. 
The history of German Catholicism proves once more that the Church is never more admirable than when she is persecuted. German Problems and Personalities |Charles Sarolea 
Treitschke has fought Roman Catholicism and its champions, the Jesuits, with relentless hate. German Problems and Personalities |Charles Sarolea