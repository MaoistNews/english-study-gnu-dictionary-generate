The man who removed the dolphin from the Indus River that Jabbar and his colleagues were trying to save told a judge he was responding to an impromptu dare by a friend. The Unlikely Dolphin Rescue Hot Spot: Pakistan |Charu Kasturi |February 2, 2021 |Ozy 
In defiance, I held my ticket above my head, which triggered the spitting and chants of “How Dare You!” Inside the Metropolitan Opera’s Insane Year |Shawn E. Milnes |November 23, 2014 |DAILY BEAST 
Despite the 21 years I did in prison for a drug conviction, I am assimilating back into mainstream or, dare I say, white America. Ferguson Tensions in Black and White |Seth Ferranti |November 21, 2014 |DAILY BEAST 
We feel their strangeness when we read their words—they lived on a plane where few dare to tread. Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 
While it may not leave you with many profound truths, I dare you not to fall in love. This Week’s Hot Reads: October 27, 2014 |Charles Shafaieh |October 27, 2014 |DAILY BEAST 
He adds: “None of the fighters will dare touch it, if an emir has given permission.” U.S. Humanitarian Aid Going to ISIS |Jamie Dettmer |October 20, 2014 |DAILY BEAST 
None other would dare to show herself unveiled to a stranger, and a white man at that. The Red Year |Louis Tracy 
I never dare venture over except as the guest of some more fortunate friend. Ancestors |Gertrude Atherton 
I do not know—I do not dare to believe—that I shall live to hear that key grating in the lock. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
She would not dare to choose, and begged that Mademoiselle Reisz would please herself in her selections. The Awakening and Selected Short Stories |Kate Chopin 
For accurate work the best instruments are the von Fleischl-Miescher and the Dare. A Manual of Clinical Diagnosis |James Campbell Todd