The good story formula, though, requires a structural us-and-them divide. T.E. Lawrence Rides Again in Scott Anderson’s New History |Melik Kaylan |August 7, 2013 |DAILY BEAST 
The culture of conflict bred an us-against-them identity among the church members. The Westboro Defectors Speak: Phelps Granddaughters Embrace Tolerance |John Avlon |March 8, 2013 |DAILY BEAST 
This didn't come naturally to them/us, who would much rather talk about health care as a right. Dems: Starting to Get It, But As Usual Not Quite Right |Michael Tomasky |July 2, 2012 |DAILY BEAST 
She continued her us-versus-them approach to political discourse. Palin Goes Nuclear With 'Blood Libel' Speech |Howard Kurtz |January 12, 2011 |DAILY BEAST 
All this contributes to a broader us-versus-them conflict over values. The Real McChrystal Story |Leslie H. Gelb |June 22, 2010 |DAILY BEAST 
The English kings had turned to the only people who could have helped them-the Normans settled in Ireland; and they failed them. Is Ulster Right? |Anonymous 
No sooner did he say the word than he and the Weasel found them-selves standing before the open door of a nice little house. The King of Ireland's Son |Padraic Colum 
Shall the dead take thought for the dead to love them?What love was ever as deep as a grave? The Ontario Readers: The High School Reader, 1886 |Ministry of Education 
After this the Kafirs gave Savage a second very long name which meant "He-who-sits-down-on-snakes-and-makes-them-flat." The Ivory Child |H. Rider Haggard 
The only additional suggestion is the new and original test, the so-called "em-them" test. The Critics Versus Shakspere |Francis A. Smith