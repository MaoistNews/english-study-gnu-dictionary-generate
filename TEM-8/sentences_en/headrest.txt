Mom leans her head back against the wheelchair’s headrest to gaze up toward the tops of the redwoods. Adventuring with My Disabled Mom Healed Me, Too |Tessa Fontaine |March 2, 2021 |Outside Online 
If they don’t want to lug one of the memory-foam headrests on their bed at home through airports, a neck pillow will serve as a good substitute in a pinch. The best gifts for travelers you care about |PopSci Commerce Team |February 24, 2021 |Popular-Science 
Adjustable height, headrest, and armrests are also key to finding a comfortable desk chair. Best desk chair for any home office |PopSci Commerce Team |February 11, 2021 |Popular-Science 
Ray, a short man, had added a 2-by-4 to his seat to make the headrest hit right. A CIA spyplane crashed outside Area 51 a half-century ago. This explorer found it. |Sarah Scoles |January 5, 2021 |Popular-Science 
The removable headrest is cured at 135 degrees to support your head and neck. Great office chairs to stop the slump and create comfort |PopSci Commerce Team |November 24, 2020 |Popular-Science 
He sat down in his chair, laid his head back in the headrest and pulled the helmet down over his forehead. The Game of Rat and Dragon |Cordwainer Smith 
He stepped in and gripped the bat-shaped gearshift, adjusted the headstone-shaped headrest, and got rolling. Makers |Cory Doctorow 
He was hurled back against the headrest with a force that filled his head with winking stars. Dave Dawson at Casablanca |Robert Sydney Bowen 
Eugene put his hand on ol man Dorts forehead an pushed him back into the headrest. Friar Tuck |Robert Alexander Wason 
Feather pillows and beds took the place of straw pallets with a log of wood for a headrest. Women of England, Volume 9 (of 10) |Burleigh James Bartlett