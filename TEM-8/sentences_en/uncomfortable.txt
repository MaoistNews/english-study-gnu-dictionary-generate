I try not to bring up anything positive about my life with her, because then she says things that make me uncomfortable. Miss Manners: Time to cut ties with longtime friend? |Judith Martin, Nicholas Martin, Jacobina Martin |February 11, 2021 |Washington Post 
The second group would be paid more if they made their partner uncomfortable. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 
Those things forced the GOP into very uncomfortable conversations about what their party should be. The GOP: the new know-nothing party |Aaron Blake |February 4, 2021 |Washington Post 
The fact that I had basically gone my entire life without ever even touching a dead body meant that I had a very uncomfortable relationship being in the presence of a dead body. How to Be Better at Death (Ep. 450) |Maria Konnikova |February 4, 2021 |Freakonomics 
Any type of wetness will increase the rate at which heat leaves your body and will be uncomfortable. Best winter gloves: Our picks for touch screen gloves, ski gloves, and more |PopSci Commerce Team |February 2, 2021 |Popular-Science 
Even in the medieval era this disparity made Christians uncomfortable. Oops! Jesus’ Last Steps Are in the Wrong Place |Candida Moss |January 6, 2015 |DAILY BEAST 
ThinkProgress calls the premise “uncomfortable and vaguely sad.” Your Husband Is Definitely Gay: TLC’s Painful Portrait of Mormonism |Samantha Allen |January 1, 2015 |DAILY BEAST 
That is a distinction with a sociological difference—for many, an uncomfortable one to consider. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 
Because holy hell was that bland, unfunny, uncomfortable, and just plain confusing. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 
He said he thought his investigation was “too uncomfortable.” Victim: I Watched British MPs Rape and Murder Young Boys |Nico Hines |December 18, 2014 |DAILY BEAST 
Bernard was uncomfortable enough not to care to be mocked; but he felt even more sorry that Gordon should be. Confidence |Henry James 
Shortly after she came to her lesson limping, and remarked that she felt very uncomfortable. Children's Ways |James Sully 
It took me back to Burma and a certain very uncomfortable night that I once passed in the jungle. Uncanny Tales |Various 
It stands up so high at the back of her neck I should think it would feel very uncomfortable. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 
But there was something about the chilly expression in her eyes that made Delancy feel decidedly uncomfortable. Hooded Detective, Volume III No. 2, January, 1942 |Various