He was practicing holding his body in a crouch, with his arms straight out behind, before jumping skis-and-all into the air. U.S. Ski Jumping Is Looking For More Friends In High Places |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |February 18, 2022 |FiveThirtyEight 
He bends down into the crouch that raisin farmers assume when they are about to examine their crop. How we drained California dry |Mark Arax |December 16, 2021 |MIT Technology Review 
Deneen believes that conservatism is in a “defensive crouch” — and has been so “since its rise in the 20th century.” The intellectual right’s war on America’s institutions |Zack Beauchamp |November 19, 2021 |Vox 
It moves in a half crouch, dragging one knee along the ground. AI is learning how to create itself |Will Douglas Heaven |May 27, 2021 |MIT Technology Review 
It’s the first time reinforcement learning has been used to teach a two-legged robot how to walk from scratch, including the ability to walk in a crouch and while carrying an unexpected load. Forget Boston Dynamics. This robot taught itself to walk |Will Douglas Heaven |April 8, 2021 |MIT Technology Review 
Eyes red and prison muscles bulging, a tattooed white man behind me jumped to his feet from a crouch and swatted me aside. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 
Democrats can't slink away, or crouch, or cut and run against their own record. Democrats Must Run on Obamacare in November |Robert Shrum |March 17, 2014 |DAILY BEAST 
Don dropped the skillet, jumped into a crouch, went for his gun. The Ballad of Johnny France |Richard Ben Cramer |January 12, 2014 |DAILY BEAST 
There was barely room to crouch, let alone lie down and sleep. Immigrants Held in Border Deep Freezers |Rachael Bale, The Center for Investigative Reporting |November 19, 2013 |DAILY BEAST 
Malheiro says that whoever claimed Crouch confirmed the cases of Krokodil “got her statements wrong.” Behind the Krokodil Panic |Abby Haglage |November 7, 2013 |DAILY BEAST 
Now it seemed to crouch as though ready to spring, and I could hear the savage growling as of some beast of prey. Uncanny Tales |Various 
I straightened out of my crouch, forced myself not to reveal what I had just seen. Hooded Detective, Volume III No. 2, January, 1942 |Various 
She continued to crouch on the steps, holding her breath and stiffening herself into complete immobility. Summer |Edith Wharton 
He raised his saber in salute—the only fencing-movement he'd become proficient in—and jumped into a crouch. The Great Potlatch Riots |Allen Kim Lang 
So there she sat, ready to crouch down into her hiding-place, if she heard a noise from her enemy. Stories about Animals: with Pictures to Match |Francis C. Woodworth