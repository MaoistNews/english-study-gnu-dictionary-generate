The ability to work remotely filled my days, and large windows with a beautiful view helped to ward off claustrophobia. I Saw Firsthand What It Takes to Keep COVID Out of Hong Kong. It Felt Like a Different Planet. |by Caroline Chen |January 6, 2022 |ProPublica 
What “Framing Britney Spears” evokes so viscerally is the claustrophobia and frustration of being Britney Spears. Britney Spears and the trauma of being young, female and famous in the ’90s |Ashley Fetters |February 5, 2021 |Washington Post 
When I stepped into his 6- by 13-foot cell, I had instant claustrophobia. Here’s a Reform Even the Koch Brothers and George Soros Can Agree On |Tina Brown |November 10, 2014 |DAILY BEAST 
Arachnophobia is an irrational fear of spiders and claustrophobia is an irrational fear of small places. Ground Zero Mosque Hurts Islam |Douglas Murray |August 12, 2010 |DAILY BEAST 
You hear stories about going loopy from claustrophobia and stuff. The Planet Strappers |Raymond Zinke Gallun 
The cigar was his protection, his secret weapon, against the claustrophobia the mosquitero gave him. The Five Arrows |Allan Chase 
I know it's horrible—many of our visitors suffer claustrophobia, but they just must be built that way. The Brain |Alexander Blade 
Claustrophobia is the malady of those who have a horror of close quarters from which they can not easily make their escape. Poise: How to Attain It |D. Starke 
To-day he was feeling the claustrophobia of London more acutely than usual. H.M.S. ---- |Klaxon