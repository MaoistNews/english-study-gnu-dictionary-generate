By the way, this article is not only impious and damnable, it's also almost certainly illegal to read or publish it in Gaza. Hamas: The Palestinian Fashion Police |Hussein Ibish |April 9, 2013 |DAILY BEAST 
It would be impossible, as well as impious, for men to imitate the making of the Covenant of Redemption, or of that of Works. The Ordinance of Covenanting |John Cunningham 
The old Don Luis shows his whitened locks, scorned by his hypocritically impious son. Charles Baudelaire, His Life |Thophile Gautier 
Impious people, you say, deprived of the flattering hopes of another life, desire to be annihilated. Superstition In All Ages (1732) |Jean Meslier 
To be impious, is to have unjust opinions about the God who is adored; to be superstitious, is to have false ideas of Him. Superstition In All Ages (1732) |Jean Meslier 
In March 1649, a quartermaster named Boutholmey was tried by council of war for uttering impious expressions. A Cursory History of Swearing |Julian Sharman