The Lost PaintingBy Jonathan Harr Not all of us have lost paintings by Caravaggio in our parlour. Alexander McCall Smith’s Art Book Bag |Alexander McCall Smith |September 4, 2012 |DAILY BEAST 
The wind is howling, and the rain is pelting against the parlour windows of the Banking-house, whose blinds are drawn close down. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
He just got a good holt–a shore enough diamond hitch–on that thirst-parlour dawg, and chawed. Alec Lloyd, Cowpuncher |Eleanor Gates 
On his return he introduced them on the Midland, both the parlour car and the sleeper. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 
Like the cobbler's stall in the old song, it served the present occupants for "kitchen and parlour and all." The World Before Them |Susanna Moodie 
But English suburban parlour-maids are on their guard against smiles, no matter how engaging. The Joyous Adventures of Aristide Pujol |William J. Locke