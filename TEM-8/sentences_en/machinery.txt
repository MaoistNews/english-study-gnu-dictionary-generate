Anti-vaxxers have also seized on the fact that some developers are using a relatively new technology called messenger RNA that attempts to alter the body’s protein-making machinery. Liberal, Educated … and Anti-Vaxxer: Pandemic Births New Vaccine Doubters |Charu Kasturi |August 25, 2020 |Ozy 
It looks circular, but this is because of some impressive perceptual machinery in your mind. This Vision Experiment Resolved a Centuries-Old Philosophical Debate - Facts So Romantic |Jim Davies |August 14, 2020 |Nautilus 
They built a model to explain why the photosynthetic machinery of plants wastes green light. Why Are Plants Green? To Reduce the Noise in Photosynthesis. |Rodrigo Pérez Ortega |July 30, 2020 |Quanta Magazine 
Somehow they’ve managed to fine-tune their machinery so that it’s perfectly suited to conditions in that ice. He Found ‘Islands of Fertility’ Beneath Antarctica’s Ice |Steve Nadis |July 20, 2020 |Quanta Magazine 
Standard ultrasound machinery is around 15 times heavier than the Butterfly iQ, which displays images on a mobile app. What will astronauts need to survive the dangerous journey to Mars? |Maria Temming |July 15, 2020 |Science News 
In the same way, bikes no longer look to me like a single mass of machinery. Biking With the Bard |Kara Cutruzzula |December 28, 2014 |DAILY BEAST 
While not all 86 million maintain positions of governance or public service, the Party's machinery runs on watchmaker precision. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 
If you zoom in on Google Maps, you can just make out the jumbles of industrial machinery tucked away inside. The Fiery Underground Oil Pit Eating L.A. |Geoff Manaugh |December 6, 2014 |DAILY BEAST 
Ferris treats celebrity machinery as a means of whitewashing the past. The Tragic History of Southern Food |Jason Berry |November 12, 2014 |DAILY BEAST 
We have provided the machinery to restore our financial system; it is up to you to support and make it work. FDR: King of All Media |John Avlon |September 2, 2014 |DAILY BEAST 
We live in an age that is at best about a century and a half old—the age of machinery and power. The Unsolved Riddle of Social Justice |Stephen Leacock 
Labor, so it was argued, was perpetually being saved by the constant introduction of new uses of machinery. The Unsolved Riddle of Social Justice |Stephen Leacock 
Even a minor dislocation breaks down a certain part of the machinery of society. The Unsolved Riddle of Social Justice |Stephen Leacock 
His voice grated—like machinery started with violent effort against resistance. The Wave |Algernon Blackwood 
When the machinery had been stopped, it was found that Mr. Jones's arms and legs were macerated to a jelly. The Book of Anecdotes and Budget of Fun; |Various