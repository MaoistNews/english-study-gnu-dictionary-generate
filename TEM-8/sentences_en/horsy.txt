Where it begins is Chagrin Falls, Ohio, a horsey-set suburb of Cleveland. Doug Kenney: The Odd Comic Genius Behind ‘Animal House’ and National Lampoon |Robert Sam Anson |March 1, 2014 |DAILY BEAST 
We know they are a horsey family, what with Zara in the Olympic show jumping team and all, but this is taking things a bit far. Princess Anne Says We Should Eat Horses! |Tom Sykes |November 14, 2013 |DAILY BEAST 
Let me own the soft impeachment: I am not a racing man—not in any degree "horsey." Mystic London: |Charles Maurice Davies 
Afterwards, when she came back into the drawing room her hair would have a decidedly horsey odor. The Enemies of Women |Vicente Blasco Ibez 
I was jammed between a monk and a short youth of the horsey kind. An Autobiography |Elizabeth Butler 
Don't you think it was rather clever of me to spot you, when you're not a bit horsey-looking? Lyre and Lancet |F. Anstey 
I urge study of horsey matters especially; that's very important. New Grub Street |George Gissing