They wanted to linger for a few days, which turned into four days of heavy drinking and blasting wit, much of it at James’s expense. To Find Hope in American Cooking, James Beard Looked to the West Coast |John Birdsall |October 2, 2020 |Eater 
To wit, check out this latest ETF flow data from BoA Securities. Is M&A back? Investors hope so, and that’s lifting global stocks |Bernhard Warner |September 15, 2020 |Fortune 
To wit, the kids this morning masked up and entered the school building, one at a time. A flurry of M&A deals lifts global stocks. Yes, even tech stocks |Bernhard Warner |September 14, 2020 |Fortune 
With caustic wit, Iannucci examines the humanity of his characters and the absurdity of their behavior with both passion and precision. FROM THE VAULTS – Straight, but not narrow |Brian T. Carney |September 11, 2020 |Washington Blade 
To wit, buyers appeared eager to jump back into tech names Wednesday. Where do tech stocks go from here? |Anne Sraders |September 10, 2020 |Fortune 
His mature wit and poetic style drew in those around him and we connected instantly. Cuban Hip-Hop Was Born in Alamar |Daniel Levin |December 26, 2014 |DAILY BEAST 
With twice as many British soldiers, Washington was in for a fiercely competitive battle of wit and strength. The British Royals Reinvade Brooklyn: William and Kate Come Watch Basketball on Historic Battle Site |Justin Jones |December 6, 2014 |DAILY BEAST 
He was renowned for his wit, disarming his critics with unfailing humor. Boris Johnson’s Churchill Man Crush |Michael F. Bishop |November 22, 2014 |DAILY BEAST 
Renowned livestock specialist and autism advocate Temple Grandin brought her unique intellect and wit to Reddit. The Most Inspiring Bits of Temple Grandin’s Reddit AMA |Emily Shire |November 18, 2014 |DAILY BEAST 
Amid our grief we now see that New York had been distracted by flash and wit and cash for too long. The Resilient City: New York After 9/11 |John Avlon |September 11, 2014 |DAILY BEAST 
We had six field-pieces, but we only took four, harnessed wit twice the usual number of horses. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
To place wit above sense is to place superfluity above utility. Pearls of Thought |Maturin M. Ballou 
Vincent Alsop died; a presbyterian clergyman, who attacked Dr. Sherlock with great wit and some seriousness. The Every Day Book of History and Chronology |Joel Munsell 
He was judge of the admiralty court of Pennsylvania; his writings abound with wit, humor and satire. The Every Day Book of History and Chronology |Joel Munsell 
John Hales died; an English author, so much admired for his wit and learning, that he is called the ever memorable. The Every Day Book of History and Chronology |Joel Munsell