Israel is powerful compared to its neighbors, but it is close to invulnerable compared to the Palestinians. The only way to solve the Israeli-Palestinian problem |Fareed Zakaria |May 20, 2021 |Washington Post 
Their love is legendary, powerful, and unique, but not invulnerable. Postpartum Stigma: Why My Patient Committed Suicide |Jean Kim |August 5, 2014 |DAILY BEAST 
This impressive record of political consolidation, however, does not leave it invulnerable. The ISIS Caliphate’s Frightening First Month |Charlie Winter |July 29, 2014 |DAILY BEAST 
They do not think they are invulnerable to criticism or that history will guarantee them success. What It Takes to Fight the Terrorists |Ursula Wilder |January 27, 2013 |DAILY BEAST 
It is also doing this at a site built under a mountain and so possibly invulnerable to air attacks. U.S. Hopes Latest Talks on Iran Nuclear Program Lead to New Diplomacy |Michael Adler |April 2, 2012 |DAILY BEAST 
Not only was she invulnerable to bullets and clubs; she had apparently drunk the elixir of eternal life. ‘The Lady and the Peacock’: Peter Popham’s Biography Reveals the Real Aung San Suu Kyi |Peter Popham |March 29, 2012 |DAILY BEAST 
Morgiana threw back her black hair, and laughed as would an invulnerable jinn. God Wills It! |William Stearns Davis 
But the choice of an invulnerable state ticket at this convention is our business and our only business. A Hoosier Chronicle |Meredith Nicholson 
Siegfried of the Nibelungenlied, after slaying the Regin dragon, makes himself invulnerable by bathing in its blood. Myths of Babylonia and Assyria |Donald A. Mackenzie 
But the artisans who follow him are 169 not invulnerable as he is; the grape-shot sweeps them down off the barricade. The History of Modern Painting, Volume 1 (of 4) |Richard Muther 
It's our one weakness—the one Achilles heel in a m-machine that was meant to be invulnerable. The Stutterer |R.R. Merliss