He was dubbed Jack the Lop Ear and has become somewhat of a local celebrity. Cats Rule on Japan’s Tashirojima Island |Nina Strochlic |September 5, 2013 |DAILY BEAST 
Lop off the endnotes and bibliography, and The Measure of Manhattan is barely 300 pages. The Manhattan Project: The Legacy of John Randel Jr. |Kevin Canfield |February 21, 2013 |DAILY BEAST 
For a summer cut her mother would lop off the braids, leaving her with a chic bob for the warmer months. Touching Sylvia Plath’s Hair |Jessica Ferri |February 11, 2013 |DAILY BEAST 
Meanwhile, mortgage modifications and foreclosures continue to lop off mortgage debt. Fed Data: Are We Richer Than We Thought? |Matthew Zeitlin |December 6, 2012 |DAILY BEAST 
A lop-sided power structure gives everyone the wrong incentives. No, the Two-State Solution Isn't Dead Yet—But You're Asking the Wrong Question |Robert Blecher |May 31, 2012 |DAILY BEAST 
Good place to lop about, y' know; a decent place to sit, and a few books and cards and that sort of thing. Raw Gold |Bertrand W. Sinclair 
The doors and the holes for windows are crooked and lop-sided as they would be in a childish attempt. Round the Wonderful World |G. E. Mitton 
The first I knew, Lop-Ear had shrunk away to one side and was crouching low against the bank. Before Adam |Jack London 
The first morning, after my night's sleep with Lop-Ear, I learned the advantage of the narrow-mouthed caves. Before Adam |Jack London 
Lop-Ear was a year older than I, but I was several times angrier than he, and in the end he took to his heels. Before Adam |Jack London