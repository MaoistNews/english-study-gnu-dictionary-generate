Dust, dirt, and cupcake icing can plunge deep into your face-forest. Best beard product for the best beard of your life |Florie Korani |August 18, 2021 |Popular-Science 
Women want the cake and the icing and the cherry on top, just like everybody else, and we have to go for it. ‘We Were Willing to Give Up Everything.’ Billie Jean King Reflects on Her Legacy and New Memoir All In |Cady Lang |August 13, 2021 |Time 
To be able to do it all in your own backyard is just the icing on the cake. Best patio furniture: outdoor seating that suits your space |Billy Cadden |July 20, 2021 |Popular-Science 
Add a dollop of icing in the middle of a cake plate, and put the first layer in place. How to Eat Delmarvelously |jversteegh |July 8, 2021 |Outside Online 
When we’re discussing using SEO data to identify your business gaps, then the icing on the cake is a good, thorough backlink gap. Using SEO data analytics to identify business gaps |Kris Jones |May 24, 2021 |Search Engine Watch 
And aside from doing the requisite things needed to seize the majority, there was icing on the cake, too. For Conservatives, Liberal Tears Taste Sweet |Matt Lewis |November 5, 2014 |DAILY BEAST 
That would be a nice little layer of icing, because it would prove the smug conventional wisdom as wrong as it usually is. Obamacare Crosses the Finish Line |Michael Tomasky |March 31, 2014 |DAILY BEAST 
Bringing along Justin Timberlake and Madonna, as he did last night, was icing on the cake. Jimmy Fallon and Justin Timberlake’s Perfect, Cameo-Filled 'Saturday Night Live' |Kevin Fallon |December 22, 2013 |DAILY BEAST 
It's icing on the, well, ice that the film's story is as emotionally cascading as the setting. ‘Frozen’ Is the Best Disney Film Since ‘The Lion King’ |Kevin Fallon |November 25, 2013 |DAILY BEAST 
The leather gloves are just icing on the weird, fabulous cake. Britney Spears's 10 Looks in "Work Bitch" |Amy Zimmerman |October 2, 2013 |DAILY BEAST 
Part of it is deposited and frozen, and by increments the icing of these monstrous "cakes" is built up. The Home of the Blizzard |Douglas Mawson 
Before serving, sprinkle with a little powdered sugar, or if desired, ice with white icing with grated cocoanut sprinkled on top. The New Dr. Price Cookbook |Anonymous 
Put over boiling water, stirring continually until the icing grates slightly on bottom of bowl. The New Dr. Price Cookbook |Anonymous 
If pink icing is desired add one tablespoon strawberry or other fruit juice. The New Dr. Price Cookbook |Anonymous 
For Chocolate Icing use above, adding 1½ ounces melted unsweetened chocolate or 4½ tablespoons cocoa after removing from fire. The New Dr. Price Cookbook |Anonymous