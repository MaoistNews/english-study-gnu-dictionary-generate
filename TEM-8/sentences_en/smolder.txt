He is a very attractive man with a very nice chiseled jaw and a very pleasant smolder. Why 'The Giver' Movie Will Disappoint the Book's Fans |Kevin Fallon |August 15, 2014 |DAILY BEAST 
This imagery shows SAF troops on the scene as huts smolder in the aftermath of their assault. Satellites Correctly Predict Military Campaign Against Civilians in Sudan |Akshaya Kumar |December 9, 2013 |DAILY BEAST 
The embers of guilt over the entire episode are placed deep within Hilly, where it will smolder for the rest of his life. When Surprise Endings Work: Stuart Nadler’s ‘Wise Men’ |Nicholas Mancusi |February 13, 2013 |DAILY BEAST 
Nevertheless, it continues to smolder away in Chicago, posing a potential threat to the Obama administration. What Will the Fallout Be for Obama? |The Daily Beast |July 10, 2010 |DAILY BEAST 
Arson…Whole streets of tenements and warehouses abandoned to smolder. The Great New York Novel |Taylor Antrim |June 23, 2009 |DAILY BEAST 
It was a brief task to gather the wood and then Ross and Shif'less Sol lighted the fire, which they permitted merely to smolder. The Young Trailers |Joseph A. Altsheler 
When the cooking was finished the logs were drawn back a few inches and the fire went down to coals, but continued to smolder. Dick in the Everglades |A. W. Dimock 
He had known how to light a fire that would smolder long enough for him to get away. St. Nicholas Magazine for Boys and Girls, Vol. 5, September 1878, No. 11 |Various 
The stump will burn and smolder to the end of the roots, leaving nothing but ashes. Golden Days for Boys and Girls |Various 
On the narrow beach, with its background of new growth, smolder the dying embers of a camp-fire. With Rod and Line in Colorado Waters |Lewis B. France