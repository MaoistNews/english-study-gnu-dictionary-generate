This r is a kind of palatal blade continuant, and its use should be avoided, as it is generally held to be affected. The Sounds of Spoken English |Walter Rippmann 
The parasphenoid is the first of the palatal bones to appear. Neotropical Hylid Frogs, Genus Smilisca |William E. Duellman 
Exquisite palatal pleasures, then, are not a sine qua non in the enjoyment of table comforts. Continental Monthly, Vol. I, No. VI, June, 1862 |Various 
This does not preclude the aspiration of consonants, or the occasional local change of a palatal into a guttural. Encyclopaedia Britannica, 11th Edition, Volume 3, Part 1, Slice 3 |Various 
This refers to a twofold mode of pronouncing the Palatal and Lingual consonants, whether plain or aspirated. Elements of Gaelic Grammar |Alexander Stewart