Yet, by far, she is the most ungracious of anyone I have met. Rihanna vs. The Daily Mail And Other Singer-Journalist Feuds |Marlow Stern |June 25, 2013 |DAILY BEAST 
Newt ungracious in defeat, Inauguration plans complete So positive he will succeed Forgot he lost should concede. The Song of Newt Gingrich: From Longshot To Hotshot To No Shot | |April 26, 2012 |DAILY BEAST 
She tried her best to forget it, but the ungracious thought would again and again intrude. The World Before Them |Susanna Moodie 
Jack said in his cheery way as he came up with the boy, whose ungracious answer was, "How do you know my name is Tom?" The Cromptons |Mary J. Holmes 
It is a painful and ungracious task to have to pen these observations, especially, too, in the case of a stranger. Notes and Queries, Number 196, July 30, 1853 |Various 
He could not send the order back without appearing ungracious or disposed to assert that he was of her own station. The Gold Trail |Harold Bindloss 
“The ungracious task shall not be forced upon unwilling chivalry,” said Hadria. The Daughters of Danaus |Mona Caird