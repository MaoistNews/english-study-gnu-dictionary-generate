Some worried about the ancient elm trees in Hyde Park that found themselves contained in the enormous structure. The metaverse is a new word for an old idea |Genevieve Bell |February 8, 2022 |MIT Technology Review 
A further experiment to plant elm trees beyond center field was mercifully cut short. 100 Years of Wrigley Field: Are the Chicago Cubs Horrible Because of the Ballpark? |Luke Epplin |March 28, 2014 |DAILY BEAST 
A red convertible turned onto Elm Street and went down the hill. Jimmy Breslin on JFK’s Assassination: Two Classic Columns |Jimmy Breslin |November 22, 2013 |DAILY BEAST 
As I opened the door to my house, a scene from Nightmare on Elm Street started to explode. ‘Has Beverly Hills Fallen Yet?’ |Shelby Coffey III |April 26, 2012 |DAILY BEAST 
This year, she starred as the protagonist Nancy Holbrook in the remake of the 1980s horror movie A Nightmare on Elm Street. Dragon Tattoo Actress Cast |Gina Piccalo |August 16, 2010 |DAILY BEAST 
As with most horror remakes, the new Nightmare on Elm Street finds the tune but loses the rhythm. Stop Remaking Horror Movies! |Keith Phipps |April 29, 2010 |DAILY BEAST 
Since Henry Hawk could sit in a great elm far up the road and see himp. The Tale of Grandfather Mole |Arthur Scott Bailey 
The three happy children stood under the elm and looked up at the tiny hanging nest. Seven O'Clock Stories |Robert Gordon Anderson 
There were a great many more there once, and we used to call it Elm Grove in old times. David Fleming's Forgiveness |Margaret Murray Robertson 
The glade was thickening with shadows, but the sunlight still marked the top of an elm and made glorious the zenith. A Virginia Scout |Hugh Pendexter 
The light had left the top of the elm and the fleecy clouds overhead were no longer dazzling because of their borrowed splendor. A Virginia Scout |Hugh Pendexter