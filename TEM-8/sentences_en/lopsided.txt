A healthy chunk of the Democratic base vote consists of nonwhite people living in overwhelmingly nonwhite neighborhoods that lend themselves to “packing” Democrats into inefficient lopsided districts. America needs a democratic revolution |Matthew Yglesias |September 17, 2020 |Vox 
To Jared’s point, it’s been strange because of the lopsided scores. We Could Have Watched Raptors-Celtics Game 6 All Day |Chris Herring (chris.herring@fivethirtyeight.com) |September 10, 2020 |FiveThirtyEight 
It was perhaps the most lopsided upset in the history of the NBA playoffs. The Bucks Played It Safe And Made The Wrong Kind Of History |Chris Herring (chris.herring@fivethirtyeight.com) |September 9, 2020 |FiveThirtyEight 
If anything, this ratio may be even more lopsided among Republicans. Why There Are So Few Moderate Republicans Left |Lee Drutman (drutman@newamerica.org) |August 24, 2020 |FiveThirtyEight 
The 21-year-old entered play Friday tied for first in the majors in wins above replacement,13 making the Padres’ deal to acquire him look like one of the most lopsided in recent history. Fernando Tatís Jr. Was Already Mashing. Then He Started Hitting The Ball Harder. |Travis Sawchik |August 21, 2020 |FiveThirtyEight 
But opponents take little comfort from these lopsided numbers. Tennessee Voters Face a Loaded Abortion Question |Eleanor Clift |October 4, 2014 |DAILY BEAST 
Despite such a lopsided vote in the House, the measure will die in the Senate. The Sane Case for Auditing the Fed |Nick Gillespie |October 2, 2014 |DAILY BEAST 
The weapons rarely hit their targets, and the death toll has been lopsided against them. Hamas Has Already Won Its Rocket War With Israel |Eli Lake |July 16, 2014 |DAILY BEAST 
This decision turned the trial of Whitey Bulger into one of the most lopsided cases in the history of jurisprudence. Whitey Bulger and the FBI Whitewash |T.J. English |November 15, 2013 |DAILY BEAST 
In case you need reminding, the final Electoral College score was a lopsided 332–206. Can a Republican Win 270 Electoral Votes in 2016...or Ever? |Myra Adams |August 18, 2013 |DAILY BEAST 
Now break up that lopsided, rickety table there and make a fire. The Ghost Breaker |Charles Goddard 
Art Kuzak could only sigh heavily, grin a lopsided grin, and produce. The Planet Strappers |Raymond Zinke Gallun 
That hitch in his development, rendering him the most lopsided of God's creatures, was his standing misfortune. The Well-Beloved |Thomas Hardy 
"Awfully sorry, Mr. Morgan," he said with a lopsided smile that didn't even look genuine. Thin Edge |Gordon Randall Garrett 
Louise, meanwhile, out of hearing, was trying to sell a very lopsided basket to an elderly gentleman. Winona of the Camp Fire |Margaret Widdemer