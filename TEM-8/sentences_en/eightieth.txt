He lived to be ninety years old, and produced the most beautiful of his tragedies in his eightieth year, the "Oedipus at Colonus." Beacon Lights of History, Volume I |John Lord 
Mlle. Clairon died on January 31, 1803, six days after completing her eightieth year. Queens of the French Stage |H. Noel Williams 
So active did he continue to the end of his days, that on his eightieth birthday he climbed an oak in my company. Wanderings in South America |Charles Waterton 
Since that date he has lived retired in Hutchinson, where he still makes his home, being now in the eightieth year of his age. Lyman's History of old Walla Walla County, Vol. 2 (of 2) |William Denison Lyman 
None of us are, you know, when we pass the eightieth milestone! Cursed |George Allan England