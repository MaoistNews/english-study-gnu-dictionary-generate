Author Jerome Klinkowitz, who has written several books about Vonnegut, believes that Lawrence may well not have changed as much as a semicolon of the manuscript. What Early Drafts of Slaughterhouse-Five Reveal About Kurt Vonnegut's Struggles |Tom Roston |November 11, 2021 |Time 
I point out the phrase “more distinct separation,” and I suggest that the purpose of the semicolon is at least in part rhythmic. Donald E. Westlake Defends the Semicolon |Donald E. Westlake |October 25, 2014 |DAILY BEAST 
The new material contains a semicolon; it has orders to call me if anybody gives it any trouble. Donald E. Westlake Defends the Semicolon |Donald E. Westlake |October 25, 2014 |DAILY BEAST 
The semicolon is used in enumerations, as in the last example, in order to keep the parts more distinctly separate. "Stops" |Paul Allardyce 
The colon is used to indicate pauses more abrupt than those indicated by the semicolon. "Stops" |Paul Allardyce 
A pause generally indicated by a comma may be indicated by a semicolon when commas are used in the sentence for other purposes. "Stops" |Paul Allardyce 
If the conjunction "and" were inserted in the last sentence, the comma would be used instead of the semicolon. "Stops" |Paul Allardyce 
Of course, this rule must be qualified by the rules for the stronger points, especially by those for the semicolon and the colon. "Stops" |Paul Allardyce