By almost any standards it’s a stunning rise to stardom for a player many mainstream soccer fans had never even heard of two years ago. The unending fairytale of a former Liberian refugee teen who’s now a European soccer champion |Yomi Kazeem |August 26, 2020 |Quartz 
A social scientist tested that idea in Iraq by putting Christians and Muslims on the same soccer teams. Interfaith soccer teams eased Muslim-Christian tensions — to a point |Sujata Gupta |August 13, 2020 |Science News 
So, the most important thing to understand about women’s soccer is that FIFA banned it until 1971. Should America (and FIFA) Pay Reparations? (Ep. 426) |Stephen J. Dubner |July 16, 2020 |Freakonomics 
Inside the animal was a squished mass the size of a soccer ball. Fossil stomach reveals a dinosaur’s last meal |Carolyn Wilke |July 7, 2020 |Science News For Students 
An ambitious plan to build a quantum computer the size of a soccer field could soon become a reality. A New Startup Intends to Build the World’s First Large-Scale Quantum Computer |Edd Gent |June 22, 2020 |Singularity Hub 
His section on why he and his wife split—he essentially chose soccer over her—would be risky if its candor were not so refreshing. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 
There is a brutally honest section of the book about how you fell out of love with your wife, and essentially chose soccer. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 
Soccer in the rest of the world is what we know as basketball. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 
United States goalkeeper Tim Howard on why soccer struggles in America and how he essentially chose his career over his wife. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 
According to contemporary reports, at several of the truces, there were rough soccer matches between the German and British sides. Royals Remember The Christmas Truce of 1914 |Tom Sykes |December 12, 2014 |DAILY BEAST 
Would I kick off the football season or attend some particular Soccer game? My Wonderful Visit |Charlie Chaplin 
She had something the size of a soccer-ball slung over her back now, and I stared admiringly at it. Little Brother |Cory Doctorow 
Among the team games suitable for girls are: field hockey, soccer, baseball played with a soft ball and basket-ball. How Girls Can Help Their Country |Juliette Low 
He is so excited that he goes outside to tell his friends during their soccer game. FreeChildrenStories.com Collection |Daniel Errico 
Neon has never played soccer before, but he actually likes it. FreeChildrenStories.com Collection |Daniel Errico