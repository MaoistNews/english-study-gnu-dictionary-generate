Without this word of humility and obedience the incarnation of our divine Saviour would not have been accomplished. Mary, Help of Christians |Various 
The man who allows himself to be crucified is no true saviour, because by allowing it he renders himself powerless to save. The Everlasting Arms |Joseph Hocking 
St. Saviour's—designed by Mr. Hibbert, architect, of this town—is one of the handsomest and best finished churches we have seen. Our Churches and Chapels |Atticus 
It may also be worthy of mention that we saw fewer sleepers at St. Saviour's than in any other place of worship yet visited by us. Our Churches and Chapels |Atticus 
Educational business in connection with St. Saviour's is carried on in various parts of the district. Our Churches and Chapels |Atticus