He was a genuine modernizer who built cities, factories and universities and took land away from aristocrats to give to peasants. America and Iran, from fascination to antagonism |Ray Takeyh |February 26, 2021 |Washington Post 
In 1948, Charles Aron, co-owner of Aristocrat, divorced his wife, Evelyn. The Stacks: How Leonard Chess Helped Make Muddy Waters |Alex Belth |August 2, 2014 |DAILY BEAST 
The French aristocrat Marquis de Sade once said that “It is only by way of pain one arrives at pleasure.” The Twisted Sadism of ‘Borgman’ |Alex Suskind |June 28, 2014 |DAILY BEAST 
He was a lowly country lawyer and she a Kentucky aristocrat who had come to visit her older sister in Illinois. Lincoln in Love |Jerome Charyn |February 14, 2014 |DAILY BEAST 
Where Kate is mild-mannered and middle-class, Cressy, 24, is a wild, blue-blooded aristocrat with bohemian heritage. Dissecting Cressida Bonas's Style: How Prince Harry's Girl Dresses |Tom Sykes |October 15, 2013 |DAILY BEAST 
Will she settle down to a life of quiet obscurity and shooting parties and marry an aristocrat like the establishment hopes? Laugh All You Like at Her Dumb Party Tips, but Pippa Has the Midas Touch |Tom Sykes |August 29, 2013 |DAILY BEAST 
Mistress of the Chevalier de Valois, and mother of a child that was attributed to the old aristocrat. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 
To-morrow he would be riding towards Paris, the cavalier of a beautiful aristocrat. The Light That Lures |Percy Brebner 
You will go most easily as a woman of the people, one who has some aristocrat enemy on whom she wishes to be avenged. The Light That Lures |Percy Brebner 
Over the wine the stranger had certainly expressed distrust of Lucien Bruslart, an aristocrat turned patriot. The Light That Lures |Percy Brebner 
Bruslart knew that to pity the aristocrat might be hardly more dangerous than to abuse the woman. The Light That Lures |Percy Brebner