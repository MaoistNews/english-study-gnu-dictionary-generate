Prices for the lightweight, silvery element have climbed 35% this year, according to FactSet. China’s environmental goals are driving aluminum prices to a 10-year high |John Detrixhe |August 30, 2021 |Quartz 
The silvery metal is essential for electric vehicles and renewable energy batteries. The Taliban now controls one of the world’s biggest lithium deposits |Tim McDonnell |August 16, 2021 |Quartz 
They were all test pilots, they wore silvery space suits and signature buzz cuts and they had perfect fly-boy names like Deke and Gus and Al and Gordo. With Private Space Flight On the Rise, Who Gets to Be Called 'Astronaut?' |Jeffrey Kluger |August 4, 2021 |Time 
In the first week alone, I was stunned by skyscraper-tall badlands and silvery snakes that glinted like holograms in the sunlight. You Don’t Need to Watch Hiking Videos to Hike |Grayson Haver Currin |June 15, 2021 |Outside Online 
Horst recommends this plant, Scindapsus pictus, with its beautiful silvery green leaves. Online plant sellers are having a moment. Here’s where to shop. |Lindsey Roberts |November 11, 2020 |Washington Post 
Little wiry chap, with silvery hair, bright brown eyes and plenty of wrinkles. Iran’s Top Spy Is the Modern-Day Karla, John Le Carré’s Villainous Mastermind |Michael Weiss |July 2, 2014 |DAILY BEAST 
It has silvery blue skin and a red dorsal fin that runs the length of its body. Fishy Mystery: Are Beached Oarfish Trying to Tell Us Something? |Kevin Bailey |October 23, 2013 |DAILY BEAST 
That the capable, silvery Kathleen Sebelius is the face of Obamacare makes me want to weep. What’s Behind the Obamacare Cock-Up |Tina Brown |October 21, 2013 |DAILY BEAST 
Also includes wedding bouquet, silvery tiara, earrings and engagement ring. William and Kate Barbies Sell Out! |Tom Sykes |April 6, 2012 |DAILY BEAST 
The gorgeous rota looks like an alien lighthouse, with silvery lights spinning out of its core. Carsten Nicolai’s Skydiving Dance |Jimmy So |September 24, 2011 |DAILY BEAST 
Though she was warmly wrapped in a soft rug of silvery fur, a chill crept into her heart. Rosemary in Search of a Father |C. N. Williamson 
The tone is of quiet silvery beauty, but the stop does not seem to have been largely adopted by other builders. The Recent Revolution in Organ Building |George Laing Miller 
Suddenly the clock in the big hall below chimed two upon its peal of silvery bells. The Doctor of Pimlico |William Le Queux 
He spoke with a grave and silvery pitch that made his words seem to soar lightly over his audience. The Chequers |James Runciman 
Georgie could not forbear a smile, while Lucy burst into inextinguishable peals of silvery laughter. The Pit Town Coronet, Volume II (of 3) |Charles James Wills