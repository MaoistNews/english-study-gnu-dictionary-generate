Hughes’s paintings are ravishing merely as color-blending exercises, but their layered depths have poignant human significance. In the galleries: A sizzling exhibit crackles with creativity |Mark Jenkins |July 23, 2021 |Washington Post 
Next they wanted to know if Hill had told "the black men to ravish all the white women." Enough With This 'Lost Cause' Nonsense |Justin Green |February 19, 2013 |DAILY BEAST 
The remembrance of these times of happiness and innocence frequently returning to my mind, both ravish and affect me. The Confessions of J. J. Rousseau, Complete |Jean Jacques Rousseau 
Thy voice sends forth such music, that I never Was ravish'd with a more celestial sound. The Plays of Philip Massinger |Philip Massinger 
Callot's men are users of the wheel and the estrapade; they roast the husband while they ravish the wife. George Cruikshank |W. H. Chesson 
It was a feat altogether to ravish a delighted father's heart, and no wonder that he counted John so great a comfort. Heart |Martin Farquhar Tupper 
The perfect symmetry of this marvellous structure would ravish Michel Angelo. The Atlantic Monthly, Vol. 13, No. 78, April, 1864 |Various