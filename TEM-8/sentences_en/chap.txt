Worry not—whether your first attempt is a notebook cover or some fringey pair of chaps, we’ve collected all the basics you’ll need to know. Everything you need to know to start leatherworking |Sandra Gutierrez G. |February 19, 2021 |Popular-Science 
Sure, you may call this petty, but it really does chap my hide! Uncle Ro Ro Daddy Hates Rudeness! |Roland S. Martin |August 9, 2014 |DAILY BEAST 
Little wiry chap, with silvery hair, bright brown eyes and plenty of wrinkles. Iran’s Top Spy Is the Modern-Day Karla, John Le Carré’s Villainous Mastermind |Michael Weiss |July 2, 2014 |DAILY BEAST 
He survived a penniless childhood and a brutal war and emerged by all accounts an admirable chap. Just Kill Mr. Bates Already! How to Save ‘Downton Abbey’ |Andrew Romano |February 20, 2014 |DAILY BEAST 
Then, handing me back my iPad, he said nonchalantly in a really good mock-English accent, “Sorry, chap, my dance card is full.” Bulletin From The Front |Rabbi Daniel Landes |April 9, 2013 |DAILY BEAST 
Just minutes earler, a chap wearing a Prince William mask tried to gain entry to the hospital via the main entrance. William Arrives At Hospital - As Masked Prankster is Lead Away! |Tom Sykes |December 4, 2012 |DAILY BEAST 
And since he was a very fast runner—for short distances—he met Grandfather Mole just as the old chap was crawling up the bank. The Tale of Grandfather Mole |Arthur Scott Bailey 
A groom is a chap, that a gentleman keeps to clean his 'osses, and be blown up, when things go wrong. The Book of Anecdotes and Budget of Fun; |Various 
But he watched Grandfather Mole narrowly, with a grin on his face, to see what the old chap would do. The Tale of Grandfather Mole |Arthur Scott Bailey 
The strenuous efforts made by the Spaniards to secure their release are fully referred to in Chap. The Philippine Islands |John Foreman 
"No, old chap," I answered, pulling the long ears gently till he smiled. The Soldier of the Valley |Nelson Lloyd