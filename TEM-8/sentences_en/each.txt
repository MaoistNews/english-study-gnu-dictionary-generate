With all that said, representation of each of these respective communities has increased in the new Congress. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 
Now, she says, her coworkers are actively pranking each other and blaming it on the ghost. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 
Each individual race involves an unusual collaboration between researchers, manufacturers, and public-health entities. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 
First, one fights with another, then they make an alliance, then they go back to fighting each other. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 
In Dresden, Germany, anti-Islam rallies each week draw thousands of demonstrators. Police Hunt for Paris Massacre Suspects |Tracy McNicoll, Christopher Dickey |January 7, 2015 |DAILY BEAST 
Each day she resolved, "To-morrow I will tell Felipe;" and when to-morrow came, she put it off again. Ramona |Helen Hunt Jackson 
Some were even re-arrested for the same nefarious purpose, and the daily papers published their names on each occasion. The Philippine Islands |John Foreman 
This city stands upon almost two equal parts on each side the river that passes through. Gulliver's Travels |Jonathan Swift 
Done, says he, why let fifty of our men advance, and flank them on each wing. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 
Each religion claims that its own Bible is the direct revelation of God, and is the only true Bible teaching the only true faith. God and my Neighbour |Robert Blatchford