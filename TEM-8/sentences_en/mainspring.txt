The outer end of the mainspring is attached to the rim of the barrel, and the inner end to the barrel arbor. The Wonder Book of Knowledge |Various 
Unless this double mainspring is known, the conduct of Catherine de' Medici will remain forever misunderstood. Catherine de' Medici |Honore de Balzac 
To such religion was the mainspring which kept the whole intellect going; and religion was to be had at the meeting. East Anglia |J. Ewing Ritchie 
All the old restraints were wanting, and self-interest alone formed the mainspring of action. The Influence and Development of English Gilds |Francis Aiden Hibbert 
Fingering is the mainspring, the determining principle, one might almost say the life and soul, of the pianoforte technique. Frederick Chopin as a Man and Musician |Frederick Niecks