By now near to drowning in complicity and subjection, I obeyed. Hitchens on Thatcher's Sex Appeal |David Frum |January 12, 2012 |DAILY BEAST 
And thou didst bow thyself to women: and by thy body thou wast brought under subjection. The Bible, Douay-Rheims Version |Various 
Nicholson and John Lawrence were there; could they hold those warrior-tribes in subjection, or, better still, in leash? The Red Year |Louis Tracy 
With this political subjection one is reluctant to associate a more sordid kind of obligation. King Robert the Bruce |A. F. Murison 
The law is immoral: it is the conspiracy of rulers and priests against the workers, to continue their subjection. Prison Memoirs of an Anarchist |Alexander Berkman 
But the most important event in this stage of evolution was the subjection of the plant world to man. Man And His Ancestor |Charles Morris