How had he, a useless old fogy, dared to blackball a man like Iver? Tristram of Blent |Anthony Hope 
They have won the annual debate right along, so that my old fogy uncle declares all the brains are in Harvard. Frank Merriwell's Races |Burt L. Standish 
They laughed at me because I found fault with these things and called me an old fogy. History of the American Clock Business for the Past Sixty Years, and Life of Chauncey Jerome |Chauncey Jerome 
"Old fogy teacher" or "he has the old ways yet" are expressions that are too common to require any explanation. The Youthful Wanderer |George H. Heffner 
I go to the latter occasionally—the institute is an old fogy concern, but the grounds are fine. The Wound Dresser |Walt Whitman