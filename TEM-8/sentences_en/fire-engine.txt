Our Toniebox was pink, but you can select from a spectrum of six choices, including fire engine red, sky blue, grass green, purple, and gray. Toniebox review: Screen-free entertainment that’s music to kids’ ears |Tony Ware |July 28, 2021 |Popular-Science 
Clara Young Kim places her camera tightly to reveal that a fire engine is as battered as it is shiny. In the galleries: Rejuvenating the obsolete into unconventional art |Mark Jenkins |February 12, 2021 |Washington Post 
It’s like paying for a fire engine even in years when there’s no fire. Reshaping COVID, Capitalism and Tech With Bill Gates |Nick Fouriezos |December 9, 2020 |Ozy 
An ambulance and fire engine responded, along with additional police. After D.C. firefighter shot and wounded on the job, department expresses frustration at violence in city |Peter Hermann |November 24, 2020 |Washington Post 
But what is there more irresponsible than playing with the fire of an imagined civil war in the France of today? Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 
The cameraman was reporting on the factory catching fire when the inevitable happened. Fireworks Factory Explodes in Colorful Burst |The Daily Beast Video |January 6, 2015 |DAILY BEAST 
Lady Edith is so sad that her sadness nearly set the whole damned house on fire. ‘Downton Abbey’ Review: A Fire, Some Sex, and Sad, Sad Edith |Kevin Fallon |January 5, 2015 |DAILY BEAST 
The jet engine instantly brought two advances over propellers: it doubled the speed and it was far more reliable. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 
Maybe Mary is being more realistic about a second marriage—but is it too much to ask for a little fire? What Downton’s Fashion Really Means |Katie Baker |January 2, 2015 |DAILY BEAST 
"A camp-fire would hardly flash and die out like that, Sarge," he answered thoughtfully. Raw Gold |Bertrand W. Sinclair 
She got up and stood in front of the fire, having her hand on the chimney-piece and looking down at the blaze. Confidence |Henry James 
The fire had been heaped over with earth—to screen it from prying eyes, I suppose, while the good work went on. Raw Gold |Bertrand W. Sinclair 
But, as the keel of the boats touched bottom, each boat-load dashed into the water and then into the enemy's fire. Gallipoli Diary, Volume I |Ian Hamilton 
The men, whose poniards his sword parried, had recourse to fire-arms, and two pistols were fired at him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter