Reports this week said they were free to roam around Qatar and were not confined to house arrest. U.S. Spies Worry Qatar Will ‘Magically Lose Track’ of Released Taliban |Eli Lake |June 5, 2014 |DAILY BEAST 
Angels begin consorting with human women, and giants roam the earth. The Backstory of ‘Noah’ Is Full of Giants, Horny Angels, and a Grieving God |Tim Townsend |March 28, 2014 |DAILY BEAST 
Sure, brutes and warlords still roam the region, but the worst enemies no longer wear uniforms or wield guerrilla manuals. Central America’s New Cold War |Mac Margolis |November 23, 2013 |DAILY BEAST 
The evening before, Mischief Night, pranksters roam the dark. When Mars Attacked 75 Years Ago—And Everyone Believed It |Marc Wortman |October 29, 2013 |DAILY BEAST 
In the script, the cheetahs drift from their owner and roam suburban Mexico unattended. The Best Scenes From Cormac McCarthy’s ‘The Counselor’ Screenplay |Thomas Flynn |October 27, 2013 |DAILY BEAST 
Thus four thousand Indians at most roam through, rather than occupy, these vast stretches of inland territory and sea-shore. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
It would be too dismal for Gwynne to roam through the purlieus with a policeman—and he cannot come down often. Ancestors |Gertrude Atherton 
He does not keep them in barns, or feed them with hay, but they roam over the hills, and feed on grass both in winter and summer. The Nursery, November 1881, Vol. XXX |Various 
He hasn't the nerve to forsake his native heath and roam the wide world, a free and independent gentleman. Prison Memoirs of an Anarchist |Alexander Berkman 
Overwhelming it was, furious, relentless; his thoughts strove to roam, but it seized him by the hair and dragged him back. Love's Pilgrimage |Upton Sinclair