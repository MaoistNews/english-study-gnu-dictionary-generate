When Iron Dome misses -- and it does sometimes miss -- the Gaza rockets kill and maim only within a very limited radius. Israel's 'Iron Dome' Can't Stop a Suitcase Bomb |David Frum |March 12, 2012 |DAILY BEAST 
But what chances should I have given them to kill or maim us: Fifty-fifty? To Shoot or Not to Shoot |Edward Conlon |April 14, 2011 |DAILY BEAST 
On land and in sea the animal creation chase and maim, and slay and devour each other. God and my Neighbour |Robert Blatchford 
We came in peace and goodwill, not to maim and slay, or to spread alarm and desolation through thy land. The Devil-Tree of El Dorado |Frank Aubrey 
If a man maim another, and does not compromise with him, there shall be retaliation in kind. The Two Great Republics: Rome and the United States |James Hamilton Lewis 
Still there were left at liberty enough to maim cattle and shoot at landlords. Punch, or the London Charivari, Volume 147, August 12, 1914 |Various 
Tho' I am not unhealthy, yet I am very weak, know maim therefore I hope it won't be long maim. The Mirror Of Literature, Amusement, And Instruction |Various