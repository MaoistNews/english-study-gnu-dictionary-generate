For one, O’Connell masters every rustle, movement of the body, caught breath, laugh, rise of emotion. ‘Dana H.’ Was Kidnapped by a Psychopath. Now Her Ordeal Is a Broadway Play. |Tim Teeman |October 18, 2021 |The Daily Beast 
Our hunter-gatherer ancestors survived by assuming every rustle in the grasses was a lurking hungry lion, not harmless birds hunting for seeds. Boost your mood with emotional brain training |By Laurel Mellin/The Conversation |January 4, 2021 |Popular-Science 
They would rustle and tussle it for like three minutes and that was it! OITNB’s New Villain Vee, Played By Lorraine Toussaint, Speaks for the First Time |Kevin Fallon |June 13, 2014 |DAILY BEAST 
Prepared with paper to rustle, rice to shake, and water to ripple. ASMR and the Rise of the Whisper Fetish |Aurora Snow |December 7, 2013 |DAILY BEAST 
In her hand she held a silk map of the region, given to agents to avoid the giveaway rustle of paper in pockets. World War II’s Most Glamorous Spy: Christine Granville |Emma Garman |July 7, 2013 |DAILY BEAST 
If I can rustle horses I'll send these two boys on home, with a note to the old man explaining how the play came up. Raw Gold |Bertrand W. Sinclair 
But the fronds of a palm-tree in the wind produce a noise that is unlike the rustle of any other foliage in the world. The Wave |Algernon Blackwood 
And before he had finished smiling, over the parquet floor behind him there came the light rustle of a dress. Bella Donna |Robert Hichens 
The faint rustle of the Black Hood's cape caused the messenger on the ground to look up. Hooded Detective, Volume III No. 2, January, 1942 |Various 
There was a rustle of expectancyupon the girls side, at leastat Assembly on Monday morning. The Girls of Central High on the Stage |Gertrude W. Morrison