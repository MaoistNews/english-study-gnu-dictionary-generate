Because when it comes to moving against the virus, we’re a long way from checkmate. Is School Out Forever? |Daniel Malloy |August 9, 2020 |Ozy 
Under these circumstances many devices were resorted to to checkmate their political moves. The Roots of Southern Voter Intimidation |David Frum |February 4, 2013 |DAILY BEAST 
Once a case is made that a leak might be imminent, it is checkmate: the agent is thwarted. Catching the Next WikiLeaker |Eli Lake |October 20, 2011 |DAILY BEAST 
Somebody knew exactly what was going on, somebody was at work to checkmate the dark design. The Weight of the Crown |Fred M. White 
You and I will try to checkmate that pack if there is anything uncanny in it. Fifty-Two Stories For Girls |Various 
The ruffian was counting confidently on having things his own way, and Matt was wondering how he could checkmate him. Motor Matt's Mystery |Stanley R. Matthews 
Victory for the South became immediately impossible, no matter how long the final checkmate might be delayed. Invention |Bradley A. Fiske 
If the Austrian minister could accomplish this, he could thereby checkmate Prussian ambitions for leadership in Germany. The Life of Napoleon Bonaparte |William Milligan Sloane