Ahmad Naeem Wakili lives in a daze, his mind often drifting to his wife and 2-year-old, a little girl with big brown curls and green eyes. Tens of Thousands of Afghans Who Fled The Taliban Are Now Marooned in America's Broken Immigration Bureaucracy |Jasmine Aguilera |January 26, 2022 |Time 
In a daze, we formed a huddle on the sidewalk one block away. I Witnessed a Fatal Bike Crash. It Changed Me Forever. |lwhelan |December 13, 2021 |Outside Online 
It was one of those things where you kind of needed him to snap and wake us up out of our daze. Peter Laviolette, master motivator, is known for his pep talks. Here’s why they work. |Samantha Pell |May 14, 2021 |Washington Post 
Earlier today, I walked around the kitchen holding one of those pink snappers, half in a daze, thinking what I could do with it. A Magical Meal at Louie’s Backyard in the Conch Republic |Jane & Michael Stern |July 13, 2014 |DAILY BEAST 
I was in a daze, still wearing clothes stiffened with evaporated sea salt. How Military Veterans Led Sandy Volunteer Efforts |Peter Meijer |October 28, 2013 |DAILY BEAST 
But others walked quietly in a daze or lay on their backs and started at the sky. The Crackdown Begins |Mike Giglio |July 4, 2013 |DAILY BEAST 
I was a little freaked out but curious enough to go back to my orange daze and hear some more words from the man in the ether. Gina Gershon’s Trip to Heaven in the Dentist’s Chair |Gina Gershon |October 23, 2012 |DAILY BEAST 
Svetlana and Ksenya would agree with that assessment—they remember arriving in a daze. How a Blogger Blocked Sex Slavery |Abigail Pesta |March 22, 2011 |DAILY BEAST 
When Henry appeared, a trifle shaken out of his daze and anxious only to get away, Mr. Cordyce stretched out his hand. The Box-Car Children |Gertrude Chandler Warner 
Only half convinced and full of suspicion, the Sultan walked on in a daze, as though he were going to his last doom. The Philippine Islands |John Foreman 
It was a glancing blow, but it was enough to daze the man and send him reeling backward. Motor Matt's "Century" Run |Stanley R. Matthews 
And Black Hood, his mind still in a daze, stared down at the gems in the copper's hand. Hooded Detective, Volume III No. 2, January, 1942 |Various 
Though the boss's disappearance was now four days old, things were still in a sort of daze down at the railroad offices. The Wreckers |Francis Lynde