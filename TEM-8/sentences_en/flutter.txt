Put loudness and slowness together, though, and the results tend to feel hefty and overgrown in our mind’s ear, even though we’re only experiencing a weightless flutter of air. Eyehategod’s high-gravity metal can connect you to the heavens |Chris Richards |March 12, 2021 |Washington Post 
There are ghosts that may flutter above the stage at the Met. When Stalin Met Lady Macbeth |Brian Moynahan |November 9, 2014 |DAILY BEAST 
Farrow smiles and butterflies flutter and stars shoot across the night sky. Do Not Defy Ronan Farrow’s Power Pout |Tim Teeman |February 25, 2014 |DAILY BEAST 
It started off small: a hint of annoyance here, a flutter of incredulity there. Just Kill Mr. Bates Already! How to Save ‘Downton Abbey’ |Andrew Romano |February 20, 2014 |DAILY BEAST 
There are no red carpets and you just flutter about watching films. Carey Mulligan, Star of ‘Inside Llewyn Davis,’ on the Coen Brothers, ‘N Sync Fandom, Lorde, and More |Marlow Stern |December 3, 2013 |DAILY BEAST 
Just as suddenly she was gone, leaving a flutter of red curtains. Nepal Old and New: Kathmandu Valley’s Royal Cities Get a Facelift |Condé Nast Traveler |August 19, 2013 |DAILY BEAST 
They generally flutter for two or three minutes about the most elevated point of any object, and then disappear. A Woman's Journey Round the World |Ida Pfeiffer 
Oh, Ive had it out and felt behind it, urged Miss Carrington, all of a flutter now. The Girls of Central High on the Stage |Gertrude W. Morrison 
There was a note in her voice of such absolute sincerity, mingled with fear, that he opened his arms and let her flutter away. A Butterfly on the Wheel |Cyril Arthur Edward Ranger Gull 
It is why they have black wings and tails, why they flutter so with joy, and why they never finish their song. Stories the Iroquois Tell Their Children |Mabel Powers 
He half expected a check to fall fluttering to the floor; but alas, there was not a single flutter. Love's Pilgrimage |Upton Sinclair