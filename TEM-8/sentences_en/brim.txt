Another approach focuses on protecting the rare slices of land and sea that brim with huge numbers of species. Can people protect as much space as nature needs? |Jonathan Lambert |January 21, 2021 |Science News For Students 
Nowadays “beanie” is kind of a catch-all word for hats designed to fit over the ears, without brims or flaps. Best winter hats: Comfortable hats to keep you warm |Carsen Joenk |January 20, 2021 |Popular-Science 
This year’s holiday season has been filled to the brim with nonstop laughs, great food and good vibes. Here’s How Our Favorite Celebrities Spent The Holidays |Brooklyn White |December 26, 2020 |Essence.com 
You need to tumble the mussels around during cooking, and you can’t do that if your pot is filled to the brim. Mussels are simple to prepare, and add tasty drama to the table |Martha Holmberg |October 29, 2020 |Washington Post 
Keep the mask above the brim of your hat when you run, then quickly flip it down over your nose and mouth when you encounter someone. 6 Ways to Carry Your Mask While Running |Joe Jackson |August 24, 2020 |Outside Online 
It had a wide brim and a tall crown, which created an insulated pocket of air and could also be used to carry water. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 
And he had a cowboy hat that he liked touching, too—he smoothed the brim back like it was a ducktail haircut. The Stacks: Pete Dexter on What It’s Like to Lose the Knack of Having Fun |Pete Dexter |September 20, 2014 |DAILY BEAST 
On it are balanced a plate of eggs and toast, an open quart jar of grape jelly, and a beer mug full to the brim with orange juice. The Ridiculousness of Father's Day |P. J. O’Rourke |June 15, 2014 |DAILY BEAST 
Just as his fingers touched the brim, his foot would kick the hat out of reach. Oklahoma Farmers Find Ways to Cope While Waiting for Drought to End |Malcolm Jones |October 3, 2012 |DAILY BEAST 
At times like these you must be careful not to brim over with elation-into-crashing-despair. What the Stars Predict for Your Week |Starsky + Cox |August 20, 2011 |DAILY BEAST 
The rebellious brown hair was almost in the shade of my own dashing hat-brim. The Soldier of the Valley |Nelson Lloyd 
Gunn touched the brim of his soft felt hat, which he wore turned down all round apparently in imitation of a flower-pot. Dope |Sax Rohmer 
At the farm-gate they met Dorothy, fresh and blooming as a rose, with a pail in each hand foaming to the brim with milk. The World Before Them |Susanna Moodie 
Piegan was already mounted, watching us whimsically from under the dripping brim of his hat. Raw Gold |Bertrand W. Sinclair 
She wore a black silk cloak, a dark grey hat with a wide brim, and a broad satin ribbon under her chin. Skipper Worse |Alexander Lange Kielland