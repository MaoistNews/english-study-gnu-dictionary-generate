Earlier this week, Huckabee ended his Fox News talk show so he could spend time mulling another bid for the Republican nomination. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 
He lost his bid for a fourth term to George Pataki that year. President Cuomo Would’ve Been a Lion |Jonathan Alter |January 2, 2015 |DAILY BEAST 
Get ready to bid farewell to the spitfire Bobby Bottleservice and big booty player Ref Jeff. The Zany Shades of Nick Kroll |Abby Haglage |December 15, 2014 |DAILY BEAST 
Another crowd moved west in an apparent bid to block the Holland Tunnel. Eric Garner Protests: ‘It’s Like Vietnam’ |Abby Haglage, Caitlin Dickson, Jacob Siegel, Chris Allbritton |December 5, 2014 |DAILY BEAST 
Sen. Mary Landrieu did everything she could Monday night to salvage the shards of her bid for a fourth term in the U.S. Senate. Democrats Leave Senator Landrieu for Dead |Patricia Murphy |December 2, 2014 |DAILY BEAST 
Here were the sources (in part) of the Po and of the Rhine, but I was rather in haste to bid the former good-bye. Glances at Europe |Horace Greeley 
Nor shall we do more hereafter if you do my pleasure now and give this Monsieur de Garnache the answer that I bid you. St. Martin's Summer |Rafael Sabatini 
Nor can he sell the property to himself, nor authorize any other person to bid and purchase for him either directly or indirectly. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 
The clerk went as he was bid, but stupified and stunned by the information he had received. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
You had better go to him, Dolly, and bid him good bye, before he takes the team to the field. The World Before Them |Susanna Moodie