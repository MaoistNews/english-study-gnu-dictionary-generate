DC Comics uses a slavish adherence to the status quo to prevent anything socially progressive from taking place on its pages. DC Comics’ Diversity Crisis: Why the Status Quo Rules |Liz Watson |July 20, 2014 |DAILY BEAST 
She must whitewash these brown men and women, rid them of their savage, slavish ways, and repaint them in her own image. The Abused Wives of Westeros: A Song of Feminism in ‘Game of Thrones’ |Amy Zimmerman |April 30, 2014 |DAILY BEAST 
Zaks had to find the delicate poise between vivid restating and slavish reenactment. New York’s Greatest Show Or How They Did Not Screw Up ‘Guys and Dolls’ |Ross Wetzsteon |April 6, 2014 |DAILY BEAST 
This is a lie, and only the most slavish of Russian propagandists are claiming otherwise. Ron Paul Is Supporting Russia’s Illegal Occupation of Crimea |James Kirchick |March 16, 2014 |DAILY BEAST 
His recent opus, “Distorting Russia,” will go down in history as one of the most slavish defenses of Putinism. How the ‘Realists’ Misjudged Ukraine |James Kirchick |March 3, 2014 |DAILY BEAST 
The whole movement reached nothing beyond a slavish imitation of Giotto and his immediate followers. The History of Modern Painting, Volume 1 (of 4) |Richard Muther 
For she remembered now that but for their slavish devotion they might claim to be her equal. Devil's Ford |Bret Harte 
She would not save him to live the toilsome, slavish life of the Jews. Fair to Look Upon |Mary Belle Freeley 
All this goes in, and yet the book cannot be a slavish repeat. The Status Civilization |Robert Sheckley 
While confidence in his own abilities freed him from a slavish adherence to facts which could serve no useful purpose. A Drake by George! |John Trevena