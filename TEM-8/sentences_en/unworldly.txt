Only the unworldly could still think this was, at its worst, only an unseemly platonic relationship rather than a serious bonding. Queen Victoria’s Secret Scottish Sex Castle |Clive Irving |August 17, 2014 |DAILY BEAST 
McIlvain lays bare the curious culture of these uniquely unworldly young men. The Book of Mormon: A Novel |Alex Beam |March 14, 2013 |DAILY BEAST 
When she arrived at the academy, Fernandez was 17 and, Sousa told me, a shy, unworldly country girl. Hugo and the Hottie |Bryan Curtis |October 8, 2009 |DAILY BEAST 
Those people had been right who had called Sir Denis unworldly. Mary Gray |Katharine Tynan 
I am ashamed that for one moment I doubted your innocent, unworldly heart. Fifty-Two Stories For Girls |Various 
The sisters were strict in their observances, and gave a good pattern of the unworldly life, which attracted Ellenbog strongly. The Age of Erasmus |P. S. Allen 
You see, I've known her from a child, and a more unworldly creature never breathed. Somehow Good |William de Morgan 
He was an austere man, and had the reputation of being singularly unworldly, for a river man. Life On The Mississippi, Complete |Mark Twain (Samuel Clemens)