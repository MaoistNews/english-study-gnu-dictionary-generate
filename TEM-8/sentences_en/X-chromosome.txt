An x-ray two hours later confirms my hunch: my tibia (the big bone behind the shin) is snapped clean in two. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 
“Getting out of X band is on option,” said one senior Air Force official. Pentagon Worries That Russia Can Now Outshoot U.S. Stealth Jets |Dave Majumdar |December 4, 2014 |DAILY BEAST 
I made experimental commercials in the experimental division of a production house, Film X, that made commercials for ad agencies. The Renegade: Robert Downey Sr. on His Classic Films, Son’s Battle with Drugs, and Bill Cosby |Marlow Stern |November 26, 2014 |DAILY BEAST 
In fact, both brothers have acting IMDb credits: Drew on Smallville and Jonathan on The X-Files. How the Property Brothers Became Your Mom’s Favorite TV Stars |Kevin Fallon |November 25, 2014 |DAILY BEAST 
A bunch of old, white, rock titans come together with young, white, X Factor hotties to persuade Britain to heal Africa. Do They Know It’s Time to Stop Band Aid? |Tom Sykes |November 22, 2014 |DAILY BEAST 
He thought they were now in touch with our troops at "X" but that they had been through some hard fighting to get there. Gallipoli Diary, Volume I |Ian Hamilton 
W was a Watchman, and guarded the door; X was expensive, and so became poor. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 
Idly his pen traced upon the paper in front of him a large X, the sign of the unknown quantity. Uncanny Tales |Various 
The Professor took his pen and wrote a large X upon the sheet of paper in front of him. Uncanny Tales |Various 
Messrs. X and Z had specially engaged two eminent organists to play for them. The Recent Revolution in Organ Building |George Laing Miller