Since he doesn't have to impress me, it's clearly a little show for Alma. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 
A new reality series spotlights the extent people will go to impress a crush—from pretending to be deaf to committing theft. ‘My Crazy Love’ Reveals the Craziest Lies People Tell for Love |Kevin Fallon |November 18, 2014 |DAILY BEAST 
Hannigan pretended to be a basketball pro in order to impress a hot guy she had a crush on—only she had never played basketball. ‘My Crazy Love’ Reveals the Craziest Lies People Tell for Love |Kevin Fallon |November 18, 2014 |DAILY BEAST 
“He spent most of his time trying to impress me,” Campbell tells Piazza about a meeting with Ryan. The World Would Go to Hell Without Nuns |William O’Connor |September 4, 2014 |DAILY BEAST 
[Your superiors] become the people you most want to impress—and this is how you do it. ‘Kill Team’: The Documentary the Army Doesn’t Want You to See |Andrew Romano |July 26, 2014 |DAILY BEAST 
We have to remember that his daily life, where the home is orderly, helps to impress on him regularity of form. Children's Ways |James Sully 
Ramona herself bore no impress of sorrow; rather her face had now an added radiance. Ramona |Helen Hunt Jackson 
He took both her hands between his as he spoke; not so much, it seemed in affection, as to impress solemnity upon her. Elster's Folly |Mrs. Henry Wood 
This glow of feeling and exhilaration gave a new impress of sweetness and fascination to her beauty. Madame Roland, Makers of History |John S. C. Abbott 
Almost all the variety of the landscape is due to this impress of water action which has operated on the surface in past ages. Outlines of the Earth's History |Nathaniel Southgate Shaler