While all this was going on a deckhand had reeved a block and tackle through the end of the cargo gaff and passed it to the winch. Captain Scraggs |Peter B. Kyne 
"Fire," he bawled as the Maggie rested an instant in the trough of the sea—and a deckhand jerked the lanyard. Captain Scraggs |Peter B. Kyne 
He said to the deckhand: "Can I clean the lady and myself up?" The Syndic |C.M. Kornbluth 
It came when Lee grimaced at him and called the deckhand in a feeble murmur. The Syndic |C.M. Kornbluth 
The deckhand brought a glass of water from the adjoining lavatory and Charles washed down some of the tablets. The Syndic |C.M. Kornbluth