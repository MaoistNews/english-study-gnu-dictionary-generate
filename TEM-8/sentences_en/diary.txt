Like an undercover operative, Graham Greene mined his diaries, letters and interviews with misinformation to foil literary snoops. The dramatic — and embellished — life of Graham Greene |Michael Mewshaw |January 15, 2021 |Washington Post 
There are other methods of teasing out social connectivity beyond diaries and cellphone data. Who Should Get a Covid-19 Vaccine First? |Jill Neimark |November 20, 2020 |Singularity Hub 
There are other methods of teasing out social connectivity beyond diaries and cell-phone data. Who should get a covid-19 vaccine first? |Niall Firth |November 20, 2020 |MIT Technology Review 
All along, she has made videos at home, which feel like a diary of her pain and endurance. 11 compelling documentaries to watch for this fall |Alissa Wilkinson |October 9, 2020 |Vox 
A nice notebook or diary is a great gift for just about anyone. These notebooks are excellent gifts |PopSci Commerce Team |September 28, 2020 |Popular-Science 
Her experiences are recorded in a prison diary in which she has visions that have significance for the whole community. First Anglican Woman Bishop A Return to Christian Roots |Candida Moss |December 18, 2014 |DAILY BEAST 
She and her family arrived in Lebanon on Oct. 17, 2012, at 1:34 p.m.—she marked it in her diary. Drawing on the Memories of Syrian Women |Masha Hamilton |November 26, 2014 |DAILY BEAST 
Here, again, Angleton comes into the picture: In exchange for the diary, he promised Ben and Tony, he would destroy it. The Bizarre Tale of Ben Bradlee, JFK, and the Master Spy |Will Rahn |October 22, 2014 |DAILY BEAST 
In Berlin, Princess Blucher wrote in her diary, “Nothing is talked of but the expected entry into Paris.” Barbara Tuchman’s ‘The Guns of August’ Is Still WWI’s Peerless Chronicle |James A. Warren |September 29, 2014 |DAILY BEAST 
In his new book, ‘OZ Diary,’ Zahm explores 10 years of his life and art. Fashion's Naughtiest Photographer, Olivier Zahm |Justin Jones |September 9, 2014 |DAILY BEAST 
My memory is well stored, but unfortunately I have never kept a diary or commonplace book of any kind. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 
Confiding these matters to his "Diary" and keeping his own opinion, Mr. Adams passed on to Philadelphia. The Eve of the Revolution |Carl Becker 
This was noticeable in many ways, among others his passion for keeping a diary. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 
About the close of the seventies Tchaikovsky started a new diary, which he kept for about ten years. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 
Things which he never had set down in his diary—things which he did not tell to any one save his few friends. Cabin Fever |B. M. Bower