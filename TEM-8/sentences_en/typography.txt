In addition to their fascination with advertising, the designers of the 1950s and 60s were deeply interested in typography. 15 Most Bonkers Chairs at Pop Art Design in London |Chloë Ashby |October 23, 2013 |DAILY BEAST 
I even try to vary the handwritten typography to reflect changing historical periods. Allan Gurganus: How I Write |Noah Charney |October 16, 2013 |DAILY BEAST 
Danielewski uses experimental typography, elliptical descriptions, and color-coded parentheses. This Week’s Hot Reads: Oct. 7, 2012 |Nicholas Mancusi |October 7, 2012 |DAILY BEAST 
Though illiterate, Castle was drawn to the look and shapes of typography. The Accidental Artist |Rachel Wolff |October 15, 2009 |DAILY BEAST 
This consisted chiefly of a fine collection of English typography, and the 784 lots occupied four days in the selling. Prices of Books |Henry B. Wheatley 
In listing titles and imprints I have sought to follow the typography and punctuation of the originals. The Bibliography of Walt Whitman |Frank Shay 
They didn't think that just because a man could write nonsense and use erratic typography, that that made him a poet. Thin Edge |Gordon Randall Garrett 
Here, however, the picture is sober truth itself to what the inquiring reader finds in the typography. The Book-Hunter |John Hill Burton 
It is without date, but the typography seems to be that of the seventeenth century. A History of the Inquisition of Spain; vol. 4 |Henry Charles Lea