Having only seen the first couple of episodes, it’s obvious that Adriana is a little bit of a flirt. The ‘Real’ deal: An interview with Julia Lemigova of RHOM |Gregg Shapiro |January 6, 2022 |Washington Blade 
The girls were so blasé about the men who came in to flirt with them—I mean genuinely blasé. Barbara Hulanicki, Queen of Fast Fashion |Lizzie Crocker |October 15, 2014 |DAILY BEAST 
And Simon Cowell and Ryan Seacrest would openly flirt with each other on American Idol? In Praise of ‘Dating Naked’ and the Glorious Rise of Butts on Reality TV |Kevin Fallon |July 17, 2014 |DAILY BEAST 
One will not know until the next round—the quarterfinals—when this mediocre Brazil team will once again flirt with defeat. World Cup 2014 Nail-Biter: Host Country Brazil Defeats Chile on Penalty Kicks |Tunku Varadarajan |June 28, 2014 |DAILY BEAST 
So too the many variations on its theme, each fueled by our limitless urge to flirt. Patient’s STD Diagnosis Posted on Facebook |Kent Sepkowitz |June 6, 2014 |DAILY BEAST 
“You look like Dave Pirner,” she said to him, meaning the remark to sound like a small insult, but also a flirt. The Moment Kurt Cobain Met Courtney Love |Charles R. Cross |April 5, 2014 |DAILY BEAST 
I like him, said Dinah; he doesnt flirt with the girls; he always talks to the old ladies. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 
One can walk, flirt and dance in a Merveilleuse costume, but it is next to impossible to sit down in it. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 
Don't flirt with him,—that isn't the rle, but talk kindly to him, and thereby find out all you can about the Everett bunch. In the Onyx Lobby |Carolyn Wells 
That confounded money-eating little flirt of a Pansy will give me the royal shake the moment she gets wise. The Woman Gives |Owen Johnson 
There is something in his eye and the expressive flirt of his tail that seems to suggest strange doings. St. Nicholas, Vol. 5, No. 5, March, 1878 |Various