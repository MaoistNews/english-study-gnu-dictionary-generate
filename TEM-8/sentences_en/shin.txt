She will wriggle under tables and press herself firmly against shins, calves, you name it. For some dogs (and humans), the pandemic adoption rush was mostly a waiting game |Hau Chu |June 25, 2021 |Washington Post 
A few uses took care of lingering discomfort in my outer shin. These Are Our Favorite Massage Guns |Christine Yu |June 2, 2021 |Outside Online 
The four muscles, the quadriceps — that come in the front of the knee that attach just beyond the knee, in the front of the shin — they have to fire constantly to give you some stability. These Shoes Are Killing Me! (Ep. 296 Rebroadcast) |Stephen J. Dubner |May 20, 2021 |Freakonomics 
He then resisted two change-ups, both near his shins, and wiggled his hips from side to side. For Juan Soto, the home runs and the hype don’t matter. The work does. |Jesse Dougherty |April 1, 2021 |Washington Post 
Each of DyRET’s four legs has two telescopic sections, so that it can change the length of its thigh or shin bones. This Shape-Shifting Robot Can Rearrange Its Body to Walk in New Environments |David Howard |March 25, 2021 |Singularity Hub 
Shin eventually escaped North Korean captivity while in Vienna. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 
One of the most famous directors of this era was Shin Sang-ok (신상옥). Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 
Afraid the Korean secret police would not believe his kidnapping story, Shin settled in Hollywood. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 
An x-ray two hours later confirms my hunch: my tibia (the big bone behind the shin) is snapped clean in two. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 
So upon his release in 1997, Yousef agreed to a meeting with Shin Bet. When the Son of Hamas Spied for Israel |Nina Strochlic |August 5, 2014 |DAILY BEAST 
De yuther servants wouldn' miss me, kase dey'd shin out en take holiday soon as de ole folks 'uz out'n de way. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 
There's a neat pile of shin-plasters getting bigger and mounting up all the time. Menotah |Ernest G. Henham 
Walk fast now till you get away from the houses, and then shin for the raft like the dickens was after you! Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 
Instead of rising to his feet, he sat doggedly up and began chafing his abraded shin. The Fiend's Delight |Dod Grile 
Other warriors carry long oval shields reaching, at least, from neck to shin. Homer and His Age |Andrew Lang