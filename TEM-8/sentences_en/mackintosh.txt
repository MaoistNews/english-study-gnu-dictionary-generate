She stripped off her mackintosh, as though she were stripping off her modesty, and stood before him revealed. Hilda Lessways |Arnold Bennett 
She closed the stove door with a bang, and approaching, assisted in removing Edna's dripping mackintosh. The Awakening and Selected Short Stories |Kate Chopin 
In this battle, the English troops, under the command of Major Mackintosh, greatly distinguished themselves. The Mirror of Literature, Amusement, and Instruction, No. 366 |Various 
The crew from the Iris were at once landed at Riga, and only Mackintosh and my father put to sea again. The Czar's Spy |William Le Queux 
Yes, I can see the bungalow, and here is a mackintosh-clad figure hastening down the path to greet us. Round the Wonderful World |G. E. Mitton