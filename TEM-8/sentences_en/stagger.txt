Can SNL stagger on with Seth Meyers of Weekend Update halfway out the door, too? Leaving ‘SNL’? Bill Hader, Fred Armisen & Jason Sudeikis |The Daily Beast Video |May 19, 2013 |DAILY BEAST 
A miner puts his head down and runs, with a long swinging stride, through places where I can only stagger. Thatcher's Economic Legacy |Megan McArdle |April 8, 2013 |DAILY BEAST 
Unless we stagger the hours of medical service provision, all those people will end up in the emergency room. The Sleepless Economy |Megan McArdle |January 15, 2013 |DAILY BEAST 
His cheeks bright red, his chin wet with spittle, the helot would weave and stagger and totter until he passed out in the dirt. Persian Fire and Rubicon (Full) |David Frum |September 23, 2012 |DAILY BEAST 
The campaign will now stagger through the February doldrums. Money Changed Everything for Mitt Romney in Florida Primary |Paul Begala |February 1, 2012 |DAILY BEAST 
It was the 'Æneid,' and I began at your bookmark and tried to stagger through a page, but it floored me. A Hoosier Chronicle |Meredith Nicholson 
The sharper was one of those men who pull themselves together in a bad cause, as they stagger from the blow. Over the Sliprails |Henry Lawson 
A cry of joy burst from his lips, and he managed to stagger to his feet. Frank Merriwell's Pursuit |Burt L. Standish 
The kneeling figure sprang to his feet with a fierce stagger. The Napoleon of Notting Hill |Gilbert K. Chesterton 
He made several trips, the last of which was to stagger under a huge burden of spruce boughs. The Man of the Forest |Zane Grey