The father, Jean Paul Kruse, was later charged with rape and sexual abuse. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 
When it comes to the increasing number of rape allegations leveled at Bill Cosby, the smoke is becoming impenetrable. Butts, Brawls, and Bill Cosby: The Biggest Celebrity Scandals of 2014 |Kevin Fallon |December 27, 2014 |DAILY BEAST 
Dean Todd arranged for me to sit behind a screen and talk about my rape for a group of student leaders and activists. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 
Another bombshell: There is no statute of limitations on rape in the Commonwealth of Virginia. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 
It was shocking to find out that the rape by Beebe was actually the last one of the night. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 
Murder, rape, arson, and a host of other atrocities are often the first evidence of a diseased brain. Ancient Faiths And Modern |Thomas Inman 
He kept his promise, and then, after much entreaty, gave Helvetius a pinch of the powder—about as much as a rape-seed. Witch, Warlock, and Magician |William Henry Davenport Adams 
Miss Laura cleaned each cage, and gave each bird some mixed rape and canary seed. Beautiful Joe |Marshall Saunders 
In the Eleusinian mysteries the rape of Persephone by Pluto, the winter god, is portrayed. The Sex Worship and Symbolism of Primitive Races |Sanger Brown, II 
The rape of the Sabines is another incident suggesting the same conclusion. Elements of Folk Psychology |Wilhelm Wundt