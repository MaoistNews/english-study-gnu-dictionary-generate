The book, edited by former Yank magazine art director Art Weithas, featured visual art from the war and was a best seller. Blood in the Sand: When James Jones Wrote a Grunt’s View of D-Day |James Jones |November 15, 2014 |DAILY BEAST 
A caveat: the networks have been known to yank a few before they even make it on the air. Fall-Winter TV Preview: Snap Judgments of 2013–14’s New Shows |Jace Lacob, Kevin Fallon |July 16, 2013 |DAILY BEAST 
Fleet Street commentary at the time described him as the “pushy Yank” who became a celebrity out of sheer force of personality. America’s Power Brothers: Ezekiel Emanuel on the Famous Three |Lloyd Grove |April 4, 2013 |DAILY BEAST 
It was simply, we don't like this piece of art, so we are going to yank your city funding. Offensive Art: Those Were the Days |Michael Tomasky |March 28, 2013 |DAILY BEAST 
But let's give Lapid the benefit of the doubt, at least before we yank it back. How Yair Lapid's Gambit Ends |Bernard Avishai |March 7, 2013 |DAILY BEAST 
"I'll bet yo 'un a hoss I ken put a ball through that Yank's heart at fifty paces," he roared. The Courier of the Ozarks |Byron A. Dunn 
"Never teched the Yank or tree, either," and he kicked up his heels like a young colt. The Courier of the Ozarks |Byron A. Dunn 
Chief of the Yank-ton band, and principal orator of the nation; his body curiously tattooed. Adventures of the Ojibbeway and Ioway Indians in England, France, and Belgium; Vol. I (of 2) |George Catlin 
I'm a good enough Yank to see if your dinky police is such an all-fired cute little bunch of wonder-workers as you say! Murder in Any Degree |Owen Johnson 
“I reckon the ‘Little Yank’ has called your death sentence, pardner,” said one of the Confederates, roughly. A Daughter of the Union |Lucy Foster Madison