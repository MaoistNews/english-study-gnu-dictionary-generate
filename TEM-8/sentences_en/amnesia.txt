Mice injected with a cocktail of protein inhibitors develop amnesia, likely forgetting information because their synapses wither away. How technology can let us see and manipulate memories |Joshua Sariñana |August 25, 2021 |MIT Technology Review 
We cannot know whether the movies will survive the pandemic, streaming and cultural amnesia. ‘A Light in the Dark’ is a love letter to the directors, personal flaws and all |Troy Jollimore |April 12, 2021 |Washington Post 
Though suffering from amnesia, he turns out to be unflappably polite, irresistibly charming and quite frisky with the nurses. Cheer yourself up with light comedies from another era |Michael Dirda |March 31, 2021 |Washington Post 
Compounding the puzzle, as Bauer writes, is the fact that “within the period eventually obscured by childhood amnesia, children had remarkably rich autobiographies.” What happens to our lost childhood memories? Motherhood sent me looking for answers. |Missy Ryan |February 4, 2021 |Washington Post 
The precise age when the veil of infantile amnesia descends is a subject of ongoing debate, in part because only limited studies have been done involving children. What happens to our lost childhood memories? Motherhood sent me looking for answers. |Missy Ryan |February 4, 2021 |Washington Post 
Liberals are outraged over the Steven Scalise scandal—but the left has selective amnesia. Reverend Jeremiah Wright Was Worse Than Scalise |Ron Christie |January 2, 2015 |DAILY BEAST 
What are the real life consequences of our collective amnesia? The Bill Cosby Controversy Stages of Grief |Amy Zimmerman |November 18, 2014 |DAILY BEAST 
“I invented everything—amnesia, pain, hemorrhoids,” he told La Stampa. Proud to Be a Coal Miner Fraudster |Barbie Latza Nadeau |October 22, 2014 |DAILY BEAST 
But there is more to this behaviour than intentional amnesia. Beirut Letter: In Lebanon, Fighting ISIS With Culture and Satire |Kim Ghattas |September 22, 2014 |DAILY BEAST 
The first is what Scottish historian Tom Devine calls “imperial amnesia.” Scotland’s ‘Yes’ Campaign and the Myth of Scottish Equality |Noah Caldwell |September 18, 2014 |DAILY BEAST 
The one form of memory disturbance is called "Word Amnesia;" the other is called "Apraxia." Essays In Pastoral Medicine |Austin Malley 
Asked her friend abruptly, "Have you ever seen a case of amnesia?" Under the Law |Edwina Stanton Babcock 
For instance, I could have amnesia so that I could see you, but there wouldn't be any me. Under the Law |Edwina Stanton Babcock 
The temporary amnesia slipped aside and the veil began to rise. The Sex Life of the Gods |Michael Knerr 
When you cracked up, a blow on the head, or something, must have created a temporary amnesia and you thought you were Danson. The Sex Life of the Gods |Michael Knerr