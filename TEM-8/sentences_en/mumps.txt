The measles, mumps and rubella vaccine is required for most schoolchildren, and its protection usually lasts a lifetime. We May Never Eliminate COVID-19. But We Can Learn to Live With It |Jamie Ducharme |February 4, 2021 |Time 
Before that, the fastest vaccine to receive FDA approval in the US was the mumps vaccine, which took four years. How the newest vaccines fight Covid-19 |Kimberly Mas |February 2, 2021 |Vox 
The measles, mumps and rubella vaccine doesn’t work in babies under a year old. What kids need to know about getting a COVID-19 shot |Sheila Mulrooney Eldred |January 7, 2021 |Science News For Students 
The previous record for developing a vaccine is four years for mumps and that was more than five decades ago. Liberal, Educated … and Anti-Vaxxer: Pandemic Births New Vaccine Doubters |Charu Kasturi |August 25, 2020 |Ozy 
Vaccines for measles, mumps and tuberculosis are examples of live attenuated virus vaccines. A COVID-19 vaccine may come soon. Will the blistering pace backfire? |Tina Hesman Saey |July 10, 2020 |Science News 
It is the family of man—because where measles and mumps and pertussis are concerned, we are all connected. Colorado’s Anti-Anti-Vaxxer Bill Gets Watered Down |Kent Sepkowitz |April 23, 2014 |DAILY BEAST 
Mumps can be a serious and a very painful disease and it is infectious to a marked degree. Essays In Pastoral Medicine |Austin Malley 
One evening, he saw her again in the theatre with 'Mumps,' as she called her husband. The Child of Pleasure |Gabriele D'Annunzio 
She laughed and called to Mumps to come and unfasten her veil. The Child of Pleasure |Gabriele D'Annunzio 
One is called away in the middle of a dance to a difficult case of—of mumps or something, and—well, there you are. Happy Days |Alan Alexander Milne 
It was like toothache or mumps or chicken-pox, an ignoble, complaint of which one is ashamed, but before which one is helpless. The Honorable Percival |Alice Hegan Rice