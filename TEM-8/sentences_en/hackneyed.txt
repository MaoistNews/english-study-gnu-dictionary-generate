The Wilhelm scream is no hackneyed narrative device, but instead a nod to what Burtt calls the legacy of the language of sound. How the Wilhelm scream became Hollywood’s ultimate ‘secret’ sound effect |Michael Cavna |September 30, 2021 |Washington Post 
The word “unique” is so hackneyed that it’s a cliche to say it’s a cliche. Drew Pisarra’s ‘dangerously funny and queerly inventive brain’ |Kathi Wolfe |July 23, 2021 |Washington Blade 
If it’s a little hammy or hackneyed, it’s because it’s a pretty reliable formula that works. 3D escape rooms, 1980’s-themed murder mysteries: Companies invest in virtual retreats |Lucinda Southern |October 12, 2020 |Digiday 
Even the harmonized choral accents are hackneyed, ripped straight from her previous mega-hit “You Belong with Me.” Taylor Swift’s ‘1989’: Country’s Prodigal Daughter Creates the Best Pop Album of the Year |Marlow Stern |October 25, 2014 |DAILY BEAST 
Hackneyed chestnuts like that are reserved for old toastmasters, and yet, there we were. From Moscow to Queens, Down Sergei Dovlatov Way |Daniel Genis |September 15, 2014 |DAILY BEAST 
Sometimes Allen retools a hackneyed plot and the bones show through—not this time. Woody Allen’s Best & Worst Movies: ‘Annie Hall’ ‘Match Point’ & More (Video) |Malcolm Jones |July 26, 2013 |DAILY BEAST 
Hackneyed and dull, it feels like a lazy throwback in every sense of the word. Fall-Winter TV Preview: Snap Judgments of 2013–14’s New Shows |Jace Lacob, Kevin Fallon |July 16, 2013 |DAILY BEAST 
Everything seemed too hackneyed or unconvincing or simply impossible. A Mathematically Impossible Novel: Manil Suri Explains “The City of Devi” |Manil Suri |March 15, 2013 |DAILY BEAST 
To use a hackneyed figure, it was as though the earth had opened and swallowed her husband. Hooded Detective, Volume III No. 2, January, 1942 |Various 
Much like general terms, which mean something or nothing, are expressions that have become trite and hackneyed. English: Composition and Literature |W. F. (William Franklin) Webster 
I shall probably be met with the hackneyed cry, The question is entirely one of price. Landholding In England |Joseph Fisher 
You can't describe them unless you label them with the hackneyed interrogation point. My Wonderful Visit |Charlie Chaplin 
Each gentleman addressed her with some hackneyed compliment. The Child of Pleasure |Gabriele D'Annunzio