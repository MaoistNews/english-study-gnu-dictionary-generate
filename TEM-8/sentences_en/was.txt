Katherine Doyle: “The news says there was/has been a homicide in Danvers High School.” Colleen Ritzer Is the Second U.S. Math Teacher Slain in Two Days |Michael Daly |October 24, 2013 |DAILY BEAST 
And Texas was/is on its way to becoming a swing state by 2024 or so, so this could have an impact on that. Supreme Court and Voting Rights |Michael Tomasky |June 25, 2013 |DAILY BEAST 
The good count was/is a local big shot and benefactor, but I can't seem to find out anything else about him. The Things Not Asked; Tonight |Michael Tomasky |October 22, 2012 |DAILY BEAST 
Let's not lose sight of the other leg--the I-was-CEO-I-wasn't-in-charge-I-retired-retroactively-but-I-still-got-paid leg. The Emerging Theory on Mitt's Taxes: It's 2009 |Michael Tomasky |July 18, 2012 |DAILY BEAST 
I could not make out what it was, for the wind-was rustling the corn-shocks, but I arose and feigned to listen. The Soldier of the Valley |Nelson Lloyd 
Mr. Newdegate was a hard-mouthed witness, but he-was saddled, bridled, and ridden to the winning-post. Reminiscences of Charles Bradlaugh |George W. Foote 
Everything else-even endowments given by private persons a few years before the Act was passed-was swept away. Is Ulster Right? |Anonymous 
Burckhardt was on that 8:51 bus, every morning of every day-that-was-June-15th, never different by a hair or a moment. The Tunnel Under The World |Frederik Pohl 
He is a most capable and felicitous talker-was born for an orator, I think. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine