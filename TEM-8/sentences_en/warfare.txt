Soon the implications for warfare occupied everybody’s attention. How understanding nature made the atomic bomb inevitable |Tom Siegfried |August 6, 2020 |Science News 
Smoke released by wildfires in the Pacific Northwest in 2017 have already helped confirm computer models of nuclear warfare, says Alan Robock. Australian wildfires pumped smoke to record heights |Maria Temming |July 27, 2020 |Science News For Students 
History has suggested ancient societies left warfare to men. Skeletons hint that ancient societies had women warriors |Bruce Bower |May 28, 2020 |Science News For Students 
There’ll be global shortages leading to warfare breaking out all over the world. Reasons to Be Cheerful (Ep. 417) |Stephen J. Dubner |May 7, 2020 |Freakonomics 
We’ve been a leader in innovations, they’ve dramatically changed warfare, and in our favor. Speak Softly and Carry Big Data (Ep. 395) |Stephen J. Dubner |October 31, 2019 |Freakonomics 
The priority that the regime places on cyber warfare is made clear by its recruiting. Inside the ‘Surprisingly Great’ North Korean Hacker Hotel |Michael Daly |December 20, 2014 |DAILY BEAST 
Mired in ideological warfare, America faces her most formidable opponent yet— herself. The Post-Brown and Garner Question: Who ‘Deserves’ to Die? |Goldie Taylor |December 9, 2014 |DAILY BEAST 
They were making it up as they went along, and creating a new kind of warfare. How the NSA Became a Killing Machine |Shane Harris |November 9, 2014 |DAILY BEAST 
In the opening moments of Call of Duty: Advanced Warfare, players are introduced to 2054 Seoul. Kevin Spacey Stars as a Frank Underwood-like Warmonger in ‘Call of Duty: Advanced Warfare’ |Alec Kubas-Meyer |November 8, 2014 |DAILY BEAST 
Think advanced unmanned vehicles, all-aspect, broadband stealth, and undersea warfare. The Pentagon May Finally Have a Plan to Keep America on Top |Bill Sweetman |November 3, 2014 |DAILY BEAST 
I never had to fight again except as an unwilling participant in our foreign warfare. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 
It is true that they wanted the picturesque splendour of ancient warfare. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
One of the latest uses to which asbestos has been proposed to be applied in connection with warfare is as a coating for ironclads. Asbestos |Robert H. Jones 
Balfour Browne and Littler now conducted the warfare on either side, and keenly they fought. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 
Organisation was his first work, and his former experience of irregular warfare in Poland stood him in good stead. Napoleon's Marshals |R. P. Dunn-Pattison