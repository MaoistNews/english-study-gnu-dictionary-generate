Yet even someone as senior as Xie is unlikely to have full remit to negotiate on his country’s behalf. China can make or break a global climate deal. What will it be willing to give? |Lily Kuo |October 29, 2021 |Washington Post 
Space travel has, for the most part, remained the remit of professional astronauts or the very wealthy. Inspiration4: Why SpaceX’s first all-private mission is a big deal |Jonathan O'Callaghan |September 15, 2021 |MIT Technology Review 
Immigration is just one of the difficult topics under his remit. Can One Agency Keep the U.S. Safe and Still Be Humane? The New DHS Chief Thinks So |Alana Abramson |May 12, 2021 |Time 
Its remit is narrow—it does not have jurisdiction over government agencies, banks, or nonprofits. This has just become a big week for AI regulation |Will Douglas Heaven |April 21, 2021 |MIT Technology Review 
That is a complex remit that they have been happy to leave to customer-facing teams. Why marketing can no longer ignore customer experience |Trevor Grigoruk |March 2, 2021 |Digiday 
But that was not within the remit of the Dutch investigators. MH17 Switched Places With Another Jet |Clive Irving |September 9, 2014 |DAILY BEAST 
He questioned whether preventing pictures being taken was part of the police protection unit's remit. James Middleton - Should Royal Police Have Parked His Car? |Tom Sykes |October 1, 2012 |DAILY BEAST 
Fifty minutes later, when the remit to oversee the bid was awarded to Hunt, Osborne replied, “I hope you like the solution!” Culture Minister Jeremy Hunt ‘Sympathetic’ to Murdoch BSkyB Bid |Peter Jukes |May 31, 2012 |DAILY BEAST 
The French war indemnity enabled him to redeem a considerable portion of the state debt and to remit certain taxes. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 
He added, that Mrs. Grants father was extremely affluent, and he should not wonder if he was to remit 500l. The Chronicles of Crime or The New Newgate Calendar. v. 1/2 |Camden Pelham 
For cabin passage in yonder vessel, tax free and duly paid, we will remit the rest. Where the Pavement Ends |John Russell 
The American Consuls can give you the names and the amounts to remit for single copies. Harper's Round Table, June 4, 1895 |Various 
The beleaguerers made themselves great fires, and seemed not to remit in their watchfulness. A Legend of Reading Abbey |Charles MacFarlane