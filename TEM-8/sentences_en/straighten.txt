The third is a party whose leaders force reality onto the base, figuring out how to reclaim authority over conservative Americans and straightening out the media ecosystem that supports it. The Republican Party tries to figure out the path forward |Philip Bump |February 2, 2021 |Washington Post 
Patients self-report their pain levels using a survey that asks about pain during various activities, such as fully straightening their knee. AI could make healthcare fairer—by helping us believe what patients say |Karen Hao |January 22, 2021 |MIT Technology Review 
Salas said it was particularly important to get all of the facts straightened out. Chula Vista Police Chief Says She Didn’t Know Department Shared Data With Feds |Gustavo Solis |January 20, 2021 |Voice of San Diego 
His initial charge was to straighten a scouting operation riddled by scandal. Nationals sign 16-year-old Dominican shortstop Armando Cruz to $3.9 million bonus |Jesse Dougherty |January 15, 2021 |Washington Post 
I’m definitely going to vote, so I have to get it straightened out. Undecided Voters Weigh In: “I Honestly Don’t Know” |Nick Fouriezos |October 14, 2020 |Ozy 
Instead, straighten your civic backbone and push back in clear conscience. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 
Morris struggled to straighten his back, which involved stiffening a spine rarely used. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 
Fitzpatrick was supposed to assess the situation in Boston and make recommendations to straighten things out. Whitey Bulger’s Defense to Reveal Widespread FBI Complicity |T.J. English |July 30, 2013 |DAILY BEAST 
The idea was to put the fear of God in Fatah so they would straighten up, but instead Hamas won—and so did Israel. Don’t Tread On My Hair, Hamas |Maysoon Zayid |April 4, 2013 |DAILY BEAST 
Smith did her best, at that press conference, to straighten the whole mess out. Whose Elephant Is It? |Winston Ross |December 9, 2012 |DAILY BEAST 
The newsboys began to call—the young nurse woke up and began to straighten her hair. Love's Pilgrimage |Upton Sinclair 
But, now, to straighten out this coil; to shake himself finally free of Chloe, and make Daphne happy again! Marriage la mode |Mrs. Humphry Ward 
Observing our left column was in advance of the right, Cornstalk was attempting to straighten his line by pulling in his left. A Virginia Scout |Hugh Pendexter 
Hurry and shave, you know how smooth Jasper Jones' chin always looks, and then straighten up this room. Time Enough at Last |Lyn Venable 
Spent day in camp trying to straighten things out: the personal, the strategical and the administrative arrangements. Gallipoli Diary, Volume 2 |Ian Hamilton