In one 2009 experiment, college students studied vocabulary words with flash cards. Top 10 tips on how to study smarter, not longer |Kathiann Kowalski |September 9, 2020 |Science News For Students 
The darkening outlook of banks is laid bare by disclosures on so-called criticized loans, which are flashing warning signals about a borrower’s ability to pay. A Loan Crisis Looms for Commercial Property |Daniel Malloy |September 8, 2020 |Ozy 
Plus its flash technology allows upgrades to future software versions so you won’t have to continually invest in new calculators. The best graphing calculators for students |PopSci Commerce Team |September 4, 2020 |Popular-Science 
Disrupt 2020 Labor Day flash sale — Starting today, you can save $100 off the price of a Disrupt Digital Pro Pass. Daily Crunch: Apple delays ad-tracking changes |Anthony Ha |September 3, 2020 |TechCrunch 
When I briefly regained consciousness, it was to flashing police and ambulance lights and what felt like an entire roll of paper towels pressed on my face. The Accidental Attempted Murder |Eugene Robinson |September 2, 2020 |Ozy 
The idea that January 1st initiates a period of new beginning is not a flash of Hallmark brilliance. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 
Afterward, you can actually see her young career flash before her eyes as she makes a kind of puffed up blowfish face. The Curious Little Shell That Restarted Jenny Slate’s Career |Luke Hopping |December 15, 2014 |DAILY BEAST 
In a flash he deflects the shot, with the speed of instinct, right past the goalkeeper. Is Soccer Great Lionel Messi Corrupt? |Jack Holmes |December 8, 2014 |DAILY BEAST 
Cops launched a flash grenade through the window and officer Joseph Weekley fired, fatally striking Stanley-Jones. Worse Than Eric Garner: Cops Who Got Away With Killing Autistic Men and Little Girls |Emily Shire |December 4, 2014 |DAILY BEAST 
The grasp on the sabre would tighten; the quiet eyes would flash. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 
The vision—it had been an instantaneous flash after all and nothing more—had left his mind completely for the time. The Wave |Algernon Blackwood 
A flash of surprise and pleasure lit the fine eyes of the haughty beauty perched up there on the palace wall. The Red Year |Louis Tracy 
"A camp-fire would hardly flash and die out like that, Sarge," he answered thoughtfully. Raw Gold |Bertrand W. Sinclair 
If those jaspers flash any part of the roll in the Territory before snowfall, I'll get them. Raw Gold |Bertrand W. Sinclair 
He devoured it whole with a kind of visual gulp—a flash; the entire meaning first, then lines, then separate words. The Wave |Algernon Blackwood