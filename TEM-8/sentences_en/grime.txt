It will start to get yellow and less translucent as you wear it and it picks up grime and dirt. The best Apple Watch case to protect the computer on your wrist |Stan Horaczek |August 23, 2021 |Popular-Science 
Eventually a bit of grime might develop around the mouth of the jar, but you can simply wipe it off with a clean, wet towel. Forget Canning, and Get Yourself a Chinese Pickle Crock |Clarissa Wei |July 14, 2021 |Eater 
Beyond the regular sweat and grime of daily life, pollution also plays a big role when it comes to wrinkles. 7 things you can do to actually prevent wrinkles |Sandra Gutierrez |July 13, 2021 |Popular-Science 
With an IP54 rating, the charging case also protects against rain and grime, but if you drop it in the dirt you’ll want to wipe it down rather than run the faucet over it. Jaybird Vista 2 review: Customizable sound for all athletes |Billy Cadden |June 21, 2021 |Popular-Science 
Use a small brush, like an old toothbrush, to dislodge as much grime as you can, then vacuum it all up. How to fix a sliding door without taking it down |John Kennedy |March 19, 2021 |Popular-Science 
Cars have national attributes and GM wants their luxury line to grab the glitz of New York instead of the grime of Detroit. Nationalism on Four Wheels |Clive Irving |October 18, 2014 |DAILY BEAST 
Restorers completed a 12-year project in 1998 that cleaned decades of grime from the ceiling. Grand Central Terminal: 100 Years, 100 Facts |Sarah Begley |February 1, 2013 |DAILY BEAST 
That grime came from the cigarette smoke of millions of commuters. Grand Central Terminal: 100 Years, 100 Facts |Sarah Begley |February 1, 2013 |DAILY BEAST 
“I am looking to get into the grime rap UK scene,” he told The Sun. Harry Potter Hip-Hop |Jaimie Etkin |July 13, 2011 |DAILY BEAST 
Faith may bolster the ascetic, but boredom wears him down—grime and solitude breed apathy. A Book About Boredom Is Anything But |Jeremy Axelrod |June 20, 2011 |DAILY BEAST 
Inly wondering why any one should wish to conceal such a trifle, I drew it forth, rubbing the grime and dust from it as I did so. A Fortune Hunter; Or, The Old Stone Corral |John Dunloe Carteret 
The students brought candles by which to study until it became light, and the roof was soon black with the grime and smoke. The Private Life of the Romans |Harold Whetstone Johnston 
They presented an uncouth spectacle bedraggled as they were with grime and dirty water. Spacewrecked on Venus |Neil R. Jones 
A cinder stung her face, and when she lifted her hand to the spot, she saw that her glove was black with grime. The Trail of the Lonesome Pine |John Fox, Jr. 
But he was covered with grime and dust and his clothing was torn and streaked with blood. The Secret Witness |George Gibbs