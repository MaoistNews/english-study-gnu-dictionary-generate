When I tell her that Clooney is rumored to also kiss the Dowager Countess during the episode, she chuckles. Elizabeth McGovern on the ‘Downton Abbey’ Xmas Album and Lady Grantham’s Kiss with George Clooney |Marlow Stern |November 13, 2014 |DAILY BEAST 
Vacations are as mysterious to me as weekends are to the Dowager Countess of Grantham. One First-Timer’s Adventures in Culebra and Puerto Rico |Kara Cutruzzula |March 14, 2013 |DAILY BEAST 
Dame Maggie Smith's fiery Dowager Countess will get a confidante in the form of Lady Shackleton, played by Dame Harriet Walters. The Red Blooded Blue Bloods Hoping To Mend Lady Mary's Heart |Tom Sykes |March 4, 2013 |DAILY BEAST 
Since her husband's death in 1999, she has been queen dowager of Jordan. Queen Noor of Jordan Demanded her Gardener Grow Mangoes in Chilly England |Tom Sykes |May 2, 2012 |DAILY BEAST 
He called up his mother, the dowager countess, and asked her to come over and look after the children. Lord Lucan’s Whereabouts: The Tabloid Rebirth of a Decades-Old Crime |William Coles |February 25, 2012 |DAILY BEAST 
How little did he divine that the letter of the doctor was called forth by a communication from the countess-dowager. Elster's Folly |Mrs. Henry Wood 
The old dowager's voice toned down, and she pulled her black feathers straight upon her head. Elster's Folly |Mrs. Henry Wood 
The doctor had been spending Easter at Cannes, and the dowager had devoutly prayed that he might not yet return. Elster's Folly |Mrs. Henry Wood 
And the countess-dowager fanned herself complacently, and neither she nor Maude cared for the absence of a groomsman. Elster's Folly |Mrs. Henry Wood 
The countess-dowager was not very adroit at spelling and composition, whether French or English, as you observe. Elster's Folly |Mrs. Henry Wood