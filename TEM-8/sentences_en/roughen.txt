The rubber used should not be of a harsh grade, since that will roughen the face of the paper and probably cause the ink to run. Mechanical Drawing Self-Taught |Joshua Rose 
Take a piece of sand-paper and roughen the edges of the break as far around as the size of the patch. Secrets of Wise Men, Chemists and Great Physicians |William K. David 
The path, to be sure, does not roughen until one has gone along it for twenty-eight hundred miles. American Big-Game Hunting |Various 
Heading weatherward, he gave more heed to the wheel, for there were signs that the water was going to roughen somewhat. The Motor Boat Club and The Wireless |H. Irving Hancock 
No amount of the hardest known abrasive will even roughen its surface. The Skylark of Space |Edward Elmer Smith and Lee Hawkins Garby