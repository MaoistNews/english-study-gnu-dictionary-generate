You may not often encounter “burgeoning” and “kelp” in the same sentence, but the seaweed is truly having a moment. Can Kelp Be Trendy? Farmers Are Betting on It. |klindsey |July 16, 2021 |Outside Online 
He raises oysters, clams, mussels — and kelp, that brown, slippery seaweed that looks like packing tape. Is the Future of Farming in the Ocean? (Ep. 467) |Stephen J. Dubner |June 24, 2021 |Freakonomics 
Carrageenan extracted from seaweed is used in products all over the world, but like so many other supply chains, this one has been unfair to local communities. How Surprising Connections Can Save the Ocean - Issue 99: Universality |Mary Ellen Hannibal |April 29, 2021 |Nautilus 
This model has diversified from nets to include seaweed farming in the Philippines. How Surprising Connections Can Save the Ocean - Issue 99: Universality |Mary Ellen Hannibal |April 29, 2021 |Nautilus 
Kayanda recently recruited 25 women on Pemba, an island off the Tanzanian coast, to start a production plant for beauty products made from seaweed. She’s Using the Quran to Fight the Patriarchy |Daniel Malloy |January 6, 2021 |Ozy 
On the other hand, he found my wasabi seaweed a bit off-putting. Tales of a Jailhouse Gourmet: How I learned to Cook in Prison |Daniel Genis |June 21, 2014 |DAILY BEAST 
Rich in dietary fiber and calcium, hijiki is a black-colored seaweed that grows wild on the Japanese coastline. What to Eat: Japanese Small Plates |Cookstr.com |May 18, 2010 |DAILY BEAST 
Poudre de Neptune (dill, fennel, star anise, seaweed) is by far the most-used seasoning in my kitchen. A Superstar Chef Does the Unthinkable |Amelia Smith |November 17, 2008 |DAILY BEAST 
The smell of wild thyme mingling with the salt of the low-tide seaweed conveyed stimulating fragrance. Jaffery |William J. Locke 
They had dug some clams at the low tide in the forenoon and put them away, covered with wet seaweed. The Rival Campers |Ruel Perley Smith 
The uniform level of the seaweed marked the line of the water at the height of the tide, and the limit of the sea in calm weather. Toilers of the Sea |Victor Hugo 
Some, covered with a hairy and glutinous seaweed, seemed like large green moles boring a way into the rock. Toilers of the Sea |Victor Hugo 
Do you see that orange-and-black striped blazer—there by the seaweed: he's pointing; that's Philip Lacey. Mushroom Town |Oliver Onions