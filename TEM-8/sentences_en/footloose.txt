That footloose pair was adding day hikes to their thru-hike because they wanted to have more fun. A Thru-Hiker’s Ode to the Humble Day Hike |awise |July 28, 2022 |Outside Online 
They were poor and footloose, and they found a fresh way to snub conventions about how a woman ought to live. Few books are timelier than ‘Woman’ |Kathi Wolfe |March 25, 2022 |Washington Blade 
On a footloose pilgrimage to the American Southwest, Wright realized that sometimes the best approach to a creative project is to just wing it and hope everything works out. Life Lessons from Elite Explorers |Outside Editors |December 17, 2020 |Outside Online 
If Footloose, the Kevin Bacon film from 1984, ever gets an Asian remake, Tokyo might be the place to shoot it. The Japanese Go All ‘Footloose’ to Protest Nightlife Crackdown |Jake Adelstein, Angela Erika Kubo |May 1, 2014 |DAILY BEAST 
They also banned the "Arab Idol" competition because anyone who has ever watched Footloose knows music is evil. Don’t Tread On My Hair, Hamas |Maysoon Zayid |April 4, 2013 |DAILY BEAST 
Footloose without Bacon is like Thanksgiving without the turkey! Lights, Camera, Cocktails |Brody Brown |October 16, 2011 |DAILY BEAST 
She lived as a footloose single lady, the kind who indulged in permanent waves and would drop $20 on a coat with a fur collar. Do I Have to Read Committed? |William Boot |January 30, 2010 |DAILY BEAST 
Laurenti wowed at the Boston auditions with her jazzy performance of “Blue Skies” and won fans with her Footloose-inspired story. Best and Worst American Idol Auditions |The Daily Beast Video |January 21, 2010 |DAILY BEAST 
This was simply because all Long's dealers were doing a Monte Carlo business in back and he was the only one footloose. Hooded Detective, Volume III No. 2, January, 1942 |Various 
Imry picked Danson because hes a footloose artist who paints illustrations for magazines. The Sex Life of the Gods |Michael Knerr 
The whole success of Tarrant Enterprises, Ltd., hinged on its being entirely footloose. Trading Jeff and his Dog |James Arthur Kjelgaard 
Was it a year ago, or only a few days, that he had been the footloose owner-manager-working force of Tarrant Enterprises, Ltd.? Trading Jeff and his Dog |James Arthur Kjelgaard 
But I do hope you've got your railroad built and are footloose and free to take another commission. Pirates' Hope |Francis Lynde