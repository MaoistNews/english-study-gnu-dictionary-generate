It’s bad news if a flash flood comes when people are caught unawares, stuck at home, or in traffic. What is a flash flood? |Maria Parazo Rose |August 3, 2022 |Popular-Science 
I found it extremely disheartening that even after so many years of dealing with the war’s aftershocks, I can still be caught unawares of their intensity. You Never Stop Being a Child of War |Nadja Halilbegovich |April 26, 2022 |Time 
And, while these shifts are designed to make search easier for consumers, many businesses have been caught unawares, losing visibility on Google. How marketers can adapt to Google’s local SEO changes |Corey Patterson |November 24, 2021 |Search Engine Land 
Know where your risks are, so that you’re not caught unawares when it happens. Why a global chip shortage is screwing up America’s pickup trucks |Rebecca Heilweil |April 9, 2021 |Vox 
Such a woman would be much more difficult to catch unawares than a teenage escort trapped in a motel room. Indiana Serial Killer’s Confession Was Just the Start |Michael Daly |October 21, 2014 |DAILY BEAST 
As a result, journalists are often caught unawares when they pop. How Twitter Got Huge |Daniel Gross |November 7, 2013 |DAILY BEAST 
And yet strange stuff happens all the time that takes investors and traders completely unawares. Welcome to the Anarchy Economy |Daniel Gross |April 23, 2013 |DAILY BEAST 
Joined by international and Israeli activists, all wearing tee-shirts that read “I have a dream,” they caught the army unawares. "Obama, Come Here To Hebron" |Ali Gharib |March 20, 2013 |DAILY BEAST 
The Italian fleet was caught unawares by British warships, who sunk three cruisers and two destroyers. Philip: How I Sunk Italian Cruisers |Tom Sykes |April 24, 2012 |DAILY BEAST 
This he did till an evil day when Aunty Rosa pounced upon him unawares and told him that he was "acting a lie." Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 
And hence, possibly, arose the disquieting sensation that something was gathering, something that might take them unawares. The Wave |Algernon Blackwood 
Instantly the captain sought to take advantage of it, thinking to catch Garnache unawares. St. Martin's Summer |Rafael Sabatini 
He did not dare to call assistance; an unguarded word might have slipped out unawares. Elster's Folly |Mrs. Henry Wood 
Queeker had been taken unawares, but he pressed his knees together, knitted his brows, and resolved not to be so taken again. The Floating Light of the Goodwin Sands |R.M. Ballantyne