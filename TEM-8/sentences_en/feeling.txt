We wouldn’t get the same feeling at the growth stage versus working with small teams or a single founder. With Goat Capital, Justin Kan and Robin Chan want to keep founding alongside the right teams |Eric Eldon |September 17, 2020 |TechCrunch 
Economists tend to have mixed feelings about unions, pointing out their inefficiencies and inflexibilities. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 
Every time, I have this feeling that it’s going to be forever. The best thing for back pain is actually more movement |Sara Chodosh |September 16, 2020 |Popular-Science 
When we think about communicating, we tend to think about our own thoughts and feelings rather than how we might be influencing others. The Dark Side of Smart - Facts So Romantic |Diana Fleischman |September 15, 2020 |Nautilus 
She said if Oracle or any new owner changed TikTok, it would be “big” and could potentially change the organic feeling of community that the app has created. What’s Oracle? TikTok users react to proposed Oracle deal |Danielle Abril |September 15, 2020 |Fortune 
Something like fluoride, which is too small for normal filters, yanks away that feeling of agency. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 
But there is an underlying feeling that the worst is yet to come. Europe’s Islam Haters Say We Told You So |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 
We have to share those feelings of concern that the people are feeling. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 
I had a feeling that Turkish authorities were closing their eyes. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 
Within a few swipes, I was already feeling that burst of romantic optimism you need the first day of the (Christian) new year. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 
It is full of poetic feeling, and the flesh tints are unusually natural. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 
Alessandro turned a grateful look on Ramona as he translated this speech, so in unison with Indian modes of thought and feeling. Ramona |Helen Hunt Jackson 
Selections for practice should be chosen which contain much variety of thought and feeling and are smooth in movement. Expressive Voice Culture |Jessie Eldridge Southwick 
It was with a feeling of relief on both sides that the arrival of Mr. Haggard, of the Home Office, was announced. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
The little boy of two, often quoted here, showed a punctilious feeling for order in the placing of things. Children's Ways |James Sully