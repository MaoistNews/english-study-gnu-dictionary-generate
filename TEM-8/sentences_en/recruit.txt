The recruits wore shoes with the sharp kirigami spikes attached and walked on ice. Shape-shifting cuts give shoes a better grip |Carolyn Wilke |July 14, 2020 |Science News For Students 
Each recruit collected some of their feces before the study began. Our gut microbes love a good workout |Silke Schmidt |May 21, 2020 |Science News For Students 
During the workout weeks, the recruits did not change what they ate — except for three days before each poop collection. Our gut microbes love a good workout |Silke Schmidt |May 21, 2020 |Science News For Students 
By the end of the study, the recruits had more SCFA-producing microbes than at the start. Our gut microbes love a good workout |Silke Schmidt |May 21, 2020 |Science News For Students 
Then they track how, on average, the health of the recruits have changed over time. Decades-long project is linking our health to the environment |Lindsey Konkel |March 12, 2020 |Science News For Students 
He also was working to recruit Castro as a driver for a drug load. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 
Ramos was 38—nearly two decades older than the average recruit. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 
The company declined to comment on their efforts to recruit more women, but the current drivers say they are working hard at it. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 
But then something funny happened: The GOP actually began to recruit black and Hispanic candidates. The Republican Rainbow Coalition Is Real |Tim Mak |November 18, 2014 |DAILY BEAST 
She is accused of using her celebrity to recruit socially disadvantaged minors with the potential to become professional models. Colombian Beauty Queen Arrested for Running Child Prostitution Ring |Jason Batansky |October 17, 2014 |DAILY BEAST 
Two years later this promising recruit, having fallen foul of the military authorities, had to leave the service under a cloud. Napoleon's Marshals |R. P. Dunn-Pattison 
A serjeant enlisted a recruit, who on inspection turned out to be a woman. The Book of Anecdotes and Budget of Fun; |Various 
Pernambuco had during the half century which had elapsed since the expulsion of the Dutch had time to recruit. Journal of a Voyage to Brazil |Maria Graham 
Young warriors returning home to recruit their health, or to die. The Floating Light of the Goodwin Sands |R.M. Ballantyne 
Before the next Sunday, Mrs Wood had taken her daughter to her distant home, to recruit in that quiet place. Ruth |Elizabeth Cleghorn Gaskell