According to them, we can get sick with leftover food, which also attracts all kinds of vermin that feed on food debris. Locked up in the Land of Liberty: Part III |Yariel Valdés González |July 21, 2021 |Washington Blade 
Without the beeps and whirs of a cellphone, you can use your ears to detect crickets, mice, or other vermin in your home. Aubrey Plaza’s Great Disconnect |Aubrey Plaza |August 15, 2014 |DAILY BEAST 
The RIP offers a clear window into the intersection of poverty and vermin. Crowdsourcing NYC’s War on Rats |Kevin Zawacki |June 24, 2014 |DAILY BEAST 
“They kept the apartment filthy and they had vermin problems,” the neighbor says. NY Couple Not Terrorists, Say Cops, Just Rich Kids With Drug Habits |Michael Daly, Lizzie Crocker |January 1, 2013 |DAILY BEAST 
Sandy may actually help the vermin spread diseases, as a matter of fact. Hurricane Sandy Worsens N.Y. Rat Problem |Winston Ross |October 31, 2012 |DAILY BEAST 
If you thought Frankenstorm would rid Gotham of its vermin, think again. Hurricane Sandy Worsens N.Y. Rat Problem |Winston Ross |October 31, 2012 |DAILY BEAST 
Others speak of vermin, noxious plants, or instances of uncleanliness. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 
The inhabitants were cowering upon the floor, playing with the children, or assisting one another to get rid of their vermin. A Woman's Journey Round the World |Ida Pfeiffer 
Athenæus assures us that he was carried, like a bird, in an iron cage until he was devoured by vermin. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 
Every instinct and habit made her a stranger among these poor swamp-people living like vermin in their lair. Summer |Edith Wharton 
Arrangements should be made for keeping the area clear of vermin and for ventilating and draining it. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various