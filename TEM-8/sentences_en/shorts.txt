The first public screening of Pathé film shorts took place sometime around 1903. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 
Our first film was all visual with no sound, and it turned into No More Excuses—which is five shorts in one intercut. The Renegade: Robert Downey Sr. on His Classic Films, Son’s Battle with Drugs, and Bill Cosby |Marlow Stern |November 26, 2014 |DAILY BEAST 
I stripped down to my gym shorts and stretched out on my cot. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 
A photo of the most recent professional tournament showed a fully male, predominantly white, t-shirt and cargo-shorts-clad top 8. Is ‘Magic: The Gathering’ Immune to GamerGate Misogyny? |David Levesley |October 29, 2014 |DAILY BEAST 
He wears a black Under Armour T-shirt, red basketball shorts, sneakers, and white socks hiked up to his calves. The Ugly Truth About Cory Booker, New Jersey’s Golden Boy |Olivia Nuzzi |October 20, 2014 |DAILY BEAST 
She had torn open bags of beans, shorts, bran, chop-feed and cotton-seed meal. The Red Cow and Her Friends |Peter McArthur 
He stood up straight and lean-muscled, in a pair of duck shorts. The Devil's Asteroid |Manly Wade Wellman 
The greater part of the year in Mesopotamia the regulation army dress consisted of a tunic and "shorts." War in the Garden of Eden |Kermit Roosevelt 
The greater the number of the too longs or the too shorts the greater his complacence in the contemplation of his labours. Blue Goose |Frank Lewis Nason 
I charge each hog $1 for bran and shorts; this is all the ready money I pay out for him. The Fat of the Land |John Williams Streeter