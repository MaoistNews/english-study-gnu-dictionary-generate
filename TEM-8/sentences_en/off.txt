Although the blood-spattered offices will be off-limits, staff have vowed to continue producing the magazine. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 
A passing off-duty school safety officer named Fred Lucas said that he had been told the man was a drug dealer. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 
The NOPD fired Knight in 1973 for stealing lumber from a construction site as an off-duty cop. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 
The off-year special election into which Duke threw himself drew little media notice at first. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 
Aaron Paul may play a young Han Solo in the first Star Wars spin-off. Juiciest ‘Star Wars: The Force Awakens’ Rumors (and Some Debunked Ones) |Rich Goldstein |January 3, 2015 |DAILY BEAST 
A far-off volley rumbled over the plain, and a few birds stirred uneasily among the trees. The Red Year |Louis Tracy 
Jean was to be an architect—God knows why—but Aristide settled it, definitely, off-hand. The Joyous Adventures of Aristide Pujol |William J. Locke 
In favorable parts of the trail he must do better than that, to off-set losses of time where the going was most difficult. Motor Matt's "Century" Run |Stanley R. Matthews 
There was something about the man that Matt liked, in spite of the deceit he had practised at the start-off of their acquaintance. Motor Matt's "Century" Run |Stanley R. Matthews 
She did not take the broad, beaten road which led to the far-off plantation of Valmonde. The Awakening and Selected Short Stories |Kate Chopin