You can't certainly if you hobnob with the rival candidate and call him a good fellow. The Opinions of a Philosopher |Robert Grant 
Im sure they used to hobnob and drink brandy and water together in seedy public-houses. The Bishop's Apron |W. Somerset Maugham 
Jacinto Quesada had not stopped in passing to hobnob with the Gypsies. The Wolf Cub |Patrick Casey 
He did not relish having to hobnob in this way with such a vulgarian as a grafting police captain. The Third Degree |Charles Klein and Arthur Hornblow 
There were even ladies in bonnets, as if they had run in neighborly to hobnob an hour with Iwakura. From the Easy Chair, series 3 |George William Curtis