Much like a hangover, there is no precise cure for this ailment. Tried and true tricks for getting over jet lag |Rachel King |December 27, 2020 |Fortune 
“The hangover of Brexit starts now,” says Nicholas Bloom, an economist at Stanford who has studied the economic effects of the 2016 referendum. The Brexit deal is done but the damage to the UK economy is just getting started |Walter Frick |December 24, 2020 |Quartz 
The looming policies investors are ignoring could bring a long hangover. Stocks in ‘election-sensitive’ sectors seem oblivious to which candidate wins. Why? |Shawn Tully |October 18, 2020 |Fortune 
That the detonation of nuclear bombs would mark the start of our new epoch is perhaps not the best harbinger of what is to come—much like starting the New Year with a hangover instead of a polar bear plunge. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 
Our underarm stink may well be an “evolutionary hangover” from our ancient ancestors. Stinky success: Scientists identify the chemistry of B.O. |Alison Pearce Stevens |September 15, 2020 |Science News For Students 
Coca-Cola was a wildly popular drink and hangover remedy because, well, it contained cocaine. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 
The tradition has lasted ever since, being seen as a great natural hangover remedy throughout the world. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 
By the late 1600s, chemists and herbalists had begun to concoct their own scientific mixtures for curing the hangover. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 
Hangover Rx: “The old ‘hair of the dog’ is pretty much just a myth,” says White. 5 Hangover Cures to Save You After a Few Too Many |DailyBurn |December 19, 2014 |DAILY BEAST 
One can only assume the hangover cries: “whoa, who did I vote for last night?” Founding Fathers Loved Drunk Voters |Kevin Bleyer |November 1, 2014 |DAILY BEAST 
Could have gone out of his way to be pleasant to customers, not snap at them when he had a terrific hangover. The Day Time Stopped Moving |Bradner Buckner 
He didn't speak English and my high school German was inadequate, especially accompanied by a blockbusting hangover. Unborn Tomorrow |Dallas McCord Reynolds 
Bless you, my son, and may you have a big enough hangover to show you the wickedness of your ways. Rastignac the Devil |Philip Jos Farmer 
As the hangover began to wear off—a little—I was almost sorry I hadn't been able to stay. Unborn Tomorrow |Dallas McCord Reynolds 
And he found that he had an awful hangover from the knockout drug, and the slapping around he had received. The Planet Strappers |Raymond Zinke Gallun