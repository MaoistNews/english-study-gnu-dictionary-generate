It can be done via a vaccine or via prior illness that you develop the antibodies. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 
We want better treatments, yes, but we also want to have enough people with antibodies against the virus that it can’t easily jump across different populations. The problem with Trump’s ‘herd mentality’ line isn’t the verbal flub. It’s the mass death. |Philip Bump |September 16, 2020 |Washington Post 
Called nanobodies, these proteins help fight off invaders in the body, but are smaller and thought to be hardier than their human antibody kin. Treatments that target the coronavirus in the nose might help prevent COVID-19 |Laura Sanders |September 14, 2020 |Science News 
The idea is that a small enough dose of the virus will prevent the person from getting severely ill but will be enough that their immune system’s develop antibodies for future protection. Wearing a mask could protect you from COVID-19 in more ways than you think |Kat Eschner |September 10, 2020 |Popular-Science 
If plasma wasn’t tested for antibody levels, well—it was probably better than nothing. Why the FDA authorized just one test to classify all Covid-19 plasma |Katherine Ellen Foley |September 9, 2020 |Quartz 
Most people—at least 95 percent of adults—have pre-existing antibody to measles. Are Viruses the Next Cure for Cancer? |Kent Sepkowitz |May 15, 2014 |DAILY BEAST 
In general, a person who has had an infection maintains detectable antibody against that infection for life. The ‘HIV-Cured Baby’ Distorts Hope |Kent Sepkowitz |March 7, 2014 |DAILY BEAST 
She, unlike the other millions, has no detectable antibody to HIV. The ‘HIV-Cured Baby’ Distorts Hope |Kent Sepkowitz |March 7, 2014 |DAILY BEAST 
For example, though I had chicken pox decades ago, I still have antibody to chicken pox. The ‘HIV-Cured Baby’ Distorts Hope |Kent Sepkowitz |March 7, 2014 |DAILY BEAST 
But in health, people with HIV always maintain readily detectable antibody. The ‘HIV-Cured Baby’ Distorts Hope |Kent Sepkowitz |March 7, 2014 |DAILY BEAST 
I should say the antibody titer has reached the danger point. Category Phoenix |Boyd Ellanby 
If this is a virus infection, we might only need to find an antibody for inoculation to stop it in its tracks. Star Surgeon |Alan Nourse 
Controls were needed, to be certain that the antibody suspension alone was bringing about the changes seen and not something else. Star Surgeon |Alan Nourse 
Back aboard the ship they started preparing a larger quantity of the antibody suspension. Star Surgeon |Alan Nourse 
Preliminary skin-tests of the antibody suspension showed no sign of untoward reaction. Star Surgeon |Alan Nourse