Never criticise a companion's dress, or indeed make any remark whatever upon it. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 
It is long, indeed, before education brings men to the point where they can criticise their first explanations of Nature. Outlines of the Earth's History |Nathaniel Southgate Shaler 
Though when they look at my pictures they are always kind and sympathetic and never criticise. My Wonderful Visit |Charlie Chaplin 
When he had set down the empty pitcher and drawn his breath, he began to criticise the liquor which it had lately contained. The Fortunes of Nigel |Sir Walter Scott 
Las Cases, however, was always ready to criticise Miss Betsy, whose hoydenish ways he could never understand. Napoleon's Young Neighbor |Helen Leah Reed