You can find excerpts below, or listen to the full interview on the show’s podcast feed. Actress, Mother, Activist Alyssa Milano on Life as a Triple Threat |Esabelle Lee |February 11, 2021 |Ozy 
During the event, the Arizona Daily Star and ProPublica presented excerpts in both original text and plain language. Two ProPublica Local Reporting Network Projects Named Finalists for Shadid Award for Journalism Ethics |by ProPublica |February 8, 2021 |ProPublica 
You can find a selection of excerpts below, or listen to the full interview on the show’s podcast feed. The Educator Fighting to Rectify the Achievement Gap |Esabelle Lee |February 6, 2021 |Ozy 
An excerpt from our interview, lightly edited for length and clarity, follows. How one hit song won The Weeknd a Super Bowl halftime show |Aja Romano |February 5, 2021 |Vox 
This is an excerpt from The Washington Post political newsletter The Daily 202. Amanda Gorman’s first political memory is her mother reading her Miranda Rights |Olivier Knox |January 20, 2021 |Washington Post 
At that same conference in D.C. where she met Saa, Gurira performed an excerpt from Eclipsed. Walking Dead’s Danai Gurira Vs. Boko Haram |Kristi York Wooten |November 30, 2014 |DAILY BEAST 
The following excerpt is printed here with the permission of the University Press of Kentucky. Mossad’s Greatest Female Assassin: An Excerpt From ‘Sylvia Rafael’ |Ram Oren,   Moti Kfir |September 20, 2014 |DAILY BEAST 
Here, an excerpt from a new book that details her path to revenge and its unintended aftermath. Mossad’s Greatest Female Assassin: An Excerpt From ‘Sylvia Rafael’ |Ram Oren,   Moti Kfir |September 20, 2014 |DAILY BEAST 
In an excerpt from his autobiography, he describes the reaction. Rebels Rise Again Over Flag Banning |Robert Khayat |July 28, 2014 |DAILY BEAST 
In an excerpt from 'What We Won,' the story of Abdul Rashid Dostum. The Warlord Who Defines Afghanistan: An Excerpt From Bruce Riedel’s ’What We Won’ |Bruce Riedel |July 27, 2014 |DAILY BEAST 
This not very complex proposition may read like an excerpt from a French grammar, but it is the epitome of the whole argument. Birds in the Calendar |Frederick G. Aflalo 
Jacobi Fabri Stapulensis conclusiones phisic, &c. ex Aristotele excerpt. The Private Diary of Dr. John Dee |John Dee 
From her long melodious lamentation we give one continuous excerpt here. William Blake |Algernon Charles Swinburne 
Reading it as an excerpt indeed one need hardly wish to see beyond the form or material figure. William Blake |Algernon Charles Swinburne 
The excerpt printed last was written at the time when she professed to entertain both beliefs. The Love Affairs of Lord Byron |Francis Henry Gribble