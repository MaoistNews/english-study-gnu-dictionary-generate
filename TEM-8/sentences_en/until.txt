It may be fun and it may get them paid, until oversaturation ruins our sense for irony and destroys the market for it. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 
In other words, the free speech exhibited by the folks at Charlie Hebdo was not virtuous—until there was a body count. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 
That would truly be a milestone to celebrate—until you see what that record “diversity” actually means. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 
At the time (and until 1913), U.S. senators were not popularly elected but were selected by the state legislature. The Black Man Who Replaced Jefferson Davis in the Senate |Philip Dray |January 7, 2015 |DAILY BEAST 
Their leader, Njie, still going by “Dave” during the operation, would stay a safe distance away until the State House was secure. The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 
The plant as a whole remains green until late in the autumn. How to Know the Ferns |S. Leonard Bastin 
He didn't need to wait—as the birds did—until an angleworm stuck his head above ground. The Tale of Grandfather Mole |Arthur Scott Bailey 
No man should regard the subject of religion as decided for him until he has read The Golden Bough. God and my Neighbour |Robert Blatchford 
Here is a chair, Monsieur Arden; but you can hardly see it until your eyes have grown a little accustomed to our crpuscula. Checkmate |Joseph Sheridan Le Fanu 
Aguinaldo withheld his decision until Paterno could report to him the definite opinions of his generals. The Philippine Islands |John Foreman