The casing and plunger materials may break or bend if they’re not used carefully. Best milk frother for delicious drinks at home |Billy Cadden |July 29, 2021 |Popular-Science 
A sturdy build lets you put muscle power behind the plunger without damaging it. Best milk frother for delicious drinks at home |Billy Cadden |July 29, 2021 |Popular-Science 
As you depress the plunger, it presses a plate and whisks through the milk to aerate it. Best milk frother for delicious drinks at home |Billy Cadden |July 29, 2021 |Popular-Science 
Founder Andrew Beckler hatched the idea after an employee fitted a ski-pole grip onto the shop plunger. The Ski-Pole Toilet Plunger You Didn’t Know You Needed |agintzler |July 20, 2021 |Outside Online 
Functionality aside, the lacquered bamboo shaft is also infinitely more aesthetic than the generically lathed handle on your typical plunger. The Ski-Pole Toilet Plunger You Didn’t Know You Needed |agintzler |July 20, 2021 |Outside Online 
You can draw the plunger up and down and it looks as if the blood is mixing with the liquid. 13 Movie Tricks…Revealed! |Marlow Stern |October 31, 2010 |DAILY BEAST 
Trying to pull the plunger back would snap the ring and effectively break the syringe. How to Save a Million Lives |Joshua Robinson |October 13, 2010 |DAILY BEAST 
It is a cast-iron plunger-pole, over the shaft, of 33 inches diameter, 10-feet stroke. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 
The unsupported pump-rods fell downwards, setting in upward motion the column of water in the plunger-pole pumps. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 
I have made the plunger-case and steam-vessel of wrought iron ¾ of an inch thick. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 
The valve open while the plunger-pole ascended 20 inches, then went the remainder of the 10-feet stroke expansive. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 
The function of this plunger is to provide a resistance in addition to that of the air valve spring to assist in acceleration. Marvel Carbureter and Heat Control |Anonymous