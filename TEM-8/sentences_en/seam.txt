We are bursting at the seams already while trying to maintain some semblance of social distancing working under these incredible stress levels. Federal judge denies request to extend Florida voter registration |Lori Rozsa |October 9, 2020 |Washington Post 
The flatlock seams made it as comfortable in bed as any of the other tops. Our Gear Guy's 5 Favorite Running Tees |Joe Jackson |October 5, 2020 |Outside Online 
The kit comes with a good variety of basics including needles, threads, scissors, buttons, a thimble, a needle threader, and a seam ripper. Sewing kits perfect for home, travel, and gift-giving |PopSci Commerce Team |October 1, 2020 |Popular-Science 
Kit comes with 14 large thread spools, 4 specialty thread spools, 12 threaded bobbins, 12 empty bobbins, a small pair of scissors, pins, buttons, needle threaders, a seam ripper, and a mini pin cushion. Sewing kits perfect for home, travel, and gift-giving |PopSci Commerce Team |October 1, 2020 |Popular-Science 
They’re designed with a patented wave-design seam for extra stability, and they’re compostable, too. Gear to make every day feel like National Coffee Day |PopSci Commerce Team |September 29, 2020 |Popular-Science 
Fumbleroooohski…'” (39) “'Look at me, ungh, splitting my own seam, oohh… going deep. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 
Roll the pork over the stuffing, like a jelly roll, until the seam is facing down and the fat back is on top. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 
You sense that Farrow out in the field reporting will be a potential strong seam. Do Not Defy Ronan Farrow’s Power Pout |Tim Teeman |February 25, 2014 |DAILY BEAST 
For a number of years, we lived in the Abu Tor neighborhood, right on the seam of East and West Jerusalem. Life In Common |Yehudah Mirsky |July 2, 2012 |DAILY BEAST 
There is no doubt that Obama is tapping, here, into a rich seam of public discontent. Obama's Phony Bank Debate |Tunku Varadarajan |April 22, 2010 |DAILY BEAST 
She essayed to sew, and stitched up a seam wrong side out, and ran the point of the needle under her finger nail. Alone |Marion Harland 
The cone is made of metal—tin, brass or copper—which can be nickelplated, the seam being soldered. The Boy Mechanic, Book 2 |Various 
The use of a hazel-rod to trace the existence of water or of a seam of coal seems a survival of this practice. Witch, Warlock, and Magician |William Henry Davenport Adams 
"There's a seam of cryolite in the Eastern Hills, according to the old maps," said Lake. Space Prison |Tom Godwin 
The smooth, unbreakable walls; the thin seam of the door; the thermometer. The Girl and The Bill |Bannister Merwin