I found the island’s one pier, paddled out, and proceeded to grovel for two-foot waves. My Quest to Surf Every Coastal State |eriley |July 28, 2021 |Outside Online 
A 17th-century astronomer and naturalist, Giovanni Battista Hodierna, thought snakes used their tongues for “picking the dirt out of their noses … since they are always grovelling on the ground.” Why do snakes flick their tongues? |Purbita Saha |June 20, 2021 |Popular-Science 
I love watching Washington partisans grovel, backpedal, and spin. Hagel Is The Wrong Choice |Gil Troy |December 14, 2012 |DAILY BEAST 
Would-be partners had to grovel for deals and were only accepted on onerous terms, they said. Is Google the Next AOL? |Peter Osnos |April 14, 2009 |DAILY BEAST 
We need not grovel in the sinks and cellars, neither need we ruminate upon the house-tops. A Cursory History of Swearing |Julian Sharman 
Behind the back of Monsieur P—— they grouch; before his face they grovel. Over the Front in an Aeroplane and Scenes Inside the French and Flemish Trenches |Ralph Pulitzer 
Nay, I would ask her forgiveness and grovel before her, if she would only let me enjoy her love again. The Making of a Saint |William Somerset Maugham 
Then the two ladies who looked beheld Houdaine fall down at the feet of Sir Tristram and grovel there with joy. The Story of the Champions of the Round Table |Howard Pyle 
Mahmud tried to grovel at the captains feet, wailing to Allah and the Prophet. Cursed |George Allan England