“I sincerely hope this woman is flattened by a lorry,” prays another. When Fame Is the Reason for Abortion, Does That Make It Wrong? |Tauriq Moosa |April 30, 2014 |DAILY BEAST 
The lorry hit him so hard he was dead by the time the nearside front wheels rolled over his neck. Alastair Campbell: I Planned My Suicide |Nico Hines |March 27, 2014 |DAILY BEAST 
They crawled down off the lorry, and took off their caps, and ate every particle of food in the house. The Amazing Interlude |Mary Roberts Rinehart 
And one day a lorry, piled high with boxes, rolled and thumped down the street, and halted by Ren. The Amazing Interlude |Mary Roberts Rinehart 
I was wanted to take ten officers at once and to jump into a motor lorry, and go with a party of 30 others to the trenches. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie 
We raced our lorry through country looking just like the Romney Marshes, Sussex. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie 
Added to these difficulties, a motor-lorry had stuck on the way up and blocked our transport for the night. The Doings of the Fifteenth Infantry Brigade |Edward Lord Gleichen