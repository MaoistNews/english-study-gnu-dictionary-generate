He also itemized, with conviction, his administration’s top priorities for the coming year. Underfunded Libraries Are Critical Infrastructure |Patrick Stewart |January 19, 2022 |Voice of San Diego 
That’s true even if they take the standard deduction on their federal returns rather than itemizing deductions. The little-known way seniors can deduct gifts to charities |Allan Sloan |October 28, 2021 |Washington Post 
But the president could have chosen not to itemize his giving, or to give that money to the U.S. Treasury, as many people do. The Obama Rule: Do as the President Says, Not as He Does |Stuart Stevens |April 23, 2013 |DAILY BEAST 
Only about one-third of Americans itemize their deductions, and they are mostly the well off. Let's Chat about Deductions |Justin Green |December 10, 2012 |DAILY BEAST 
They tend to advantage wealthier taxpayers (who are more likely to itemize) and corporations. End the Tax-Break Scam |Michael Tomasky |May 8, 2011 |DAILY BEAST 
I itemize what struck me as being their real, if unuttered trend. A Case in Camera |Oliver Onions 
In any report of a wedding, it is proper to itemize the plunder. The Sheriff of Badger |George B. Pattullo 
Don't keep spending money for a lot of things that you would hardly care to itemize in the account you send to Father. The College Freshman's Don't Book |George Fullerton Evans 
The original method was to itemize all transactions in the ledger, but the present custom is to post the totals only. Cyclopedia of Commerce, Accountancy, Business Administration, v. 4 |Various 
So I propose allowing all taxpayers, whether they itemize or not, to deduct their charitable contributions. Complete State of the Union Addresses from 1790 to 2006 |Various