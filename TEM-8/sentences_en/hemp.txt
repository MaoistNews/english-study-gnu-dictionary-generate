You might also consider a rice protein or hemp protein-based powder. Best protein powder: Better nutrition in a bottle |Carsen Joenk |January 11, 2021 |Popular-Science 
The carbon-oxygen-hydrogen compound can be found in high concentrations in Cannabis sativa and less-potent hemp plants. Can CBD help you chill? Here’s what we know so far. |Purbita Saha |January 4, 2021 |Popular-Science 
A blend of cotton, hemp, and spandex makes this coat the most versatile layer I own. 7 of Our Favorite Men's and Women's Fall Layers |Jakob Schiller |October 3, 2020 |Outside Online 
During this time, I reconnected with some high school buddies of mine—the Stanley brothers—who had started a hemp company called Charlotte’s Web. OneRepublic’s Ryan Tedder on launching a hemp-infused sparkling water brand |Rachel King |August 24, 2020 |Fortune 
Legal cannabis and hemp business owners say that Border Patrol checkpoints between Imperial and San Diego counties are costing them millions. Businesses Say Border Patrol Is Seizing Legal Cannabis Between San Diego, Imperial |Jesse Marx and Maya Srikrishnan |June 29, 2020 |Voice of San Diego 
A total ban on hemp production was enacted in the late 1950s. The Chronic Chronicles: A History of Pot |Roger Roffman |July 6, 2014 |DAILY BEAST 
We may see hemp again become a major American agricultural product. The Chronic Chronicles: A History of Pot |Roger Roffman |July 6, 2014 |DAILY BEAST 
“It is an outrage that DEA is using finite taxpayer dollars to impound legal industrial hemp seeds,” he said. Kentucky Tells Feds: Hands Off Our Hemp! |Abby Haglage |May 21, 2014 |DAILY BEAST 
The fate of hemp production in America hangs in the balance. Kentucky Tells Feds: Hands Off Our Hemp! |Abby Haglage |May 21, 2014 |DAILY BEAST 
Hemp, too close to reefer madness for comfort, was one of them. Kentucky Tells Feds: Hands Off Our Hemp! |Abby Haglage |May 21, 2014 |DAILY BEAST 
Up on the side of the mountain, not far from here, is a large hemp plantation; I will seek work there. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 
The plant from which is made what is called Manila hemp belongs to the same family as the banana and the plantain. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 
Alila had never before seen hemp gathered, and he had much to learn. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 
His father has told him that nothing else in his island home is shipped in such quantities as Manila hemp. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 
As soon as he got back from the hunt, he went off to look over the farm to see if the hemp was growing well. Alila, Our Little Philippine Cousin |Mary Hazelton Wade