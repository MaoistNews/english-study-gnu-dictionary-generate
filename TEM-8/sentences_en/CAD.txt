Lift Tickets: Adult three-day passes begin at around $200 (CAD), depending on whenyou visit. Olympians Dish on Their Favorite Spots to Ski & Snowboard |The Daily Beast |October 26, 2013 |DAILY BEAST 
The writer Rick Marin, author of the book Cad and the dad-as-sports-coach Kindle single memoir Keep Swinging, agrees. Is Prince William Trying to Miss the Royal Baby’s Birth? |Tom Sykes |July 1, 2013 |DAILY BEAST 
By the time of this third episode of Season 1, we were well aware of Don as a liar, cad, and adulterer. The 10 Most Essential TV Episodes From ‘Sopranos,’ ‘Breaking Bad,’ and More |Brett Martin |June 30, 2013 |DAILY BEAST 
It is hard not to think Connors is an insensitive cad for bringing up such a private matter almost 40 years after the fact. Jimmy Connors Memoir Shows He Wasn’t Misunderstood, He Was Just a Jerk |James Zug |May 14, 2013 |DAILY BEAST 
A few minutes later she watched as the cad tore his hair out screaming on his front lawn. Paula Broadwell, Eminem, & More Spurned Lovers Who Went Ballistic |Paula Froelich |November 15, 2012 |DAILY BEAST 
Of course you would think me a cad, a—well, I have my opinion of a man that would tell his side of such a story to a woman. The Homesteader |Oscar Micheaux 
My indignant protest that the cowardly cad tried to kiss me counted for nothing. The Weight of the Crown |Fred M. White 
The pain was devilish, and I wasn't used to being alone, and nobody caring a damn, and everybody believing me a cad and a bully. Marriage la mode |Mrs. Humphry Ward 
The Cad bait, with a little hackle round the top of the shank of the hook, kills well. The Teesdale Angler |R Lakeland 
But I shall say something, if I see you working up a flirtation with that cad. The Dark House |Georg Manville Fenn