“Our entire aim is to help make it easier to talk about sex,” says Gallop. Silicon Valley’s Soft Sex Ban |Joshua Rivera |May 29, 2014 |DAILY BEAST 
At one point the coach driver whipped the horses into a gallop and drove right through the Hessian army. Beethoven in Love: The Woman Who Captivated the Young Composer |John Suchet |January 26, 2014 |DAILY BEAST 
The haredi leadership is ready to gallop at full speed over this religious fiscal cliff. Voting For Yair Lapid, Israel’s Maimonides |Rabbi Daniel Landes |February 4, 2013 |DAILY BEAST 
I suspect some of them also cling to the belief that candy-colored unicorns will ultimately gallop in to save the day. Obama’s Osama bin Laden Ad Is a Well-Played Attack |Michelle Cottle |May 1, 2012 |DAILY BEAST 
She had seen Lawrence gallop to his quarters, and his drawn, haggard face told her the worst. The Red Year |Louis Tracy 
As it was he had to press Nejdi into a fast gallop before he could clear the left wing of the advancing army. The Red Year |Louis Tracy 
Our escort was mounted within a few minutes, and we were in full gallop over the fruitful levels of Champagne. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
The horses gallop, hats are waved in every direction, and those who have no hats flourish beer-bottles instead. The Portsmouth Road and Its Tributaries |Charles G. Harper 
Crossing the drawbridge at a fast gallop, he saw a number of guards looking at him wonderingly. The Red Year |Louis Tracy