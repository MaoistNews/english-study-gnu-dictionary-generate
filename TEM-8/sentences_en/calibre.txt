He added that it exposed the calibre of forces that Nato was able deploy. Taliban: Harry Comparing War to Xbox Demeans Him |Tom Sykes |January 22, 2013 |DAILY BEAST 
Romney did not deliver last night the calibre performance he displayed during the first debate. How Can Romney Recover? |David Frum |October 17, 2012 |DAILY BEAST 
It would have been pure calibre that determined the outcome. 11 Revelations From Blair's Memoir |The Daily Beast |September 1, 2010 |DAILY BEAST 
Every cadence of their voices, every gesture, proclaimed the radical difference of nature and calibre. The Daughters of Danaus |Mona Caird 
I earnestly pray he will get right again quickly for there are not many Commanders of his calibre. Gallipoli Diary, Volume 2 |Ian Hamilton 
The Germans continued to bombard Ypres with large calibre shells, heaping ruins upon ruins. Ypres and the Battles of Ypres |Unknown 
I found I had a mould for that calibre, and you shall have four-and-twenty cartridges to-day, brother. Columba |Prosper Merimee 
Because Arsne Lupin is the only man in France of sufficient calibre to invent and carry out a scheme of that magnitude. The Extraordinary Adventures of Arsene Lupin, Gentleman-Burglar |Maurice Leblanc