Her sister, Irina Press, won Olympic gold in the 80-meter hurdles in 1960 and the pentathlon in 1964. Tamara Press, Soviet Olympic champion whose gender was questioned, dies at 83 |News Services |May 7, 2021 |Washington Post 
Modern pentathlon, which involves fencing, horse riding, swimming, running, and shooting, was expected to be dumped. Sex, Bribes & Rhythmic Gymnastics: The IOC’s Biggest Scandals |Sujay Kumar |September 6, 2013 |DAILY BEAST 
To get yourself into the right mindset for a long trip with your children, it requires a carefully meted out pentathlon of events. Make Your Kids Smarter in the Car |K. Emily Bond |July 31, 2010 |DAILY BEAST 
The Pentathlon was a contest of five gymnastic exercises combined. Athens: Its Rise and Fall, Complete |Edward Bulwer-Lytton 
And he who is beautiful for the Pentathlon is very bad for wrestling? The Teaching of Epictetus |Epictetus 
It formed a part of the pentathlon, or quintuple games, in the ancient Olympic Games. Encyclopaedia Britannica, 11th Edition, Volume 8, Slice 5 |Various 
A pentathlon meet was held at Empire for the purpose of developing all-around athletes. The Panama Canal |Frederic Jennings Haskin 
He had the three games out of five and was winner of the pentathlon. Buried Cities, Part 2 |Jennie Hall