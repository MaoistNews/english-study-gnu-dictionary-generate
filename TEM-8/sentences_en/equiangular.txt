Euclid does not use the word regular, but he describes the polygons in question as equiangular and equilateral. Encyclopaedia Britannica, 11th Edition, Volume 11, Slice 6 |Various 
About a given circle to circumscribe a triangle equiangular to a given triangle. Encyclopaedia Britannica, 11th Edition, Volume 11, Slice 6 |Various 
The proof of Euclid's axiom looked for in the properties of the Equiangular Spiral. A Budget of Paradoxes, Volume I (of II) |Augustus De Morgan 
Let the Triangle aei be given: And the circle, o, into which a Triangle equiangular to the triangle given, is to be inscribed. The Way To Geometry |Peter Ramus 
It opens with a definition of a regular polygon as one that is both equilateral and equiangular. The Teaching of Geometry |David Eugene Smith