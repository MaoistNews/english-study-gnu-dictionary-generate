To those who agreed with him, Bush pledged that the law against same-sex marriage would remain intact. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 
To his critics, he explained—sometimes at painful length—his reasoning against it. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 
This is a blow against freedom of speech, we were told, by the likes of Homeland Security chief Jeh Johnson. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 
Spouting off against police online has become criminalized in recent weeks. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 
The reason we were liberals is we were against oppression. Bill Maher: Hundreds of Millions of Muslims Support Attack on ‘Charlie Hebdo’ |Lloyd Grove |January 8, 2015 |DAILY BEAST 
Do not the widow's tears run down the cheek, and her cry against him that causeth them to fall? The Bible, Douay-Rheims Version |Various 
Vain also was the valour and ability he showed in the campaign against the Royalists in La Vende. Napoleon's Marshals |R. P. Dunn-Pattison 
Beginners must be warned against mistaking the edges of cells, or particles which have retained the red stain, for bacilli. A Manual of Clinical Diagnosis |James Campbell Todd 
Whether they had ever, at different times, pleaded for or against the same cause, and cited precedents to prove contrary opinions? Gulliver's Travels |Jonathan Swift 
He leant against the wall of his refuge, notwithstanding this boast, and licked the ice to moisten his parched lips. The Giant of the North |R.M. Ballantyne