Funeral directors couldn’t quickly answer the calls of grieving families, much less retrieve bodies. The Leader of New York’s “City of the Dead” Cashes In. Again. |by Carson Kessler |July 25, 2022 |ProPublica 
The cop reholsters his gun, and it seems to have ended with no further bloodshed as he moves to retrieve the knife. Synagogue Slay: When Cops Have to Kill |Michael Daly |December 10, 2014 |DAILY BEAST 
When gunshots burst out, she ran to retrieve her child from school and returned to her house just as a bomb hit next door. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 
After weeks of training, the first mission set out to retrieve Kent Brantly, an American doctor. The American Ebola Rescue Plan Hinges on One Company. Meet Phoenix. |Abby Haglage |November 22, 2014 |DAILY BEAST 
He put it down his shirt and asked her to retrieve it, saying she was “all talk” and “no play” when she refused. Speed Read: Lena Dunham’s Most Shocking Confessions From ‘Not That Kind of Girl’ |Kevin Fallon |September 26, 2014 |DAILY BEAST 
Rising to retrieve it, I offer her what meager reassurance I can muster. The Stacks: Grateful Dead I Have Known |Ed McClanahan |August 30, 2014 |DAILY BEAST 
I could not stop to retrieve the question papers with a pair of tongs—as I might, had I not been hurried. Ruth Fielding At College |Alice B. Emerson 
From that moment he did everything that mortal man could do to retrieve his fatal mistake. "Shiloh" as Seen by a Private Soldier |Warren Olney 
This young king continued the war, but was unable to retrieve the ill-fortunes of his people. The Works of Hubert Howe Bancroft, Volume 5 |Hubert Howe Bancroft 
As Parr moved to retrieve these, his companions called out to halt him. The Devil's Asteroid |Manly Wade Wellman 
Lady Evenswood, with a quick perception, tried to retrieve the observation. Tristram of Blent |Anthony Hope