So, no, I might’ve said something slick, but I wasn’t cracking jokes all day, I don’t think. Dulcé Sloan on Where She Gets Her Funny From |Joshua Eferighe |February 9, 2021 |Ozy 
Almost a decade ago, my step-siblings bought a pair for me and my partner as a joke. The Most Practical Outdoor Dining Outfit Is a Snuggie |Jaya Saxena |February 9, 2021 |Eater 
“I do the standard eyeroll and the older the joke the bigger the eyeroll,” Monica wrote. Habit forming: Here are some of the quirks that make these readers unique |John Kelly |February 7, 2021 |Washington Post 
Save any top secrets, dirty jokes or spoiler alerts for somewhere else. Georgetown gains a little gem in Lutèce |Tom Sietsema |February 5, 2021 |Washington Post 
“We just drug around there and told jokes all the way down,” he said. Vietnam-era draftees may recall inoculations without needles. They were fast. |John Kelly |February 1, 2021 |Washington Post 
Policemen on the show joke about prison riots, bomb threats, and the shooting of unarmed civilians. 'Babylon' Review: The Dumb Lives of Trigger-Happy Cops |Melissa Leon |January 9, 2015 |DAILY BEAST 
A running joke inside the tribe is that the group is like that club with a hundred people waiting outside to get in. ‘We Out Here’: Inside the New Black Travel Movement |Charlise Ferguson |January 4, 2015 |DAILY BEAST 
And that was well before this Christmas, when he appeared to joke about Obama being a Muslim. How James Woods Became Obama’s Biggest Twitter Troll |Asawin Suebsaeng |December 31, 2014 |DAILY BEAST 
Within a concentration camp, would someone make a joke about the number, the tattooed number? Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 
Anyone willing to threaten war over a joke is clearly not playing with a full deck. The Sony Hack and America’s Craven Capitulation To Terror |David Keyes |December 19, 2014 |DAILY BEAST 
The patriarchal decree of the government was a good deal of a joke on the plains, anyway—except when you were caught defying it! Raw Gold |Bertrand W. Sinclair 
Mr. Crow was rocking back and forth on his perch, for a joke—on anybody except himself—always delighted him. The Tale of Grandfather Mole |Arthur Scott Bailey 
Well, thinks I, this is no joke sure, at this lick I'll have family enuff to do me in a few years. The Book of Anecdotes and Budget of Fun; |Various 
"I don't think that's a very good joke," said the disappointed little boy. Davy and The Goblin |Charles E. Carryl 
The king, by way of joke, desired the earl to personate him, and ordered the petitioner to be admitted. The Book of Anecdotes and Budget of Fun; |Various