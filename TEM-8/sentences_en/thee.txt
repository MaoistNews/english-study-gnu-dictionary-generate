Progressives in the grip of one of their signature moral crusades routinely embrace money in politics for me, but not for thee. Money and Guns: How We Escape Our Existential Dread |James Poulos |April 18, 2014 |DAILY BEAST 
Make thee an ark of gopher wood; rooms shalt thou make in the ark, and shalt pitch it within and without with pitch. The Story of Noah's Ark From the Bible’s Book of Genesis |The Daily Beast |March 24, 2014 |DAILY BEAST 
Go forth of the ark, thou, and thy wife, and thy sons, and thy sons' wives with thee. The Story of Noah's Ark From the Bible’s Book of Genesis |The Daily Beast |March 24, 2014 |DAILY BEAST 
Woe unto thee good people of North Carolina for the Muslims are coming to impose Islamic law. North Carolina, Your Anti-Sharia Law Takes the Cake |Dean Obeidallah |August 9, 2013 |DAILY BEAST 
Set to the tune of "I Vow to Thee My Country," it finds the Royalist on the verge of patriotic tears. Live Blogging Thatcher’s London Funeral |Tom Sykes |April 17, 2013 |DAILY BEAST 
For it is better that thy children should ask of thee, than that thou look toward the hands of thy children. The Bible, Douay-Rheims Version |Various 
Give not up thy heart to sadness, but drive it from thee: and remember the latter end. The Bible, Douay-Rheims Version |Various 
O death, how bitter is the remembrance of thee to a man that hath peace in his possessions! The Bible, Douay-Rheims Version |Various 
Take care of a good name: for this shall continue with thee, more than a thousand treasures precious and great. The Bible, Douay-Rheims Version |Various 
Of silence before them that salute thee: of looking upon a harlot: and of turning away thy face from thy kinsman. The Bible, Douay-Rheims Version |Various