Like sculptors, you don’t take a block of marble then add another block halfway through. T. Rex Was a Slacker - Issue 102: Hidden Truths |Mark MacNamara |July 7, 2021 |Nautilus 
Against the warnings of his partner, the sculptor Edmund Kara, and his friends, he decided to tell Duke in person that he was quitting. Did Doris Duke get away with murder? |Kathi Wolfe |April 28, 2021 |Washington Blade 
Even then, whether a sculptor chooses chimpanzee or human muscles as their starting point can produce very different outcomes. New depictions of ancient hominids aim to overcome artistic biases |Tina Hesman Saey |April 5, 2021 |Science News 
They’re like blank canvases for these artists and sculptors and gardeners. Should Traffic Lights Be Abolished? (Ep. 454) |Stephen J. Dubner |March 11, 2021 |Freakonomics 
Frankel and Mougne, who are sculptors, and Williams, a painter, all produce sleek, stylish abstractions. In the galleries: Immersive exhibit explores a wonderland in blue |Mark Jenkins |January 22, 2021 |Washington Post 
Szymon Oltarzewski is a Polish-born artisan who is also a sculptor in his own right. Damien Hirst’s Army of Geppettos |Tim Teeman |December 2, 2014 |DAILY BEAST 
My mother is a sculptor (the sculptor Helaine Blumenfeld) and my father is a writer. Vogue Photographer Erwin Blumenfeld: Secrets of a Fashion Legend |Tim Teeman |September 14, 2014 |DAILY BEAST 
Sculptor John Raimondi filed a separate federal complaint for a very similar situation. Real Estate Mogul Igor Olenicoff Busted for Art Forgery |Justin Jones |June 13, 2014 |DAILY BEAST 
Now every Long Island divorcee thinks she can come and be a sculptor. The Second Life of San Miguel de Allende |Michele Willens |February 26, 2014 |DAILY BEAST 
They also met with sculptor Ron Nagle, best known for abstraction. Fall Won’t Be Dreary for Proenza Schouler |Erin Cunningham |February 13, 2014 |DAILY BEAST 
He was a great friend of the distinguished American sculptor, Mr. Story, and was a frequent visitor at his house. A Selection from the Works of Frederick Locker |Frederick Locker 
Jacques Pradier died near Paris, aged 54; the most distinguished sculptor of his day in France. The Every Day Book of History and Chronology |Joel Munsell 
In 1839 the sculptor Dorlange-Sallenauve knew of Bixiou and complained of his slanders. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 
The leading sculptor for this unusually beautiful memorial was Leo Friedlander. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 
This unusual condition of his daughter was a real grief to the sculptor; but this grief was of short duration. The Nabob |Alphonse Daudet