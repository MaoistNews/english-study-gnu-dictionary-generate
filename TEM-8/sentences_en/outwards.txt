Before the construction of the mole, the flow and ebb of the sea cleared the mud away entirely, by forcing it outwards. The Geography of Strabo, Volume III (of 3) |Strabo 
His chin, which was nothing if not determined, was thrust upwards and outwards over his irreproachable high collar. The Creators |May Sinclair 
Twig the drum-boy, he has turned his coat—do you see, with the lining outwards to keep it clean. Newton Forster |Captain Frederick Marryat 
Budge is lambskin with the wool dressed outwards, worn on the edge of the hoods of bachelors of arts, etc. Milton's Comus |John Milton 
Now the door opened outwards; and as the door opened wider and wider, Mr. Pickwick receded behind it, more and more. The Pickwick Papers |Charles Dickens