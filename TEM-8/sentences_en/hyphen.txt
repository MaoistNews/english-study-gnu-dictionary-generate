In the case of ecommerce, since you’ll probably have a series of photos for each product, give the image files titles that reflect the product, with words separated by hyphens. Four Google SERP features for ecommerce SEO |Kris Jones |November 18, 2021 |Search Engine Watch 
When you talk about Debbie Allen, you need to realize that hyphens will not get you anywhere close to wrapping your head around what she has accomplished. Getting to the Dance With Debbie Allen |Eugene Robinson |October 5, 2020 |Ozy 
It’s become common enough since it was first recorded in the 1910s that we write it out as a single—one word, not two, without a hyphen. How To Know If A Term Is One Word Or Two Words |Brigid Walsh |July 24, 2020 |Everything After Z 
A hyphen is part of the common spelling: daylight-saving time. Is it Daylight Saving or Daylight Savings? |Dictionary.com |November 8, 2010 |DAILY BEAST 
He belonged to the branch of the family that owns the hyphen and most of the money. From Place to Place |Irvin S. Cobb 
If the capital-letter be retained where a prefix is put to a proper name, the hyphen is obviously necessary. "Stops" |Paul Allardyce 
The hyphen distinguishes the etymological meaning of these words as distinguished from their derived and ordinary meaning. "Stops" |Paul Allardyce 
When the combination is likely to be misunderstood, modern editors generally put a hyphen between the two words. Ephemera Critica |John Churton Collins 
Taken out hyphen for 'woman-kind', majority are 'womankind'. Dryden's Works Vol. 3 (of 18) |John Dryden