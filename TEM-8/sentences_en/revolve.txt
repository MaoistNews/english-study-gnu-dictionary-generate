The story revolves around Daphne, a young American woman who has come to the city of her parents’ birth to explore her heritage. ‘A Recipe for Daphne’ is a delicious debut |Vanessa Larson |February 5, 2021 |Washington Post 
The biggest argument for Rivers’s Hall of Fame entry revolves around his total passing yards and touchdowns. Sorry, but Philip Rivers is not a Hall of Fame quarterback |Neil Greenberg |January 22, 2021 |Washington Post 
Marketing strategy will still revolve around data, but in a different ecosystem. Marketing Briefing: Marketers and agency execs are hoping for more optimism post-inauguration |Kristina Monllos |January 19, 2021 |Digiday 
It revolves around a problem that, curiously, is both solved and unsolved, closed and open. Mathematicians Resurrect Hilbert’s 13th Problem |Stephen Ornes |January 14, 2021 |Quanta Magazine 
All those conversations we had about whether and when to become parents revolved around a future that is no longer ours. This country is a risky place to be a parent, and the pandemic reminded me of that |Haley Swenson |January 14, 2021 |Washington Post 
Somehow, everything in the world has to revolve around men here, men and their parts. What It Feels Like For a Girl in Iraq |Noor |November 6, 2014 |DAILY BEAST 
Our debates about federal budgets still revolve around degrees of imposed austerity. The Tea Party Is Dead? Nah, That’s Just a Flesh Wound |Michael Tomasky |May 20, 2014 |DAILY BEAST 
In the 1980s and 1990s, blockbusters were star vehicles; now they revolve around brands (Marvel Comics) and concepts (vampires). Is This the End of Arnold Schwarzenegger's Comeback? |Andrew Romano |March 30, 2014 |DAILY BEAST 
Although many of his films revolve around a past time, Wolf is not a nostalgic. Who Invented the ‘Teenager’? |Nina Strochlic |March 14, 2014 |DAILY BEAST 
“Prison has taught me that the world does not revolve around me,” he says. The Party Monster Lives For the Applause: Michael Alig’s Second Act |Caitlin Dickson |February 28, 2014 |DAILY BEAST 
About this centre the spheres revolve, each in a way swinging around the other. Outlines of the Earth's History |Nathaniel Southgate Shaler 
The center wheel must revolve once in each hour, which is 61⁄2 times faster than the barrel. The Wonder Book of Knowledge |Various 
The several plans I revolve in my mind do not prove, upon closer examination, feasible. Prison Memoirs of an Anarchist |Alexander Berkman 
The vapours revolve, the waves spin, the giddy Naiads roll; sea and sky are livid; noises as of cries of despair are in the air. Toilers of the Sea |Victor Hugo 
When the current is applied, the disk will revolve in a direction relative to the position of the poles on the magnet. The Boy Mechanic, Book 2 |Various