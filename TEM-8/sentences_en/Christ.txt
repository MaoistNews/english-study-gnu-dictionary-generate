But a closer look reminds us how little Christ there really is in Christmas. Meet Krampus, the Seriously Bad Santa |Jay Michaelson |December 5, 2014 |DAILY BEAST 
It was like, he was the anti-Christ and we came from a snobby, purist direction. How Questlove Is Bringing Music Back to Television |Kevin Fallon |July 23, 2014 |DAILY BEAST 
(To which the obvious retort was: the Christ-like thing to do would be to forgive me). Sympathy for the Devils: Scenes From the Social Conservative Collapse |Olivia Nuzzi |June 21, 2014 |DAILY BEAST 
As for me, I am not worried about meeting Christ one day and hearing firsthand what he has to say about that prayer. How to Pray in the Public Square |Gene Robinson |May 11, 2014 |DAILY BEAST 
After knocking, I heard Mrs. Goode say, ‘Holy Fucking Christ.’ The Teen Love Letters that Led to a Tragic Murder-Suicide in Florida |Michael Daly |March 30, 2014 |DAILY BEAST 
The blessings of time and eternity constitute the part of the promise offered to believers, through Christ. The Ordinance of Covenanting |John Cunningham 
Only they who are without Christ, are aliens from the commonwealth of Israel and strangers from the covenants of promise. The Ordinance of Covenanting |John Cunningham 
Had this stupendous miracle no effect upon the Jewish priests who had crucified Christ as an impostor? God and my Neighbour |Robert Blatchford 
The Gentiles shall seek and find Christ, but the Jews will persecute him, and be rejected, only a remnant shall be reserved. The Bible, Douay-Rheims Version |Various 
The bisection of the victim symbolized Christ slain and affording access to God through himself. The Ordinance of Covenanting |John Cunningham