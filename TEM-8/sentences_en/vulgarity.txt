“FFV,” First Families of Virginia, is a lineage society whose members would likely disapprove of such vulgarity. Homosexuality, an ‘inherently divisive concept’ at UVA |Charles Francis |March 11, 2022 |Washington Blade 
The editors who denounced Benglis in 1974 called the magazine image “an object of extreme vulgarity.” Artist Lynda Benglis became controversial in an instant, but her career has thrived for decades |Philip Kennicott |August 26, 2021 |Washington Post 
I can see vulgarity and ugliness anywhere I want to look, and I can see it for free. ‘Transformers: Age of Extinction’ Is an Empty Pit of Nihilism |Teo Bugbee |June 30, 2014 |DAILY BEAST 
Explicitness and vulgarity are both different subsets of honesty, both very necessary. The Modern ‘Lolita’: Dramatizing the Mind of a Female Pedophile in Alissa Nutting’s ‘Tampa’ |Roxane Gay |June 28, 2013 |DAILY BEAST 
Sometimes I wonder if I actually believe in vulgarity, as a concept. The Modern ‘Lolita’: Dramatizing the Mind of a Female Pedophile in Alissa Nutting’s ‘Tampa’ |Roxane Gay |June 28, 2013 |DAILY BEAST 
Where do you see the line between explicitness and vulgarity? The Modern ‘Lolita’: Dramatizing the Mind of a Female Pedophile in Alissa Nutting’s ‘Tampa’ |Roxane Gay |June 28, 2013 |DAILY BEAST 
Wilde deplored American commercialism and vulgarity, but he admired American simplicity and decency. Wilde Ride |Anthony Paletta |January 3, 2013 |DAILY BEAST 
Smoking now is as common as eating and drinking, and to smoke amongst ladies is a vulgarity. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 
There is no surer mark of vulgarity than over dressing or gay dressing in the street. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 
So that the Provençal language, in spite of everything, keeps a certain patois vulgarity. Frdric Mistral |Charles Alfred Downer 
It is said that the late Ward McAllister shrank with peculiar distaste from the vulgarity of divorce. The Onlooker, Volume 1, Part 2 |Various 
Vulgarity, mother says, is pretending to be what you are not. Sarah's School Friend |May Baldwin