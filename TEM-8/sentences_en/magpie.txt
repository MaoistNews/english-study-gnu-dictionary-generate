She’s depressed and isolating herself from her loved ones, but things start to change for the better once an injured baby magpie named Penguin is fatefully brought into her life. The True Story Behind the New Netflix Film Penguin Bloom |Annabel Gutterman |January 27, 2021 |Time 
Television is a magpie, and it will take back to its stuffed nest whatever shiny things it can find. The Fascist Historian and Me |Guy Walters |January 12, 2014 |DAILY BEAST 
Instead, he was more of a magpie, listening to what came before and incorporating it into his own inimitable music. J.J. Cale, Dead at 74, Was a Songwriter Beyond Compare |Malcolm Jones |July 30, 2013 |DAILY BEAST 
Turn Up The Trustafarian Chic: As Tory Burch said of her collection, it's "American prep meets magpie traveler." 6 Lessons From The Spring Runways: New York Fashion Week Spring Summer 2013 |Isabel Wilkinson, Lizzie Crocker |September 14, 2012 |DAILY BEAST 
But the latter caught him by the coat sleeve and held on while she chattered like a magpie to the young college man. The Campfire Girls of Roselawn |Margaret Penrose 
Could that be the wild beasts of which the magpie had warned him? Fifty-Two Stories For Girls |Various 
"She doesn't believe you, anyhow," said Lionel to the magpie. Fifty-Two Stories For Girls |Various 
The meadowlark was given a pleasing voice so that his songs would make the magpie ashamed. Prairie Smoke (Second Edition, Revised) |Melvin Randolph Gilmore 
Harakka, the Magpie, sitting on her nest among her fledglings began to feel nervous. Mighty Mikko |Parker Fillmore