Terry eventually came back out for another cigarette and heard the news. The Last Bar Standing? |Eugene Robinson |September 15, 2020 |Ozy 
It’s also stupid human decisions, like fireworks and flaming cigarette butts and gender reveal parties, which ignite flames. Our Climate-Change Future Is Here |Heather Hansman |September 11, 2020 |Outside Online 
Once a source of cheap, unskilled labor and affordable cigarettes, Poland has developed into Eastern Europe’s economic powerhouse since joining the borderless EU in 2007. Eastern Germany Picks Up Polish for Children’s Future |Charu Kasturi |September 8, 2020 |Ozy 
Like I talked to a man who was standing there, smoking a cigarette. Kenosha’s looting is a symptom of a decrepit democracy |Aaron Ross Coleman |September 4, 2020 |Vox 
This study provided some of the first clues that cigarette smoking could contribute to heart disease. Decades-long project is linking our health to the environment |Lindsey Konkel |March 12, 2020 |Science News For Students 
He observes the bodies floating away on the river, pulling on his cigarette with a sneer. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 
She retrieved a cigarette from her purse and lit it without moving her face away from the screen. I Watched a Casino Kill Itself: The Awful Last Nights of Atlantic City’s Taj Mahal |Olivia Nuzzi |December 8, 2014 |DAILY BEAST 
They say the currency devalues a few points just in the time it takes to smoke a cigarette. Recession? Devaluation? Inflation? Putin Tells Russia Stay the Course. |Anna Nemtsova |December 4, 2014 |DAILY BEAST 
Conservative hit man turned liberal media critic David Brock, spotted smoking an e-cigarette in the lobby. Team Clinton Prepares for the Other Side of If |David Freedlander |November 22, 2014 |DAILY BEAST 
“The truth is, we feel threatened,” her husband adds between drags on a cigarette. Lebanese Christians Gun Up Against ISIS |Susannah George |November 10, 2014 |DAILY BEAST 
One or two of the buffalo-hunters exchanged words with us while Mac was building his cigarette and lighting it. Raw Gold |Bertrand W. Sinclair 
She took a cigarette from the gayly tendered case and smoked for a few moments in silence. Ancestors |Gertrude Atherton 
She lit another cigarette, and for a few moments looked silently out of the window at the darkening woods beyond the lawn. Ancestors |Gertrude Atherton 
She led Isabel over to Mrs. Kaye, who sat alone on a small sofa, sipping her coffee and absently puffing at a cigarette. Ancestors |Gertrude Atherton 
Goodell lighted another cigarette and nonchalantly seated himself in the vacant chair. Raw Gold |Bertrand W. Sinclair