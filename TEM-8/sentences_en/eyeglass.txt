The eye shield should fit seamlessly over the face with or without eyeglasses and provide venting and anti-fogging technology. Best snowboard helmet: Snow gear to stay safe on the slopes |PopSci Commerce Team |March 4, 2021 |Popular-Science 
Meanwhile, she is holding off on expenses like new eyeglasses and co-pays for the dentist. The IRS Cashed Her Check. Then the Late Notices Started Coming. |by Lydia DePillis |February 16, 2021 |ProPublica 
Luckily, when you wear prescription eyeglasses you have a couple of options when looking for ski goggles. Best ski goggles: What to look for in a pair you’ll love |Eric Alt |January 20, 2021 |Popular-Science 
Some, like the PlayStation VR, allows you to adjust the lens distance from your eyes, which is helpful for those who wear eyeglasses. Best VR headsets: Bring your entertainment to life |Eric Alt |January 11, 2021 |Popular-Science 
It’s also embedded within shopping apps and store mirrors that let people try on anything from eyeglasses to makeup virtually. Podcast: Attention, shoppers–you’re being tracked |Tate Ryan-Mosley |December 21, 2020 |MIT Technology Review 
Seager writes about being threatened by a patient with a shank carved out of an eyeglass stem. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 
Seton, smoking one of the inevitable cheroots, watched him, tapping his teeth with the rim of his eyeglass. Dope |Sax Rohmer 
His interlocutor began, with a shake of the eyeglass, to shift and sidle again, as if distinctly excited by the subject. The Awkward Age |Henry James 
Verinder helped himself to a sandwich, ogling Moya the while with his eyeglass. The Highgrader |William MacLeod Raine 
"We mustn't put it too baldly," observed Southend, dangling his eyeglass. Tristram of Blent |Anthony Hope 
Mr. O'Royster adjusted his eyeglass and looked with a sort of serene curiosity at the man. Tin-Types Taken in the Streets of New York |Lemuel Ely Quigg