My upcoming April birthday would be a celebration of rebirth. The Anatomy Of A Breast Cancer Survivor: ‘Early Detection Saved My Life’ |Charli Penn |October 6, 2020 |Essence.com 
With the momentum on high, she’s now welcoming the second collection release since the rebirth of her brand. How Designer Elle Mambetov Started Again |Nandi Howard |September 28, 2020 |Essence.com 
Unfortunately, there has been like a rebirth of an awakening of hatred and racism that is in your face. Debbie Allen’s Grandmother Love Doubled |Joi-Marie McKenzie |September 11, 2020 |Essence.com 
Whether this rebirth process happens in humans is profusely debated. Couch Potato No More: How the Benefits of Exercise Transfer to the Brain |Shelly Fan |July 14, 2020 |Singularity Hub 
Moreover, the US constitution remains intact and federalism has undergone something of a rebirth since the start of the pandemic. As Minneapolis Burns, Trump’s Presidency Is Sinking Deeper Into Crisis. And Yet, He May Still Be Re-Elected |LGBTQ-Editor |June 2, 2020 |No Straight News 
The ancient Egyptian festival of Wepet Renpet (“opening of the year”) was not just a time of rebirth—it was dedicated to drinking. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 
It almost mirrors the Buddhist cycle of life, death, and rebirth. The Buddhist Business of Poaching Animals for Good Karma |Brendon Hong |December 28, 2014 |DAILY BEAST 
Before the 16thcentury, Spanish conquest, the Aztecs saw the skull as a symbol of rebirth. New Orleans’ Carnivalesque Day of the Dead |Jason Berry |November 1, 2014 |DAILY BEAST 
The lotus flower symbolized rebirth, and the red lotus is the lotus of compassion. ‘American Idol’ Bandleader Rickey Minor on His Favorite Performance and What It Takes to Win |Kevin Fallon |May 20, 2014 |DAILY BEAST 
Pittsburgh is a city with nine lives, having experienced more than one rebirth over the past century. Everybody’s Welcome at the Pittsburgh Banjo Club |Vivian Salama |April 23, 2014 |DAILY BEAST 
But to know the full charm of the great city, one must wake with it at some rebirth of dawn. The Dragon Painter |Mary McNeil Fenollosa 
As already stated, its rebirth dates from the second half of the seventeenth century. Yorkshire Dialect Poems |F.W. Moorman 
Renascence here means rebirth, and it is applied to the recovery of the entire Western world. The Outline of History: Being a Plain History of Life and Mankind |Herbert George Wells 
Hence the Renaissance was not merely a rebirth, as its name might suggest, but a new world culture. Elements of Folk Psychology |Wilhelm Wundt 
I am trying to tell them something of the ideal poetry that marked the rebirth of the Saxon genius. Seven Keys to Baldpate |Earl Derr Biggers