Another minister arose; a man not yet in his thirtieth year, his form stooped, as beneath the weight of sixty winters. Alone |Marion Harland 
Given at our palace of Hampton Court, the 24th day of July, 1679, and in the one and thirtieth year of our reign. The Loyalists of America and Their Times, Vol. 1 of 2 |Egerton Ryerson 
The reference is to the thirtieth chapter of Jeremiah, from which an extract is here made at length. Messages from the Epistle to the Hebrews |Handley C.G. Moule 
He continued to linger till the thirtieth, when he calmly expired, in the sixty-second year of his age. History of Prince Edward Island |Duncan Campbell 
At Thirtieth Street he got off the car and walked west to Silver's place. From Place to Place |Irvin S. Cobb