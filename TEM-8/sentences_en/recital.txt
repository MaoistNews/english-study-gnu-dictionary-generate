There were levels and evaluations and recitals where we had to play from memory the pieces we’d learned. My nemesis, the piano |Doree Shafrir |August 27, 2021 |Vox 
Student recitals have already been streaming on Juilliard’s website, but Juilliard Live also includes live streams and recorded performances from all departments and divisions. Dance companies are finding their footing on streaming services |Anying Guo |April 15, 2021 |Washington Post 
I always just saw Maybelline mascara in the house, and sometimes she would put it on me for recitals or auditions. Actress Storm Reid Is The Newest Face Of Maybelline |cmurray |March 19, 2021 |Essence.com 
They’re hosting church services, weddings, graduations, dance recitals, concerts, stand-up comedy. Drive-Ins Theaters, Once Fading, Are Becoming Community Centers During the Pandemic |Andrew R. Chow |August 6, 2020 |Time 
It happened late in the day and captured about as much attention as a middle-school band recital. Does Congress’s Budget Delay Actually Matter? |Daniel Stone |February 2, 2012 |DAILY BEAST 
By age 4, Condoleezza had already given her first music recital. One Woman's War on Gangs |Christine Pelisek |October 18, 2010 |DAILY BEAST 
She warbled and wobbled through the recital and was greeted with thunderous applause. Dead Cool: Florence Foster Jenkins |Simon Doonan |July 10, 2010 |DAILY BEAST 
There is no fear, here—at least not of something as insignificant as a recital. Alice, Bratty in Wonderland |Nicole LaPorte |February 28, 2010 |DAILY BEAST 
He need not stop further study, but whatever else he learns let him at least practise this daily recital for one month. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 
He produced a watch and studied it frowningly, then dismissed us and the recital of our troubles with a ponderous gesture. Raw Gold |Bertrand W. Sinclair 
She eyed his crafty face narrowly, as she inquired whether there was any news, and listened to his recital of Toby Crackits story. Oliver Twist, Vol. II (of 3) |Charles Dickens 
More than once the young officer would have cut short the recital, but this Havelock would not permit. The Red Year |Louis Tracy 
She is explaining a very sad “histoire” to the “type” next to her, intense in the recital of her woes. The Real Latin Quarter |F. Berkeley Smith