That concept also explains why frozen meat defrosts faster in water than in air. Busy beavers may be speeding thaw of Arctic permafrost |Kathiann Kowalski |August 31, 2020 |Science News For Students 
The virus’s tendency to survive longer in the cold bolstered New Zealand’s frozen food shipment theory. Mystery of New Zealand’s new COVID outbreak deepens as it rules out primary suspect |Naomi Xu Elegant |August 18, 2020 |Fortune 
The planet turned dwarf planet is pretty much an ice ball, with a surface that’s 98% frozen nitrogen and mountains made of water ice. The 5 best places to explore in the solar system—besides Mars |Neel Patel |August 17, 2020 |MIT Technology Review 
It’s kind of like a cross between sangria, a slushy and a daiquiri, with rum, wine, triple sec and frozen fruit. This Weekend: A Sangria Slushie Summer |Fiona Zublin |August 7, 2020 |Ozy 
Scientists reason that the faintness of the young sun should have meant that Earth remained frozen solid for the first half of its existence. How Earth’s Climate Changes Naturally (and Why Things Are Different Now) |Howard Lee |July 21, 2020 |Quanta Magazine 
Defrost overnight in the refrigerator (if frozen) and bake before serving. Make These Barefoot Contessa Chicken Pot Pies |Ina Garten |November 29, 2014 |DAILY BEAST 
Stir in the frozen peas and chicken, taste for seasonings, and pour the mixture into six (2-cup) ovenproof serving bowls. Make These Barefoot Contessa Chicken Pot Pies |Ina Garten |November 29, 2014 |DAILY BEAST 
Without it in the atmosphere, the Earth would be a barren, frozen wasteland. Extreme Weather? Blame the End Times |Jay Michaelson |November 28, 2014 |DAILY BEAST 
Since then, all dividend payments have been frozen and Iran receives “no uranium or revenue from the mine.” McCain Helps a Business Partner of Iran |Ben Jacobs |November 13, 2014 |DAILY BEAST 
He was later sued by his lawyers in London for failing to pay $419,400 in counsel fees when his assets were frozen. The Mysterious Death of the Art World’s Favorite Sheikh |Lizzie Crocker |November 13, 2014 |DAILY BEAST 
Thus he continued to rush over the frozen sea during a considerable part of that night. The Giant of the North |R.M. Ballantyne 
The former, in its frozen state, somewhat resembled hard butter. The Giant of the North |R.M. Ballantyne 
So you have lost that frozen heart of yours at last, and after such boasting, too! Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 
I thought that the frozen surface of the American woman thawed on the stratum soubrette. Ancestors |Gertrude Atherton 
Dorothy led Jack off to the stable, and the half-frozen yeoman turned in to enjoy his cheerful fire. The World Before Them |Susanna Moodie