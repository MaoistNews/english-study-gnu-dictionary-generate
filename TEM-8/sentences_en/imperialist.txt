Islands overrun by flawed people, both indigenous and imperialist. How Haoles Destroyed Hawaii |Wendy Smith |December 7, 2014 |DAILY BEAST 
Soviet leaders had already decried the rescue mission as an act of imperialist aggression. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 
Wilson wanted to end imperialist rivalries in European politics. How WWI Produced the Holocaust |J.P. O’Malley |November 21, 2014 |DAILY BEAST 
The regime has, to date, never definitively weighed in on whether these troublesome critters had imperialist or fascist ties. Who Will Maduro Blame for Venezuela’s Blackout This Time? |Daniel Lansberg-Rodríguez |June 28, 2014 |DAILY BEAST 
The name change, it said, smacked of “anachronistic imperialist ambitions.” The Pope and the Queen to Square up Over Falklands |Tom Kington |April 2, 2014 |DAILY BEAST 
Many a hot argument have we had about Boer and Briton; and I'm afraid he thinks me but a knock-kneed imperialist. In the Ranks of the C.I.V. |Erskine Childers 
The Venezuela incident and the recent Jubilee ceremonies had fanned imperialist sentiment. The Canadian Dominion |Oscar D. Skelton 
Conservatives and Unionists, almost indistinguishable, were waving the Imperialist banner in the face of the world. The Arbiter |Lady F. E. E. Bell 
Such, apparently, is the simple doctrine of this typical imperialist. The Outline of History: Being a Plain History of Life and Mankind |Herbert George Wells 
Yet the imperialist movement in Great Britain never had the authority nor the unanimity it had in Germany. The Outline of History: Being a Plain History of Life and Mankind |Herbert George Wells