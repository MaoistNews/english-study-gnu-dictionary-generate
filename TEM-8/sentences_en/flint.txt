For more than an hour, they were all sitting there around the bedside — Flint, his wife and his sister-in-law. So You Got Vaccinated … And Then You Got COVID. Now What? |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |February 23, 2021 |FiveThirtyEight 
Reyes, a Flint native who leads We the People Michigan, was expecting this. The Secret History of the Shadow Campaign That Saved the 2020 Election |Molly Ball |February 4, 2021 |Time 
Criminal charges and a class-action settlement may seem like the last chapter in Flint’s story, which has already begun to fade in public memory. The Unfinished Business of Flint’s Water Crisis |by Anna Clark |January 22, 2021 |ProPublica 
He admits that Flint used to be just “another town through which I’ve passed free of claim.” In ‘Standpipe,’ David Hardin offers poignant, fleeting reflections on the Flint water crisis |Kerri Arsenault |January 21, 2021 |Washington Post 
We must remember that the Flint water crisis is not some relic of the past. Ex-Michigan governor indicted for “willful neglect” in Flint water crisis |Jon Brodkin |January 14, 2021 |Ars Technica 
“If you are a waiter, you can make twice as much in Austin relative to Flint,” remarked Moretti. The Rustbelt Roars Back From the Dead |Joel Kotkin, Richey Piiparinen |December 7, 2014 |DAILY BEAST 
That side is volunteering extensively in his hometown of Flint, and recently, pastoring Charity United Methodist Church. A Black Cop’s Tough Words for Mike Brown |Mary M. Chapman |December 3, 2014 |DAILY BEAST 
The third eaglet was never found despite a search by the Flint Creek volunteers and the landowner. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 
Born in Flint, Michigan, Bragman says he was a “fat gay Jewish kid… who grew up to be a Martian.” How Howard Bragman Orchestrated Michael Sam’s Coming Out |Itay Hod |March 12, 2014 |DAILY BEAST 
Either way, a whistle, just a flint of music, rang out that Sunday. From Emmett Till to Jordan Davis, a Foolish, Lethal Fear of Black Teens |Joshua DuBois |February 9, 2014 |DAILY BEAST 
When the men worked on their flint points, Fleetfoot liked to play near the workshop. The Later Cave-Men |Katharine Elizabeth Dopp 
While the men struck off large flint flakes, Fleetfoot played not far away. The Later Cave-Men |Katharine Elizabeth Dopp 
Straightshaft let him try, but Fleetfoot was not strong enough to press off hard flint flakes. The Later Cave-Men |Katharine Elizabeth Dopp 
Can you think of any way of removing little pieces of flint besides striking them off? The Later Cave-Men |Katharine Elizabeth Dopp 
Scarface always used flakes of flint for the points of spears and javelins. The Later Cave-Men |Katharine Elizabeth Dopp