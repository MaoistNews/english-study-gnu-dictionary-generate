It is very easy indeed to generalise about a class or human beings, but much harder to produce a sample. The New Machiavelli |Herbert George Wells 
People who have to do with hundreds of young men at a time are unavoidably compelled to generalise. Oxford |Andrew Lang 
But I promised you not to generalise, and perhaps there will be more expression when we arrive. Lady Barbarina |Henry James 
It ought never to be forgotten that the observer can generalise his own observations incomparably better than any one else. More Letters of Charles Darwin Volume II |Charles Darwin 
Now I would say it is your duty to generalise as far as you safely can from your as yet completed work. More Letters of Charles Darwin Volume II |Charles Darwin