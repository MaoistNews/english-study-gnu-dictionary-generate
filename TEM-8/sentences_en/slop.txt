This waterproof boot helped us love winter, whether we were on wet slop, powder, hiking trails, or city streets. The Best Winter Hikers of 2022 |jversteegh |October 26, 2021 |Outside Online 
Specifically, it doesn’t produce as much downward pressure on the heel of your boot, so in a powered-up turn, the G-forces can overcome the binding and result in lateral slop. Marker’s New Touring Binding Does (Almost) Everything |Marc Peruzzi |February 22, 2021 |Outside Online 
Out of the wok comes an umami-rich slop, fried rice only in memory. Celebrate the House Meal, the Go-To Dish for When There’s No One to Satisfy but Yourself |Jaya Saxena |September 30, 2020 |Eater 
But draft one can be a bit “less hard” because you just slop it down. Thriller Author Steve Berry: How I Write |Noah Charney |July 11, 2012 |DAILY BEAST 
I never apprehended,” puts in Dr. Slop, “that such a thing was ever thought of—much less executed. A Cursory History of Swearing |Julian Sharman 
But, when I looked down, I saw that she had taken away the good egg and left the bad one—all green and yellow—in the slop basin. Masterpieces of Mystery, Vol. 1 (of 4) |Various 
May-be you expected to have a china bowl and pitcher in your room, and somebody to empty your slop. The English Orphans |Mary Jane Holmes 
Slop sinks have practically the same connections as the closets. Elements of Plumbing |Samuel Dibble 
Wash the slop jar and chamber with the cleaning substance or soap and the stiff brush. The Library of Work and Play: Housekeeping |Elizabeth Hale Gilman