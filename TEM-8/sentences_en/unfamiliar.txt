Somehow though, through it all, McCarthy claims he’s still unfamiliar with this thing called QAnon. The GOP: the new know-nothing party |Aaron Blake |February 4, 2021 |Washington Post 
One of the biggest tricks that we have is blending familiar with unfamiliar. The Downside of Disgust (Ep. 448) |Stephen J. Dubner |January 21, 2021 |Freakonomics 
A poll from the Economist and YouGov shows that Hawley is still unfamiliar to many Americans, but among those who do have an opinion of him, it’s 2-to-1 negative. From bad to worse for Josh Hawley |Aaron Blake |January 14, 2021 |Washington Post 
Bradley Beal found himself in a somewhat unfamiliar position at the close of the Washington Wizards’ 128-107 win over the Phoenix Suns on Monday night. Minus Russell Westbrook and Thomas Bryant, Bradley Beal powers Wizards past Suns |Ava Wallace |January 12, 2021 |Washington Post 
His first recordings were for the Vox label, which specialized in the discovery of unknown young artists, usually in mostly unfamiliar material. Ivry Gitlis, Israeli violinist who mesmerized global audiences, dies at 98 |Tim Page |December 26, 2020 |Washington Post 
For those unfamiliar, soba is buckwheat noodle dish—and they proved much more popular amongst the public. Inside The World’s 10 Oldest Restaurants |Justin Jones |December 20, 2014 |DAILY BEAST 
For those unfamiliar with Michals, an annotated biography and useful essays are included. The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 
Artists like Orsola Maddalena Caccia will be unfamiliar to most, and yet she has six works in the exhibition. The Virgin Mary Lookbook |William O’Connor |December 7, 2014 |DAILY BEAST 
Time plus desire can you lead you to some strange unfamiliar places. Michael Sheen’s Masterful Study of Sex and Insecurity |Caryn James |September 28, 2014 |DAILY BEAST 
In Crimson Room, players awoke in an unfamiliar room with no way of knowing how to escape. Escape the Room—New York's Hottest Game |Justin Jones |September 15, 2014 |DAILY BEAST 
In search of a lost handkerchief, they regained the Clayhanger premises by an unfamiliar side door. Hilda Lessways |Arnold Bennett 
He had been something real and tangible in that shadowy place—something familiar in an unfamiliar world. The Amazing Interlude |Mary Roberts Rinehart 
Weston was in no mood to discuss questions of that kind, though the curious sensation was not altogether unfamiliar to him. The Gold Trail |Harold Bindloss 
There was a slight, persistent tingling vibration in everything that was unfamiliar, too. The Stars, My Brothers |Edmond Hamilton 
This book was the work of some one unfamiliar, unrecognizable, forgotten by the happy woman that she was. The Creators |May Sinclair