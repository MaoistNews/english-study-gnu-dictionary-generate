Clarke also posted shots of the ad and the shoe on Instagram. Louis Vuitton—yes, that Louis Vuitton—has created a pro skate shoe |Marc Bain |August 25, 2020 |Quartz 
Try raising it and attaching it to the studs, leaving room below for your shoe collection. Living large in small spaces |Valerie Blake |August 8, 2020 |Washington Blade 
The theory suggests that when I put myself in your shoes, my brain tries to copy the computations inside your brain. How the Brain Builds a Sense of Self From the People Around Us |Sam Ereira |July 17, 2020 |Singularity Hub 
Attached to the sole of a shoe, it stays flat as someone stands. Shape-shifting cuts give shoes a better grip |Carolyn Wilke |July 14, 2020 |Science News For Students 
With this information, they can do anything from show us an ad for a pair of shoes we’ll probably like to try to change our minds about which candidate to vote for in an election. Your Personal Data Is Worth Money. Andrew Yang Wants to Get You Paid |Vanessa Bates Ramirez |June 28, 2020 |Singularity Hub 
If we begin to see the other as our possession and commodity, our shoe, the shadow of our shadow, is there ever a happy outcome? Owning Up to Possession’s Downside |Samantha Harvey |December 14, 2014 |DAILY BEAST 
They seem to belong to us, and then they freely go—behavior very uncharacteristic of a shadow or a shoe. Owning Up to Possession’s Downside |Samantha Harvey |December 14, 2014 |DAILY BEAST 
If I say “my shoe,” do I mean it in the same way as “my life,” or “my sister” or “my husband”? Owning Up to Possession’s Downside |Samantha Harvey |December 14, 2014 |DAILY BEAST 
And a perfectly amber whisky might as well taste like an old shoe. Why Natural Color Is So Crucial To Understanding A Whisky’s Flavors | |December 10, 2014 |DAILY BEAST 
And Christopher Walken warbling and doing a little soft-shoe? ‘Peter Pan Live!’ Review: No Amount of Clapping Brings It to Life |Kevin Fallon |December 5, 2014 |DAILY BEAST 
Bondad sua, seor, I'll be sworn there is not one fit to tie the latchet of your shoe in the whole army. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 
I should judge from the streets that not more than one-fourth of the females of Galway belong to the shoe-wearing aristocracy. Glances at Europe |Horace Greeley 
The pig-headed prowler I saw, with my pompon missing from his shoe, and his bonne amie wearing the stolen ring. The Joyous Adventures of Aristide Pujol |William J. Locke 
Buckles were first worn as shoe fastenings in the reign of Charles II. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 
With her little satin shoe she tapped the carpet, biting her under lip and seeming to be listening. Dope |Sax Rohmer