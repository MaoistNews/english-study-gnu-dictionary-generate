Sprawling across a quarter of the globe, the nineteenth and twentieth-century British Empire was the largest in history. Britain Can No Longer Hide Behind the Myth That Its Empire Was Benign |Caroline Elkins |April 2, 2022 |Time 
This was all the rage among Bible scholars in the nineteenth century. Christian Bale: One Man's Moses Is Another Man's Terrorist |Candida Moss, Joel Baden |December 7, 2014 |DAILY BEAST 
First appearing in 1779, sex-assignment surgeries became increasingly popular from the nineteenth century onward. Intersexuality and God Through the Ages |Candida Moss |November 9, 2014 |DAILY BEAST 
A series of political movements and cultural revolutions changed this, beginning as far back as the nineteenth century. Climate Change Needs the Politics of the Impossible |Jedediah Purdy |April 6, 2014 |DAILY BEAST 
The origins of the term dyslexia can be traced back to late nineteenth century Europe. Your Kid's Dyslexia Diagnosis Is B.S. |Julian Elliot |April 1, 2014 |DAILY BEAST 
He has compared attempts to justify slavery as being like attempts to justify slavery in the nineteenth century. Brian May Compares Royal Hunting To Slavery |Tom Sykes |February 19, 2014 |DAILY BEAST 
The "new world" was really found in the wonder-years of the eighteenth and early nineteenth centuries. The Unsolved Riddle of Social Justice |Stephen Leacock 
The economists and the leading thinkers of the nineteenth century were in no doubt about this question. The Unsolved Riddle of Social Justice |Stephen Leacock 
The narrow individualism of the nineteenth century refused to recognize the social duty of supporting somebody else's grandmother. The Unsolved Riddle of Social Justice |Stephen Leacock 
The attitude of the nineteenth century upon this point was little short of insane. The Unsolved Riddle of Social Justice |Stephen Leacock 
As my friend said, could any one believe this of a well-educated man in the nineteenth century? Birds of Guernsey (1879) |Cecil Smith