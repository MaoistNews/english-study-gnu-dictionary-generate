That is decidedly not to say that politics and economics are irrelevant. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 
One green-eyed man, nicknamed “Cai the Roman,” became an instant celebrity due to his decidedly Roman physical characteristics. The Chinese Town Descended From Romans? |Nina Strochlic |December 4, 2014 |DAILY BEAST 
But for Carter, this is a capstone to a career spent in the decidedly unsexy corridors of Pentagon power. Ashton Carter, the Wonk Who Would Lead the Pentagon |Shane Harris, Tim Mak |December 2, 2014 |DAILY BEAST 
It can be bad enough to have your clothes described in the decidedly unsexy way as “plus-size.” Let’s Get Rid of ‘Plus-Size’ for Good |Emily Shire |November 12, 2014 |DAILY BEAST 
Outside the United States and Western Europe, however, the picture is decidedly mixed. It Gets Better—but Mostly if You Live in a Rich, Democratic Country |Jay Michaelson |November 11, 2014 |DAILY BEAST 
Ned reached home about breakfast time, and "fetched up" at the back door, with a decidedly guilty countenance. The Book of Anecdotes and Budget of Fun; |Various 
The party at Walls End Castle, though its elements were decidedly heterogeneous, was a success. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
But there was something about the chilly expression in her eyes that made Delancy feel decidedly uncomfortable. Hooded Detective, Volume III No. 2, January, 1942 |Various 
It was a little on one side and gave the good clergyman a decidedly rakish appearance. The Campfire Girls of Roselawn |Margaret Penrose 
If you have a reason for declining to play, do so decidedly when first invited, and do not change your decision. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley