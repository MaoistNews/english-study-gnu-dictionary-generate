The magazine kept its readers in the know about a range of fields, announcing the discovery of penicillin —which one reporter mused “may turn out to be a useful antiseptic” — and tracking the emerging field of quantum mechanics. We’re celebrating a century of Science News |Maria Temming |March 24, 2022 |Science News 
Later, motivated by wartime demand, chemists honed their craft with poison gas, explosives and propellants, as well as disinfectants and antiseptics. Materials of the last century shaped modern life, but at a price |Carolyn Wilke |January 28, 2022 |Science News 
Hydrogen peroxide is an antiseptic more commonly used for minor cuts, burns and scrapes. Asthma group warns against social media trend of inhaling hydrogen peroxide to treat coronavirus |Aaron Gregg |September 22, 2021 |Washington Post 
Sure, some of those “nuances” may include squealing, grinding, and the occasional bout of fork judder, but if you want an antiseptic riding experience, you might as well skip bicycles altogether and go lease a Kia Telluride. Long Live Old Bikes |Eben Weiss |June 8, 2021 |Outside Online 
Portable and compact, these boxes are packed with helpful items, including gauze pads, exam gloves, and antiseptic towelettes. The best first aid kits for staying safe and prepared |PopSci Commerce Team |September 4, 2020 |Popular-Science 
The petroleum industry has depicted fracking as a few antiseptic drills dug on peaceful farmland. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 
Getting off the elevator at the fourth floor, he thumped across the antiseptic hallway. Football Great Bob Suffridge Wanders Through the End Zone of Life |Paul Hemphill |September 6, 2014 |DAILY BEAST 
And I have to have whiskey because alcohol is both an antiseptic and an anesthetic. My Reverse-Cyrano Moment Wooing the Supreme Court |P. J. O’Rourke |March 30, 2014 |DAILY BEAST 
Before antibiotics, most newborn eyes were treated with an antiseptic, and Barnes pushed Argyrol as the cure of choice. Philadelphia’s Reopened Barnes Foundation Puts Its Masterpieces in a Better Light |Blake Gopnik |May 18, 2012 |DAILY BEAST 
How did that survive the vetting at this often timid and antiseptic White House? Michael Tomasky: With Joe Biden’s Speech, The Democrats Finally Man Up |Michael Tomasky |April 28, 2012 |DAILY BEAST 
He used an antiseptic solution which Bert had provided, for Bert was still buying my milk. The Idyl of Twin Fires |Walter Prichard Eaton 
We simply fill the box with gas at ordinary temperature, taking advantage only of its antiseptic properties. Yachting Vol. 2 |Various. 
The introduction of zinc oxid, aromatic and antiseptic oils and animal fat was a new feature. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 
Hexamethylenamin is a valuable so-called urinary antiseptic—​probably one of the best we have. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 
Quickly render the whole mouth and throat thoroughly antiseptic. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various