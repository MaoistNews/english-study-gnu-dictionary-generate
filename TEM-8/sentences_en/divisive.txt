Democrats have their agenda to pass, and Republicans don’t want to have to dwell on anything potentially divisive within their party. What Does Impeachment And The Division Over Marjorie Taylor Greene And Liz Cheney Tell Us About The GOP? |Sarah Frostenson (sarah.frostenson@abc.com) |February 3, 2021 |FiveThirtyEight 
Today, the Harvard International Negotiation Program is exploring how to use the Overview Effect to mitigate the “tribes effect,” a divisive mindset that fosters conflict. Can Astronaut Thinking Heal America at Last? |Leslie dela Vega |January 28, 2021 |Ozy 
We don’t feel this way because of disagreements over policy, though, even when it comes to the most divisive questions. Our Radicalized Republic |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |January 25, 2021 |FiveThirtyEight 
He had hoped it would be a sign that, after a divisive campaign, the president would be able to unify the country. Inauguration Day, in 2017 and 2021 | |January 20, 2021 |Washington Post 
This last year has been so divisive, and so much has happened to pull people apart. After a year of pain and strife, volunteers see MLK Day as a way to come together |Joe Heim |January 18, 2021 |Washington Post 
Johnson knew that the proposals he was going to send to the Hill would be divisive. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 
The Democrats were able to sideline Kucinich and avoid a divisive impeachment battle. Repubs Should Take It From Kucinich: Impeachment Isn’t Worth It |Eleanor Clift |December 5, 2014 |DAILY BEAST 
Past winning artists generally were either complete outsiders (McQueen) or divisive figures in the art world. Has the Turner Prize Gone Soft? |David Levesley |December 2, 2014 |DAILY BEAST 
But getting the bill implemented proved to be highly divisive. The Daily Beast’s Best Longreads, Oct 6-12, 2014 |William Boot |October 12, 2014 |DAILY BEAST 
These will be essential to dealing with this incredibly divisive issue. What the GOP Will Do If It Wins Congress |Stuart Stevens |October 3, 2014 |DAILY BEAST 
New and divisive questions which lead only to faction are propounded so that the voters are confused. Humanly Speaking |Samuel McChord Crothers 
Only the last two of these had any lasting importance as divisive issues. An American Religious Movement |Winfred Ernest Douglas 
Such a conception of the Deity proves divisive rather than unifying. Religion and the War |Various 
Their experiences have no sharp edges, no abrupt precipices, no divisive gulfs, no defined beginnings and endings. Murder Point |Coningsby Dawson 
The divisive force is International Communism and the power that it controls. United States Presidents' Inaugural Speeches |Various