Handsome Feilding at once proceeded to materialise his bargain, and at last the termagant was tamed. Court Beauties of Old Whitehall |W. R. H. Trowbridge 
But they did not materialise; the rule of Martial Law—bad to beat—remained unbeatable. The Siege of Kimberley |T. Phelan 
Who can never materialise because she's mostly made up of dreams. Poppy |Cynthia Stockley 
Who so fit to materialise reforms as the man who had conceived them? Sir John French |Cecil Chisholm 
Rumours drifted around about accompanying the 41st division to Italy, but they did not materialise. The Seventh Manchesters |S. J. Wilson