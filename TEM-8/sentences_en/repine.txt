Seek not then to stay me; when the Great Spirit calleth, weep not and repine not, for then is the hour of my deliverance. The Devil-Tree of El Dorado |Frank Aubrey 
"We must not repine," he wrote to his wife on the 12th of October, the day after Hood sailed for England. The Life of Nelson, Vol. I (of 2) |A. T. (Alfred Thayer) Mahan 
But there—we must not repine—even in my sorrow, I feel how much we have to be thankful for. A Final Reckoning |G. A. Henty 
But for us it is not fitting to question or repine, but rather to rejoice in the rare possession that we hold. The Poems of Emma Lazarus |Emma Lazarus 
I repine, I find a reluctation of spirit against believing that this is the place. The Works of Robert Louis Stevenson - Swanston Edition Vol. XXII (of 25) |Robert Louis Stevenson