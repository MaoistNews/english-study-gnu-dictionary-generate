If we were to laugh, it would never be at the expense of anyone who was suffering. How Hallmark is handling pandemic Valentine’s Day |Luke Winkie |February 12, 2021 |Vox 
I’ve suffered from lower-back problems for years, and I’m so tight that I scream out loud when I try to touch my toes. 5 Pieces of Gear That Help Me Stay Active During Winter |Jakob Schiller |February 11, 2021 |Outside Online 
Let’s say you’re suffering from deep depression, and you call a doctor’s office. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 
If you’re someone whose extremities suffer in the cold, think about investing in hand warmers. Best hand warmers: Block the chill during your favorite winter activities |PopSci Commerce Team |February 10, 2021 |Popular-Science 
We have been suffering a lot, so we need people to work with us, with our language translated for us. African Communities Warn Language Issues Could Shut Them Out of Redistricting |Maya Srikrishnan |February 10, 2021 |Voice of San Diego 
I suffer from no delusion that the justice system treats black and white equally. Bill de Blasio’s Tea Party Problem |Will Cain |December 30, 2014 |DAILY BEAST 
How does it happen that citizens of modest means suffer as public sector unions gain? How Public Sector Unions Divide the Democrats |Daniel DiSalvo |December 29, 2014 |DAILY BEAST 
“One-third of South Asians and more than half of all Sub-Saharan Africans suffer from malnutrition or undernutrition,” he writes. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 
The birds are debeaked, suffer ulcers, and terrible feet conditions. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 
He was instructed several times to abuse the kids, he says, or he would suffer the abuse. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 
We suffer, nearly all of us, from a lack of quantitative grasp and from an imperfect grasp of form. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
My mother opposed her vow to his; not to suffer her child to leave her, till the time of her being professed. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 
They will try to compel you to confession; and, though you are blameless, you will suffer the cruelest ordeal of transgression. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 
Is there any earthly father who would allow his children to suffer as God allows Man to suffer? God and my Neighbour |Robert Blatchford 
In that case, Valerie, you shall suffer no constraint; you shall continue here as you have done. St. Martin's Summer |Rafael Sabatini