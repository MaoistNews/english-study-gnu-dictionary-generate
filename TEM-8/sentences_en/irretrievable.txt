Unlike adults, many of the events and milestones they missed out on are irretrievable. What happened to America’s teens when coronavirus disrupted high school? |Moriah Balingit |October 28, 2021 |Washington Post 
His death during the storm was recognized as an irretrievable loss of part of what made the neighborhood the neighborhood. Rockaway Mailman Rick Gold Killed in Storm and Mourned on His Route |Michael Daly |November 9, 2012 |DAILY BEAST 
But she and her family have learned enough about irretrievable loss to keep a hard-earned perspective. Heroism in the Rockaways After Hurricane Sandy |Michael Daly |November 6, 2012 |DAILY BEAST 
Besides this good news, the "Mutine" brought word of another misfortune, more irretrievable than the loss of spars. The Life of Nelson, Vol. I (of 2) |A. T. (Alfred Thayer) Mahan 
After all the demolitions of Totila, the ruin was not irretrievable. Theodoric the Goth |Thomas Hodgkin 
We know that the fact is irretrievable, and struggle to be proud of what we cannot help. The Two Admirals |J. Fenimore Cooper 
The qualities by which court is made to the people, were to render every fault inexpiable, and every error irretrievable. Thoughts on the Present Discontents |Edmund Burke 
A false move now, the least slip of a tongue aching to rain curses on Power, and irretrievable mischief would be done. The Terms of Surrender |Louis Tracy