Those who may have been obstinate about the vaccines when called by a pollster seem to have been a bit more flexible when called by their bosses. Turns out a lot of those never-vaxxers were really ‘I’ll get it if required’ |Philip Bump |September 30, 2021 |Washington Post 
In Illinois, one parent was so obstinate in her refusal to properly wear her mask, she was arrested and banned from school grounds. ‘COVID Has Broken You People’: Pennsylvania School District Melts Down Over Mask Battle |Justin Rohrlich |September 1, 2021 |The Daily Beast 
While some people may face legitimate obstacles, others are just obstinate. Yes, Covid-19 vaccine mandates are legal |Ian Millhiser |July 30, 2021 |Vox 
Her call was greeted with skepticism by some who see unions as having been overly cautious or outright obstinate. Teachers union chief calls for full return to school this fall |Laura Meckler |May 13, 2021 |Washington Post 
In practice, though, it’s a mechanism that allows a lone obstinate voice to deny the majority the ability to govern. Why the Filibuster Defenders' Ranks Are Shrinking |Philip Elliott |March 11, 2021 |Time 
Obstinate Pierre refuses to care about anything, even when a passing lion threatens to eat him. When Activism Is Worse Than Apathy |Nancy Kaffer |October 6, 2014 |DAILY BEAST 
The president also learned that the Palestinians and the Arab states were every bit as obstinate as Netanyahu. From Washington To Sderot |Jonathan Rynhold |February 7, 2013 |DAILY BEAST 
The more the Republicans lose, the more stubborn and obstinate they become. Starbucks’ Effort to Solve the Fiscal Cliff Probably Won’t Work |Daniel Gross |December 26, 2012 |DAILY BEAST 
It is the story of a village of unlikely Davids going up against a notoriously obstinate governmental Goliath. The Power of Documentary: Danny Glover, Djimon Hounsou, and ‘Budrus’ Director Julia Bacha |Kevin Fallon |November 18, 2012 |DAILY BEAST 
He predicted the embattled and obstinate leader will fall in a matter of days. A Strongman's Last Stand |Julia L. Ritchey |April 5, 2011 |DAILY BEAST 
The battle was for a long time maintained by both armies with obstinate energy. The Every Day Book of History and Chronology |Joel Munsell 
Now this is what we call a "pursuit of knowledge under difficulties" of the most obstinate kind. The Book of Anecdotes and Budget of Fun; |Various 
Obstinate as he was, the girl's frank honesty conquered the angry old man. The World Before Them |Susanna Moodie 
They are sometimes obstinate and are desperate fighters, squealing and neighing on all occasions. Our Little Korean Cousin |H. Lee M. Pike 
The men in charge of the boat were slow and obstinate, and consequently it took a long time for all to get across the river. Our Little Korean Cousin |H. Lee M. Pike