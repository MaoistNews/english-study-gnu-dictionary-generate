Scientists can conclude from those tiny ridges how active someone had been. Women like Mulan didn’t need to go to war in disguise |Bethany Brookshire |September 4, 2020 |Science News For Students 
I don’t know if I’d normally be hiking at eleven o’clock at night, up on some ridge, during a thunderstorm. Inside an FKT Attempt on the Appalachian Trail |Martin Fritz Huber |September 3, 2020 |Outside Online 
As a mountain-bike trail builder and rider, I have an intimate knowledge of the contours of the forest, of each ridge and each drainage. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 
One, which came to be known as the CZU Lightning Complex fire, was burning across the ridges and through the canyons towards Nichols’s house. We Lost Our Home to a Wildfire |Wallace J. Nichols |August 25, 2020 |Outside Online 
The traditional “river groover” is an ammo can with an airtight top, which cuts ridges in your behind if you sit on it for too long—hence the name. The Best Portable Toilets for Camping |Heather Hansman |August 23, 2020 |Outside Online 
The fundamental issue: the boundaries of the underwater Lomonosov Ridge. Russia Preps Its North Pole Invasion |Dave Majumdar |November 8, 2014 |DAILY BEAST 
That should immediately destroy any illusions about how “libertarian” the Liberty Ridge ruling might be. Gay Marriage Vs. the First Amendment |James Poulos |August 22, 2014 |DAILY BEAST 
This vision, in the context of the incident at Liberty Ridge, reveals a substantial weakness in our constitutional framework. Gay Marriage Vs. the First Amendment |James Poulos |August 22, 2014 |DAILY BEAST 
To dwell on that for a moment is to get a sharp taste of the overarching issue that Liberty Ridge raises for us. Gay Marriage Vs. the First Amendment |James Poulos |August 22, 2014 |DAILY BEAST 
High on the slopes of Everest, some 70 sherpas surged over a ridge to see the beating. Breaking Mount Everest’s Glass Ceiling |Amanda Padoan, Peter Zuckerman |March 30, 2014 |DAILY BEAST 
With this company he had rendered valiant service in the campaign which ended with the battle of Pea Ridge. The Courier of the Ozarks |Byron A. Dunn 
A short distance off was another ridge or spur of the mountain, widening out into almost a plateau. Ramona |Helen Hunt Jackson 
I turned away from the bank and raced up a long slope to a saw-backed ridge that promised largely of unobstructed view. Raw Gold |Bertrand W. Sinclair 
It lit up every ridge and hollow for two or three seconds, and showed me four riders tearing up the slope at a high run. Raw Gold |Bertrand W. Sinclair 
It was kind of Josiah to come, for he is an old man and lives a full mile above the village, half way up the ridge-side. The Soldier of the Valley |Nelson Lloyd