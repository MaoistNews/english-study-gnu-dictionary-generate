For Kirke it was being paid to pretend to play the oboe that heightened her affair with classical music. ‘Mozart in the Jungle’: Inside Amazon’s Brave New World of Sex, Drugs, and Classical Music |Kevin Fallon |December 23, 2014 |DAILY BEAST 
Boring math geniuses, oboe-playing poets, and rich kids from New York need not apply. Dirty Secrets of College Admissions |Kathleen Kingsbury |January 9, 2009 |DAILY BEAST 
Rich kids from New York, boring Asian math geniuses, and oboe-playing poets need not apply. Dirty Secrets of College Admissions |Kathleen Kingsbury |January 9, 2009 |DAILY BEAST 
A musical prodigy, seven years old, who will order the fifth oboe out of the Albert Hall as soon as look at him. Punch, or the London Charivari, May 13, 1914 |Various 
It is the timid oboe that sounds the A for the orchestra to tune by. How to Listen to Music, 7th ed. |Henry Edward Krehbiel 
The young man in the red velvet cap plays on the violoncello; the other on the oboe, of which only the reed is visible. A Popular Handbook to the National Gallery, Volume I, Foreign Schools |Various 
He also had lessons in the vestry room of the Octagon Chapel; and he acquired some skill upon the flute and oboe from Mr. Fish. A Comprehensive History of Norwich |A. D. Bayne 
An oboe of ivory, carved by Anciuti in Milan, beginning of the eighteenth century. Musical Myths and Facts, Volume I (of 2) |Carl Engel