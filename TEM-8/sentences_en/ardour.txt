But Jack Scott came in and entered into the “game,” as he called it, with ardour. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 
Are you sufficiently grateful to me for having quelled your matrimonial ardour of two months ago? St. Martin's Summer |Rafael Sabatini 
They show to the full the secret of the Marshal's success as a soldier, the blending of ardour with method and dash with caution. Napoleon's Marshals |R. P. Dunn-Pattison 
It seemed an unpromising subject, but they fell upon it with ardour, and found it strangely fruitful. Bella Donna |Robert Hichens 
The main certainty about his hapless157 expedition to Ireland is the certainty that he fought with the most chivalrous ardour. King Robert the Bruce |A. F. Murison 
It impresses with a sense of increased obligation, that furnishes an ardour of mind, powerfully impelling to duty. The Ordinance of Covenanting |John Cunningham