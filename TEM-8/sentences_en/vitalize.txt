If they borrow from others, they assimilate the information, and thus vitalize it before giving it out again. Elementary Guide to Literary Criticism |F. V. N. Painter 
It is a plain question of your ability to choose, arrange and vitalize things. Atlantic Classics |Various 
The true distribution would require that each man should possess what goods he could animate and vitalize. Atlantic Classics |Various 
The deeds of the Canadian army in this World War will vitalize the pages of the nation's history in all the years-to-be. The Story of the Great War, Volume VIII (of VIII) |various 
It is seeking to vitalize our good impulses and render them effective by acting on them whenever opportunity offers. The Mind and Its Education |George Herbert Betts