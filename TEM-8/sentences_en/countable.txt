For Decrypt, it’s not even a countable portion of the company’s revenue, Roazzi-Laforet added. Media Briefing: Publishers are tapping internal first-party data ‘influencers’ to rally their broader organizations |Tim Peterson |December 9, 2021 |Digiday 
One, two, three, and so on, to countable infinity, denoted somewhat biblically as 0ℵ, or aleph-zero. How I Escaped My Troubles Through Science - Issue 104: Harmony |Subodh Patil |August 25, 2021 |Nautilus 
Meanwhile, the countable process in the animation above, if allowed to run forever, will contain ℵ0 points. Can You Bowl Three Strikes? |Zach Wissner-Gross |June 18, 2021 |FiveThirtyEight 
I don't believe they are really any relation to Lady Myrtle—at least not anything countable. Robin Redbreast |Mary Louisa Molesworth 
The lightning came, in one of those broad, sheetlike flickers that seem to irradiate the world for countable seconds. The Flying Death |Samuel Hopkins Adams 
If I'm to be held 'countable he doesn't live here no longer; I know that much.' Demos |George Gissing 
This was not sufficient, because though visible not sufficiently tangible, countable, and tariffable. A Decade of Italian Women, vol. I (of 2) |T. Adolphus Trollope 
Bemebibi, chief of the Lesser Isisi, was too fat a man for a dreamer, for visions run with countable ribs and a cough. Bones |Edgar Wallace