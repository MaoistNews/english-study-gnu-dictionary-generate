Guy has a troublesome wife who won’t divorce him, and Bruno, an engaging sociopath is enraged that his wealthy father won’t give him money. Highsmith at 100: Literary legacy marred by racism |Kathi Wolfe |February 10, 2021 |Washington Blade 
It enraged a cadre of coastal residents who have long believed the city could outright ban short-term rentals. What’s Behind the Effort to Recall Council President Jen Campbell |Scott Lewis |February 9, 2021 |Voice of San Diego 
She can’t find the words to express her true feelings for her father, and this enrages the feeble-minded king. The cloud of impeachment hangs over the markets—even Bitcoin is sinking |Bernhard Warner |January 11, 2021 |Fortune 
In 1970, Milton Friedman wrote an op-ed that would embolden investors and enrage activists for decades. Investors are starting to demand better of the companies they own |jakemeth |December 27, 2020 |Fortune 
The policy enraged families, home administrators, epidemiologists and politicians of both major parties. Not Mentioned in Cuomo’s Coronavirus Book: How Many Nursing Home Residents Died in New York |by Joe Sexton |October 23, 2020 |ProPublica 
It also would enrage those who have made Pelosi an object of derision and hate. Nancy Pelosi Becoming House Speaker Again Would Be Sweet Justice |Robert Shrum |April 27, 2013 |DAILY BEAST 
He approached ahead of her, barking furiously, and I put down my hand for him to sniff, which seemed to enrage him further. Working in The Royal Archives and Dreaming Up a Novel |Tom Sykes |October 16, 2012 |DAILY BEAST 
Blasphemy remains interesting only so long as it retains the power to enrage. The Cost of Cartooning |David Frum |September 19, 2012 |DAILY BEAST 
Attempting to reason with enraged people may only enrage them more. The Answer is Romneycare |David Frum |August 3, 2012 |DAILY BEAST 
In his new film, Savages, pot growers who enrage a drug cartel, are the protagonists. Oliver Stone: Seven Drug Movies |Oliver Stone |July 17, 2012 |DAILY BEAST 
The explanation of the aged wanderer that the dust and particles came from many sources, seemed to enrage them further. David Lannarck, Midget |George S. Harney 
What the boy had done to thus enrage the animal no one seemed to know. The Pony Rider Boys in Texas |Frank Gee Patchin 
The whole table would take it up, every man doing his best to insult and enrage the victim. The Iron Puddler |James J. Davis 
Don't you, Sir, put questions to me that you know I will answer truly, though my answer were ever so much to enrage you. Clarissa, Volume 6 (of 9) |Samuel Richardson 
Why, I have still the key that you gave me, and which I hung round my neck to enrage your gentlemen, and with this I entered. The Forty-Five Guardsmen |Alexandre Dumas