Keep moving the paper towels to absorb the stain on each area. Hints From Heloise: Our children are worried |Heloise Heloise |February 8, 2021 |Washington Post 
If the kids still don’t get on board, she suggests putting a basket where they toss the towels, or even establishing consequences or rewards. 5 simple ways to get your home in order, from ‘Clean Mama’ Becky Rapinchuk |Lindsey Roberts |February 4, 2021 |Washington Post 
I actually threw in the towel in November, ordering and paying for my bike then. Is Peloton overvalued? |Alan Murray |February 4, 2021 |Fortune 
Next time I go to clean, I take the paper towels off and just wipe off. Hints From Heloise: Before buying a diamond, learn about the four C’s |Heloise Heloise |February 4, 2021 |Washington Post 
Thoroughly pat the meat dry with paper towels and generously season it with salt and pepper. Find out why Anthony Bourdain’s boeuf bourguignon is one of our most popular recipes ever |Becky Krystal |January 20, 2021 |Washington Post 
Using a heatproof slotted spoon, remove the shallots to a paper towel-lined plate. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 
Allow beans to cool completely then remove to a paper towel-lined plate to dry. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 
“They would bend my head back, put a towel over my face and pour water over the towel,” Harrison was quoted as saying. The Luxury Homes That Torture and Your Tax Dollars Built |Michael Daly |December 12, 2014 |DAILY BEAST 
Place the stack of phyllo dough sheets on a cutting board and cover it with a slightly damp towel. The Barefoot Contessa’s Tasty Trip to Paris |Ina Garten |November 27, 2014 |DAILY BEAST 
Inside the bag, a second body was naked, wrapped in a towel, and almost decapitated. Hong Kong’s High-Flying British Psycho Killer Suspect |Nico Hines, Tom Sykes |November 3, 2014 |DAILY BEAST 
"Come and get a clean towel," said Jess, who noticed that Benny was already "picking into" his own mouth. The Box-Car Children |Gertrude Chandler Warner 
The berries were so thick she did not have to change her position before the towel held over a quart. The Box-Car Children |Gertrude Chandler Warner 
She watched the two children a moment as they dropped handfuls of the bluish globes on the towel. The Box-Car Children |Gertrude Chandler Warner 
It was reserved for me to open the towel, which I did with no little pride at having the only plum pudding in camp. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie 
I had buttered the towel so that it should not stick to it; it did not, but it did not stick together either. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie