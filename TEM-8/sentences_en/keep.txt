Domestically, the prime minister maintains the dubious line that he is the only man who can keep the still-fragile peace. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 
“Someone is determined to keep Bill Cosby off TV,” she continued. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 
I think if you keep trying to do things the same way it becomes diminishing returns. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 
Ney said McDonnell needs to “keep a stiff lip” and stay in close contact with family members. Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 
I keep meeting more and more people where that seems to be the case. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 
She was growing accustomed to like shocks, but she could not keep the mounting color back from her cheeks. The Awakening and Selected Short Stories |Kate Chopin 
And it was no light task, then, for six hundred men to keep the peace on a thousand miles of frontier. Raw Gold |Bertrand W. Sinclair 
He will keep the sayings of renowned men, and will enter withal into the subtilties of parables. The Bible, Douay-Rheims Version |Various 
Keep closely covered with a bell glass and, in a few weeks, more or less, the baby Ferns will start to put in an appearance. How to Know the Ferns |S. Leonard Bastin 
My thought was to keep pushing in troops from "W" Beach until the enemy had fallen back to save themselves from being cut off. Gallipoli Diary, Volume I |Ian Hamilton