By the end, characters are either Team Anna, or they’re painted as shrews and simpletons. Inventing Anna’s lazy girlboss revenge fantasy |Alex Abad-Santos |February 28, 2022 |Vox 
Unless we are simpletons, we are all to some extent paradoxes, and Lopez was both an indefatigable traveler and, as writers must be, a monastic homebody. Remembering My Friend Barry Lopez |Bob Shacochis |January 5, 2021 |Outside Online 
To put it charitably, this is the jurisprudence of the simpleton. Who to Blame for Donald Sterling and Cliven Bundy |Robert Shrum |April 30, 2014 |DAILY BEAST 
George Lindsay played the lovable simpleton Goober Pyle on ‘The Andy Griffith Show.’ George Lindsay Dies: The Best of Goober Pyle (Video) |Brittany Jones-Cooper |May 7, 2012 |DAILY BEAST 
There's some vile plot laid against Howard, but if he doesn't come clean out of it with flying colors, call me a simpleton. St. Nicholas, Vol. 5, No. 5, March, 1878 |Various 
Then he pinched Betsy's ear in his usual familiar fashion, saying as he did so, "Saucy simpleton!" Napoleon's Young Neighbor |Helen Leah Reed 
Don't talk to me as if I were a simpleton—with your own false simplifications! The Tragic Muse |Henry James 
And you ought to have kept me from making such a simpleton of myself, Edward. The Sleeping Car |William D. Howells 
The credulous simpleton already saw himself beheaded and wept in anticipation over the fate of his family. The Reign of Greed |Jose Rizal