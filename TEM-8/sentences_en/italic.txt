Emotions cause the brain to store memory in a stronger and more permanent fashion, set down in bold type and italics. You can’t completely trust your memories |David Linden |September 30, 2020 |Popular-Science 
Note that text format (bold or italic) has semantic meaning in this volume. Selections from Early Middle English 1130-1250: Part II: Notes |Various 
Still another abhors dashes or colons, or quotation marks, and yet another will not have Italic type used in his work. The Building of a Book |Various 
Pervenerunt ad nos propositiones qudam Italic satis Lutheran. History of the Great Reformation, Volume IV |J. H. Merle D'Aubign 
Quirinus, kwi-rī′nus, n. an Italic divinity identified with the deified Romulus. Chambers's Twentieth Century Dictionary (part 3 of 4: N-R) |Various 
At the north pole is an hour circle bearing the inscription “Index Hor: Italic.” Terrestrial and Celestial Globes Vol II |Edward Luther Stevenson