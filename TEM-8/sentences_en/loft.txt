Look for a pillow with a low loft, which puts the top closer to the mattress, and is filled with soft support from down or feathers. The best pillow: Sleep better in any position |Carsen Joenk |December 11, 2020 |Popular-Science 
If you’re one of the lucky few who naturally sleep this way, look for a pillow with medium firmness and loft to elevate the neck slightly. The best pillow: Sleep better in any position |Carsen Joenk |December 11, 2020 |Popular-Science 
The actual loft of the face changes as you go from the top to the bottom of the club, which helps negate the ill-effects of hitting the ball too early or late in your stroke. Cobra 3D-printed its limited-edition putter with stainless steel |Stan Horaczek |December 1, 2020 |Popular-Science 
The neighborhood boasts plenty of lofts, cafes, and trendy shops and restaurants. Des Moines : Come and See All that The Capital City has to Offer! |LGBTQ-Editor |November 22, 2020 |No Straight News 
Electric forces also loft dust into Earth’s atmosphere, Grossman notes. Readers ask about Mars dust storms, Fermi bubbles and more |Science News Staff |August 23, 2020 |Science News 
South Korean activists are already planning to loft them over the Demilitarized Zone in balloons. U.S. Should Make North Korea Pay for Sony Hack |Gordon G. Chang |December 18, 2014 |DAILY BEAST 
In testimony, witnesses described a lifestyle that included “a downtown loft and several luxury cars.” The Navy SEAL Who Swindled His Brothers |Jacob Siegel |September 12, 2014 |DAILY BEAST 
We kept going up until we found ourselves in a vast Sharkarama, a huge loft with fake sharks hung from hooks everywhere. My Time on the Set of 'Jaws,' or How to Get a Photo of a Frickin' Mechanical Shark |Tom Shales |August 17, 2014 |DAILY BEAST 
A few weeks ago I was invited to a Soho loft for a board game day. All the Grown-Up Hipsters Playing Kids’ Games |Daniel Genis |June 29, 2014 |DAILY BEAST 
The Arsenal has been converted to “Manhattan style, loft apartments,” the vast majority still unsold. Welcome to Woolwich, Where English Terrorists Say Sorry While They Murder |Peter Pomerantsev |May 23, 2013 |DAILY BEAST 
She should not show panic because of the mysterious noise in the loft of the abandoned Carter house. The Campfire Girls of Roselawn |Margaret Penrose 
Frank climbs up the tall ladder to the loft, which is the second story of the barn, and throws down the hay. Seven O'Clock Stories |Robert Gordon Anderson 
I sawed off the upper four feet carefully, and stowed the remainder back in the loft. The Idyl of Twin Fires |Walter Prichard Eaton 
I had rather sleep upon a pallet in a loft, by myself, than in the handsomest room in the house, with her for a room-mate. Alone |Marion Harland 
So alarmed were we for our safety that we crossed that night into a third valley and slept in the loft of a horse-barn. Famous Adventures And Prison Escapes of the Civil War |Various