She gave him the what-for pretty regularly and he would give her that condescending dad eye. Can ‘Great British Bake Off’ Be Saved From Itself? |Madeleine Davies |November 18, 2021 |Eater 
Steward says what persuaded him to get the shot was clear facts presented in a way that wasn’t condescending. How to talk to vaccine-hesitant people |Tanya Basu |August 16, 2021 |MIT Technology Review 
European leaders even more obviously condescend to him and Angela Merkel is not longer holding back about settlements. What Went Wrong For Netanyahu |Bernard Avishai |January 21, 2013 |DAILY BEAST 
"I wouldn't condescend to be mean, Laura," put in Lady Margaret, whilst the dowager fanned her hot face. Elster's Folly |Mrs. Henry Wood 
“If you will condescend to explain the frying-pan I may perhaps relieve you from the fire,” said Selby with emphasis. Hunted and Harried |R.M. Ballantyne 
It will be quite all right for you to offer me a cup of tea, if your kitchen mechanic will condescend. The Hidden Places |Bertrand W. Sinclair 
It went perforce to the ragman, if he would condescend to accept it. South American Fights and Fighters |Cyrus Townsend Brady 
They might condescend to drop me a courtesy, and then—anarchy, as before. My New Curate |P.A. Sheehan