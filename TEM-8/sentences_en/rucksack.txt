Meathead nodded down the hallway where two olive drab duffel bags and a rucksack sat next to the front door. Short Stories from The Daily Beast: Four Hundred Grand |Elliot Ackerman |July 6, 2014 |DAILY BEAST 
But instead of getting my rucksack, he stepped out of the car and made a call. I Got Arrested by the Secret Police |Khalid Ali |September 17, 2011 |DAILY BEAST 
My father carried a rucksack, a Tommy gun, lots of spare rounds of ammunition, various grenades and a collapsible bicycle. My Father, The Inglourious Basterd |Kim Masters |August 9, 2009 |DAILY BEAST 
Jimmy picked up the bag and fastened the deerskin straps, by which it hung from his shoulders like a rucksack. Northwest! |Harold Bindloss 
We thought of—I thought of having lunch in the hotel, but, of course, you can have my rucksack to carry yours in. Once a Week |Alan Alexander Milne 
He was busy tying a large rucksack of lunch on to himself, and was in no mood for Samuel's ball-room chatter. Once a Week |Alan Alexander Milne 
He carried two English ration bags, besides his own rucksack, and they were all filled to bursting with loot. War and the Weird |Forbes Phillips 
There they were, coming down the passage from a side door—she in front with her alpenstock and rucksack—smiling. The Dark Flower |John Galsworthy