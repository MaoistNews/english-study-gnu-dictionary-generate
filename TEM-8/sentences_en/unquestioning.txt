And so, consequently, the release may damage the unquestioning acceptance that an army at war must have to survive in battle. An Active Duty Soldier on the Lessons of the WikiLeaks Documents |Anonymous |July 27, 2010 |DAILY BEAST 
They do not inspire us into unquestioning loyalty; they sadden us and provoke the unanswerable existential question: Why? A Very Good War |Bob Kerrey |March 10, 2010 |DAILY BEAST 
She has stuck to the things in which she believes: doing her duty, the tenets of her unquestioning Anglican faith—and her dogs. To the Queen, on Her 83rd Birthday |Robert Lacey |April 21, 2009 |DAILY BEAST 
In her presence, the quiet, unquestioning devotion of her personality roused him sometimes to moments of vain regret. The Woman Gives |Owen Johnson 
I do not think we have this strongly disciplined sense of duty nor this unquestioning acceptance of sacrifice. The Wasted Generation |Owen Johnson 
He did nothing by halves; nothing without unquestioning confidence and prodigality. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 
For a length of time the temper of our ancient histories was one of unquestioning reception. Studies on Homer and the Homeric Age, Vol. 1 of 3 |W. E. Gladstone 
Her blue eyes were filled with the sweetness of a loving and unquestioning nature. The Camp Fire Girls at the End of the Trail |Margaret Vandercook