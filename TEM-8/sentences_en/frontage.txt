The 5.7-acre property has 400 linear feet of ocean frontage. Celine Dion Selling $100 Million Worth of Mansions | |August 26, 2013 |DAILY BEAST 
The property includes 4,000 feet of ocean frontage and a 12-bedroom Victorian home. $100 Million House Listings Return with a Vengeance |CNBC |July 16, 2013 |DAILY BEAST 
It has a frontage of one hundred feet on the Via Appia, and an extension in agro of two hundred and thirty feet. The Catacombs of Rome |William Henry Withrow 
It has a frontage of seventy-four feet on Main street, and is sixty-five feet deep. The Bay State Monthly, Vol. II, No. 6, March, 1885 |Various 
It had a high wall frontage of about three hundred and fifty feet. The Daffodil Mystery |Edgar Wallace 
A hole was dug in the ground, with a frontage toward the wind. The Banner Boy Scouts on a Tour |George A. Warren 
It is a long log and frame building, situate on the south side of the road, with a porch extending along its entire frontage. The Old Pike |Thomas B. Searight