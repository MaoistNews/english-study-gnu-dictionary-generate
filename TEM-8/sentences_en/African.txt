Paperback publishers distributed their titles in African-American neighborhoods because it expanded their market base. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 
Ed Brooke, the first African-American Senator since Reconstruction, embraced fights with the left and right. Ed Brooke: The Senate's Civil Rights Pioneer and Prophet of a Post-Racial America |John Avlon |January 4, 2015 |DAILY BEAST 
Still, for all of this, South Carolina is now represented in the U.S. Senate by Tim Scott, a Republican and an African-American. Steve Scalise Shows There’s a Fine Line Between Confederate & Southern |Lloyd Green |January 2, 2015 |DAILY BEAST 
Finally, we have a major film on civil rights in which African Americans are the heroes in their own story. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 
Among African Americans, the likes of Al Sharpton, Jesse Jackson, or Jeremiah Wright cannot do it. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 
I looked up at sound of a startled exclamation, and beheld the round African physog of Lyn Rowan's colored mammy. Raw Gold |Bertrand W. Sinclair 
Mr. Ernescliffe manages him very well—used to illness on that African coast, and the doctor is very fond of him. The Daisy Chain |Charlotte Yonge 
South African railways enjoy one great advantage—cheap coal for their engines. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 
The celebrated African farmer of Cedar creek, Del., died, almost 118 years of age. The Every Day Book of History and Chronology |Joel Munsell 
She spoke in a strange language, no doubt some African tongue, but one which Rita understood perfectly. Dope |Sax Rohmer