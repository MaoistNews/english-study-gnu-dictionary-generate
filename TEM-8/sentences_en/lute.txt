However, DHS has not had a permanent deputy secretary in place since the spring, when Jane Holl Lute resigned. Exclusive: White House Struggles to Replace Janet Napolitano at DHS |Daniel Klaidman |September 25, 2013 |DAILY BEAST 
Carper described Allen as a “very fine person” and said either Lute or Allen would be fine. Lute, Allen Top Congressional Wish List for DHS |Josh Rogin |July 16, 2013 |DAILY BEAST 
While several names have been floated for her replacement, on Capitol Hill, Lute and Allen have the most senior level support. Lute, Allen Top Congressional Wish List for DHS |Josh Rogin |July 16, 2013 |DAILY BEAST 
Carper said Lute would get through the Senate quickly and easily. Lute, Allen Top Congressional Wish List for DHS |Josh Rogin |July 16, 2013 |DAILY BEAST 
“I think Jane Holl Lute would be a terrific candidate, and I urge the president to consider her,” Carper said. Lute, Allen Top Congressional Wish List for DHS |Josh Rogin |July 16, 2013 |DAILY BEAST 
That done I went over the water and walked over the fields to Southwark, and so home and to my lute. Diary of Samuel Pepys, Complete |Samuel Pepys 
Here every breath was eloquence, every word a poem, and the voice of Mary sweeter than Musa's lute. God Wills It! |William Stearns Davis 
Up by five o'clock, and while my man Will was getting himself ready to come up to me I took and played upon my lute a little. Diary of Samuel Pepys, Complete |Samuel Pepys 
Parson Lute softly entered from the kitchen, wiping the rain from his face and hands, stepping on tiptoe over the bare floor. The Cruise of the Shining Light |Norman Duncan 
And I said: What do I care for a kingdom in comparison with my lute? The Substance of a Dream |F. W. Bain