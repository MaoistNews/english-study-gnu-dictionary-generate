More than half a million Instacart shoppers have been mobilized to act as human barriers between wealthy suburbanites and deadly disease. The COVID-19 Pandemic Has Been Tough on Shopping Malls. History Suggests We Should Be Wary of What Might Replace Them |Sam Wetherell |October 28, 2020 |Time 
I expect he was hoping I'd be shocked, because I am only a suburbanite. I'm Not The Sort of Man Who Goes To Prostitutes |Louis Bernières |October 18, 2008 |DAILY BEAST 
A kind-hearted Suburbanite happened to be passing along on his Way to the 5:42 Train. Fables in Slang |George Ade 
The railway, the trolley, the automobile and the top buggy have transformed him into a suburbanite. The Romance of the Reaper |Herbert Newton Casson 
There are certain little routine joys known only to the servantless suburbanite. Shandygaff |Christopher Morley 
There is much of man's life in the figure of the suburbanite standing absorbed in his own thoughts in the midst of his radishes. Marching Men |Sherwood Anderson 
By 9.30 every student would be in his chair, which he had dragged as near to the piano as the early suburbanite would let him. Edward MacDowell |Lawrence Gilman