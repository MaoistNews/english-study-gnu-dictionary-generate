馬加里安認為這個術語仍然有用，但在這個詞出現的時候，大多數新玩家都是美國人。 杜松子酒已經走向了全球，其吸引人的新風格和口味延伸了它的定義。 Carrie Allan | 2021年9月27日|華盛頓郵報 
 數字人群的詞彙中充滿了異想天開的新詞，其中許多已經轉移到更廣泛的全國對話中。 從“科學家”到“午餐肉”，英語單詞令人驚訝的有趣起源|拉爾夫·凱斯| 2021年4月1日|時間 
 然而，嚴肅地說，這個詞成了現代最成功的新詞之一。 從“科學家”到“午餐肉”，英語單詞令人驚訝的有趣起源|拉爾夫·凱斯| 2021年4月1日|時間 
 這是我在發展整個社會和它的經濟以及它所使用的貨幣和錢幣學。 第九作者Tamsyn Muir如何搞怪太空歌劇|Constance Grady | 2021年2月5日|Vox 
 現在，十多年過去了，這個新詞又獲得了新的、合法的分量。 立法者瞄準潛伏的數字“黑暗模式” 
 早些時候，我把這種學說稱為“信心仙女”的信仰，這個詞似乎一直流傳下來。 保羅·克魯格曼:緊縮政策大錯特錯! 保羅·克魯格曼，2012年5月6日，每日野獸 
 還記得他在去年6月開始的辯論中使用的新詞“奧巴馬醫改”嗎? 邁克爾·托馬斯基關於裡克·桑托勒姆如何在羅姆尼醫改問題上釘住米特|邁克爾·托馬斯基| 2012年1月29日|每日野獸 
 《TLS》的南希·坎貝爾抱怨說，“邂逅”是出版商為偶爾的作品創造的酷詞。 英國最好的文學|Peter Stothard | 2010年1月31日|每日野獸 
 但我覺得，如果我接受這個獎項，最終會降低獎章的鑄造價值。 克里斯托弗·巴克利| 2009年10月10日|每日野獸 
 現在的銅幣是1860年12月1日開始使用的，從那時起希頓先生已經簽了好幾份合同。 《肖威爾伯明翰詞典》|托馬斯·t·哈曼和沃爾特·肖威爾 
 最後一個名字的引擎是為造幣操作在利馬造幣廠。 理查德·特里維西克的生平，卷二(2)|弗朗西斯·特里維西克 
 這件事是公開進行的，錢幣一有機會就歸還了。 秘密社團和顛覆運動|內斯塔·h·韋伯斯特 
 如果他離開了，人們可能會以為他在看藍皮書，或者在計算鑄幣，或者在準備演講。 首相安東尼·特羅洛普 
 瑞士的軍隊、郵政系統和財政都置於聯邦政府的控制之下，並建立了國家貨幣。 19世紀的歷史，年復一年|埃德溫·愛默生