她問我有幾個孩子，我面無表情地告訴她我沒有。 媽媽最知道:大流行的痛苦和希望|Kate Bartlett | 2021年5月10日|Ozy 
 所以它基本上把它看作整個東西，就是一個眼皮。 播客:《旁觀者的AI》安東尼·格林|，2021年4月1日，《麻省理工科技評論》| 
 就像在未來，你可以想象你可以自動地在你的眼瞼上塗抹眼影，只需要通過檢測面部，就可以有一個物體來塗抹它。 播客:顧客們注意了，我們正在跟蹤你，泰特·瑞安·莫斯利，2020年12月21日，麻省理工科技評論 
 我的眼皮耷拉了下來，我想睜開眼睛，沿著熟悉而曲折的道路前行。 我開車撞了一個騎自行車的人布魯克·沃倫，2020年10月28日，戶外在線 
 一小部分患者可能出現眉毛或眼瞼下垂，但這通常在幾周內就會解決。 肉毒桿菌會太多嗎? |作者:Matthew J. Lin/The Conversation | 2020年10月1日|大眾科學 
 近到可以看到他眼皮上那個看起來像胎記的小傷疤。 堆疊:穆罕默德·阿里真正的偉大|彼得·里奇蒙| 2014年2月23日|每日野獸 
 這款售價16美元的塑料鏡框似乎能推入眼瞼，將其與眼瞼分開。 DIY整形手術:你能不動刀就改變你的臉嗎? Nina Strochlic | 2014年1月6日 
 因此，陳尋求雙眼皮手術，也被稱為東亞眼瞼成形術。 改變西方美的理想:妮娜·達烏魯裡和朱莉·陳索拉雅·羅伯茨2013年9月25日 
 我們在任何地方都能找到沃爾多，就像在指甲下或我們的眼皮裡。 嘿，吸大麻的人，你的孩子知道你吸毒了傑森·古德| 2013年5月1日|每日野獸 
 然而，在一場車禍後，她的職業生涯被擱置了下來，在這場車禍中，她的左眼瞼脫落了。 《Vogue》創意總監格蕾絲·柯丁頓的回憶錄沒有什麼啟示|羅賓·吉夫漢| 2012年11月20日|每日野獸 
 愛會隨著一個尖下巴的呼喚，一個聲音的音調，或一個眼皮的下垂而飛奔而來。 幽居病| B。 m·鮑爾 
 即使隱藏在眼瞼後面，它們也能被找到，就像一個嬰兒的情況一樣。 戴兜帽的偵探，第三卷第二冊，1942年1月|各種 
 大衛嚇壞了，掀開年輕人半閉著的眼皮。他的眼睛呆滯、呆滯、呆滯。 《七宗罪:嫉妒和懶惰》歐熱尼·蘇 
 高燒不退; 生與死的問題似乎就懸在眼皮的顫動上。 愛的朝聖|厄普頓辛克萊 
 嘴唇是蒼白的，眼瞼的紅色，從下眼瞼向下看，也會呈現出類似的外觀。 英國幽默史|C。 哈里森