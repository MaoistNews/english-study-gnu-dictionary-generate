她是電子音樂的一位默默無聞的先驅，在默默無聞幾十年後才被承認她的貢獻，她相信思維的力量可以獲取遙遠的過去的記憶和不可知的未來的願景。 波琳·安娜·斯特羅姆的遺作鞏固了她作為電子音樂夢想家的遺產喬納森·威利格2021年2月19日華盛頓郵報 
 Struum正確地指出了現代流媒體領域的一個問題，即大量未開發的內容現在分佈在數百個較小的服務中，其中大部分都處於默默無聞的狀態。 迪士尼和探索頻道的前高管將推出“流媒體服務的ClassPass”Struum, 2021年1月7日 
 也許大多數無名辯護律師將繼續默默無聞地辛苦工作。 海豚隊用他們的無名防守為自己贏得了名聲 
 在許多方面，基諾沙成功地做到了這一點——使它成為一個相對成功的故事，在這個國家，許多以前依賴汽車的中西部經濟體已經變得默默無聞。 基諾沙不同尋常的經濟演變使其成為完美的政治引爆點 
 看似勢不可擋的網站逐漸消失，取而代之的是其他網站。 TikTok讓他成名。 現在他正在想象一個沒有它的世界，Abby Ohlheiser, 2020年8月14日，MIT科技評論 
 一些最受同時代人尊敬的作家如今已處於相對默默無聞的狀態。 小說的誕生|尼克·羅密歐| 2014年11月27日|每日野獸 
 通常情況下，候選人有多年時間在相對低調的情況下制定出他們的全球議程。 蘭德·保羅通過馬拉拉抨擊奧巴馬|奧利維亞·努茲| 2014年10月23日|每日野獸 
 相反，它們最多隻能成為我們這個時代的一個陳腐而苦澀的點睛之筆，然後逐漸消失，不再受人喜愛，變得默默無聞。 美國堅果簡史; 從喬治·華盛頓到伍德斯托克|約翰·阿夫倫| 2014年8月17日|每日野獸 
 《孟子白蟲子》的寫作速度變得很慢，2013年又回到了相對默默無聞的狀態。 佔領王座:賈斯汀·唐尼，新反動派，和新1%的人Arthur Chu | 2014年8月1日|每日野獸 
 然後他們失敗了，回到了默默無聞的狀態，在州或地方政府任職。 參議員競選失敗只是喬希·曼德爾壞消息的開始|本·雅各布斯| 2014年6月10日 
 的確，他的視力已經習慣了黑暗，因為他現在可以更清楚地看到男爵的容貌了。 約瑟夫·謝里丹·勒法努將軍 
 到那日，聾子必聽見這書上的話。瞎子的眼，必從黑暗黑暗中看見。 《聖經》，Douay-Rheims版本| 
 因為房屋被離棄，城中的群眾被撇下，黑暗和幽暗來到城內的坑洞，直到永遠。 《聖經》，Douay-Rheims版本| 
 在大眾的吹捧下，他被捧上了天，但從那以後，他就因為歷史上的蔑視而陷入了默默無聞的境地。 布萊克伍德愛丁堡雜誌，No。 CCCXXXIX。 1844年1月。 卷,LV。 |各種 
 令人尊敬的默默無聞一直是戈達明的顯著特徵。 朴茨茅斯路及其支流|查爾斯·g·哈珀