然而，把響亮和緩慢放在一起，結果往往會在我們的腦海中感到沉重和過度，即使我們只是在體驗無重力的空氣顫動。 Eyehategod的高重力金屬可以連接你到天堂|克里斯·理查茲| 2021年3月12日|華盛頓郵報 
 在大都會博物館的舞臺上可能會有幽靈在飛舞。 《當斯大林遇見麥克白夫人》|Brian Moynahan | 2014年11月9日|每日野獸 
 法羅微笑著，蝴蝶飛舞，星星劃過夜空。 不要違抗羅南·法羅的力量撅嘴|蒂姆·蒂曼| 2014年2月25日|每日野獸 
 事情一開始很小:這裡有一點煩惱，那裡有一點懷疑。 快殺了貝茨先生! 如何拯救《唐頓莊園》|Andrew Romano | 2014年2月20日|DAILY BEAST 
 那裡沒有紅地毯，你只會在看電影時飄飄然。 凱瑞·穆ligan，《醉夢民謠》主演，在科恩兄弟、超級男孩粉絲圈、洛德和更多|Marlow Stern | 2013年12月3日|DAILY BEAST 
 突然間，她走了，留下一陣紅窗簾的飄動。 尼泊爾新舊:加德滿都谷地的皇家城市煥然一新|Condé納斯特旅行者| 2013年8月19日|每日野獸 
 它們通常在任何物體的最高點擺動兩到三分鐘，然後消失。 一個女人的環球旅行|艾達·菲佛 
 “哦，我已經把它拿出來了，在它後面摸了摸，”卡靈頓小姐催促道，這時她心裡一陣慌亂。 舞臺上的中央高中女孩|格特魯德·w·莫里森 
 她的聲音中有一種絕對的真誠，夾雜著恐懼，他張開雙臂，讓她飛走了。 輪子上的蝴蝶|西里爾·阿瑟·愛德華遊騎兵海鷗 
 這就是為什麼它們有黑色的翅膀和尾巴，為什麼它們如此高興地撲騰，為什麼它們永遠唱不完它們的歌。 易洛魁人給孩子講的故事|梅布爾·鮑爾斯 
 他幾乎以為一張支票會飄飄蕩蕩地掉在地上; 但是，可惜的是，沒有一點動靜。 愛的朝聖|厄普頓辛克萊