所以，在戰爭中，這類可怕的事情偶爾會發生，但它們仍然是值得譴責的。 科林·鮑威爾，第一次海灣戰爭的勝利者，第二次海灣戰爭的失敗者，於2021年10月18日，埃莉諾·克里夫特去世，享年84歲 
 我聯繫了幾位消費者行為專家，希望他們能從心理上了解MyPillow的所有者，他們對林德爾的信息感到遺憾，但卻全身心地投入到他的產品中去——或者是那些成為抵制目標的產品的粉絲。 抵制MyPillow:一款產品如何引發身份危機|Elizabeth Chang | 2021年2月12日|華盛頓郵報 
 金指出了人們，包括那些可能譴責南方不公正的人，是如何維持種族現狀的。 小馬丁·路德·金對自由主義盟友的挑戰——以及為什麼它今天能引起共鳴Jeanne Theoharis | 2021年2月8日|華盛頓郵報 
 一方面，你會感到遺憾，因為他們已經成功地入侵了我們的信息架構。 前頂級間諜坦白一切|尼克·福裡埃佐斯| 2021年1月19日|Ozy 
 在拒絕獲獎的信中，她譴責“種族和經濟不公正的影響日益殘酷”。 瞭解酷兒文學偶像Adrienne Rich |Kathi Wolfe | 2020年12月11日|Washington Blade 
 我對似乎以價格標籤和大量土地掠奪席捲約旦河西岸的無法無天感到遺憾。 奧巴馬:來看看定居點|Samuel Lebens | 2013年3月21日|DAILY BEAST 
 他們將看到我們經常譴責的僅僅是香腸製作，他們會喜歡的。 一位內戰教授評論《林肯》|大衛·弗拉姆| 2012年11月27日|每日野獸 
 它的意思很快就變成了，以一種特別充滿道德負擔的方式哀嘆或反對。 David Frum | 2012年10月19日|DAILY BEAST 
 我要引用一些非常可悲的東西，但不是為了可悲。 為什麼米歇爾·馬爾金討厭她一半的國家? 大衛·弗拉姆2012年9月21日每日野獸 
 奧巴馬總統可能會對這種趨勢感到遺憾，但他似乎對如何改變這種趨勢沒有非常明確的想法。 2012年8月27日，David Frum，每日野獸 
 這也許是對的，但從某種意義上說，他並沒有感到痛心。 《悲劇的繆斯》亨利·詹姆斯 
 否則，他只會成為一個母親的悲哀，一個舅舅的喜悅。 華盛頓歐文|亨利w博因頓 
 如果它有幽默感，就抱怨它缺乏深思熟慮; 如果它是嚴肅的，吹毛求疵它的缺乏歡樂。 文學如何失敗|安德魯·朗 
 我們在這個世界上遇到的許多人並不完全為之而活，他們也沒有能力做出你所痛惜的行為。 蒙克頓家族:小說，第一卷 
 親愛的，聽到你為失去這樣的特權而哀嘆，我很高興，因為這證明你珍視它。 《羊圈與公地》第二卷(2卷)|蒂莫西·伊斯特