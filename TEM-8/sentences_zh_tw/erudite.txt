我們一小群人跟著博學而友好的海草專家斯賓塞·馬利走在一條小路上，爬過岩石，來到一個偏遠的海灘，在那裡可以發現不同種類的海草。 在加州的中央海岸，低調探索酒莊、餐廳和自然|特雷西·庫維爾| 2021年10月7日|華盛頓郵報 
 不過，就目前而言，這個聽起來博學的交互式數字愛因斯坦聊天機器人仍然有足夠的滯後，可以洩露遊戲的秘密。 人工智能驅動的音頻克隆初創公司為愛因斯坦聊天機器人|提供聲音，娜塔莎·洛瑪斯，2021年4月16日 
 相比之下，認真創造的術語可能太過古板，太過博學，太過專注於讓創造者看起來聰明。 從“科學家”到“午餐肉”，英語單詞令人驚訝的有趣起源|拉爾夫·凱斯| 2021年4月1日|時間 
 別忘了，儘管他博學可愛，但他只是個市長。 性感的交通部布洛克·湯普森，2020年12月24日，華盛頓刀鋒 
 故事以一種生動的、有見地的風格講述，沒有寫出來的音樂例子，但貫穿始終的是對作曲家作品的博學和熱情的討論。 莫扎特令人驚歎的才華和持久的快樂蒂姆·佩奇| 2020年12月18日|華盛頓郵報 
 在一個被公眾認為是怪人的身體裡，有一個溫柔博學的靈魂，這是《象人》的核心對比。 “象人”的真實故事|Russell Saunders | 2014年11月3日|DAILY BEAST 
 帕特里夏·克拉克森(Patricia Clarkson)在這兩個角色上都有所表現，她扮演的是一個對博學的怪物著迷的女人。 秋季百老匯預告:《這是我們的青春》，布萊德利·庫珀飾演“象人”，以及更多|珍妮絲·卡普蘭| 2014年9月11日|每日野獸 
 帶著一個既博學又無畏的計劃，伯格投入到這個項目中，迎接每一個挑戰。 《分歧者》不是《飢餓遊戲》，導演尼爾·伯格說 
 博學派正試圖通過邪惡的手段從無私派手中奪取政府的控制權。 獨家:謝琳·伍德利談《分歧者》，J. Law，以及她為什麼拒絕《五十度灰》|Marlow Stern | 2014年3月7日|DAILY BEAST 
 但與布魯姆和伊格爾頓不同的是，他的書雖然博學而精闢，卻毫無顧忌地民粹主義。 約翰·薩瑟蘭的有趣的小文學史|馬爾科姆·福布斯| 2013年11月29日|每日野獸 
 以這種方式準備並最終被大會通過的答覆，比哈欽森先生的發言更長、更有見地。 革命前夕|卡爾·貝克爾 
 然而，在交談中，並不是只有博學的人，也不是隻有富有想象力的人才會被賦予樂趣。 《女士禮儀手冊和禮貌手冊》|弗洛倫斯·哈特利 
 “你為你博學的追求者辯護，我並不感到奇怪，”約瑟芬說，在這個形容詞上加了一個令人討厭的重音。 獨自|馬里昂哈蘭 
 除了枯燥的正確和博學的準確之外，他的努力中還有一些數學的成分。 《現代繪畫史》第一卷(共四卷)|理查德·馬瑟 
 弗朗西斯·帕克曼莊嚴而博學的著作就是一個很好的例子。 美國速寫|查爾斯·惠布利