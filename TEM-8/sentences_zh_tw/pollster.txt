到上個月，這一比例已經下降了近一半，只有18%——這是民意調查機構測量的七個指標中最低的。 殘酷、孤立的一年導致了正義和邪惡之間令人困惑的戰鬥|菲利普·邦伯| 2021年2月4日|華盛頓郵報 
 偏好調查反映了一種典型的政治民意調查，由米勒擔任民意調查者。 喬治亞州決選:一名數據科學家混合使用民調和投注數字，認為民主黨獲勝的幾率為2021年1月5日 
 他們還評估了為什麼許多民調專家不參加喬治亞州參議院決選，並花一些時間回答聽眾的問題。 為什麼許多民調機構不參加喬治亞州決選|Galen Druke | 2020年12月29日|FiveThirtyEight 
 其他民調機構沒有看到這一點，儘管他們都發現大多數共和黨人同意總統。 預告片:為什麼從喬治亞州到法庭，大老黨都在談論中國|大衛·韋格| 2020年12月10日|華盛頓郵報 
 穆雷是蒙茅斯大學民意調查研究所的創始主任，他在中西部和佛羅里達州遇到了許多與其他民意調查機構相同的挑戰。 政治播客:如何讓民調更好|蓋倫·德魯克| 2020年12月4日|FiveThirtyEight 
 “要在這些地方選出一名共和黨人，需要很多民主黨人的支持，”共和黨民調專家約翰·麥克勞克林(John McLaughlin)說。 2014年11月4日，東北共和黨人大衛·弗雷德蘭德迴歸 
 共和黨民調專家尼爾·紐豪斯說:“在這樣的競選中，你擔心的是選民不投票。 在安永&加德納事務所，共和黨人認為他們找到了公式 
 “我會建議任何候選人評估自己的生存能力，而不是隻是象徵性地參加競選，”民主黨民調專家塞琳達·萊克(Celinda Lake)說。 希拉里團隊想挑戰民主黨嗎? David Freedlander 2014年9月25日|DAILY BEAST 
 “堪薩斯州的選民正在尋找兩黨之外的另一種選擇，”Orman民意測驗專家大衛·比蒂說。 能控制參議院的堪薩斯獨立報|約翰·阿夫倫| 2014年9月6日|每日野獸 
 正如民意測驗專家約翰·佐格比所寫的，總統已經因為許多原因疏遠了許多年輕選民。 希拉里有一個千禧一代的問題|尼克·吉萊斯皮| 2014年8月28日|每日野獸