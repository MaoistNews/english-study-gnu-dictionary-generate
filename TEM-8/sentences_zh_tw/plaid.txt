用社交媒體的術語來說，這是一種美學——哥特式建築、佈滿灰塵的圖書館和復古格紋圖案，所有這些都帶有一種邪惡的感覺。 《弗拉基米爾與迷失》中校園的陰暗面|安娜貝爾·古特曼| 2022年1月31日|時間 
 週二晚上，一群西棕櫚灘居民身著粉彩和格子服裝，聚集在一起，為一場席捲他們社區的激烈爭鬥提起訴訟。 佛羅里達州的億萬富翁在西棕櫚灘大出氣，Noah Kirsch, 2021年9月23日，每日野獸 
 你會覺得，沒有什麼比一件格子襯衫或一把條紋椅子更使她高興的了。 愛麗絲·尼爾是20世紀美國最偉大的肖像畫家。 她的工作一直令人驚訝。 塞巴斯蒂安·斯米，2021年3月25日，華盛頓郵報 
 根據他的襪子木偶，我以為他會是一個穿著格子衣服、蓄著大鬍子的魁梧巨人——基本上就是加拿大的保羅·班揚。 加拿大顛覆性的馬甲木偶:襪子愛德不怕說任何話|索拉雅·羅伯茨| 2014年11月13日|每日野獸 
 這裡有年輕的搖滾女郎和搖滾男孩，有穿著格子、牛仔、破洞和彈力開衫的文藝男男女女。 《金髮女郎的崇拜:黛比·哈里非常特別的紐約影展》|蒂姆·蒂曼| 2014年10月1日|每日野獸 
 身穿格子襯衫，這位矮胖的歌手看起來就像一個在酒吧裡打開麥克風的瓦匠。 我不是鄉村音樂也不是流行音樂。 我就是純粹的加斯·布魯克斯。 David Masciotra 2014年9月10日|DAILY BEAST 
 在一輛豪華轎車的後座上，身穿格子西裝的奎斯特舉起酒杯，眨眨眼說:“你要保持優雅，愛荷華。 “7個你無法忽視的WTF競選廣告，從閹人到鱷魚摔跤手|Olivia Nuzzi | 2014年5月19日|DAILY BEAST 
 哈利穿著他的格子襯衫，在自己舒適的空中包廂裡跳舞。 新單身哈里在邁阿密聚會! 湯姆·賽克斯2014年5月1日，每日野獸 
 當他進屋時，他發現原來是他主人的格子斗篷掛在門口。 《動物故事:配圖》弗朗西斯·c·伍德沃思 
 你可能認為自己是“geyan fine”，全身都是蘇格蘭格子呢，但我無論如何也不會這麼“kenspeckle”! 《佩內洛普在蘇格蘭的經歷》|凱特·道格拉斯·維金 
 我們認為她的成功主要來自於她用蘇格蘭格紋鉛筆寫詩。 《佩內洛普在蘇格蘭的經歷》|凱特·道格拉斯·維金 
 他所說的立方體，我從來沒有懷疑過; 果然，那是一本小《聖經》，可以裝在格子襯衫裡。 羅伯特·路易斯·史蒂文森的作品-斯旺斯頓版第10卷(25頁)|羅伯特·路易斯·史蒂文森 
 穿著這件衣服的人，有點像蘇格蘭高地人穿格子呢，把它系在肩膀上，讓手臂自由活動。 在錯誤的天堂|安德魯·朗