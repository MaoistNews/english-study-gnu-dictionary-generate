《馬爾科姆和瑪麗》的前三分之一有一些智慧和神韻，還有風格。 贊達亞在平淡無奇的馬爾科姆和瑪麗|斯蒂芬妮·扎克| 2021年2月5日|時間 
 如果你能忽略作者創造這樣一個敏感而可愛的無賴的動機，你會發現這本小說以巨大的氣魄和洞察力探索了表演的需求和男子氣概的幻想。 伊桑·霍克將他的表演經驗——以及過去的不忠——變成了精彩的小說 
 當然，史蒂文森的小說和故事展現了非凡的氣魄，無論是描繪多方面的人物、豐富的氛圍背景還是政治歷史。 羅伯特·路易斯·史蒂文森的《綁架》不僅僅是一個冒險故事，它是一本關於政治和異見的及時雨小說 
 這是francacorta的另一個例子，它的氣魄和興奮程度接近香檳，而且價格更實惠。 這款售價11美元的波爾多酒乞求披薩或漢堡，會讓你臉上帶著微笑|Dave McIntyre | 2020年12月18日|華盛頓郵報 
 他是一個扭轉局面的藝術家，有著驚人的氣魄，幾乎沒有明顯的恐懼。 埃隆·馬斯克的火箭之旅，安德魯·努斯卡，2020年12月2日 
 但拜恩自己是一個戲仿者，他用他的凹陷的眼睛，冷若冰霜的氣魄指揮著舞臺。 棧:寶琳·凱爾的談話頭的痴迷|寶琳·凱爾| 2014年11月22日|每日野獸 
 溫迪·史密斯寫道，一部新的歷史以敏感和熱情講述了他們不平凡的故事。 無種族的女性:“哈林區的安妮小姐”溫迪·史密斯2013年9月19日 
 與此同時，學生們以前所未有的熱情閱讀這本書。 託尼·莫里森的《寵兒》是如何在學校裡被教授|安娜·克拉克| 2012年10月4日|每日野獸 
 因此，現在的問題不僅是瑞安能否用他的遠見和熱情重振羅姆尼的競選團隊。 保羅·瑞安會幫助羅姆尼贏得藍領白人的支持嗎? 2012年8月27日每日野獸 
 “他應該寫張支票，閉上嘴，”克里斯蒂以他典型的氣場回應道。 斯蒂芬·金:向我徵稅，看在我的份上! Stephen King 2012年4月30日每日野獸 
 這些碎片本身幾乎毫無價值; 拜倫似乎在搬家的過程中失去了他的氣魄。 《英國舞臺》|奧古斯丁·菲隆 
 她在這裡扮演的丘比特是如此的神采飛揚、銳氣十足、厚臉皮、活潑好動，以至於其他丘比特都是為她而創造的。 《英國舞臺》|奧古斯丁·菲隆 
 一部充滿激情和悲情的社會故事則是另一回事，編輯可能會被它說服。 最長的旅程。 m·福斯特 
 喝了一大杯紅酒似乎使他精神煥發，他開始賣弄那股不可抗拒的神韻。 我們自己國家的神話和傳說，完成查爾斯·m·斯金納| 
 她既有南方人的夢幻、慵懶的優雅，又有北方人的神韻和力量。 《利平科特雜誌》，1885年8月