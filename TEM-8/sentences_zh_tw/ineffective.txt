簡單地告訴內容團隊把他們的行為放在一起是一個簡單但無效的決定。 內容營銷失敗:如何分析和改進|Michael Doer | 2020年8月27日|搜索引擎觀察 
 如果一種不安全或無效的疫苗被廣泛使用，這將是一個比康復期血漿被證明無效更大的挫折。 美國剛剛批准使用來自covid-19倖存者的血漿作為治療。安東尼奧·雷加拉多，2020年8月24日，麻省理工科技評論 
 在大流行之初，檢測無法獲得、容易出錯或根本無效。 2020年8月19日，《麻省理工科技評論》 
 機器學習研究人員如果沒有意識到這一點，並期望工具能夠“現成”地工作，往往會創建出無效的模型。 太多的人工智能研究人員認為現實世界的問題與此無關 
 我們可以瞭解到，政府一直在失敗，沒有效率，而不是政府，我們不得不求助於企業。 如何防止另一次大蕭條(第421頁)|Stephen J. Dubner | 2020年6月11日|魔鬼經濟學 
 作為華盛頓的一名律師，他接手了那些似乎對變革免疫的公司，即使它們是無效的。 你當地的學校並不一定要糟糕|邁克爾s羅斯| 2014年12月17日|每日野獸 
 八個月過去了，人們對營救失蹤女學生的努力收效甚微感到憤怒。 博科聖地恐怖的新面孔:少女|Nina Strochlic | 2014年12月13日|DAILY BEAST 
 政策是解決訪問優先於所有權的文化問題的無效工具。 年輕人如何破壞自由|詹姆斯·普洛斯| 2014年10月11日|每日野獸 
 如果你附加了太多的條件，讓任何軍事行動都失效了呢? 共和黨頭號人物:“災難”如果奧巴馬失去ISIS的選票，所以不要去國會|Tim Mak | 2014年9月9日|每日野獸 
 如有必要，他們可以迅速解僱不稱職的教員。 特許學校做對了什麼，為什麼他們在我們的高中排名中名列前茅 
 布魯斯摧毀了城堡; 也許是因為它是舊的無效的類型，或者是因為他沒有辦法持有它。 羅伯特·布魯斯國王| f . Murison 
 就賽克斯和他的同輩來說，你也許會說，決定論的邏輯是健全的，但是無效的:它沒有任何結果。 上帝與我的鄰居羅伯特·布拉奇福德 
 如果轟炸是無效的，無論出於什麼原因，那麼這些人都不應該被允許突破掩體。 加里波利日記，第一卷|伊恩·漢密爾頓 
 雖然雨滴的跳動眾所周知是輕的，但它的擊打併不是無效的。 地球歷史概述|納撒尼爾·索斯蓋特·謝勒 
 培根厭惡無效的邏輯推測，蘇格拉底厭惡無效的物理研究。 歷史的燈塔之光，第一卷|約翰·洛德