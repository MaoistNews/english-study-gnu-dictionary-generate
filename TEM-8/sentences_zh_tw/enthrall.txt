在《侏羅紀公園》和《侏羅紀世界》電影中，活生生的恐龍讓人類遊客著迷。 酷工作:把古生物學帶給人們|貝斯·蓋格| 2022年6月9日|學生科學新聞 
 維克多溫德博物館的珍品，美術和自然歷史是在這裡迷住你與奇怪，不尋常和超凡脫俗。 噓! 韋恩·柯蒂斯| 2021年10月29日|每日野獸 
 不管你喜歡與否，科技總是以改變的承諾吸引著我們。 技術驅動的期貨的危險吸引力|Sheila Jasanoff | 2021年6月30日|MIT技術評論 
 仔細選擇，這個柔軟，李子味的品種可以像精品赤霞珠一樣迷人。 勇敢喝酒的五種方法|馬克·奧德曼| 2010年10月25日|每日野獸 
 但可以肯定的是，《協和航班》在未來幾年將繼續吸引它的粉絲群體。 HBO《遊吟詩人起飛》|傑斯·萊科布| 2009年9月18日|每日野獸 
 我們能不能讓他對我們的書感興趣，讓他讀基辛頓編年史，這樣他就不會再發動戰爭了? 阿比·法威爾·布朗 
 金斯利的《水中的嬰兒》(Water Babies)和其他精選作品全部出現; 這些可以激發想象力，培養品味。 世界及其人民:第七卷|安娜·b·巴德拉姆 
 周圍的群山被十月的枝葉所籠罩，這景象即使是最遲鈍的人也會被迷住。 招聘:丈夫塞繆爾·霍普金斯·亞當斯 
 菲爾斯的海灘會讓我著迷，但也會讓我絕跡——我的意思是，社交的繆斯會讓別人的經典鼻子分離。 亨利·詹姆斯書信(第一卷)|亨利·詹姆斯 
 一個如此強烈地宣稱自己對世界的否定的人，一定感受到了它的吸引力，它的誘惑和蠱惑的力量。 丹麥的讚美詩和讚美詩作者|延斯·克里斯蒂安·阿伯格