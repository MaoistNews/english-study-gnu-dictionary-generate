去年10月推出的在線平臺Sip Wines主要是為了應對疫情，該平臺將酒莊標榜為可持續或有機的、有社會責任感的、由女性、家庭所有和第一代小型企業領導的。 網上葡萄酒銷售持續增長，但它們能否——或者是否應該——取代本地商店? 戴夫·麥金太爾2021年2月26日|華盛頓郵報 
 在無情的大流行中，養老院的工作人員疲憊不堪，壓力重重。從表面上看，10月又是一個平靜的月份。 一個否認的小鎮直面了病毒|Will Englund | 2021年2月26日|華盛頓郵報 
 總部位於西雅圖的威爾肯寧諮詢公司和AAM在10月份進行的一項調查發現，自去年3月封鎖以來，近30%的博物館仍處於關閉狀態。 GoFundMe活動能拯救我們的文化收藏嗎? 拉斯維加斯的彈球名人堂就寄希望於此。 凱特·西爾弗2021年2月26日|華盛頓郵報 
 今年10月，BarFly宣佈被兩家投資公司收購。 2019冠狀病毒病的破產浪潮已經開始了喬納森·奧康奈爾，阿努·納拉亞索米，2021年2月26日，華盛頓郵報 
 PG&E的訴訟是為了回應灣區混凝土公司(Bay Area Concrete)去年10月對該公司提起的違約訴訟。 訴訟揭示了對被指控欺詐的PG&E承包商的新指控，作者是Scott Morris，海灣城市新聞基金會|，2021年2月26日| 
 Grindr在同年10月推出了這一功能，並將其稱為“部落”。 Grindr的變性約會問題|大衛·萊維斯利| 2015年1月9日|每日野獸 
 今年10月，他與弗萊一起前往丹佛，支持他與LGBT權利組織馬修·謝帕德基金會(Matthew Sheppard Foundation)的合作。 來見見史蒂芬·弗萊的未來丈夫(不到他一半的年齡)|湯姆·賽克斯| 2015年1月6日|每日野獸 
 據美聯社報道，截至10月，受該法案影響的在世人士只有4人。 納粹、防曬霜和海鷗蛋:2014年的國會非常高效|本·雅各布斯| 2014年12月29日|每日野獸 
 10月22日洩露給聖路易斯郵報的是什麼? Michael Tomasky的年終小測驗:測試你2014年的新聞知識|Michael Tomasky | 2014年12月26日|DAILY BEAST 
 克里沃夫於2012年10月被捕，罪名是參與“大規模騷亂”。 在2015年|運動中，我們希望看到的11名政治犯被釋放。 Org | 2014年12月25日|DAILY BEAST 
 拿破崙本人於10月2日抵達維爾茨堡，發現他的軍隊集中，但缺乏補給。 拿破崙的那幾個小元帥| R。 p . Dunn-Pattison 
 九月在十月棕色的懷抱中逝去了，最後奈傑爾來了一封信。 貝拉唐娜|羅伯特希肯斯 
 他立即被逮捕，並於10月13日由軍事法庭審判，判處死刑，幾小時後被處決。 拿破崙的那幾個小元帥| R。 p . Dunn-Pattison 
 這是完美的一天; 清澈明亮，醇厚爽脆，色彩豐富，只有在英國十月的一天才會如此。 英格蘭、蘇格蘭和愛爾蘭五十年的鐵路生活|約瑟夫·塔特洛 
 我估計那臺巨大的壓力發動機將在十月中旬之前開始工作。 理查德·特里維西克的生平，卷二(2)|弗朗西斯·特里維西克