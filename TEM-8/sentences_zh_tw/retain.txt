他成功地利用了人們對體制的懷疑，來幫助自己保住權力的野心。 當一個黨派破壞根深蒂固的兩黨制時，會發生什麼? 2021年2月12日|華盛頓郵報 
 你必須記住的一件事是能夠慶祝你和你的成就，而不是與別人的對比。 第九作者Tamsyn Muir如何搞怪太空歌劇|Constance Grady | 2021年2月5日|Vox 
 他在法律上對她的大部分日常生活擁有權威，儘管他在2019年暫時離開了這一角色。 布蘭妮·斯皮爾斯和90年代年輕、女性和名人的創傷 
 這可以讓奶酪保持水分，形成我們非常喜歡的光滑、天鵝絨般的質地。 製作完美的玉米片奶酪的科學建議|Sandra Gutierrez G. | 2021年2月4日|大眾科學 
 研究人員似乎一致認為，隨著兒童記憶能力的增強，當成年人向他們講述事情發生後的經歷時，他們對特定事件的回憶能力就會增強。 我們失去的童年記憶會發生什麼? 母親的身份讓我尋找答案。 米西·瑞恩，2021年2月4日，華盛頓郵報 
 儘管作者們的學術水平很高，但他們還是設法保持了大多數漫畫所具有的那種歡快和樂觀的氣氛。 2014年最好的咖啡桌書|Robert Birnbaum | 2014年12月13日|DAILY BEAST 
 現在，兩家啤酒廠都在努力保住自己的那一半蛋糕。 the House of the Witch: the Renegade Craft Brewers of Panama |Jeff Campagna | 2014年11月30日|DAILY BEAST 
 我們仍然保持11月27日的習慣，通過純粹的暴飲暴食。 火雞殺手的自白|湯姆·賽克斯| 2014年11月26日|每日野獸 
 加拿大和美國是唯一保留出生公民權的工業化國家。 準備好開始聽到關於“對錨寶寶的行政特赦”|埃莉諾·克里夫特| 2014年11月19日|每日野獸 
 不過，羅拉巴克確實保留了這瓶酒，在他華盛頓特區的辦公室裡正式使用。 冥想地毯，劍和馬頭小提琴:給政府權貴最奇怪的禮物|本·雅各布斯| 2014年11月11日|每日野獸 
 國王仍然設法保持他的聲望，並試圖以自己的方式改善他的臣民的命運。 拿破崙的那幾個小元帥| R。 p . Dunn-Pattison 
 後者，在他這邊，急於挽留馬什納的感情，並立即提出讓他指揮意大利軍隊。 拿破崙的那幾個小元帥| R。 p . Dunn-Pattison 
 由於他死於胃癌，他只能保留很少的食物。 同化記憶|馬庫斯·德懷特·拉羅(又名A. Loisette教授) 
 很少有學生能完全掌握這些愛好，即使掌握了，也無法長久地保持。 同化記憶|馬庫斯·德懷特·拉羅(又名A. Loisette教授) 
 他們知道到哪裡去尋找他們所渴望的知識，但他們不擁有它，也不把它保留在他們的頭腦中。 同化記憶|馬庫斯·德懷特·拉羅(又名A. Loisette教授)