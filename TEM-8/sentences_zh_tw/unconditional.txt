你和你的孩子的關係需要有靈活性、同情心、界限和很多無條件的愛。 好孩子有時會對父母撒謊。 羞恥和嚴厲的懲罰只會把他們推開。 梅根·萊希2021年1月20日|華盛頓郵報 
 母親無條件的愛超越了理性、距離、時間、容貌的變化、疾病和失望。 如何從悲傷中找到自由|Jasmine Grant | 2021年1月7日|Essence.com 
 為了展示對軍隊無條件的支持，他們赦免和獎勵了被指控或被判犯有戰爭罪的士兵。 《非常害怕:虛擬恐怖屋》|特雷西·莫蘭| 2020年10月25日|Ozy 
 所以我在這段感情中變得更加體貼和深思熟慮，這是我從未有過的，這種努力、那種專注、那種釋放、那種開放和無條件的愛已經給我帶來了回報。 愛與失去，與科裡·布克，尼克·富里埃佐斯，2020年9月22日，| 
 它的目標是為每個人提供一個安全的空間，培養無條件的接納，並通過領導、社區支持和倡導鼓勵自我表達。 夏洛特:在皇后城慶祝多樣性| lgbtq編輯| 2020年8月11日|沒有直接新聞 
 為人父母就是要能夠提供真正無條件的愛。 親愛的Leelah，我們將為你戰鬥下去:給一個死去的變性少年的信|Parker Molloy | 2015年1月1日|每日野獸 
 我很激動，因為我是傑克·尼克爾森的無條件粉絲。 路易斯·貝格利:我如何寫作|諾亞·查尼| 2013年8月28日|每日野獸 
 只要交戰雙方得到其贊助人的無條件支持，他們就沒有放下武器的動機。 如何結束敘利亞戰爭|Irena L. Sargsyan | 2013年8月13日|DAILY BEAST 
 但這些組織並不經常向以色列施壓，而是提供無條件的支持。 約翰·克里呼籲美國猶太人支持和平努力|阿里·加里布| 2013年6月4日|每日野獸 
 理查德森的一位朋友說，她用“無條件的愛，不斷地保護，讚美她的丈夫。” 關於瑪麗·理查德森·肯尼迪自殺的新問題|南希·柯林斯| 2013年5月16日|每日野獸 
 我想就連哈德森醫生都驚呆了; 我們沒有期望無條件投降。 九人及時記|諾埃爾·米勒·盧米斯 
 作為回報，我要求無條件使用農場直到收穫後三個月內。 草原上的溫斯頓哈羅德·邦德洛斯 
 事實上，莎士比亞一直認為沒有無條件的禁止，也沒有無條件的義務。 托爾斯泰評莎士比亞 
 當他沿著沼澤行進時，萊西諾的使者前來，提出無條件投降。 《波利比烏斯的歷史》第二卷(2卷)| 
 我說，瑟琳茜·安，人們總是自己動手; 他們必須無條件投降。 部長的求愛|哈麗特·比徹·斯托