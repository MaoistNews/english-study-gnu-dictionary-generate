幾何和數論之間的聯繫給數學家們提供了一個機會，但他們必須努力利用它。 四面體解決方案在計算機搜索幾十年後終於被證明|凱文·哈特內特| 2021年2月2日|量子雜誌 
 一些基本的幾何圖形是相同的，而一些主要的方面被修改了，特別是人物模型和背景幾何圖形。 《黃金眼007》丟失的Xbox 360重製版已經洩露——作為一個完整的遊戲極速跑|Sam Machkovech | 2021年2月1日|Ars Technica 
 在這裡，目標的幾何形狀是相關的，因為現在連續的箭落在同一個環內的概率是非零的。 你能找到神秘的數字嗎? 扎克·威斯納-格羅斯，2021年1月15日 
 這些超曲面的幾何結構尚不清楚，但在過去的幾十年裡，數學家們已經能夠證明在某些情況下超曲面總是有直線的。 數學家復活希爾伯特的第13個問題|Stephen Ornes | 2021年1月14日|量子雜誌 
 然而，這些樂高積木的基本部分是你可以在普朗克尺度下，在連續幾何的概念開始失去意義之前，將其波包打包到你想要的儘可能小的區域內的實體。 測量大自然組成部分的突破-如此浪漫的事實|蘇博德·帕蒂爾| 2021年1月8日|鸚鵡螺 
 穿過鄧恩公園附近的市中心，敏銳的觀察者會看到一個隱藏的不規則幾何的遊樂場。 《硅谷豪宅》，活吞|Geoff Manaugh | 2014年11月8日 
 一切都變成了一種針對IDF的幾何和拋物線的計算。 塔利班戰鬥季節開始，阿富汗躲避火箭彈尼克·威拉德| 2014年5月14日|每日野獸 
 他把自己的論文寫成了《物理中的幾何攝動理論》一書，講述微分幾何的新發展。 這就是當你教機器自然選擇的力量時發生的事情James Barrat | 2014年2月1日|DAILY BEAST 
 如果你懂幾何，我想你就能懂很多科學知識。 米歇爾·岡瑞和諾姆·喬姆斯基在做什麼? 2013年11月20日，每日野獸 
 我認為在幾何中，你可以僅憑你的感覺，而不是直覺，得出非常準確的結論。 米歇爾·岡瑞和諾姆·喬姆斯基在做什麼? 2013年11月20日，每日野獸 
 哲學與幾何學齊頭並進，那些觀察自然的人也以深奧的計算為榮。 歷史的燈塔之光，第一卷|約翰·洛德 
 文法學校還教授音樂和幾何，這兩門課程完成了少年時代的普通教育。 羅馬人的私生活|哈羅德·惠茨斯通·約翰斯頓 
 當剩下的句子加上“試著學習幾何課”時，整個句子就必須重新構建。 英語:寫作與文學|W。 韋伯斯特 
 人們認為幾何學是從埃及傳入希臘的，天文學和算術是從法烏尼西亞傳入的。 斯特拉波的地理，卷三|斯特拉波 
 在繪畫中，他看到了一種抽象的幾何，其中存在著固定不變的形式。 《現代繪畫史》第一卷(共四卷)|理查德·馬瑟