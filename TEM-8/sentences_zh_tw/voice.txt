我將盡我所能去改變，去遊說，用我的聲音來代表這些改變。 Tom Colicchio希望(和擔心)COVID-19將改變餐飲業|Pallabi Munsi | 2020年9月16日|Ozy 
 語音搜索和語音設備使用量的增加也為消費品牌提供了一個機會，讓消費者比以往任何時候都更容易找到他們的產品。 如何在大流行期間推動必要的數字創新|Nick Chasinov | 2020年9月16日|搜索引擎觀察 
 在德克薩斯州長大的普麗婭·塔希姆(Priya Tahim)覺得自己沒有發言權。 在西方的南亞移民中醞釀的一場心理健康危機需要引起嚴重關注 
 向我們的祖父母致敬是很重要的，能有你們的聲音參與到這場對話中是很好的。 黛比·艾倫的祖母愛加倍|喬-瑪麗·麥肯齊| 2020年9月11日|Essence.com 
 在赫斯頓的書中，權力是通過誰有發言權來體現的。 《財富》雜誌40位40歲以下政府和政策人士的書籍推薦|瑞秋·金| 2020年9月10日|《財富》雜誌 
 “傑弗裡想讓我告訴你，你看起來很漂亮。”那個女人的聲音傳到我難以置信的耳邊。 我試圖警告你關於骯髒的億萬富翁傑弗裡·愛潑斯坦在2003年|Vicky Ward | 2015年1月7日|每日野獸 
 當他開車把我送回伐木路時，弗蘭克用低沉的聲音向我講述了那個地區的情況。 7歲的飛機失事倖存者穿越森林的殘酷旅程|James Higdon | 2015年1月7日|每日野獸 
 當他這樣做的時候，他的聲音中有一種溫柔，一種沉思和可愛的品質，這是他參演的任何電影都沒有捕捉到的。 Lee Marvin的Liberty Valance微笑背後的故事|Robert Ward | 2015年1月3日|DAILY BEAST 
 千禧年行動計劃(MAP)旨在讓年輕人參與政治，讓他們在治理方面有更多的發言權。 我們什麼時候能看到#千禧國會? Linda Killian | 2014年12月26日|DAILY BEAST 
 “他在借用我的聲音給你們講這個故事，”她對人群說。 一個被基地組織威脅的遜尼派-什葉派愛情故事|Ruth Michaelson | 2014年12月26日|DAILY BEAST 
 在其他條件相同的情況下，說話的音量衡量的是頭腦賦予該思想的價值。 表達聲音文化|傑西·埃爾德里奇·索斯威克 
 她臉紅了，為自己的聲音和不習慣的坦率感到陶醉。 《覺醒與短篇小說集》|凱特·肖邦 
 我用盡全力喊了好幾次，但都沒有用。 格列佛遊記|喬納森·斯威夫特 
 公主的眼睛仍然盯著路易斯，同時用一種壓抑的、顫抖的聲音回答著她的家庭教師。 牧師的爐邊卷3 4 |簡·波特 
 為了使聲音在特殊服務中發揮意志力或推動力的作用，有必要首先檢驗它的自由。 表達聲音文化|傑西·埃爾德里奇·索斯威克