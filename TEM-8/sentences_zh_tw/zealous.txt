相反，我把這種熱情的程序理解為必要的安全防範措施。 蘭迪·羅森塔爾(Randy Rosenthal) | 2021年11月5日|《華盛頓郵報》 
 儘管他們熱衷於捍衛自己的權利和自由，但卻不太關心那些與他們不同的人的權利和自由。 我們的憲法危機已經來了，羅伯特·卡根，2021年9月23日，華盛頓郵報 
 由於再多的邏輯論證、哄騙或賄賂都無法改變狂熱陰謀論者的想法，福奇可能無法說服懷疑論者，認為這位NIH大佬是公共服務的典範。 成為安東尼·福奇博士的沉重負擔|尼克·施格| 2021年9月10日|每日野獸 
 為了做到這一點，重要的是不要過於熱衷於使用的渠道數量，無論使用的是什麼公司工具進行溝通——無論是Microsoft Teams, Salesforce，還是谷歌Docs。 實驗、會議主持人和異步工作:微軟從混合工作模式中學到了什麼 
 人們很容易變得過分熱情，購買你永遠不會使用的燒烤工具。 最佳燒烤配件和燒烤工具為最好的後院|Irena Collaku | 2021年6月25日| 
 這些標籤很重要，但同樣重要的是，我們過分熱心地想要分發它們並無休止地思考它們。 莉娜vs女權主義警察|艾米·齊默爾曼| 2014年11月9日|每日野獸 
 美國人有權利擔心國安局過度熱情和毫無根據的監視。 埃博拉，ISIS，邊境:太多的恐懼，如此少的時間! 2014年11月2日|DAILY BEAST 
 極度狂熱的凱文·鮑總統一直在發行戰爭債券以籌集資金，以防必要的戰爭。 你想統治一個王國嗎? Nina Strochlic | 2014年7月17日|每日野獸 
 熱心的民粹主義愛國者可能會在原則上結盟，但有效地團結起來則是另一回事。 選舉可能是歐洲終結的開始特雷西·麥克尼科爾，納黛特·德維瑟| 2014年5月21日|每日野獸 
 他們中間有因著信仰長大的。 有些人是狂熱的皈依者。 少女被敘利亞聖戰組織誘惑? Christopher Dickey | 2014年4月21日 
 他是民主和克倫威爾的狂熱擁護者，以至於當局經常讓他穿上直筒夾克。 《每日曆史與年表》|喬爾·蒙塞爾 
 保羅·埃基德去世，享年81歲; 《格陵蘭島遊記》的作者，也是熱心的格陵蘭島傳教士。 《每日曆史與年表》|喬爾·蒙塞爾 
 他發現自己的親戚都是狂熱的羅馬天主教徒。 從詹姆斯二世即位開始的英國曆史。 |托馬斯·賓頓麥考利 
 弗農是一個狂熱的輝格黨人，他本人並沒有被輝格黨的領導人所接受。 從詹姆斯二世即位開始的英國曆史。 |托馬斯·賓頓麥考利 
 但是，儘管波特蘭是一個不講道理、愛抱怨的朋友，他卻是一個最忠實、最熱心的牧師。 從詹姆斯二世即位開始的英國曆史。 |托馬斯·賓頓麥考利