In that photo, Merabet has a big smile that spreads across his whole face and lights up his eyes. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 
“We talked about the science the whole time the other day,” Krauss told The Daily Beast in a phone interview. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 
If the Israel model ban were directed towards disordered eating, Ravin says she would support it whole-heartedly. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 
What an amazing thing to be able to listen to any music you want, a whole world of bands. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 
And Air Force assessors are the first to say such imaging never tells the whole story. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 
You would not think it too much to set the whole province in flames so that you could have your way with this wretched child. St. Martin's Summer |Rafael Sabatini 
Now, it immediately occurred to Davy that he had never in his whole life had all the plums he wanted at any one time. Davy and The Goblin |Charles E. Carryl 
But Polavieja started his campaign with the immense advantage of having the whole of the dry season before him. The Philippine Islands |John Foreman 
All changes are to be Rang either by walking them (as the term is) or else Whole-pulls, or Half-pulls. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 
The plant as a whole remains green until late in the autumn. How to Know the Ferns |S. Leonard Bastin