As the ’60s turned into the ’70s, stewardesses began bringing grievances to the Equal Employment Opportunity Commission and, eventually, winning their cases. Excitement, glamour and occasional gunfire: The life of a Pan Am stewardess |Mythili Rao |April 9, 2021 |Washington Post 
I have a conversation with one stewardess about our favorite places around the world. Join The Mile High (Dining) Club |Allison McNearney |September 26, 2014 |DAILY BEAST 
The stewardess conferred with the captain,” Caro said, “and they found us this little hotel in Paris. ‘The Power Broker’ Turns 40: How Robert Caro Wrote a Masterpiece |Scott Porch |September 16, 2014 |DAILY BEAST 
Reluctantly, the stewardess fetches the cord, and Willie finishes lashing the vintage Gibsons into position. Stacks: Hitting the Note with the Allman Brothers Band |Grover Lewis |March 15, 2014 |DAILY BEAST 
Willie signals to the stewardess that he needs some help with his tray. Stacks: Hitting the Note with the Allman Brothers Band |Grover Lewis |March 15, 2014 |DAILY BEAST 
Irritably, he complies, but when the stewardess moves on, he reclines the chair again, muttering balefully under his breath. Stacks: Hitting the Note with the Allman Brothers Band |Grover Lewis |March 15, 2014 |DAILY BEAST 
After that the little girl staid mostly with the stewardess, and was comparatively happy. The Cromptons |Mary J. Holmes 
He drew back hastily and threw a quick glance at the stewardess. The Stutterer |R.R. Merliss 
But she soon cheered up and asked the stewardess to show her to her cabin. Daisy Ashford: Her Book |Daisy Ashford 
On entering the main waiting room of the airport, the young stewardess looked quickly about her. Gypsy Flight |Roy J. Snell 
His questions, never very personal, were about the life an airplane stewardess leads. Gypsy Flight |Roy J. Snell