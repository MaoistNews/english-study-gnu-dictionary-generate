The former Portuguese colony and seaport had become a special district of China a few years earlier. Sheldon Adelson, casino magnate who influenced policy from D.C. to Jerusalem, dies at 87 |Donald Frazier |January 12, 2021 |Washington Post 
Delicate melodies do not represent this tough industrial seaport. 'Heroic Old Warhorse' |Jan Morris |February 18, 2011 |DAILY BEAST 
She had to cross the Atlantic in that year, and stopped on some business in the harbor of Portsmouth, an English seaport. Stories of Our Naval Heroes |Various 
Goods sell at Chihuahua at about 200 per cent, on the prices of our Atlantic seaport towns. The Expeditions of Zebulon Montgomery Pike, Volume II (of 3) |Elliott Coues 
According to one of their own traditions Eridu, originally a seaport, was their racial cradle. Myths of Babylonia and Assyria |Donald A. Mackenzie 
At last she said, 'Give us some sea songs; she comes from a seaport town, and will maybe like them better.' Yorkshire Oddities, Incidents and Strange Events |S. Baring-Gould 
On April 22, the bishop, with one companion, left the seaport for his first journey in the land of his adoption. Heroes of To-Day |Mary R. Parkman