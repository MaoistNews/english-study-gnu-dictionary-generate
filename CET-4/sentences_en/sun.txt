Vast power of the sun is tapped by battery using sand ingredient. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 
Any sun shirt is better than no sun shirt, but I like the Crossover because it’s so damn comfortable. The Gear That Lets Me Enjoy the Last Days of Summer |Graham Averill |September 15, 2020 |Outside Online 
“Maybe when Venus comes around on the other side of the sun again,” Greaves says, “things will be better for us here on Earth.” Phosphine gas found in Venus’ atmosphere may be ‘a possible sign of life’ |Lisa Grossman |September 14, 2020 |Science News 
As the sun rose, a new series of medical experts began to evaluate her. Sent Home to Die |by Annie Waldman and Joshua Kaplan |September 2, 2020 |ProPublica 
At the moment, it’s about the equivalent of standing outside at noon in the sun, about 1 kW per square meter. New Zealand Is About to Test Long-Range Wireless Power Transmission |Jason Dorrier |August 30, 2020 |Singularity Hub 
There was deep brown flesh, and bronze flesh, and pallid white flesh, and flesh turned red from the hot sun. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 
They will do it,” Revels declared, “as certainly as the sun shines in the heavens. The Black Man Who Replaced Jefferson Davis in the Senate |Philip Dray |January 7, 2015 |DAILY BEAST 
The nanas and poppies and grannies and grampses who flocked there to roast in the sun. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 
He likes when the sun glances off it from the top, because it looks like the black marlin. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
She had to break the news to William that The Sun had the story. Pulled Documentary Says William Felt ‘Used’ by Charles’ Push for Camilla |Tom Sykes |December 30, 2014 |DAILY BEAST 
Behold a dumpy, comfortable British paterfamilias in a light flannel suit and a faded sun hat. God and my Neighbour |Robert Blatchford 
It was very warm, and for a while they did nothing but exchange remarks about the heat, the sun, the glare. The Awakening and Selected Short Stories |Kate Chopin 
The sun was shining when they arrived at Salon, the gayest, the most coquettish, the most laughing little town in Provence. The Joyous Adventures of Aristide Pujol |William J. Locke 
Only the petrol tins they took for water right and left of their pathway up the cliff; huge diamonds in the evening sun. Gallipoli Diary, Volume I |Ian Hamilton 
I am pleading for a clear white light of education that shall go like the sun round the whole world. The Salvaging Of Civilisation |H. G. (Herbert George) Wells