Because that minimum-wage hike was a project of the federal government, and that means that you-know-who was for it. Inside the Democrats’ Godawful Midterm Election Wipeout |Michael Tomasky |November 5, 2014 |DAILY BEAST 
Meanwhile, they have devastated mental-health funding since you-know-who became president. The Great GOP Mental-Health Hypocrisy |Michael Tomasky |September 20, 2013 |DAILY BEAST 
Another doctor claimed that Afridi ran off with several WHO-supplied boxes of vaccines. Pakistan Sentences Shakil Afridi to 30 Years, Sends U.S. Clear Signal |Ron Moreau, Sami Yousafzai |May 24, 2012 |DAILY BEAST 
So pondering this situation has got me thinking for the first time semi-seriously about you-know-who. Michael Tomasky on Obama’s Hillary Clinton-Joe Biden Switcheroo |Michael Tomasky |May 22, 2012 |DAILY BEAST 
On the other side, President Obama seems to have become Lord Voldemort to the Hollywood elite: He-Who-Must-Not-Be-Named. Hollywood and the Oscars Surprise With Celebration of Traditional Values |Michael Medved |February 27, 2012 |DAILY BEAST 
O my people!Who crowd with greedy eyes round this my jewel,Poor ivory, token of his outward beauty—Oh! The Saint's Tragedy |Charles Kingsley 
From time to time they caught the grewsome head by the hair and shook it violently, shouting, Who-oo-oo! The Head Hunters of Northern Luzon From Ifugao to Kalinga |Cornelis De Witt Willcox 
Aveline-who-never-lied lies half stretched out upon a bench. The Iron Trevet or Jocelyn the Champion |Eugne Sue 
But Miss Wilson will not talk about the row, whatever it was, with the chance of goodness-knows-who coming in any minute. Somehow Good |William de Morgan 
Peter-who-lives-next-door came in this morning to display an infinitesimal, bandaged thumb. Mavis of Green Hill |Faith Baldwin