Luckily for him, he found Sairam Palicherla, a scientist who has spent more than two decades studying farming. UrbanKisaan is betting on vertical farming to bring pesticide-free vegetables to consumers and fight India’s water crisis |Manish Singh |September 17, 2020 |TechCrunch 
Last week, she spent hours on the phone with colleagues to check on the status of their intensive care units. The new Covid-19 case surge in Europe, explained |Julia Belluz |September 17, 2020 |Vox 
I’ve written before about how billionaire philanthropists can spend their money to fight climate change. Want to fight climate change effectively? Here’s where to donate your money. |Sigal Samuel |September 17, 2020 |Vox 
I was tired of spending my evenings trying to understand the next day. Christian Puglisi Is Closing His Influential Copenhagen Restaurants. COVID Is Only Partly to Blame |Rafael Tonon |September 17, 2020 |Eater 
American spent an estimated $9 billion on the holiday in 2018, according to Vox. Zoom parties, throwing candy: How Halloween might happen during Covid-19 |Alexandra Ossola |September 17, 2020 |Quartz 
Speech, in this case, is our ability to spend money on a goofy entertainment. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 
Earlier this week, Huckabee ended his Fox News talk show so he could spend time mulling another bid for the Republican nomination. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 
Prosecutors wanted him to spend at least 10 years behind bars. Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 
He is expected to spend the next few days closeted with lawyers and advisers at his home, Royal Lodge, in Windsor Great Park. From Playboy Prince to Dirty Old Man? |Tom Sykes |January 5, 2015 |DAILY BEAST 
Instead, I spend much of my time criticizing my fellow atheists. The Case Against In-Your-Face Atheism |Steve Neumann |January 4, 2015 |DAILY BEAST 
It's a certainty that they will be captured if they spend that money at any trading-post within our jurisdiction. Raw Gold |Bertrand W. Sinclair 
He told her he would probably spend the next day in bed for a thorough rest, and she agreed that that would be a very good idea. Uncanny Tales |Various 
This is a big country, but you can count on the fingers of one hand the places where a man can spend money. Raw Gold |Bertrand W. Sinclair 
Also, some ominous comments on what armies spend and what Governments scrimp:—that is ammunition. Gallipoli Diary, Volume I |Ian Hamilton 
In order not to weary your Majesty, I shall not dwell longer upon this, or spend time setting forth our losses. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various