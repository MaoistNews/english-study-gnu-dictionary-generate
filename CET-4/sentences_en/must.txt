Ohio is a must-win swing state for Republicans if they are to reclaim the White House. John Kasich: The GOP’s Hobbled 2016 Dark Horse |W. James Antle III |November 3, 2014 |DAILY BEAST 
During the Showtime era, Lakers games became a must-see event that attracted fans and celebrities alike. How to Rescue the Clippers From Donald Sterling’s Racist Clutches |Jesse Lawrence |April 29, 2014 |DAILY BEAST 
In other words, this book is a must-read for every U.S. citizen. Beauty and Subversion in the Secret Poems of Afghan Women |Daniel Bosch |April 6, 2014 |DAILY BEAST 
Meyers still has a way to go if he wants to make his new weeknight gig must-see daily viewing. Seth Meyers Gets Off to a Rocky Start on 'Late Night' |Kevin Fallon |February 25, 2014 |DAILY BEAST 
It's now a must-have wardrobe staple, similar to that of the t-shirt or a pair of jeans. Diane von Furstenberg Celebrates 40 Years of the Wrap Dress |Erin Cunningham |January 14, 2014 |DAILY BEAST 
All of a sudden the Writer and the Sketcher found themselves thrust into the presence of He-who-must-be-obeyed. Punch's Almanack for 1890 |Various 
All of a sudden, with one accord, they put to the Sphinx the question that He-who-must-be-obeyed had asked them. Punch's Almanack for 1890 |Various 
Had not He-who-must-be-obeyed ordered them to seek out the solution of the Great Conundrum? Punch's Almanack for 1890 |Various 
The starching, the fluting, the ironing, all take precious hours that might be employed upon some of the must-haves. The Secret of a Happy Home (1896) |Marion Harland 
It was that terrible once-on-board-the-lugger-and-the-girl-is-mine-I-must-and-shall-possess-her feeling in its most acute form. The Limit |Ada Leverson