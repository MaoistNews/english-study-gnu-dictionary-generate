Really, is it any wonder that fluoride should freak people out? Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 
For a while yoga and pilates classes were sought out at luxury gyms like Equinox. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 
On Thursday, Garcetti ruled himself out of the race to succeed Boxer. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 
Police officials told the AP that they came out with guns blazing. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 
“I think for trans men who are dating every time they hook up they have another coming out,” Sandler said. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 
And he was gone, and out of sight on the swift galloping Benito, before Father Gaspara bethought himself. Ramona |Helen Hunt Jackson 
Most of the men leaped up, caught hold of spears or knives, and rushed out. The Giant of the North |R.M. Ballantyne 
Liszt looked at it, and to her fright and dismay cried out in a fit of impatience, "No, I won't hear it!" Music-Study in Germany |Amy Fay 
The most High hath created medicines out of the earth, and a wise man will not abhor them. The Bible, Douay-Rheims Version |Various 
Squinty could look out, but the slats were as close together as those in a chicken coop, and the little pig could not get out. Squinty the Comical Pig |Richard Barnum