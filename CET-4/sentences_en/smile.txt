Someone who has a great smile, knows what he wants and is driven by his passions. Meet D.C.’s Most Eligible LGBTQ Singles |Staff reports |February 11, 2021 |Washington Blade 
The price she’s paid for getting everything she ever wanted is written into her coquette’s smile, and it’s the saddest sight in the world. Rebecca Hall's Passing Is a Complex, Moving Story About Racial Identity—and a Sundance Standout |Stephanie Zacharek |February 3, 2021 |Time 
Service people were told to plaster a smile onto their faces. Why You Don't Feel as Fulfilled From Your Job as You Think You Should |Eliana Dockterman |January 25, 2021 |Time 
Ally noticed his smile right away and took it as a “good sign.” Date Lab: Mariah Carey. Alligators. Urology. Is this any way to start a romance? |Damona Hoffman |January 21, 2021 |Washington Post 
Brace responded to her ire with a friendly smile and wished her a blessed day. Why good manners are more important than ever during pandemic travel |Christopher Elliott |January 20, 2021 |Washington Post 
In that photo, Merabet has a big smile that spreads across his whole face and lights up his eyes. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 
For those living in poor communities in particular, interactions with police rarely come with good news and a smile. How to Solve the Policing Crisis |Keli Goff |January 5, 2015 |DAILY BEAST 
He was grabbing my hips and he was pouring with perspiration and he had this cheesy smile. From Playboy Prince to Dirty Old Man? |Tom Sykes |January 5, 2015 |DAILY BEAST 
At this point Marvin gives his Liberty Valance smile, the kind that makes you wish you could disintegrate in front of him. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
Nobody terrified audiences with a smile as well as Lee Marvin. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
Joe looked at her with a smile, his face still solemn and serious for all its youth and the fires of new-lit hope behind his eyes. The Bondboy |George W. (George Washington) Ogden 
The moon seemed to smile on him; the aurora appeared to dance with unwonted vigour, as if in glee; the very stars winked at him! The Giant of the North |R.M. Ballantyne 
One adorable smile she gave him, and before he could advance to hold the door for her, she had opened it and passed out. St. Martin's Summer |Rafael Sabatini 
A smile of beatitude spread over his enormous countenance during the process. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
This unreasoning, feminine obstinacy so wrought upon him that he permitted himself a smile and a lapse into irony and banter. St. Martin's Summer |Rafael Sabatini