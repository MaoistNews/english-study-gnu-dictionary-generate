Kanye West finally got Kim Kardashian the Vogue cover he/she/they have been waiting for. Kimye Finally Gets a ‘Vogue’ Cover |Erin Cunningham |March 21, 2014 |DAILY BEAST 
Thinking about fairness comes more naturally to liberals, and they/we care passionately about it. Michael Tomasky: Economic Fairness Isn’t Enough for Obama’s Game Plan |Michael Tomasky |April 12, 2012 |DAILY BEAST 
The congressman's public flogging doesn't fit the crime, and is emblematic of our kick-'em-when-they're-down culture. Why Anthony Weiner Shouldn't Quit |Peter Beinart |June 13, 2011 |DAILY BEAST 
Whichever way you look at it, the are-they-or-aren't-they couple earned the portmanteau Brittana and a lot of attention. Most Memorable Kisses of the Year |Shefali Kulkarni |December 30, 2010 |DAILY BEAST 
Who are Hov, Hova, Jigga, and Jiggaman?They are all the same person: Jay-Z. A Baby Boomer's Guide to Jay-Z |Peter Lauria |November 14, 2010 |DAILY BEAST 
There was a certain difficulty in obtaining the necessary funds without announcing precisely what they-were for. Hyacinth |George A. Birmingham 
A rose from the cottage of Mavis Clare?a rose from the garden of Eden!they are one and the same to me! The Sorrows of Satan |Marie Corelli 
"Reckon I could hold my own agin most of they-uns if I only had a new-fangled gun," returned the boy. Harper's Round Table, October 1, 1895 |Various 
What a noble illustration of the tender laws of this favoured country!they let the paupers go to sleep! Oliver Twist, Vol. I (of 3) |Charles Dickens 
Father declared that he could not lift his feet-they seemed sealed to the ground, and he felt that he must go back. Precious Memories |Various