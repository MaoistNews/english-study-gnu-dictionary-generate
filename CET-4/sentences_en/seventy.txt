Seventy-two adults between the ages of 18 and 50 are participating in the trial, led by the pediatrics department at Oxford. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 
Some seventy-plus countries currently offer some paternity leave or parental leave days reserved for the father. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 
Seventy percent of Hispanic Americans reported being very or somewhat concerned about climate change, and 57 percent of blacks. Extreme Weather? Blame the End Times |Jay Michaelson |November 28, 2014 |DAILY BEAST 
Seventy three percent of students at David Douglas High School in Portland, Oregon are low-income. What It Takes to Fix American Education |Jonah Edelman |November 23, 2014 |DAILY BEAST 
Seventy-five percent of the collection was done by the time he got there. Fashion Designer Oscar de la Renta, American Great, Dead at 82 |Tim Teeman |October 21, 2014 |DAILY BEAST 
The Rev. Alonzo Barnard, seventy-one years of age, accompanied by his daughter, was present. Among the Sioux |R. J. Creswell 
After about the forty-fifth year it becomes gradually less; after seventy-five years it is about one-half the amount given. A Manual of Clinical Diagnosis |James Campbell Todd 
In 1848 there were only seven priests in Birmingham, and but seventy in the whole diocese. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 
He remained with the Midland until 1897, when he retired on superannuation at the age of seventy-six. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 
He is small, alert, brimful of jokes and of years; seventy they say, but he neither looks it nor acts it. Gallipoli Diary, Volume I |Ian Hamilton