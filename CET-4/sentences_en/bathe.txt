Jasmin helps her transfer in and out of her wheelchair, get dressed, and bathe. Care Providers Fight for $15 and a Union |Jasmin Almodovar, Shirley Thompson |December 5, 2014 |DAILY BEAST 
With little water and only a few hours of electricity a day they were unable to shower or bathe the 25 children in the apartment. Israel’s Campaign to Send Gaza Back to the Stone Age |Jesse Rosenfeld |July 29, 2014 |DAILY BEAST 
I remember getting up and going in to bathe him and dress him. Ireland’s ‘Mother and Baby Home’ Horror Goes Beyond Tuam’s Dead Infants |Patsy McGarry |June 6, 2014 |DAILY BEAST 
Women would first bathe their feet in a mixture of vinegar and natural vegetation. Corsets, Muslin Disease, and More of the Deadly Fashion Trends |The Fashion Beast Team |April 1, 2014 |DAILY BEAST 
Early in the voyage, he decided to start the day with a sea bathe. Peter Worthington: Cliff Diver |David Frum |May 14, 2013 |DAILY BEAST 
"To bathe in the very heart's blood of the one desired," he added with grave emphasis. Three More John Silence Stories |Algernon Blackwood 
He helped her to light the lamp; then she went into her room to take off her hat and to bathe her face and hands. The Awakening and Selected Short Stories |Kate Chopin 
If a Hindoo happens to touch a Paria as he is passing, he thinks himself defiled, and is obliged to bathe immediately. A Woman's Journey Round the World |Ida Pfeiffer 
Once I had taken him to bathe in the river; this was summer vacation and several boys came with me to help. Kari the Elephant |Dhan Gopal Mukerji 
He had gone to bathe in the river, had slipped too far out, and not knowing how to swim had almost been drowned. Kari the Elephant |Dhan Gopal Mukerji