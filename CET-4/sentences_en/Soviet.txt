Unlike the Soviet Union at a certain period in history, the Russian economy does not hold a candle to that of the United States. Oliver Stone’s Latest Dictator Suckup |James Kirchick |January 5, 2015 |DAILY BEAST 
Copies of the letter were sent to senior members of the church hierarchy and to the Soviet government. Remembering the Russian Priest Who Fought the Orthodox Church |Cathy Young |December 28, 2014 |DAILY BEAST 
Miles of Soviet era housing projects sat along on the ocean. Cuban Hip-Hop Was Born in Alamar |Daniel Levin |December 26, 2014 |DAILY BEAST 
Not even after its parent company, the Soviet Union, took a dive in 1991. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 
Soviet forces had deftly enveloped the German 4th and 9th Armies, annihilating some 28 divisions. Hitler’s Hail Mary |James A. Warren |December 20, 2014 |DAILY BEAST 
But during the second year Soviet parachutists began to drop, a few at first, then more and more. Second Variety |Philip Kindred Dick 
Factories a long way under ground, behind the Soviet lines, factories that had once made atomic projectiles, now almost forgotten. Second Variety |Philip Kindred Dick 
The Soviet Union had gained great initial success, usual with the side that got the war going. Second Variety |Philip Kindred Dick 
You might have started for the Soviet lines without knowing anything about the work of the other varieties. Second Variety |Philip Kindred Dick 
The people, their costumes—definitely not Pan-Soviet uniforms—and the room and its machines, told him nothing. Hunter Patrol |Henry Beam Piper and John J. McGuire