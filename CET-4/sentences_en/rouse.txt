At last, a cause that could rouse it to action: defending the honor of campaign contributors. How Obama Can Use Executive Actions to Improve Our Democracy |Michael Waldman |April 18, 2014 |DAILY BEAST 
They startle viewers, rouse viewers, occasionally put off and occasionally turn on viewers. ‘Veep’ Is a F*@king Masterclass in Cursing |Kevin Fallon |April 7, 2014 |DAILY BEAST 
He said he had lunch with Pete Rouse, a longtime aide who came with him from the Senate. Obama Counters an Agitated Press Corps With Optimism |Eleanor Clift |December 20, 2013 |DAILY BEAST 
By listening in on their conversation, we get to know the narrator, Asa Baker-Rouse, and the filmmaker, Bianca Giaever. Vimeo Presents: The Top 10 Videos of 2013 | |December 20, 2013 |DAILY BEAST 
A world away, Kristen Rouse was finishing up a tour in Afghanistan as an Army officer. Relief Workers Report on Conditions in the Philippines |Peter Meijer |November 15, 2013 |DAILY BEAST 
Let us go up to Juda, and rouse it up, and draw it away to us, and make the son of Tabeel king in the midst thereof. The Bible, Douay-Rheims Version |Various 
Mr. Mayne thought they should rouse the household at the first reputable looking dwelling they found. The Red Year |Louis Tracy 
They sent agents into the most degraded quarters of the city to rouse and direct the mob. Madame Roland, Makers of History |John S. C. Abbott 
It will only rouse him up, and make him swear at normal graduates in general, and this one in particular. The Cromptons |Mary J. Holmes 
Nothing could rouse him out of his tame civility, which had been taken more than once for obsequiousness. Elster's Folly |Mrs. Henry Wood