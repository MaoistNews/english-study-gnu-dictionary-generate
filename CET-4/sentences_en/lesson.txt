Thirty minutes after my lesson began, I was turned loose with my Pennsylvania map, my van full of supplies, and the traps. How to hunt for star-nosed moles (and their holes) |Kenneth Catania |September 15, 2020 |Popular-Science 
This result would be an extraordinary lesson in how life really can adapt to all available niches within an environment. Gas spotted in Venus’s clouds could be a sign of alien life |Neel Patel |September 14, 2020 |MIT Technology Review 
The TikTok “sale” is turning out to be an object lesson in what you get when governments broker tech deals. Microsoft is dodging a bullet by “losing” out on TikTok |Jane Li |September 14, 2020 |Quartz 
The lesson of 2020 has been that brands need to tighten up every aspect of their e-commerce operation. Deep Dive: How the Summer of 2020 forced brand marketing to change for the better |jim cooper |September 14, 2020 |Digiday 
They gave her piano lessons but they couldn’t afford a piano, so Carlos’ father drew piano keys on a piece of paper so she could practice. Trans musician celebrated in new biography |Terri Schlichenmeyer |September 11, 2020 |Washington Blade 
He was getting another lesson in what he had seemed not to appreciate fully about cops. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 
The second lesson is that no one writing before the twentieth century holds a key to our problems. American Democracy Under Threat for 250 Years |Jedediah Purdy |December 28, 2014 |DAILY BEAST 
A senior law enforcement official suggested one early lesson from the tragedy. Two Cops ‘Assassinated’ in Brooklyn |Michael Daly |December 21, 2014 |DAILY BEAST 
The testimony is damning: the world has not learned its lesson. The Man Who Invented the Word ‘Genocide’ |Nina Strochlic |November 19, 2014 |DAILY BEAST 
“Business theory teaches us one important lesson,” says the instructress. Russia’s Gold Digger Academy |Peter Pomerantsev |November 11, 2014 |DAILY BEAST 
And remember it is by our hypothesis the best possible form and arrangement of that lesson. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
“It means, my dear, that the Dragoons and the 60th will have to teach these impudent rebels a much-needed lesson,” said her uncle. The Red Year |Louis Tracy 
To-day I'm more dead than alive, as we had a lesson from him yesterday that lasted four hours. Music-Study in Germany |Amy Fay 
Shortly after she came to her lesson limping, and remarked that she felt very uncomfortable. Children's Ways |James Sully 
At the lesson following he accordingly presented himself with his arm in a sling. Music-Study in Germany |Amy Fay