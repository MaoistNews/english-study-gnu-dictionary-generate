Possible Summer Happening: There will be a “Song of the Summer,” put out by a 19-year-old pop starlet whose name ends in -ie. Let’s Lay Out the Odds on Your Crazy Summer |Kelly Williams Brown |May 25, 2014 |DAILY BEAST 
One-third of all Egyptians who work for wages (ie, excluding peasant farmers) work for the state, directly or indirectly. Welcome to the Free World, President Morsi |David Frum |September 25, 2012 |DAILY BEAST 
Maybe I just needed more time for IE to start feeling familiar. Can Microsoft Come Back? Dan Lyons’s Month Without Apple and Google |Dan Lyons |April 9, 2012 |DAILY BEAST 
Which leaves... (3) "All instrumentalities of national power," ie, covert operations. Obama's Eye for an Eye: Jerusalem for No Iranian Nukes |David Frum |March 5, 2012 |DAILY BEAST 
Top of the agenda: the government (ie taxpayer) pumping $100bn into their balance sheets in return for a temporary equity stake. Iceland Crisis: Panic Spreads |Andrew Neil |October 7, 2008 |DAILY BEAST 
Ie raconteray seulement ce qu' mon advis on orroit plus volontiers. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
Ie ne say s'il y eust aucun qui fermast l'œil de toute cette nuit. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
Ie leur promis d'ainsy le faire, et n'en attends que les moyens. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
Ie leur donnay quelques croix et quelques images, leur en donnant apprehender ce que je pouvois. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
Ie ne veux rememorer ce que i'ay crit en mon Histoire de la Nouvelle-France, livre 4. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various