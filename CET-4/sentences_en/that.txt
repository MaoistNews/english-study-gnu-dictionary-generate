I am tickled to see that pundittracker.com has named me one of three finalists for best political prediction of 2012. The Tiresias of Punditry |Michael Tomasky |December 6, 2012 |DAILY BEAST 
True, it can be fun to know that this-or-that athlete is Jewish. Redeeming 'Jewish Jocks' |Spencer Ackerman |October 31, 2012 |DAILY BEAST 
Should you suggest something inspired or adventurous, many chefs will demur and revert to their been-there, drank-that pairing. The Food Lover’s Guide to Wine: Perfect Pairings |David Lincoln Ross |January 6, 2012 |DAILY BEAST 
From Moscow to deepest Siberia, subversive artists are provoking the powers-that-be. Russian Artists Mock Putin |Anna Nemtsova |August 4, 2011 |DAILY BEAST 
Is this year's Sundance sales frenzy a direct result of last year's little-movie-that-could? The 'Winter's Bone' Effect |Nicole LaPorte |January 29, 2011 |DAILY BEAST 
"Buy something for your wife that-is-to-be," he said to his grand-nephew, as he handed him the folded paper. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
And whilst I was stretched out that-a-way, Mace come clost and give me her hand. Alec Lloyd, Cowpuncher |Eleanor Gates 
"T-that's why," stuttered Cordelia, smiling through tear-wet eyes. The Sunbridge Girls at Six Star Ranch |Eleanor H. (Eleanor Hodgman) Porter 
"You oughtn't to slip up an' s-startle a lady that-a-way," she said with grave rebuke, and Hale looked humbled. The Trail of the Lonesome Pine |John Fox, Jr. 
For marriage is like life in this-that it is a field of battle, and not a bed of roses. The Pocket R.L.S. |Robert Louis Stevenson