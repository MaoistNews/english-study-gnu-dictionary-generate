The numbers reinforce another article in the Post, in which cops confessed to “turning a blind eye” to minor crimes. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 
If anything, officer training and in-field policing methodologies reinforce those beliefs. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 
No precautions have been taken to reinforce the ceilings, which could collapse onto the statues. Florence Preps ‘David’ for the Big One |Barbie Latza Nadeau |December 25, 2014 |DAILY BEAST 
But there are dozens of new gadgets that reinforce the idea that knowledge is power. Nothing Says I Love You Like Data |The Daily Beast |December 8, 2014 |DAILY BEAST 
We need to reinforce the message that decision-making, the power of choices, is also important. A Black Cop’s Tough Words for Mike Brown |Mary M. Chapman |December 3, 2014 |DAILY BEAST 
So the first question has been whether to reinforce Gaba Tepe from Helles or vice versa. Gallipoli Diary, Volume I |Ian Hamilton 
In 1807, however, he was called up to reinforce the Grand Army in time to take part in the decisive battle at Friedland. Napoleon's Marshals |R. P. Dunn-Pattison 
With Lannes and Mortier sent to reinforce him, it was still more difficult to show patience. Napoleon's Marshals |R. P. Dunn-Pattison 
Napoleon's failure to reinforce Massna left the situation before Lisbon precarious. The Life of Napoleon Bonaparte |William Milligan Sloane 
And would you like to reinforce one battalion, in case of attack, by another battalion? The Doings of the Fifteenth Infantry Brigade |Edward Lord Gleichen