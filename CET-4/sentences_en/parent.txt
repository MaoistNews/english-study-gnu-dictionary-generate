Those invitations went to all but two of the team’s cheerleaders – who happen to be the children of parents who expressed concerns with the practices in recent months. School Sports Became ‘Clubs’ Amid the Pandemic – Now Two Coaches Are Out |Ashly McGlone |September 17, 2020 |Voice of San Diego 
I can’t know how my life would be different had my parents given me another name. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 
By focusing on teletherapy tools and reassuring parents, we’ve seen substantial growth and brand recognition during the pandemic. How to drive digital innovation necessary during the pandemic |Nick Chasinov |September 16, 2020 |Search Engine Watch 
Bad parents, bad genes, bad society, bad luck, bad decisions — pick your poison. ‘The Origins of You’ explores how kids develop into their adult selves |Bruce Bower |September 16, 2020 |Science News 
That can mean children are banned from seeing their own parents or siblings. While We’re Rethinking Policing, It’s Time to End Gang Injunctions |Jamie Wilson |September 15, 2020 |Voice of San Diego 
After four or five months of casual interaction, they realized they both had lost a young parent to cancer. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 
Tinder and OkCupid are both owned by IAC, the parent company of The Daily Beast. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 
To be a parent is to be able to offer truly unconditional love. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 
Not even after its parent company, the Soviet Union, took a dive in 1991. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 
At least one parent would have to be a U.S. citizen or permanent resident. The Progressive Case Against Birthright Citizenship |Keli Goff |December 15, 2014 |DAILY BEAST 
"I'm afraid I couldn't quite manage that, my dear boy," your fond parent would respond. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 
He paused; and hastily wrote a few lines, to say that parent still lived, and would yet proclaim himself with honour to the world. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 
I betray my beloved parent's confidence, to save you from a certain and ignominious death. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 
In some states he becomes the heir of the adopted parent like a natural child, with some limitations. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 
The fibre can be separated with great facility, though firmly attached at one end to the parent rock. Asbestos |Robert H. Jones