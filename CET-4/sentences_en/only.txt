Her striking new, vinyl-only single once again confirms St. Vincent's idiosyncratic talent. Is Bigger Better for St. Vincent? |David Yaffe |December 4, 2014 |DAILY BEAST 
One winter night, Posho Wembore sneaked into the exclusive whites-only club of the Hotel Pourquoi Pas? ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 
The monogamous or safe-only gay man is a far safer bet than the unprotected hetero swinger. The Outrageous Celibacy Requirement for Gay Blood Donors |Jay Michaelson |November 22, 2014 |DAILY BEAST 
It was just what Berners-Lee had envisioned, a Web that was read-write rather than read-only. You Can Look It Up: The Wikipedia Story |Walter Isaacson |October 19, 2014 |DAILY BEAST 
His supporters hosted a men-only fundraiser with this admonition on the invitation: “Tell the misses not to wait up.” The Republican War on Women Continues, Just More Quietly |Eleanor Clift |October 13, 2014 |DAILY BEAST 
In Captain Joseph Pelham's mind there was only-one answer to this question,—that the lad should come to him. By The Sea |Heman White Chaplin 
In the first place the boys all spoke to him in that specially offensive you're-only-a-little-kid sort of way. War and the Weird |Forbes Phillips 
He called Murgatroyd, placed him before the communicator, and set it at voice-only transmission. Pariah Planet |Murray Leinster 
If I had only had a pistol I would have shot him, but boy scouts don't carry pistols-only in crazy story books. Roy Blakely, Pathfinder |Percy Keese Fitzhugh 
She'd be hopeless in a sick-room; and this is a real keep-your-distance, Sundays-only sick-room, ain't it, Sarah Gamp? The Honour of the Clintons |Archibald Marshall