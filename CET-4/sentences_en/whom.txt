By whom?By such as pretend to love her; but come To feed upon her. The Mirror of Taste, and Dramatic Censor, Vol. I, No. 4, April 1810 |Various 
They were among the whom-not-to-know-argues-one-self-unknowns. We Can't Have Everything |Rupert Hughes 
D-did-did you o-over-overhear huh-huh-whom he was going to kuk-kill? A Proposal Under Difficulties |John Kendrick Bangs 
And so do his sisters, and his cousins, and his auntsHis sisters and his cousins!Whom he reckons by the dozens,And his aunts! Aunt Jo's Scrap-Bag, Vol. 5 |Louisa M. Alcott 
A woman can't be too careful not to be seen alone with I-don't-know-whom.' A Pair of Blue Eyes |Thomas Hardy