In more normal times, people already struggled to take time off from work, polling machines broke down, and it was hard for many to even get to the polls. Why you should vote as early as possible (and how to do it) |John Kennedy |September 17, 2020 |Popular-Science 
Allowing the flow of water through coastal areas to return to normal seems key. Soggy coastal soils? Here’s why ecologists love them |Alison Pearce Stevens |September 17, 2020 |Science News For Students 
The crowded bar scene is likely one of the last things that will go “back to normal” after the pandemic. Bar Rescue: Pandemic Edition |jakemeth |September 15, 2020 |Fortune 
Everyone wants to know what the new normal will be like for everything. Are you ready to start traveling for work again? TripActions’ CEO is banking on it |Michal Lev-Ram, writer |September 15, 2020 |Fortune 
During this time, commuters couldn’t take their normal routes—they were forced to use other subway stops to get to work. How a vacation—or a pandemic—can help you adopt better habits now |matthewheimer |September 12, 2020 |Fortune 
Something like fluoride, which is too small for normal filters, yanks away that feeling of agency. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 
Carlisle writes that the Air Force would want a crew ratio of 10 to one for each drone orbit during normal everyday operations. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 
He appears only normal, even in video footage from just two minutes before the shooting. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 
The flight path remained close to the Indonesian archipelago, well within what is the normal reach of air traffic control radar. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 
Six months of sterility results, after which normal fertility returns. Men, Ice Your Balls To Make Babies—and Other Male Fertility Fixes |Tom Sykes |December 22, 2014 |DAILY BEAST 
In the early stages of chronic nephritis, when diagnosis is difficult, it is usually normal. A Manual of Clinical Diagnosis |James Campbell Todd 
Walls End Castle, when the party broke up, returned to its normal state. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
No trait is better marked in the normal child than the impulse to subject others to his own disciplinary system. Children's Ways |James Sully 
It is often present in the respiratory tract under normal conditions. A Manual of Clinical Diagnosis |James Campbell Todd 
Then, inexplicably, he shifted to the other side that the old, the normal Tom presented generously to the new. The Wave |Algernon Blackwood