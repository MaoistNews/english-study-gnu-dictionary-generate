As a company that is beholden to stockholders, Kate Spade usually lags, not leads trends. Handbags: The More You Pay, The Smaller They Shrink |Elizabeth Landers |December 29, 2014 |DAILY BEAST 
Because it is, as Spade and Wilse say, a “tool of social control used by governments to regulate sexuality and family formation.” Were Christians Right About Gay Marriage All Along? |Jay Michaelson |May 27, 2014 |DAILY BEAST 
But then we might have been deprived of Nick and Nora, Sam Spade and the Continental Op. The Man With Stories to Tell |Allen Barra |December 8, 2013 |DAILY BEAST 
But there are four other published Spade stories out there, and it would be nice to have them between covers in one volume. The Man With Stories to Tell |Allen Barra |December 8, 2013 |DAILY BEAST 
The administration refused to budge on calling a spade a spade. Time To Cut Off Egypt |Ali Gharib |August 14, 2013 |DAILY BEAST 
His strong legs and his broad, spade-like feet helped to make him a fine swimmer. The Tale of Grandfather Mole |Arthur Scott Bailey 
The labour of the spade and of the loom, and the petty gains of trade, he contemptuously abandoned to men of a lower caste. The History of England from the Accession of James II. |Thomas Babington Macaulay 
When a spade declaration has been made by dummy, one trump less is necessary and the doubler need not be on the declarer's left. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 
Except in the case of a spade declaration, cases in which redoubling is justifiable are very rare. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 
A spade declaration by the dealer can be doubled with even less strength. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various