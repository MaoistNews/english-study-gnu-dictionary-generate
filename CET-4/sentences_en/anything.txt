If anything the work the two cops and the maintenance guy were doing deserves more respect and probably helped a lot more people. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 
Like many trans users, Transartist often gets used as a source of information more than anything else. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 
Neither the Republican nor the Democratic party have done anything to consistently target Asian- American voters. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 
But Krauss said that from the moment he and the other scientists arrived on the island, they never saw anything untoward. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 
So I started to think about anything in my life that would be worth people giving it any amount of time. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 
There was a rumor that Alessandro and his father had both died; but no one knew anything certainly. Ramona |Helen Hunt Jackson 
As men fixed in the grip of nightmare, we were powerless—unable to do anything but wait. Gallipoli Diary, Volume I |Ian Hamilton 
He made me think of an old time magician more than anything, and I felt that with a touch of his wand he could transform us all. Music-Study in Germany |Amy Fay 
Strathland would bundle me out in ten minutes if anything happened to Jack. Ancestors |Gertrude Atherton 
It is no good settling down in a world that, on its part, refuses to do anything of the sort. The Salvaging Of Civilisation |H. G. (Herbert George) Wells