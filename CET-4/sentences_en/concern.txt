Without school-based counselors and social workers, these concerns may not be investigated. I’m an epidemiologist and a dad. Here’s why I think schools should reopen. |Benjamin P. Linas |July 9, 2020 |Vox 
I don’t find a lot of concern about his actual policies or what’s in his heart. Is evangelical support for Trump a contradiction? |Sean Illing |July 9, 2020 |Vox 
These cameras are not always accurate and raise a whole host of privacy concerns. The coronavirus is ushering in a new era of surveillance at work |Erica Pandey |July 7, 2020 |Axios 
Initially the company had contemplated personalizing ads, using cameras and eye-tracking, on the basis of age and gender but has since declined because of privacy concerns. Cooler Screens is trying to solve digital advertising’s ‘last-mile’ problem |Greg Sterling |July 7, 2020 |Search Engine Land 
Still, some may argue that culture and conduct concerns are too “squishy” to worry about when near-term economic outlooks are so uncertain. The coronavirus crisis is increasing the risk of bank fraud. Here’s how banks can play defense |matthewheimer |July 7, 2020 |Fortune 
Scruff believes that sex is not the primary concern of users. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 
We have to share those feelings of concern that the people are feeling. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 
The economy has begun to add jobs, but the quality of those jobs is an increasing concern. Christie Blames Parents for Bad Economy |Monica Potts |January 3, 2015 |DAILY BEAST 
Until concern trolls like Sarah Ditum came along trying to cover it up again. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 
There is a lot of residual concern that Lizard Squad was able to get even this far. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 
He turned at the sound of my voice with vastly more concern than he'd betrayed under the muzzle of Piegan's gun. Raw Gold |Bertrand W. Sinclair 
It stands very much in the way of that universal adult education which is our present concern. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
That—and no existing institution and no current issue—is the primary concern of the present age. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
I desire and am endeavoring to be on my guard respecting matters which concern his inclination and not his reason. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 
The news caused general concern throughout the house, and Ethne was frightfully upset. Uncanny Tales |Various