As we hired new employees, a few did stupid stuff, leading to errors that cost the company money. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 
There’s a societal position that these cases are between these two people, and if it’s between a man and a woman, that’s personal stuff that we can’t get involved in. “People want to believe”: How Love Fraud builds an absorbing docuseries around a romantic con man |Alissa Wilkinson |September 4, 2020 |Vox 
So now we get to the cool stuff – reverse-engineering the SERPs to snag those premium organic placements. How to become a master of featured snippets |Mark Webster |September 3, 2020 |Search Engine Watch 
That’s going to create more pressure for the stuff that isn’t political. ‘A healthy rebound’: Programmatic rally continues for publishers |Max Willens |September 3, 2020 |Digiday 
It tracks the typical fitness stuff you’d expect like steps and pulse trends. Amazon’s new fitness tracker listens to your voice to figure out your mood |Stan Horaczek |September 2, 2020 |Popular-Science 
The best comparison here for an American audience is, well, Internet stuff. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 
We did ThunderAnt stuff for ourselves and just put it online, and then it blossomed into something else. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 
I think all the traveling and all the nationalities put that stuff in my head. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 
It was a bit strange for a while here with all the Newsweek stuff. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 
There were a lot of little pieces, pieces of lead and stuff. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
I have seen a lot of Bolshevik propaganda and it is not very convincing stuff. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
It makes out of the savage raw material which is our basal mental stuff, a citizen. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
I've got a writ here, Baptiste, and will be glad to have you tell me where this stuff of yours is so I can go and get it. The Homesteader |Oscar Micheaux 
In his youngest days, when his mother used to regulate his food, she would stuff him full of rice. Our Little Korean Cousin |H. Lee M. Pike 
We're right under their noses, so they won't do anything till the stuff's actually in sight. Raw Gold |Bertrand W. Sinclair