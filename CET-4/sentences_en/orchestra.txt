As a director, you’re essentially a conductor of an orchestra. How Making a Film Led Isabel Sandoval to Come Out as Trans |Eugene Robinson |October 20, 2020 |Ozy 
In school, I was involved in the orchestra as the band, the marching band. Lenny Kravitz on Life, Love and the Pursuit of Music |Sean Braswell |October 17, 2020 |Ozy 
Though the pandemic has put all orchestras, the Philharmonic included, in a perilous position, it has also highlighted how such institutions might rethink the conventions of classical music. Why Beethoven’s 5th Symphony matters in 2020 |Charlie Harding |September 25, 2020 |Vox 
New York Philharmonic horn player Leelanee Sterrett says that every orchestra member brings “a different interpretation to their parts each time.” Why Beethoven’s 5th Symphony matters in 2020 |Charlie Harding |September 25, 2020 |Vox 
It doesn’t donate to the city’s vaunted orchestra, and isn’t a member of the region’s chamber of commerce. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 
People scream, the orchestra stops playing, and the stage manager whisks the diva into the wings. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 
Since the arrival of Chorus Master Donald Palumbo, the Met chorus now commands that same level of excellence as the orchestra. Inside the Metropolitan Opera’s Insane Year |Shawn E. Milnes |November 23, 2014 |DAILY BEAST 
She could no longer go to the orchestra; she was confined to a wheelchair. The Nurse Coaching People Through Death by Starvation |Nick Tabor |November 17, 2014 |DAILY BEAST 
Orchestra seats cost $100; mezzanine is $75; and balcony, $50. Here’s the Program for Women in the World Texas! | |October 2, 2014 |DAILY BEAST 
The organ itself is part of the show, as it can rise or drop independent of the orchestra pit. How to Save Silent Movies: Inside New Jersey’s Cinema Paradiso |Rich Goldstein |October 2, 2014 |DAILY BEAST 
I asked of Kellermann, who sat next, "and how is it one finds such an orchestra in such a place?" Music-Study in Germany |Amy Fay 
Why should not Aristide, past master in drumming, find an honourable position in the orchestra of the Tournée Gulland? The Joyous Adventures of Aristide Pujol |William J. Locke 
Mrs. S. said she was familiar with it from having heard Thomas's orchestra play it in New York. Music-Study in Germany |Amy Fay 
But the quiet old town, with its musical name and its great orchestra, will long remain in my memory. Music-Study in Germany |Amy Fay 
So he took my copy and played the orchestra part which is indicated above the piano part, and I played without notes. Music-Study in Germany |Amy Fay