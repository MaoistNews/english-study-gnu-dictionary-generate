So if you get into this puritan mode that to survive we’re all going to have to shelter in place forever, it’s a groan from everybody. "You Need to Use Hope like a Club to Beat Your Opponent." Kim Stanley Robinson on Climate Change and Fiction |Aryn Baker |September 8, 2021 |Time 
Nocturnal grunts, gasps, and groans harmonized to sounds of crashing waves. Paradise lost: Remembering the popular Rehoboth men’s guest house |James Sears |September 2, 2021 |Washington Blade 
Mention “virtual class” and you can almost hear a collective groan reverberate through the internet. How MasterClass became the style-master of MOOCs |Anne Quito |June 5, 2021 |Quartz 
Immediately following the groan of disappointment, a conspiracy of silence began. Banning Kahane Google App Won't Work |Zack Parker |July 2, 2013 |DAILY BEAST 
Copper mining is the most toxic form of metal mining in the United States, but you can only moan and groan about it so much. Copper, the Metal That Runs the World: ‘Boom, Bust, Boom,’ by Bill Carter |Peter Madden |October 26, 2012 |DAILY BEAST 
I've been going by this billboard almost daily for the best part of a month, and it still makes me laugh, and groan. The Prozac Costs Extra |Blake Gopnik |April 30, 2012 |DAILY BEAST 
Sure, if Romney finishes below 40 percent the media will moan and groan that he failed to meet expectations. Will New Hampshire’s Primary Matter in the 2012 GOP Nomination Battle? |Howard Kurtz |January 10, 2012 |DAILY BEAST 
When we hear a groan in the audience we know we did something right, because they get it. Glenn Beck’s ‘Daily Show’ Is the Right’s Latest Bid at Funny |McKay Coppins |November 22, 2011 |DAILY BEAST 
The next moment a pistol was fired at their head, and a deep groan shewed it had taken too true an aim. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 
With a groan, wrung from the very depths of his heart, he tossed the man a gold-piece; another to the woman. Ramona |Helen Hunt Jackson 
Tom sat down as he said this, and, uttering a sort of groan, leaned his back against a tree. Hunting the Lions |R.M. Ballantyne 
With a groan and a curse the guerrilla chieftain yielded himself a prisoner. The Courier of the Ozarks |Byron A. Dunn 
You must imagine this sound as something between a grunt and a groan, that the estimable lady gave vent to whenever put out. Elster's Folly |Mrs. Henry Wood