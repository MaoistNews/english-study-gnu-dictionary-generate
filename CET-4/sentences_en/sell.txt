On Earth, claiming that an environment is lifeless is a tough scientific sell. Missing Antarctic microbes raise thorny questions about the search for aliens |Elise Cutts |July 20, 2021 |Science News 
Big Tech has made that a hard sell in America, simply because the companies have created so much value for consumers. What does breaking up Big Tech really mean? |James Surowiecki |June 30, 2021 |MIT Technology Review 
At the full retail price of $249, these can be a tough sell. Libratone Q Adapt On-ear Headphone Review: this is what a ‘made for Google’ sticker gets you |empire |June 25, 2021 |Popular-Science 
So it wasn’t a hard sell when the North Face asked Chin, who has been sponsored by the company for 20 years, to colead its new Explore Fund Council. Jimmy Chin's Plan to Make the Outdoors More Inclusive |Kathy Karlo |May 11, 2021 |Outside Online 
This will be a hard sell for many minorities who already have low homeownership rates, in large part because of housing discrimination. Covid forced more borrowers to be late on their mortgages than at any time since the Great Recession, CFPB reports |Michelle Singletary |May 7, 2021 |Washington Post 
The Dallas Cowboys sell out their state-of-the art football stadium. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 
As more people come online, the most basic tasks—such as going out to the market to sell produce—will become more efficient. Silicon Valley Sets Its Sights on Africa |Christian Borys |December 22, 2014 |DAILY BEAST 
Along the Prado they used to sell slaves on the auction block, too. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 
As for the political class, I doubt I need to give you a very hard sell on its failure. The U.S. Will Torture Again—and We’re All to Blame |Michael Tomasky |December 12, 2014 |DAILY BEAST 
Question 9: If the female captive was impregnated by her owner, can he then sell her? ISIS Jihadis Get ‘Slavery for Dummies’ |Jamie Dettmer |December 9, 2014 |DAILY BEAST 
He couldn't sell them; he couldn't burn them; he was even compelled to insure them, to his intense disgust. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
Those four pictures—I would not sell those four Watteaus for one hundred thousand francs. Checkmate |Joseph Sheridan Le Fanu 
He thought these things over carefully and finally decided that he would sell them himself. The Homesteader |Oscar Micheaux 
For an Indian to sell a horse and wagon in the San Jacinto valley was not an easy thing, unless he would give them away. Ramona |Helen Hunt Jackson 
For us to take her place it became necessary for us to loan before we could sell and buy. Readings in Money and Banking |Chester Arthur Phillips