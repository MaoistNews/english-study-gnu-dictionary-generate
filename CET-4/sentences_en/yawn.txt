So, a yawn could be a good way for an individual in a social species to communicate to group mates that it is experiencing some kind of internal change. Yawning helps lions synchronize their groups’ movements |Jake Buehler |April 6, 2021 |Science News 
The team found that lions that saw another member of the pride yawn were about 139 times as likely to yawn themselves within the next three minutes. Yawning helps lions synchronize their groups’ movements |Jake Buehler |April 6, 2021 |Science News 
Seeing the lions yawn reminded Palagi of her own work on contagious yawning in primates. Yawning helps lions synchronize their groups’ movements |Jake Buehler |April 6, 2021 |Science News 
Watch a group of lions yawn, and it may seem like nothing more than big, lazy cats acting sleepy, but new research suggests that these yawns may be subtly communicating some important social cues. Yawning helps lions synchronize their groups’ movements |Jake Buehler |April 6, 2021 |Science News 
That yawn is the greatest compliment an inventor can receive. The unfinished business Jeff Bezos leaves behind at Amazon |Lila MacLellan |February 3, 2021 |Quartz 
The arcane pair of paragraphs are packed with the sort of yawn-provoking polysyllables that only a lawyer could love. Brazil’s Rich Ban Biographies Via Arcane Law |Mac Margolis |November 21, 2013 |DAILY BEAST 
Civil libertarians are outraged, but elsewhere the news was met with a collective yawn. Surveillance Program Got You Yawning? Here’s Why You Should Worry |Josh Dzieza |June 11, 2013 |DAILY BEAST 
My greatest fear is that we will find out they are spying on us, and the American public will yawn. Big Brother Is Watching Your Cell Phone |Megan McArdle |June 6, 2013 |DAILY BEAST 
He would read her his poetry, and she would stretch and yawn like a cat. Baudelaire’s Femme Fatale Muse |James MacManus |May 7, 2013 |DAILY BEAST 
But so far, the markets have pretty much offered a big yawn. Cyprus is Imploding, So Why Aren't Markets Freaking Out? |Megan McArdle |March 22, 2013 |DAILY BEAST 
He was about to stretch himself and give vent to a noisy yawn when the word “Laidlaw” smote his ear. The Garret and the Garden |R.M. Ballantyne 
Men yawn and cough, chairs and beds are noisily moved about, heavy feet pace stone floors. Prison Memoirs of an Anarchist |Alexander Berkman 
He disliked the audible yawn with which Cash manifested his return from the deathlike unconsciousness of sleep. Cabin Fever |B. M. Bower 
He insisted upon sending for the doctor, who came, striving not to yawn, but to look pleased. Love's Pilgrimage |Upton Sinclair 
We sometimes yawn, and ask, just by way of conversation, Whether Spain will joyn? Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon