The young man's acute reflexion appeared suddenly to flower into a vision of opportunity that swept everything else away. The Awkward Age |Henry James 
On the contrary, memory is produced suddenly as a result of the last hearing or reflexion. Plotinos: Complete Works, v. 3 |Plotinos (Plotinus) 
If it's a question of further reflexion why did you drag me up here? The Tragic Muse |Henry James 
Carlyle interpreting young Luther's reflexion on the sudden death by his side of his friend Alexis. Dictionary of Quotations from Ancient and Modern, English and Foreign Sources |James Wood 
Superficially it does not appear to be present, but a little reflexion shows that it is there. The Sex Worship and Symbolism of Primitive Races |Sanger Brown, II