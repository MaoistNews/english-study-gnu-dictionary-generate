您始終可以通過參加測試會議來檢查縮放設置，在測試會議中，該服務將播放聲音以檢查您的揚聲器，並在播放前錄製一段簡短的音頻片段以測試您的麥克風。谷歌新的綠屋功能讓你在視頻會議前準備好相機|斯坦·霍拉切克| 2021年2月8日|科普
我們看到了選舉權運動和種族平等，所以許多運動員拿起話筒，宣傳他們所信仰的事業。堪薩斯城負責人勞倫特·杜維內·塔迪夫（Laurent Duvernay Tardif）繼續留在新冠病毒-19的前線。現在他錯過了超級碗|肖恩·格雷戈裡| 2021年2月4日|時間
高靈敏度麥克風甚至可以拾取最細微的聲音，並可以記錄和分析噪音以確定睡眠質量。最佳嬰兒監視器：如何為您的家庭找到合適的嬰兒| PopSci商業團隊| 2021年1月29日|科普
他告訴我應該在廣播時關掉麥克風，這樣效果就不太好了。英國廣播公司（BBC）的凱蒂·凱（Katty Kay）關閉了“叫我閉嘴”的男人|尤金·羅賓遜| 2021年1月23日|奧齊
其中一人走到麥克風前，問了一個有關警官身心健康的問題。哥倫比亞特區市長穆里爾·鮑澤（Muriel Bowser）在賓夕法尼亞大道的一箇中央隔離帶上說，她很快就會歡迎遊客，但現在不會了。|朱莉·扎茲默| 2021年1月19日|華盛頓郵報|
她找到了一種方法，可以讓小貓隨著音樂一起走向麥克風。大一點對聖文森特更好嗎|David Yaffe | 2014年12月4日|每日野獸
兩者都有電子設備，但為男孩提供的圖像是一個麥克風；對於女孩來說，一臺收音機。性別歧視始於玩具走道|南希·卡弗| 2014年11月29日|每日野獸
是的，有一個麥克風和這麼多人在你的控制下總是讓我害怕。喬治·克林頓（George Clinton）關於行業“暴徒”和沒人願意聽瘋子的演講|柯蒂斯·斯蒂芬| 2014年11月19日|每日野獸
當巴雷特把麥克風遞給她旁邊的學生時，他碰巧把一隻父親的手放在她的頭上。11名兒童在密爾沃基被槍殺，其中一名在她祖父的膝上|邁克爾·戴利| 2014年11月12日|每日野獸報
經過大約一分鐘的漫談，一位女士跑上臺，指示某人關掉麥克風。白人至上主義者在肯塔基州競選參議員|奧利維亞·努齊| 2014年9月21日|每日野獸
總工程師接通電源，然後把麥克風掛在兩個前排座位之間。火焰山|哈羅德·利蘭·古德溫
工程師輕敲麥克風，聲音被放大，在機場上空迴響。火焰山|哈羅德·利蘭·古德溫
他把麥克風遞給州長，擺到合適的位置，準備在路上對部隊進行一次碾壓。火焰山|哈羅德·利蘭·古德溫
他早年的自我，在沒有任何真正的麻煩預感的情況下，愉快地沿著麥克風電線巡視了Valier。擠得很緊|迪安·查爾斯·寧
休斯教授對麥克風的發現使我們能夠理解這次失敗的原因。《電報英雄》| J.蒙羅