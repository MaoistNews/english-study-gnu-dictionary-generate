當然，值得一提的是，您當地的99美分商店已經提供了低端杯子，而SpaceEmber不太可能渴望參與競爭。恩伯任命前戴森·海德為消費者首席執行官，因為這家初創公司將目光投向智能杯子之外| Brian Heater | 2021年2月12日| TechCrunch
在這方面，單是一顆新星很難與超新星爆炸相抗衡，超新星爆炸雖然罕見，但釋放出的新產生的元素遠遠多於一顆新星。每年銀河系新星爆炸的次數已經被控制住了|肯·克羅斯韋爾| 2021年2月12日|科學新聞|
它與Robinhood、M1 Finance和其他美國金融科技公司競爭，這些公司為消費者提供了一種以低或零費用投資股票的方式。羅比霍德的痛苦是公眾的收穫，因為風投們急於給它更多的錢|瑪麗·安·阿澤維多| 2021年2月11日| TechCrunch
颶風已經在一月份舉辦了一次公開選拔賽，150名球員在名單上爭奪五個空缺。貓頭鷹在倫敦的“噴火”希望通過回家變得更大|泰迪·阿蒙納巴爾| 2021年2月11日|華盛頓郵報|
《華盛頓郵報》受其所有者傑夫·貝佐斯（Jeff Bezos）的啟發，建立了一套名為宙斯技術（Zeus Technology）的廣告工具，幫助開放網絡上的出版商和廣告商與谷歌和Facebook等大型科技公司爭奪廣告收入。出版商在舊的開放網絡中看到新的生命|斯科特·羅森伯格| 2021年2月9日| Axios
好的、有愛心的老師認識到了他的天賦，並向他挑戰，讓他努力工作，在最高水平上競爭。您當地的學校不必太差勁| Michael S.Roth | 2014年12月17日| DAILY BEAST
正如發明者所描述的，有一個誠實的團隊和一個黑手黨團隊相互競爭。車臣人逃離超現實的過去和危險的現在的地方|安娜·涅姆佐娃| 2014年12月9日|每日野獸
這使他們和無數的多神教相矛盾，在多神教中，許多神都在爭奪地位。教皇方濟各相信基督徒和穆斯林信仰同一個上帝嗎|傑伊·帕里尼| 2014年12月7日|每日野獸
一支龐大的愛爾蘭隊伍參加了聲名狼藉的選秀暴動，因為他們不想與黑人競爭工作。本週的暴亂是美國長期種族憤怒歷史的一部分|沙龍·阿達洛| 2014年11月29日|每日野獸|
它還迫使銀行考慮設計自己的互聯網貨幣市場基金來與Yu'e Bao競爭。阿里巴巴的黑暗面：審查客戶| Brendon Hong | 2014年11月18日| DAILY BEAST
他完全是自己設計的；他不必為建造它而競爭，但在每一個細節上都有全權。最近的風琴製造革命|喬治·萊恩·米勒
它在這個國家並沒有很好的聲譽，而且由於關稅高，無法和我們國內的菸草競爭。菸葉它的歷史、品種、文化、製造和商業——E.R.比林斯。
冒險中的英雄不會與任何數量的騎士競爭，而是每天都要面對一位被選中的冠軍。三天比賽|傑西·L·韋斯頓
所有爭奪這77個獎項的女性都將聚集在大樓梯上的管絃樂隊前。真正的拉丁四分之一| F.Berkeley Smith
現在只有三個人剩下來競爭這個獎項，其他人都放棄了。黑社會|詹姆斯·C·威爾士