他上班時會發現自己必須通過另一個樓梯到達自己的樓層，因為通常的樓梯是為那些在紅色區域的人準備的。他的球隊將參加超級盃。他一直站在冠狀病毒的前線|亞當·基爾戈爾| 2021年2月1日|華盛頓郵報
幾十年前，一位住在二樓公寓的婦女夢見自己從樓裡的木樓梯上摔下來。在我們的夢中，一個無意識的劇場|尼克·羅密歐| 2021年1月22日|華盛頓郵報
它們還可以有效地清潔樓梯，因為它們可以輕鬆地上下移動，這是直立式樓梯無法做到的。最佳吸塵器：如何快速清理|夏洛特·馬庫斯| 2021年1月14日|科普
它們包含從時刻表到周圍環境的描述的信息，比如樓梯有多少英尺遠，下一列火車預計何時到達，或者哪些線路正在經歷延誤。下一個偉大的交通黑客：盲人地鐵乘客應用程序| Carly Stern | 2020年11月25日| Ozy
一條通往階梯山的小徑把她帶到一個寬闊的樓梯上，樓梯上有一隻受傷的巨鷹《無路者》：少於其各部分之和《克里斯托弗·伯德》《2020年11月23日》《華盛頓郵報》
我小心翼翼地走下中央樓梯，走進了十月一個寒冷、陽光明媚的早晨。30年前，我在一個UVA兄弟會被輪姦，沒有人做任何事情| Liz Seccuro | 2014年12月16日| DAILY BEAST
攝像機沿著塔樓的樓梯向後移動，同時鏡頭向前縮放。阿爾弗雷德·希區柯克的《褪色為黑色：偉大導演的最後日子》|大衛·弗里曼| 2014年12月13日|每日野獸報
家裡有孩子的堅強女人蜷縮在大樓梯腳下的圓形大廳裡。彈性城市：9/11後的紐約|約翰·阿夫隆| 2014年9月11日|每日野獸
第二天早上，我跌跌撞撞地走下樓梯，吞下了一塊厚厚的早餐冰沙。奧布里廣場的大分離|奧布里廣場| 2014年8月15日|每日野獸
店主為我們打開了一扇門，我們沿著一個狹窄的螺旋樓梯走到牆上的一個公共洞裡。我在觀看世界盃時聽說了最新的瘋狂射手，當時他差點殺死了一些人| Daniel Genis | 2014年7月1日| DAILY BEAST |
一個穿著制服的高個子幽靈出現了，彷彿是被施了魔法似的，他示意我登上大樓梯。德國音樂研究|艾米·費伊
職員們還沒到，他從樓梯的窗戶往外看，以此消磨時間。匹克威克俱樂部的遺書，v。查爾斯·狄更斯
通過最後提到的樓梯，公眾可進入會議廳。肖厄爾伯明翰詞典|托馬斯·T·哈曼和沃爾特·肖厄爾
當我們到達那所房子時，我們進入了一條不起眼的走廊，開始沿著黑暗狹窄的樓梯上路。德國音樂研究|艾米·費伊
過了一會兒，她和格溫在樓梯腳下匯合，然後他們就到樹林裡去了。祖先|格特魯德·阿瑟頓