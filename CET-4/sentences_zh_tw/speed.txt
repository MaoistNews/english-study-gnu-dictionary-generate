從那時起，總工作時間以完全相同的速度反彈。儘管方法大不相同，但美國和英國的工作時間都出現了相同的下降
這一被理論家稱為普遍性的現象的發現，與發現大象和白鷺以完全相同的最高速度移動一樣奇怪。數學“Hocus Pocus”如何拯救粒子物理學| Charlie Wood | 2020年9月17日| Quanta雜誌
除此之外，一個已經針對速度進行了適當優化的頁面可能也不需要AMP來提高速度。2021年穀歌改變搜索排名的因素：核心網絡要害、E-A-T還是AMP|Aleh Barysevich | 2020年9月16日|搜索引擎觀察
你可以指導速度中的銳度，這意味著你如何開始，如何起跑，以及如何通過你使用的技術保持你的衝刺速度。加拿大青少年是世界上速度最快的足球運動員之一|朱利安·麥肯齊| 2020年9月16日|五歲38歲
當下一次耀斑遇到前一次耀斑釋放的質子時，它會以相同的速度在同一方向加速質子和電子。中微子可以揭示射電爆發的速度|麗莎·格羅斯曼| 2020年9月16日|科學新聞
期限限制可能是加速變革的良方。國會難以忍受的白色|迪安·奧貝達拉| 2015年1月8日|每日野獸
他感覺自己的身體變得軟弱無力（就像一部高速電影中的一朵枯萎的花）。有權勢的國會議員寫“豐滿的乳房”| Asawin Suebsaeng | 2015年1月7日|每日野獸
噴氣式發動機在螺旋槳上立刻帶來了兩大進步：速度提高了一倍，可靠性也提高了很多。8501航班提出了一個問題：現代噴氣式飛機是否過於自動化而無法飛行|克萊夫·歐文| 2015年1月4日|每日野獸
有報道稱，亞航空中客車的飛行速度非常接近引發低速失速的速度。惡劣的天氣使亞航8501飛機墜毀了嗎|克萊夫·歐文| 2014年12月29日|每日野獸
當局指責無政府主義者抗議擬建的連接都靈和法國里昂的TAV高速鐵路線。意大利的鐵軌恐怖|芭比拉薩·納多| 2014年12月28日|每日野獸
真的，對於一個挖地道的人來說，他的速度是驚人的。鼴鼠爺爺的故事|亞瑟·斯科特·貝利
接著是一長段平滑的冰，他以越來越快的速度滑過。北方巨人| R.M.巴蘭坦
他再也不會坐在那個輪子後面，為速度的傲慢而高興了。阿里斯蒂德·普約爾|威廉·J·洛克的歡樂冒險
事實上，他的外表是如此令人敬畏，以至於戴維沒有停下來再看一眼，而是以最快的速度出發了。戴維與妖精|查爾斯·E·卡里爾
女孩幾乎不相信自己的好運，以近乎可笑的速度離開了。聖馬丁之夏|拉斐爾·薩巴蒂尼