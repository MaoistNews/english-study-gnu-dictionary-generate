未來幾個月將揭示中國對加速其脫碳有多麼認真。中國承諾在2060年內實現碳中性，李麗丕可解釋了2020年9月25日
供應商名單中包括了一些在國防部工作中不常見的可疑公司，反映了旨在加快雄心勃勃的系統時間表的“創新收購戰略”。安杜里爾是為戰爭打造空軍“物聯網”的公司之一|泰勒制帽商| 2020年9月24日| TechCrunch
研究作者指出，馬瑙斯較低的社會經濟條件、更擁擠的住房和對乘船旅行的依賴，這些因素可能加速了病毒在那裡的傳播。巴西一座被新冠病毒-19摧毀的城市可能已經達到群體免疫力|喬納森·蘭伯特| 2020年9月24日|科學新聞
多納霍說：“我們正在加速未來四到五年內零售環境中可能發生的事情。”。Nike如何提前三年實現其電子商務目標| Phil Wahba | 2020年9月23日|財富
換句話說，冰面現在處於足夠低的高度，足以加速融化。全球變暖可能導致幾乎不可逆轉的南極融化|卡洛琳·格拉姆林| 2020年9月23日|科學新聞
它可能會加速通過賦予患者更多權力的法律。埃博拉的好處（是的，實際上可能有一個）| Nick Gillespie | 2014年10月13日| DAILY BEAST
“加快這一進程，”羅伊斯說，並補充說，敘利亞自由軍是目前ISIS在敘利亞擴張的解毒劑。史蒂文·索洛夫謀殺案發生後，國會要求就奧巴馬的伊斯蘭國戰爭進行投票
這一趨勢只會隨著白人的流失率高於非白人而加速。即使是保守的福音派支持也無法拯救移民改革|雅各布·盧普費爾| 2014年7月6日|每日野獸報
博尼卡說：“我們完全有理由期待這些人口變化繼續下去，如果不是加速的話。”。《美國醫學的新面孔：自由女性》|戴爾·艾辛格| 2014年6月9日|每日野獸報
保守的福音派正在戈壁地區失去千禧一代，而世界宣明會的餘波肯定只會加速餘波。世界宣明會的同性戀妥協|布拉德·克萊默| 2014年3月26日|每日野獸
但促成西班牙殖民地定居的環境也可能加速他們的解放。巴西航行日誌|瑪麗亞·格雷厄姆
同時，他放棄了對這一圓滿的一切興趣或願望。《英國曆史》三卷，第三卷|E.法爾和E.H.諾蘭
然而，即使是這種簡單的交流也使福斯特夫人的脈搏加快了。牛頓·福斯特|弗雷德裡克·馬里亞特船長
“當然是的，”肖蒂回答，搔搔頭以加快他的精神活動。Si Klegg，第2卷（共6卷）| John McElroy
儘管情況微不足道，但這不僅讓家變得更噁心，而且似乎加速了他的解體。神祕的流浪者，第三卷|索菲亞·裡夫