他說，一旦疫情結束，該系統將更好地幫助該地區恢復。地鐵董事會對增加的債務表示謹慎，但給出了初步批准|賈斯汀·喬治| 2021年2月11日|華盛頓郵報|
這是一次罕見的休賽期，埃姆比德沒有從手術或受傷中恢復過來，而該隊則佔了上風。喬爾·埃姆比德改變了他的休賽期狀態。現在他打得像個MVP|Yaron Weitzman | 2021年2月9日|第五屆
我發現在一天結束的時候我更累了，但我一直努力早點上床睡覺，這樣第二天我就可以恢復並跑步了。想提高你的跑步水平嗎？關注復甦|外部編輯| 2021年2月5日|外部在線
沒有足夠的錢幫助重病兒童康復整個體系需要改變。“俄羅斯的抗議不僅僅是阿列克謝·納瓦爾尼（Alexei Navalny）|馬德琳·羅切（Madeline Roache）| 2021年2月5日|時間
“他們都康復了，但不是每個人都那麼幸運，”視頻中的文字寫道，貼在無面具的人們慶祝的錄像上。超級撒佈機星期天？專家擔心超級碗可能引發冠狀病毒爆炸|布列塔尼·沙瑪斯、費尼特·尼拉皮爾、馬克·馬斯克| 2021年2月5日|華盛頓郵報|
我們需要恢復和培養這樣一種觀念，即對糟糕的演講的正確答案是更多更好的演講。個人電腦警察如何威脅言論自由|尼克·吉萊斯皮| 2015年1月9日|每日野獸
女性從出生後恢復得更快，產後抑鬱的可能性更小。好爸爸如何改變世界|加里·巴克博士，邁克爾·考夫曼| 2015年1月6日|每日野獸
這會減慢上述所有過程的速度，並增加患者能夠足夠快地恢復到醒來的機會。醒來是什麼感覺| Anand Veeravagu博士，醫學博士，Tej Azad | 2014年11月21日|每日野獸
漫畫花了幾十年的時間才恢復併成為一種成人藝術形式。瘋狂的瑞典人計劃對遊戲進行性別歧視評級| Nick Gillespie | 2014年11月20日| DAILY BEAST
實際上，為了恢復體力，她不得不在兩次治療之間躺下。危險冠軍朱莉婭·柯林斯的大腦感覺像糊狀物|蘇傑·庫馬爾| 2014年11月20日|每日野獸
我們將重新恢復舊宗教生活的部分或全部堅定和尊嚴。拯救文明| H.G.（赫伯特·喬治）威爾斯
買主發現這一說法沒有充分的根據，並試圖追回他們的錢。從公元前七世紀到公元二十世紀的女性美術作品|克拉拉·厄斯金·克萊門特
停了幾秒鐘恢復呼吸後，他用胳膊肘擦了擦帽子，宣佈自己準備好了。匹克威克俱樂部的遺書，v。查爾斯·狄更斯
法律只賦予你對他提起訴訟的權利，要求他賠償法律損害的金錢損失。普特南為門外漢準備的簡易法律書|阿爾伯特·西德尼·博爾斯
同樣，如果代理人以自己的名義存入資金，真正的所有者可以向銀行追討。普特南為門外漢準備的簡易法律書|阿爾伯特·西德尼·博爾斯