這臺打印機與大多數微型計算機兼容。
你可以一邊踩踏板一邊看電影，把微型計算機放在你想去的任何地方。
微機控制模糊系統的一種控制算法。
一種數據庫微機日常管理系統的總體設計方法。
微機原理與接口課程教學研究。
基於模糊自適應控制算法的微機配料系統設計。
具有實時性和高可靠性命令響應的微機網絡。
微機中文數據庫系統及其在東北電網中的應用。
《微機原理與應用》教材和教學內容改革的思考。
單片機在頻率信號數字檢測分析中的應用。