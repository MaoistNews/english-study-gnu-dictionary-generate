科學家和政界人士在這場大流行開始時幾乎沒有關於這種冠狀病毒的數據。冠狀病毒模型總是比曲線平坦更重要| Maggie Koerth（Maggie.Koerth-baker@fivethirtyeight.com)2020年9月10日第五屆第三十八屆
在打包過程中，政客們把反對黨的選民塞進了幾個選區，為他們自己的政黨保住了剩下的選區。下一代計算機生成的地圖如何檢測黨派間的選區劃分| Sujata Gupta | 2020年9月7日|科學新聞
根據新措施，Facebook表示將禁止政客和競選活動在選舉前一週發佈新的選舉廣告。Facebook旨在最大限度地減少錯誤信息，將在選舉前一週限制新的政治廣告| radmarya | 2020年9月3日|財富|
因此，我們不需要等待改變遊戲的實際規則，就可以讓能夠獨立於現有政治工業綜合體行事的政客上臺。《美國隱藏的雙頭壟斷》（Ep.356重播）|斯蒂芬·J·杜布納| 2020年9月3日|畸形經濟學
也許暴亂讓尼克松這樣的政客更容易利用現有的種族仇恨，從而對少數族裔採取懲罰性政策。60年代和90年代針對警察暴行的暴力抗議改變了公眾輿論|德國洛佩茲| 2020年8月28日| Vox
他們是模範公民，他們的生活可以被政客在競選演說中作為榜樣。2014年度小說《納撒尼爾·裡奇》2014年12月29日《每日野獸》
“他是一個終生的政治家，”一位華盛頓的顧問說。為什麼左翼喜歡沃倫，但不會迷戀桑德斯|大衛·弗裡德蘭德| 2014年12月19日|每日野獸
哪個政客想為聯邦政府資助的罪犯墮胎辯護？共和黨隱藏的監獄墮胎禁令|哈羅德·波拉克| 2014年12月13日|每日野獸
他一再重申“我不是政治家”和“我不是政治顧問”的說法，這似乎是口頭上的抽搐。奧巴馬醫改的設計師：我想讓自己聽起來很聰明|本·雅各布斯| 2014年12月9日|每日野獸
她寫道：“弗朗索瓦·奧朗德（François Hollande）從未對我說過一句不符合標準的話，也沒有像許多政客那樣對我表現不當。”。地獄沒有瓦萊麗·特里韋勒那樣的憤怒，法國總統的前|莉齊·克羅克| 2014年11月28日|每日野獸|
這位元帥證明了一個強壯的士兵往往是一個軟弱的政治家，1834年他辭職了。拿破崙元帥R.P.鄧恩·帕蒂森
除了今天使他心煩意亂的可憐的虛榮心之外，他身上是否還具有純粹的修剪者和政治家的氣質？祖先|格特魯德·阿瑟頓
摩爾上校是舊自治領的一位資深政治家，是一位非常和藹可親的紳士，也是一位偉大的口齒不清者。《奇聞軼事》和《趣味預算》|各種各樣的
我將比科爾頓更好地管理這些人，科爾頓是美國最粗魯的政治家。祖先|格特魯德·阿瑟頓
他一開始只是一個微不足道的政客，但民主黨老闆接手了他，他的雄心壯志也在增長。祖先|格特魯德·阿瑟頓