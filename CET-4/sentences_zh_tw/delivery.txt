為了應對傳統業務的下滑，優步繼續推進消費者交付，而Lyft則宣佈推進企業對企業物流。騎馬兜售的利潤會來嗎|Alex Wilhelm | 2021年2月12日| TechCrunch
你可以挑選任何你想要的東西——你不需要烹飪、點菜、等兩個小時，或者付送貨費——你只需要拿一個盤子，站起來。我們現在都在幻想后冠狀病毒時代的餐飲：“我只想有人把啤酒灑在我身上”|艾米麗·海爾| 2021年2月12日|華盛頓郵報
卡瓦魯佐說，這是有道理的，因為電子商務和按需交付並不是曇花一現的時尚從你的手機訂購酒：為什麼像吉尼斯、貝比酒這樣的品牌見Drizly | Kimeko McCoy | 2021年2月11日| Digiday的廣告價值
其他的，如餐盒和葡萄酒遞送，比平常更具吸引力。品牌用不同的策略吸引顧客在這個帶有新冠病毒色彩的情人節| Erika Wheless | 2021年2月11日| Digiday
雜貨配送服務收取費用——通常平均每花費100美元收取10美元左右。來自Heloise的提示：雜貨配送有其利弊| Heloise Heloise | 2021年2月11日|華盛頓郵報
如果能力交付經歷了額外的變化，將對該估算進行適當修訂。五角大樓在隱形飛機醜聞中失火| Dave Majumdar | 2015年1月8日| DAILY BEAST
它還可以幫助飛行員在武器運送過程中確定自己的方位。美國最新隱形戰鬥機“落後於”老式戰機10年| Dave Majumdar | 2014年12月26日| DAILY BEAST
本週是美國通用快遞公司的哈姆先生。書堆：出售大衣，保持尊嚴|保羅·亨菲爾| 2014年12月22日|每日野獸
更少的婦女在分娩時戴上鐐銬，儘管這種情況仍然存在。共和黨隱藏的監獄墮胎禁令|哈羅德·波拉克| 2014年12月13日|每日野獸
一種粉狀麻疹疫苗可以使世界各地的分娩更加安全和容易。麻疹粉末疫苗對發展中國家來說可能是巨大的| Kent Sepkowitz | 2014年12月2日| DAILY BEAST
因此，向保險經紀人交付保單，就是向其委託人交付保單。普特南為門外漢準備的簡易法律書|阿爾伯特·西德尼·博爾斯
如果受要約人發送了電報，那麼他將有義務證明發送了電報。普特南為門外漢準備的簡易法律書|阿爾伯特·西德尼·博爾斯
他必須實際交付，儘管契約可以在其他各方面完成，但它不是有效契約。普特南為門外漢準備的簡易法律書|阿爾伯特·西德尼·博爾斯
假設將契約郵寄給承授人，或交給另一個人交付給承授人，這將是一次很好的交付。普特南為門外漢準備的簡易法律書|阿爾伯特·西德尼·博爾斯
當保單寄出並預付郵資，以便及時交付給被保險人時，合同即告完成。普特南為門外漢準備的簡易法律書|阿爾伯特·西德尼·博爾斯