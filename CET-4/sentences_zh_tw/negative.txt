在過去幾年中，人們對中國的態度變得消極。民調：目前在外交政策、美國例外論觀點上存在明顯的黨派分歧|丹·巴爾茲、斯科特·克萊門特| 2020年9月17日|華盛頓郵報
我認為，當我們進入4月份，在溫暖的天氣裡，這對那種病毒有著非常負面的影響。時間線：特朗普淡化冠狀病毒威脅的124次|艾倫·布萊克，JM Rieger | 2020年9月17日|華盛頓郵報
這是一個線索，即疫苗中使用的脊髓灰質炎病毒可能沒有按照預期完全滅活，即使樣本對活脊髓灰質炎病毒檢測呈陰性。對於新冠病毒-19疫苗，大型製藥公司知道只說“不”|馬修海默| 2020年9月11日|財富
在一個選舉年，在經歷了許多對其形象的負面打擊之後，Facebook正試圖將自己重建為最初的社交媒體公司，這對於校園來說意味著迴歸其根源。Facebook剛剛發明……Facebook | Tanya Basu | 2020年9月10日|麻省理工技術評論
超過70%的帖子是負面的，而只有13%的評論是正面的。迪斯尼的《木蘭》將在市場上令人失望地首次亮相，這是它最受歡迎的作品|克萊爾·齊爾曼，記者| 2020年9月10日|財富|
邁爾森本人似乎也接受了這一汙名，對美國小姐選美比賽提出了褒貶不一的看法。為什麼貝絲·邁爾森是第一位也是最後一位猶太裔美國小姐|艾米麗郡| 2015年1月7日|每日野獸
我認為在公眾眼中的一部分是得到認可，以及處理積極和消極的審查。蒂姆·霍華德的密集之牆|威廉·奧康納| 2014年12月22日|每日野獸
除了負面的場上關注，場外問題今年也同樣糟糕。有大學足球教練價值6000萬美元嗎？吉姆·哈博是|傑西·勞倫斯| 2014年12月20日|每日野獸
負面頭條和性侵犯指控仍在繼續。科斯比團隊的不可能任務|勞埃德·格羅夫| 2014年12月18日|每日野獸
他說，他還沒有經歷過來自惠維亞星系的任何負面反饋。醫生：是黑人、亞裔或女醫生的時候了| Nico Hines | 2014年12月11日| DAILY BEAST
路易還沒來得及做出否定的回答，他就聽到了隔壁花園裡的聲音。《牧師的火面》第3卷，共4卷|簡·波特
絕大多數屬於大腸桿菌群，革蘭氏染色法陰性。臨床診斷手冊|詹姆斯·坎貝爾·託德
這是一根細小的桿狀物，位於膿小體內部和之間（圖125），革蘭氏染色呈陰性。臨床診斷手冊|詹姆斯·坎貝爾·託德
該生物體是一種短而厚的雙歧桿菌，通常是細胞內的，革蘭氏陰性（圖126）。臨床診斷手冊|詹姆斯·坎貝爾·託德
新聞審查制度在倫敦是一種負面的罪惡；在開羅，這無疑是積極的。加里波利日記，第一卷|伊恩·漢密爾頓