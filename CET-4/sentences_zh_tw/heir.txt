安迪·賈西（Andy Jassy）可能是傑夫·貝佐斯（Jeff BezosBezos）的繼承人。傑夫·貝佐斯上個月剛滿57歲，去年夏天，該公司宣佈他可能的繼任者之一傑夫·威爾克（Jeff Wilke）將很快退休。傑夫·貝佐斯（Jeff Bezos）辭去亞馬遜首席執行官職務，過渡到執行主席職位|傑伊·格林、託尼·羅姆| 2021年2月4日|華盛頓郵報|
最合乎邏輯的繼承人傑夫·威爾克擊敗貝佐斯，於去年宣佈退休。傑夫·貝佐斯（Jeff Bezos）辭去亞馬遜首席執行官一職受到攻擊|亞倫·普雷斯曼| 2021年2月3日|財富|
氣泡也會迅速變化——它們的壁面在向外飛行時接近光速，具有量子力學的隨機性和波動性。宇宙是如何產生泡沫和碰撞的——事實如此浪漫|查理·伍德| 2021年1月28日|鸚鵡螺
少年後衛埃裡克·阿亞拉作為考恩的接班人進入了本賽季，因為他在教練馬克·圖爾金希望考恩在這個位置上打外線球時就已經有了經驗。對於馬里蘭籃球隊來說，控球后衛已經變成了一份雙人工作|艾米莉·吉安巴沃| 2021年1月26日|華盛頓郵報|
英國王儲的明顯繼承人查爾斯王子要求世界各國的首席執行官在資本主義中保障自然權利。英國君主制的最新“條約”要求首席執行官承認自然權利，邁克爾·J·科倫，2021年1月13日
通過允許他活著，元帥避免了殺害手無寸鐵的繼承人的恥辱。英格蘭最偉大的騎士讓《權力遊戲》蒙羞|威廉·奧康納| 2014年12月9日|每日野獸報
這位走鋼絲家庭的繼承人在他那個時代嘗試過一些瘋狂的特技。Nik Wallenda的矇眼鋼索跳躍：在芝加哥河上挑戰死亡| Jacqui Goddard | 2014年11月2日|每日野獸
呸，他也可以做愛，所以唐頓的繼承人得到了保證。丹·史蒂文斯引爆了《唐頓莊園》：從胖乎乎的亞里士多德到瘦削、刻薄的美國心理醫生蒂姆·蒂曼2014年9月19日《每日野獸》
無論如何，他作為富有的繼承人/反社會者約翰·杜邦（John du Pont）的轉變永遠都是令人震驚和不安的。奧斯卡賽季在多倫多拉開帷幕：Channing Tatum、Kristen Stewart和More Court Awards Glory | Marlow Stern | 2014年9月14日| DAILY BEAST
嗯，他總是可以問安德魯王子，對於迷路的備用繼承人來說，情況如何。花花公子王子的危險|湯姆·賽克斯| 2014年8月29日|每日野獸
這位繼承人和他的兄弟們在恐懼中畏縮，害怕罷工，卻希望其他人會為他們罷工。紅色的一年|路易斯·特蕾西
他現在患有憂鬱症，會密切關注繼承人的健康和習慣；你可以肯定這一點。祖先|格特魯德·阿瑟頓
在一些州，他像親生子女一樣成為領養父母的繼承人，但有一些限制。普特南為門外漢準備的簡易法律書|阿爾伯特·西德尼·博爾斯
該繼承人顯然是一個九歲的男孩，與鄰居國王的女兒結婚。女人的環球之旅|艾達·普費弗
兩分鐘就足夠在“塔爾博特”換隊了，教練時代的接班人又走了。朴茨茅斯公路及其支流|查爾斯G.哈珀