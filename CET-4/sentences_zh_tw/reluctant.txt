政府可能更不願意依賴其他國家的戰略物資，如口罩、藥品或電腦芯片。在20萬億美元的流行病救濟支出之後，仍然沒有通貨膨脹的跡象。發生了什麼事|伯恩哈德·華納| 2020年8月25日|財富
由於未來法庭之爭的不確定性，官員們不願向債券市場的投資者推銷。晨報：101 Ash St.交易前的交易|聖地亞哥之聲| 2020年8月24日|聖地亞哥之聲
倫敦金融城的律師們相信他錯了，但未來法庭鬥爭的不確定性意味著倫敦金融城不願在債券市場向投資者推銷自己的產品。101 Ash St.崩潰前的交易有助於解釋我們是如何來到這裡的| Lisa Halverstadt和Jesse Marx | 2020年8月24日|聖地亞哥之聲|
最近幾周病例的上升被歸咎於社交聚會和旅行者，但官員們不願意求助於在3月和4月疫情最初高峰期實施的嚴格封鎖。歐洲正處於一個轉折點，因為新冠病毒病例激增，脆弱的政府感受到了財富的熱度|伯恩哈德·華納| 2020年8月20日|
其他出版商不願提供幫助，要麼因為他們有自己的訂閱要推廣，要麼他們不確定Scroll是否會成功。託尼·海爾拯救新聞業的探險隊﻿ |Steven Perlberg | 2020年7月27日| Digiday
一些最具爆炸性的機會可能是基於西方世界似乎不願採納的東西。硅谷將目光投向非洲|克里斯蒂安·鮑里斯| 2014年12月22日|每日野獸
我不願意問太具體的問題，因為我覺得我會自找麻煩！克里斯托弗·諾蘭·安切特：《星際》、《本·阿弗萊克的蝙蝠俠》和《人類的未來》|馬洛·斯特恩| 2014年11月10日|《每日野獸》
但美國官員仍然不願承認這一點。間諜警告白宮：不要打擊敘利亞的基地組織| Shane Harris，Jamie Dettmer | 2014年11月7日| DAILY BEAST
如果今年的選票上的候選人不願意和桑德斯一起競選，他們也不會羞於接受他的錢。伯尼·桑德斯（Bernie Sanders）向我們展示了社會黨競選總統的方式|大衛·弗裡德蘭德| 2014年11月3日|每日野獸報
由安德魯·林肯扮演的裡克·格里姆斯開始了這部系列劇，既不情願又痛苦。《行屍走肉》中的盧克·天行者：裡克·格里姆斯是現代神話中的完美英雄|裡賈娜·利茲克| 2014年10月28日|每日野獸
用當時的卑鄙手段，很難從勉強生存的土地上榨取生計。社會正義的未解之謎|斯蒂芬·利科克
當終場哨聲從發動機中響起時，他才勉強從她懷裡掙脫出來。《Pit Town Coronet》，第一卷（共3卷）|查爾斯·詹姆斯·威爾斯
人們不願意把這種政治服從與一種更骯髒的義務聯繫起來。羅伯特國王布魯斯·A·F·穆裡森
這名婦女似乎很不願意接受這一提議，並以各種藉口辯解。他們面前的世界|蘇珊娜·穆迪
法庭更不願意承認醉酒是犯罪行為的藉口。普特南為門外漢準備的簡易法律書|阿爾伯特·西德尼·博爾斯