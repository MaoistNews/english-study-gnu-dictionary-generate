法官們正在考慮《外國主權豁免法》，該法一般禁止對外國提起訴訟。最高法院在納粹藝術品出售案中一致裁決德國|羅伯特·巴恩斯| 2021年2月3日|華盛頓郵報
例如，你可能想要一個門鈴攝像頭，但也許你住在公寓樓裡，房東禁止更換門鎖。最佳家庭安全系統：確保家庭和財產安全| PopSci商業團隊| 2021年1月29日|科普
它的居民和其他兩個大城市的居民一樣被禁止外出冒險。2021年1月22日，查理·坎貝爾（Charlie Campbell）/長沙和武漢時間，隨著新冠肺炎病例的不斷增多和高風險假日旅遊的迫在眉睫，中國迎來了武漢禁閉週年紀念日
值得注意的是，Telegram非常簡短的服務條款禁止在公共頻道上宣傳暴力，但沒有提及在私人頻道或團體上宣傳暴力。為什麼右翼極端分子最喜歡的新綱領如此危險|拉尼·莫拉| 2021年1月20日| Vox
Parler首席執行官John Matze在一份聲明中表示，該公司禁止煽動暴力，並努力滿足蘋果和谷歌對內容節制的要求。蘋果和谷歌可能會在國會暴亂禁令後再次放緩執法行動| Gerrit De Vynck、Reed Albergotti、Jay Greene | 2021年1月12日|華盛頓郵報|
而且有很高比例的女性說她們的丈夫禁止她們工作。與伊斯蘭強人克里斯蒂娜·阿斯奎斯（Christina Asquith）戰鬥的女性2014年12月22日《每日野獸》
這是這個人的信條，他相信不會有任何不守規矩的行為，或者，上帝禁止，任何場景。阿爾弗雷德·希區柯克的《褪色為黑色：偉大導演的最後日子》|大衛·弗里曼| 2014年12月13日|每日野獸報
根據索恩的說法，物理定律可能禁止蟲洞的存在。認識基普·索恩（Kip Thorne），他創造了“星際”的巧妙科學《Asawin Suebsaeng》2014年11月14日《每日野獸》
另一種是禁止進行比聯邦法律要求的更嚴格的背景調查。今年11月美國最重要（也是最古怪）的全民公決|本·雅各布斯| 2014年10月22日|每日野獸
我甚至會小心地把它們撿起來，上帝禁止我把它們帶回家。《你好，先生》《賈斯汀·瓊斯》2014年9月26日《每日野獸》的獲獎同性戀精妙
但是，走到她面前，我的手臂緊握著一個裝有天竺葵植物的錫罐，這是天方夜譚！山谷戰士|納爾遜·勞埃德
在一些州，法規禁止為債權人的利益轉讓此類保單。普特南為門外漢準備的簡易法律書|阿爾伯特·西德尼·博爾斯
許多南方州通過決議，要求北方州禁止發表廢除死刑的文件。神聖遺產：弗吉尼亞的生活|多蘿西·M·託佩
我要禁止它，我要明確地禁止它；這是我對親愛的喬治的責任。《Pit Town Coronet》，第二卷（共3卷）|查爾斯·詹姆斯·威爾斯
我不會再寫這些日記信了，除非有更嚴重的事態發展，這是上帝所禁止的。我所看到的新芬黨叛亂|挪威漢密爾頓夫人