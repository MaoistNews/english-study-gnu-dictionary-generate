你關注了很多媒體品牌的靈魂和良知。媒體購買簡報：Mediabrands首席執行官Daryl Lee討論媒體代理業務的靈魂| jim cooper | 2021年2月8日| Digiday
他想知道下個賽季他是否能夠重新贏得工作，或者病毒的狀態是否已經改善到足以讓他問心無愧地回來。他的球隊將參加超級盃。他一直站在冠狀病毒的前線|亞當·基爾戈爾| 2021年2月1日|華盛頓郵報
如果共和黨人真的重新定位了他們的財政意識，他們會傾聽的。共和黨人如期再次假裝關心赤字|凱瑟琳·蘭佩爾| 2021年1月21日|華盛頓郵報|
她是一個有良心的說唱歌手，她不會離開的。2021年追蹤25顆新星| Daniel Malloy | 2020年12月20日| Ozy
我不知道這是否是你一直在尋找的答案，但這是我唯一能出於良心給出的答案。釘住的藝術|尤金·羅賓遜| 2020年12月14日|奧齊
相反，挺直你的公民脊樑，問心無愧地後退。為什麼我們支持Charlie Hebdo而你也應該支持| John Avlon | 2015年1月8日| DAILY BEAST
“寧可做一個自由的乞丐，”他喊道，“也不要被迫違揹我的良心做出妥協。與希特勒較量的天主教哲學家|約翰·亨利·克羅斯比| 2014年12月26日|每日野獸報|
馬丁·路德·金、納爾遜·曼德拉、奧斯卡·辛德勒——當我們想到良知英雄時，這些名字就會浮現在腦海中。與希特勒較量的天主教哲學家約翰·亨利·克羅斯比2014年12月26日《每日野獸》
正如你所說，“讓一些企業主行使他們的良知不會對同性戀者造成傷害。”同性戀者是否欠基督徒一個橄欖枝？換個角度看|傑伊·邁克爾森| 2014年12月14日|每日野獸
“在這個良知的國家，沒有任何事情是沒有抗議的，”他說。埃裡克·加納的抗議者有直達市政廳的線路|雅各布·西格爾| 2014年12月11日|每日野獸
她欣喜若狂地伸手去拿它；但她可能不喜歡它，除非她付出良心所要求的代價
通過這種方式，它將比現在受到更少的冒犯，更容易受到良心的譴責。《菲律賓群島》，1493-1898年，第二十卷，1621-1624年|
我的良知迫使我直截了當地告訴她，他們只會先到沃爾什的腳邊
他可能會失去理智，“引入性元素”是良心承認它已經被引入
但路易的良知已經平靜下來了；他很快發現“人不是單靠麵包生活的！”牧師的《火邊》第3卷，共4頁《簡·波特》