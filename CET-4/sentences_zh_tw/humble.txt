和人們一樣，有時你也會遇到一場來自卑微出身的特殊風暴。2020年8月的極端天氣背後是什麼？氣候變化與厄運|卡羅琳·格拉姆林| 2020年8月27日|科學新聞
在20世紀60年代早期，一塊簡陋的微芯片可能會擁有一些晶體管。摩爾定律依然有效：英特爾稱芯片將封裝50倍以上的晶體管（Jason Dorrier）2020年8月23日奇點中心
他們並不是唯一一個試圖提升簡陋磚塊功能的人。科學家發現了一種將磚塊變成電池的方法| Edd Gent | 2020年8月17日|奇點中心
與其他視頻類型相比，這些廣告單元的煩人程度要小得多，但它們也是經濟高效的工具，可以為廣告預算不高的公司實現上漏斗和下漏斗目標。2020年最熱門的以用戶為中心的視頻廣告趨勢：CTV、垂直和社交格式| Ivan Guzenko | 2020年6月12日|搜索引擎觀察
儘管《城市詞典》起步不起眼，但它已經名聲大噪，並不斷髮展壯大，每天都有新的詞條出現《城市詞典》賦予了一些精選詞全新的含義| Truthbetold | 2019年11月12日| Truthbetold.news
重要的是要誠實、謙虛，成為一個忠誠的朋友、父親和社區成員。阿布拉莫夫對弗吉尼亞新監獄的建議Guv | Tim Mak，Jackie Kucinich | 2015年1月7日|每日野獸
他們將以謙卑的毅力和絕對的信念面對壓迫。墨西哥牧師被判謀殺罪|傑森·麥加漢| 2015年1月7日|每日野獸
阿布拉莫夫說州長需要記住“謙虛”。阿布拉莫夫對弗吉尼亞新監獄的建議Guv | Tim Mak，Jackie Kucinich | 2015年1月7日| DAILY BEAST
他的目標是讓人覺得他很謙卑，這削弱了他分享自己在如此年輕的時候是如何變得如此偉大的能力。世界最偉大板球運動員威廉·奧康納的故事2014年12月24日《每日野獸》
乍一看，人們可能會把這種奢侈的薪酬水平解讀為這位曾經卑微的實習生的勝利。硅谷實習生在三個月內就能掙到服務人員的年薪| Samantha Allen | 2014年11月25日| DAILY BEAST
她生病了，渾身發抖，美麗的臉謙卑而含淚，她把它彎在他的肩上，流下了一陣陣苦澀的眼淚。埃爾斯特的愚蠢|亨利·伍德夫人
這個人靠艱苦的體力勞動掙的錢只夠支付簡陋的住所和普通食物。社會正義的未解之謎|斯蒂芬·利科克
他立刻向地鞠躬表示感謝，並用最謙卑的話回報了他的感謝。我們的韓國小表弟| H.李M.派克
依我拙見，鐵路專員們做了許多有益的工作，而且做得很好。英格蘭、蘇格蘭和愛爾蘭50年的鐵路生活|約瑟夫·塔洛
他是一個生活卑微的織布工，直到他自己獲得的成就吸引了贊助人。《每日曆史與年表》喬爾·蒙塞爾