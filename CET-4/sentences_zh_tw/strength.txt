不僅僅是你的美麗，還有你的力量和聲音。阿拉斯加州前副州長向這位女士求婚，她第一次講述了自己的故事。|由凱爾·霍普金斯和米歇爾·塞里奧特·布茨撰寫，安克雷奇每日新聞| 2020年9月10日|出版
Philip Tetlock在他的書《專家政治判斷》中幫助我獲得了創業之旅所需的力量。《財富》雜誌《40歲以下40歲健康》的書籍推薦|雷切爾·金| 2020年9月9日|《財富》
如果他們能將這種明顯的力量與更穩定的得分進攻相結合，火箭隊確實可以贏得這一切。火箭隊的新首發陣容剛剛觸及其潛力的表面，邁克爾·皮納（Michael Pina）於2020年9月8日（Fivethirty8）
據知情人士透露，Peloton還在為其自行車準備訓練營訓練課程，這些課程是目前可在踏板和Peloton移動應用程序上獲得的力量訓練視頻訓練。Peloton正在降低其標準自行車的價格，併發布新的運動設備《瑞秋·沙洛姆》（Rachel Schallom）2020年9月4日《財富》（Fortune）
重蹲和負重舉需要巨大的核心力量，但至關重要的是，它們不需要移動你的腹肌。如何獲得六包（甚至八包）| Sara Chodosh | 2020年9月4日|科普
莫里斯尋找他營養不良的身體裡剩下的任何力量。有權勢的國會議員寫“豐滿的乳房”| Asawin Suebsaeng | 2015年1月7日|每日野獸
最危險的攻擊是那些破壞你感知的力量的攻擊。克里斯克里斯蒂會後悔他的牛仔擁抱嗎|馬特·劉易斯| 2015年1月5日|每日野獸
這給了他們巨大的道德力量和安慰。希特勒的《萬歲瑪麗》|詹姆斯·A·沃倫| 2014年12月20日|每日野獸報
它們是關於迫害和勝利、逆境和力量的故事。法定人數：全球LGBT之聲| | 2014年12月10日|每日野獸
華盛頓擁有兩倍於英國士兵的兵力，這將是一場激烈的智慧和力量之戰。英國皇室成員萊因維德·布魯克林：威廉和凱特來到歷史遺址觀看籃球比賽|賈斯汀·瓊斯| 2014年12月6日|每日野獸
醉酒的熱度是愚人的絆腳石，它會削弱體力，造成傷害。《聖經》，杜埃·萊姆斯版本，多種多樣
她的家人顯然很煩惱，這很容易解釋，但他對她的保鏢的力量感到驚訝。紅色的一年|路易斯·特蕾西
比別人聰明就是比別人誠實；而心靈的力量只是看到和說出真相的勇氣。思想的珍珠|成熟M.巴盧
因為死亡是憂傷的結果，它壓倒力量，心中的憂傷垂下頸項。《聖經》，杜埃·萊姆斯版本，多種多樣
但是，他的手臂的力量和內心的勇氣不可能長久地保護他免受他們的堅決攻擊。《牧師的火面》第3卷，共4卷|簡·波特