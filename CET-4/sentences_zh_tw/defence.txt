那尊雕像上刻著對南方士兵的悼念：“他們為了捍衛他們認為神聖的權利，拿起武器反抗弗吉尼亞的入侵者”——每當他經過這座雕像時，他都會感到一陣寒意。邦聯的最後安息之地|馬克·費希爾| 2021年5月30日|華盛頓郵報
丹尼斯與約翰·列儂錄製了一首單曲，為法律辯護基金籌集資金。反文化偶像和出版商費利克斯·丹尼斯（Felix Dennis）在《快速生活》（Fast Living）上花費1億美元，於2014年6月23日在《每日野獸》（DAILY BEAST）去世
在愛沙尼亞，哈里王子將感謝愛沙尼亞國防軍對駐阿富汗英軍的支持。克雷西達婚禮峰會|湯姆·賽克斯| 2014年4月4日|每日野獸
好吧，英國最著名的公關大師馬克斯·克利福德的辯護團隊出來了——搖擺不定？這就是：馬克斯·克利福德的陰莖“當然不是很大”|湯姆·賽克斯| 2014年3月26日|每日野獸
隨後，對拉脫維亞國防部和國防軍基地進行了類似的襲擊。俄羅斯硬實力的迴歸|Michael Weiss | 2013年11月23日|每日野獸
英國最有思想的左翼報紙《衛報》今天發表了一篇為查爾斯王子辯護的文章，向其致敬。衛報作家為查爾斯王子辯護|湯姆·賽克斯| 2013年9月5日|每日野獸
在辯護期間，皇后頻頻搖頭；完成後，她從椅子上站了起來。《牧師的火面》第3卷，共4卷|簡·波特
她也有一顆偉大而富有哲理的頭腦，為洛克寫了一篇精闢的辯護文章。《每日曆史與年表》喬爾·蒙塞爾
當然，這必須歸功於當地“高尚的自衛藝術”的支持者——布魯姆·布魯塞爾斯。肖厄爾伯明翰詞典|托馬斯·T·哈曼和沃爾特·肖厄爾
再也找不到更令人欽佩的例子來說明防衛的本質在於激烈的局部進攻這一事實。拿破崙元帥R.P.鄧恩·帕蒂森
土耳其人在奎因的哨所下發射了一枚地雷，然後衝向因爆炸而被隔離的防禦部分。加里波利日記，第一卷|伊恩·漢密爾頓