儘管共和黨和民主黨預算鷹派努力控制支出，經濟新聞也有所改善，這表明美國人的境況比前兩輪檢查時要好。你是否應該接受經濟刺激檢查的經濟理由| Roya Wolverson | 2021年2月26日|石英
這種“成群結隊”騷擾像蛇和鷹這樣的鳥類捕食者，是這類動物存在的可靠線索。一隻雄性琴鳥可以模仿整個鳥群的聲音|傑克·布勒| 2021年2月25日|科學新聞
現在，桑德斯掌握了真正的議程制定權力，這一點受到了極左翼議員的讚揚，他們被鼓勵在關鍵位置上有一個盟友——而不是赤字鷹派。伯尼·桑德斯手拿木槌，毫不掩飾地提出了一個自由主義的經濟議程。|邁克·德博尼斯| 2021年2月19日|華盛頓郵報|
事實上，她隨時都可能消失，因為最近鷹在頭頂盤旋的次數越來越多。一隻生病的松鼠給了我意想不到的安慰| Pam Spritzer | 2021年2月8日|華盛頓郵報|
在煮沸過程接近尾聲時，像鷹一樣觀察你的汁液。在不傷害樹木的情況下自己製作楓樹糖漿|由Tim MacWelch/戶外生活| 2021年2月7日|科普
當唐納德·里根（Donald Regan）問道：“我們反對以色列人向伊朗發射鷹派……導彈嗎？”里根白宮如何對伊朗反政府組織（Iran-Contra）披露的事件做出錯誤的反應|馬爾科姆·伯恩| 2014年11月3日|每日野獸報
NSPG會議確定了霍克協議的官方路線，但沒有解決資金轉移問題。里根白宮如何對伊朗反政府組織的揭露做出錯誤的反應|馬爾科姆·伯恩| 2014年11月3日|每日野獸報
蒙克對霍克說，“你是偉大的科爾曼·霍金斯，對嗎？堆棧：約翰·科爾特蘭的強大音樂探索|納特·亨托夫| 2014年10月18日|每日野獸
對我來說，我簽下了復仇者1-3，然後是鷹眼，以防他們想做一個鷹眼分拆。傑里米·雷納（Jeremy Renner）在《每日野獸》（DAILY BEAST）上公開了婚姻、他與媒體的問題以及鷹眼的未來
大多數美國人都是通過“生活援助”或“黑鷹墜落”或麥當娜的利他衝動來看待非洲的。我是如何沉迷於非洲的（併為此寫了一部驚悚片）|託德·莫斯| 2014年9月9日|每日野獸
因為亨利·霍克可以坐在路邊遠處的一棵大榆樹上看到他。鼴鼠爺爺的故事|亞瑟·斯科特·貝利
黑暗的天空中響起了夜鷹的鳴叫聲，頭頂的樹葉中傳來了打瞌睡的母雞的咯咯聲。山谷戰士|納爾遜·勞埃德
對這位鷹頭鼻子、鐵眼睛的軍官的記憶像鞭子一樣鞭打著他。這位軍官在二十四小時內從庫納爾騎到米魯特。紅色的一年|路易斯·特蕾西
我把這隻鳥帶回家，一直放在我的鳥舍裡，直到1868年3月，它被一隻鷹穿過電線擊斃。根西島鳥類（1879）|塞西爾·史密斯
一個巨人般的傢伙，有一雙鷹一樣的眼睛，留著黑色的大鬍子，從某種程度上說，似乎是個鐵匠。紅色的一年|路易斯·特蕾西