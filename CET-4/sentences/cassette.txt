I absorbed their sorrow in the wistful Vietnamese ballads my parents played on their cassette tapes and their anguish in the stories my relatives shared about their childhood homes I never saw. The U.S. Offered My Family a Home After the Vietnam War. It Must Do the Same for Those Fleeing Afghanistan |Aimee Phan |August 17, 2021 |Time 

Gone are the days of books on cassettes that were sold in big, cumbersome bricks of 10 or more tapes. Best Audible books: The most popular titles and best sellers for your listening pleasure |Irena Collaku |July 29, 2021 |Popular-Science 

Why we would ever want to go back to torturing our bodies by carrying plastic cartridges that play music is beyond me, but we certainly are—2020 saw an increase in cassette sales of 103 percent compared to the previous year. A modern guide to cassette tape players |Sandra Gutierrez |July 23, 2021 |Popular-Science 

I would rewind the cassette over and over, manically running around my room, yelling and doing ferocious dunks on my Fisher Price four-foot plastic hoop. Debating How Space Jam: A New Legacy Stacks Up Against the Original |Cady Lang |July 19, 2021 |Time 

One system is called Yoto, but it’s got much more in common with a cassette player than a smart speaker. When AI becomes child’s play |Anthony Green |June 9, 2021 |MIT Technology Review 

As a result, prisoners collect, collate, trade and secretly alter cassette tapes of their desired sounds, which are rap and R&B. Prisoners Get Cultural Fix with 8-Tracks and Bootleg Cassettes |Daniel Genis |August 18, 2014 |DAILY BEAST 

Cassette tapes, specifically finding them, are also a challenge. Prisoners Get Cultural Fix with 8-Tracks and Bootleg Cassettes |Daniel Genis |August 18, 2014 |DAILY BEAST 

Halford, absent from court all morning, arrives late in the afternoon with a large, black double-deck and a cassette. The Stacks: The Judas Priest Teen Suicide Trial |Ivan Solotaroff |June 28, 2014 |DAILY BEAST 

During the ride, Jai Johany plays lacy Afro jazz on a cassette machine, frowning, saying nothing. Stacks: Hitting the Note with the Allman Brothers Band |Grover Lewis |March 15, 2014 |DAILY BEAST 

Growing up in the 1990s, I watched Free to Be on VHS and listened to the songs on a cassette tape over and over again. ‘Free to Be…You and Me’ Did Not Emasculate Men |Emily Shire |March 11, 2014 |DAILY BEAST 

Monsieur Cassette had not sold another such cane during the last two years. The Mystery of the Yellow Room |Gaston Leroux 

The theft of the cassette, however, was a transaction which his enemies never suffered to be forgotten. Contemporary Socialism |John Rae 

The cassette of St. Joseph, wherein were deposited the oboli for the poor, had long been emptied. The Golden Dog |William Kirby 

II rencontra la Relique prs de Sens; elle toit enferme dans une triple cassette. The Churches of Paris |S. Sophia Beale 

It had supplied each of them with a small box, cassette, containing a few articles of clothing. The American Quarterly Review, No. 17, March 1831 |Various