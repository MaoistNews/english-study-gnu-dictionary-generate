In Bedminster this spring, the records show, Trump’s club charged the Secret Service more than $21,800 to rent a cottage and other rooms while the club was closed and otherwise off-limits to guests. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

Chico-San’s ads proposed trading bread for rice cakes and using the low-calorie rice saucers as a surface to support jelly, cottage cheese, fruit, and other toppings. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

The final stop is Lewa House, a cluster of cottages on the 61,000-acre Lewa Wildlife Conservancy. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

Matheson was evicted along with her 90-year-old mother, who was living in a guest cottage. The Mystery House: How a Suspicious Multimillion Dollar Real Estate Deal Is Connected to California’s Deadliest Fire |by Scott Morris, Bay City News Foundation |August 26, 2020 |ProPublica 

I have a summer house by the sea, a wooden cottage with no heat. That chatbot I’ve loved to hate |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Finally, as the sun was about to set, she came upon a little cottage that belonged to seven dwarfs. In New Brothers Grimm 'Snow White', The Prince Doesn't Save Her |The Brothers Grimm |November 30, 2014 |DAILY BEAST 

When Little Snow White awoke, they asked her who she was and how she had managed to come to their cottage. In New Brothers Grimm 'Snow White', The Prince Doesn't Save Her |The Brothers Grimm |November 30, 2014 |DAILY BEAST 

Nevertheless, a cottage industry has grown up around the case. Jack the Ripper Is Still at Large |Christopher Moraff |September 29, 2014 |DAILY BEAST 

How Bill Simmons and company have turned NFL indignation into a cozy little cottage industry. Forget the Wife Beating—Are You Ready for Some Football? |Steve Almond |September 11, 2014 |DAILY BEAST 

If ESPN is a sleek bachelor pad, ESPNW is the cottage next door filled with Activia and ultra-soft toilet paper. Women's Sports Are Getting Less Airtime |Evelyn Shoop |August 23, 2014 |DAILY BEAST 

Madame Ratignolle, when they had regained her cottage, went in to take the hour's rest which she considered helpful. The Awakening and Selected Short Stories |Kate Chopin 

She rose with a smile as Lady Victoria emerged from the cottage at the upper end of the village. Ancestors |Gertrude Atherton 

They had almost reached the sawyer's cottage, when a black animal ran out towards them. The Nursery, July 1873, Vol. XIV. No. 1 |Various 

The entire scene had vanished, vanished like smoke over the roof of a cottage when the wind blows. Three More John Silence Stories |Algernon Blackwood 

Dr. Stanmore came down the flagged path from the smith's cottage, pulling on his gloves. Uncanny Tales |Various