Scientists have long known that the brain harbors the biological equivalent of a car’s fuel gauge—a complex homeostatic system that allows our gray matter to track the state of our basic biological needs, like those for food, water, and sleep. Why do you feel lonely? Neuroscience is starting to find answers. |Amy Nordrum |September 4, 2020 |MIT Technology Review 

Once the temperature gauge on top let us know that the grill was adequately heated, we loaded the 285-square-inch grate with all our meat at once. 5 Portable Grills for All Your Outdoor Cooking Needs |Amy Marturana Winderl |September 2, 2020 |Outside Online 

Simultaneous increases in equity and volatility gauges are unusual, and a reason for concern for some. The stock market is hitting new records—but a warning signal called the ‘VIX’ is flashing |Lee Clifford |August 27, 2020 |Fortune 

They assess how financially healthy a company is and act as an independent gauge, letting investors in a company know how likely that company is to pay back its debt, for instance. Environment Report: The Latest Power Struggles for SDG&E and Sempra |MacKenzie Elmer |June 29, 2020 |Voice of San Diego 

Use this activity from NASA to design and build your own rain gauge. Let’s learn about rain |Bethany Brookshire |June 17, 2020 |Science News For Students 

It took the entire day, but the slow pace indicated that it was probably a test to gauge public reaction. The Monuments Men of Occupy Hong Kong |Brendon Hong |December 4, 2014 |DAILY BEAST 

So, he approached his nomadic friends to gauge their interest in the collaboration. London’s Pagan Counterculture Kings |Justin Jones |October 12, 2014 |DAILY BEAST 

To gauge his level of truthfulness, I asked, “So, you wouldn't mind if I included your donor identification number in the story?” Today’s Sperm Donor Isn’t a Broke 20-Something |Stephanie Fairyington |September 20, 2014 |DAILY BEAST 

The only gauge of normality that young people have is their observation of each other. Kids Don’t Know How Overweight They Really Are |Russell Saunders |July 29, 2014 |DAILY BEAST 

When a soldier is hit by an IED the gauge records the event. How a Thumb-Sized Gauge Is Revolutionizing Traumatic Brain Injuries |Brian Castner |March 23, 2014 |DAILY BEAST 

The gauge of railways in Great Britain was not fixed upon any scientific principle. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Trevithick determined in future to use two safety-valves, and also a safety steam-gauge. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The Commonwealth has for some time been considering the conversion of the lines into one standard gauge, the British gauge of 4ft. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The leaf is held in one hand and the gauge and knife in the other, the edge of the leaf being drawn through the gauge. Philippine Mats |Hugo H. Miller 

For this purpose there is used in most localities a small gauge held between the thumb and index fingers. Philippine Mats |Hugo H. Miller