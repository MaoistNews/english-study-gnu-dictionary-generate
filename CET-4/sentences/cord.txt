The firm claims that the prototype's supposedly functional dashboard display was powered by an extension cord snaking up from under the stage. Nikola stock craters after chairman fails to rebut fraud allegations |Timothy B. Lee |September 11, 2020 |Ars Technica 

Drivers will have to find places to charge their vehicles, which is trickier than fueling up at a gas station, especially if you don’t live in a place where you can just run an extension cord from your house out to your car. Uber Wants to Go All-Electric by 2030. It Won’t Be Easy |Vanessa Bates Ramirez |September 10, 2020 |Singularity Hub 

Yet retinoic acid signaling was thought to be essential for making a brain, nerve cord and other vital features. By Losing Genes, Life Often Evolved More Complexity |Viviane Callier |September 1, 2020 |Quanta Magazine 

Musk said he wants to eventually conduct a clinical trial on people who suffer from tetraplegia, a type of paralysis caused by spinal cord injuries. Elon Musk shows off Neuralink brain implant technology in a living pig |jonathanvanian2015 |August 29, 2020 |Fortune 

This Ranvoo bag is perfect for your high schooler, as it fits up to a 15-inch laptop, has a cord for phone charging, and provides a dedicated headphone jack that allows for listening on the go. Backpacks that will charge your phone |PopSci Commerce Team |August 26, 2020 |Popular-Science 

The popular snack has also struck a cord with Paleo dieters, according to Lewis. Is Cricket Flour the New Protein Powder? |DailyBurn |November 21, 2014 |DAILY BEAST 

They hold signs depicting a fetus with a hanging umbilical cord. Abortion in Missouri Is the Wait of a Lifetime |Justin Glawe |November 12, 2014 |DAILY BEAST 

He carried an extension cord in case he needed to recharge at one of his emergency spots. How Brooklyn’s First Ice Cream Girl Fought City Hall–and Won |Michael Daly |October 13, 2014 |DAILY BEAST 

She tried, says Schreiber, “to cut the umbilical cord to her parents mainly by way of entrance into high culture.” Still Desperately Seeking Susan Sontag |Allen Barra |September 26, 2014 |DAILY BEAST 

Some doctors speculate they are generated in the spinal cord. Real Life Lazarus: When Patients Rise From the Dead |Sandeep Jauhar |August 21, 2014 |DAILY BEAST 

Strange to say, the silken cord yielded to the first pull, as if nothing had been wrong with it at all! The Giant of the North |R.M. Ballantyne 

"You spoke of disgrace," she observed gently, swaying her fan before her by its silken cord. Elster's Folly |Mrs. Henry Wood 

He turned his cool regard upon Chief Inspector Kerry, twirling the cord of his monocle about one finger. Dope |Sax Rohmer 

Stop and try a ride, Billy, urged Lance Darby, holding the cord of the tugging kite. The Girls of Central High on the Stage |Gertrude W. Morrison 

Taking the scissors from Violet's workbag, she cut the laundry bag carefully into two pieces, saving the cord for a clothesline. The Box-Car Children |Gertrude Chandler Warner