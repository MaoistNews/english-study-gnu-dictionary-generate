He called to receive financial advice on tax implications, then had a chat with his father. Trevor Bauer, unorthodox star with an unorthodox deal, gets an unorthodox Dodgers intro |Chelsea Janes |February 12, 2021 |Washington Post 

Individuals who register for this free event can receive a downloadable affirmations journal and a Valentine’s Day card. Calendar: Feb. 12-18 |Philip Van Slooten |February 9, 2021 |Washington Blade 

Wyden has also said in a statement that families that had received the first two payments would expect a third. House Democrats reject plan to sharply curtail $1,400 stimulus payments in coronavirus relief package |Jeff Stein, Erica Werner |February 9, 2021 |Washington Post 

He said he had received grass-roots donations from people in all 67 counties of Pennsylvania and all 50 states. Republican Sen. Richard C. Shelby announces he will retire in 2022 |Colby Itkowitz, Erica Werner |February 9, 2021 |Washington Post 

After agreeing to pay cuts last year, players will receive 100 percent of their salaries and bonuses in 2021. MLS and players’ union ratify labor agreement |Steven Goff |February 8, 2021 |Washington Post 

Specifically, what briefing did the flight crew receive before they went to the airplane? Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

They will still receive a salary if something is to happen to Ziad, but she is trying to make sure she saves as much as possible. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

The younger man rolled down his window to receive the approaching Williams “to see what he wanted.” Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 

The defense team expects to receive all of the documents and evidence in the coming week. The Strange Case of the Christian Zionist Terrorist |Creede Newton |December 14, 2014 |DAILY BEAST 

And I was lucky enough to receive an invitation to stay at Easter Elchies House, the spiritual home at The Macallan. A Whisky Connoisseur Remembers That First Sip of The Macallan | |December 10, 2014 |DAILY BEAST 

The Authorised Version has: “And as a mother shall she meet him, and receive him as a wife married of a virgin.” Solomon and Solomonic Literature |Moncure Daniel Conway 

General Lachambre, as the hero of Cavite, followed to receive the applause which was everywhere showered upon him in Spain. The Philippine Islands |John Foreman 

The wisdom of a scribe cometh by his time of leisure: and he that is less in action, shall receive wisdom. The Bible, Douay-Rheims Version |Various 

Then both the partners laughed together—pleasantly and cheerfully, as men who are going to receive money, often do. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Various matters mentioned by the governor receive perfunctory and formal answers. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various