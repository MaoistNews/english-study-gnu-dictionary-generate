With tens of millions across the US currently unemployed, the number of residents with anything close to an ample emergency nest egg has likely dropped even lower. Hurricane Laura is the strongest storm to hit Louisiana in more than a century |Sara Chodosh |August 28, 2020 |Popular-Science 

Most creatures, most of the time, find it easier to do the latter than the former, which is why living things generally are more concerned with feathering their nests than de-feathering those of others. Just Because It’s Natural Doesn’t Mean It’s Good - Issue 89: The Dark Side |David P. Barash |August 19, 2020 |Nautilus 

For example, some of them sat on eggs in open nests, like modern birds. Early dinosaurs may have laid soft-shelled eggs |Jack J. Lee |August 3, 2020 |Science News For Students 

They also worry that the investigation could be used to build up his own nest of public funds and could be used to resurrect the controversial $470 million desalination project in Playas de Rosarito. Border Report: Surviving in Tijuana Has Gotten Even Harder for Haitian Migrants |Maya Srikrishnan |July 20, 2020 |Voice of San Diego 

This could include whole nests of honeybees, other types of hornets and yellow jackets. What you need to know about ‘murder hornets’ |Susan Milius |July 20, 2020 |Science News For Students 

Unlike Brunner, Remer was itinerant, and spent much time in that other nest of postwar Nazis—Cairo. Hitler’s Henchmen in Arabia |Guy Walters |December 7, 2014 |DAILY BEAST 

Mark Reay is a handsome model-turned-photographer who is homeless, living in a secret ‘nest’ on top of an apartment building. This Fashion World Darling Is Homeless |Erica Wagner |December 2, 2014 |DAILY BEAST 

And an eaglet does not start off flying from the ground, but from the nest. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

When he approached the wrecked nest, Patterson saw one of the eaglets on the exposed ground near the base of the tree. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

He then spoke to the state police, who proved to be aware of the fallen nest. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

A small book, bound in full purple calf, lay half hidden in a nest of fine tissue paper on the dressing-table. Hilda Lessways |Arnold Bennett 

Almost as soon as she had finished building her nest she had discovered a strange-looking egg there. The Tale of Grandfather Mole |Arthur Scott Bailey 

The grass had a delightful fragrance, like new-mown hay, and was neatly wound around the tunnel, like the inside of a bird's-nest. Davy and The Goblin |Charles E. Carryl 

And away she flew to her nest, leaving Grandfather Mole to talk to the air, if he wished. The Tale of Grandfather Mole |Arthur Scott Bailey 

Weimar being such a "kleines Nest (little nest)," as Liszt calls it, every stranger is immediately remarked. Music-Study in Germany |Amy Fay