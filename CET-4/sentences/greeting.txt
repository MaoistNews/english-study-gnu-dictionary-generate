Whether you buy a pack of these bread-shaped greeting cards or attach one to a gift, Food for Thoughts donates the cash equivalent of one peanut butter and jelly sandwich to hunger-fighting organizations. Gifts that keep on giving: These retailers offer one-for-one deals or make financial donations |Laura Daily |November 30, 2020 |Washington Post 

Everyone being stuck inside and parents likely distracted by work is a recipe for kids getting into trouble — and now the holiday season, with its battery-filled gifts and even greeting cards, threatens to intensify the situation still further. Stop Eating Batteries Already |Fiona Zublin |November 27, 2020 |Ozy 

She added that nearly 300 Hawks employees were helping out by filling roles as varied as greeting voters and serving as poll workers alongside county employees. Take Me Out To The Ballot Box |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |November 3, 2020 |FiveThirtyEight 

At times I do not have the time to shop for a get well, sympathy or other type of greeting card. Hints From Heloise: Let it grow, then let it go |Heloise Heloise |October 30, 2020 |Washington Post 

Since then, several calls and texts to my sister and brother-in-law to extend birthday wishes — only messages with birthday greetings were left — have gone unanswered. Carolyn Hax: It takes two to tango or feud, so just dance around bride’s snub |Carolyn Hax |October 30, 2020 |Washington Post 

Or, after a surprise party greeting for Gloria goes awry, “Just a thought: maybe we should stop doing these.” Stop Hating on ‘Modern Family’ (But Also Stop Giving It Emmys) |Jason Lynch |October 15, 2014 |DAILY BEAST 

Clinton, meanwhile, spent several minutes greeting audience members along the rope line and posing for cellphone selfies. Hillary Clinton Basks in Labor’s Love: ‘This Is Like a Homecoming!’ |David Freedlander |September 16, 2014 |DAILY BEAST 

Later she explains why typical forms of greeting in Sierra Leone, such as shaking hands and hugging, are no longer acceptable. Courageous Filmmakers Are Fighting Ebola On Screen |Abby Haglage |August 20, 2014 |DAILY BEAST 

According to the recipient of his greeting, Patreese Johnson, he said, “I want some of that” and motioned to her crotch. ‘Out in the Night’ and the Redemption of the ‘Killer Lesbian Gang' |Nina Strochlic |June 21, 2014 |DAILY BEAST 

They accuse old friends and colleagues of terrible things, even if they do something simple like return a greeting on Facebook. Iran Says Take Off the Veil—and Be Raped |IranWire |June 9, 2014 |DAILY BEAST 

Tressan advanced to meet him, a smile of cordial welcome on his lips, and they bowed to each other in formal greeting. St. Martin's Summer |Rafael Sabatini 

In their greeting there seemed a taunting note as though they knew they had no more to fear from me and could be generous. The Soldier of the Valley |Nelson Lloyd 

Garnache bowed to the lady, who returned his greeting by an inclination of the head, and his keen eyes played briskly over her. St. Martin's Summer |Rafael Sabatini 

But the next moment I found I was mistaken, for Ethne was holding out both hands to me in greeting. Uncanny Tales |Various 

She could only hear Beaudelet's voice; Robert had apparently not even spoken a word of greeting to his companion. The Awakening and Selected Short Stories |Kate Chopin