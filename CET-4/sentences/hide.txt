Those applied to deer hide displayed much in common with the business end of the ancient stone tool, including a wavy surface and clusters of shallow grooves. The oldest known abrading tool was used around 350,000 years ago |Bruce Bower |January 21, 2021 |Science News 

The last reported sighting was in 1953, and blue tigers were soon the stuff of legends, with not so much as a preserved hide to prove they ever existed. How Neutral Theory Altered Ideas About Biodiversity |Christie Wilcox |December 8, 2020 |Quanta Magazine 

The hide is roughly twice the thickness of most leather gloves, providing top-notch protection for whatever type of task you want to do. The Gear Our Editors Loved in November |The Editors |December 4, 2020 |Outside Online 

Other stone tools and a pigment chunk buried with her likely were used to cut apart game and prepare hides. This prehistoric woman from Peru hunted big game |Bruce Bower |December 2, 2020 |Science News For Students 

Dixon says they already had bored their way through the tough alligator hide. Whales get a second life as deep-sea buffets |Stephen Ornes |October 15, 2020 |Science News For Students 

He does not hesitate to hide some Marxist books from her library because she fears that the military could use them against her. How Pope Francis Became the World’s BFF |Jason Berry |December 21, 2014 |DAILY BEAST 

Don't hide behind meaningless rhetoric or claim you're ready for action only to back off when the NRA comes knocking. A Navy Vet’s Case for Gun Control |Shawn VanDiver |November 23, 2014 |DAILY BEAST 

And he scarcely bothered to hide his chief ambition: to lead his country as prime minister. Boris Johnson’s Churchill Man Crush |Michael F. Bishop |November 22, 2014 |DAILY BEAST 

They carefully scanned open windows along the route, looking for places where a shooter might hide. Honoring The Late John Doar, A Nearly Forgotten Hero Of The Civil Rights Era |Gary May |November 15, 2014 |DAILY BEAST 

You can hide your extreme views and duck from having to answer questions about them. Why You Can’t Tell the Truth About Race |Michael Tomasky |November 3, 2014 |DAILY BEAST 

Poor Squinty ran and tried to hide under the straw, for he knew the boy was talking about him. Squinty the Comical Pig |Richard Barnum 

Instinctively he tried to hide both pain and anger—it could only increase this distance that was already there. The Wave |Algernon Blackwood 

Consult not with him that layeth a snare for thee, and hide thy counsel from them that envy thee. The Bible, Douay-Rheims Version |Various 

As Isabel walked carefully down the slippery stair she veiled her eyes to hide the wonder in them. Ancestors |Gertrude Atherton 

Even the stern, inflexible commander turned to hide an emotion he would have blushed to betray. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various