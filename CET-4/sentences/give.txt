But give the Kingdom credit for its sense of mercy: The lashes will be administered only 50 at a time. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

With chemotherapy, her doctors give her at least an 80 percent chance of survival. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 

One of the other cops fired three times and those who were still able to give chase did. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Our fans have seen all our sketches, so we wanted to give them something a little deeper about each character. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Could the (thus far) timid trembling give way to a full-on, grand mal seizure? 26 Earthquakes Later, Fracking’s Smoking Gun Is in Texas |James Joiner |January 7, 2015 |DAILY BEAST 

Bacteria, when present in great numbers, give a uniform cloud which cannot be removed by ordinary filtration. A Manual of Clinical Diagnosis |James Campbell Todd 

Give a sweet savour, and a memorial of fine flour, and make a fat offering, and then give place to the physician. The Bible, Douay-Rheims Version |Various 

Finally, let me ask the general reader to put aside all prejudice, and give both sides a fair hearing. God and my Neighbour |Robert Blatchford 

If you have any thoughts of influencing me or my men to join the regular Confederate army, you may as well give up the idea. The Courier of the Ozarks |Byron A. Dunn 

It was he who first said, If thine enemy hunger give him food, if he thirst give him drink. Solomon and Solomonic Literature |Moncure Daniel Conway