It requires that likely answers be anticipated, that elaborate “if/then” scenarios be war-gamed. David Frost and the Art of the Interview |David Frum |September 7, 2013 |DAILY BEAST 

If they abused that power, it would be noticed-if not by the press, then by their colleagues, and if noticed, then punished. Memo: The Aaron Sorkin Model of Political Discourse Doesn't Actually Work |Megan McArdle |April 23, 2013 |DAILY BEAST 

Seems like people in dangerous neighborhoods are braver and trust in God more than people in safer neighborhoods-if you ask me. 'Let's Talk About Guns' Live Chat Transcript |David Frum |January 25, 2013 |DAILY BEAST 

That is, when things are going well, no one feels compelled to spend money on a what-if project. What to Do With the Thousands Displaced by Hurricane Sandy? |Malcolm Jones |November 8, 2012 |DAILY BEAST 

Andrew hopes he sees in this division the seeds of a "return to sanity" if/after Romney loses. Conservative Schism? Not Getting My Hopes Up |Michael Tomasky |September 18, 2012 |DAILY BEAST 

If-itty-teshi-mow Jays Haddee ny up-plo-now-shi-buh nays;ha! The Book of Humorous Verse |Various 

It is a succession of short notes rapidly uttered, as if the bird said "if-if-if-if-if-if-if." A Year in the Fields |John Burroughs 

By all the gods!if he had only known Who held him between life and death upon the Mauvais Pas! The Sorrows of Satan |Marie Corelli 

If-n Mars Andy wanted to do any thing all Hell couldn't keep him from doin' it. Slave Narratives: A Folk History of Slavery in the United States |Work Projects Administration 

Sir S. returned the nod stiffly, with an "I-wonder-if-I-really-do know-you,-or-if-this-is-a-trick-to-claim-acquaintance?" The Heather-Moon |C. N. Williamson and A. M. Williamson