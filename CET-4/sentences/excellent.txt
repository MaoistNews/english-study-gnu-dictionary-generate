Though organic cotton can be a little more expensive, it’s excellent for all skin types, including those who are more sensitive. Best weighted blanket: Sleep like a baby with our comfy bedding picks |PopSci Commerce Team |February 12, 2021 |Popular-Science 

Just because you—like me—waited until the last minute to get a Valentine’s Day gift for your significant other doesn’t mean it can’t be excellent. Outdoorsy Gear Guy–Approved Valentine's Day Gifts |Joe Jackson |February 10, 2021 |Outside Online 

Only new closer Brad Hand was just as excellent as usual in 2020, leading the American League in saves. The Nationals, like the rest of us, want to forget about 2020. That might be pretty smart. |Thomas M. Boswell |February 10, 2021 |Washington Post 

Duplex, for example, in its early days came under criticism for how its excellent quality might actually be deceptive, because it wasn’t clear to users they were speaking to a machine logging their responses in a data harnessing exercise. Infinitus emerges from stealth with $21.4M for ‘voice RPA’ aimed at healthcare companies |Ingrid Lunden |February 4, 2021 |TechCrunch 

That means future Fords will feature Sync systems that use Google Maps and Android's excellent voice recognition, as well as opening up the platform to third-party developers. Ford is switching to Android OS for infotainment in 2023 |Jonathan M. Gitlin |February 1, 2021 |Ars Technica 

That's fine—excellent TV shows are snubbed all the time by these awards organizations. ‘The Comeback’ Finale: Give Lisa Kudrow All of the Awards |Kevin Fallon |December 29, 2014 |DAILY BEAST 

But truth be told, I never came close to mastering the language despite my excellent grades. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

As this excellent piece in Mother Jones describes, however, Holsey had outrageously poor representation during his trial. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

The text amply surveys the various cultural exponents of tattooing accompanied by excellent images. The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 

In his 2010 evaluation, Wright was praised for “excellent knowledge of RRC rules, regulations and policies.” Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

We have been in the profession some years, Mr. Pickwick, and have been honoured with the confidence of many excellent clients. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

I was a little riled at first myself, but the second and last lady who came out put me in excellent humor. Glances at Europe |Horace Greeley 

She played very well, keeping excellent waltz time and infusing an expression into the strains which was indeed inspiring. The Awakening and Selected Short Stories |Kate Chopin 

Excellent as he was as cavalry commander in the field, Murat had no head for great combinations. Napoleon's Marshals |R. P. Dunn-Pattison 

This alone could hinder the execution of his appointment, for in other things he has excellent qualifications for the dignity. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various