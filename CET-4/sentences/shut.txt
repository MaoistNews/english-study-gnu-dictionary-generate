Instead, insurers refused to pay, and Century 21 is shutting down after almost 60 years in business. Got interruption insurance? These companies found it’s useless in the age of COVID-19 |Bernhard Warner |September 12, 2020 |Fortune 

A day after Shanghai Disneyland’s closure, Hong Kong Disneyland shut down. How the coronavirus outbreak is roiling the film and entertainment industries |Alissa Wilkinson |September 11, 2020 |Vox 

Then on Sunday, Mayor London Breed shut down not just the Great Highway but the parking lots along Ocean Beach. The Climate Crisis Is Happening Right Now. Just Look at California’s Weekend. |by Elizabeth Weil |September 9, 2020 |ProPublica 

Heeding the advice of public health experts, they quickly shut down businesses, enacted shelter-in-place orders, and built up testing and contact tracing capacity. In defense of California |James Temple |September 4, 2020 |MIT Technology Review 

Major Hollywood movie studios largely shut down film production in March. Will ‘Tenet’ revive U.S. movie theaters as it’s finally released? |dzanemorris |September 3, 2020 |Fortune 

And the series was implausibly shut out by both the Golden Globe and SAG Awards. ‘The Comeback’ Finale: Give Lisa Kudrow All of the Awards |Kevin Fallon |December 29, 2014 |DAILY BEAST 

A train had actually arrived at the station but its doors were already shut. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

“I was really sexually inexperienced, really sexually shut down,” she explained. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

And so it was that the federal government did not shut down just when we all had visions of sugar plumbs dancing in our heads. Congress’ Gift That Keeps on Giving |P. J. O’Rourke |December 20, 2014 |DAILY BEAST 

Human trials of the Ebola vaccine have been temporarily shut down due to adverse side effects. Uh Oh: Ebola Vaccine Trials Stop |Leigh Cowart |December 19, 2014 |DAILY BEAST 

Another crash, which nearly shut up his spine like a telescope, told him that there were no wings. The Giant of the North |R.M. Ballantyne 

After his death crowds flocked to his grave to touch his holy monument, till the authorities caused the church yard to be shut. The Every Day Book of History and Chronology |Joel Munsell 

The gnarled hands shut up into clenched fists, and the feeble voice trailed off in an agonized moan. Raw Gold |Bertrand W. Sinclair 

He shut his fist and hit Butterface a weak but well intended right-hander on the nose. The Giant of the North |R.M. Ballantyne 

The Professor went straight home and shut himself up in his study. Uncanny Tales |Various