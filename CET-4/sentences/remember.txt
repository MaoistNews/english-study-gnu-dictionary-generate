I remember when he started in politics, and I used to live on 9th and Franklin, and I'd see him. The Trailer: The First State goes last |David Weigel |September 15, 2020 |Washington Post 

There is no end to 2020 that will lead us to remember it as a great year, but when an entire year can be compared to a colonoscopy, a couple months of “not so bad” could do us all a lot of good. How 2020 is like a colonoscopy |jakemeth |September 10, 2020 |Fortune 

“This is not OK,” Peter remembers saying to Potts at the time. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

Roark remembers sitting in his designated driver’s car, waiting for a friend in the backseat for several minutes, when a flashlight shined into the backseat. Those Ticketed for Seditious Language Say Their Only Crime Was Talking Back |Kate Nucci |September 9, 2020 |Voice of San Diego 

That larger network makes it easier to learn and remember things. Top 10 tips on how to study smarter, not longer |Kathiann Kowalski |September 9, 2020 |Science News For Students 

The plan is to stretch it out as long as possible, then probably forget about it, and then suddenly remember it. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

I remember H. Jon Benjamin told me it was a way-too-late apology for Hiroshima and Nagasaki. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Lacey Noonan's A Gronking to Remember makes 50 Shades of Grey look like Madame Bovary in terms of its literary sophistication. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

And with stand-ups, I remember liking George Carlin and Steve Martin. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

I remember all our music appeared on Spotify overnight, without anybody asking us. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

Give not up thy heart to sadness, but drive it from thee: and remember the latter end. The Bible, Douay-Rheims Version |Various 

And remember it is by our hypothesis the best possible form and arrangement of that lesson. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Hasten the time, and remember the end, that they may declare thy wonderful works. The Bible, Douay-Rheims Version |Various 

We have to remember that his daily life, where the home is orderly, helps to impress on him regularity of form. Children's Ways |James Sully 

Henceforth he must remember Winifred only when his sword was at the throat of some wretched mutineer appealing for mercy. The Red Year |Louis Tracy