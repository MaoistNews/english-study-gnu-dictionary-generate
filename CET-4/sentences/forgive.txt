Rais Bhuiyan survived one of Stroman’s shootings and the book explores his quest to forgive. Calendar: Sept. 4-10 |Philip Van Slooten |September 4, 2020 |Washington Blade 

Instead, the heroes held up in media are the family members and activists who forgive their attackers or retain faith in the political system. Kenosha’s looting is a symptom of a decrepit democracy |Aaron Ross Coleman |September 4, 2020 |Vox 

You can be forgiven for not knowing what to make of Tesla’s amazing run and the battling views on whether it can keep racing. Tesla has a business model problem: It can never justify its current stock price by simply making cars |Shawn Tully |August 29, 2020 |Fortune 

A popular sentiment on social media suggests you’re either willing to forgive or overlook the rioting or you’re not really with the protesters. Violent protests against police brutality in the ’60s and ’90s changed public opinion |German Lopez |August 28, 2020 |Vox 

As of today, it has been 131 days since the NBA last conducted a regular-season game, so you’d be forgiven for not necessarily recalling the particulars of where things left off. Who’s Who In The NBA Bubble: The Teams Just Along For The Ride |Jared Dubin |July 20, 2020 |FiveThirtyEight 

When a popular Sunni televangelist does it, to forgive is divine. Disco Mullah Blasphemy Row Highlights Pakistan’s Hypocrisy |Shaheen Pasha |December 21, 2014 |DAILY BEAST 

I do not forgive those who saw the attacks and have refused to cooperate with law enforcement. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

In a blog post titled “im sorry,” he wrote only “please forgive me.” School Shooters Love This Pickup Artist Website |Brandy Zadrozny |December 5, 2014 |DAILY BEAST 

It turns out audiences will forgive such reports when your movies are good. It’s Time to Stop Hating Katherine Heigl |Kevin Fallon |November 17, 2014 |DAILY BEAST 

In the Church of Sorkin, this is the prayer of humility: forgive them, for they know not what they do. ‘Newsroom’ Premiere: Aaron Sorkin Puts CNN on Blast Over the Boston Bombing |Kevin Fallon |November 10, 2014 |DAILY BEAST 

But "the cards never forgive," and as a rule Dame Fortune is relentless to the reckless player. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

If I don't hear from you very decisively to the contrary, I shall come, and trust to your good nature to forgive it. Elster's Folly |Mrs. Henry Wood 

And man hath bowed himself down, and man hath been debased: therefore forgive them not. The Bible, Douay-Rheims Version |Various 

Nor would it be just for you to forgive him because another son of yours was willing to be punished in his stead. God and my Neighbour |Robert Blatchford 

“You must forgive her, Mrs. Foley,” Jessie said, coming down to meet the woman and taking the baby from her. The Campfire Girls of Roselawn |Margaret Penrose