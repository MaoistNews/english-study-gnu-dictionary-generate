It’s a mathematical problem layered with a huge element of uncertainty around what will be the right thing to do in terms of safety and health. Publishers look to allay employee fears over a return to the office by offering additional flexibility |Jessica Davies |February 10, 2021 |Digiday 

They’ve been capitalizing on a recent mathematical advance that has injected new life into a millennia-long quest to identify shapes that can perfectly fill, or tile, three-dimensional space. Undergraduates Hunt for Special Tetrahedra That Fit Together |Kevin Hartnett |February 9, 2021 |Quanta Magazine 

“These are idealized mathematical objects that are going to be with us forever, and now we know all of them,” said Martin Weissman of the University of California, Santa Cruz. Tetrahedron Solutions Finally Proved Decades After Computer Search |Kevin Hartnett |February 2, 2021 |Quanta Magazine 

The mathematical disease modeler Joseph Wu, a member of the University of Hong Kong’s infectious disease and public health teams, was looking closely at the numbers of cases being reported in and around Wuhan. The Hard Lessons of Modeling the Coronavirus Pandemic |Jordana Cepelewicz |January 28, 2021 |Quanta Magazine 

The third, however, can be viewed through a mathematical lens. Topology 101: The Hole Truth |David S. Richeson |January 26, 2021 |Quanta Magazine 

It takes Sharp four hours to get into character: “I take joy in the mathematical, symmetrical precision and perfectness of Bach.” The Brit Who Stormed Broadway |Tim Teeman |December 7, 2014 |DAILY BEAST 

He majored in mathematical physics, studying mind-bending theories of quantum mechanics and partial differential equations. How the NSA Became a Killing Machine |Shane Harris |November 9, 2014 |DAILY BEAST 

To my mathematical brain, the numbers alone make thinking about aliens perfectly rational. The Other Side of Stephen Hawking: Strippers, Aliens, and Disturbing Abuse Claims |Marlow Stern |November 6, 2014 |DAILY BEAST 

Kempton subsequently offered what he reasoned to be mathematical proof of her powers. Tupac and Murray Kempton: The Godfather Who Wore Tweed |Michael Daly |June 22, 2014 |DAILY BEAST 

All Facebook will see is cyphertext—the mathematical gibberish computers generate to thwart spying eyes. Crypto for the Masses: Here’s How You Can Resist the NSA |Quinn Norton |May 12, 2014 |DAILY BEAST 

A second main division of our schooling was mathematical instruction of a sort. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

This truth is as old as Homer, and its proofs are as capable of demonstration as a mathematical axiom. The Book of Anecdotes and Budget of Fun; |Various 

Upon reflection, it is surprising how few subjects are capable of a mathematical demonstration. Gospel Philosophy |J. H. Ward 

There is not a mathematical line—length without breadth—in all the universe. Gospel Philosophy |J. H. Ward 

With mathematical accuracy the old man filled the two tumblers with boiling water. The Fifth String |John Philip Sousa