People from those ports acquired a reputation among the Portuguese as the best hands for mining gold. How African body markings were used to construct the idea of race in colonial Brazil |Aldair Rodrigues |January 22, 2021 |Quartz 

It plans to expand into Mexico and to bring on Portuguese instruction. Henry picks up cash to be a Lambda School for Latin America |Natasha Mascarenhas |December 3, 2020 |TechCrunch 

The Portuguese post office conducted some kind of investigation on the card. Not getting any travel postcards? A project in Mali delivers delight — and a lifeline. |Bailey Berg |December 3, 2020 |Washington Post 

Ronaldo, whom Portuguese soccer officials said had no covid-19 symptoms, called Spadafora’s claim “a lie” in an Instagram Live video posted from his home. Cristiano Ronaldo to miss Champions League game vs. Barcelona weeks after positive coronavirus test |Matt Bonesteel |October 28, 2020 |Washington Post 

It didn’t help that Portugal’s foreign affairs minister, Rui Machete, had publicly withdrawn his apology to dos Santos under pressure from the Portuguese public. Is It Portugal’s Turn to Gain ‘Freedom’ From Angola? |Eromo Egbejule |September 28, 2020 |Ozy 

In 2008, the Portuguese police did clear the McCanns of any wrongdoing and closed the case. Does a Perv Know Maddie McCann’s Fate? |Barbie Latza Nadeau |October 16, 2014 |DAILY BEAST 

In October 2013, the Portuguese police reopened the case in Portugal, paving the way for further investigations by Scotland Yard. Does a Perv Know Maddie McCann’s Fate? |Barbie Latza Nadeau |October 16, 2014 |DAILY BEAST 

I still have the beautiful Portuguese wire cage that the love birds came in. Vogue Photographer Erwin Blumenfeld: Secrets of a Fashion Legend |Tim Teeman |September 14, 2014 |DAILY BEAST 

The city was founded in 1471 as a base for Moroccans to fight off the invading Portuguese, who occupied the coastal areas. Morocco's Secret All-Blue City |Nina Strochlic |August 28, 2014 |DAILY BEAST 

The goal came off his tummy inside the Portuguese box, an apt way to score for a very gutsy player. Team USA 2, Portugal 2: Seconds Away From World Cup Glory |Tunku Varadarajan |June 23, 2014 |DAILY BEAST 

The Portuguese frigate Cine captured by the Algerines, after a smart action. The Every Day Book of History and Chronology |Joel Munsell 

Portuguese snuff seemed to be in favor and was delicately perfumed. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The circumstances of Spanish and Portuguese America were very different in every stage. Journal of a Voyage to Brazil |Maria Graham 

The revenge of the Portuguese was horrible, the Cahetes were hunted, slaughtered, and all but exterminated. Journal of a Voyage to Brazil |Maria Graham 

I cannot pretend to speak of the character or measures of these or any other Portuguese or Brazilian ministers. Journal of a Voyage to Brazil |Maria Graham