Another dataset, Dave Leip’s Atlas of Elections, enabled the researchers to evaluate each party’s share of votes per county in midterm and presidential elections. Mandatory mail-in voting hurts neither Democratic nor Republican candidates |Sujata Gupta |August 26, 2020 |Science News 

They have traditionally been funded by county resources and depend on volunteers. Hundreds of Thousands of Nursing Home Residents May Not Be Able to Vote in November Because of the Pandemic |by Ryan McCarthy and Jack Gillum |August 26, 2020 |ProPublica 

The city of San Diego can nearly call the shots at SANDAG all on its own, since cities representing a majority of county residents can overrule any decision made by the rest of the board. Politics Report: Who Will Get the Midway Rose? |Scott Lewis and Andrew Keatts |August 15, 2020 |Voice of San Diego 

Ivey, however, like the county, cited the emergency as the reason for not immediately providing records. We’re Suing for COVID-19 Data |Scott Lewis and Jesse Marx |August 14, 2020 |Voice of San Diego 

He wanted the county representative to see this neatly ordered world he created, so that such a program might be brought to other schools districts across California. The Learning Curve: One School District Stayed Open – and Didn’t Have Problems |Will Huntsberry |August 13, 2020 |Voice of San Diego 

“Please, please do not permit this to happen here in Florida,” wrote Cris K. Smith of East Polk County. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

As of this afternoon, gay Floridians can get married in Dade County. The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

Will Christian pharmacists, county clerks, florists, and for-profit wedding chapels really withdraw from society, as you describe? Do LGBTs Owe Christians an Olive Branch? Try The Other Way Around |Jay Michaelson |December 14, 2014 |DAILY BEAST 

Kocurek became especially frustrated with a commercial waste facility in Jim Wells County. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

Sonoma County district attorney Jill Ravitch announced in July that she would not bring criminal charges against Gelhaus. Worse Than Eric Garner: Cops Who Got Away With Killing Autistic Men and Little Girls |Emily Shire |December 4, 2014 |DAILY BEAST 

The taxes whether public, county, or parish, were payable in tobacco. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He had the land in Tripp County that was broken into winter wheat, while that in the next county east was rented. The Homesteader |Oscar Micheaux 

Better wait till these county nobs have cleared, I suppose—there goes the last of 'em—now for it! Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

A destructive tornado swept over a portion of Lapeer county, Michigan. The Every Day Book of History and Chronology |Joel Munsell 

Samuel Dale, an eminent pioneer in the settlement of the southwest, died in Lauderdale county, Mississippi. The Every Day Book of History and Chronology |Joel Munsell