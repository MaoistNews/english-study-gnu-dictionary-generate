She agreed several times that “no one is above the law,” but she warned that the Supreme Court has no real recourse to ensure that people, including the president, obeyed its orders. To Democrats’ frustration, GOP predicts clear sailing as Barrett testimony ends |Robert Barnes, Seung Min Kim, Ann Marimow |October 15, 2020 |Washington Post 

“The Supreme Court can’t control what the president obeys,” she said flatly. Barrett confirmation hearing day three: Barrett declines to say whether it’s wrong to separate migrant children from parents |Derek Hawkins, Seung Min Kim, Ann Marimow, Karoun Demirjian |October 14, 2020 |Washington Post 

If they obey and seem comfortable, there’s a good chance your pup will survive the day without you. How to help your pet with separation anxiety |Sara Kiley Watson |August 26, 2020 |Popular-Science 

They provide continuous information, so that the population can have real-time updates, and then believe in the government and strictly obey all their guidelines. Vietnam’s covid hospital |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

The strong force operating between quarks obeys very complicated rules—so complicated, in fact, that usually the only way to calculate its effects is to use approximations and supercomputers. CERN: Physicists Report the Discovery of Unique New Particle |Harry Cliff |July 15, 2020 |Singularity Hub 

Those advocating justice should first obey the rule of law themselves. It’s Time to Hold Protesters Accountable |Ron Christie |December 4, 2014 |DAILY BEAST 

This argument is vital to a larger argument: Do we obey the rules set up to constrain government or not? Obama’s ISIS War Is Illegal |Sen. Rand Paul |November 10, 2014 |DAILY BEAST 

THE DISHONOR OF HONOR KILLINGS Imagine a young woman killed by her own relatives for failing to obey. Lone Star Leaders | |October 24, 2014 |DAILY BEAST 

No soldier is obliged to obey a law contrary to the law of God. Why Pope Francis Wants to Declare Murdered Archbishop Romero a Saint |Christopher Dickey |August 24, 2014 |DAILY BEAST 

I continue to live here, work here, pay the taxes and obey the law. Men Without a Country: Mike Brown, Trayvon Martin, My Father and Me |Arthur Chu |August 12, 2014 |DAILY BEAST 

The sepoys refused to obey, and the sowars, drawing their pistols, shot dead or severely wounded six British officers. The Red Year |Louis Tracy 

At the Flagstaff Tower the 74th and the remainder of the 38th suddenly told their officers that they would obey them no longer. The Red Year |Louis Tracy 

He felt very sorry for the Temecula people, the sheriff did; but he had to obey the law himself. Ramona |Helen Hunt Jackson 

He understood, and would obey; but his eyes followed her wistfully till she disappeared from sight. Ramona |Helen Hunt Jackson 

When he was only three years old the huge creature would obey him and allow him to drive anywhere he pleased. Alila, Our Little Philippine Cousin |Mary Hazelton Wade