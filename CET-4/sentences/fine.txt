Target was fined for not enforcing the county’s mask law and citations were mailed to the protesters, the South Florida Sun Sentinel reported. Twisted Sister’s Dee Snider does not approve of anti-maskers using ‘We’re Not Gonna Take It’ |radmarya |September 17, 2020 |Fortune 

Now, for most general purposes, stock tires will work fine, especially if you have a winch. Six ways to make your ATV even more rugged |By Tyler Freel/Outdoor Life |September 17, 2020 |Popular-Science 

The state’s environmental regulators are seen as friendly to coal companies, so the reduced fines are in keeping with prior actions. This Billionaire Governor’s Coal Company Might Get a Big Break From His Own Regulators |by Ken Ward Jr. |September 17, 2020 |ProPublica 

Repeating the process, he smoothed the lattice’s fine details, zooming out to grok the system’s overall behavior. How Mathematical ‘Hocus-Pocus’ Saved Particle Physics |Charlie Wood |September 17, 2020 |Quanta Magazine 

So let’s see what happens, but I think it’s going to work out fine. Timeline: The 124 times Trump has downplayed the coronavirus threat |Aaron Blake, JM Rieger |September 17, 2020 |Washington Post 

As this list shows, punishments typically run to a short-ish jail sentence and/or a moderately hefty fine. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

Hey, whatever keeps those lecherous freaks from sexually assaulting humans is fine by us. Zebra Finches, Dolphins, Elephants, and More Animals Under the Influence |Bill Schulz |December 31, 2014 |DAILY BEAST 

There are instances in which private rehoming works out fine and is the best solution for the struggling family and the children. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 

That's fine—excellent TV shows are snubbed all the time by these awards organizations. ‘The Comeback’ Finale: Give Lisa Kudrow All of the Awards |Kevin Fallon |December 29, 2014 |DAILY BEAST 

The exhibit also includes examples of designers borrowing from fine art, as Yves Saint Laurent did with his Mondrian dress. The Big Business of Fashion Counterfeits |Lizzie Crocker |December 24, 2014 |DAILY BEAST 

A small book, bound in full purple calf, lay half hidden in a nest of fine tissue paper on the dressing-table. Hilda Lessways |Arnold Bennett 

A flash of surprise and pleasure lit the fine eyes of the haughty beauty perched up there on the palace wall. The Red Year |Louis Tracy 

Give a sweet savour, and a memorial of fine flour, and make a fat offering, and then give place to the physician. The Bible, Douay-Rheims Version |Various 

His strong legs and his broad, spade-like feet helped to make him a fine swimmer. The Tale of Grandfather Mole |Arthur Scott Bailey 

When the days were fine, Jean in his basket assisted at the dramatic performance in the market-place. The Joyous Adventures of Aristide Pujol |William J. Locke