Center Nicklas Backstrom said the Capitals have grown passive when playing with a lead when aggressiveness is necessary. For first time in three weeks, Capitals could be at full strength Sunday |Samantha Pell |February 11, 2021 |Washington Post 

“Federal funding — together with the region’s commitment — will support the major maintenance and capital rehabilitation activities that are necessary to restore and improve the aging transit system,” the letter said. Metro seeks bond sales to raise $360 million for capital projects |Justin George |February 10, 2021 |Washington Post 

The audit also found that the agency does not keep track of who owes money or how much they owe, and it isn’t taking necessary steps to collect it. Utility Companies Owe Millions to This State Regulatory Agency. The Problem? The Agency Can’t Track What It’s Owed. |by Scott Morris, Bay City News Foundation |February 10, 2021 |ProPublica 

While 84 percent of registered Republicans supported the recall, increased support among Democrats and independents will be necessary for Newsom to lose. California’s Gavin Newsom Will Likely Face A Recall Election — But He’ll Probably Survive It |Geoffrey Skelley (geoffrey.skelley@abc.com) |February 10, 2021 |FiveThirtyEight 

They’ve noted, too, that they would pursue a vote to bar him from federal office if he was convicted by the necessary 67 members of the Senate. The 5 Senate Republicans to watch on impeachment |Li Zhou |February 9, 2021 |Vox 

He could order the Justice Department to begin the necessary regulatory work. Obama’s Pot Policy Is Refer Madness |James Poulos |January 5, 2015 |DAILY BEAST 

Yet, what my peers do not realize – or cannot handle – is that rejection is a necessary part of forging a romantic relationships. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 

But as is her way, Kaling defended why the episode was not only funny, but necessary. Year of the Butt: How the Booty Changed the World in 2014 |Kevin Fallon |December 30, 2014 |DAILY BEAST 

Unfortunately, the underground tunnels that were used to transport booze and, if necessary, escaping patrons, are off-limits. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

However, we have just had a necessary wake-up call that all is not as secure as we believed. A Gift to the Jihadis: The Unseen Airport Security Threat |Clive Irving |December 27, 2014 |DAILY BEAST 

In sorting notes it is necessary to be able readily to distinguish between notes of this bank and notes of other reserve banks. Readings in Money and Banking |Chester Arthur Phillips 

As long as may be necessary, Sam,” replied Mr. Pickwick, “you have my full permission to remain. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

But it was necessary to take Silan, which the rebels hastened to strengthen, closely followed up by the Spaniards. The Philippine Islands |John Foreman 

For this use of the voice in the special service of will-power, or propelling force, it is necessary first to test its freedom. Expressive Voice Culture |Jessie Eldridge Southwick 

It is only necessary to have a zinc, or a galvanized tray on which to stand the glass in an inverted position. How to Know the Ferns |S. Leonard Bastin