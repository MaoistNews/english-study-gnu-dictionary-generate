Elizabeth Hamon Reid, 39VP engineering, Google GeoThe biggest change that I’ve made as a result of the pandemic is that I have to be more intentional in a number of areas. How this year’s 40 Under 40 are surviving the pandemic |jonathanvanian2015 |September 7, 2020 |Fortune 

This intentional, early morning quiet time, while often hard to carve out with two young kids at home, sets the tone for the day and makes me a better wife, mom, leader, and investor. 17 extremely useful productivity tips from this year’s 40 Under 40 |Maria Aspan |September 6, 2020 |Fortune 

These racial disparities, along with evidence suggesting intentional discrimination, erode the community trust that is critical to effective policing. Violent protests against police brutality in the ’60s and ’90s changed public opinion |German Lopez |August 28, 2020 |Vox 

We offer the flexibility to live and work from wherever you want, in nature or in the city, changing locations whenever you want, but at the same time being part of an intentional community. The New Camper Companies Redefining Road Travel |Alex Temblador |August 27, 2020 |Outside Online 

As Alexandra Rojas, executive director of Justice Democrats, told HuffPost, “We’ve been intentional about building infrastructure and an ecosystem that can take on decades worth of the establishment’s.” Progressive Groups Are Getting More Selective In Targeting Incumbents. Is It Working? |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |August 21, 2020 |FiveThirtyEight 

At the moment, the same dynamic is at work, but largely the result of market forces, not intentional policy in Washington. China Is Financing Putin’s Aggression |Gordon G. Chang |November 13, 2014 |DAILY BEAST 

But there is more to this behaviour than intentional amnesia. Beirut Letter: In Lebanon, Fighting ISIS With Culture and Satire |Kim Ghattas |September 22, 2014 |DAILY BEAST 

Surely it was intentional and perpetrated by Assad or ISIS or a still-unrecognized radical group. Measles Vaccine Mix-Up Kills Dozens of Syrian Children |Kent Sepkowitz |September 18, 2014 |DAILY BEAST 

The Estonian statement implied the alleged abduction is an intentional slap in the face to the Americans. Russia Steps Up Pressure on the Baltics |Will Cathcart |September 5, 2014 |DAILY BEAST 

To McCain, that amounts to an American alliance with those countries, whether that was intentional or not. Obama Admin Debates Whether Assad Really Must Go |Josh Rogin |July 3, 2014 |DAILY BEAST 

He loathed himself for submitting to her cruelty, for it was intentional cruelty—she made him writhe and suffer of set purpose. The Wave |Algernon Blackwood 

The term malice means something more than "the intentional doing of a wrongful act to the injury of another without legal excuse." Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

According to him, literature ought to be intentional, and the accidental restrained as much as possible. Charles Baudelaire, His Life |Thophile Gautier 

And for two years he had without intentional selfishness kept Sara Lee for himself. The Amazing Interlude |Mary Roberts Rinehart 

It was more ferocious than the merely brutal glare of a tiger; it was an intentional malignity, super-beastly and sub-human. Overland |John William De Forest