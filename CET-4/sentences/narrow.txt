Even those who were in the Capitol that day had only a narrow sense of what happened. I was skeptical of Democrats pursuing a second impeachment. I was wrong. |Karen Tumulty |February 11, 2021 |Washington Post 

Google previously added a menu at the top to let you filter based on those alternate meanings and better narrow down the search results to what you are looking for. Google Image Search update reduces duplicate image results |Barry Schwartz |February 11, 2021 |Search Engine Land 

As we noted earlier, the research subjects — college students and MTurk users — they represent narrow slices of the population. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

Her terror is played out for entertainment, whether that means a narrow escape or a bloody death. Capitol rioters searched for Nancy Pelosi in a way that should make every woman’s skin crawl |Monica Hesse |February 11, 2021 |Washington Post 

I think that on both sides of the aisle, you see these entrenched politicians, this entrenched political class who have a very narrow viewpoint of how the world should act. When Madison Cawthorn Felt ‘Invisible’ |Eugene Robinson |February 9, 2021 |Ozy 

What it endangers is a narrow conception of Russian power, understood through the eyes of its dictatorial leader. Oliver Stone’s Latest Dictator Suckup |James Kirchick |January 5, 2015 |DAILY BEAST 

By that time, SantaCon had already spread beyond the narrow confines of a few prankster-explorers. Before the Bros, SantaCon Was as an Anti-Corporate Protest |David Freedlander |December 12, 2014 |DAILY BEAST 

A cruiser shows up and eyes narrow and citizens often withdraw. The Wildly Peaceful, Human, Almost Boring, Ultimately Great New York City Protests for Eric Garner |Mike Barnicle |December 8, 2014 |DAILY BEAST 

And besides, the studies that do enjoy widespread media circulation focus on a very narrow segment of the LGBT community: gay men. The Problematic Hunt for a ‘Gay Gene’ |Samantha Allen |November 20, 2014 |DAILY BEAST 

He has sunken eyes and a narrow black beard speckled with gray. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

The streets here are rather wide for an Italian city but would be deemed intolerably narrow in America. Glances at Europe |Horace Greeley 

He is rather tall and narrow, and wears a long abb's coat reaching nearly down to his feet. Music-Study in Germany |Amy Fay 

As he was toiling slowly up a narrow, rocky pass, he suddenly saw an Indian's head peering over the ledge. Ramona |Helen Hunt Jackson 

Her thin and narrow hands held the balcony railing rather tightly. Bella Donna |Robert Hichens 

When we got to the house we entered an obscure corridor and began to find our way up a dark and narrow staircase. Music-Study in Germany |Amy Fay