Download this guide to explore all the winners of the 2020 Digiday Media Awards Europe. Sky News, Hearst UK and RT are Digiday Media Awards Europe winners |Digiday Awards |August 21, 2020 |Digiday 

Looking through this public art guide, I noticed a number of artworks placed in police stations throughout the city. A Closer Look at the Public Art at Chicago Police Stations |by Logan Jaffe |August 21, 2020 |ProPublica 

Gear Patrol, which bills itself as the definitive buying guide for men, is taking similar steps to increase engagement on its social media channels, primarily on Instagram, in order to introduce audiences to products that it covers on its website. How a new order of commerce is increasingly coming to the rescue for publishers with advertising challenges |Kayleigh Barber |August 17, 2020 |Digiday 

Readers were particularly drawn to deep-dives, explainers and guides around streaming platforms’ catalogs of older shows, Hill said. U.K. entertainment title Digital Spy looks to use Apple News+ to boost global reach |Lara O'Reilly |July 29, 2020 |Digiday 

The made-for-Amazon brand is using most of the tools and techniques of a best in class ad campaign strategy to have, and can serve as a guide if brands don’t know where to start. Deep Dive: How to master Amazon advertising in the new normal |Digiday |July 29, 2020 |Digiday 

I've seen video of that satirical guide to SXSW in 1998 where you asked a bunch of bands odd questions. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

If history is a guide, Huckabee will need to resonate with more than just the faithful if he is to win. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

Objectively, they are not just riding with the tide, but helping to guide its very direction. Corporations Are No Longer Silent on LGBT Issues | |December 24, 2014 |DAILY BEAST 

Add to that the DISH Anywhere app, and you have instant access to the program guide and the ability to record shows on the go. Four TV Shows We Can’t Wait to Return In 2015 |DISH |December 22, 2014 |DAILY BEAST 

In “Cartoons and Cereal,” he sings, “Reminisce when I had the morning appetite/ Apple Jacks, had nothing that I hit the TV Guide.” Cereal Cafe’s Big Bowl of Hate |David Levesley |December 14, 2014 |DAILY BEAST 

This, of course, I always gave to the guide to use in sending the letter when he got to the trading-post. The Boarded-Up House |Augusta Huiell Seaman 

He said something laughingly to the head guide to the effect that climbing was good sport and a fine test for the nerves. Uncanny Tales |Various 

To guide his mind into the channel of the printed exposition, he calls into play the Directory power of the attention. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Taking half a dozen men with him, and compelling the woman to act as guide, he went to the tomb in the dark. The Red Year |Louis Tracy 

Men of science strove to read the riddle of life; to guide and to succour their fellow creatures. God and my Neighbour |Robert Blatchford