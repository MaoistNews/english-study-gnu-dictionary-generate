A physicist turned neuroscientist, Koulakov is working to understand how humans perceive odors and to classify millions of volatile molecules by their “smellable” properties. The Doctor Will Sniff You Now - Issue 95: Escape |Lina Zeldovich |February 3, 2021 |Nautilus 

Varghese Mathai is a physicist at the University of Massachusetts at Amherst who studies the flow of fluids and gases. How to reduce the risk of covid-19 airborne transmission inside a car | |January 31, 2021 |Washington Post 

He’s a physicist at Goethe University in Frankfurt, Germany. Physicists have clocked the shortest time span ever |Maria Temming |January 29, 2021 |Science News For Students 

In the early 1980s, as physicists investigated how space might have started—and stopped—inflating, an unsettling picture emerged. How Universes Might Bubble Up and Collide - Facts So Romantic |Charlie Wood |January 28, 2021 |Nautilus 

In the early 1980s, as physicists investigated how space might have started — and stopped — inflating, an unsettling picture emerged. Physicists Study How Universes Might Bubble Up and Collide |Charlie Wood |January 25, 2021 |Quanta Magazine 

Obama is widely believed to tap an ex-physicist who cuts military waste like a laser to become the next secretary of defense. Ashton Carter, the Wonk Who Would Lead the Pentagon |Shane Harris, Tim Mak |December 2, 2014 |DAILY BEAST 

The renowned theoretical physicist has for years been a proponent of real-life, NASA-led interstellar travel. Meet Kip Thorne, the Man Who Crafted the Artful Science of ‘Interstellar’ |Asawin Suebsaeng |November 14, 2014 |DAILY BEAST 

Even there, the answer is no, as physicist John Baez explains in detail. Dear NASA: Fuel-Free Rocket Thruster Is Literally Too Good to Be True |Matthew R. Francis |August 4, 2014 |DAILY BEAST 

Demicheli also had been a physicist but had switched to oncology research after his wife died of Hodgkin lymphoma in 1976. How Big Pharma Holds Back in the War on Cancer |ProPublica |April 23, 2014 |DAILY BEAST 

Lawrence M. Krauss, a physicist and cosmologist, is Director of the Origins Project at Arizona State University. The New 'Cosmos' Reboot Marks a Promising New Era for Science |Lawrence M. Krauss |March 10, 2014 |DAILY BEAST 

No, the physicist wants to understand those connections of cause and effect as necessary ones. Psychotherapy |Hugo Mnsterberg 

The psychologist's problem of explanation is in one way entirely different from that of the physicist. Psychotherapy |Hugo Mnsterberg 

This is a statement which no present-day physicist would dispute. The Book of Life: Vol. I Mind and Body; Vol. II Love and Society |Upton Sinclair 

I have now to note a resemblance of some interest to the physicist, and of a more settled character than any hitherto observed. Scientific American Supplement, Vol. XV., No. 388, June 9, 1883 |Various 

Nor does the physicist find the laws of mechanics holding good one day and not the next. The Science of Human Nature |William Henry Pyle