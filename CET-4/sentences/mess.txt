The upcoming US presidential election seems set to be something of a mess—to put it lightly. Microsoft’s New Deepfake Detector Puts Reality to the Test |Vanessa Bates Ramirez |September 4, 2020 |Singularity Hub 

Cleanup also has to be simple, because when your vehicle is full, even small greasy messes can create big problems. 5 Portable Grills for All Your Outdoor Cooking Needs |Amy Marturana Winderl |September 2, 2020 |Outside Online 

“I just thought it was extremely odd to, say the least – just a way to mess with the homeless and those that need the discount,” Sheetz said. MTS Frequently Overrules Doctors’ Orders on Reduced Fares for the Disabled |Lisa Halverstadt |August 31, 2020 |Voice of San Diego 

Work and life were colliding in ways that it was impossible to stay organized and looking at the mess while I was watching TV on the couch was making me more stressed out. The best things I bought in August |Rachel Schallom |August 30, 2020 |Fortune 

Make the easy into a festering mess of sweat and cursing and they’ll mosey along. The most secure ways to lock up your bike |By Michael Frank/Cycle Volta |August 26, 2020 |Popular-Science 

Texas has always had a sense of place—that is why we are told not to mess with it. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

It was being the riskiest studio in Hollywood that got Sony into this mess in the first place. Sony: Hollywood’s Most Subversive Studio Under Attack |Marlow Stern |December 23, 2014 |DAILY BEAST 

In fact, that candy store is heavy industry, with all the mess that entails. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

There was The Brittany Murphy Story, dubbed a “colossal mess” by reviewers. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

“The idea was to mess with the concept of Christmas,” recalled John Law, an original Cacophony member. Before the Bros, SantaCon Was as an Anti-Corporate Protest |David Freedlander |December 12, 2014 |DAILY BEAST 

If Wee Willie Winkie took an interest in anyone, the fortunate man was envied alike by the mess and the rank and file. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

And yet they tell me he was a pleasant enough fellow in the Mess, this Brigadier, and liked good cooking. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

Coming back along the well-beaten sandy track, my heart sank to see our mess tent still lit up at midnight. Gallipoli Diary, Volume I |Ian Hamilton 

Coming back found mess tent brilliantly lit up and my staff entertaining their friends. Gallipoli Diary, Volume I |Ian Hamilton 

I shall make an unholy mess of things if I'm left alone, and if you like I'll keep you on here. The Everlasting Arms |Joseph Hocking