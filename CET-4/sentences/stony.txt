In fact, of all underwater environments, the vertical, stony walls that mark the deep-water edges of coral reefs are among the most sublime, colorful, and ancient of Earth’s biologically produced habitats. Twilight of the Nautilus - Issue 104: Harmony |Peter Ward |August 11, 2021 |Nautilus 

Plankton and organic-rich sediments tumbled down the long stony walls of coral reefs. Twilight of the Nautilus - Issue 104: Harmony |Peter Ward |August 11, 2021 |Nautilus 

Of course it's a big world out there, and this stony pairing had indeed been noticed before, to amusing effect. The Dankest Crossword Clues You Can Get |Matt Gaffney |June 7, 2021 |The Daily Beast 

These meteorites are so commonplace that we call them “ordinary” chondrites, or stony meteorites. Comets Are More Dangerous Than We Thought - Issue 98: Mind |Sean Raymond |March 10, 2021 |Nautilus 

Over several days, this wine danced from bright plum flavors to stony minerality, and back again to fruit, then earth, until the flavors melded into something more complex. An unexpected merlot from Tuscany surprises with complex flavors — and a $15 price tag |Dave McIntyre |February 26, 2021 |Washington Post 

Faced with a moping, stony-faced new partner, Beth did not selflessly set aside her own pain to help. A Perfect 'Walking Dead' Episode |Melissa Leon |March 3, 2014 |DAILY BEAST 

It exists to make one feel things, to make the stone stony. Sondheim on Sondheim: American Musical Theater in Six Songs |Jimmy So |December 9, 2013 |DAILY BEAST 

Rey, after sitting for long hours in stony silence, eventually gave the police her last name. The Mad Shooter of Paris Is a ‘Natural Born Killer’ |Christopher Dickey |November 21, 2013 |DAILY BEAST 

Sara Sonnack, a Stony Brook journalism major who will be a senior next year, says she hopes she can make the trip. NYC Auction Held to Raise Funds for Stony Brook’s New Marie Colvin Center |Caitlin Dickson |June 22, 2012 |DAILY BEAST 

Havel was a hero who shrugged off the title, understandably uncomfortable with the assumptions of stony perfection. John Avlon: Vaclav Havel's Heroic Politics of Truth and Responsibility |John Avlon |December 19, 2011 |DAILY BEAST 

Why didn't he send a trooper to report at once instead of wasting time in going to Stony Crossing? Raw Gold |Bertrand W. Sinclair 

It took us all of the next day to make the trip to Stony Crossing and back by way of the place where Rutter was buried. Raw Gold |Bertrand W. Sinclair 

It ended in a broad open moor, stony; and full of damp boggy hollows, forlorn and desolate under the autumn sky. The Daisy Chain |Charlotte Yonge 

The tones of the neighbouring convent bell, echoing through the stony vaults, sounded loud and awful as the knell of doom. The Catacombs of Rome |William Henry Withrow 

Why did he quit the fruitful banks of the Euphrates for a spot so remote, so barren, and so stony as Sichem? A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire)