Here are the upgrades I think are most important in relation to the places I hunt. Six ways to make your ATV even more rugged |By Tyler Freel/Outdoor Life |September 17, 2020 |Popular-Science 

A Hyderabad-headquartered startup, which is competing in the TechCrunch Disrupt Startup Battlefield this week, thinks it has found a way to address both of these challenges. UrbanKisaan is betting on vertical farming to bring pesticide-free vegetables to consumers and fight India’s water crisis |Manish Singh |September 17, 2020 |TechCrunch 

I didn’t want to have a Michelin star because I thought it was not the kind of restaurant we wanted to be. Christian Puglisi Is Closing His Influential Copenhagen Restaurants. COVID Is Only Partly to Blame |Rafael Tonon |September 17, 2020 |Eater 

I’m thinking about that Chicago train rumor that spread through Facebook in the spring. Mobilizing the National Guard Doesn’t Mean Your State Is Under Martial Law. Usually. |by Logan Jaffe |September 17, 2020 |ProPublica 

This is something James Robinson has spent a lot of time thinking about. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

I think a lot of it has to do with the attitude and the energy behind it and the honesty. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

“I think the types of stories we do are very similar to what happened with hip-hop,” says Jones. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

“I think for trans men who are dating every time they hook up they have another coming out,” Sandler said. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

I think a large majority of our fans are [other] nationalities. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

Think back to the Bush-Kerry race of 2004, the Thrilla in Vanilla. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

You would not think it too much to set the whole province in flames so that you could have your way with this wretched child. St. Martin's Summer |Rafael Sabatini 

Few people, I think, realize that, and fewer still realize the reasonable consequences of that. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

We are apt to think of these little ones as doing right only when under compulsion: but this is far from the truth. Children's Ways |James Sully 

He made me think of an old time magician more than anything, and I felt that with a touch of his wand he could transform us all. Music-Study in Germany |Amy Fay 

"I hope you don't think I speak always to strangers, like that," said the girl in the rose hat. Rosemary in Search of a Father |C. N. Williamson