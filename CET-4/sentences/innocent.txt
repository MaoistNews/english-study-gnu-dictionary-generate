Two innocent people were publicly named as the attacker — a marketing executive and a retired police captain. Bethesda bike-trail assailant receives probation for attack in flier-snatching case |Dan Morse |February 2, 2021 |Washington Post 

Eighty-seven percent of those accused were eventually found to be innocent. Why did education leaders trash a school full of music, dance and art? |Jay Mathews |January 30, 2021 |Washington Post 

Russell Westbrook, the terrifyingly athletic stat-stuffer who seems to hurt innocent rims for no apparent reason. The Nuggets Look Mediocre, But Nikola Jokić Is Setting The World On Fire |T.J. McBride |January 22, 2021 |FiveThirtyEight 

Most presidents in real life and in games have stained their hands with the blood of innocents, and Sonic the Hedgehog’s president is no different. The best presidents in video games |Gene Park |January 20, 2021 |Washington Post 

There’s probably a mixture of kind of innocent people swept along and people who were hardcore organizers knowing exactly what they wanted to do. A Former Top Spy Tells All |Nick Fouriezos |January 19, 2021 |Ozy 

There is just no way of selling this picture with an innocent defense like, “she just asked for a snap.” Buckingham Palace Disputes Sex Allegations Against Prince ‘Randy Andy’ |Tom Sykes |January 4, 2015 |DAILY BEAST 

Denied parole nine straight times, he insists he is innocent of the crime for which he was convicted. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

I was declared innocent, and they said I should pay $104,000. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The Butterbrief, issued by Pope Innocent VIII, was a turning point for the then bland Stollen, which gradually became sweeter. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 

Koenig must know by now that second to knowing if Adnan is innocent, we want to know if she thinks Adnan is innocent. Adnan Killed Her! No, Jay Did It! Serial’s Uncertain, True-to-Reality End |Emily Shire |December 18, 2014 |DAILY BEAST 

The mother's lips could not finish the charge she was about to put upon her innocent child. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

But green Chartreuse unhappily is not innocent; it is more than a spirit, it is a powerful drug. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

They used to believe in witchcraft, and they burned millions—yes, millions—of innocent women as witches. God and my Neighbour |Robert Blatchford 

Blanche sat there with her little excited, yet innocent—too innocent—stare; her eyes followed Mrs. Vivian's. Confidence |Henry James 

"I wouldn't do him any harm for the world," said Mrs. Kaye, casting down her eyes and looking very young and innocent. Ancestors |Gertrude Atherton