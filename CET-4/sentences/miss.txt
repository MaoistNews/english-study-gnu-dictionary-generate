Myerson herself appears to have bought into that stigma, offering mixed to negative views on the Miss America pageant. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

So, why no Jewess in the mix of more recent and diverse Miss Americas? Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Her Miss America win transcended mere superficial beauty standards. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

In 1995, Myerson made a point not to attend the 75th anniversary of the Miss America pageant. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

No Jewish woman has been crowned Miss America since Bess Myerson won in 1945. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

But Lucy had noted, out of the corner of her watchful eye, the arrival of Miss Grains, indignant and perspiring. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

But with all her advantages Miss Solomonson failed with the old lord, and she abuses him to this day. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The strains of the syren at last woke her uncle, and brought back Miss Hood, who suggested that it was late. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He, with others, thinking the miss-sahib had gone to church, was smoking the hookah of gossip in a neighboring compound. The Red Year |Louis Tracy 

Miss Christabel blushed furiously and emitted a sound half between a laugh and a scream. The Joyous Adventures of Aristide Pujol |William J. Locke