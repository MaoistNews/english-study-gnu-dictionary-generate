I am willing to bet that you have not eaten mutton in the last six months, probably the last six years. The Future of Meat (Ep. 367 Rebroadcast) |Stephen J. Dubner |August 29, 2019 |Freakonomics 

Cue heartbroken Galavant engorging himself on booze and mutton back home. ‘Galavant’: A Drunken, Horny Musical Fairy Tale |Melissa Leon |January 5, 2015 |DAILY BEAST 

The co-owner of Metropolis Collectables, Vincent has Wolverine mutton chops, a Tony Stark goatee, and Lex Luthor swagger. The Holy Grail of Comic Books Hid in Plain Site at New York Comic Con |Sujay Kumar |October 14, 2014 |DAILY BEAST 

The speciality was mutton tagine, softly braised in the tagine pot with peas, vegetables, and spices. The World’s Five Best Cheap Meals |Jodi Ettenberg |November 14, 2012 |DAILY BEAST 

Mutton Tagine in Zaita, Morocco This photo was taken at a tiny roadside town on the drive to Fez. The World’s Five Best Cheap Meals |Jodi Ettenberg |November 14, 2012 |DAILY BEAST 

Generally, meat gets stringier, gamier, and less tasty as the animal ages: compare mutton (an old sheep) to lamb. What 'Bath Salts' Will—and Won’t—Make You Do |Kent Sepkowitz |June 1, 2012 |DAILY BEAST 

Do not gastronomists complain of heaviness in London after eating a couple of mutton-chops? Little Travels and Roadside Sketches |William Makepeace Thackeray 

No doubt, having feasted on mutton so long, he had got a little sick of it, and thought he would make a dinner on beef. Mike Marble |Uncle Frank 

They mostly raise a few sheep and goats; the sheep are a poor lot, the wool is of a very inferior class, and the mutton poor. Campaign Pictures of the War in South Africa (1899-1900) |A. G. Hales 

My wife and I to church this morning, and so home to dinner to a boiled leg of mutton all alone. Diary of Samuel Pepys, Complete |Samuel Pepys 

M. Noel, in a dress-coat, very dark skinned and with mutton-chop whiskers, came forward to meet us. The Nabob |Alphonse Daudet