Inspiration to combine the plastic knob with the touchscreen came from a Keurig cup on a conference room table during a meeting to discuss how to handle volume input, he said. Ford’s electric Mustang Mach-E is an important leap into the future |Dan Carney |February 12, 2021 |Popular-Science 

There are probably other cups or plates you will grow to love as much. Replacing pieces of flatware or china can be a challenge. Here’s how to track them down. |Laura Daily |February 11, 2021 |Washington Post 

With brain signals alone, these people have been able to shop online, communicate — even use a prosthetic arm to sip from a cup. New technology can get inside your head. Are you ready? |Laura Sanders |February 11, 2021 |Science News For Students 

There’s no pretense that Travis Scott was deep in the test kitchen carefully dipping fries into different sauce cups, or that Charli D’Amelio stood behind the espresso machine layering in syrups until something clicked. Celebrities have always done endorsements. Now they sell their fast food orders. |Jameson Rich |February 5, 2021 |Vox 

Living in a home based on that of an insect that might have stung you as a child and which you’d gladly squash with a rolled-up newspaper if given the opportunity might not be everyone’s cup of tea. This Hive-Like House Is 3D Printed, Carbon-Neutral, and Made of Clay |Vanessa Bates Ramirez |February 3, 2021 |Singularity Hub 

Anthony Goldstein probably chose a trip to the Quidditch World Cup over his Birthright trip to Israel. Harry Potter and the Torah of Terror |Candida Moss, Joel Baden |January 4, 2015 |DAILY BEAST 

Preheat the oven to 450°F. Soak the cranberries in ¾ cup cranberry juice for 15 minutes. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

His ups and downs professionally outside of the World Cup are a vital a part of his story in the book. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 

I was bored, but I grabbed a red Solo cup, filled it with beer, and stayed with my group, chatting with the brothers about Jim. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

Finally, he says, “Would you like a cup of tea or something?” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

And he himself brought her the golden-brown bouillon, in a dainty Sevres cup, with a flaky cracker or two on the saucer. The Awakening and Selected Short Stories |Kate Chopin 

She thrust a bare, white arm from the curtain which shielded her open door, and received the cup from his hands. The Awakening and Selected Short Stories |Kate Chopin 

A quite young child will, for example, pretend to do something, as to take an empty cup and carry out the semblance of drinking. Children's Ways |James Sully 

His pacing had brought him to the opposite end of the board, where stood the cup of wine madame had poured for Marius. St. Martin's Summer |Rafael Sabatini 

She gave him his cup of tea, with the same gesture that had charmed Nigel on the day when he first visited her. Bella Donna |Robert Hichens