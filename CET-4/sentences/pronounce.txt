I think you’ll find that if you say the translations out loud, you’ll find that they’re pretty close to how the names are pronounced. Style Conversational Week 1423: Unnatural acts |Pat Myers |February 11, 2021 |Washington Post 

Police said Wright was shot multiple times in the head and body and was pronounced dead at the scene. D.C. police investigating three fatal shootings since Monday |Peter Hermann, Martin Weil |January 20, 2021 |Washington Post 

Israel Johnson, 20, of Fort Washington was pronounced dead at the scene, police said. Two slain in two days in Prince George’s County, police say |Martin Weil, Dan Morse |January 20, 2021 |Washington Post 

The effect is particularly pronounced in kids, who are less likely to differentiate between bonds forged in the real world and those developed over the internet—since they’ve always had both. Stressed out? Video games can help—if you follow these tips. |Stan Horazek |January 20, 2021 |Popular-Science 

Don’t worry—it’s way easier to find this muscle than to pronounce its name. A step-by-step guide to giving yourself a massage |Sandra Gutierrez G. |January 14, 2021 |Popular-Science 

At the time Kristina still could not quite pronounce the "R" sound in Russian, but she made her meaning clear. Is 9-Year-Old Russian Model Kristina Pimenova Too Sexualized? |Anna Nemtsova |December 12, 2014 |DAILY BEAST 

Never mind the word "bazaar," which you pronounce as "bizarre" and Hassan pronounces as "buzzer." Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

Then, Jessica Biel was very big around 2005-2010, and she had that scene in a bra in I Now Pronounce You Chuck and Larry. The Movie Nudity Maestro: Jim McBride on 15 Years of Mr. Skin and That Scarlett Johansson Scene |Marlow Stern |August 9, 2014 |DAILY BEAST 

“After what seemed an eternity, but probably was 15 seconds, the doctor said, ‘I pronounce this man dead,’” Gozik recalled. The Last American Soldier Executed for Desertion |Michael Daly |June 6, 2014 |DAILY BEAST 

The upper elite still try to pronounce judgments and lead, but fewer and fewer of those down below pay attention. The Smartest Book About Our Digital Age Was Published in 1929 |Ted Gioia |January 5, 2014 |DAILY BEAST 

That persons competent to judge of their merit would in after years pronounce them of priceless value. The World Before Them |Susanna Moodie 

The danger which seemed so terrible to many honest friends of liberty he did not venture to pronounce altogether visionary. The History of England from the Accession of James II. |Thomas Babington Macaulay 

The best way of scansion is perhaps to read despyt-e with final e, preserved by csura, and to pronounce Diane as Din'. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

I think a prudent reader should pronounce as the judges do—Pater est is quern nuptiæ demonstrant. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

The last act brings us to the great hall of the papal palace at Avignon, where the Pope is to pronounce judgment upon the Queen. Frdric Mistral |Charles Alfred Downer