A lot of throw blankets and children’s bedding are made from Minky. Best weighted blanket: Sleep like a baby with our comfy bedding picks |PopSci Commerce Team |February 12, 2021 |Popular-Science 

The best heated throw blankets will let you control the temperature setting. Best heated throw blanket: Bundle up with these electric blankets |PopSci Commerce Team |February 11, 2021 |Popular-Science 

Mahomes ran 497 yards before making throws or taking sacks, according to NFL Next Gen Stats, the most in one game since it started keeping track. What went wrong for the Chiefs and Patrick Mahomes in a brutal Super Bowl defeat |Adam Kilgore |February 8, 2021 |Washington Post 

Those throws come from a highly practiced palm and well-schooled eyes. Patrick Mahomes has unmatched physical gifts. His intellect might be what sets him apart. |Sally Jenkins |February 5, 2021 |Washington Post 

Still, the ability to get in Mahomes’s face is associated with a big drop in accuracy even after accounting for the particulars of the throw. Our Guide To Super Bowl LV |Neil Paine (neil.paine@fivethirtyeight.com) |February 3, 2021 |FiveThirtyEight 

In general, their presence is meant to throw you off your game by design. Brie Larson’s Hollywood Transformation |Marlow Stern |December 29, 2014 |DAILY BEAST 

Would a state with a keen understanding of the power of propaganda be so willing to just throw away such a trove of information? No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

Just wanted to place it in the context of slates needing picture choices that throw off revenue to make the numbers work. Inside Sony’s ‘Pineapple Express 2 Drama’: Leaked Emails Reveal Fight Over Stoner Comedy Sequel |William Boot |December 21, 2014 |DAILY BEAST 

Archrival India has money to throw around, and Iran and Russia are also exerting influence in the region. Pakistan’s Dance With Terrorists Just Backfired and Killed 132 Children |Chris Allbritton |December 17, 2014 |DAILY BEAST 

He went so far as to throw an “Activation Party” in their honor just weeks before his September arrest. The Father Who Made His Kids Have Sex With a Dog |John L. Smith |December 16, 2014 |DAILY BEAST 

If you throw away this chance, you will both richly deserve to be hanged, as I sincerely trust you will be. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

That woman meant mischief, or she would never have dared to suggest that a British officer should throw in his lot with hers. The Red Year |Louis Tracy 

If the Turks get hold of a lot of fresh men and throw them upon us during the night,—perhaps they may knock us off into the sea. Gallipoli Diary, Volume I |Ian Hamilton 

Nothing will be easier then to throw the Poles into the shade of the picture, or to occupy the foreground with a brilliant review. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

All this will, doubtless, throw a number of deserving persons out of employ. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various