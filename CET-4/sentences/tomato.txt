Though we disagree, Matt and Dana have brought me tomatoes from their garden and shopped for me as I’ve quarantined. ‘Hoax’ reveals ‘rot at core of our politics’ |Kathi Wolfe |September 4, 2020 |Washington Blade 

Once a week you drive to the allotment on the edge of the city where you grow marigolds and petunias, tomatoes and broad beans. The first murder |Katie McLean |August 19, 2020 |MIT Technology Review 

He worked for the Tip-Top Canning Factory, which was founded by his great-grandfather, and the factory’s tomato farm. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

Peter Timmer, you will recall, grew up working on the tomato farm and cannery founded by his great-grandfather. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

De Moraes and her colleagues set up experiments with mustard and tomato plants. Bumblebees may bite leaves to spur plant blooming |Susan Milius |July 2, 2020 |Science News For Students 

Fittingly to that point, its Rotten Tomato score (as of Tuesday evening) was a flat 50 percent. ‘Selfie’ Is Both a Brilliant and Terrible TV Show |Kevin Fallon |September 30, 2014 |DAILY BEAST 

The tomato sauce is ‘gravy’ to many Italian-Americans of a certain class. Tales of a Jailhouse Gourmet: How I learned to Cook in Prison |Daniel Genis |June 21, 2014 |DAILY BEAST 

The sandwich is made with thick tiles of quality bread and adorned with lettuce and tomato. Become a Fried Seafood Believer at South Beach Market |Jane & Michael Stern |April 20, 2014 |DAILY BEAST 

Fresh tomato juice makes an incredible difference in the drink. Cat Cora’s Valentine’s Day Menu for Single People |Cat Cora |February 13, 2014 |DAILY BEAST 

Mongolians bravely swallow a glass of pickled sheep eyeballs mixed into tomato juice to chase away their morning-after blues. The Wildest Hangover Cures From Around the World |Nina Strochlic |November 29, 2013 |DAILY BEAST 

The next morning it was that Cash cut the ball of his right thumb open on the sharp edge of a tomato can. Cabin Fever |B. M. Bower 

He made his loaves, tucked them into the pan and greased the top with bacon grease saved in a tomato can for such use. Cabin Fever |B. M. Bower 

A dibble made of a round and sharp-pointed stick is the usual tool for making holes when setting tomato and other plants. The Boy Mechanic, Book 2 |Various 

Among these words are the names of such common things as chocolate, cocoa, tomato. Stories That Words Tell Us |Elizabeth O'Neill 

She's very pretty, but towards the end of the evening she looked rather like a squashed tomato, I thought. Happy House |Betsey Riddle, Freifrau von Hutten zum Stolzenberg