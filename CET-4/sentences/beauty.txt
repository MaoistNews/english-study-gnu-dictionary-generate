There’s a clear trend away from gendered anything these days—clothes, beauty products, pronouns. Bacardi’s ‘by women, for women’ vodka is a pandemic-era branding blunder |ehinchliffe |July 23, 2020 |Fortune 

On the surface, this seems reasonable — swapping out Black pain for Black beauty. Representation is deeper than putting Black icons on magazine covers |Fabiola Cineas |July 23, 2020 |Vox 

A disappointment became an opportunity to enjoy the natural beauty of a place we could access through general aviation. Flying over mountains isn’t as scary (or hard) as you might think |By Julie Boatman/Flying Mag |July 22, 2020 |Popular-Science 

This modestly upscale dermaplaning wand is designed for people who want the quality of a salon device but are seeking beauty upgrades on a budget. Best facial razors designed for women |PopSci Commerce Team |July 22, 2020 |Popular-Science 

That’s the beauty of why we’ve presented it the way we have. The keeping of the Star-Spangled Banner: A story of emblematic resilience |John Barrat |July 4, 2017 |The Smithsonian Insider 

If you read the reactions, she was billed as ‘Beauty and Brains.’ Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Her Miss America win transcended mere superficial beauty standards. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Also, she was tall and thin, too, further adding to the ways she met the physical beauty conventions. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

There was so much beauty, talent, potential, and most importantly, honesty in your work. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

There was something beautiful about this, and I still see that beauty. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 

A flash of surprise and pleasure lit the fine eyes of the haughty beauty perched up there on the palace wall. The Red Year |Louis Tracy 

Let the thought of self pass in, and the beauty of great action is gone, like the bloom from a soiled flower. Pearls of Thought |Maturin M. Ballou 

The beauty, the mystery,—this fierce sunshine or something—stir——' She hesitated for a fraction of a second. The Wave |Algernon Blackwood 

Thy eye desireth favour and beauty, but more than these green sown fields. The Bible, Douay-Rheims Version |Various 

Sir Cadge was about the same age as the famous beauty, and rose quite two inches above her lofty head. Ancestors |Gertrude Atherton