According to CNN, the city released some 325 pages of documents on Monday, detailing the extent to which officials went to try and control the narrative around Prude’s death. Officials Worked To Deliberately Delay Release Of Daniel Prude Body Cam Footage |mharris |September 17, 2020 |Essence.com 

In January of this year, a Harvard Berkman Klein Center review of 36 of the most prominent documents guiding national and company AI strategies found eight common themes—among them privacy, safety, fairness, and explainability. Why kids need special protection from AI’s influence |Karen Hao |September 17, 2020 |MIT Technology Review 

The rates for the extra suites ranged from $142 to $283 per night, according to the documents. Trump’s businesses charged Secret Service more than $1.1 million, including for rooms in club shuttered for pandemic |David Fahrenthold, Josh Dawsey |September 17, 2020 |Washington Post 

According to the department’s standard operating procedures document, the department had previously used the system to identify “work-related problematic behavioral patterns among members.” The policing reforms in the Breonna Taylor settlement, explained |Fabiola Cineas |September 17, 2020 |Vox 

The proposal would guarantee free public access to judicial documents, ending the current practice of charging 10 cents per page for many documents—as well as search results. Bill to tear down federal courts’ paywall gains momentum in Congress |Timothy B. Lee |September 16, 2020 |Ars Technica 

A second document was titled: “Gambia Reborn: A Charter for Transition from Dictatorship to Democracy and Development.” The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

I am not the first or last person to document the hip-hop scene in Cuba. Cuban Hip-Hop Was Born in Alamar |Daniel Levin |December 26, 2014 |DAILY BEAST 

Normal procedure is that any member country can request that a document be circulated, and the UN does it pro-forma. Exclusive: Sony Emails Say Studio Exec Picked Kim Jong-Un as the Villain of ‘The Interview’ |William Boot |December 19, 2014 |DAILY BEAST 

This is both an outstanding work of scholarship and a commanding visual document. The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 

The document said Wright was condescending and had “resorted to name-calling,” though no examples were offered. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

The Empress had shewn herself too entirely prejudiced, to have been affected by any document he could have presented. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

“Lecompton” constitution of Kansas was a pro-slavery document which Buchanan favoured. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

The Assistant Commissioner, hand pressed to brow, began to study a document which lay before him. Dope |Sax Rohmer 

Decollat,” says a contemporary document, with a grim succinctness, “in castrum Londin: vulgo turris appellatur. The Portsmouth Road and Its Tributaries |Charles G. Harper 

One of them snarls quietly out of a long document about the Statement of Claim. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various