Interesting that those who sat in judgment of him found those two sets of beliefs to be incompatible. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

If anything the work the two cops and the maintenance guy were doing deserves more respect and probably helped a lot more people. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

Toomey lives here with her husband, Mark, a managing director at Goldman Sachs, and their two daughters. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

But no more so than the Sodexo building maintenance man or the two cops who were also killed in the crossfire. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

France 24's coverage of two developing hostage situations in Paris on Friday. LIVE Coverage of the Paris Terror Attacks | |January 9, 2015 |DAILY BEAST 

The case was an assault and battery that came off between two men named Brown and Henderson. The Book of Anecdotes and Budget of Fun; |Various 

On the thirteenth of the same month they bound to the stake, in order to burn alive, a man who had two religious in his house. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The night wore on, and the clock downstairs was striking the hour of two when she suddenly awakened. The Homesteader |Oscar Micheaux 

"The Smoker," and "Mother and Daughter," a triptych, are two of her principal pictures. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

The Spaniards captured two schooners, having on board 22 officers and 30 men, all of whom were hanged or sent to the mines. The Every Day Book of History and Chronology |Joel Munsell