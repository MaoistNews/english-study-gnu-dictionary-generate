Today, the solution that she built moves into public beta and will compete at TechCrunch Disrupt Battlefield with other startups for $100,000 and the Disrupt Cup. Perigee infrastructure security solution from former NSA employee moves into public beta |Ron Miller |September 17, 2020 |TechCrunch 

The filings have been at the center of a legal fight between the impeached president and Democrats, who have demanded that he make the contents available to the American public. Jaime Harrison Says He Feels ‘A Little Sad’ For Lindsey Graham |Hope Wright |September 17, 2020 |Essence.com 

During the most recent fiscal year, which ended in June, it rose again, to 314, according to the Cook County public guardian’s office. Still No Answers to Lawmakers’ Questions About Children Stuck in Psychiatric Hospitals |by Duaa Eldeib |September 15, 2020 |ProPublica 

No Silicon Valley startup has gone public yet this year, but that bleak stretch is about to end. Why Snowflake could become a tech giant |Aaron Pressman |September 15, 2020 |Fortune 

In 2015, the Obama-Biden administration orchestrated Mission Innovation, a compact among 20 countries to double public funding for energy RD&D over five years. To confront the climate crisis, the US should launch a National Energy Innovation Mission |Amy Nordrum |September 15, 2020 |MIT Technology Review 

When cities started adding chlorine to their water supplies, in the early 1900s, it set off public outcry. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Not to be left behind, progressives in neighboring Wisconsin clamored to join the cutting edge of public health. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Just the hard-on before you shoot unarmed members of the public. 'Babylon' Review: The Dumb Lives of Trigger-Happy Cops |Melissa Leon |January 9, 2015 |DAILY BEAST 

Great American leaders have long contributed profound thoughts of tremendous consequence to the public discourse. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

Saved from the public gallows, Weeks was virtually exiled from the city, and wound up in Mississippi, where he raised a family. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

As such it is now presented to the public for whatever meed of praise or censure it is found to deserve. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Five of the number had studied with Liszt before, and the young men are artists already before the public. Music-Study in Germany |Amy Fay 

Many of these have been seen in the Corcoran Art Gallery and in other public exhibitions. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

It was close upon twelve o'clock, and the "Rooms" had been open to the public for two hours. Rosemary in Search of a Father |C. N. Williamson 

It will be a busy session; and I want to see if I can't become a useful public man. Elster's Folly |Mrs. Henry Wood