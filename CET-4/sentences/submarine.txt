Undersea robots, submarines and devices that track fish or other sea animals use a similar technique. Bye-bye batteries? Power a phone with fabric or a beacon with sound |Kathryn Hulick |January 6, 2021 |Science News For Students 

Shuttered in a house on foreign soil where I don’t speak the language, I have found myself snapping back into submarine deployment mode. How a Nuclear Submarine Officer Learned to Live in Tight Quarters - Issue 94: Evolving |Steve Weiner |December 30, 2020 |Nautilus 

Instead of rockets, the proposed lander would use fans to push itself around, almost like a submarine, turning the disadvantage of the dense atmosphere into an advantage. How future spacecraft might handle tricky landings on Venus or Europa |Lisa Grossman |December 23, 2020 |Science News 

After we’d laid the groundwork in the design-and-build class, my students and I were joined in our efforts by Draper Laboratory in Cambridge, where researchers work on things like missile guidance systems and submarine navigation. My satellite would fit in a small suitcase. |Katie McLean |December 18, 2020 |MIT Technology Review 

Lost to the sea, sunken ships and submarines can create new habitats in the deep. Analyze This: Shipwrecks provide a home for bottom-dwelling fish |Carolyn Wilke |December 4, 2020 |Science News For Students 

The new submarine and the LRS-B are “uploadable” systems that can carry more warheads if strategic requirements change. US & Russia Re-Arming for a New Cold War |Bill Sweetman |September 30, 2014 |DAILY BEAST 

William—absent Kate and Prince George—arrived by helicopter to Gosport Royal Navy Submarine Museum in Hampshire today. William Has a Tipple in the Line of Duty |Tom Sykes |May 12, 2014 |DAILY BEAST 

The tactical balance between the surface warship and the submarine has strategic impact. Tomorrow’s Stealthy Subs Could Sink America’s Navy |Bill Sweetman |May 12, 2014 |DAILY BEAST 

Other improvements are making the submarine more elusive and lethal. Tomorrow’s Stealthy Subs Could Sink America’s Navy |Bill Sweetman |May 12, 2014 |DAILY BEAST 

The onetime nuclear submarine weapons officer has entered a not guilty plea. Former Navy Officer Bryan Caisse Is Accused of Defrauding Friends and Classmates |Michael Daly |January 22, 2014 |DAILY BEAST 

Meanwhile, the Australian submarine has got up through the Narrows and has torpedoed a gunboat at Chunuk. Gallipoli Diary, Volume I |Ian Hamilton 

The submarine E.14 sailed into harbour after a series of hair-raising adventures in the Sea of Marmora. Gallipoli Diary, Volume I |Ian Hamilton 

As we were leaving, a message came in to say that an enemy submarine had been sighted off Gaba Tepe. Gallipoli Diary, Volume I |Ian Hamilton 

The submarine scare is full on; the beastly things have frightened us more than all the Turks and all their German guns. Gallipoli Diary, Volume I |Ian Hamilton 

Asking his commands, the stranger said, 'I am one of the submarine inhabitants of this neighborhood. The Book of Anecdotes and Budget of Fun; |Various