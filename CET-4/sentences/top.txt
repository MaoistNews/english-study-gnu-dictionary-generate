There is a particular focus in the magazine on attacking the United States, which al Qaeda calls a top target. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

In the meantime, Epstein has tried to use his charitable projects to float him back to the top. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

If so, he has his silence -- on top of poor judgment -- to blame. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

One of its top officials is the current minister of the interior in Baghad. What an Iranian Funeral Tells Us About the Wars in Iraq |IranWire |January 6, 2015 |DAILY BEAST 

Pulling oil from the tar sands is costly, even more so when you tack transportation costs on top. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

He came to the top of the stairs with a lamp in his hand, and wanted to know what the rumpus was about. The Bondboy |George W. (George Washington) Ogden 

The offspring of the ungodly shall not bring forth many branches, and make a noise as unclean roots upon the top of a rock. The Bible, Douay-Rheims Version |Various 

The jagged top and spurs of San Jacinto Mountain shone like the turrets and posterns of a citadel built of rubies. Ramona |Helen Hunt Jackson 

The way was under a double row of tall trees, which met at the top and formed a green arch over our heads. Music-Study in Germany |Amy Fay 

In fact, his appearance was so formidable that Davy did not pause for a second look, but started off at the top of his speed. Davy and The Goblin |Charles E. Carryl