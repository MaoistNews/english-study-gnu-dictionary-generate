Plus, you won’t have to worry about the snow because these gloves are designed with an outer waterproof shell, which protects the batteries from any liquid damage. Best hand warmers: Block the chill during your favorite winter activities |PopSci Commerce Team |February 10, 2021 |Popular-Science 

In 2019 he started working with SmartWater CSI, a Florida-based company that produces a proprietary traceable liquid. Bee theft is almost a perfect crime—but there’s a new sheriff in town |Andrew Zaleski |February 9, 2021 |Popular-Science 

If you leave your boiling pot unattended and the liquid level gets too low, it’s very possible to scorch your tree syrup. Make your own maple syrup without harming the trees |By Tim MacWelch/Outdoor Life |February 7, 2021 |Popular-Science 

There isn’t actually that much onscreen, you know, liquids, which is the only way I can put it. How Gideon the Ninth author Tamsyn Muir queers the space opera |Constance Grady |February 5, 2021 |Vox 

The action in GameStop proves that liquid, high-volume markets aren't necessarily efficient markets. How the government might react to GameStop |Felix Salmon |February 5, 2021 |Axios 

Total oil production figures include crude oil, natural gas liquids, and other liquid energy products. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

During the height of his disenchantment, he visited his hometown where an old friend gave him some liquid acid. DJ Spooky Wants You To Question Everything You Know About Music, Technology, and Philosophy |Oliver Jones |December 27, 2014 |DAILY BEAST 

How can a chilled, acidic, and bubbly liquid make you feel all warm and fuzzy inside? Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

Slow at first, then steadily, a stream of liquid drips off the incision. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

We want to be the global auction house for the new collector at lower price points and create a more liquid market. William, Kate, and Jay Z’s Favorite Art Star: Alexander Gilkes' World of Rock Stars and Royalty |Tim Teeman |December 10, 2014 |DAILY BEAST 

Aristide stood gossiping until the Mayor invited him to take a place at the table and consume liquid refreshment. The Joyous Adventures of Aristide Pujol |William J. Locke 

Carbolic Acid … liquid … oil … sweet oil … castor oil … aperient … Epsom Salts … white … white of egg. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

On the other hand, the rapidity with which liquid manure produces its effect must be taken into account. Elements of Agricultural Chemistry |Thomas Anderson 

The quantity of this liquid distributed per acre is about 50,000 gallons, at a cost of 2d. Elements of Agricultural Chemistry |Thomas Anderson 

In its liquid form, however, sewage manure has been employed with the best possible effect in the cultivation of meadows. Elements of Agricultural Chemistry |Thomas Anderson