Going forward, the city will open the system again every Thursday and Friday, at least until it replaces the current vaccine website with a new system that it has promised will be operational in the coming weeks. Glitches, errors, a nonworking number: D.C. residents still struggle to get vaccine appointments |Julie Zauzmer, Meagan Flynn |February 26, 2021 |Washington Post 

“I can’t wait for this problem to be fixed so I can spend my time working on something else … and surfing,” Dedina said Thursday. Mexico Says It Fixed the Tijuana River Sewage Problem. It’s Partly True. |MacKenzie Elmer and Vicente Calderón |February 26, 2021 |Voice of San Diego 

The authority’s board, which sets toll ranges in the state, heard details of the preliminary rates Thursday but won’t vote on a final proposal until the fall, after the authority holds hearings and solicits public comments, officials said. Maryland reveals potential toll rates for proposed HOT lanes on Beltway, I-270 |Katherine Shaver |February 26, 2021 |Washington Post 

The Metro Transit Police Department is making strides in addressing community concerns about policing, its chief said Thursday, highlighting efforts to rewrite policing standards, review past internal investigations and improve transparency. Metro Transit Police chief says changes underway to address community concerns |Justin George |February 25, 2021 |Washington Post 

GameStop shares closed up 19 percent Thursday, after surging as much as 88 percent, as retail investors returned to the shorted stock that set off a trading frenzy last month that shocked Wall Street and sparked federal scrutiny. GameStop, other ‘meme’ stocks surge again |Hannah Denham |February 25, 2021 |Washington Post 

On Thursday, Garcetti ruled himself out of the race to succeed Boxer. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

“Barbarism,” said retired NYPD Officer Jim Smith on Thursday. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 

But on Thursday Boxer triggered a Golden State political earthquake, announcing that she would not seek a fifth term in 2016. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

Houellebecq on Thursday announced that he is suspending promotion of the novel. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Angelina Jolie was able to seemingly glide into the Vatican on Thursday to present her new film ‘Unbroken.’ Pope Francis Has the Pleasure of Meeting Angelina Jolie for a Few Seconds |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

A little boy aged two years and four months was deprived of a pencil from Thursday to Sunday for scribbling on the wall-paper. Children's Ways |James Sully 

Their sin began on Holy Thursday, with so little secrecy and so bad an example, that the affair was beginning to leak out. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Although it was the second Thursday in the month she turned to the portion appointed for the first Wednesday. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

A still more signal triumph to American ingenuity was accorded on Thursday. Glances at Europe |Horace Greeley 

But on the last Thursday of each month he was quite another man. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow