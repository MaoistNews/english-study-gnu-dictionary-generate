McDonald’s accompanied the release with an expensive marketing campaign that iterated, then reiterated, that this “burger with the grownup taste” was not for childish palates. The Arch Deluxe Was a Hell of a Burger. It Was Also McDonald’s Most Expensive Flop. |Jeremy Glass |July 23, 2021 |Eater 

Tim was goofy and childish, whereas Jill, his wife, was always ready – with a disapproving scowl, a snappy remark and seemingly endless stores of patience – to bring him back in line. Why Are Sitcom Dads Still So Inept? |LGBTQ-Editor |June 17, 2021 |No Straight News 

There’s something about when you’re a parent and your children get sick, they get more childish. These Mothers Wanted to Care for Their Kids and Keep Their Jobs. Now They're Suing After Being Fired |Eliana Dockterman |March 3, 2021 |Time 

If anything, the only real surprise was that these childish actions were coming from a sitting member of Congress. Why the Equality Act Matters For Our Family—And For Many Others Across America |Marie Newman and Evie Newman |March 3, 2021 |Time 

Resorting to Santa, a childish character in whom adults should not believe, shows how desperate I was. 2021 is the year to dream big |Michael K. Lavers and Yariel Valdés González |December 31, 2020 |Washington Blade 

We are not "equal" and you are not an ally if this is the childish base of your notions. The Unbearable Whiteness of Protesting |Rawiya Kameir, Judnick Mayard |December 10, 2014 |DAILY BEAST 

The slimy, childish, petulant Viserys starts off as a symbol of everything about Targaryen rule Westeros has rejected. Who Died and Made You Khaleesi? Privilege, White Saviors, and the Elusive Male Feminist Who Doesn’t Suck |Arthur Chu |June 24, 2014 |DAILY BEAST 

Why turn a genuine good-news story into an expression of childish pettiness? What Is William and Harry's Problem With The Press? |Tom Sykes |February 14, 2014 |DAILY BEAST 

It seems to be attempting satire in the vein of a Grand Theft Auto game, but, like GTA is occasionally, it just seems childish. A Study in Xbox One Violence: Dead Rising 3 Vs. Ryse: Son of Rome |Alec Kubas-Meyer |December 1, 2013 |DAILY BEAST 

The gorgeous sushi chef he meets and falls in love with, Mi-do (Kang Hye-jung), is equally silly and childish. What Asian Film Remakes Like ‘Oldboy’ Get Wrong |Jimmy So |November 26, 2013 |DAILY BEAST 

In this case, I suspect, there was co-operant a strongly marked childish characteristic, the love of producing an effect. Children's Ways |James Sully 

One of the simplest of these childish tricks is the invention of an excuse for not instantly obeying a command, as "Come here!" Children's Ways |James Sully 

There is, perhaps, in this childish suffering often something more than the sense of being homeless and outcast. Children's Ways |James Sully 

Could this be the safe old house in which childish days had passed, in which all around were always friendly and familiar faces? Checkmate |Joseph Sheridan Le Fanu 

Here, as in so many of these childish admirations, we have to do not with a purely æsthetic perception. Children's Ways |James Sully