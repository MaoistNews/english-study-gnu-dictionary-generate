It was my escape, but it was also a bridge for me to connect and build friendships. Motown President Ethiopia Habtemariam on Steering the Legendary Label Through the Pandemic |Eben Shapiro |February 7, 2021 |Time 

From Carol and Harry, Teichner has learned that we all have the capacity to create and build new friendships and attachments at whatever age or stage of life. A ‘money pit’ dog, an ailing owner and one big question |Diane Cole |February 5, 2021 |Washington Post 

A letter or even just a conversation stating how much the friendship means to you would be lovely. Miss Manners: Sorry, receptionists aren’t going to remember your name |Judith Martin, Nicholas Martin, Jacobina Martin |February 5, 2021 |Washington Post 

For Erik’s part, he seemed interested in future contact with Amanda but only on a friendship basis. Date Lab: Opposites didn’t attract — but they didn’t repel, either |Rich Juzwiak |February 4, 2021 |Washington Post 

As in Firefly Lane, the new novel mines the power of friendship. What to Know About the Book Behind Netflix’s Firefly Lane |Annabel Gutterman |February 3, 2021 |Time 

Their friendship began when Krauss, who was chairman of the physics department at Case Western in Cleveland, sought out Epstein. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

We hit it off amazingly well, and started a real friendship. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Their solidified friendship is one of the most touching details of the premiere, but it also puts Branson in a tricky predicament. What Downton’s Fashion Really Means |Katie Baker |January 2, 2015 |DAILY BEAST 

There are moments of humor even, and friendship and love, and there are moments of religion, or lack of religion. ‘Walking Dead’ Showrunner Scott Gimple Teases ‘Darker, Weirder’ Times Ahead |Melissa Leon |December 2, 2014 |DAILY BEAST 

The new way to show your love and affection for your bestie is with a fashionable Little Scocha friendship bracelet. The Daily Beast’s 2014 Holiday Gift Guide: For the Blue Ivy in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

A burning crimson flushed over the cheek of Wharton, as Louis uttered this ardent appeal to friendship and to Heaven. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

A friendship had arisen between them, which the years had idealized rather than impaired. The Joyous Adventures of Aristide Pujol |William J. Locke 

With both Tom and me it was friendship at first sight, and nothing until the final severance came ever disturbed its course. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Our friendship was close and intimate, such as is formed in the warmth of youth and which the grave alone dissolves. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

We were much alike in our tastes and habits, yet there was enough of difference between us to impart a relish to our friendship. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow