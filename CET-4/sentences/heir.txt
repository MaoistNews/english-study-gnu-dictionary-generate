Andy Jassy is the likely heir apparent to Jeff BezosBezos, who turned 57 last month, set up the transition to Jassy last summer, when the company announced that one of his possible successors, Jeff Wilke, would soon retire. Jeff Bezos stepping down as Amazon CEO, transitioning to executive chair role |Jay Greene, Tony Romm |February 4, 2021 |Washington Post 

The most logical heir, Jeff Wilke, beat Bezos to the door, announcing his retirement last year. Jeff Bezos drops CEO role with Amazon under attack |Aaron Pressman |February 3, 2021 |Fortune 

Bubbles also change rapidly— heir walls approach the speed of light as they fly outward—and feature quantum mechanical randomness and waviness. How Universes Might Bubble Up and Collide - Facts So Romantic |Charlie Wood |January 28, 2021 |Nautilus 

Junior guard Eric Ayala entered the season as the heir apparent to Cowan because he already had experience at that spot from when Coach Mark Turgeon wanted Cowan to play off the ball. For Maryland basketball, point guard has become a two-man job |Emily Giambalvo |January 26, 2021 |Washington Post 

Prince Charles, heir apparent to the British crown, has asked the world’s CEOs to guarantee the rights of nature in capitalism. The British monarchy’s latest “treaty” asks CEOs to recognize the rights of nature |Michael J. Coren |January 13, 2021 |Quartz 

By allowing him to live, Marshal avoided the shame of killing an unarmed heir-apparent. England’s Greatest Knight Puts ‘Game of Thrones’ to Shame |William O’Connor |December 9, 2014 |DAILY BEAST 

The heir to a tightrope walking family has tried some crazy stunts in his day. Nik Wallenda’s Blindfolded Tightrope Caper: Defying Death Over The Chicago River |Jacqui Goddard |November 2, 2014 |DAILY BEAST 

Phew, he could have sex too, so an heir for Downton was assured. Dan Stevens Blows Up ‘Downton’: From Chubby-Cheeked Aristo to Lean, Mean American Psycho |Tim Teeman |September 19, 2014 |DAILY BEAST 

Regardless, his turn as wealthy heir/sociopath John du Pont is never less than utterly transfixing and unsettling. Oscar Season Kicks Off in Toronto: Channing Tatum, Kristen Stewart, and More Court Awards Glory |Marlow Stern |September 14, 2014 |DAILY BEAST 

Well, he could always ask Prince Andrew how things work out for the spare heir who loses his way. The Perils of a Playboy Prince |Tom Sykes |August 29, 2014 |DAILY BEAST 

The heir apparent and his brothers were cowering in fear, afraid to strike, yet hoping that others would strike for them. The Red Year |Louis Tracy 

He is a hypochondriac now and would keep a close watch on his heir's health and habits; you may be sure of that. Ancestors |Gertrude Atherton 

In some states he becomes the heir of the adopted parent like a natural child, with some limitations. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The heir apparent, a boy of nine years old, is affianced to the daughter of a neighbouring king. A Woman's Journey Round the World |Ida Pfeiffer 

Two minutes suffice for changing teams at the “Talbot,” and off that heir of the coaching age goes again. The Portsmouth Road and Its Tributaries |Charles G. Harper