If you can’t get outside that often, looking out a window regularly should help. Healthy screen time is one challenge of distance learning |Kathryn Hulick |September 11, 2020 |Science News For Students 

Generally, advertisers’ cancelation amounts increased from 30% to 50%, and the cancelation windows shrunk from 45 to 60 days before a quarter’s start to 30 to 45 days. ‘There wasn’t a huge shift’: TV upfront market did not undergo expected overhaul this year |Tim Peterson |September 9, 2020 |Digiday 

Closed stores and empty windows result in emptier sidewalks and streets. Myths and Shame Shouldn’t Guide Cannabis Regulations |John Bertsch |September 8, 2020 |Voice of San Diego 

It took a few gummy bites of the curtain material, allowing me to open the window over its head. Why last week’s great tech sell-off should make investors wary |Bernhard Warner |September 8, 2020 |Fortune 

On May 25, he threw a chair through a window at his brother’s house and drove off, hitting several parked vehicles. Longtime Sheriff’s Employee Contradicts Official Account of Jail Death |Kelly Davis |September 3, 2020 |Voice of San Diego 

The interior video shows the gunman firing the shot through the window. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

I fall back into a dream and then suddenly there is a tapping on the window just above my bed. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

In fact, these kinds of advances helped give religion another huge window of opportunity for racial reconciliation in the 1960s. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

As it was, The Affair ended its first season last night with me contemplating hurling my television out of the window. What On Earth Is ‘The Affair’ About? Season One’s Baffling Finale |Tim Teeman |December 22, 2014 |DAILY BEAST 

The younger man rolled down his window to receive the approaching Williams “to see what he wanted.” Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 

But at the instant I caught a sight of my counterfeit presentment in a shop window, and veiled my haughty crest. God and my Neighbour |Robert Blatchford 

She had listened—she had listened intently, looking straight out of the window and without moving. Confidence |Henry James 

The east window in this church has been classed as the A1 of modern painted windows. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

The clerks had not arrived yet, and he beguiled the time by looking out of the staircase window. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

As the window dropped, Ripperda saw the wounded postilion fall on the neck of his horse. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter