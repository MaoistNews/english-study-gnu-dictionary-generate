He adds that a planet formed in this misaligned part of the fractured disk would have a highly unusual orbit. A strange dusty disk could hide a planet betwixt three stars |Paola Rosa-Aquino |September 11, 2020 |Popular-Science 

Phobos also completes an orbit in just 7 hours and 39 minutes, so its dark shadow is constantly sweeping across the Martian landscape. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

Northrup Grumman is contributing designs from its Cygnus spacecraft for parts of the lander that will propel it from orbit to the moon and Draper is designing guidance and avionics systems. Here’s what NASA’s next moon lander may look like |Aaron Pressman |August 21, 2020 |Fortune 

The company delivered 58 of the company’s Starlink satellites to orbit, along with three SkySat satellites for Planet Labs Inc. Elon Musk’s SpaceX gets a stratospheric valuation in its latest funding |Verne Kopytoff |August 18, 2020 |Fortune 

Company CEO Elon Musk had stated he wanted to fly Starship 12 miles into the air within just a couple months when he first unveiled the design last September, and that it would fly into orbit within half a year. SpaceX flew a prototype of its Starship vehicle for the first time |Neel Patel |August 5, 2020 |MIT Technology Review 

Carlisle writes that the Air Force would want a crew ratio of 10 to one for each drone orbit during normal everyday operations. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

During an emergency that ratio could be allowed to drop to 8.5 people per orbit. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

Each CAP, also known as an “orbit,” consists on four aircraft. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

Other groups in the progressive orbit are trying out other tactics. Progressives: Big Ideas Will Win Us 2016 |David Freedlander |December 10, 2014 |DAILY BEAST 

Orion will orbit Earth twice before splashing down off the California coast. To Infinity and Beyond! NASA’s Orion Mission Blasts Off |Matthew R. Francis |December 4, 2014 |DAILY BEAST 

See that silver spiral going out from Venus and around the table to the orbit of Saturn? Fee of the Frontier |Horace Brown Fyfe 

Each new orbit out from the sun has cost plenty in money, ships, and lives; it's the admission price. Fee of the Frontier |Horace Brown Fyfe 

In an orbit made elliptical by the planetary attraction the sun necessarily occupies one of the foci of the ellipse. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Bad her fleen, bade her flee; because her motion in her orbit was faster than his. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

A dark moon has joined this celestial grouping, and is now swinging in an orbit about the earth. Astounding Stories, May, 1931 |Various