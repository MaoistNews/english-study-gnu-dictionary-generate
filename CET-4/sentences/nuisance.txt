My experience working through the many nuisances associated with vacation rentals has shown me that a compromise was the best approach for Mission Beach and is likely the best approach for San Diego. There’s a Vacation Rental Compromise on the Table — Take it |Matt Gardner |September 3, 2020 |Voice of San Diego 

The public nuisance complaint came down in January 2020, triggering the evacuation. The Deal Before the 101 Ash St. Debacle Helps Explain How We Got Here |Lisa Halverstadt and Jesse Marx |August 24, 2020 |Voice of San Diego 

San Diego’s real estate assets director is resigning less than a week after the release of a devastating review of the city’s acquisition of a downtown high-rise that the county declared a public nuisance following a series of asbestos violations. City’s Real Estate Assets Director Resigns Amid Scrutiny Over Ash Street Deal |Lisa Halverstadt and Jesse Marx |August 4, 2020 |Voice of San Diego 

Then, in January 2020, after finding debris in a conference room on the seventh floor that was theoretically accessible to city workers, the county issued a public nuisance order. City Botched High-Rise Deal from Acquisition to Renovation, Investigation Finds |Jesse Marx and Lisa Halverstadt |July 30, 2020 |Voice of San Diego 

A day after the fire started, the county air pollution control agency issued three violations against the Navy, citing the fire’s smoke and odors as a public nuisance under state health and safety and county codes. San Diego County Cites Navy for Ship Fire |MacKenzie Elmer |July 24, 2020 |Voice of San Diego 

Poetry would be too obvious, too ‘portrait of the artist as a young nuisance’. Colm Toibin Describes The Creation Of His Quiet Masterpiece ‘Nora Webster’ |Jennie Yabroff |November 3, 2014 |DAILY BEAST 

If my legs made me a nuisance, I vowed to become less of one. ‘Tracing the Blue Light’: Read Chapter 1 of Eileen Cronin’s ‘Mermaid’ |Eileen Cronin |April 8, 2014 |DAILY BEAST 

Other questions: Should nuisance bears be euthanized if they are serial offenders? In Florida, Sprawling Humans Confront the Bears Who Lived There First |Jacqui Goddard |March 22, 2014 |DAILY BEAST 

Last year, there were a record 6,726, covering “nuisance” behavior, property damage, injuries to bears, and injuries to humans. In Florida, Sprawling Humans Confront the Bears Who Lived There First |Jacqui Goddard |March 22, 2014 |DAILY BEAST 

It also reduces nuisance, so there is less trouble in the neighborhood. Why Everyone Should Copy Amsterdam’s Beer-for-Work Scheme |Nadette De Visser |January 12, 2014 |DAILY BEAST 

Beastly nuisance; we shall all have to clear out, for I suppose it won't be a mere matter of scratches. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

It was rather a nuisance, too, to find that wherever he went he excited a considerable amount of attention. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Even the storage of gasoline in suitable tanks set down in the earth is not a nuisance. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Yet the business may become a nuisance when conducted in some localities, or in an improper manner. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

I am very much taken with her, which causes Rubinstein to be a perfect nuisance. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky