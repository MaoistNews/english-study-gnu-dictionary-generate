After being told of Cline’s past, and recent release from prison, a Montgomery County detective drove to the apartment building where Cline lived. Previously convicted bank robber allegedly hits new bank while under covert surveillance, gets arrested |Dan Morse |February 12, 2021 |Washington Post 

The organization joined with two others, CommunicationFIRST and Autistic Self Advocacy Network, to issue a news release on the day Sia agreed to remove the scenes from the film. The very real, very painful reasons the autistic community demanded two restraint scenes be removed from Sia’s new film ‘Music’ |Theresa Vargas |February 10, 2021 |Washington Post 

Unlike more than two dozen other states, Maryland bars the release of such records — another way in which advocates say police are shielded from accountability. The first state to pass a law protecting police accused of misconduct may also be the first to repeal it. |Ovetta Wiggins |February 9, 2021 |Washington Post 

Oakland Airport said in a news release that it is the first airport in the country to get testing vending machines. For $149, this Oakland Airport vending machine dispenses covid tests |Rachel Lerman |February 5, 2021 |Washington Post 

His debut album, Right Now, is one of the most impressive releases I’ve added to my collection and you should do the same. Soulection’s Joe Kay Presents ‘A Beginner’s Guide To Future Sounds’ |Brande Victorian |February 5, 2021 |Essence.com 

Rashad was there to celebrate the release of the Civil Rights drama Selma. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

The United States government might not release that information for years, if ever. Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 

On his eighth try, more than three decades after he went in, the parole board finally voted to release Sam. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

The “nature of the crime” was too serious to release him, they said. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

And so, he says he left prison without proper ID, just his release papers and the “dress-out gear” he was given by the state. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

He has secured the release of certain Spanish prisoners, and is building two ships. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

He had no rest until the seals were fixed to parchment, and the warrant of his release appeared in public print. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The strenuous efforts made by the Spaniards to secure their release are fully referred to in Chap. The Philippine Islands |John Foreman 

The whole party was captured by the insurgents, who were afterwards ordered to release them all. The Philippine Islands |John Foreman 

Now at the feast the governor was wont to release unto the multitude one prisoner, whom they would. His Last Week |William E. Barton