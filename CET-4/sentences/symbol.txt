In the debates over how Britain should handle its departure from the EU, fishing morphed into a symbol of Britain’s potential independence. The UK clothing industry wants the government to focus on fashion as much as on fishing |Marc Bain |February 9, 2021 |Quartz 

QAnon, a dangerous once-fringe collection of conspiracy theories, was well-represented in January’s deadly Capitol riot and many photos from the day show the prevalence of QAnon symbols and sayings. House punishes Republican lawmaker who promoted violent conspiracy theories |Taylor Hatmaker |February 5, 2021 |TechCrunch 

Some have turned masks into a symbol of government overreach. Time to double or upgrade masks as coronavirus variants emerge, experts say |Fenit Nirappil |January 27, 2021 |Washington Post 

In March, its northern city of Bergamo, then the site of the world’s deadliest Covid-19 outbreak, became a global symbol of the pandemic, as the number of dead forced army trucks to transport them to other cities for cremation. A government crisis is poised to make Italy’s Covid-19 trouble even worse |Annalisa Merelli |January 26, 2021 |Quartz 

Its journey from provincial beginnings to global fashion symbol traces Britain’s history as it became a financial powerhouse. What Dr. Martens’ IPO journey says about London’s financial future |John Detrixhe |January 23, 2021 |Quartz 

We see detoxing as a path to transcendence, a symbol of modern urban virtue and self-transformation through abstinence. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

It is now possible the building can be a symbol for progress. Meet America’s Next Ambassador to Cuba |Eleanor Clift |December 18, 2014 |DAILY BEAST 

Conservative Muslim women in Turkey hailed Esme as a martyr and a symbol of female strength and resistance. Allah, Mom, and Baklava: Turkish President Uses Mothers and Kids as Political Pawns |Xanthe Ackerman |November 27, 2014 |DAILY BEAST 

Sherlock Holmes is a new millennium sex symbol with books, movies, and TV episodes introducing him to a new generation of fans. Can Tarzan of the Apes Survive in a Post-Colonial World? |Ted Gioia |November 23, 2014 |DAILY BEAST 

In Wicca, the female goddess is represented by the Moon, a symbol of Mother Earth and fertility. ‘Gods of Suburbia’: Dina Goldstein’s Arresting Photo Series on Religion vs. Consumerism |Dina Goldstein |November 8, 2014 |DAILY BEAST 

Light, the symbol of life's joy, seems to be the first language in which the spirit of beauty speaks to a child. Children's Ways |James Sully 

This method of concealing the face, together with the wearing of the immense hat, was a symbol of mourning. Our Little Korean Cousin |H. Lee M. Pike 

The mayorʼs symbol of office is a cane with a silver knob, plated ferrule, and black cord and tassels. The Philippine Islands |John Foreman 

The olive, too, was sacred to Minerva, and as the symbol of peace was woven into the victors crown. The Catacombs of Rome |William Henry Withrow 

The symbol of the heaven-bound ship— —is mentioned by Clement of Alexandria as being in vogue in the second century. The Catacombs of Rome |William Henry Withrow