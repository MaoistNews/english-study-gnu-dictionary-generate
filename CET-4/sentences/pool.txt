“When families and communities are in crisis, that’s when they come together to pool and share resources,” says Prado, a fifth-year doctoral student in education at the University of California, Irvine. Creative school plans could counter inequities exposed by COVID-19 |Sujata Gupta |September 8, 2020 |Science News 

That is a smaller pool of recipients than the $600 enhanced checks, which weren’t limited in this manner. Federal money for the $300 enhanced unemployment benefit is running out. Here’s what to know |Lance Lambert |September 7, 2020 |Fortune 

Swimply is an online pool sharing marketplace by 23- year-old brainchild Bunim Laskin. No Plans This Weekend? No Problem! This App Lets You Rent Other People’s Pools |Charli Penn |September 4, 2020 |Essence.com 

A beach ball, on the other hand, has very little mass but takes up a lot of space, so if it were placed at the bottom of a pool, it would bob to the surface. Toy boats float upside down underneath a layer of levitated liquid |Maria Temming |September 2, 2020 |Science News 

When you’re not going to those conventions, when you’re not going to those meetings … that’s going to affect your talent pool. Deep Dive: How companies and their employees are facing the future of work |Digiday |September 1, 2020 |Digiday 

Flesh encircled him at the main pool of the Paradise Hotel and Residences at Boca. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

Marvin takes off his T-shirt and dives into his swimming pool. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

There may be no entrapped pool of human talent left on earth with the dollar value of Cuban athletes. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

In the course of her remarkable travels Thecla baptizes herself by diving into a pool of “man-eating seals.” First Anglican Woman Bishop A Return to Christian Roots |Candida Moss |December 18, 2014 |DAILY BEAST 

They include “The Goldfish Pool at Chartwell” painted in 1932 and “The Harbour, Cannes,” painted circa 1933. Churchill’s Secret Treasures for Sale: A British PM’s Life on the Auction Block |Tom Teodorczuk |December 8, 2014 |DAILY BEAST 

Big Reginald took their lives at pool, and pocketed their half-crowns in an easy genial way, which almost made losing a pleasure. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Captains Spotstroke and Pool were equally careful; the rest of those present drank freely. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

John was baptizing at a large pool called Ænon-by-Saleim,—probably allegorical, meaning “Fountain of Repose.” Solomon and Solomonic Literature |Moncure Daniel Conway 

The pool was drained in 1866, and, having been filled up, its site will ere long be covered with streets of houses. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

A germ flies from a stagnant pool, and the laughing child, its mother's darling, dies dreadfully of diphtheria. God and my Neighbour |Robert Blatchford