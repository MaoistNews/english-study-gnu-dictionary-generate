In 2015, he returned to the Minneapolis area to become headmaster of another Christian school. ‘People of Praise leaders failed me’: Christian group tied to Justice Amy Coney Barrett faces reckoning over sexual misconduct |Beth Reinhard, Alice Crites |June 11, 2021 |Washington Post 

Arndt said that in 2009 she reported the allegation to new headmaster Jon Balsbaugh “so that it didn’t fall by the wayside because Dave was still there,” according to an audio recording of her police interview. ‘People of Praise leaders failed me’: Christian group tied to Justice Amy Coney Barrett faces reckoning over sexual misconduct |Beth Reinhard, Alice Crites |June 11, 2021 |Washington Post 

The headmaster of a school wondering how she could better handle hard conversations with the powerful and wealthy parents of her students. Better conversations: The 7 essential elements of meaningful communication |matthewheimer |November 24, 2020 |Fortune 

Meanwhile, Benjamin says, the Board watched, applauded the parents, and barred the headmaster from responding. Lawsuit Claims Author Nicholas Sparks Is a Racist, Anti-Semitic Bully |Melissa Leon |October 3, 2014 |DAILY BEAST 

It really was a surprise to see, after that dancing scene, that she was the rigid headmaster of a military school. Joan Allen on ‘The Killing’ Finale and That Mother of a Twist |Kevin Fallon |August 7, 2014 |DAILY BEAST 

Abd Ar-Rahim al-Hammadi, who goes by the name Abu Omar, is the assistant headmaster at the school. Millions of Refugees from Syria’s War Are Clinging to Life In Toxic Conditions |Christopher Looney |April 14, 2014 |DAILY BEAST 

I knew the headmaster there, he had set it up to have lunch with the head of the English department. Peter Guralnick: In Love With the Life of Music |Ron Hogan |March 29, 2014 |DAILY BEAST 

Unamused, the headmaster had destroyed the issue and threatened to bounce Bonzo's creator from school. Doug Kenney: The Odd Comic Genius Behind ‘Animal House’ and National Lampoon |Robert Sam Anson |March 1, 2014 |DAILY BEAST 

French, who was expecting the headmaster of his church schools, gathered up some papers and left the room. Marriage la mode |Mrs. Humphry Ward 

Trevor made a note in his mind to effect these improvements in future essays, and was getting up, when the headmaster stopped him. The Gold Bat |P. G. Wodehouse 

Trevor waited till the headmaster had gone back to his library, gave him five minutes to settle down, and then went in. The Gold Bat |P. G. Wodehouse 

“Yes,” said the headmaster, seemingly roused by the silence following on the conclusion of the essay. The Gold Bat |P. G. Wodehouse 

The headmaster nearly always invited a few of the house prefects to Sunday supper during the term. The Gold Bat |P. G. Wodehouse