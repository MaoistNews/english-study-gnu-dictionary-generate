That’s not good for a leader like Xi, keen to rule for life. Butterfly Effect: Can Xi Avoid Trump’s Trap? |Charu Kasturi |August 27, 2020 |Ozy 

Sorkin is keen to stress that passion and persistence matter more than talent and the value of visualizing dreams before they become reality. How CNBC’s Andrew Ross Sorkin Would Close the Wealth Gap |Eromo Egbejule |August 26, 2020 |Ozy 

While countries are keen to build up scant domestic manufacturing capacity, it’s a costly and complex business, and difficult to compete with larger producers, Gavi’s Berkley said. These countries aren’t waiting for a U.S., China, or U.K. COVID vaccine |Claire Zillman, reporter |August 26, 2020 |Fortune 

Like young adults who are keen to leave home at age 18, young leopards migrate in order to begin their family and establish territorial control over new areas. Leopards Are Indeed Changing Their Spots |Eromo Egbejule |August 14, 2020 |Ozy 

For the last few months, Facebook, keen to look like it’s working with credible publishing sources, has paid Upday journalists to populate its Coronavirus Information Center. Investments in journalism, algorithms has Axel Springer’s five-year-old aggregator app Upday up over 30% in ad revenue |Lucinda Southern |July 30, 2020 |Digiday 

Would a state with a keen understanding of the power of propaganda be so willing to just throw away such a trove of information? No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

Manttan is keen to carry out research on that Burmese side of the railway as his father worked on that section. Riding Thailand’s WWII Death Railway |Liza Foreman |December 21, 2014 |DAILY BEAST 

It offers keen insights into Hitch's craft while painting an intimate and unsentimental picture of the man behind the camera. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

And because millions of us are so keen to do just that, our behavioral habits are changing. Why Every Home Needs a Drone This Holiday |Charlie Gilbert |December 8, 2014 |DAILY BEAST 

The Telegraph reports that he is fluent in Swahili and a keen zoologist. How A British Aristocrat Used Big Game Hunter’s Sperm To Get Pregnant Without His Permission |Tom Sykes |December 2, 2014 |DAILY BEAST 

The student who does not intend to arouse himself need hope for no keen sense of beauty. Expressive Voice Culture |Jessie Eldridge Southwick 

One other illustration of this keen childish dialectic when face to face with the accuser deserves to be touched on. Children's Ways |James Sully 

Impersonation may be more easily achieved intellectually, requiring only keen observation and the power of imitation. Expressive Voice Culture |Jessie Eldridge Southwick 

The keen resentment had faded from his face, but an immense reproach was there—a heavy, helpless, appealing reproach. Confidence |Henry James 

Garnache bowed to the lady, who returned his greeting by an inclination of the head, and his keen eyes played briskly over her. St. Martin's Summer |Rafael Sabatini