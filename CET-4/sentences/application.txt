The use of machine learning applications has also eased the way people write or create content. Five ways to use machine learning in digital marketing |Birbahadur Kathayat |February 12, 2021 |Search Engine Watch 

The mission of the fund is to “make bitcoin the internet’s currency,” a job application describes. Jack Dorsey and Jay Z invest 500 BTC to make Bitcoin ‘internet’s currency’ |Manish Singh |February 12, 2021 |TechCrunch 

A platform created by OVR Technology releases aroma to make virtual reality experiences more vivid, with plans for applications that range from PTSD treatment to trainings for hazardous jobs. Aromas can evoke beloved journeys — or voyages not yet taken |Jen Rose Smith |February 11, 2021 |Washington Post 

MetaHuman Creator could be similarly versatile, offering advanced character creation for next-gen platform games, mobile applications, and special effects in movies. Meet the eerily realistic digital people made with Epic’s MetaHuman Creator |Stan Horaczek |February 11, 2021 |Popular-Science 

Purchase applications also continue to increase on an annual basis, and the average loan balance again climbed to a new high of $402,200. Mortgage rates stuck in neutral amid economic uncertainty |Kathy Orton |February 11, 2021 |Washington Post 

“Defendant moved his hands in a manner so as to avoid the application of handcuffs to his wrists,” the complaint says. Protesters Slimed This Good Samaritan Cop |Michael Daly |December 16, 2014 |DAILY BEAST 

My parents were thrilled with my choice, even though I had never even paid the campus a visit during the application process. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

CORRECTION: An earlier version of this story stated that ISIS has been known to use the application FireChat. ISIS Keeps Getting Better at Dodging U.S. Spies |Shane Harris, Noah Shachtman |November 14, 2014 |DAILY BEAST 

While that application has been used in Iraq, ISIS has not been confirmed to have used it. ISIS Keeps Getting Better at Dodging U.S. Spies |Shane Harris, Noah Shachtman |November 14, 2014 |DAILY BEAST 

Denied in his green-card application, he said, “I came instantly that day,” to the shrine. Mother Cabrini, Saint of the Green Card |Michael Luongo |November 11, 2014 |DAILY BEAST 

This system had been in full operation in both districts prior to the general application of the voluntary system. Readings in Money and Banking |Chester Arthur Phillips 

This demand was made with scornful seriousness; with a ruthless application to the feelings of a son. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

It is true that such application is not so high or grand as when they govern ideas, but it is equally genuine. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Modified and amended as their system is in its practical application, it still largely conditions our outlook to-day. The Unsolved Riddle of Social Justice |Stephen Leacock 

Application of gentle heat or appropriate chemicals will serve to differentiate them. A Manual of Clinical Diagnosis |James Campbell Todd