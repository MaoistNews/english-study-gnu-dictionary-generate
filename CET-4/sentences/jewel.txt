Svo turned up with an associate dressed in police uniforms and confiscated 800,000 rubles’ worth of cash and jewels, then told the mark to report himself to the local authorities the next day. The Mobster Who Brought Armenia and Azerbaijan Together … in Death |Fiona Zublin |October 9, 2020 |Ozy 

If you weren’t told it was a prison, the “crown jewel” of prisons, you might not know. The Best Places to Be Locked Up If You Love Luxury |Eugene Robinson |October 7, 2020 |Ozy 

This book is a jewel, and soon it will be translated into English so that many others can learn about your incredible life. A letter to Soraya Santiago (1947-2020) |Wilfred W. Labiosa |September 30, 2020 |Washington Blade 

Denver, the mile-high city, is the largest in a 600-mile radius and the jewel of Colorado. Denver: Where the Queer Community is a Mile High |LGBTQ-Editor |July 14, 2020 |No Straight News 

So it’s surrounded by desert, and it’s this kind of emerald jewel in the middle of sub-Saharan Africa. How to Make Meetings Less Terrible (Ep. 389) |Stephen J. Dubner |September 19, 2019 |Freakonomics 

“The agency tasked with protecting the highest office in our land should be the crown jewel of federal law enforcement,” he said. Why Secret Service Chief Julia Pierson Was Shown the Door |Tim Mak |October 2, 2014 |DAILY BEAST 

After all, the Hamptons has the reputation of being an artistic jewel in the East Coast crown. The Hell of the Hamptons: Why the Exclusive Hotspot Is a Mind-Numbing Drag |Robert Gold |August 18, 2014 |DAILY BEAST 

These days, Tirico announces everything from the World Cup to the NBA Finals to the crown jewel, Monday Night Football. ESPN: The Worldwide Leader in Pricks |Marlow Stern |July 29, 2014 |DAILY BEAST 

For dinner, Sidney Street Café, the jewel in St. Louis' culinary crown. Get Cultured on Your Weekend Getaway: Best Trips for Art Lovers |Condé Nast Traveler |January 19, 2014 |DAILY BEAST 

In the end, internal conflict tore apart the New Jewel Movement. Remembering The Invasion of Grenada 30 Years On |Michael Ledeen |October 24, 2013 |DAILY BEAST 

You never know when you are going to stumble upon a jewel in the most out-of-the-way corner. Music-Study in Germany |Amy Fay 

The quiet of the deserted building incircled the little, glowing room as the velvet incircles the jewel in its case. Uncanny Tales |Various 

With such an assembly at hand the time was ripe for selling Daisy-Jewel to the highest bidder. Dorothy at Skyrie |Evelyn Raymond 

And I put a jewel upon thy forehead and earrings in thy ears, and a beautiful crown upon thy head. The Bible, Douay-Rheims Version |Various 

Automatically, as a result of habit, she unlocked her jewel-case and took out a tiny phial containing minute cachets. Dope |Sax Rohmer