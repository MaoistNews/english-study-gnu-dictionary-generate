Thinning and prescribed burns both generally cover around tens of thousands of acres per year, a tiny fraction of what the Little Hoover Commission recommended. Suppressing fires has failed. Here’s what California needs to do instead. |James Temple |September 17, 2020 |MIT Technology Review 

Kurtz and Franz found that tiny crustaceans called copepods got better at warding off parasitic tapeworm larvae with repeated exposure — but the results were inconsistent. ‘Trained Immunity’ Offers Hope in Fight Against Coronavirus |Esther Landhuis |September 14, 2020 |Quanta Magazine 

These tiny marine snails, or thecosomes, migrate up to surface waters at night to feed and sink to deeper waters during the day to hide from predators. Sea butterflies’ shells determine how the snails swim |Maria Temming |September 8, 2020 |Science News 

Every time you rip those muscles, tiny bone molecules build up. Women like Mulan didn’t need to go to war in disguise |Bethany Brookshire |September 4, 2020 |Science News For Students 

However on the publisher side, it seems we’re looking at an even higher concentration of ad spending with a tiny number of companies. Traditional media suffer as digital ad spend grows in 2020 forecast shows |Greg Sterling |September 2, 2020 |Search Engine Land 

Mr. Bachner found it by wandering through the market and identified a craftsmen here who works in a tiny booth. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

It was in a tiny dark overheated little bar called Niagara, and three women read before me, younger and one not so much younger. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

Twin girls, Greta and Grace, run around the floor in circles, wearing pink playsuits with tiny pink wings attached. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

Bob Cratchit, the clerk who is the father of Tiny Tim and who meekly serves Scrooge, is paid fifteen shillings a week. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

Civilians left flowers as well as a tiny frosted Christmas tree that had two red ornaments. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

He thrust his tiny tuft of beard between his teeth—a trick he had when perplexed or thoughtful. St. Martin's Summer |Rafael Sabatini 

The tiny frown reappeared between her eyes, lingered a trifle longer than before, and vanished. The Wave |Algernon Blackwood 

The sudden pall of darkness in this strange house of mystery was just a tiny bit awesome. The Boarded-Up House |Augusta Huiell Seaman 

This tiny person spent little or none of his time in the tree-tops, but chose to stay near the ground. The Tale of Grandfather Mole |Arthur Scott Bailey 

One of the first out-goings of admiration towards form is the child's praise of "tiny" things. Children's Ways |James Sully