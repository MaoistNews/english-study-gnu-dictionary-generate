At the moment, you are racing against three other riders up one of the mountains. Can You Reach The Summit First? |Zach Wissner-Gross |September 11, 2020 |FiveThirtyEight 

They lowered the price of gasoline, charging less than the station at the bottom of the mountain and less than the stations up in Shaver. A California mountain community loses its heart |Mike Silverstein |September 10, 2020 |Washington Blade 

At the park’s eastern Rincon Mountain District, mountain bikers will find a section of singletrack that links to more than 800 miles of dirt riding along the Arizona Trail. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

Crack a cold one after a race, a hard session, or a long day in the mountains. The Most Futuristic Workout Gear of 2020 |Hayden Carpenter |September 5, 2020 |Outside Online 

That was a big part of why we wanted to defend our side of the mountain. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 

Then they came up against a police patrol on mountain bicycles, which again led to more shooting, without injuries. Police Hunt for Paris Massacre Suspects |Tracy McNicoll, Christopher Dickey |January 7, 2015 |DAILY BEAST 

Also in Germany, he made The Mountain Eagle, which was set, Hitchcock recalled, “in Old Kentucky, wherever that might be.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Cadets mimicked his commands, which he issued in drawn-out syllables in his high-pitched, mountain-inflected voice. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

There were rumors that Schmidt was motivated by buried treasure or another secret of the mountain, but they were never proven. The Mole Man’s Tunnel to Nowhere |Nina Strochlic |November 28, 2014 |DAILY BEAST 

Then, he set to work building a shortcut that would take the minable minerals he found through the mountain. The Mole Man’s Tunnel to Nowhere |Nina Strochlic |November 28, 2014 |DAILY BEAST 

We had now approached closely to the foot of the mountain-ranges, and their lofty summits were high above us in mid-air. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

The jagged top and spurs of San Jacinto Mountain shone like the turrets and posterns of a citadel built of rubies. Ramona |Helen Hunt Jackson 

These residents then killed the parish priest, and without arms fled for safety to the mountain ravines. The Philippine Islands |John Foreman 

We came down the rest of the mountain more carefully, though still a great deal too fast. Glances at Europe |Horace Greeley 

A short distance off was another ridge or spur of the mountain, widening out into almost a plateau. Ramona |Helen Hunt Jackson