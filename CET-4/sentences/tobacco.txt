So while supporters see an increased tobacco tax as more revenue for the state, disincentives for kids to smoke and a win for public health, the measure could also allow America’s premium tobacco companies to gain market share. Big Tobacco stands down as Colorado and Oregon hike cigarette taxes |Lydia Belanger |January 9, 2021 |Fortune 

Annapolis juries “awarded the Butlers not only their freedom but also hundreds of pounds of tobacco in damages, lawyer’s fees, and court costs.” The enslaved families who went to court to win their freedom |Alison LaCroix |December 11, 2020 |Washington Post 

Possibly we brought in a disease or two, but on the other hand, you hooked us on tobacco. Top 10 questions I’d ask an alien from the Galactic Federation |Tom Siegfried |December 9, 2020 |Science News 

They point to America’s experiences with the alcohol and tobacco industries in particular, which have built their financial empires in large part on some of the heaviest consumers of their products. US House of Representatives votes to legalize marijuana |German Lopez |December 4, 2020 |Vox 

He's chewed tobacco since he was a young man, and when we're in the office, he also does it elegantly and turns away when he spits. Miss Manners: Exec’s disgusting habit needs to move off-screen |Judith Martin, Nicholas Martin, Jacobina Martin |December 2, 2020 |Washington Post 

Park employees helped John quit tobacco by way of a butts-proof glass enclosure, a drastic change in diet, and regular exercise. Zebra Finches, Dolphins, Elephants, and More Animals Under the Influence |Bill Schulz |December 31, 2014 |DAILY BEAST 

You spice it with blues and skiffle music, and pickle it in alcohol and tobacco smoke. The Greatest Rock Voice of All Time Belonged to Joe Cocker |Ted Gioia |December 23, 2014 |DAILY BEAST 

They want to change bad behaviors—tobacco, alcohol, using a seat belt, anything. Can the U.S. Government Go Moneyball? |Peter Orszag, Jim Nussle |December 23, 2014 |DAILY BEAST 

In the dense atmosphere of tobacco and conspiracy, one hot topic has been the death penalty. Ukraine Rebels Love Russia, Hate Gays, Threaten Executions |Anna Nemtsova |October 25, 2014 |DAILY BEAST 

My grandfather lived fast and large—he liked his liquor and his tobacco, and he was also an ace gambler. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

The segments of the corolla are pointed but on some varieties unequal, particularly that of Shiraz tobacco. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Tobacco at this period was also rolled up in the leaves of the Palm and smoked. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Tobacco is a strong growing plant resisting heat and drought to a far (p. 018) greater extent than most plants. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Botanists have enumerated between forty and fifty varieties of the tobacco plant who class them all among the narcotic poisons. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

European tobacco is lacking in flavor and is less powerful than the tobacco of America. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.