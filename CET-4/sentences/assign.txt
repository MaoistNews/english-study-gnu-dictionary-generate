It is designed to listen to meetings with multiple participants and will parse discussion patterns to produce informative synopses and assign post-meeting action items. In pursuit of pragmatic solutions to pervasive problems |Francesca Fanshawe |March 31, 2021 |MIT Technology Review 

Such randomized, double-blinded controlled trials randomly assign patients to receive a drug or a placebo, and don’t reveal to participants or doctors who is getting which. The arthritis drug tocilizumab doesn’t appear to help fight COVID-19 |Aimee Cunningham |October 23, 2020 |Science News 

That AI could pore over an astronaut’s symptoms and then recommend medical tests, make diagnoses and assign treatments. Surviving Mars missions will take planning and lots of innovation |Maria Temming |October 22, 2020 |Science News For Students 

So I rose beyond cleaning, to working as an operational dispatcher for cabin services in the American Airlines traffic control center, assign cleaning crews to each incoming aircraft. What I learned from 5 years of cleaning airplanes in the middle of the night |matthewheimer |August 30, 2020 |Fortune 

Ideally, the Mars spaceship would be equipped with artificial intelligence that could consider an astronaut’s symptoms, recommend medical tests, make diagnoses and assign treatments. What will astronauts need to survive the dangerous journey to Mars? |Maria Temming |July 15, 2020 |Science News 

Now the Kremlin will assign more loyal people to rule the region, mostly military leaders. Recession? Devaluation? Inflation? Putin Tells Russia Stay the Course. |Anna Nemtsova |December 4, 2014 |DAILY BEAST 

When we assign a primitive “not me” status to another individual or social group, it can—and does—take us down a destructive path. Ferguson, Immigration, and ‘Us Vs. Them’ |Gene Robinson |November 27, 2014 |DAILY BEAST 

Other folks can debate and assign blame for “who lost Iraq.” Iran Is the Biggest Loser in Iraq |Aki Peritz |June 15, 2014 |DAILY BEAST 

Renee Richardson knows she'll likely never be able to assign blame for her son's death—she's done fighting for that. What Military Base Shootings Reveal About the Mental Health Debate |Caitlin Dickson |February 9, 2014 |DAILY BEAST 

Girls are directed through several pages of this until they are asked to assign the guy a series of pre-decided adjectives. Meet Lulu: An App That Lets Girls Rate Guys Anonymously |Isabel Wilkinson |August 22, 2013 |DAILY BEAST 

The designs of Russia have long been proverbial; but the exercise of the new art of printing may assign them new features. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

With what honest pride did John Smith, the best farmer of them all, step to the fore and assign to each man his place! Dorothy at Skyrie |Evelyn Raymond 

If the lessee die, his executor or administrator can assign the remainder of his term. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

As the lessee may assign or sublet unless forbidden, so may the lessor part with his interest in the leased premises. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

If offered any dish of which you do not wish to partake, decline it, but do not assign any reason. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley