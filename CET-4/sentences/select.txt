That’s about enough to vaccinate everyone in the world, except those doses are in the hands of just a select few countries. Don’t hate the vaccine tourists, hate the vaccine game |Jen Kirby |February 12, 2021 |Vox 

Then select Option 1 to input the last six digits of your Social Security number as well as your Zip code. Tax season 2021: A tornado is coming |Michelle Singletary |February 12, 2021 |Washington Post 

Next, Pymetrics’s system compares the data from those 50 employees with game data from more than 10,000 people randomly selected from over two million. Auditors are testing hiring algorithms for bias, but there’s no easy fix |Amy Nordrum |February 11, 2021 |MIT Technology Review 

All the participants had to do was select the questions they were going to ask a conversation partner — again, from the pool of questions ranked by sensitivity. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

Adjust the heat by selecting one of the three levels based on the outside temperature, calculated for 5, 32, and 50 degrees Fahrenheit. Best hand warmers: Block the chill during your favorite winter activities |PopSci Commerce Team |February 10, 2021 |Popular-Science 

All it needs is one more “pipe” to select and transmit the crucial information. Red Tape and Black Boxes: Why We Keep ‘Losing’ Airliners in 2014 |Clive Irving |December 29, 2014 |DAILY BEAST 

You can even buy containers of their Cereal Milk in select stores. Cereal Cafe’s Big Bowl of Hate |David Levesley |December 14, 2014 |DAILY BEAST 

And, second, we already use sortition to select an important deliberative body, the trial jury. Is It Time to Take a Chance on Random Representatives? |Michael Schulson |November 8, 2014 |DAILY BEAST 

Until recently, being published was a pleasure afforded only to a select few. You Can Look It Up: The Wikipedia Story |Walter Isaacson |October 19, 2014 |DAILY BEAST 

Roark was a staffer on the House Permanent Select Committee on Intelligence, and focused on NSA oversight. Speed Read: James Risen Indicts The War On Terror’s Costly Follies |William O’Connor |October 14, 2014 |DAILY BEAST 

I do not care very much how you censor or select the reading and talking and thinking of the schoolboy or schoolgirl. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Of the methods given, the physician should select the one which best meets his needs. A Manual of Clinical Diagnosis |James Campbell Todd 

New proposals regarding telephone charges are expected as soon as the Select Committee has reported. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

There is no necessity for giving a table of all of their tones here; we select the two most useful. The Recent Revolution in Organ Building |George Laing Miller 

Then, as Man did not make nor select his power of choice, Man cannot be blamed if that power chooses evil. God and my Neighbour |Robert Blatchford