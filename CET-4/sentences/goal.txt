The goal is not just about being the Greatest Of All Time, Kan adds. With Goat Capital, Justin Kan and Robin Chan want to keep founding alongside the right teams |Eric Eldon |September 17, 2020 |TechCrunch 

“My goal as a board member is to try to make sure we, as a school system, learn from this mistake and get better as a result,” she wrote in an email. When a Calculus Class Abruptly Became Ceramics at Lincoln High |Scott Lewis |September 16, 2020 |Voice of San Diego 

Fed chair Jerome Powell first said last month that the Fed would seek inflation above 2% over time, rather than just keeping it as a static goal. Fed leaves short-term interest rates unchanged at nearly zero |Lee Clifford |September 16, 2020 |Fortune 

DeChiaro joined Golf nearly a year-and-a-half ago and came in with the goal of making Golf less dependent on ad revenue. ‘One endless loop’: How Golf is using its new retail marketplace as a first-party data play |Kayleigh Barber |September 16, 2020 |Digiday 

Key to that goal is hammering home his ban on travel from China and his administration’s work to provide ventilators. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

So however detailed the statistics of the battlefield are, they cannot achieve the goal. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

So, as far as Mexican officials like Peña Nieto are concerned, the goal is to keep their countrymen here — and keep them happy. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

The goal is to create a literary anatomy of the last century—or, to be precise, from 1900 to 2014. The 2014 Novel of the Year |Nathaniel Rich |December 29, 2014 |DAILY BEAST 

His goal: to make the perfect (and absolutely comfortable) high-heel, with the help from Nike CEO Mark Parker. What, and Who, You'll Be Wearing in 2015 |Justin Jones |December 27, 2014 |DAILY BEAST 

The NYPD remained his ultimate goal as he went to work as a carrier for Airborne Express/DHL and then as a school safety officer. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

I know I strive after the unattainable, but still every year I get nearer and nearer to the goal. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Here again the first thing necessary is a clear vision of the goal towards which we are to strive. The Unsolved Riddle of Social Justice |Stephen Leacock 

With a definite goal in mind at last, the children set out again with a better spirit. The Box-Car Children |Gertrude Chandler Warner 

With the next fellow as a goal, he gradually crept alongside, and passed him with a spurt. The Box-Car Children |Gertrude Chandler Warner 

He is a man who takes life seriously, and whom nothing will divert from the road that leads to the goal. Friend Mac Donald |Max O'Rell