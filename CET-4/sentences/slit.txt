Frog and toad pupils come in quite the array, from slits to circles. Frog and toad pupils mainly come in seven different shapes |Carolyn Wilke |August 24, 2021 |Science News 

To promote even cooking whether you’re roasting or steaming a whole fish, cut shallow slits into both sides. How to make Cantonese sizzling fish |Daniela Galarza |July 22, 2021 |Washington Post 

Tuck some of the larger pieces of scallion, ginger and citrus zest into each slit and slip the rest inside the cavity. How to make Cantonese sizzling fish |Daniela Galarza |July 22, 2021 |Washington Post 

The beam of the flashlight went through the crate slits, and two airport workers saw the light. Man seeks pals he once persuaded to ship him around the world in a crate |Cathy Free |April 16, 2021 |Washington Post 

Paired with Shift or Kingpin bindings, this lightweight setup is an ideal grab-and-go ski for in-bound uphillers or backcountry skiers looking for a starter ski—it even has skin-attachment slits at the tip and tail. Long-Term Review: The Best Skis, Period |Heather Schultz and Marc Peruzzi |March 4, 2021 |Outside Online 

More items came in time, as funding was available, like a slit-and-lace-up jacket and a line of boxer underwear. Look Who’s Wearing The Pants: Haute Butch’s Gender-Blending Style |Nina Strochlic |October 24, 2014 |DAILY BEAST 

Slice challah bread into 1.5 inches then slit hole in middle of each slice and fill with Nutella. Epic Meal Empire’s Meat Monstrosities: From the Bacon Spider to the Cinnabattleship |Harley Morenstein |July 26, 2014 |DAILY BEAST 

His name was Alexander, and he had a rifle in his hands, but the eyes you could see through the slit in the mask looked friendly. Held at Gunpoint by Ukraine Rebels |Anna Nemtsova |May 31, 2014 |DAILY BEAST 

That throat slit is so real, so jarring, and so matter-of-fact. Eli Roth, Director of ‘The Green Inferno,’ On His Favorite Bloody Movie Kills |Eli Roth |September 14, 2013 |DAILY BEAST 

First, Father Johannes Baptiste is found in the rectory, his throat slit. This Week’s Hot Reads: July 8, 2013 |Mythili Rao |July 8, 2013 |DAILY BEAST 

"I see," said Castle, closing his eyes and squinting through a slit between the lids. Scattergood Baines |Clarence Budington Kelland 

"Sh-h—be quiet," warned Betty, peeping again through the slit in the curtain. The Outdoor Girls in the Saddle |Laura Lee Hope 

I have recently fitted the lower half of the slit with a wind-screen, which has proved to be a most useful addition. Photographs of Nebul and Clusters |James Edward Keeler 

Occasionally an ear-like outgrowth appears on the neck, indicative of the attempt of a second slit to develop into an ear. Man And His Ancestor |Charles Morris 

Without clear sight of what he was fighting, he struck down with his knife and felt it slit flesh. Star Born |Andre Norton