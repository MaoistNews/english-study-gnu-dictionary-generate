Alberto Oggero’s 2018 vintage offers peach and apricot flavors along with an intriguing herbal note and a refreshing bone-dry finish. This $11 Bordeaux begs for pizza or burgers and will put a smile on your face |Dave McIntyre |December 18, 2020 |Washington Post 

This rosé is deceptively pale in hue—with a nose of bright citrus, as well as lush ripe peach, apricot, and blossom along with a dense palate and full body—and offers an experience that can be difficult to find in other styles. The best wines for Thanksgiving |Rachel King |November 21, 2020 |Fortune 

Luckily for the farm, summer peach season is typically much slower than the fall, only attracting an average of 5,000 visitors in a normal year. Pick Your Poison |Nick Mancall-Bitel |September 3, 2020 |Eater 

In the case of peaches, Wegmans bakery items containing peaches and Russ Davis Wholesale peach salsa were also recalled. The Salmonella Outbreaks Among Peaches and Onions, Explained |Jenny G. Zhang |August 28, 2020 |Eater 

Salmonella is “good at surviving under dry conditions as we might find on the surface of an onion or a peach,” says Donald Schaffner, a professor and extension specialist in food science at Rutgers University. The Salmonella Outbreaks Among Peaches and Onions, Explained |Jenny G. Zhang |August 28, 2020 |Eater 

And then I remembered a name and said, ‘I want a peach melba.’ Fashion Designer Oscar de la Renta, American Great, Dead at 82 |Tim Teeman |October 21, 2014 |DAILY BEAST 

In the Peach State, Michelle Nunn, the daughter of former Senator Sam Nunn, appears to have scratched out a tentative lead. What Do Women Want? Not the GOP |Lloyd Green |September 8, 2014 |DAILY BEAST 

There will be no Peach state Todd Akin after the disparate Tea Party strands in Georgia failed to produce a competitive candidate. Tea Party Flops in Georgia Senate Race |Patricia Murphy |May 19, 2014 |DAILY BEAST 

Sky and soft sunlight tint the snow blue, pink, lilac, peach. Visiting the Arctic Circle…Before It’s Irreversibly Changed |Terry Greene Sterling |April 1, 2014 |DAILY BEAST 

You still have to get a ball through a rim - even if a peach basket bottom no longer prevents it from dropping to the ground. Secret History of the First Dunk |Evin Demirel |February 15, 2014 |DAILY BEAST 

We there meet with the fruits of the torrid zone, and near them the apple and the peach of Europe. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

"I've got a peach," cried Mollie slangily, as her hand struck a big stone sharp enough to serve her purpose. The Outdoor Girls in the Saddle |Laura Lee Hope 

Better get some more peach-leaf pain-killer on your arm 'n' set straight down to breakfast. Overland |John William De Forest 

Surely not more than twenty years of age, of medium height, a peach complexion, tanned a little but fair to look at. David Lannarck, Midget |George S. Harney 

They then diluted the mass of fruit with raki, or peach brandy, and struggled home or to sleep as best they could. The British Expedition to the Crimea |William Howard Russell