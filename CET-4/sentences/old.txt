He also owns three of the top five seasons by average fastball velocity among players 30 and older. Jacob DeGrom Just Keeps Throwing Faster |Travis Sawchik |September 17, 2020 |FiveThirtyEight 

Meanwhile, a 20-something university student told me that the coronavirus now feels like old news to her peers. The new Covid-19 case surge in Europe, explained |Julia Belluz |September 17, 2020 |Vox 

Innovation is all about using new technology to improve old processes. How to drive digital innovation necessary during the pandemic |Nick Chasinov |September 16, 2020 |Search Engine Watch 

After 147 years, the Paris Cafe, one of the oldest bars in New York City, has poured its final cocktail. Bar Rescue: Pandemic Edition |jakemeth |September 15, 2020 |Fortune 

Then came the discovery of 279,000-year-old stone spear tips in Ethiopia. Let’s learn about ancient technology |Bethany Brookshire |September 15, 2020 |Science News For Students 

So here I am in my requisite Lululemon pants, grunting along to an old hip-hop song at a most ungodly hour. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

It would became one of the first great mysteries in the United States of America, as it was only then 23 years old. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

To borrow an old right-wing talking point, these people are angry no matter what we do. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

He plays an aging punk rocker and I play the drummer from his old band. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Twelve-year-old dance prodigy Maddie Ziegler has suffered the wrath of Dance Moms tyrant Abby Lee Miller. See Burly Shia LaBeouf Interpretive Cage Fight Lil Sia in the Singer’s Fantastic New Music Video |Marlow Stern |January 7, 2015 |DAILY BEAST 

Davy looked around and saw an old man coming toward them across the lawn. Davy and The Goblin |Charles E. Carryl 

His wife stood smiling and waving, the boys shouting, as he disappeared in the old rockaway down the sandy road. The Awakening and Selected Short Stories |Kate Chopin 

Old Mrs. Wurzel and the buxom but not too well-favoured heiress of the house of Grains were at the head of the table. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Vicars' wives had come and gone, but all had submitted, some after a brief struggle, to old Mrs. Wurzel's sway. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

But with all her advantages Miss Solomonson failed with the old lord, and she abuses him to this day. The Pit Town Coronet, Volume I (of 3) |Charles James Wills