My partner and I decided to grab a drink and a bite at a local wine bar, assuming we could order at the sidewalk window and then take the food to one of the high-tops set up in the street. People Are Dining Out, They Just Don’t Want Anyone to Know About It |Jaya Saxena |September 3, 2020 |Eater 

This tray has a slot for your phone, a reinforced book prop that folds down when not in use, wine glass holder, and double hooks to hang a washcloth, loofah, or razor. Bathtub trays that will keep you entertained and relaxed |PopSci Commerce Team |September 2, 2020 |Popular-Science 

In Boston, for example, they’ll get early check-in and late checkout, breakfast included, a bottle of wine, and a $50 credit toward other purchases. 5 businesses that pivoted to new business models creatively during the pandemic |Geoffrey Colvin |August 31, 2020 |Fortune 

We’ll miss celebrating with you and hope that we can cheers over a good glass of wine together sometime in the future. How to update your guests about your pandemic wedding plans |Brooke Henderson |August 29, 2020 |Fortune 

For example, The Fat Jewish built the wine brand Babe Rose and sold that to Anheuser-Busch for last June. ‘A different language’: Why venturing into meme-based marketing can be risky for brands |Kristina Monllos |August 28, 2020 |Digiday 

I guess we know how Bacchus kept his title as the god of wine and intoxication. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

The possibilities seem endless: Who needs a trip to the liquor store when the toddler can turn water into wine, amirite? Was Baby Jesus A Holy Terror? |Candida Moss |December 21, 2014 |DAILY BEAST 

All Champagne is sparkling wine but not all sparkling wine is Champagne. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

“Enjoying the bubbles is as important as enjoying the wine,” Goldston says. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

The wine cellar—one of the best in the world—survived World War II and is guarded around the clock. Inside The World’s 10 Oldest Restaurants |Justin Jones |December 20, 2014 |DAILY BEAST 

HE ordered a lunch which he thought the girl would like, with wine to revive the faculties that he knew must be failing. Rosemary in Search of a Father |C. N. Williamson 

And when wine had unselfed my noble father, you received his passionate insults with forbearance and forgiveness! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

“Let us have some of your best wine to-day, waiter,” said old Wardle, rubbing his hands. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

He smoked two cigars; then he went inside and drank another glass of wine. The Awakening and Selected Short Stories |Kate Chopin 

Aristide called on Madame Coquereau, who entertained him with sweet Frontignan wine, dry sponge cakes and conversation. The Joyous Adventures of Aristide Pujol |William J. Locke