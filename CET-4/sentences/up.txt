“I think for trans men who are dating every time they hook up they have another coming out,” Sandler said. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

In that photo, Merabet has a big smile that spreads across his whole face and lights up his eyes. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 

We won't find out this season, though it comes up occasionally. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Kickstarter is one start-up platform that seems to have realized the danger. Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 

The most recent issue contains detailed instructions for building car bombs, and the magazine frequently draws up hit-lists. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

What need to look to right or left when you are swallowing up free mile after mile of dizzying road? The Joyous Adventures of Aristide Pujol |William J. Locke 

Most of the men leaped up, caught hold of spears or knives, and rushed out. The Giant of the North |R.M. Ballantyne 

Some weeks after, the creditor chanced to be in Boston, and in walking up Tremont street, encountered his enterprising friend. The Book of Anecdotes and Budget of Fun; |Various 

In less than ten minutes, the bivouac was broken up, and our little army on the march. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The bride elect rushes up to him, and so they both step down to the foot-lights. Physiology of The Opera |John H. Swaby (AKA "Scrici")