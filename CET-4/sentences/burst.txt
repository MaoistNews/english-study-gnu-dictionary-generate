Astrophysicists had thought that if a magnetar ever exploded, it would release one of the highest bursts of energy ever seen in the universe. Exploding neutron star proves to be energy standout of the cosmos |Lisa Grossman |February 12, 2021 |Science News For Students 

If you’re active in short bursts, a reusable, chemically activated option might be right for you. Best hand warmers: Block the chill during your favorite winter activities |PopSci Commerce Team |February 10, 2021 |Popular-Science 

Another line of evidence against a sudden lake burst is that “there were no lakes of any size visible” in the satellite images taken over the region. Three things to know about the disastrous flood in India |Carolyn Gramling |February 9, 2021 |Science News 

Apart from the occasional burst of energy, which propels me through a flurry of cooking, the joy has seeped from my kitchen. Love, Loneliness, and the Chicken in My Freezer |Elazar Sontag |February 9, 2021 |Eater 

Maryland responded, building a six-point advantage, but Ohio State followed the same script with a quick burst of three-pointers and led 35-30 by halftime. Maryland misses a chance to boost its NCAA tournament hopes with a loss to No. 4 Ohio State |Emily Giambalvo |February 9, 2021 |Washington Post 

The gunman then burst from the restaurant and fled down the street with the other man. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Within a few swipes, I was already feeling that burst of romantic optimism you need the first day of the (Christian) new year. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

“So, about good for one tactical burst,” the first Air Force official said. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

People will always scratch and save if a sudden burst of unrestrained pleasure can be purchased. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

He was in the bathroom, perhaps trying to flush some pot down the toilet, when a cop burst in. ‘I Can’t Breathe!’ ‘I Can’t Breathe!’ A Moral Indictment of Cop Culture |Michael Daly |December 4, 2014 |DAILY BEAST 

He burst into a loud laugh, clapped his hands, and danced before the delighted babe. The Joyous Adventures of Aristide Pujol |William J. Locke 

After a moment's silence, the cavaliers both burst into a gay laugh. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

A burst of indignation within seemed to do more for him than the outward buffetings. The Giant of the North |R.M. Ballantyne 

Some hidden magnetism burst from him like an aura, and his cold pasty face and light gray eyes flamed into positive beauty. Ancestors |Gertrude Atherton 

It burst upon them ere long with awful fury and grandeur, the elements warring with incredible vehemence. Hunting the Lions |R.M. Ballantyne