Root cells release malic acid, which acts like a shepherd’s whistle. Junk Food Is Bad For Plants, Too - Issue 90: Something Green |Anne Bikl&#233; & David R. Montgomery |September 23, 2020 |Nautilus 

Influencers, who are paid by brands to promote their products to their thousands of followers, generally agree that Reels lacks some of the bells and whistles of TikTok. Instagram’s would-be TikTok killer, Reels, struggles to gain traction |Danielle Abril |September 22, 2020 |Fortune 

There’s no whistle, bell, or any other serious notification of when the harmless pre-ejaculate ends and the fluids of fatherhood begin. Does Pre-Confessing Clear You of Sexual Causality? |Eugene Robinson |September 20, 2020 |Ozy 

Snarky Alexis may have had a ritzy Rolls-Royce, but today’s Sentra—completely redesigned for 2020—offers a lot more bells and whistles. Snazzy sedans and comfort rides |Joe Phillips |September 19, 2020 |Washington Blade 

Yet offense continued to rule even once the whistles became less frequent. Why Have NBA Offenses Been So Good In The Bubble? |Mike Prada |August 20, 2020 |FiveThirtyEight 

“Clean as a whistle,” says a senior investigator involved in the case. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

But admit it: at the first whistle, we all paid attention, to a part of the world that would usually prefer us all to butt out. Pyongyang Primer: Kenneth Bae Comes Home |Kevin Bleyer |November 15, 2014 |DAILY BEAST 

He is on trial along with three others, and Bogucki is blowing the whistle on government practices he says are not fair play. A Navy Lawyer Cries Foul on Gitmo’s Kafkaesque Legal System |Eleanor Clift |September 26, 2014 |DAILY BEAST 

Not a lot of air tooting that whistle, no matter what the video would lead viewers to believe. Anti-Vaxxers Have a New Hero |Russell Saunders |September 1, 2014 |DAILY BEAST 

And yep, the flag can be used in dog whistle fashion to signal a position on “those blacks.” We've Got Bigger Problems Than a Confederate Flag |John McWhorter |August 28, 2014 |DAILY BEAST 

Robert uttered a shrill, piercing whistle which might have been heard back at the wharf. The Awakening and Selected Short Stories |Kate Chopin 

Goodell paused in the doorway and emitted a whistle of surprise at sight of a horse in one of the stalls. Raw Gold |Bertrand W. Sinclair 

Then he goes out, gits into his Pullman section, blows his punkin whistle and departs. Alec Lloyd, Cowpuncher |Eleanor Gates 

He only tore himself from her reluctant arms as the final whistle sounded from the engine. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

In a lull of the raging earth the distant whistle of the train could be distinctly heard. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward