Mulan is the story of 1,500 years of shifting ideas about gender and virtue. The history of Mulan, from a 6th-century ballad to the live-action Disney movie |Constance Grady |September 4, 2020 |Vox 

They also found, by analyzing speeches from Senate floor proceedings coded for virtue and vice signals,6 that United States senators were higher in the Dark Triad than the general population—which makes sense, given how competitive politics can be. Are You Yoda or Darth Vader? - Issue 89: The Dark Side |Brian Gallagher |August 26, 2020 |Nautilus 

I’ve spent a good bit of my career arguing for the virtues of solitude. That chatbot I’ve loved to hate |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Now, declared covid vulnerable by virtue of age, I was not just alone but afraid. That chatbot I’ve loved to hate |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Interestingly enough, by virtue of this very unobtrusive nature of the Pilos helmet, it was also used by lighter troops, such as the archers employed by Athens. Know Your Ancient Greek Helmets: From Attic to Phrygian |Dattatreya Mandal |May 19, 2020 |Realm of History 

We see detoxing as a path to transcendence, a symbol of modern urban virtue and self-transformation through abstinence. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

Advocates claimed that it helped to preserve virtue and to affirm the application of Sharia law. Saudi Activist Manal Al-Sharif on Why She Removed the Veil |Manal Al Sharif, Advancing Human Rights |October 30, 2014 |DAILY BEAST 

For these self-righteous and thin-skinned folks, there are apparently limits to the liberal virtue of tolerance. Pew Study: Americans Are Self-Segregating Amid Proliferating Partisan Media |John Avlon |October 21, 2014 |DAILY BEAST 

By virtue of being readers we are also writers, I now believe, but that was not always the case. Book Bag: Overlooked Classic Books From the Sunshine State |Randy Wayne White |September 30, 2014 |DAILY BEAST 

He calmly offered his vision of an ideology that merges libertarian values with social conservative virtue. Paul, Cruz Duel at ‘Values Voter’ Event |Olivia Nuzzi |September 26, 2014 |DAILY BEAST 

When we speak against one capital vice, we ought to speak against its opposite; the middle betwixt both is the point for virtue. Pearls of Thought |Maturin M. Ballou 

And it would be hard indeed, if so remote a prince's notions of virtue and vice were to be offered as a standard for all mankind. Gulliver's Travels |Jonathan Swift 

Why expect that extraordinary virtues should be in one person united, when one virtue makes a man extraordinary? Pearls of Thought |Maturin M. Ballou 

She may be as chaste as unsunned snow, she is certainly as cold: but for warm, inspiring virtue! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The smiling face of man was blotted out; gratitude, virtue, were annihilated; and life had no longer an object! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter