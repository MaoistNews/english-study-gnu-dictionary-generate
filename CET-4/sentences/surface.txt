Then tipped my feet up, pushed below the surface and held my breath, listening. A Maui vacation in three acts |Alex Pulaski |February 12, 2021 |Washington Post 

Meticulous in construction, Wu’s patterns nonetheless slip, slide and oscillate — much like images made by simply brushing paint onto a surface. In the galleries: Rejuvenating the obsolete into unconventional art |Mark Jenkins |February 12, 2021 |Washington Post 

Three and a half billion years ago, all but a couple percent of the Earth’s surface was deep underwater, researchers have estimated, and LUCA lived far before that. Your ancestors might have been Martians |Charlie Wood |February 12, 2021 |Popular-Science 

Make sure your mask stays folded so the inside doesn’t touch any other surface. Why you shouldn’t ever wear your mask around your neck |Erin Fennessy |February 11, 2021 |Popular-Science 

After entering orbit, it will begin a landing attempt with the aim to place the rover on the surface in the massive impact basin Utopia Planitia. Check out the first images of Mars from China’s Tianwen-1 probe |Erin Fennessy |February 10, 2021 |Popular-Science 

The more resources and education society becomes equipped with, the fewer stories like yours will surface. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

The most exciting and thrillingly unique artist to surface in 2014. The 10 Best Albums of 2014: Taylor Swift, Sia, Run the Jewels, and More |Marlow Stern |December 28, 2014 |DAILY BEAST 

Set a heatproof bowl over a pot of gently simmering water, making sure that the bowl does not touch the surface of the water. Carla Hall’s Christmas Day Treat: Rum Balls |Carla Hall |December 25, 2014 |DAILY BEAST 

You can go as deep as you like, or float about on the surface. D’Angelo’s ‘Black Messiah’ Was Worth Waiting 15 Years For |James Joiner |December 16, 2014 |DAILY BEAST 

Nicki treats the obsession with her pop ambitions as an irrelevant, surface-level irritation. Nicki Minaj Bares Her Own Vulnerability on ‘The Pinkprint’ |Rawiya Kameir |December 16, 2014 |DAILY BEAST 

First a shower of shells dropping all along the lower ridges and out over the surface of the Bay. Gallipoli Diary, Volume I |Ian Hamilton 

Some of those halls that Mr. Meadow Mouse mentioned ran right out beneath the surface of the garden. The Tale of Grandfather Mole |Arthur Scott Bailey 

It depends upon the fact that bile acids lower surface tension. A Manual of Clinical Diagnosis |James Campbell Todd 

She did shout for joy, as with a sweeping stroke or two she lifted her body to the surface of the water. The Awakening and Selected Short Stories |Kate Chopin 

Its vitals were going—were gone, before the smallest indications of mischief appeared upon the surface. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various