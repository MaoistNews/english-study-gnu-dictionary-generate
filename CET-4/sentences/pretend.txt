Let’s pretend that politicians wake up and don’t reopen restaurants and we avoid a big wave in March. Why Opening Restaurants Is Exactly What the Coronavirus Wants Us to Do |Caroline Chen |February 6, 2021 |ProPublica 

After a certain point, you can’t even pretend that you have control. This is how we lost control of our faces |Karen Hao |February 5, 2021 |MIT Technology Review 

We should not pretend that the Facebook Oversight Board is more than a McGuffin designed to distract us from serious issues. Facebook's "Oversight Board" Is a Sham. The Answer to the Capitol Riot Is Regulating Social Media |Maria Ressa |January 28, 2021 |Time 

It’s terrible, and you constantly have to pretend you like it. Why You Don't Feel as Fulfilled From Your Job as You Think You Should |Eliana Dockterman |January 25, 2021 |Time 

I wasn’t, but I pretended otherwise, just to see if Pennyroyal Station followed through on requests. Like a good neighbor, Pennyroyal Station is there for you |Tom Sietsema |January 22, 2021 |Washington Post 

For Kirke it was being paid to pretend to play the oboe that heightened her affair with classical music. ‘Mozart in the Jungle’: Inside Amazon’s Brave New World of Sex, Drugs, and Classical Music |Kevin Fallon |December 23, 2014 |DAILY BEAST 

The irony has thinned with the economy, perhaps: Who can really afford just to pretend to DIY today? Glenn Beck Is Now Selling Hipster Clothes. Really. |Ana Marie Cox |December 20, 2014 |DAILY BEAST 

Sure, some parents would rather pretend their kids would never look at that stuff. The Next Frontier of Sex Ed: How Porn Twists Teens’ Brains |Aurora Snow |November 29, 2014 |DAILY BEAST 

Quickly, the lines between their pretend feelings for each other and their real ones are blurred. Team Peeta or Team Gale: Why the ‘Hunger Games’ Love Triangle Ruins ‘Mockingjay – Part 1’ |Kevin Fallon |November 28, 2014 |DAILY BEAST 

Or: “Jazz: Just pretend you like it, that's what everyone else is doing.” What’s With This Uncool Surge in Jazz Bashing? |Ted Gioia |November 2, 2014 |DAILY BEAST 

A quite young child will, for example, pretend to do something, as to take an empty cup and carry out the semblance of drinking. Children's Ways |James Sully 

I believe I murmured something suitable, but it was absurd to pretend to be overjoyed at the news. Uncanny Tales |Various 

That my aspirations were satisfied I do not pretend, for ambition forbade any settled feeling of rest or content. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

I've only known Indian rivers for five and twenty years, and I don't pretend to understand. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

We shall have to pretend to do some gun practice, and drop a shell on to its surface to find out. Gallipoli Diary, Volume I |Ian Hamilton