Meanwhile, almost exactly 30 years after the trial, the judge left his home to board a steamboat and was never heard from again. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

Again, the difference can seem subtle and sound more like splitting hairs, but the difference is important. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

Then they came up against a police patrol on mountain bicycles, which again led to more shooting, without injuries. Police Hunt for Paris Massacre Suspects |Tracy McNicoll, Christopher Dickey |January 7, 2015 |DAILY BEAST 

At a press conference today with Scalise, Speaker Boehner again defended him. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

We take enough time off in between so that our batteries get recharged and we get inspired again. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Each day she resolved, "To-morrow I will tell Felipe;" and when to-morrow came, she put it off again. Ramona |Helen Hunt Jackson 

And that was that if he and his wife were to ever live together again and be happy, the family were to be kept out of it. The Homesteader |Oscar Micheaux 

Before he could finish the sentence the Hole-keeper said snappishly, "Well, drop out again—quick!" Davy and The Goblin |Charles E. Carryl 

"Better so," was the Senora's sole reply; and she fell again into still deeper, more perplexed thought about the hidden treasure. Ramona |Helen Hunt Jackson 

Before the outlaw can comply with this small request the horn sounds again. Physiology of The Opera |John H. Swaby (AKA "Scrici")