The likes of Coffman are “probably” already halfway wards of the state anyhow. The FBI’s Bogus ISIS Bust |James Poulos |November 21, 2014 |DAILY BEAST 

In almost none of these circumstances does Lyme disease seem likely, but everyone wants testing done anyhow. Predator Doctors Take Advantage of Patients With ‘Chronic Lyme’ Scam |Russell Saunders |September 19, 2014 |DAILY BEAST 

Let him write; he probably understands it all better than I do, anyhow. The Stacks: Grateful Dead I Have Known |Ed McClanahan |August 30, 2014 |DAILY BEAST 

You might not have been so cheap this year and accepted a hand-me-down (I mean ALL they want is the candy anyhow). Ding Dong, You Have Lice |Kent Sepkowitz |October 31, 2013 |DAILY BEAST 

Anyhow, they realized, Assad had also lost his legitimacy as a ruler. What Israel Really Thinks About Syria |Yaakov Katz |May 23, 2013 |DAILY BEAST 

If it did come to that—and of course it would—I'd get out anyhow, so I may as well go now and save myself further horrors. Ancestors |Gertrude Atherton 

I have recently found out that she was christened Tabitha—or, anyhow, would have been, if the clergyman had known his job. First Plays |A. A. Milne 

If he puts in clothing it'll cost me five hundred dollars a year in profits, anyhow. Scattergood Baines |Clarence Budington Kelland 

And most of them have gone under anyhow—in the cheerful California fashion: three generations from shirt sleeves to shirt sleeves. Ancestors |Gertrude Atherton 

I reckon you're the one Bolivar an' me's been waitin' here fer, an'—what's the matter with ye, anyhow? Motor Matt's "Century" Run |Stanley R. Matthews