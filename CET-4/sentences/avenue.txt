You hurry out of the neighborhood, across the avenue that divides one section of town from another, relieved when you reach home. The first murder |Katie McLean |August 19, 2020 |MIT Technology Review 

There are other avenues in biotechnology beyond gene editing that may help reduce the cattle industry’s footprint. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

So mathematicians hoped that Bateman and Katz’s breakthrough might offer an avenue into proving the Erdős conjecture, especially when combined with other recent advances. Landmark Math Proof Clears Hurdle in Top Erdős Conjecture |Erica Klarreich |August 3, 2020 |Quanta Magazine 

The Coronado City Council could decide this month whether to pursue a legal challenge, and the best avenue to do so. Politics Report: Mara Elliott, Plumber |Scott Lewis and Andrew Keatts |August 1, 2020 |Voice of San Diego 

Economists have long pointed to home-ownership as a major avenue for building family wealth. Should America (and FIFA) Pay Reparations? (Ep. 426) |Stephen J. Dubner |July 16, 2020 |Freakonomics 

The police learned that Kemp worked in a grocery on Decatur Avenue. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

They were looping back around and coming down Tiebout Avenue when they spotted two figures. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

He then escaped from his detention and arrived on Tverskaya Avenue to join his supporters. Russia’s Rebel In Chief Escapes House Arrest |Anna Nemtsova |December 30, 2014 |DAILY BEAST 

In the mid-afternoon, Ramos and Liu were parked on Tomkins Avenue on a meal break. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

And the bells chimed for victory at 1211 Avenue of the Americas. Why I’m for the War on Christmas |Asawin Suebsaeng |December 23, 2014 |DAILY BEAST 

He left the arabyieh at the western entrance and went on foot down the avenue of headless rams. The Wave |Algernon Blackwood 

Aristide was young, he loved flirtation, and flirtation flourished in the Avenue des Plantanes. The Joyous Adventures of Aristide Pujol |William J. Locke 

The stout brigadier grunted an assent and rolled monumentally down the Avenue. The Joyous Adventures of Aristide Pujol |William J. Locke 

He walked briskly down the avenue, and Hedges stood looking after him, slightly puzzled in his mind. Elster's Folly |Mrs. Henry Wood 

They joined in bands of youths and maidens and whirled down the Avenue in Bacchic madness. The Joyous Adventures of Aristide Pujol |William J. Locke