Scaling up sheep meant keeping more and feeding them in winter on bought in concentrate feeds. American Agriculture Almost Ruined My Little English Farm. Now I'm Trying to Save It |James Rebanks |August 5, 2021 |Time 

If you drink from a flute, do so from a tulip-shape one to concentrate the notes, Simonetti-Bryan says. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

He did suffer from ‘Black Dog’ [depression] as he called it and having something to concentrate on was therapeutic for him. Churchill’s Secret Treasures for Sale: A British PM’s Life on the Auction Block |Tom Teodorczuk |December 8, 2014 |DAILY BEAST 

She struggled to concentrate on crossword puzzles and read books, so she just watched television. Jeopardy! Champion Julia Collins’s Brain Feels Like Mush |Sujay Kumar |November 20, 2014 |DAILY BEAST 

In times of crisis, President Obama can be counted on to concentrate power in the White House. Ron Klain Will Be the Best Ebola Czar Yet |Tim Mak, Abby Haglage |October 17, 2014 |DAILY BEAST 

The Guard will concentrate its resources on carrying out this limited mission. Can the National Guard Really Help Calm an Already Militarized Ferguson? |Jacob Siegel |August 19, 2014 |DAILY BEAST 

He was trying hard to concentrate his mind upon the sweet and common things of life. Three More John Silence Stories |Algernon Blackwood 

I dont know that I care for chess; I can not concentrate my attention as I could a year ago. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

Hence the alternative has been to concentrate the "less eligibility" on the conditions of the pauper's mental life. English Poor Law Policy |Sidney Webb 

They were evidently desirous only of gaining time by negociation to assemble and concentrate their forces. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Citizens, by birth or choice of a common country, that country has a right to concentrate your affections. Key-Notes of American Liberty |Various