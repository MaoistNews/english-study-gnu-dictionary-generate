He’s allowing that perhaps the prosecutors mean well, but as the Lewis quote suggests, even those good intentions can give way to overzealousness. William Barr’s eyebrow-raising ‘robber barons’ metaphor for the Trump era |Aaron Blake |September 17, 2020 |Washington Post 

Survey respondents also indicated a corresponding intention to spend more with small businesses. Facebook Business Suite is a new cross-app management tool for SMBs |Greg Sterling |September 17, 2020 |Search Engine Land 

Custom affinity audiences with keywords will convert to “People with any of these interests or purchase intentions,” as will Display custom intent audiences. Google custom audiences, the combo of custom affinity and custom intent audiences, now live |Ginny Marvin |September 14, 2020 |Search Engine Land 

And, yes, you can create an intention to meditate, drink more water, and start journaling. How a vacation—or a pandemic—can help you adopt better habits now |matthewheimer |September 12, 2020 |Fortune 

Of course, the model has no intention to deceive or convince. Welcome to the Next Level of Bullshit - Issue 89: The Dark Side |Raphaël Millière |September 9, 2020 |Nautilus 

My intention is to make it with the same approach as Anchorman 2. Inside Sony’s ‘Pineapple Express 2 Drama’: Leaked Emails Reveal Fight Over Stoner Comedy Sequel |William Boot |December 21, 2014 |DAILY BEAST 

One morning at about eleven, he announces his intention as though it's truly an unusual thought: “Let's have a little drink.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

“The intention is to create a 21st century auction house,” Gilkes says. William, Kate, and Jay Z’s Favorite Art Star: Alexander Gilkes' World of Rock Stars and Royalty |Tim Teeman |December 10, 2014 |DAILY BEAST 

“Hollywood dilutes material to a level I have no interest in, or intention following,” Wiseman says firmly. Inside The Secret World of London’s National Gallery |Tim Teeman |November 8, 2014 |DAILY BEAST 

One reason for withholding information was the president had no intention of shutting down the initiative. How the Reagan White House Bungled Its Response to Iran-Contra Revelations |Malcolm Byrne |November 3, 2014 |DAILY BEAST 

The two women had no intention of bathing; they had just strolled down to the beach for a walk and to be alone and near the water. The Awakening and Selected Short Stories |Kate Chopin 

There is more of artfulness in the flatteries which appear to involve a calculating intention to say the nice agreeable thing. Children's Ways |James Sully 

However this be, it is hard to say that these fibs have that clear intention to deceive which constitutes a complete lie. Children's Ways |James Sully 

It was never the intention of the Federal Reserve Act that member banks should continue the maintenance of these reserve accounts. Readings in Money and Banking |Chester Arthur Phillips 

Was she merely an egoist—it ran in the family—or did it conceal much that she had no intention of revealing? Ancestors |Gertrude Atherton