It’s inexpensive enough to outfit an entire cub-scout troop, and even comes with three AA batteries. Flashlights for emergency scenarios and outdoor fun |PopSci Commerce Team |August 27, 2020 |Popular-Science 

Grant said that as Cleveland scouts spoke with Civale in college, they came away impressed with how the CAA All-Academic standout at Northeastern University could recall every pitch from his recent starts. Cleveland’s League-Leading Rotation Relies On Homegrown Talent … From A Single Draft |Travis Sawchik |August 25, 2020 |FiveThirtyEight 

These scout for infrared light — a type the human eye can’t see. Developing planet emerges in a swirl of gas |Lisa Grossman |July 6, 2020 |Science News For Students 

Superconducting chips interface with those cavities and are used to perform operations on the qubits and scout for errors. To live up to the hype, quantum computers must repair their error problems |Emily Conover |June 22, 2020 |Science News 

However, Ledbetter noted that “scouts have already been scouting these athletes for two years.” NFL Decision to Stick to Draft Date May Penalize Some ‘Diamonds in the Rough’ |Shirley Carswell |April 23, 2020 |TruthBeTold.news 

The brokers then scout out potential “crew members” who can earn substantial discounts for working the journey. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

We did a movie down in Durango — Great Scout and Cat House Thursday. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

From there, he led groups of Kansa and Osage to scout for Spanish garrisons. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

One was about Girl Scout cookies and the other was aptly titled, “I Love It.” Is John Mulaney the Next Seinfeld? |Kevin Fallon |October 5, 2014 |DAILY BEAST 

The Boy Scout that a people thought they knew does not exist. The Religious Right's 'Nice Guy' Who Threw His Wife Under the Bus |Patricia Murphy |September 5, 2014 |DAILY BEAST 

"You must promise never again to leave without permission, or this is your last scout with me," said Harry, sternly. The Courier of the Ozarks |Byron A. Dunn 

So whilst we was eatin' breakfast I begins t' quiz, an', one way an' another, lets on I wanted t' see that Injun scout. Raw Gold |Bertrand W. Sinclair 

The periscope is the one part of the submarine scout equipment that is open to vision from the surface. The Wonder Book of Knowledge |Various 

I can imagine a scout standing here sending up smoke signals. Mystery Ranch |Arthur Chapman 

Cousin was there, seated and his head bowed on his chest, a waiting victim for the first Indian scout who might happen along. A Virginia Scout |Hugh Pendexter