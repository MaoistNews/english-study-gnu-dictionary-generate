Suzuki sold a few, mostly to owners who must’ve parked the things under a cover once the honeymoon was over and reality set in. 22 of the weirdest concept motorcycles ever made |By John Burns/Cycle World |September 10, 2020 |Popular-Science 

Aké should provide solid cover when needed, but the center back City really wants is Napoli’s Kalidou Koulibaly. Will Liverpool Run Away With The Premier League Again, Or Can Manchester City Take The Title Back? |Terrence Doyle |September 10, 2020 |FiveThirtyEight 

Megaflashes form within vast networks of thunderstorms and cloud cover called mesoscale convective systems. Readers ask about neutrinos in the sun’s core, megaflashes and mussels |Science News Staff |September 6, 2020 |Science News 

It can support up to three riders and features a “kwik-connect” hook-up system for an easy connection to you towline, a heavy-duty nylon cover, and a speed safety valve for easy inflation. The best boating tubes |PopSci Commerce Team |September 3, 2020 |Popular-Science 

This magic maker is practically all blade, but not to worry, it comes with a protective cover for safe handling and a sleekly designed handle so you can get a good grip. Pizza cutters that will get you the slice of your dreams |PopSci Commerce Team |September 2, 2020 |Popular-Science 

They took cover inside a print works to the north east of Paris, where they held a member of staff as a hostage. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

Clad in a blue, striped button-down, a silver watch adorning his left wrist, Huckabee beams on the cover. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

Until concern trolls like Sarah Ditum came along trying to cover it up again. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

Jourdan Dunn is the first sole black woman to feature on a British ‘Vogue’ cover in 12 years. One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

When it was announced that Jourdan Dunn would be the first black model to cover British Vogue in twelve years it made me sad. One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

All elements of expression modify each other, so that no mere rule can cover all cases. Expressive Voice Culture |Jessie Eldridge Southwick 

The Vine is a universal favorite, and rarely out of view; while it often seems to cover half the ground in sight. Glances at Europe |Horace Greeley 

He thus decoyed them away, and the fortunate couple were enabled to reach the British lines under cover of the darkness. The Red Year |Louis Tracy 

Given one more Division we might try: as things are, my troops won't cover the mileage. Gallipoli Diary, Volume I |Ian Hamilton 

Here there was a scuffling sound in the basket, and the Roc rapped on the cover with her hard beak, and cried, "Hush!" Davy and The Goblin |Charles E. Carryl