Knowing the nova rate is vital for determining how much these explosions have contributed to the galaxy’s chemical makeup by creating new elements. The number of Milky Way nova explosions per year has been pinned down |Ken Croswell |February 12, 2021 |Science News 

Experts worry Super Bowl could trigger coronavirus explosion. Super Bowl highlights: Bucs celebrate championship, Tom Brady wins MVP |Des Bieler, Mark Maske, Chuck Culpepper |February 8, 2021 |Washington Post 

Experts worry Super Bowl parties could trigger coronavirus explosion. What you need to know about Super Bowl LV |Sam Fortier |February 7, 2021 |Washington Post 

Kling was one of the first county workers to arrive at the scene of a massive explosion and fire at a Silver Spring apartment complex in August 2016. Blanca Kling, a pillar in the Latino community who helped thousands of crime victims, dies of complications of covid-19 |Luz Lazo |February 5, 2021 |Washington Post 

Ending the Fairness Doctrine was one key factor leading to an explosion of right-wing radio programming in the 1990s — though sometimes an overstated one. The Fairness Doctrine won’t solve our problems — but it can foster needed debate |Victor Pickard |February 4, 2021 |Washington Post 

Pleasure shoots magically in every direction like an explosion of sparks. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

At least two people were injured in the explosion but no fatalities have been reported. Fireworks Factory Explodes in Colorful Burst |The Daily Beast Video |January 6, 2015 |DAILY BEAST 

But since that explosion of popularity, Angry Birds has become about everything else. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

Even a relatively small 250-pound bomb could kill or injure friendly troops who are within 650 feet of the explosion. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

Now is hardly the time to be doing anything on the so-called peace process other than trying to avoid an explosion on the ground. Why We Should Delay The Israel-Palestinian Peace Process |Aaron David Miller |December 19, 2014 |DAILY BEAST 

Every light in the hall was ablaze; every lamp turned as high as it could be without smoking the chimney or threatening explosion. The Awakening and Selected Short Stories |Kate Chopin 

And when Lessard flung out that last unthinkable accusation, the explosion came. Raw Gold |Bertrand W. Sinclair 

Ribsy snapped at it, and swallowed it, and the next instant disappeared with a tremendous explosion in a great cloud of smoke. Davy and The Goblin |Charles E. Carryl 

An explosion took place in the Blackheath coal mines, Virginia; by which of the twenty-three workmen only one escaped death. The Every Day Book of History and Chronology |Joel Munsell 

The Turks fired a mine under Quinn's Post and then rushed a section of the defence isolated by the explosion. Gallipoli Diary, Volume I |Ian Hamilton