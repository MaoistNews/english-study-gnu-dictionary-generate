In 1950, in the suburbs of New Jersey, researchers at Bell Labs were busy making breakthroughs that paved the path for the first practical solar cells. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

Western Pomerania, Dahlemann says, is becoming a suburb of Szczecin. Eastern Germany Picks Up Polish for Children’s Future |Charu Kasturi |September 8, 2020 |Ozy 

These activities aren’t usually allowed within cities and suburbs. Are coyotes moving into your neighborhood? |Kathryn Hulick |September 3, 2020 |Science News For Students 

In other words, drone deliveries are likely not coming to densely-packed suburbs anytime soon. Amazon is one big step closer to delivering packages by drone |Rob Verger |September 2, 2020 |Popular-Science 

Polling data suggests it is likely that this election will be decided in the suburbs. Explainer: What do political databases know about you? |Tate Ryan-Mosley |August 31, 2020 |MIT Technology Review 

It looks like the shoot-out at the OK Corral in a leafy Eindhoven suburb—but nobody seems to be a very good shot. Dutch Biker Gangs Vs. ISIS |Nadette De Visser, Christopher Dickey |December 9, 2014 |DAILY BEAST 

By September, he was flashing a thumbs-up to assembled fans as he walked into court in a Barcelona suburb. Is Soccer Great Lionel Messi Corrupt? |Jack Holmes |December 8, 2014 |DAILY BEAST 

On a recent trip to a rough Roman suburb, he apologized for the extra police protection. The Pope’s Risky Trip |Barbie Latza Nadeau |November 21, 2014 |DAILY BEAST 

And as this map shows, there are now five Soofas in Boston proper as well as two in the suburb of Babson. Parks and Regeneration |The Daily Beast |November 3, 2014 |DAILY BEAST 

Stephen Knolls School is a public school in a Maryland suburb of Washington DC. Magical Gardens for the Blind, Deaf, and Disabled |Elizabeth Picciuto |October 22, 2014 |DAILY BEAST 

The city and commercial suburb of Binondo wore their usual aspect, although trade was almost at a standstill. The Philippine Islands |John Foreman 

Parochial church of Santiago, a suburb of Manila, and the souls cared for therein. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

On February 18th the French captured the suburb on the left bank of the river, and thus placed the inner town between two fires. Napoleon's Marshals |R. P. Dunn-Pattison 

It was one of a terrace of three that stood high above the suburb, close to the elm-tree walk overlooking the West Heath. The Creators |May Sinclair 

This part is really the city; beyond is a suburb laid out in gardens densely inhabited. Ways of War and Peace |Delia Austrian