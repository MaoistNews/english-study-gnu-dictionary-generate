Other measures include better access to testing for family or household members of players. NHL adds game-day rapid testing to coronavirus protocols |Samantha Pell |February 12, 2021 |Washington Post 

Notre Dame players were greeted by family members on the field after the game, rather than before. For this college football team, covid means the season starts in February — with Senior Day |Glynn A. Hill |February 11, 2021 |Washington Post 

The Pro Football Hall of Fame named eight new members last weekend, including legendary quarterback Peyton Manning. Kenny Washington’s time has come. NFL needs to recognize the man who broke the color barrier. |Fred Bowen |February 10, 2021 |Washington Post 

Richard Maulsby, one of the other founding members, has said he favored changing the club’s name. Stein Club changes name to Capital Stonewall Democrats |Lou Chibbaro Jr. |February 9, 2021 |Washington Blade 

She had worked at the NFL Players Association and helped athletes from skateboarders to softball players band together and form member organizations. How break dancing made the leap from ’80s pop culture to the Olympic stage |Rick Maese |February 9, 2021 |Washington Post 

They took cover inside a print works to the north east of Paris, where they held a member of staff as a hostage. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

What matters is being honest, humble, and a faithful and loyal friend, father and member of your community. Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 

"That was crazy," Lynn Jenkins of Kansas muttered to another member as she walked to greet Boehner. Democrats Accidentally Save Boehner From Republican Coup |Ben Jacobs, Jackie Kucinich |January 6, 2015 |DAILY BEAST 

Another member of the plot took care of the ammo along with black uniforms, night-vision equipment, and body armor. The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

Reprinted by arrangement with The Penguin Press, a member of Penguin Group (USA) LLC, A Penguin Random House Company. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

Mr. Brown seizes the proffered member, and gives it as hearty a pressure as the publicity of the occasion will permit. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

The member banks should look upon the reserve bank not as an alien but as their own institution. Readings in Money and Banking |Chester Arthur Phillips 

The Act permits member banks to accept an amount of bills not exceeding 50 per cent. Readings in Money and Banking |Chester Arthur Phillips 

It was never the intention of the Federal Reserve Act that member banks should continue the maintenance of these reserve accounts. Readings in Money and Banking |Chester Arthur Phillips 

He was a member of the first provincial congress, and eighteen years lieutenant governor of the state of New York. The Every Day Book of History and Chronology |Joel Munsell