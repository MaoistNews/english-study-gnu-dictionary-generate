You could also seek out countries with internet choke points that slow or make Wi-Fi use pointless and frustrating. If We Ever Go On Vacations Again |Eugene Robinson |July 25, 2021 |Ozy 

Instead, the growing costs of sustaining Moore’s Law have encouraged consolidation among chipmakers and created more choke points in the immensely complex business of chip production. The great chip crisis threatens the promise of Moore’s Law |Jeremy Hsu |June 30, 2021 |MIT Technology Review 

Of all the choke points, it was my least favorite to pass through, and when I did so as a ship captain I was up and awake all night. The Blocked Suez Canal Isn't the Only Waterway the World Should Be Worried About |James Stavridis |March 29, 2021 |Time 

The family was told her handcuffed son reached for an officer’s gun, prompting them to put him in a choke hold–a maneuver Minneapolis police finally banned last June. It's a 'Nightmare Being Replayed' as a Cop Faces Trial in George Floyd's Death |Janell Ross |March 18, 2021 |Time 

Gently twist and pull away the petals at the very center until the furry choke is revealed. How to prep, cook and enjoy stuffed artichokes |Ann Maloney |March 11, 2021 |Washington Post 

He then began to choke her, and as she lost consciousness it must have seemed that his might be the last face she would ever see. The Girls Who Were Taken Before Hannah Graham |Michael Daly |September 30, 2014 |DAILY BEAST 

According to their friend, producer/filmmaker Choke No Joke, it was over a mutual flame. Did Beyoncé Just Accuse Jay Z of Cheating? |Marlow Stern |July 2, 2014 |DAILY BEAST 

Shiomura continued to speak even though she had to choke back tears at one point. Japanese Parliament to Women: Breed, Don't Lead |Jake Adelstein, Angela Erika Kubo |June 22, 2014 |DAILY BEAST 

A new Kimberley Process working group to monitor CAR might help focus attention on the guilty and choke the diamond flow. The Curse of CAR: Warlords, Blood Diamonds, and Dead Elephants |Christopher Day |May 25, 2014 |DAILY BEAST 

But for a movie that choke-holds you in suspense from the very first scene, Cold in July is also surprisingly funny. Michael C. Hall on Where ‘Dexter’ Went Wrong and His New Killer Role in ‘Cold in July’ |Melissa Leon |May 23, 2014 |DAILY BEAST 

It's good for nothing but to choke a man and fill him full of smoke and embers. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He felt that to eat would choke him, but forced himself to take a sip of coffee and a bit of corn bread. The Cromptons |Mary J. Holmes 

There was another choke in her voice as she smoothed Judy's old brown dress, and brushed a bit of bran from her face. The Cromptons |Mary J. Holmes 

The spoondrift began to fly so that you could not see the moon, and the wind was enough to choke you if you faced it. The Chequers |James Runciman 

Hester fiercely bit her lip and gulped down the tears that threatened to choke her. Those Dale Girls |Frank Weston Carruth