I spoke first with Scott Ellman, a student at Wesleyan University and now the Huffington Post editor-at-large for his campus. Fraternities in a Post-UVA World |Samantha Allen |December 12, 2014 |DAILY BEAST 

The at-home genetics testing company 23andme, established in 2006, helps people learn more about their “DNA relatives.” Nothing Says I Love You Like Data |The Daily Beast |December 8, 2014 |DAILY BEAST 

This at-home blood test kit gives a full reading of antioxidant, fatty acid, or vitamin panels. Nothing Says I Love You Like Data |The Daily Beast |December 8, 2014 |DAILY BEAST 

At-home caretakers are eligible for Medicaid waivers, which allow benefits regardless of income. Medicaid Will Give You Money for At-Home Care, but You Might Wait Years |Elizabeth Picciuto |December 2, 2014 |DAILY BEAST 

Because women with disabilities are one of the most at-risk demographics in the world. #YesAllWomen, but Not Really: How Feminism Leaves the Disabled Behind |Elizabeth Heideman |November 24, 2014 |DAILY BEAST 

Tressan was monstrous ill-at-ease, and his face lost a good deal of its habitual plethora of colour. St. Martin's Summer |Rafael Sabatini 

A fellow rudely clad—a hybrid between man-at-arms and lackey—lounged on a musket to confront them in the gateway. St. Martin's Summer |Rafael Sabatini 

And the Seneschal, moved by that confident promise of evil, threw himself before the men-at-arms. St. Martin's Summer |Rafael Sabatini 

On the first day, a thousand English archers, supported by men-at-arms, attempted to draw the Scots. King Robert the Bruce |A. F. Murison 

Belhaven made his escape to his own country, and was there beyond the reach of the Serjeant-at-Arms. The History of England from the Accession of James II. |Thomas Babington Macaulay