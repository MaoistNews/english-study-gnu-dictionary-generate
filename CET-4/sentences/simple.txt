This will cost a little extra money, but there are several simple after-market modifications you can make to get the most out of your new 4x4. Six ways to make your ATV even more rugged |By Tyler Freel/Outdoor Life |September 17, 2020 |Popular-Science 

At Stanford, they were focused mostly on ethanol and methanol, which are simple liquid alcohols. ClearFlame Engine Technologies takes aim at cleaning up diesel engines |Kirsten Korosec |September 17, 2020 |TechCrunch 

You can take some simple steps now to make the voting process smoother. ProPublica’s Pandemic Guide to Making Sure Your Vote Counts |by Susie Armitage |September 16, 2020 |ProPublica 

So we felt that during a pandemic, the simplest version of a vaccine is the one that could reach billions of doses. Synthetic biologists have created a slow-growing version of the coronavirus to give as a vaccine |David Rotman |September 16, 2020 |MIT Technology Review 

Based on The Wall Street Journal’s reporting, it seems that Oracle may simple serve as the warehouse for TikTok’s domestic user data. How the future of TV and streaming has – and hasn’t – been reshaped so far by 2020 |Tim Peterson |September 16, 2020 |Digiday 

The simple, awful truth is that free speech has never been particularly popular in America. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

The reason pilots would choose to use guns over a bomb or a missile is simple. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

Still, I worry that a simple traffic stop could have tragic consequences. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 

“It is the hardest to play,” one of the buglers says of this seemingly simple tune. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

The premise was simple: satire is devastating against tyrants. The Sony Hack and America’s Craven Capitulation To Terror |David Keyes |December 19, 2014 |DAILY BEAST 

Smith's method usually gives good results, as does also the more simple method of Hiss (p. 263). A Manual of Clinical Diagnosis |James Campbell Todd 

It was a mighty simple transaction, but it produced some startling results for me, that same coin-spinning. Raw Gold |Bertrand W. Sinclair 

Let us look over these points again, and make the matter still clearer and more simple. God and my Neighbour |Robert Blatchford 

If I selected lucid and simple extracts, they would give no idea of the intricacy and prolixity of Duns. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

It has long since dismissed as too short and simple for its pages, the short and simple annals of the poor. The Unsolved Riddle of Social Justice |Stephen Leacock