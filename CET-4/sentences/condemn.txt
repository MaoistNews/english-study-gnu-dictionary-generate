After that report, the Lincoln Project condemned Weaver, calling him a “predator,” “liar” and “an abuser.” Lincoln Project tweeted a co-founder’s private messages after leaders promised to probe sexual harassment claims |Andrea Salcedo |February 12, 2021 |Washington Post 

For every bit of man-made excess the exhibition condemns, it shows us a dozen times over that if we look close enough and deep enough, natural “excess” is everywhere. Baltimore exhibition of outsider art celebrates nature’s ‘excess’ with a wake-up call about pollution |Kelsey Ables |February 8, 2021 |Washington Post 

Some of her colleagues even voted in Greene’s defense in spite of condemning her behavior in the past. House punishes Republican lawmaker who promoted violent conspiracy theories |Taylor Hatmaker |February 5, 2021 |TechCrunch 

On the one hand, establishment leaders have condemned Greene. The Marjorie Taylor Greene committee removal vote, explained |Gabby Birenbaum |February 5, 2021 |Vox 

He released a statement condemning Greene’s past statements, some of which she admitted were false and said she regretted in a House floor speech Thursday. Republicans worry their big tent will mean big problems in 2022 elections |Michael Scherer, Josh Dawsey |February 4, 2021 |Washington Post 

The campaign included a push for the World Health Organization to condemn gay-conversion therapy. China’s Electroshock Gay-Conversion Case |Nina Strochlic |December 19, 2014 |DAILY BEAST 

Dean Teresa A. Sullivan praised the “overwhelming response by this community to condemn the evil acts” reported by Rolling Stone. Why It Was Right to Question Rolling Stone’s U-VA Rape Story |Michael Moynihan |December 5, 2014 |DAILY BEAST 

All religions condemn lying, but Alicia Florrick likely will not be elected if she runs as an atheist. The Good Wife’s Religion Politics: Voters Have No Faith in Alicia's Atheism |Regina Lizik |November 24, 2014 |DAILY BEAST 

Is it the one where we condemn the genocide of Native Americans? Are Politicians Too Dumb to Understand the Lyrics to ‘Born in the USA’? |Parker Molloy |November 6, 2014 |DAILY BEAST 

It may come as a surprise then that Scott and, particularly Sata, have refused to condemn Mugabe. Democratic Africa Gets Its First White Leader |Nico Hines |October 29, 2014 |DAILY BEAST 

Yet if there is a measure of untruth in such pretty flatteries, one needs to be superhuman in order to condemn them harshly. Children's Ways |James Sully 

Some affirm that he wrote to please royalty, but if so why did he not condemn the custom to appease the wrath of a sapient king. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Do not believe this; be certain that those who profess such a doctrine are practising themselves the deceit they condemn so much. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

In reference to this, as well as to any other matter inculcated upon them, their consciences will either approve or condemn them. The Ordinance of Covenanting |John Cunningham 

We do not pretend to justify either his ignorance or his imposture; but we cannot condemn his doctrine of one only God. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire)