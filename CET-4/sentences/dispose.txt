Nothing much to use in cleaning up the baby and his mother after the birth, no place to dispose of the placenta. Jesus Wasn’t Born Rich. Think About It. |Gene Robinson |December 25, 2014 |DAILY BEAST 

The Jewish Week reported that sources said Hynes was expected to dispose of the case with a lenient plea deal. The Orthodox Sex Abuse Crackdown That Wasn’t |Emily Shire |October 7, 2014 |DAILY BEAST 

And with so many pigs dying, farms have been challenged to try to find hygienic ways to dispose of the carcasses. Aporkalypse Now: Pig-Killing Virus Could Mean the End of Bacon |Carrie Arnold |August 20, 2014 |DAILY BEAST 

He wanted to make sure that he had the maximum amount of time to attack his victim and dispose of her body. A Serial Killer on the Loose in Nazi Berlin |Scott Andrew Selby |January 11, 2014 |DAILY BEAST 

The Japanese team then folded, allowing Rong Guotuan quickly to dispose of his last opponent. How to Hide a Famine with Ping-Pong |Nicholas Griffin |January 9, 2014 |DAILY BEAST 

But its use as such is to dispose of any such idea as that there is a natural price of coal or of anything else. The Unsolved Riddle of Social Justice |Stephen Leacock 

My object was to dispose of a cargo of cotton which I had brought from Realejo, and to purchase sugar in return. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Truce now, Gregory; and consider how we can best dispose ourselves here, till the morning. The Battle of Hexham; |George Colman 

During this journey I was an eye-witness of the manner in which the missionaries dispose of their religious tracts. A Woman's Journey Round the World |Ida Pfeiffer 

His creature has no right to ask the reason of His conduct; He can dispose at will of the works of His hands. Superstition In All Ages (1732) |Jean Meslier