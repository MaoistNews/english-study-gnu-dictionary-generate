Faruqui addressed the intruder, who was only a few years older as “sir.” How An Indian Stand Up Comic Found Himself Arrested for a Joke He Didn't Tell |Sonia Faleiro |February 10, 2021 |Time 

Now that he was Sir Alfred, there was one final blast of publicity. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

After his knighthood, I stop by to see him and call him Sir Alfred. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

De Mena offers me a glass of Sir Francis Drake, Casa Bruja's red ale. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

Sir Bob, 63, responded with his usual colorful language to his critics. Do They Know It’s Time to Stop Band Aid? |Tom Sykes |November 22, 2014 |DAILY BEAST 

Good old Sir Bob Geldof stepping into the breach again to raise money for crisis-hit Africa? Do They Know It’s Time to Stop Band Aid? |Tom Sykes |November 22, 2014 |DAILY BEAST 

The old man seemed to be greatly agitated, and hurriedly whispering, "We thought you were never coming, sir!" Davy and The Goblin |Charles E. Carryl 

Mr. Pickwick—deepest obligations—life preserver—made a man of me—you shall never repent it, sir. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Sir Cadge was about the same age as the famous beauty, and rose quite two inches above her lofty head. Ancestors |Gertrude Atherton 

I wouldn't go on if I were you, sir; the luck's dead against you to-night; I wouldn't go on, indeed I wouldn't. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

No, Sir, said the other, nothing at all except the enjoyment of your good company: and so gave over importuning him. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe