With so many options on the market, it can be tough to whittle down the optimal brand, ink quality, and pen type to suit your exact needs. Add some verve to your life with these colorful pens |PopSci Commerce Team |September 11, 2020 |Popular-Science 

With ink consistency that falls in line with the standard gel pen, you won’t have to worry about smudging or being completely exact in your line drawing. Add some verve to your life with these colorful pens |PopSci Commerce Team |September 11, 2020 |Popular-Science 

Even better, you can refill the ink to spare you from buying another set. Add some verve to your life with these colorful pens |PopSci Commerce Team |September 11, 2020 |Popular-Science 

From affordable do-it-all photo printers with dye-based inks and dual-sided document printing, to studio-grade options that use archival grade pigments and thick fine art mediums, we’ve tried to include an option for every budget and skill level. Professional photo printers for every budget |D.L. Cade |September 10, 2020 |Popular-Science 

That’s when a new era of terrorism will become truly visible, like secret ink, unless countries take note now. Butterfly Effect: Pandemic Shrouds Terrorist Activities |Charu Kasturi |September 3, 2020 |Ozy 

Much ink has been spilled over the wonky science and plot contrivances of Interstellar. ‘Interstellar’ Is Wildly Ambitious, Very Flawed, and Absolutely Worth Seeing |Marlow Stern |November 7, 2014 |DAILY BEAST 

Powdered ink would adhere to the charged image and then it would be pressed via heat onto paper. Pioneers in Printing |The Daily Beast |October 21, 2014 |DAILY BEAST 

The edges of the elegant paper are crackled; the ink bled into the linen weave long ago and has not faded. How Gettysburg Did Not Unlock the Past |Laird Hunt |September 21, 2014 |DAILY BEAST 

A lot of ink will continue to be spilled about the first-order problems surrounding that fact. The Rhinohawks Come Roaring Back |James Poulos |September 7, 2014 |DAILY BEAST 

His ink ranges from images of his children to the Superman logo (that one seems especially fitting given his leaping blocks). Team USA Lost, but Tim Howard Is a Winner |Emily Shire |July 1, 2014 |DAILY BEAST 

She also practises etching, pen-and-ink drawing, as well as crayon and water-color sketching. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

And, old ink pot, tuck a horse blanket under my chin, and rub me down with brickbats while I feed! The Book of Anecdotes and Budget of Fun; |Various 

Maype he trinks ret ink gocktails, like de Injuns; maype he trinks Florita Vater, oder golone. Alec Lloyd, Cowpuncher |Eleanor Gates 

Her mother, wearing an ink-stained jacket, was busy at her desk, the pen scratching on the big sheets of pad paper. The Girls of Central High on the Stage |Gertrude W. Morrison 

Her hair was black as India ink, drawn back from her rounded forehead to knot softly at the back of her head. Hooded Detective, Volume III No. 2, January, 1942 |Various