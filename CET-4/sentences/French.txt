As far as I can tell, this magazine spent as much time making fun of French politicians as it did of Muslims or Islam. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

The comedian responded to the deadly attack on a French satirical magazine by renewing his recent criticisms of the Islamic faith. Bill Maher: Hundreds of Millions of Muslims Support Attack on ‘Charlie Hebdo’ |Lloyd Grove |January 8, 2015 |DAILY BEAST 

The FBI has also been searching its records for any information that could assist the French investigation, a spokesperson added. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

Gunshots rang out in Paris this morning on a second day of deadly violence that has stunned the French capital. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

I think the response of the French government so far has been pretty appropriate in that regard. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

However, on reaching Spain, the magic of the Emperor's personality soon restored the vigour and prestige of the French arms. Napoleon's Marshals |R. P. Dunn-Pattison 

He gives a list of the sponsors of the baptized Indians, who included many of the French nobility and clergy. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

It being offensive to the French, they took none of it with them on their return. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Eustache le Sueur died; one of the best French historical painters of his time. The Every Day Book of History and Chronology |Joel Munsell 

Sanson's Atlas: a very large atlas by a French geographer in use in Swift's time. Gulliver's Travels |Jonathan Swift