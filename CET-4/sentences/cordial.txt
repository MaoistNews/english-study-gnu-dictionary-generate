You may recall the Great Drinking Vinegar Renaissance of the 2010s, when Pok Pok chef Andy Ricker riffed on Southeast Asia’s drinking vinegar tradition and American shrubs to create the Som cordial that captivated much of the food world. Homemade Shrubs Are Your Gateway to a World of Sweet, Tangy Summer Drinking |Aliza Abarbanel |July 8, 2021 |Eater 

This is the first I have heard of such complaints, and our pre-coronavirus family get-togethers were always cordial. Miss Manners: Relative lets out long-held grudges |Judith Martin, Nicholas Martin, Jacobina Martin |June 17, 2021 |Washington Post 

If it was a married couple, he would be cordial and friendly, he used the liking principle, speaking to both people. How to Get Anyone to Do Anything (Ep. 463) |Stephen J. Dubner |May 27, 2021 |Freakonomics 

They are all now adults, and we have very cordial relationships. Miss Manners: How inclusive should I be in my obit? |Judith Martin, Nicholas Martin, Jacobina Martin |March 12, 2021 |Washington Post 

The interactions are often cordial — she often strikes up conversation with patients about their tattoos — but necessarily brief. The joy of vax: The people giving the shots are seeing hope, and it’s contagious |Maura Judkis |February 25, 2021 |Washington Post 

The 2008 Republican presidential nominee and longtime Arizona senator has long had a cordial relationship with Hillary Clinton. Remember When Republicans Loved Hillary Clinton? |Ben Jacobs |December 1, 2014 |DAILY BEAST 

Neither trusts the other, yet cultural norms dictate that everyone remain cordial. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

Despite any partisan enmities, the two top politicos maintained a cordial relationship. The McConnell Friend Obama Just Hired |Jonathan Miller |November 10, 2014 |DAILY BEAST 

The president and former president, who once despised each other, are cordial but far from friendly. The Only Way for Democrats to Win |Jonathan Alter |October 24, 2014 |DAILY BEAST 

The email exchanges started out as cordial, if cold, but gradually grew more confrontational. The Kardashian Look-Alike Trolling for Assad |Noah Shachtman, Michael Kennedy |October 17, 2014 |DAILY BEAST 

The relations between country bank officials and the officials of this bank have been most cordial. Readings in Money and Banking |Chester Arthur Phillips 

Tressan advanced to meet him, a smile of cordial welcome on his lips, and they bowed to each other in formal greeting. St. Martin's Summer |Rafael Sabatini 

He put out his hand in the most cordial and friendly way, and greeted me with the most winning smile in the world. Music-Study in Germany |Amy Fay 

He was well entitled to the Resolution of cordial thanks which the associated companies accorded to him. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

But to rulers possessed of scriptural qualifications, cordial obedience is due. The Ordinance of Covenanting |John Cunningham