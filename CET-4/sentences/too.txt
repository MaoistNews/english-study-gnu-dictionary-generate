But along with the cartoon funk is an all-too-real story of police brutality embodied by a horde of evil Pigs. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

I remember H. Jon Benjamin told me it was a way-too-late apology for Hiroshima and Nagasaki. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

In the not-too-distant future, the AMRAAM might also be out-ranged by new weapons that are being developed around the world. Pentagon Worries That Russia Can Now Outshoot U.S. Stealth Jets |Dave Majumdar |December 4, 2014 |DAILY BEAST 

Jennifer Lawrence has long been praised for her authenticity in a Hollywood elite full of all-too-carefully curated personalities. Jennifer Lawrence Shouldn’t Laugh Off Her Nude Photo Hack |Samantha Allen |September 2, 2014 |DAILY BEAST 

Sheets of bright-but-not-too-bright blue streaked with thin clouds. Native American Basketball Team in Wyoming Have Hoop Dreams Of Their Own |Robert Silverman |August 31, 2014 |DAILY BEAST 

Coppy, in a tone of too-hastily-assumed authority, had told her over night that she must not ride out by the river. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

From time to time Lockhart gave vent to a grim laugh, and Spivin displayed his feelings in a too-amiable smile. The Garret and the Garden |R.M. Ballantyne 

Sylvan scenes, with a dash of human savagery in the foreground, form the best relief for a too-extended assimilation of books. Mystery Ranch |Arthur Chapman 

Then she looked more hopeful as her eyes rested on Betty, who was sorting the contents of a too-crowded dresser drawer. The Outdoor Girls in the Saddle |Laura Lee Hope 

Characteristically, this weakness seems to have taken the form of a too-generous estimate of his fellows. The Fatal Dowry |Philip Massinger