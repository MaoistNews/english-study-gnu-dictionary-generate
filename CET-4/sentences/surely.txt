For many coaches and athletes, such practices are surely on the wrong side of what Salazar himself once described as “the line between acceptable performance enhancement and cheating.” A New Book on Nike Pulls No Punches |Martin Fritz Huber |October 2, 2020 |Outside Online 

After digesting the acquisitions in the pipeline, the company projected, revenues and profits would surely soar. Investors Extracted $400 Million From a Hospital Chain That Sometimes Couldn’t Pay for Medical Supplies or Gas for Ambulances |by Peter Elkind with Doris Burke |September 30, 2020 |ProPublica 

Van Cortlandt, or “Vanny” for insiders and posers, is home to what is surely the most storied cross-country course in the United States. The Eternal Magic of Van Cortlandt Park |Martin Fritz Huber |September 28, 2020 |Outside Online 

Antitrust protects competition, which surely serves the goals of stakeholder capitalism sometimes, but even healthy competition doesn’t always produce what is best for all stakeholders. FTC commissioner: Is antitrust the next stakeholder capitalism battleground? |jakemeth |September 26, 2020 |Fortune 

Keeping kids at home for six straight months is surely enough to break a person. The Learning Curve: School Is Now an Antidote Few Families Can Access |Will Huntsberry |September 24, 2020 |Voice of San Diego 

Without it, they say, the disease would surely kill her within two years. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 

Surely all this graphic talk of gastrointestinal distress is making you queasy. Why My Norovirus Panic Makes Me Sick |Lizzie Crocker |January 5, 2015 |DAILY BEAST 

If they were meaningful, we might have realized it before—surely one of these kids wore a cross, or a yarmulke, or a hijab? Harry Potter and the Torah of Terror |Candida Moss, Joel Baden |January 4, 2015 |DAILY BEAST 

Perhaps it always seems that way at the time, but surely we face our fair share right now. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

They had more than enough to build fabulous new domiciles that surely at least equal their wildest dreams. The Luxury Homes That Torture and Your Tax Dollars Built |Michael Daly |December 12, 2014 |DAILY BEAST 

I presume the twenty-five or thirty miles at this end is unhealthy, even for natives, but it surely need not be so. Glances at Europe |Horace Greeley 

At last his anxiety reached a point where he was positive that if he received an adverse decision, it would surely kill him. The Homesteader |Oscar Micheaux 

The performers and the nuns nearly died of fright, believing that their last hour had surely come. The Red Year |Louis Tracy 

"I do think Angel would surely want me to go, if she knew," thought Rosemary. Rosemary in Search of a Father |C. N. Williamson 

Had he not meant the Fleet to shove in K. must have made some reference to the second Division, surely. Gallipoli Diary, Volume I |Ian Hamilton