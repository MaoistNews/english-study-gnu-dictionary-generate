Watson recently has requested that the Texans trade him, reportedly displeased that the team didn’t follow through on promises that he would have input in an organizational overhaul this offseason. Deshaun Watson accused of sexual misconduct in lawsuits stemming from massage sessions; QB issues denial |Mark Maske, Nicki Jhabvala |March 17, 2021 |Washington Post 

This will displease only hawks who pray for large-scale U.S. air attacks plus lots of U.S. aid and ground-level advice. A Winning Strategy for Iraq and Syria |Leslie H. Gelb |June 21, 2014 |DAILY BEAST 

Davies said he disregarded these orders but did not want to displease a man he respected so much. CBS Backs Off Benghazi Source |Eli Lake |November 8, 2013 |DAILY BEAST 

Ethel's mode of pleading was unfortunate; the "very foolish of Margaret" were the very words to displease. The Daisy Chain |Charlotte Yonge 

You are an honest man,404 and do not make it your business either to please or displease the favourites. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

If I understand you rightly, monsieur, you have decided to rid yourself of persons who annoy you or displease you? The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 

He supped with me; but a cunning, crafty fellow he is, and dangerous to displease, for his tongue spares nobody. Diary of Samuel Pepys, Complete |Samuel Pepys 

For a word, she would flare up into a sudden anger, nor try as he would, could he divine what action of his would displease her. The Woman Gives |Owen Johnson