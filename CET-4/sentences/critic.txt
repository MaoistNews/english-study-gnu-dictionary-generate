For example, the UK-based digital health company Babylon Health came under fire in 2018 for announcing that its diagnostic chatbot was “on par with human doctors,” on the basis of a test that critics argued was misleading. New standards for AI clinical trials will help spot snake oil and hype |Will Heaven |September 11, 2020 |MIT Technology Review 

Lam has responded to the testing scheme’s critics, accusing experts who oppose the effort of “smearing” Beijing in an attempt to “sever Hong Kong’s relations with the central government.” Hong Kong’s citywide COVID-19 testing has become a barometer of public trust |eamonbarrett |September 9, 2020 |Fortune 

For a long time, critics of pure deep-learning approaches, such as Gary Marcus, have been calling for exactly the sort of hybrid approaches NDTT represents. Disco, bell bottoms, big hair…and cutting-edge A.I.? |Jeremy Kahn |September 8, 2020 |Fortune 

Critics of the regulationMedia analyst Ben Thompson points out media mogul Rupert Murdoch owns most of the Australian press and has been the platforms’ biggest critic. State of play: Where the battle with Google and Facebook to pay for news is hottest |Lucinda Southern |September 7, 2020 |Digiday 

I am but a film critic, so I won’t pretend to understand or fully explain the entire history of the Sator square here. The ancient palindrome that explains Christopher Nolan’s Tenet |Alissa Wilkinson |September 4, 2020 |Vox 

A lot of your reflections on the classics are pretty intense, have you ever thought about being a film critic? Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

“Every critic encounters one book like that,” was his reply. A Novel Nearly Impossible to Review |Nicholas Mancusi |December 28, 2014 |DAILY BEAST 

Greste has also taken a stand in prison as a staunch critic of what has transpired. Behind Bars for the Holidays: 11 Political Prisoners We Want to See Free In 2015 |Movements.Org |December 25, 2014 |DAILY BEAST 

Film critic David Ehrlich continues his annual tradition of making a supercut of his favorite films of the year. ‘Sexual’ Barbershop Quartet, a Panda Family Reunion, and More Viral Videos |The Daily Beast Video |December 14, 2014 |DAILY BEAST 

However, he asked her to give up her career as a political journalist—she subsequently became a book critic. Hell Hath No Fury Like Valerie Trierweiler, the French President’s Ex |Lizzie Crocker |November 28, 2014 |DAILY BEAST 

Her attachment to impressionism leads this artist to many experiments in color—or, as one critic wrote, "to play with color." Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Peter Elmsly, a partner of the celebrated Paul Valliant, and himself an importer of books and no mean critic and linguist, died. The Every Day Book of History and Chronology |Joel Munsell 

She is, however, a severe critic of her own work and is greatly disturbed by indiscriminating praise. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Dominic Bouhours, a French Jesuit, died; celebrated as a learned writer and critic. The Every Day Book of History and Chronology |Joel Munsell 

If the critic repents his evil deeds, it is because something has happened to awake his remorse. God and my Neighbour |Robert Blatchford