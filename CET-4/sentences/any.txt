The open-up-to-any-chapter method might, in fact, be the best approach here. What Made Twain Famous |Nathaniel Rich |April 20, 2010 |DAILY BEAST 

Stalking the unexpected is “an all-year-round, any-kind-of-weather sport,” she says. 6 Secrets of Perfect Gift Giving |Lee Eisenberg |November 26, 2009 |DAILY BEAST 

For he's the soul of honor, Thyrsis; and he can't help how he feels about me-any more than I can help it. Love's Pilgrimage |Upton Sinclair 

I appeal to yourself, Madam, whether these sublime notions have-any thing consoling in them? Letters To Eugenia |Paul Henri Thiry Holbach 

They take their coats off anywhere and any-when, and somehow it strikes the visitor as the most symbolic thing about them. Letters from America |Rupert Brooke 

Hyphens are sometimes used in cases like the following: "A never-to-be-forgotten event," "peace-at-any-rate principles." "Stops" |Paul Allardyce 

On the fall of Richmond, and the surrender of Lee, this any-how impracticable scheme was necessarily abandoned. The Judicial Murder of Mary E. Surratt |David Miller DeWitt