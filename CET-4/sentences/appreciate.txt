I wrote about how much I appreciate that kind of sweet kookiness in comedies — the sort of story ideas you simply can’t imagine a filmmaker pitching today, let alone a studio executive greenlighting. The fabulous Barb and Star Go to Vista Del Mar recalls a bygone age of wackadoodle comedy |Alissa Wilkinson |February 12, 2021 |Vox 

This is a Mustang, after all, so that is important to the people who appreciate that heritage. Ford’s electric Mustang Mach-E is an important leap into the future |Dan Carney |February 12, 2021 |Popular-Science 

We have a card that was specifically planned to espouse how fast life moves, and how we may forget to stop and say, “I appreciate you.” How Hallmark is handling pandemic Valentine’s Day |Luke Winkie |February 12, 2021 |Vox 

It was a true lightswitch wine for me, and helped me understand and appreciate grower Champagne in a deeper way. You Should Be Drinking Grower Champagne |Jordan Michelman |February 11, 2021 |Eater 

That’s one thing Kramon appreciates, even on the most painful calls. The loneliness of an interrupted adolescence |Ellen McCarthy |February 11, 2021 |Washington Post 

He was getting another lesson in what he had seemed not to appreciate fully about cops. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

She fails to appreciate the congressional and constitutional obstacles Johnson had to overcome to win passage of the bill. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

What I appreciate is, they are respectful of the democratic process. NYC’s Garner Protesters vs. Pro-Cop Protesters |Jacob Siegel |December 20, 2014 |DAILY BEAST 

Body Positivist Who Also Happens to Look Like Beyoncé "Young people don't appreciate how beautiful our bodies are." The Beyoncé Manifesto: Quotes on Nihilism and Feminism |Amy Zimmerman |December 12, 2014 |DAILY BEAST 

But this is shaping up to be the hottest product of the year, so your favorite techie will surely appreciate an I.O.U from Santa. The Daily Beast’s 2014 Holiday Gift Guide: For the Richard Hendriks in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

The pupil will appreciate its practical value the moment he masters the key to it. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

He, who knows the dangers and necessities we were in, will appreciate the joy we felt and that we feel at its arrival. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

No one could appreciate better than ourselves the unpleasant possibilities that stared us in the face. Raw Gold |Bertrand W. Sinclair 

It is just this joyous, care-free nature of the Irish that the stolid Englishman will never learn to appreciate. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

We say—mark this, in order to appreciate a display of the true genius of statesmanship. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various