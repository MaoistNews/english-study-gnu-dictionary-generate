If you want to understand the flamboyant family of objects that make up our solar system—from puny, sputtering comets to tremendous, ringed planets—you could start by immersing yourself in the technical terms that fill the scientific literature. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

Poway Unified anticipates bringing forward two new courses – ethnic studies and ethnic literature – to the school board for review, said Christine Paik, a spokeswoman for the district. As School Resumes, Students Bring Racial Justice Push to the Classroom |Kayla Jimenez |August 18, 2020 |Voice of San Diego 

The book she completed after that trip, Coming of Age in Samoa, published in 1928, would be hailed as a classic in the literature on sexuality and adolescence. Gender Is What You Make of It - Issue 88: Love & Sex |Charles King |August 5, 2020 |Nautilus 

He also told Chemistry World he envisages the robots eventually being able to analyze the scientific literature to better guide their experiments. This Robotic Chemist Does Over 600 Experiments a Week and Learns From Its Own Work |Edd Gent |July 13, 2020 |Singularity Hub 

Research also suggests that reading literature may help increase empathy and understanding of others’ experiences, potentially spurring better real-world behavior. The case for using literature to kickstart conversations about race at work |Sarah Todd |July 12, 2020 |Quartz 

The research literature, too, asks these questions, and not without reason. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

She wanted to know what happened over five years, or even 10, but the scientific literature had little to offer. Why Your New Year’s Diet Will Fail |Carrie Arnold |December 30, 2014 |DAILY BEAST 

The religion shaped all facets of life: art, medicine, literature, and even dynastic politics. The Buddhist Business of Poaching Animals for Good Karma |Brendon Hong |December 28, 2014 |DAILY BEAST 

Speaking of the literature you love, the Bloomsbury writers crop up in your collection repeatedly. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

Literature in the 14th century, Strohm points out, was an intimate, interactive affair. A Year In The Life of The Canterbury Tales’ Storied Beginnings |Wendy Smith |December 25, 2014 |DAILY BEAST 

All along the highways and by-paths of our literature we encounter much that pertains to this "queen of plants." Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

There cannot be many persons in the world who keep up with the whole range of musical literature as he does. Music-Study in Germany |Amy Fay 

In early English literature there was at one time a tendency to ascribe to Solomon various proverbs not in the Bible. Solomon and Solomonic Literature |Moncure Daniel Conway 

He was deeply versed in Saxon literature and published a work on the antiquity of the English church. The Every Day Book of History and Chronology |Joel Munsell 

Such unromantic literature as Acts of Parliament had not, it may be supposed, up to this, formed part of my mental pabulum. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow