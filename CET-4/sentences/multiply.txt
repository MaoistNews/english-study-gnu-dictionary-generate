You might have heard that to get a dog’s “biological” age, just multiply its age in years by seven. To figure out your dog’s ‘real’ age, you’ll need a calculator |Bethany Brookshire |August 12, 2020 |Science News For Students 

When given food, even the most ancient microbes revived themselves and multiplied, researchers report July 28 in Nature Communications. These ancient seafloor microbes woke up after over 100 million years |Carolyn Gramling |July 28, 2020 |Science News 

Divide this number by the total number of people asked and multiply it by 100 to get a percentage score. Seven brand health metrics and how to measure them |Aleh Barysevich |July 24, 2020 |Search Engine Watch 

For pensions to work, money paid by governments and employees is invested, and the stock market multiplies the dollars to pay for pension checks guaranteed to workers in retirement. Here’s Where Local Pensions Funds Stand After Losing Billions to the Pandemic |Ashly McGlone |July 24, 2020 |Voice of San Diego 

As humans clear swaths of forest for agriculture or roads, forest edges multiply, increasing spillover risk from once-isolated wildlife to humans and livestock. To prevent the next pandemic, we might need to cut down fewer trees |Jonathan Lambert |July 23, 2020 |Science News 

Idiocies multiply in direct proportion to the accumulating legal rigidities. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

A flamboyant, multi-titled, multiply married royal to remember, the Duchess of Alba died Thursday at the age of 88. Adiós to the Diva Duchess |Barbie Latza Nadeau |November 20, 2014 |DAILY BEAST 

Multiply that number by 365 and you get more than 30,000 families who suffer from gun violence over the course of a year. Joe the Plumber’s ‘Dead Kid’ Callousness |Erica Lafferty |May 29, 2014 |DAILY BEAST 

As the assemblies multiply and spread, the disparity between communities has thrown up a series of issues. Godless Church Services for Atheists Go Global |Nico Hines |May 4, 2014 |DAILY BEAST 

And you, be ye fruitful, and multiply; bring forth abundantly in the earth, and multiply therein. The Story of Noah's Ark From the Bible’s Book of Genesis |The Daily Beast |March 24, 2014 |DAILY BEAST 

And thou didst multiply riddles in parables: thy name went abroad to the islands far off, and thou wast beloved in thy peace. The Bible, Douay-Rheims Version |Various 

And I will save you from all your uncleannesses: and I will call for corn, and will multiply it, and will lay no famine upon you. The Bible, Douay-Rheims Version |Various 

It would be easy to multiply individual cases supporting the same general principles. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

It was still summer, and the gnats had begun to multiply to a prodigious and alarming extent. My Ten Years' Imprisonment |Silvio Pellico 

Dey wanted to sell him an buy a 'oman so dey could have a lot of slave chilluns cause de 'oman could multiply. Slave Narratives: a Folk History of Slavery in the United States |Various