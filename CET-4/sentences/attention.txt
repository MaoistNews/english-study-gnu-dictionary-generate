We know more than ever about attention span and engagement, or the connection between socio-emotional development and academic outcomes. Why hasn’t digital learning lived up to its promise? |Walter Thompson |September 17, 2020 |TechCrunch 

A lack of institutional leadership also means there aren’t many prominent people or groups showing up to nudge politicians to pay attention to their issues. More And More Americans Aren’t Religious. Why Are Democrats Ignoring These Voters? |Daniel Cox |September 17, 2020 |FiveThirtyEight 

The QAnon conspiracy theory, which originated online three years ago, has gained more attention this year. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

With no iPhone to soak up all the attention this year, the two new models—the premium Apple Watch Series 6 and the lesser Apple SE—shone. Why I’m caving and finally buying an Apple Watch |rhhackettfortune |September 16, 2020 |Fortune 

While this campaign is mostly geared toward informing Instagram users, Steyer said he also hopes the celebrity messages will garner the attention of advertisers. Kim Kardashian, Katy Perry, and Leonardo DiCaprio plan one-day Instagram boycott. Here’s why |Danielle Abril |September 15, 2020 |Fortune 

One witness said the gunfire began after a traffic collision, which drew the attention of a nearby police officer. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

First, they allow Paul to siphon off attention from whichever potential candidate is making news. Rand Paul’s Passive-Aggressive Trolling Campaign |Olivia Nuzzi |January 6, 2015 |DAILY BEAST 

In short, fatherhood gets little attention in policy debates. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

Anyone who tries to draw attention to threats instead of quietly burying them is worsening the problem. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

In an effort to gain early attention, he focused his attention on the Iowa precinct caucuses, which had never mattered much. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

This attracted much attention, and the London journals praised the artist. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

His parents were peasants and he wrought as a day laborer till he attracted attention. The Every Day Book of History and Chronology |Joel Munsell 

The little crowd and the boats on the beach were right under them and no one paid any attention or seemed to be in a hurry. Gallipoli Diary, Volume I |Ian Hamilton 

The card table profitably occupies some six to eight hours daily of these old fellows' attention. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Claude de Vert died; he devoted much attention to the ceremonies of the church of Rome, of which he wrote a history. The Every Day Book of History and Chronology |Joel Munsell