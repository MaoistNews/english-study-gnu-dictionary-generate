He identifies as transgender on Scruff but not always on Grindr. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

They are always suspended over a precipice, dangling by a slender thread that shows every sign of snapping. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

The motives were most always harmless, and only sometimes ethically questionable. Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 

A guard is manning the door, which is always kept ajar so she can be monitored. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 

I always wanted my life to be that way, and it became that way. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

This is a feature by means of which it is always possible to distinguish the Great Horsetail from any other species. How to Know the Ferns |S. Leonard Bastin 

In the drawing-room things went on much as they always do in country drawing-rooms in the hot weather. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

You see, I'd always thought of him as the boy whom Great-aunt Lucia described having seen. The Boarded-Up House |Augusta Huiell Seaman 

Many British Ferns evidence a marked tendency to “sport,” and this is a fact which the beginner should always bear in mind. How to Know the Ferns |S. Leonard Bastin 

You see, they always butter their chairs so that they won't stick fast when they sit down. Davy and The Goblin |Charles E. Carryl