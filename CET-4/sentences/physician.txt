“For inpatient studies, the EAU makes studying this treatment more difficult,” says Daniel Hanley, a physician who directs multisite clinical trials at Johns Hopkins. COVID-19 plasma treatments may be safe, but we don’t know if they work |Tina Hesman Saey |August 25, 2020 |Science News 

The remaining five countries do not have even a single physician per 1,000 people. Why South Asia’s COVID-19 Numbers Are So Low (For Now) |Puja Changoiwala |June 23, 2020 |Quanta Magazine 

Places like a patient’s bedroom in their home, some nursing home rooms and a classroom would all be concerning to me as a physician. The CDC Now Recommends Wearing A Mask In Some Cases – A Physician Explains Why And When To Wear One |LGBTQ-Editor |April 12, 2020 |No Straight News 

I believe it was the physician that I saw, asked me if I was interested in getting help. The Opioid Tragedy, Part 2: “It’s Not a Death Sentence” (Ep. 403) |Stephen J. Dubner |January 23, 2020 |Freakonomics 

So in general you know physicians will tell you not to dispose of medication in those ways but that it should be disposed of safely. The Opioid Tragedy, Part 1: “We’ve Addicted an Entire Generation” (Ep. 402) |Stephen J. Dubner |January 16, 2020 |Freakonomics 

That distant whirring sound you hear is a long-dead Greek physician spinning in his grave. Why So Many Surgeons Are Psychos |Russell Saunders |December 17, 2014 |DAILY BEAST 

An electrocardiogram is taken over the phone and then sent to his physician for examination. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The only physician she was permitted to see was the jail doctor. States Slap Pregnant Women With Harsher Jail Sentences |Emily Shire |December 12, 2014 |DAILY BEAST 

This is entirely understandable—after all, it is unsettling that a physician could make such an obvious mistake. What It’s Like to Wake Up Dead |Dr. Anand Veeravagu, MD, Tej Azad |November 21, 2014 |DAILY BEAST 

Maynard was 29 years old at the time of her death, able to request drugs from her physician after considered reflection. U.K. Courts Grant Mother Right to End Her 12-Year-Old Disabled Daughter’s Life |Elizabeth Picciuto |November 4, 2014 |DAILY BEAST 

Honour the physician for the need thou hast of him: for the most High hath created him. The Bible, Douay-Rheims Version |Various 

Give a sweet savour, and a memorial of fine flour, and make a fat offering, and then give place to the physician. The Bible, Douay-Rheims Version |Various 

William Woodville died; a distinguished English physician and medical writer. The Every Day Book of History and Chronology |Joel Munsell 

Christopher Bennet died; a distinguished London physician, and writer on medical subjects. The Every Day Book of History and Chronology |Joel Munsell 

He was afterwards a member of the Massachusetts senate, and much esteemed as a physician and a patriot. The Every Day Book of History and Chronology |Joel Munsell