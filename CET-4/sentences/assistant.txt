Mimi Gehin, a director’s assistant for global production company Stink Films, lives with five other people in a warehouse in East London. ‘It can take on a panopticon effect’: Slack’s presenteeism problem grows with no end in sight for remote work |Lucinda Southern |August 28, 2020 |Digiday 

That shared trait, he said, may help researchers find a way to get proof assistants to, in some sense, explain themselves. How Close Are Computers to Automating Mathematical Reasoning? |Stephen Ornes |August 27, 2020 |Quanta Magazine 

Fitbit had more success with the $200 Versa, released in 2018, and last year’s follow up, the Versa 2, which first added the ability to connect with Amazon’s digital assistant Alexa. Fitbit debuts new smartwatches that track how stressed the owners are |Aaron Pressman |August 25, 2020 |Fortune 

Jessie Buckley is fantastic as her long-suffering British assistant Rosalyn Wilder and Rufus Sewell is fascinating as Judy’s third husband Sid Luft. FROM THE VAULTS: Best Actress edition |Brian T. Carney |August 20, 2020 |Washington Blade 

Hannah Kerner is an assistant research professor at the University of Maryland in College Park. Too many AI researchers think real-world problems are not relevant |Amy Nordrum |August 18, 2020 |MIT Technology Review 

The garrulous assistant to a fading screen siren in Clouds of Sils Maria. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 

Thankfully, his assistant knows these roads like the back of his hand. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

Through a work-study program with the school, he is a Program Assistant at the UNC-Chapel Hill LGBT Center. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

The assistant manager at the A&F store had found Elauf qualified for the position and was apparently going to hire her. Muslims & Jews Unite vs. Abercrombie & Fitch |Dean Obeidallah |December 16, 2014 |DAILY BEAST 

I spent four years in a prison where each handicapped convict was issued an underpaid inmate assistant. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

Of the parts the young assistant of Nicolas Amati was allowed to put his individuality to, conspicuously stands the scroll. Antonio Stradivari |Horace William Petherick 

Sometimes the tracing down may have been done by some advanced pupil or competent assistant. Antonio Stradivari |Horace William Petherick 

The Assistant Commissioner, hand pressed to brow, began to study a document which lay before him. Dope |Sax Rohmer 

"Certainly—certainly," said the Assistant Commissioner, waving one large hand in the direction of a bookshelf. Dope |Sax Rohmer 

"Certainly—certainly," murmured the Assistant Commissioner, glancing up absently. Dope |Sax Rohmer