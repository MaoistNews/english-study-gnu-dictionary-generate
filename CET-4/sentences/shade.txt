The poor representation of people of color from around the world, and their range of facial features and skin shades, creates what researchers have called a “demographic bias” built into the technology. The Bias in the Machine - Issue 89: The Dark Side |Sidney Perkowitz |August 19, 2020 |Nautilus 

That means the upper tiers of Wrigley are closer to the playing surface than those of parks that opened from 1992 to 2017, which have an average lower-deck shade of 26 percent. MLB’s Newest Ballpark Is A Shift Away From Retro-Era Stadiums |Travis Sawchik |July 16, 2020 |FiveThirtyEight 

In the videos the dogs lie around, keeping cool in the shade. How to Make Meetings Less Terrible (Ep. 389 Rebroadcast) |Stephen J. Dubner |May 28, 2020 |Freakonomics 

Another element to note was a slight front projection of the skull that not only provided the user with some shade but also provided practical protection from downward angled sword blows aimed at the head. Know Your Ancient Greek Helmets: From Attic to Phrygian |Dattatreya Mandal |May 19, 2020 |Realm of History 

Trees provide homes for animals and shade for people on hot days. Let’s learn about trees |Sarah Zielinski |April 22, 2020 |Science News For Students 

Petty, shade, and thirst are my favorite human “virtues” and the trifecta of any good series of “stories.” ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

It took me 1,015 strokes to see this shade of green in a world of orange, and my jaw nearly dropped. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

But publicly throwing shade at Louis Vuitton wasn't Kanye's first fashion faux pas. Kanye West and Kim Kardashian’s Balmain Campaign: High Fashion Meets Low Culture |Amy Zimmerman |December 23, 2014 |DAILY BEAST 

Later schools empty out children, who race over to play games in the shade. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

In the summer and in hotter regions, they provide shade for parked cars, preventing them from getting too hot. Paved Paradise |The Daily Beast |September 24, 2014 |DAILY BEAST 

The Seneschal leaned back, and was concerned to feel his pulses throbbing a shade too quickly. St. Martin's Summer |Rafael Sabatini 

The seeds of some species are of a dark brown while others are of a lighter shade. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Lady Maude sat alone in her room; the white robes upon her, the orthodox veil, meant to shade her fair face thrown back from it. Elster's Folly |Mrs. Henry Wood 

Nothing will be easier then to throw the Poles into the shade of the picture, or to occupy the foreground with a brilliant review. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Grandfather Mole demanded of Mr. Meadow Mouse, almost as soon as he had stepped just outside the shade of the toadstool. The Tale of Grandfather Mole |Arthur Scott Bailey