That’s because plastics are mainly made up of carbon and hydrogen, very widespread elements. Microplastics are everywhere. Here’s what that means for our health. |Ula Chrobak |February 11, 2021 |Popular-Science 

Making renewable hydrogen requires making electricity to send a charge through water to split the liquid into hydrogen and oxygen. A startup using a new tech to make hydrogen extracts cash from Bill Gates’ climate tech fund |Jonathan Shieber |February 9, 2021 |TechCrunch 

There, a chemical process breaks the bond between water’s hydrogen and oxygen molecules, forming heat. The Kora Xenolith Is My Secret Weapon Against the Cold |Wes Siler |February 2, 2021 |Outside Online 

To begin, the scientists shined X-ray light on hydrogen gas. Physicists have clocked the shortest time span ever |Maria Temming |January 29, 2021 |Science News For Students 

Polyethylene is a long molecule, in which hydrogen atoms are connected to a carbon backbone that can be thousands of carbon atoms long. Chemists are reimagining recycling to keep plastics out of landfills |Maria Temming |January 27, 2021 |Science News 

Methane (chemical formula CH4) is one of the simplest hydrocarbons, which literally means “containing hydrogen and carbon.” Methane on Mars: Life or Just Gas? |Matthew R. Francis |December 17, 2014 |DAILY BEAST 

From that, they extracted the ratio of the number of deuterium atoms to the number of hydrogen atoms. Are Comets the Origin of Earth’s Oceans? |Matthew R. Francis |December 14, 2014 |DAILY BEAST 

They found that there are roughly 1,900 hydrogen atoms for each deuterium atom in the water on Comet 67P. Are Comets the Origin of Earth’s Oceans? |Matthew R. Francis |December 14, 2014 |DAILY BEAST 

Deuterium is an isotope of hydrogen containing a proton and neutron in its nucleus, while normal hydrogen has only a proton. Are Comets the Origin of Earth’s Oceans? |Matthew R. Francis |December 14, 2014 |DAILY BEAST 

Jupiter and its cousins, by contrast, are mostly made of hydrogen and hydrogen compounds. Mega-Earth Is the Weirdest Exoplanet Yet |Matthew R. Francis |June 8, 2014 |DAILY BEAST 

Hydrogen sulphid is easily prepared in the simple apparatus shown in Fig. 30. A Manual of Clinical Diagnosis |James Campbell Todd 

It is allowed to cool, and hydrogen sulphid gas is passed through it for about five minutes. A Manual of Clinical Diagnosis |James Campbell Todd 

Ammonia is a compound of nitrogen and hydrogen, but it cannot be formed by the direct union of these gases. Elements of Agricultural Chemistry |Thomas Anderson 

One morning he found that the amount of hydrogen was scarcely perceptible; still there was water in the pit. Black Diamonds |Mr Jkai 

The older workman reassured them; the carbon was much heavier than oxygen, and even thicker than hydrogen. Black Diamonds |Mr Jkai