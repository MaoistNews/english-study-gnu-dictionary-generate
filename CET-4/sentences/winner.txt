Congratulations to Patrick Boylan of Alexandria, Virginia, winner of last week’s Riddler Classic. Can You Cover The Globe? |Zach Wissner-Gross |August 28, 2020 |FiveThirtyEight 

If they just manage to stroll, they’ll rank among the market’s big winners. Despite Warren Buffett’s selloff, bank stocks look like great buys in this market |Shawn Tully |August 18, 2020 |Fortune 

The winner will begin a long, exclusive negotiating relationship with city staff to come up with much more specific plans about what will happen to the vast tract of city-owned land in the area. Politics Report: Who Will Get the Midway Rose? |Scott Lewis and Andrew Keatts |August 15, 2020 |Voice of San Diego 

The winner will determine whether the county has a Republican or Democratic majority. Morning Report: SANDAG Head Wades Into Supes Race |Voice of San Diego |August 14, 2020 |Voice of San Diego 

“If someone is the sole bread winner in their family and tests positive, we need to have a support system in place, so they don’t have to go to work,” she said. Coronavirus Hit Latinos Harder Thanks to a Perfect Storm of Disparities |Maya Srikrishnan |August 12, 2020 |Voice of San Diego 

With a .700 career winning percentage as a coach in college and the NFL, Harbaugh is a winner. Is Any College Football Coach Worth $60 Million? Jim Harbaugh Is |Jesse Lawrence |December 20, 2014 |DAILY BEAST 

The outcome of the rum feud is critical for both Bacardi and Pernod Ricard, because the winner could net billions in future sales. Why Congress Hates Your Cuban Rum |Tim Mak |December 19, 2014 |DAILY BEAST 

The winner will be the one who most squarely gets on the side of the working class. Bush, Christie, Romney: Who’ll Be the GOP Class Warrior? |Lloyd Green |December 15, 2014 |DAILY BEAST 

The outré character is sure to throw even the most ardent fans of the Golden Globe winner for a loop. Michael C. Hall on Going Drag for ‘Hedwig and the Angry Inch’ and Exorcising ‘Dexter’ |Marlow Stern |December 4, 2014 |DAILY BEAST 

The worst is probably to avoid paying taxes on the money and go to prison, like Survivor season one winner Richard Hatch. From Socially Isolated Nerd to Jeopardy! Bad Boy: A Thank You Note |Arthur Chu |November 27, 2014 |DAILY BEAST 

Sometimes Yung Pak would be the winner, and then he would march home with great glee and show the trophy to his father. Our Little Korean Cousin |H. Lee M. Pike 

He repeated over and over, the name of each winner as it appeared on the huge signboard. The Box-Car Children |Gertrude Chandler Warner 

The suspense of waiting for the committee to decide upon the winner of the prize was hard to endure indeed. The Girls of Central High on the Stage |Gertrude W. Morrison 

Mr. Monterey will confer with us at noon, and before school is dismissed to-day we will announce the winner. The Girls of Central High on the Stage |Gertrude W. Morrison 

We have tried to decide upon the prize winner in a way that will satisfy the giver of the prize, tooMrs. The Girls of Central High on the Stage |Gertrude W. Morrison