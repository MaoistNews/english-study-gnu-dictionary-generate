There was nothing I could do about it, but it was also a very vivid period of my life. 'People Expect a Woman to Grieve a Certain Way.' What One Mom Learned About Trauma and Strength After Losing Her Young Son |Belinda Luscombe |January 20, 2021 |Time 

The book begins on a hill above his neighborhood, a favorite place from childhood, and though the place has changed, the memories remain vivid. Gabriel Byrne’s ‘Walking with Ghosts’ is a revelation in unexpected ways |Keith Donohue |January 12, 2021 |Washington Post 

This year demonstrated the importance of agility in a particularly vivid way, as companies of all sizes had to integrate creative solutions to implement transitions of people and their work environments. Communication and courage: The top 5 corporate leadership lessons from 2020 |matthewheimer |December 31, 2020 |Fortune 

Ideal for middle school through college, this enhanced calculator has a vivid backlit display, rechargeable battery, preloaded apps, and MathPrint mode. The best graphing calculators for students |PopSci Commerce Team |September 4, 2020 |Popular-Science 

Part of this inspires my dreams of the global, multidimensional, interactive classroom where you can have a vivid, virtual meta-verse that allows real people from around the world to communicate with and learn from one another. Book recommendations from Fortune’s 40 under 40 in tech |Rachel King |September 4, 2020 |Fortune 

Yet for a vivid decade or so, sleaze was, somewhat paradoxically, a force for literacy and empowerment. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 

Today, with the memories of Ingrid Bergman so vivid in his mind, it seems clear that he's been thinking about her a great deal. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

By the end of his life, the memories of corporal punishment at the hands of his teachers were vivid. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

For such songs, she pairs raunchy lyrics with vivid imagery. From Church of Christ to Pansexual Rapper |Tyler Gillespie |November 28, 2014 |DAILY BEAST 

This particular bit of airplane was vivid with the possibility that it was a significant clue. How Amelia's Plane Was Found |Michael Daly |October 30, 2014 |DAILY BEAST 

First Impressions are usually vivid but the power to revive them is weak—a poor memory. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

And the most vivid First Impressions always result from the action of the intellect upon the sensuous stimuli from ear and eye. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

He was a new force and his pages kindled in our hearts a vivid feeling for the poor and their wrongs. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Traveling is useful in that it gives us a more vivid idea of the immense amount of knowledge we yet lack. Glances at Europe |Horace Greeley 

A more vivid concurrence can scarcely be imagined, since he and Bonaparte were both born in the same year, 1769. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)