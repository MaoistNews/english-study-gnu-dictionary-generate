Adjusting to changes at the drop of a hat is common in most kitchens, but it’s something Rivera was used to well before he started working in restaurants. Eric Rivera Is Playing the Game |Alberto Perez |October 1, 2020 |Eater 

Decades after her stint in Scandinavia, she still watched Ingmar Bergman films without the subtitles — and wore a small top hat, conferred on her along with an honorary doctorate by the University of Lund, to academic occasions. The Trip Abroad That Awakened RBG to Feminism |Fiona Zublin |September 19, 2020 |Ozy 

For a fleeting moment, I was Jackie Kennedy sans the pink Chanel suit and matching pillbox hat. Anti-maskers assaulted my husband |Matt Foreman |September 1, 2020 |Washington Blade 

Don’t buy GOODYEAR TIRES – They announced a BAN ON MAGA HATS. Trump’s call for a Goodyear boycott might mean new tires for the presidential limo |Claire Zillman, reporter |August 20, 2020 |Fortune 

In high school, we began knitting, starting with sweaters and hats. Crafting our path |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Stetson returned east in 1865 and created his own hat company, which produced high-quality hats made for outdoor use. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

Based on the hat he had created for himself, Stetson made a version called “The Boss of the Plains.” My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

While panning for gold, he made himself a large hat from the hides he had collected on his trip. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

John B. Stetson was born in 1830 in New Jersey, the son of a hat maker. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

A gifted marketer, he sent samples of the hat to merchandisers all over the West, asking for a minimum order of a dozen. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

Behold a dumpy, comfortable British paterfamilias in a light flannel suit and a faded sun hat. God and my Neighbour |Robert Blatchford 

On his head was the second-hand hat of some parvenu's coachman, gold lace, cockade and all. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

"I hope you don't think I speak always to strangers, like that," said the girl in the rose hat. Rosemary in Search of a Father |C. N. Williamson 

Afterwards we saw you once or twice at tea at the Ritz, and you took off your hat, so you must have remembered then. Rosemary in Search of a Father |C. N. Williamson 

His face flushed with annoyance, and taking off his soft hat he began to beat it impatiently against his leg as he walked. The Awakening and Selected Short Stories |Kate Chopin