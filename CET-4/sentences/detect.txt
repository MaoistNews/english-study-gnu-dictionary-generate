The Australian state of Victoria has entered a five-day coronavirus lockdown to curb a local outbreak of the coronavirus variant first detected in Britain. Novak Djokovic’s five-set battle at Australian Open started with fans and ended without them |Matt Bonesteel |February 12, 2021 |Washington Post 

As the signal faded, some telescopes also detected fluctuations in the light. Exploding neutron star proves to be energy standout of the cosmos |Lisa Grossman |February 12, 2021 |Science News For Students 

Now, by detecting the explosions’ infrared light, which penetrates dust better than visible light does, Caltech astronomer Kishalay De and his colleagues have estimated how often these outbursts occur in the Milky Way. The number of Milky Way nova explosions per year has been pinned down |Ken Croswell |February 12, 2021 |Science News 

There might be many more, Nesbitt said, as only a small sample of all positive coronavirus tests are sequenced to detect variants. South Africa and U.K. coronavirus variants detected in D.C.; Maryland to open third mass vaccination site |Erin Cox, Julie Zauzmer, Rachel Chason |February 11, 2021 |Washington Post 

They mutate and develop new strains, which makes it very difficult to detect them. Among Latino immigrants, false vaccine claims are spreading as fast as the virus |Teo Armus |February 11, 2021 |Washington Post 

No one who lives in an American city requires a flashlight to detect the presence of immigrants or the challenges they face daily. The 2014 Novel of the Year |Nathaniel Rich |December 29, 2014 |DAILY BEAST 

That could change, however, if ISIS obtained the knowledge to build hard-to-detect bombs. U.S. Clams Up on Xmas Airline Bomb ‘Plot’ |Shane Harris |December 2, 2014 |DAILY BEAST 

Testing methods can now detect HIV within ten days of infection. The Outrageous Celibacy Requirement for Gay Blood Donors |Jay Michaelson |November 22, 2014 |DAILY BEAST 

The company is already brainstorming what it can detect next. The Gluten Detecting Device You’ll Want to Own |DailyBurn |October 10, 2014 |DAILY BEAST 

There is only one approved, working test that can detect whether or not Ebola is present in the blood. This New Ebola Test Is As Easy As a Pregnancy Test, So Why Aren’t We Using It? |Abby Haglage |October 3, 2014 |DAILY BEAST 

Yet I think if we observe closely we shall detect traces of a spontaneous impulse towards self-adornment. Children's Ways |James Sully 

Tubercle bacilli are nearly always present, although animal inoculation may be necessary to detect them. A Manual of Clinical Diagnosis |James Campbell Todd 

It is frequently desirable to detect formalin, which is the most common preservative added to cow's milk. A Manual of Clinical Diagnosis |James Campbell Todd 

Fetherston strained his eyes towards the horizon, but declared that he could detect nothing. The Doctor of Pimlico |William Le Queux 

This she had the wit to detect, as well as the incontrovertible fact that her youth and her chances were gone. Ancestors |Gertrude Atherton