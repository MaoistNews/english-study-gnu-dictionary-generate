When Unicef held a consultation workshop in East Asia, Zeng attended as a speaker. Why kids need special protection from AI’s influence |Karen Hao |September 17, 2020 |MIT Technology Review 

Patrick Henry cheer, like other school sports, largely went dark, but the school’s junior varsity coach was not eager to shut down a private offseason competition team attended by some students at the Infinity Gymnastics gym in El Cajon. School Sports Became ‘Clubs’ Amid the Pandemic – Now Two Coaches Are Out |Ashly McGlone |September 17, 2020 |Voice of San Diego 

While all of BI’s virtual events are free to attend with email registration, they can be monetized in several different ways. ‘Eager to explore more’: Business Insider’s virtual events strategy lifts volume and plants evergreen on-demand feature |Max Willens |September 16, 2020 |Digiday 

Billington, who attends school with Baxter, said she would continue using TikTok regardless of its owner “as long they don’t change it and turn it into Instagram,” a TikTok rival. What’s Oracle? TikTok users react to proposed Oracle deal |Danielle Abril |September 15, 2020 |Fortune 

We attended to connect with potential clients who need mobile applications for their businesses. Last day to save on passes to TC Sessions: Mobility 2020 |Alexandra Ames |September 11, 2020 |TechCrunch 

In 1995, Myerson made a point not to attend the 75th anniversary of the Miss America pageant. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Those who are not working on Sunday will almost certainly attend the funeral for Liu. Cop Families Boo De Blasio at NYPD Graduation |Michael Daly |December 30, 2014 |DAILY BEAST 

In neighborhoods such as Harlem, 33 percent of students attend charter schools, a majority of them black or Latino. How Public Sector Unions Divide the Democrats |Daniel DiSalvo |December 29, 2014 |DAILY BEAST 

“Five of them would attend a show and each one would memorize a certain part of a garment,” said Elia. The Big Business of Fashion Counterfeits |Lizzie Crocker |December 24, 2014 |DAILY BEAST 

A few even noted that they attend Christmas mass with Christian friends. Why Muslims Love Jesus Too |Dean Obeidallah |December 23, 2014 |DAILY BEAST 

While Louis was reading these dispatches, he received a summons from Elizabeth, to attend her immediately. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Full provision is made for Catholics and Nonconformists desiring to attend the services of their respective bodies. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Will your Majesty be pleased to have them given a goodly number of religious, so that they may attend to their ministries. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Hedges had to go on his way also, for it was close upon the countess-dowager's dinner-hour, at which ceremony he must attend. Elster's Folly |Mrs. Henry Wood 

"Would you like to attend services at the church this evening," said Irene after a time, and when they were again alone. The Homesteader |Oscar Micheaux