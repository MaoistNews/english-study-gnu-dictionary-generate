Marx could only find evidence of five tickets since December – so far. Morning Report: Not Much COVID Enforcement Happening Despite Crackdown |Voice of San Diego |February 12, 2021 |Voice of San Diego 

Ground surveys at Waun Mawn in 2011 failed to find evidence of a stone circle. Stonehenge may have had roots in a Welsh stone circle |Bruce Bower |February 12, 2021 |Science News 

Since Gloria’s announcement in December, Voice of San Diego could only find evidence of five new tickets. Despite Crackdown Announcement, Not Much COVID-19 Enforcement Is Happening |Jesse Marx |February 11, 2021 |Voice of San Diego 

Umansky and research fellow Simon showed that the NYPD frequently withholds evidence from civilian investigations into police abuse. ProPublica’s “NYPD Files” Wins John Jay College/Harry Frank Guggenheim Award for Excellence in Criminal Justice Reporting |by ProPublica |February 11, 2021 |ProPublica 

She’s been disappointed by some friends’ lack of effort to connect during the pandemic,and wrecked by photographic evidence that other friends have gotten together without inviting her. The loneliness of an interrupted adolescence |Ellen McCarthy |February 11, 2021 |Washington Post 

But sources said that the evidence so far is pointing away from an ISIS connection. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

And the fact that satire unnerves the intolerant is evidence of its positive power. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

Is it sort of evidence of the Gladwellian 10,000 hours theory? Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Evidence is piling up that as men do more of the caregiving, violence against women falls. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

The third problem is the evidence of corroborating witnesses. Buckingham Palace Disputes Sex Allegations Against Prince ‘Randy Andy’ |Tom Sykes |January 4, 2015 |DAILY BEAST 

Many British Ferns evidence a marked tendency to “sport,” and this is a fact which the beginner should always bear in mind. How to Know the Ferns |S. Leonard Bastin 

It is a very common error to consider these deposits as evidence of excessive excretion. A Manual of Clinical Diagnosis |James Campbell Todd 

It was a tremendous training in the sifting of evidence and the examination of appearances. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

While this reaction lasted he laughed away the evidence, and honestly believed he was exaggerating trifles. The Wave |Algernon Blackwood 

And this fact seemed pregnant with evidence as to Gordon's state of mind; it did not appear to simplify the situation. Confidence |Henry James