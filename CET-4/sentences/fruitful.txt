We’d do well, then, to seek guidance from Darrel Morrison, who has spent a fruitful life designing with native flora and teaching others how to do the same. A native plant designer’s memoir reflects on a life in the field |Adrian Higgins |August 25, 2021 |Washington Post 

In this regard, one of the most fruitful research approaches has involved the study of how nerve cells communicate with other cells in the body, and how various drugs might alter this communication. A piece of our mind |The Editors |August 25, 2021 |MIT Technology Review 

Forty-seven years ago, Japan used the Olympics Opening Ceremony to announce itself to the world as a modern, fruitful society. The Tokyo Olympics Opening Ceremony Was Depressing as Hell |Kevin Fallon |July 23, 2021 |The Daily Beast 

Transits soon overtook wobbles as the most fruitful planet-finding strategy. A century of astronomy revealed Earth’s place in the universe |Lisa Grossman |July 21, 2021 |Science News 

To those who looked myopically only at recent years of the harvest, which had been quite sufficient, he pointed out that those years had been unusually fruitful, which masked the problem. Why I’m a proud solutionist |Jason Crawford |July 13, 2021 |MIT Technology Review 

These are all fruitful options to pursue for any atheist interested in challenging the immoral stereotypes we have. Loud, Proud, and Atheist: ‘Openly Secular’ Encourages Nonbelievers to Come Out of the Closet |Vlad Chituc |September 25, 2014 |DAILY BEAST 

After a fruitful year (earning $81 total) he expanded his enterprise. The Ghost Hotels of the Catskills |Brandon Presser |August 25, 2014 |DAILY BEAST 

That marked the beginning of a fruitful long-term relationship with his Ethiopian clients. 'Made in China' Now Being Made in Africa |Brendon Hong |August 23, 2014 |DAILY BEAST 

Then, defying all logic, it somehow blossomed into an amazing, fruitful romance. 'You're the Worst': TV's Best Couple Is Awful and Perfect for Each Other |Jason Lynch |August 21, 2014 |DAILY BEAST 

Reporters found that scouring the Internet for remarks made by Ukip members was proving fruitful. Is Britain’s Tea Party Turning Politics Upside Down? |Nico Hines |April 30, 2014 |DAILY BEAST 

Nothing, however, can save us but a union, which would turn our barren hills into fruitful valleys. The Book of Anecdotes and Budget of Fun; |Various 

Our escort was mounted within a few minutes, and we were in full gallop over the fruitful levels of Champagne. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

It seemed an unpromising subject, but they fell upon it with ardour, and found it strangely fruitful. Bella Donna |Robert Hichens 

Why did he quit the fruitful banks of the Euphrates for a spot so remote, so barren, and so stony as Sichem? A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

One of the most fruitful fields for this instrument is undoubtedly stellar spectroscopy. Photographs of Nebul and Clusters |James Edward Keeler