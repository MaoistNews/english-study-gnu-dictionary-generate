When it comes to beloved products, we are “very good at rationalizing in order to reduce this discomfort while still consuming the product we want to consume,” John said. MyPillow boycott: How a product can spark an identity crisis |Elizabeth Chang |February 12, 2021 |Washington Post 

He said he is not a proponent of double masking because it consumes more masks and can also lead to more air leakage. Masks should fit better or be doubled up to protect against coronavirus variants, CDC says |Lena H. Sun, Fenit Nirappil |February 11, 2021 |Washington Post 

Newsletters support the Times’ subscription-driven business by getting readers into the habit of consuming its content everyday via The Morning. The New York Times aims to convert newsletter readers into paid subscribers as The Morning newsletter tops 1 billion opens |Sara Guaglione |February 10, 2021 |Digiday 

Intent-signaling behaviors — observable as users search, click, consume and repeat — are the lifeblood of the digital advertising ecosystem and something publishers must honor. Splitting the atom: Decoupling audience from inventory unleashes power of pubs |Trevor Grigoruk |February 9, 2021 |Digiday 

If the cost of consuming water wasn’t high enough in San Diego, consider the cost of getting rid of it. Environment Report: The High Cost of Getting Rid of Water |MacKenzie Elmer |February 8, 2021 |Voice of San Diego 

The French still consume more than half of the Champagne produced. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

And as technology adapts to reflect the ways we consume media, so too is the family adapting to technology. Binge Watching is the New Bonding Time |The Daily Beast |December 10, 2014 |DAILY BEAST 

This is just the way we consume TV now: our favorite shows, whenever we want them, wherever we want them. New Innovations Let You Watch TV Anywhere You Go | |December 8, 2014 |DAILY BEAST 

And new innovations continue to make wireless, on-demand viewing the new way our families consume TV. New Innovations Let You Watch TV Anywhere You Go | |December 8, 2014 |DAILY BEAST 

You consume a nutritionally unbalanced diet because of concerns about “food purity.” Orthorexia: When Healthy Eating Becomes an Obsession |DailyBurn |October 25, 2014 |DAILY BEAST 

And it shall devour the mountains, and burn the wilderness, and consume all that is green as with fire. The Bible, Douay-Rheims Version |Various 

Aristide stood gossiping until the Mayor invited him to take a place at the table and consume liquid refreshment. The Joyous Adventures of Aristide Pujol |William J. Locke 

She thought the idol would consume them, for bachelor cooking was never intended for bachelor invalids. The Soldier of the Valley |Nelson Lloyd 

The fire also being sent from above to consume mountains, and woods, doth as it is commanded. The Bible, Douay-Rheims Version |Various 

I said therefore that I would pour out my indignation upon them in the desert, and would consume them. The Bible, Douay-Rheims Version |Various