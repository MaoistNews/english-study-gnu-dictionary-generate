Boston Dynamics is widely recognized as a pioneer in the field of animal-like robots. Boston Dynamics adds an ‘arm’ to its robotic dog Spot |Dalvin Brown |February 4, 2021 |Washington Post 

Following Hyundai acquisition, Boston Dynamics’ CEO discusses the robotics pioneer’s future SoftBank teams with home-goods maker Iris Ohyama for new robotics venture |Brian Heater |January 27, 2021 |TechCrunch 

Since then, Wahl has been a trimmer technology pioneer, creating the first cordless rechargeable hair clipper in 1967, the first cordless beard and mustache trimmer in 1975, and the first vacuuming consumer beard trimmer in 2001. The best beard trimmer: Shape your facial hair with ease |Carsen Joenk |January 19, 2021 |Popular-Science 

In addition, mRNA could in the future be used, as BioNTech and Moderna are pioneering, to fight cancer. mRNA Technology Gave Us the First COVID-19 Vaccines. It Could Also Upend the Drug Industry |Walter Isaacson |January 11, 2021 |Time 

These characteristics are exactly the kind of attributes the SETI scientists have been looking for since the astronomer Frank Drake first began the pioneering initiative some 60 years ago. SETI: New Signal Excites Alien Hunters—Here’s How We Could Find Out if It’s Real |Michael Garrett |January 7, 2021 |Singularity Hub 

Peter Christopherson made the leap to life on the bandstand and became a pioneer in the industrial music genre. The Golden Age of Rock Album Covers |Ted Gioia |December 5, 2014 |DAILY BEAST 

His great-grandfather, David Yellin, was a prominent Zionist scholar and Israeli pioneer. Trans in the Holy Land: ‘Marzipan Flowers,’ Tal Kallai, and the Shattering of Israel’s LGBT Taboos |Itay Hod |November 4, 2014 |DAILY BEAST 

The first pioneer to reach the riparian tributary where Kansas City now shimmers was, in fact, on the lam himself. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

Carver was an agricultural and industrial pioneer—in more ways than one. Growth Stocks |The Daily Beast |October 17, 2014 |DAILY BEAST 

And when he died, another agricultural pioneer was just starting to bring research to bear on food production. Growth Stocks |The Daily Beast |October 17, 2014 |DAILY BEAST 

Hamo in alluding to the early cultivation of tobacco by the colony, says, that John Rolfe was the pioneer tobacco planter. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Samuel Dale, an eminent pioneer in the settlement of the southwest, died in Lauderdale county, Mississippi. The Every Day Book of History and Chronology |Joel Munsell 

Roosevelt was also the pioneer in using electro-pneumatic action here. The Recent Revolution in Organ Building |George Laing Miller 

His high-pressure steam-engine was the pioneer of locomotion and its wide-spreading civilization. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Such is the story of a pioneer enterprise, that of the use of submarine vessels as commerce carriers. The Wonder Book of Knowledge |Various