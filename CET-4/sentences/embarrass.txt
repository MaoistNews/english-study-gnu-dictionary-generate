He’s still working out his identity as a writer, and thus far that identity has been well-intentioned celeb turned author who hasn’t embarrassed himself. David Duchovny wants to be taken seriously as a novelist. His new book makes a good case. |Mark Athitakis |February 1, 2021 |Washington Post 

I made a good living and I’m embarrassed to say that I never really loved it, but now, let’s backtrack. Danica Patrick on Why She Never Loved Racing and Her Post-Track Career |Joshua Eferighe |January 30, 2021 |Ozy 

Miss Manners has always disliked the public ranking of donors, apparently intended to embarrass the smaller ones into buying their way into a higher category. Miss Manners: Skip the ‘You’re welcome’ email |Judith Martin, Nicholas Martin, Jacobina Martin |January 19, 2021 |Washington Post 

These elite schools should be embarrassed by their graduates who have committed sedition and they must take action in response if they are to save their reputations. Harvard, Stanford, Yale: Denounce sedition of your graduates |Peter Rosenstein |January 14, 2021 |Washington Blade 

This revelation embarrassed Democratic members of the committee. Ted Cruz’s proposed election commission can only hurt the country |Stuart MacKay |January 6, 2021 |Washington Post 

After almost five months without a solution, the lack of initiative is starting to embarrass the Lebanese government. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

The mass dump suggests that whoever did this, their primary motivation was to embarrass Sony Pictures. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

The final question we should ask: are they pursuing justice and the rule of law, or merely silencing those who embarrass them? Sentencing Looms for Barrett Brown, Advocate for “Anonymous” |Kevin M. Gallagher |December 15, 2014 |DAILY BEAST 

Much of the praise of Pence is in this vein—he will not embarrass us. Mike Pence, Dark Horse in Training for 2016 |David Freedlander |June 27, 2014 |DAILY BEAST 

Nothing to see, just Republican witch hunts designed to embarrass the president and perhaps land blows against Hillary Clinton. The Scandal at the VA Is Real, and Obama Is Ducking It |Ron Christie |May 20, 2014 |DAILY BEAST 

Ministers deprecated the motion as tending to embarrass the administration, and defeat the very end for which it was proposed. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

As we approached Pomeroy the militia began to embarrass our march by felling trees and erecting barricades across the roads. Famous Adventures And Prison Escapes of the Civil War |Various 

Jane, the elder sister, was the more dignified and it was therefore easier to embarrass her. Napoleon's Young Neighbor |Helen Leah Reed 

The first questions must never perplex or embarrass the pupil, for they are very important. The Sabbath-School Index |Richard Gay Pardee 

Not being hot at his preaching there was less enthusiasm about him now, and the presence of the grinder seemed to embarrass him. Tess of the d'Urbervilles |Thomas Hardy