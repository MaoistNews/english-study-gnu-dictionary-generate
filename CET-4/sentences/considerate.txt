So while we will celebrate the end of this pandemic with our customers, we will remain conscious that it has hugely affected people and will be considerate of that fact in our marketing. Marketing Briefing: Ad execs and marketers say this Olympics has ‘lost its luster’ |Kristina Monllos |July 27, 2021 |Digiday 

He would spend that time envisioning who he was going to see and how he would be fully engaged and thoughtful and considerate and kind with whomever he encountered. We Need More Kindness in Our Lives: Let's Make 143 Day a National Holiday to Honor Mr. Rogers' Legacy |Gregg Behr and Ryan Rydzewski |May 21, 2021 |Time 

I should have been more considerate and taken more responsibility. I Was a Bad Dog Owner. Don’t Be Like Me. |Kate Siber |May 11, 2021 |Outside Online 

Advertisers’ businesses were grappling with the pandemic, and the networks wanted to be considerate. ‘An anomaly year’: In this year’s TV upfront market, agency executives expect a return to business as usual |Tim Peterson |May 10, 2021 |Digiday 

While brief unmasked interactions with servers are likely low risk, experts say you should still put your mask on when they approach your table to be considerate. Should I wear a mask outside? Experts weigh in on scenarios. |Allyson Chiu |April 22, 2021 |Washington Post 

The two strengthened ties over the years and now Krauss considers Epstein a “close” and “considerate” friend. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

She had a sweet, gentle voice, and she was obviously an educated and very considerate young lady. Gay Talese on Charlie Manson’s Home on the Range |Gay Talese |October 31, 2014 |DAILY BEAST 

A hard-working doctor (of any race) has rendered careful, considerate care to a complicated patient. How Obamacare Will Screw Black Doctors |Daniela Drake |May 26, 2014 |DAILY BEAST 

One appeal I got by email does ask for children's clothes, but says this: “Be considerate and thoughtful while giving clothes.” The Pointlessness of Some Disaster Charity After the Indian Floods |Dilip D’Souza |June 26, 2013 |DAILY BEAST 

“Considerate and thoughtful”: not words I could have used for myself after an earlier disaster. The Pointlessness of Some Disaster Charity After the Indian Floods |Dilip D’Souza |June 26, 2013 |DAILY BEAST 

I am not afraid of being crushed, for no doubt you would always remember to be polite, if not considerate. Ancestors |Gertrude Atherton 

My chief was as kind and considerate as ever, and I confided to him the thoughts that disturbed me. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

He received what she said with suitably impressed eyebrow and nods of considerate assent. Hilda |Sarah Jeanette Duncan 

Conventional, polite, considerate, and a great respecter of persons in authority was the Chinese sage. Beacon Lights of History, Volume I |John Lord 

I think I can risk my case as to care and friendship with a man who is considerate to little bears. David Lannarck, Midget |George S. Harney