These nanobodies were stable when dried and aerosolized, the researchers found, suggesting that they could be made into a nose spray. Treatments that target the coronavirus in the nose might help prevent COVID-19 |Laura Sanders |September 14, 2020 |Science News 

Right now the livestock markets are overall not very stable. Why Is China Cracking Down on Food Waste? |Daniel Malloy |September 3, 2020 |Ozy 

Which creates shelf-stable foods without the use of preservatives. America’s Hidden Duopoly (Ep. 356 Rebroadcast) |Stephen J. Dubner |September 3, 2020 |Freakonomics 

A second report, based on a geotechnical and structural inspection by the Millennium Engineers Group of Pharr, Texas, also hired by the National Butterfly Center, found that the fence was stable for now, but that it faces a host of issues. New Engineering Report Finds Privately Built Border Wall Will Fail |by Jeremy Schwartz and Perla Trevizo |September 2, 2020 |ProPublica 

In July, researchers showed they could get a spin-orbit qubit implemented in silicon to last for about 10 milliseconds, while trapped ion qubits can stay stable for as long as 10 minutes. Could Quantum Computing Progress Be Halted by Background Radiation? |Edd Gent |August 31, 2020 |Singularity Hub 

At St. Barnabas Hospital, Pellerano was listed in stable condition with wounds to his chest and arm. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

His lone stable was a girl from Newport News, Virginia, who had already escaped one nightmare. The Navy ‘Hero’ Who Pimped an HIV-Positive Teen |M.L. Nestel |December 11, 2014 |DAILY BEAST 

The country, long viewed as stable, has recently been home to upheaval. An African Dictatorship’s Friend in D.C. |Center for Public Integrity |November 20, 2014 |DAILY BEAST 

The areas that were once the most food stable flipped on their heads when Ebola arrived, and are now the least. Liberia’s Ebola Famine |Abby Haglage, Nina Strochlic |November 13, 2014 |DAILY BEAST 

Unlike metals like iron, which rusts and corrodes dramatically, chromium remains stable after oxidation and subsequent burial. Why Did It Take So Long For Complex Life To Evolve On Earth? Blame Oxygen. |Matthew R. Francis |November 2, 2014 |DAILY BEAST 

Adjoining the engine-house on the other side, is the stable, where five splendid horses are kept. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Yet the word is general and apparently unconnected with the house, as it was not a stable but a boarding-house. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Dorothy led Jack off to the stable, and the half-frozen yeoman turned in to enjoy his cheerful fire. The World Before Them |Susanna Moodie 

When she went to go into her stable, Mace slowed her down till the street cars was gone by. Alec Lloyd, Cowpuncher |Eleanor Gates 

A broken broom, covered with very ancient cobwebs, lay under one manger, and the remnants of a stable-bucket under another. The Portsmouth Road and Its Tributaries |Charles G. Harper