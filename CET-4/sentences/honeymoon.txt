Bezos’s likely Amazon successor is an executive made in Bezos’s imageAs Klobuchar and other Amazon critics made clear when the executive transition was announced Tuesday, Jassy won’t get any sort of honeymoon period when he assumes his new post. Amazon CEO Jeff Bezos‘s successor will inherit his challenges |Jay Greene, Cat Zakrzewski |February 4, 2021 |Washington Post 

On the one hand, that could be unique to him since he was a historically unpopular presidential candidate, but on the other hand, there were signs that presidential honeymoon periods were shrinking even before him. Is The Presidential Honeymoon Over? |Geoffrey Skelley (geoffrey.skelley@abc.com) |January 26, 2021 |FiveThirtyEight 

The honeymoon is far from over for Wanda and Vision, and for Marvel Studios. ‘WandaVision’ on Disney Plus gives Marvel Studios its first true power couple |David Betancourt |January 15, 2021 |Washington Post 

It wasn’t my whole honeymoon, but I did get to spend several days where the whole day could be dedicated to writing and that really helped. Alexi Pappas's New Book Tackles the Heavy Stuff |Molly Mirhashem |January 11, 2021 |Outside Online 

They learned that the band Tame Impala would be playing a series of California shows in early March, and figured a road trip through the desert following their wedding at M3F — to see the band two days later — would make a great honeymoon. Going to a Festival … and Gonna Get Married |Joshua Eferighe |October 16, 2020 |Ozy 

There is no word on where or when the Clooneys might honeymoon, but the power couple surely will have to get back to work. After the Wedding: George Clooney and Amal Alamuddin in Venice |Barbie Latza Nadeau |September 28, 2014 |DAILY BEAST 

There is no word where the newlyweds will spend their honeymoon. In Run-Up to Wedding of George & Amal, Celebs and Paparazzi Stir Up the Canals of Venice |Barbie Latza Nadeau |September 27, 2014 |DAILY BEAST 

They take you up to the honeymoon, and then leave you there. Is ‘Satisfaction’ a Love Story That’s Too Real About Sex and Marriage? |David Masciotra |September 19, 2014 |DAILY BEAST 

Kanye West and Kim Kardashian spent their honeymoon in Ballyfin house, one of Ireland's most stunning and exclusive stately homes. Kanye and Kim's Irish Honeymoon Hideaway |Tom Sykes |May 30, 2014 |DAILY BEAST 

Cara Delevingne and Michelle Rodriguez's PDA-Filled Holiday: The honeymoon phase rages on. Cara Delevingne and Michelle Rodriguez's PDA-Filled Holiday; Emma Watson Talks Pressures of Fashion Industry |The Fashion Beast Team |March 31, 2014 |DAILY BEAST 

The bride and bridegroom will leave us after the breakfast to pass their honeymoon at the Lakes. A Charming Fellow, Volume II (of 3) |Frances Eleanor Trollope 

Still, she came quite to like him in time; and when they were married, Pepper went with them for the honeymoon.' The Talking Horse |F. Anstey 

Judy says he was yawnin' afore they got to the station on their honeymoon. Susan Clegg and a Man in the House |Anne Warner 

I shouldn't think of ever going on a honeymoon without them. Jane Journeys On |Ruth Comfort Mitchell 

But I cannot go off on a honeymoon unless I leave her in safety. Jane Journeys On |Ruth Comfort Mitchell