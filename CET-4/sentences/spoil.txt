It was as though he wanted to have the vast spoils of the world all for himself. The tangled history behind David Fincher’s Mank — and its link to Citizen Kane — explained |Alissa Wilkinson |December 4, 2020 |Vox 

When Jones County Confederates tried to steer a corn-filled wagon through the county en route to Mobile, Alabama, Newton and his fellow soldiers opened fire and took the wagon as spoils. These Pioneers Tried to Get the Confederate Flag Out of Mississippi 156 Years Ago |Fiona Zublin |December 3, 2020 |Ozy 

The problem is that the spoils from technology-driven productivity gains have not been shared equally. MIT Report: Robots Aren’t the Biggest Threat to the Future of Work—Policy Is |Edd Gent |November 23, 2020 |Singularity Hub 

Later, laaities in town would cross blocks to crowd into living rooms where we sat in front of the machines of more fortunate sons, garlanded in their spoils and praise as we cracked consoles our parents couldn’t afford. Fiction: Quiet earth philosophy |Katie McLean |October 21, 2020 |MIT Technology Review 

Connected TVs are streaming more video than ever to people in Europe and Samsung wants a share of the spoils. ‘We want to be as frictionless as possible’: Samsung ramps up its pitch to advertisers across Europe |Seb Joseph |October 15, 2020 |Digiday 

Rico Finally Paid: its easy fast money money and im welling to spoil you to def . The Sex-Trafficking Kings of Facebook |Michael Daly |May 20, 2014 |DAILY BEAST 

Of course I dismissed him for the day, and of course I paid him for the full time, that being the way we spoil our models. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

So in episode five—not to spoil anything—Cohle gives one of his metaphysical addresses. Inside the Obsessive, Strange Mind of True Detective’s Nic Pizzolatto |Andrew Romano |February 4, 2014 |DAILY BEAST 

At these wellness retreats, the staff will kick your butt—and then spoil you silly. Best Luxury Boot Camps to Get Fit in 2014 |Lonely Planet |December 31, 2013 |DAILY BEAST 

There are other ways in which the shutdown can spoil the holiday season – for retailers and workers. The Shutdown that Stole Christmas? |Daniel Gross |October 11, 2013 |DAILY BEAST 

She says she ain't going to spoil her children by sparing rods when our 'upper lot' is full of 'em. Dorothy at Skyrie |Evelyn Raymond 

If only we could obtain running powers to Limerick and carry them back to Ireland, we should have secured some of the spoil. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Thy riches and thy treasures I will give unto spoil for nothing, because of all thy sins, even in all thy borders. The Bible, Douay-Rheims Version |Various 

A soiled bonnet cap, untidy strings, or torn gloves and collar will utterly spoil the prettiest costume. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

They were rowing down the channel of the wider portion of the creek towards Isabel's landing, their boat filled with spoil. Ancestors |Gertrude Atherton