Outside, a flock of crows takes off in unison from the branches of an ancient oak. Near the end of life, my hospice patient had a ghostly visitor who altered his view of the world |Scott Janssen |January 2, 2021 |Washington Post 

Almost all young mammals play, as do birds like parrots and crows. Why It Pays to Play Around - Issue 94: Evolving |Andreas Wagner |December 16, 2020 |Nautilus 

Neuroscientists have become increasingly interested in birds like the crow, which appear to be able to think creatively in ways that mirror human cognition. Person, Woman, Man, Camera, TV - Issue 93: Forerunners |Adithya Rajagopalan |December 2, 2020 |Nautilus 

It’s the first time that researchers have observed this behavior in snakes, though animals like crows or raccoons eat some toads in a similar fashion. This snake rips a hole in living toads’ stomachs to feast on their organs |Erin Garcia de Jesus |October 2, 2020 |Science News 

Some sponsorship contracts are being sold for anything between five to 10 times lower than usual, Crow said. ‘We are permanently in beta’: European sports broadcasting is still in a coronavirus-forced state of reinvention |Lara O'Reilly |September 15, 2020 |Digiday 

This mass incarceration is destroying the Black community -- it is, as Michelle Alexander writes, the New Jim Crow. We Need More Ferguson-style Grand Juries |Kaimipono Wenger |November 30, 2014 |DAILY BEAST 

Thus, during Jim Crow, black men were routinely hanged, castrated, and lynched for alleged sexual assaults against white women. No Wonder Cosby's Keeping Quiet: He Could Still Be Prosecuted |Jay Michaelson |November 23, 2014 |DAILY BEAST 

Each working in its own way was essential to ending Jim Crow in the South. Honoring The Late John Doar, A Nearly Forgotten Hero Of The Civil Rights Era |Gary May |November 15, 2014 |DAILY BEAST 

Slowly, still falteringly but inexorably, Jim Crow justice was disappearing in the South. Honoring The Late John Doar, A Nearly Forgotten Hero Of The Civil Rights Era |Gary May |November 15, 2014 |DAILY BEAST 

“It tasted like a crow enchilada,” Morrissey said, as he literally ate his words. The Chicago Bulls’ Joakim Noah Sounds Off on Weed, the Weather, and Winning |Bill Schulz |October 19, 2014 |DAILY BEAST 

Not much use as the high crests hid the intervening hinterland from view, even from the crow's nests. Gallipoli Diary, Volume I |Ian Hamilton 

The cat had been about to spring at Grandfather Mole again when Mr. Crow spoke to her. The Tale of Grandfather Mole |Arthur Scott Bailey 

And he quite agreed with old Mr. Crow, who had come hurrying up to see what was going on. The Tale of Grandfather Mole |Arthur Scott Bailey 

Mr. Crow was rocking back and forth on his perch, for a joke—on anybody except himself—always delighted him. The Tale of Grandfather Mole |Arthur Scott Bailey 

Farmer Green's cat had never liked Mr. Crow, for no particular reason. The Tale of Grandfather Mole |Arthur Scott Bailey