When Tui Tuileta left Honolulu for college in 2016, he was renowned for his skills in volleyball, not baking. How Home Bakers Have Found Sweet Success During the Pandemic |Raisa Bruner |March 4, 2021 |Time 

Longtime residents of Lanikai remember a wide beach where they would gather with family and friends to play volleyball and football. Officials Let Hawaii’s Waterfront Homeowners Damage Public Beaches Again and Again |by Sophie Cocke, Honolulu Star-Advertiser |December 31, 2020 |ProPublica 

Adrion Howell, of Bowie, said his 14-year-old daughter, Aaliyah, misses playing volleyball with her travel club — especially because she does not get to see and hang out with her teammates. After telling more than 800 athletes and coaches to quarantine, a county halts youth sports |Ovetta Wiggins |November 30, 2020 |Washington Post 

Twin volleyball players Bria and Cimone Woodard transferred from Texas A&M to Howard. What Does Makur Maker Mean For The Future Of HBCU Sports? |Josh Planos |November 24, 2020 |FiveThirtyEight 

I miss doing theater, playing volleyball, and hanging out with my friends. It’s about time adults start rising up against climate change |Alexandria Villaseñor |October 21, 2020 |Popular-Science 

Inside the prison are regulation-size basketball and volleyball courts. Colorado Man Buys Former Prison, Hoping to Transform It Into a Pot Factory |Abby Haglage |July 31, 2014 |DAILY BEAST 

You can also work up an appetite with a beach volleyball tournament or rock out to your favorite local band. America’s Best Summer Food Festivals |Lonely Planet |July 5, 2014 |DAILY BEAST 

All they wanted was the chance to cheer on the Iranian volleyball team in its game against Brazil. Iran Won’t Let Women Watch The World Cup |IranWire |June 21, 2014 |DAILY BEAST 

Oh, OK, her first post-natal engagement, at a volleyball club, was dominated by photos of her top riding up her chest. Kate Middleton's History of Flesh-Flashing Wardrobe Malfunctions |Tom Sykes |May 29, 2014 |DAILY BEAST 

By contrast, the era of American POWs offers photos of Christmas parties and volleyball in the courtyard. Going Back to Vietnam Is Sometimes Amusing, Often Fraught, and Always Surreal |Jeff Greenfield |March 9, 2014 |DAILY BEAST 

He was organizing ping-pong games and indoor baseball for the boys and girls and even volleyball for some grown men who had come. Across the Fruited Plain |Florence Crannell Means 

The injured knee was already so swollen that it was visible, like a volleyball, beneath his baggy trousers. Someone Comes to Town, Someone Leaves Town |Cory Doctorow 

We fixed a place between the barracks to play volleyball and played occasionally. The Biography of a Rabbit |Roy Benson 

Other recreation at the site included volleyball and hunting. Trinity [Atomic test] Site |White Sands Missile Range Public Affairs Office