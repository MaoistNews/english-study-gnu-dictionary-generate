Whoever sent more soldiers to a given castle conquered that castle and won its victory points. Can You Reach The Summit First? |Zach Wissner-Gross |September 11, 2020 |FiveThirtyEight 

The top strategies clustered soldiers into a small number of castles worth exactly 28 points. Can You Reach The Summit First? |Zach Wissner-Gross |September 11, 2020 |FiveThirtyEight 

Photographer Jose Villa regularly flies around the world to shoot couples’ wedding celebrations, whether it’s a castle in Tuscany, a ballroom in Malaysia, or a private estate in Napa Valley. Couples spend thousands on a wedding photographer for that perfect shot |Rachel King |September 6, 2020 |Fortune 

Each castle has its own strategic value for a would-be conqueror. The Fifth Battle For Riddler Nation |Zach Wissner-Gross |September 4, 2020 |FiveThirtyEight 

Specifically, the castles are worth 1, 2, 3, …, 12, and 13 victory points. The Fifth Battle For Riddler Nation |Zach Wissner-Gross |September 4, 2020 |FiveThirtyEight 

I meet Otis J. the night he arrives at “The Castle,” a West Harlem halfway house for newly-released convicts. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

Crain posted a cash bond of $102.50 apiece shortly before 1:30 P.M., and they returned to the Castle Hotel. How Martin Luther King Jr. Influenced Sam Cooke’s ‘A Change Is Gonna Come’ |Peter Guralnick |December 28, 2014 |DAILY BEAST 

Denton, who speaks in the clipped cadence of the Oxford-educated Brit he is, has built quite a castle. The Gospel According to Nick Denton—What Next For The Gawker Founder? |Lloyd Grove |December 14, 2014 |DAILY BEAST 

Terry Castle has this great book called The Professor, which came out after I was in grad school. Meghan Daum On Tackling The Unspeakable Parts Of Life |David Yaffe |December 6, 2014 |DAILY BEAST 

Well, the dwarfs took pity on him and gave him the coffin, and the prince had it carried to his castle. In New Brothers Grimm 'Snow White', The Prince Doesn't Save Her |The Brothers Grimm |November 30, 2014 |DAILY BEAST 

Walls End Castle, when the party broke up, returned to its normal state. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

His presence, also, always graced Walls End Castle at the regulation periods. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

His lordship during the earlier part of his reign never came near Walls End Castle. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Nothing doubtful or "reputed" ever arrived in the huge packing-cases consigned to Walls End Castle. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Thank you for nothing, Reginald; however, I shall certainly take a dip in the lucky-bag at the Castle. The Pit Town Coronet, Volume I (of 3) |Charles James Wills