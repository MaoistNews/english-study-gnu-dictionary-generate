Sara Duncan, spokesperson for the Oregon Forest & Industries Council, the state’s largest timber lobbying group, said officials were still evaluating bills that propose the elimination of OFRI and had no comment on them. “We Have Counties in Deep Trouble”: Oregon Lawmakers Seek to Reverse Timber Tax Cuts That Cost Communities Billions |by Rob Davis, The Oregonian/OregonLive, and Tony Schick, Oregon Public Broadcasting |February 5, 2021 |ProPublica 

I remember year after year of trying to understand the difference between a layoff and the elimination of an unfilled position or of a filled position but the person doesn’t lose their job. Politics Report: The Fragile City |Scott Lewis |January 16, 2021 |Voice of San Diego 

By process of elimination, that left axions as kind of the last man standing. A Prodigy Who Cracked Open the Cosmos |Claudia Dreifus |January 12, 2021 |Quanta Magazine 

Currently, the legislative filibuster has remained in place, despite elimination by both Democratic and Republican leaders of the filibuster for judicial nominees. Dems win in Georgia, oust Republican who introduced anti-trans bill |Chris Johnson |January 6, 2021 |Washington Blade 

Despite the elimination of preseason games this season, Laviolette said he isn’t too concerned about getting a fair evaluation of his No. The Capitals miss Braden Holtby but are ready for Ilya Samsonov to step forward |Samantha Pell |January 5, 2021 |Washington Post 

Part of that was due to the elimination of the Mysterious Man. Rob Marshall Defends ‘Into the Woods’ |Kevin Fallon |December 9, 2014 |DAILY BEAST 

Sen. Rand Paul has called for the “attrition if not an outright elimination of the IRS.” Huckabee: ‘A Tax Is Punishment’ |Lloyd Green |September 29, 2014 |DAILY BEAST 

The only thing missing from this bill of particulars was elimination of the bourgeoisie. Communism's Victims Deserve a Museum |James Kirchick |August 25, 2014 |DAILY BEAST 

First, it turned its hand to the elimination of organized resistance within Germany itself. The Gestapo Still Sets the Bar for Evil |James A. Warren |July 13, 2014 |DAILY BEAST 

There is sentiment against corporate welfare, and this is a ‘doable’ target [for elimination]. Conservative Support for the Export-Import Bank Turned to Hate |Tim Mak |June 23, 2014 |DAILY BEAST 

With the elimination of Frick, responsibility for Homestead conditions would rest with Carnegie. Prison Memoirs of an Anarchist |Alexander Berkman 

This affords the maximum of riding comfort by the elimination of all jar and jolt occasioned by an uneven roadway. The Wonder Book of Knowledge |Various 

As the sex organs are also the channels for the elimination of waste, exaggerated modesty often hinders discussion. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

This completeness of expression may even go to the elimination of what is ordinarily looked upon as "finish." The Painter in Oil |Daniel Burleigh Parkhurst 

In the section marked “I. B,” Burke begins the real argument by the method of elimination. English: Composition and Literature |W. F. (William Franklin) Webster