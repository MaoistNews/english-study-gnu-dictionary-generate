People starting on a health journey often turn to supplements, probably because we like buying things more than we like doing hard physical work. BCAA supplements can enhance your workout, but should you take them? |Amy Schellenbaum |September 10, 2020 |Popular-Science 

Vegans who want to build significant muscle, however, may benefit from a leucine supplement, because plant-based proteins are generally lower in this amino acid than their animal-based counterparts. BCAA supplements can enhance your workout, but should you take them? |Amy Schellenbaum |September 10, 2020 |Popular-Science 

Digital contact tracing is a supplement to traditional manual contact tracing. Apple and Google expand digital coronavirus contact-tracing tools to help speed adoption |dzanemorris |September 1, 2020 |Fortune 

Today, scientists around the world are using cutting-edge technologies, from subcutaneous biosensors to specialized food supplements, in an effort to improve safety and efficiency within the $385 billion global cattle meat industry. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

Others noted that while the extra payment made sense as a short-term stimulus measure, economists might approach the long-term consequences of such a generous supplement differently. Economists Think Congress Should Keep Paying Unemployed Workers $600 A Week — Or Even More |Neil Paine (neil.paine@fivethirtyeight.com) |July 21, 2020 |FiveThirtyEight 

The flowers and leaves of this herb are used to make medications and the supplement is popularly used for depression. Fish Oil, Turmeric, and Ginseng, Oh My! Are ‘Brain Foods’ B.S.? |Dr. Anand Veeravagu, MD |October 10, 2014 |DAILY BEAST 

This dietary supplement originates in China and has been reported to enhance cognitive ability in healthy individuals. Fish Oil, Turmeric, and Ginseng, Oh My! Are ‘Brain Foods’ B.S.? |Dr. Anand Veeravagu, MD |October 10, 2014 |DAILY BEAST 

Williams was trying to promote Anatabloc, a dietary supplement developed by his company. The Five Weirdest Revelations From the McDonnell Trial |Ben Jacobs |August 22, 2014 |DAILY BEAST 

I was grateful I could supplement my voracious reading with other media, as it kept me feeling current. Prisoners Get Cultural Fix with 8-Tracks and Bootleg Cassettes |Daniel Genis |August 18, 2014 |DAILY BEAST 

Annunziato and Akerman are in agreement that CrowdMed is best deployed as a supplement, not a substitute. Strangers Diagnose Your Illness and Get Cash in Return |Kevin Zawacki |August 15, 2014 |DAILY BEAST 

He prepared a glossary of provincial and archological words, intended for a supplement to Johnson's Dictionary. The Every Day Book of History and Chronology |Joel Munsell 

It came to be used as a supplement to the law to indicate ways of doing things unknown to the law, which ought to be done. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Our valley does not number among its men many who can supplement their sentimental attentions with gifts of books. The Soldier of the Valley |Nelson Lloyd 

A few further remarks on this and other important topics will well supplement what we have said. Violins and Violin Makers |Joseph Pearce 

He also wrote a supplement of three volumes, entitled "Lee's Lieutenants," which was exceptionally well received. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey