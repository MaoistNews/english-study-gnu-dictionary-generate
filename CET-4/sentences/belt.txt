He followed through, seizing the belt and casting Woodley into his current descent. As Kamaru Usman edges toward UFC greatness, his former teammate wants to stop him |Glynn A. Hill |February 12, 2021 |Washington Post 

At the Street Church program, he would quietly give out sandwiches and once noticed a man who would “wear out his belts and shoes a lot,” Mooneyham said. Frank Anderson, 87, cared passionately about feeding the homeless in D.C. |Dana Hedgpeth |February 5, 2021 |Washington Post 

Avdija, who missed more time than Hachimura and has just three games under his belt after getting out of the league protocols, scored 13 points and shot well in each of his past two games, pulling in seven rebounds Tuesday and four Wednesday. Rui Hachimura and Deni Avdija are regaining their footing after missing so much time |Ava Wallace |February 4, 2021 |Washington Post 

Jim is one of the best pilots I know—calm and extremely skilled, with thousands of hours of mountain flying under his belt. After the Crash, They Said I Was Fine. I Wasn't. |Erin Tierney |February 4, 2021 |Outside Online 

Retention is going to be a big focus of ours this year now that we have a solid business under our belts. Media Briefing: Media companies’ DE&I follow-throughs fall short |Tim Peterson |February 4, 2021 |Digiday 

They want to change bad behaviors—tobacco, alcohol, using a seat belt, anything. Can the U.S. Government Go Moneyball? |Peter Orszag, Jim Nussle |December 23, 2014 |DAILY BEAST 

Now they are a notch on a belt, and the savior can feel good about themselves. To Catch a Sex Worker: A&E’s Awful, Exploitative Ambush Show |Samantha Allen |December 19, 2014 |DAILY BEAST 

Det. 2: No, not your belt . . . . Remember being out in the sunroom, the room that sits out to the back of the house? How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

Not long after, a 10-year-old girl wearing a suicide belt was arrested. The New Face of Boko Haram’s Terror: Teen Girls |Nina Strochlic |December 13, 2014 |DAILY BEAST 

He would laboriously make his way from desk to loo, belt down a few, then return. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

His boyish suspenders had been put away in favor of a belt, which was tight-drawn about his slim waist. The Bondboy |George W. (George Washington) Ogden 

He reached down inside my shirt, with a none too gentle hand, and relieved me of the belt that held the money. Raw Gold |Bertrand W. Sinclair 

Just smiled, a sardonic sort of grimace, and unbuckled his belt and handed it over without a word. Raw Gold |Bertrand W. Sinclair 

He put his hand to his belt, screwed up his mug, and said he felt plumb et up inside. Alec Lloyd, Cowpuncher |Eleanor Gates 

A millionaire might offer more for a life belt as a souvenir than a drowning man could pay for it to save his life. The Unsolved Riddle of Social Justice |Stephen Leacock