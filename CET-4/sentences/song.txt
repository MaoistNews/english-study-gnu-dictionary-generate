Nelson told the Sun Sentinel he will continue to use the Twisted Sister song, despite Snider’s criticism. Twisted Sister’s Dee Snider does not approve of anti-maskers using ‘We’re Not Gonna Take It’ |radmarya |September 17, 2020 |Fortune 

The ’90s cover band behind her struck up a song and a ribbon was cut with five pairs of oversize scissors. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 

For example, someone could ask their Internet-connected set-top box to play a song from their web-connected speaker. Amazon’s A.I. voice project gets help from Facebook, Dolby, and Garmin |jonathanvanian2015 |September 9, 2020 |Fortune 

The most popular songs appear to be getting a little less popular. Is Spotify killing the top 40? |Dan Kopf |September 5, 2020 |Quartz 

All they want to do is play guitar, pass a history test, write a song, win a board game, make sure everyone is okay, and still be friends at the end of their lives. The essential kindness of Bill and Ted |Alissa Wilkinson |August 28, 2020 |Vox 

So here I am in my requisite Lululemon pants, grunting along to an old hip-hop song at a most ungodly hour. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

I still do find it a tremendously useful device to invent a character and have the character sing the song. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

In 2012, as a 10th grader, Lean says he recorded his first legitimate song, “Hurt.” The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

So we picked out the song (“Rhiannon,” click here for video), and Deer Tick learned it. Deer Tick's John McCauley on Ten Years in Rock and Roll |James Joiner |January 2, 2015 |DAILY BEAST 

So this is Christmas, as the song goes, and what have we done? No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

At this moment the tinkling of a mule's bells, mingled with the song of the muleteer, came on the air. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Gushing waters thrilled the ears with the sweetness of an old familiar song. The Giant of the North |R.M. Ballantyne 

The song stopped abruptly, the music died away, there was an interval of silence no one broke. The Wave |Algernon Blackwood 

"He 's getting well," thought Black Sheep, who knew the song through all its seventeen verses. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

As Edna waited for her husband she sang low a little song that Robert had sung as they crossed the bay. The Awakening and Selected Short Stories |Kate Chopin