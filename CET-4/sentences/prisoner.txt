I realize in the public’s mind … the reaction might be why would you give a vaccination to a prisoner when people outside need them…. U.S. Marshals Service’s lax covid-19 oversight of some inmates reflects a larger problem |Joe Davidson |February 12, 2021 |Washington Post 

She was part of the expert team that presented covid-19 vaccine policy recommendations to the Israeli government, and the group was among those petitioning for prisoners to be vaccinated. “Everyone is impressed by Israeli vaccination, but I don’t think we’re a success story” |Lindsay Muscato |January 22, 2021 |MIT Technology Review 

The American Correctional Association recommends each prisoner have 25 square feet of “unencumbered” area to themselves—in most cases, not enough to keep a safe social distance. On surviving—and leaving—prison during a pandemic |Sarah Scoles |January 21, 2021 |Popular-Science 

Though Weber has largely focused her legislative efforts on education and criminal justice reform, she has targeted a specific issue related to voting rights – and that’s expanding voting rights for the incarcerated and former prisoners. Sacramento Report: New Report Shows More of the Same Disparate Police Outcomes |Sara Libby |January 8, 2021 |Voice of San Diego 

They raised a total of $32,000 over about three years — a remarkable feat considering prisoners in California earn a base wage of 8 cents an hour for many of their daily jobs, such as mopping the floors. A high school student needed help with tuition, so an unlikely group stepped up: Prison inmates |Kellie B. Gormly |January 1, 2021 |Washington Post 

And now, similarly, former Arkansas governor Mike Huckabee: "Bend over and take it like a prisoner!" Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

Clickbait title notwithstanding, Bend Over and Take It Like a Prisoner! Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

However, legal issues are only one of the things standing between an ex-prisoner and a job. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

I was put in a solitary confinement completely cut off from the outside world without even enjoying basic prisoner rights. A Daughter’s Plea: Free My Father from Prison in Iran |Mitra Pourshajari, Movements.Org, Advancing Human Rights |December 26, 2014 |DAILY BEAST 

One prisoner, who was left naked and shackled to a cold floor, died of suspected hypothermia. What the Torture Report Kept Hidden |Shane Harris, Tim Mak |December 10, 2014 |DAILY BEAST 

They also seized the lake gunboats, took an entire Spanish garrison prisoner, and captured a large quantity of stores. The Philippine Islands |John Foreman 

During his mild régime the insurrection increased rapidly, and in one encounter he himself was very near falling a prisoner. The Philippine Islands |John Foreman 

A white woman, Mrs. Henry Jacobi, who had been taken prisoner early in the month, crossed the plain holding a white flag. The Red Year |Louis Tracy 

The manner of his prisoner, sufficiently mollified the officer; and he made a sign to his attendants to withdraw. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Don Diego beckoned two guards, who immediately drew near their prisoner. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter