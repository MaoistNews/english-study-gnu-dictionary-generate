Coursera has raised millions for simply replicating the old-fashioned experience of a teacher lecturing at the front of a classroom. Why hasn’t digital learning lived up to its promise? |Walter Thompson |September 17, 2020 |TechCrunch 

Students with special needs and some elementary students are returning to classrooms as early as this month across North County. North County Report: Schools Are Reopening for Students Most in Need |Kayla Jimenez |September 16, 2020 |Voice of San Diego 

This back-to-school season, consider the girls who may never go back to a classroom. Malala Yousafzai tells the business community: Education is the best way to guard against future crises |Michal Lev-Ram, writer |September 15, 2020 |Fortune 

As schools worldwide have sent students home to protect against the spread of Covid-19, the impacts of the time spent out of the classroom have begun to come into focus. Closing schools for Covid-19 hurts students’ financial future |Alexandra Ossola |September 10, 2020 |Quartz 

Additionally, as schools prepare for students to return, Arthrex is donating thousands of face shields to school systems across our region to help protect teachers and school staff as they welcome students back to the classroom. How the Best Workplaces in Manufacturing have risen to the COVID-19 challenge |lbelanger225 |September 10, 2020 |Fortune 

This is a provocative subject that is ready-made for the classroom. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

Why had teachers at Jamaica High School resorted to overusing 911 for common classroom disruptions? Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

After years at the head of a parochial school classroom, he could no longer distinguish one blond Irish Catholic kid from another. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

The killers went from classroom to classroom mowing down teachers and students alike. Taliban: We Slaughtered 100+ Kids Because Their Parents Helped America |Sami Yousafzai |December 16, 2014 |DAILY BEAST 

A classroom experiment seeks to demonstrate what it looks like. What Is Privilege? |The Daily Beast Video |December 11, 2014 |DAILY BEAST 

As the classroom they had selected was in a remote part of the building, there was little immediate chance of detection. Eric, or Little by Little |Frederic W. Farrar 

Next evening when preparation began, Pietrie and Graham got everything ready for the carouse in their classroom. Eric, or Little by Little |Frederic W. Farrar 

So I walked back into the empty senior classroom, there to remain until I should have got the worst of it over. In Accordance with the Evidence |Oliver Onions 

I look with veneration upon those of our sisterhood who have grown old in the classroom. A Hoosier Chronicle |Meredith Nicholson 

We all go into this classroom, and we come out two hours later for recess. The Status Civilization |Robert Sheckley