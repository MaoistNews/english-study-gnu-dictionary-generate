As made famous on the big screen in High Fidelity, mixtapes were the perfect way to woo your crush, or just impress your friends with how many obscure bands you’d heard of. The best way to share playlists on every major platform |David Nield |October 8, 2020 |Popular-Science 

Facebook is, in particular, notorious for its attempts to copy and crush rivals, like Snapchat and now TikTok. Big Tech’s time is up |rhhackettfortune |October 7, 2020 |Fortune 

How to make a fortune tellerThis craft is one of the more difficult on this list, but we’ve placed it high up because of how common a tool it was for trying to find out if your crush liked you back, or if you were going to pass the next test. Five classic paper toys you can make when you’re bored (whether you’re in school or not) |John Kennedy |October 7, 2020 |Popular-Science 

The benefit, according to Rayl, is by everyone taking the same day off, there’s no pent-up crush of workload when people return—because no one else was assigning work while they were out either. ‘Integrators’ and ‘separators’: How managers are helping the two types of remote workers survive the pandemic |Jen Wieczner |September 16, 2020 |Fortune 

If you’re a little boy who has a crush on fox Robin Hood, maybe that leads you to accept yourself sooner. Toward a queer Disney canon |Emily VanDerWerff |September 4, 2020 |Vox 

Why would “they” want to crush him just for attempting to buy something twenty years ago? Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

His high school prom was around the corner, and he had been hanging out with a boy that he had a crush on. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

Tank Battle Jeep Guard Crush -- some editorial changes and the removal of all blood when the guards are crushed by the tank. Sony Emails Show How the Studio Plans to Censor Kim Jong Un Assassination Comedy ‘The Interview’ |William Boot |December 15, 2014 |DAILY BEAST 

We have a specific idea to attach to THE INTERVIEW that will crush. Exclusive: Sony Emails Reveal Destiny’s Child and Kanye West Movies, and Spidey Cameo in Capt. 3 |William Boot |December 14, 2014 |DAILY BEAST 

The main article called Reflections on the Final Crusade outlines in prophetic terms just how ISIS will crush Christianity. The Pope’s Risky Trip |Barbie Latza Nadeau |November 21, 2014 |DAILY BEAST 

Her feet crush creeping things: there is a busy ant or blazoned beetle, with its back broken, writhing in the dust, unseen. God and my Neighbour |Robert Blatchford 

The Emperor found he had no longer any reason to fear him, and for the moment determined to crush him completely. Napoleon's Marshals |R. P. Dunn-Pattison 

There, in the crush, he unceremoniously lost her, and sped like a maniac to the entrance gates. The Joyous Adventures of Aristide Pujol |William J. Locke 

You have heard of my misfortunes, and you take a mean advantage of your knowledge to crush and kill me. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The Emperor staked his last card, and ordered the Guard to make one last effort to crush the English infantry. Napoleon's Marshals |R. P. Dunn-Pattison