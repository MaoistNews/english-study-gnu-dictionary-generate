Unlike molecular and antigen diagnostic tests, the FDA warned that blood-based antibody tests were not intended to diagnose an active infection but rather reveal if someone previously had the virus. The COVID-19 Charmer: How a Self-Described Felon Convinced Elected Officials to Try to Help Him Profit From the Pandemic |by Vianna Davila, Jeremy Schwartz and Lexi Churchill |September 25, 2020 |ProPublica 

To ensure you’re getting those steps in and staying active this fall, there are a few essential items you might need to elevate your home or gym workout routine. 5 Fitness Products For Your Workout Routine This Fall |Jasmine Grant |September 25, 2020 |Essence.com 

Opposition groups with links to politically active churches — who were blamed for the recent virus resurgence — are planning a series of mass protests in the capital over the coming weeks. South Korea’s Religious Right Torpedoes the Fight Against COVID-19 |Fiona Zublin |September 24, 2020 |Ozy 

An initial handful of fine-dining partners has climbed to over 100 active and pledged members. This restaurant duo want a zero-carbon food system. Can it happen? |Bobbie Johnson |September 24, 2020 |MIT Technology Review 

In 2018, Damer set up shop at an active geothermal area in New Zealand, named along the usual theme — Hells Gate — to test that hypothesis. Life on Earth may have begun in hostile hot springs |Jack J. Lee |September 24, 2020 |Science News 

“The United States had gone to war declaring it must destroy an active weapons of mass destruction program,” the Times reported. Political Memes That Absolutely Must Die in 2015 |Asawin Suebsaeng |January 1, 2015 |DAILY BEAST 

Almost immediately, another group active at the protests called the Justice League snitches. De Blasio and the New York City Protesters Have No Blood on Their Hands |Jacob Siegel |December 22, 2014 |DAILY BEAST 

Along the river, crumbling remnants of an active trading hub are overtaken by nature. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

But, under the hawkish eye of the media and through a heavily active social media presence, she carried on as usual. Nicki Minaj Bares Her Own Vulnerability on ‘The Pinkprint’ |Rawiya Kameir |December 16, 2014 |DAILY BEAST 

Female members have been involved in the carnage for the past two years, but never in such an active role. The New Face of Boko Haram’s Terror: Teen Girls |Nina Strochlic |December 13, 2014 |DAILY BEAST 

We must have motif first, then technique to adapt and adjust expression and to develop facility in the active agents. Expressive Voice Culture |Jessie Eldridge Southwick 

With the announcement of the thirty-six directors, it was possible to proceed to the active opening of the institutions. Readings in Money and Banking |Chester Arthur Phillips 

He became one of the assistants of Mr. Wesley, and was active in the service of the church. The Every Day Book of History and Chronology |Joel Munsell 

The opposite of these two methods of rote learning is my method, which injects an active process between each pair of words. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

He continued active till his 35th year, when he began to decline, and died of water in the chest. The Every Day Book of History and Chronology |Joel Munsell