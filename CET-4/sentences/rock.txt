In an earlier experiment, I showed that sugar seed crystals are important for making rock candy. Build ice towers with bottled water and ice |Bethany Brookshire |September 16, 2020 |Science News For Students 

She was confident, she was outspoken, she was kind of the queen of the underground New York rock music scene. Inside ‘I Am Woman’: A new biopic tells the story of Helen Reddy and her famous song |radmarya |September 10, 2020 |Fortune 

Those wingbeats also caused bodies of the tiny, coiled shell species and the midsize, long shell snails to rock back and forth as they swam. Sea butterflies’ shells determine how the snails swim |Maria Temming |September 8, 2020 |Science News 

They are very similar to the rocks that may have formed our planet billions of years ago. Scientists Say: Asteroid, meteor and meteorite |Bethany Brookshire |September 7, 2020 |Science News For Students 

This 14-inch rocking pizza cutter will give you restaurant quality cut slices. Pizza cutters that will get you the slice of your dreams |PopSci Commerce Team |September 2, 2020 |Popular-Science 

The atmosphere on campuses has gotten repressive enough that comedian Chris Rock no longer plays colleges. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

And, as the enigmatic front man to an avant garde indie rock group, he is droll, perceptive, and splendidly weird. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 

My ball bounced back and the rock rolled just a little bit forward. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

I think 2014 was my big rock and roll year, and 2015 is gonna be a really good year to hang around the house. Deer Tick's John McCauley on Ten Years in Rock and Roll |James Joiner |January 2, 2015 |DAILY BEAST 

Yes, it was a fairly disappointing year in music—one devoid of Goth teen prodigies, Yeezy, and galvanizing rock anthems. The 14 Best Songs of 2014: Bobby Shmurda, Future Islands, Drake, and More |Marlow Stern |December 31, 2014 |DAILY BEAST 

The offspring of the ungodly shall not bring forth many branches, and make a noise as unclean roots upon the top of a rock. The Bible, Douay-Rheims Version |Various 

There lay Bob Rock, covered with blood, and apparently insensible. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

When she heard it there came before her imagination the figure of a man standing beside a desolate rock on the seashore. The Awakening and Selected Short Stories |Kate Chopin 

It was a pretty house, stood a little apart from the forge, and was called Rock Villa. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Between each group of figures the face of the rock was scored with mysterious signs and rudely limned weapons of war and chase. Raw Gold |Bertrand W. Sinclair