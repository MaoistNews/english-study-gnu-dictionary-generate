In discussing his two leading characters, he has said, “I didn’t want us to feel sorry for them.” Lesbian love story becomes thriller in ‘Two of Us’ |John Paul King |February 4, 2021 |Washington Blade 

I’m even more sorry to report that Val Curtis, the British hygiene scholar we interviewed, died in October. The Downside of Disgust (Ep. 448) |Stephen J. Dubner |January 21, 2021 |Freakonomics 

We are truly sorry, but unfortunately, the vaccine supply is not under our control. How Operation Warp Speed Created Vaccination Chaos |by Caroline Chen, Isaac Arnsdorf and Ryan Gabrielson |January 19, 2021 |ProPublica 

To account for travel restrictions between the United States and Canada, the North Division — sorry, the Scotia North Division — is composed of the league’s seven Canadian teams. What you may have missed in the Caps’ offseason, and what experts predict for this season |Scott Allen |January 14, 2021 |Washington Post 

To attend and then leave, it is, “I’m sorry, I have to go now.” Miss Manners: What to tell artist friends, besides ‘That’s pretty!’ |Judith Martin, Nicholas Martin, Jacobina Martin |January 11, 2021 |Washington Post 

Its biggest asset, of course, is the steely Atwell, who never asks you to feel sorry for Carter despite all the sexism around her. Marvel’s ‘Agent Carter’ Stomps on the Patriarchy |Melissa Leon |January 7, 2015 |DAILY BEAST 

So she lies to the knight, telling him Madalena is sorry and wants him back. ‘Galavant’: A Drunken, Horny Musical Fairy Tale |Melissa Leon |January 5, 2015 |DAILY BEAST 

I am so sorry that your parents did this to you, developing their own discriminatory take on your existence. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

“Sorry I can't give you more, but at this time our office has no comment,” his spokesman, Sergio Gor, said. Presidential Hopeful Rand Paul Backs Obama on Cuba Deal |Olivia Nuzzi |December 18, 2014 |DAILY BEAST 

“I feel sorry for what she did to Russ and his daughter,” Gill said. Open-Carry Militia Mom ‘Murders’ Family |Brandy Zadrozny |December 16, 2014 |DAILY BEAST 

Bernard was uncomfortable enough not to care to be mocked; but he felt even more sorry that Gordon should be. Confidence |Henry James 

"I will," gruffly replied the man, with a look which showed that he was sorry to be forced to choose the second alternative. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

He felt very sorry for the Temecula people, the sheriff did; but he had to obey the law himself. Ramona |Helen Hunt Jackson 

"I am sorry," she replied instead, not saying a word about the poor little toes which the pretty pink lady had crushed. Rosemary in Search of a Father |C. N. Williamson 

Meanwhile, as you may well believe, he began to feel very sorry that he had said anything about the verses. Davy and The Goblin |Charles E. Carryl