Even as IT firms bring back between 10% and 30% of their workforce, it might be a while before their campuses go back to full capacity with buzzing cafeterias and noisy bays. What Indian IT leaders miss the most while working from home |Ananya Bhattacharya |July 17, 2020 |Quartz 

When performing medical exams, astronauts won’t have the starship Enterprise’s sick bay at their disposal. What will astronauts need to survive the dangerous journey to Mars? |Maria Temming |July 15, 2020 |Science News 

These cells normally keep clots at bay so that blood can flow smoothly. Preventing dangerous blood clots from COVID-19 is proving tricky |Aimee Cunningham |June 23, 2020 |Science News 

The mucus marvels rise out of the heads of four species of spineless, roughly tadpole-shaped giant larvaceans living in the twilight depths of the bay. Larvaceans’ underwater ‘snot palaces’ boast elaborate plumbing |Susan Milius |June 15, 2020 |Science News 

Kelp forests were on average 20 times larger in areas where sea otters have lived for decades on Vancouver Island, compared with bays where the otters were absent, Watson and her colleagues found. Bringing sea otters back to the Pacific coast pays off, but not for everyone |Jonathan Lambert |June 11, 2020 |Science News 

As part of the MassEquality coalition, Marc Solomon, a former Senate aide, was working to get Bay State legislators to vote no. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

The Tampa Bay Times got their hands on a full copy of the letter the retired judge sent to Winston. Jameis Winston Cleared of Rape Like Every Other College Sports Star |Robert Silverman |December 22, 2014 |DAILY BEAST 

Rising up from scooping bay, the steep topography—hemmed by hills of evergreens—promises panoramas at practically every turn. Next Stop, Quito: Our Top Cities for 2015 |Brandon Presser |December 19, 2014 |DAILY BEAST 

Perhaps the guards at the Guantanamo Bay detention facilities will finally be allowed to smoke cubans, too. Cigar Dealers Light Up Over Cuba News |Asawin Suebsaeng |December 17, 2014 |DAILY BEAST 

The proceedings expected this week in Guantanamo Bay had been canceled. 9/11 Mastermind Is Afraid of the Ladies |Tim Mak |December 16, 2014 |DAILY BEAST 

First a shower of shells dropping all along the lower ridges and out over the surface of the Bay. Gallipoli Diary, Volume I |Ian Hamilton 

These have canted bay windows below them, and their pediments are surmounted by figures representing Mercury and Athæne. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Three men were sentenced to grow potatoes at Botany Bay the rest of their lives. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

There are two principal bays of vast size, one called the gulf of St. Lawrence, the other French bay. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

But this port (to obviate misunderstanding) is not on the Ocean lying eastward, but on that gulf which I have called French bay. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various