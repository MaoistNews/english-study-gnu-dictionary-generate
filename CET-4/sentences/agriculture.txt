Many workers, including those in healthcare, manufacturing, and agriculture, don’t enjoy the ability to work from home. I’m a physician and a CEO. Why I won’t bring my employees back to the office before Labor Day 2021 |matthewheimer |August 26, 2020 |Fortune 

In less than three decades, most of Southeast Asia’s peatlands have been deforested, drained, and dried for agriculture or other purposes, leaving them vulnerable to wildfires that spew pollution and greenhouse gases. Peat’s Sake |Stephanie Arnett |August 19, 2020 |MIT Technology Review 

This is a problem because machine learning holds great promise for advancing health, agriculture, scientific discovery, and more. Too many AI researchers think real-world problems are not relevant |Amy Nordrum |August 18, 2020 |MIT Technology Review 

Have worked on agriculture and food policy, poverty reduction, economic development for well over 50 years now. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

With Africa much of the focus is on supply chains around health and agriculture including food security—30% to 50% of the food produced in Sub Saharan Africa is estimated to be lost along the supply chain. Africa is tackling its supply chain deficit with a US-backed research center in Ghana |Yinka Adegoke |July 27, 2020 |Quartz 

Fracking, in this regard, is no different from gypsum mining, or some kinds of industrial agriculture. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

The previous guess had been 10,000 years, around the time agriculture emerged. Ebola's Roots Are 50 Times Older Than Mankind. And That Could Be the Key to Stopping It. |Michael Daly |October 20, 2014 |DAILY BEAST 

The film examines labor in the U.S. agriculture sector and the “immoral practices” that affect thousands of farmworkers. This Is How Eva Longoria Is Trying to Win the Midterms |Asawin Suebsaeng |October 1, 2014 |DAILY BEAST 

Last week, the U.S. Department of Agriculture tallied up the costs of raising a child born in 2013. Free Market Failure: Raising a Kid Is a Rigged Game in the USA |Monica Potts |August 25, 2014 |DAILY BEAST 

Agriculture supported larger populations and gave them more goods to fight over. War! What Is It Good For? A Lot |Nick Romeo |August 13, 2014 |DAILY BEAST 

It laid its hold upon agriculture, sowing and reaping the grain and transporting it to the ends of the earth. The Unsolved Riddle of Social Justice |Stephen Leacock 

English Agriculture has a thorough and cleanly aspect which I have rarely observed elsewhere. Glances at Europe |Horace Greeley 

Only in the pursuit of agriculture can the black man not complain that he is discriminated against on account of his color. The Homesteader |Oscar Micheaux 

They have developed fishing and agriculture, and have brought the tourist into districts little visited before. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The bones used in agriculture are chiefly those of cattle, but sheep and horse bones are also employed. Elements of Agricultural Chemistry |Thomas Anderson