We did a movie down in Durango — Great Scout and Cat House Thursday. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Alastair Sim had jowls like melting candle wax, a snarl like a cornered cat and eyes cold with contempt. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

We coo over how cute our cat is and minimize the drudgery of cleaning the litter box. Why Didn’t Camille Dump Bill Cosby? |Amanda Marcotte |December 17, 2014 |DAILY BEAST 

So Western governments are caught in a cat-and-mouse game and at times it is unclear who is the cat and who the mouse. ISIS Has a Message. Do We? |Jamie Dettmer |December 8, 2014 |DAILY BEAST 

The sort of thing where someone write “I love my cat more than my kids” or something like that. Meghan Daum On Tackling The Unspeakable Parts Of Life |David Yaffe |December 6, 2014 |DAILY BEAST 

A lateen sail was visible in the direction of Cat Island, and others to the south seemed almost motionless in the far distance. The Awakening and Selected Short Stories |Kate Chopin 

And if he was worried about Farmer Green's cat, why didn't he dig a hole for himself at once, and get out of harm's way? The Tale of Grandfather Mole |Arthur Scott Bailey 

The cat had been about to spring at Grandfather Mole again when Mr. Crow spoke to her. The Tale of Grandfather Mole |Arthur Scott Bailey 

At that Farmer Green's cat began to run up and down between the rows of vegetables. The Tale of Grandfather Mole |Arthur Scott Bailey 

Farmer Green's cat had never liked Mr. Crow, for no particular reason. The Tale of Grandfather Mole |Arthur Scott Bailey