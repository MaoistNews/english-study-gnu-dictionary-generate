The president of the Senate, aka Vice President Mike Pence, breaks the tie. Sunday Magazine: The Deciders |Daniel Malloy |September 13, 2020 |Ozy 

In the event of a tie, both warlords were granted half a victory. Can You Reach The Summit First? |Zach Wissner-Gross |September 11, 2020 |FiveThirtyEight 

Such strengthening of ties between community organizations and schools could present a chance for educators to make learning resonate with students of all backgrounds. Creative school plans could counter inequities exposed by COVID-19 |Sujata Gupta |September 8, 2020 |Science News 

The relationship between China and Africa has grown substantially in recent years, to include not only deeper financial ties but also geopolitical ones. What China has to gain from Africa |Chika Dunga |September 1, 2020 |Quartz 

Supporters of the army, which maintains close ties with Beijing, have exploited dysfunction in Prime Minister Imran Khan’s government to push legislation that would give it greater control over China’s infrastructure program. Pakistan’s Military Plots Takeover of Marquee China Project |Charu Kasturi |August 31, 2020 |Ozy 

What could be more important, to make sure that side of things is right before we tie ourselves to someone forever? ‘Downton Abbey’ Review: A Fire, Some Sex, and Sad, Sad Edith |Kevin Fallon |January 5, 2015 |DAILY BEAST 

In fact, Clark fell back first from her blows, losing his cap, tie, and badge in the melee. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

However, an article designed to act as a tie-in to the piece has been published as planned in the BBC magazine Radio Times. Pulled Documentary Says William Felt ‘Used’ by Charles’ Push for Camilla |Tom Sykes |December 30, 2014 |DAILY BEAST 

Instead, the Republicans should tie their push for infrastructure to getting folks off the couch and back to work. Bush, Christie, Romney: Who’ll Be the GOP Class Warrior? |Lloyd Green |December 15, 2014 |DAILY BEAST 

Cheney is relying on some thin evidence to tie Hussein to al-Qaida. Fact-Checking the Sunday Shows: Dec. 14 |PunditFact.com |December 14, 2014 |DAILY BEAST 

First of all, wrap a portion of damp newspaper round the roots, and then tie up with dry paper. How to Know the Ferns |S. Leonard Bastin 

Bondad sua, seor, I'll be sworn there is not one fit to tie the latchet of your shoe in the whole army. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

But these hidden passions were before young farmer Wurzel, in his blue tie and white hat, had proposed to her. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

His foot caught; it is unknown in what,—in a twisted tie, or perhaps in a crevice of the cracking earth. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward 

It was the only thing I needed to snap my last tie with England and brace me for the struggle in America. Ancestors |Gertrude Atherton