They’ve done it without Elena Delle Donne, Natasha Cloud, LaToya Sanders or Tina Charles, and that’s thanks in large part to Myisha Hines-Allen’s remarkable campaign. It’s Win Or Go Home This Week In The WNBA Playoffs |Howard Megdal |September 15, 2020 |FiveThirtyEight 

He has a remarkable, well-documented power for taking issues that normally are apolitical, and splitting opinion on them along political lines. A third of Americans might refuse a Covid-19 vaccine. How screwed are we? |Brian Resnick |September 4, 2020 |Vox 

The fact that the dodecahedron has this hidden symmetry group is, I think, quite remarkable. Mathematicians Report New Discovery About the Dodecahedron |Erica Klarreich |August 31, 2020 |Quanta Magazine 

Which is to say, Walter Hutchins won a remarkable legal victory that was also remarkably limited. Hundreds of Thousands of Nursing Home Residents May Not Be Able to Vote in November Because of the Pandemic |by Ryan McCarthy and Jack Gillum |August 26, 2020 |ProPublica 

Inspired by this remarkable skill, researchers in South Korea have developed a robotic tongue that springs forth quickly to snatch up nearby items. This Week’s Awesome Tech Stories From Around the Web (Through August 15) |Singularity Hub Staff |August 15, 2020 |Singularity Hub 

The precision it took to craft such a cohesive, wholly compelling work over 12 years is nothing short of remarkable. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 

This initiative had the support of all 22 members of the Arab League, which in and of itself is remarkable. In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

This man has a remarkable ability, and remarkable independence because of it. A Blind Man Sees With Sound |The Daily Beast Video |December 22, 2014 |DAILY BEAST 

Over the years, this country has offered many of its immigrant groups a remarkable opportunity for reinvention. COEXIST’s Bonehead Bumper-Sticker Politics |Michael Schulson |December 21, 2014 |DAILY BEAST 

In the course of her remarkable travels Thecla baptizes herself by diving into a pool of “man-eating seals.” First Anglican Woman Bishop A Return to Christian Roots |Candida Moss |December 18, 2014 |DAILY BEAST 

Nothing remarkable occurred in our march through this country. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Never was a change more remarkable than that which had come upon Mrs. Collingwood. The Boarded-Up House |Augusta Huiell Seaman 

The remarkable thing was that all the hurrying people she met seemed also each of them to be on a secret and mystic errand. Hilda Lessways |Arnold Bennett 

Alford speaks of this as remarkable; but vision is the especial promise of Wisdom, therefore of Solomon, son of David. Solomon and Solomonic Literature |Moncure Daniel Conway 

But it was neither his talents as a diplomatist, nor his remarkable mind, nor his solid erudition, which made Nicot immortal. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.