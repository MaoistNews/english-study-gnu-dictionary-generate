Kick scooters are often more portable than a standard bicycle or electric scooter and definitely less expensive than a car or frequent rideshares. The best scooters for a smooth commute or cruise |PopSci Commerce Team |September 3, 2020 |Popular-Science 

Those are affordable prices for a quality bicycle and damn great for an e-bike but, in real economic terms, still serious money. Do You Want to Buy an E-Cargo Bike? Read This First. |Joe Lindsey |August 30, 2020 |Outside Online 

It’s like a race pitting a horse against a car against an airplane against a bicycle. This Week’s Awesome Tech Stories From Around the Web (Through August 22) |Singularity Hub Staff |August 22, 2020 |Singularity Hub 

The one that is that is the top end of the skill spectrum is bicycle racing. The Economics of Sports Gambling (Ep. 388 Rebroadcast) |Stephen J. Dubner |August 20, 2020 |Freakonomics 

E-bike sales have more than doubled since January, outpacing even huge sales growth in conventional bicycles. Bikes are hot but e-bikes are on fire |Michael J. Coren |July 23, 2020 |Quartz 

As I was coming out, my sister [Valerie] tugged on me and said, ‘That’s the boy who kicked me off my bicycle. Joe Biden: ‘I’ll Kill Your Son’ |Olivia Nuzzi |December 12, 2014 |DAILY BEAST 

So I went home—we only lived about a quarter mile away—and I got on my bicycle and rode back, and he was in the donut shop. Joe Biden: ‘I’ll Kill Your Son’ |Olivia Nuzzi |December 12, 2014 |DAILY BEAST 

The street is closed to traffic and kids run unfettered across inviting hopscotch squares and bicycle lanes. Allah, Mom, and Baklava: Turkish President Uses Mothers and Kids as Political Pawns |Xanthe Ackerman |November 27, 2014 |DAILY BEAST 

Two of the recruits have admitted to two sexual assaults and a bicycle theft in Market Square right at the center of the old town. Libyan Troops Go Wild in England |Nico Hines |November 4, 2014 |DAILY BEAST 

Bicycle riders are prudent to fear being clipped by a passing car. Ebola, ISIS, the Border: So Much to Fear, So Little Time! |Gene Robinson |November 2, 2014 |DAILY BEAST 

The youth nearly fell off the bicycle, but British doggedness saved him from disaster. The Joyous Adventures of Aristide Pujol |William J. Locke 

The bicycle courier receives a fee very thankfully and no doubt this constitutes his chief source of revenue for service rendered. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Manufacture of the motorcycle upon a commercial scale forthwith commenced in the bicycle manufactory at Springfield, Mass. The Wonder Book of Knowledge |Various 

Along in the late nineties a keen interest in bicycle racing led to the introduction of what is known as the motor-paced tandem. The Wonder Book of Knowledge |Various 

Plainly everything after “bicycle” is nothing to the present purpose and should be excluded. English: Composition and Literature |W. F. (William Franklin) Webster