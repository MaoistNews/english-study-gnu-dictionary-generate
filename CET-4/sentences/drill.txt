The link is about as thick as the human skull, and Musk said it could plop neatly onto the surface of the brain through a drill hole that could then be sealed with superglue. Elon Musk’s Neuralink is neuroscience theater |David Rotman |August 30, 2020 |MIT Technology Review 

Collen gave Laney the green light after seeing her dominate early shooting drills in practice. These 3 Breakout Stars Are Making The Most Of The WNBA Bubble |Howard Megdal |August 14, 2020 |FiveThirtyEight 

During several trips to Greenland, he and NASA researchers tested a drill that can cut through hundreds of feet of ice, measuring organic matter and other “biosignatures” as it goes down. He Found ‘Islands of Fertility’ Beneath Antarctica’s Ice |Steve Nadis |July 20, 2020 |Quanta Magazine 

In 2017 and 2019, we went to the Greenland ice sheet to test a drill you could take to Europa. He Found ‘Islands of Fertility’ Beneath Antarctica’s Ice |Steve Nadis |July 20, 2020 |Quanta Magazine 

Coaches can create drills to help players focus on those shots. Why sports are becoming all about numbers — lots and lots of numbers |Silke Schmidt |May 21, 2020 |Science News For Students 

Prince may have pranced around like a carefree libertine onstage, but in rehearsal he was more drill sergeant than sprite. Speed Read: The Juiciest Bits From the History of ‘Purple Rain’ |Jennie Yabroff |January 1, 2015 |DAILY BEAST 

As I forced my exhausted body to exercise, I yelled at my legs like a drill sergeant, demanding five more minutes or one more set. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

For example, studies cited in the report have shown a higher incidence of birth defects for people living near drill sites. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

In place of horses, underclassmen would pull the field pieces around the drill ground. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

And as it turns out, those adorable pink drill bits are potentially facilitating the addition of carcinogens into the environment. The Misogynistic Companies Jumping On The Breast Cancer Bandwagon |Emily Shire |October 16, 2014 |DAILY BEAST 

His drill-like nose, his powerful fore-legs and big, strong feet all served to make him the fastest digger in Pleasant Valley. The Tale of Grandfather Mole |Arthur Scott Bailey 

A minikin three-and-a-half-feet Colonel, being one day at the drill, was examining a strapper of six feet four. The Book of Anecdotes and Budget of Fun; |Various 

She rode the drill every day, like any soldier; and she could take the bugle and direct the evolutions herself. A Horse's Tale |Mark Twain 

They were learning how to drill, how to fire, how to dig ditches and build impromptu forts in haste. Ways of War and Peace |Delia Austrian 

It was explained that great difficulty frequently exists in getting firemen to take part in a boat drill. Loss of the Steamship 'Titanic' |British Government