I mean, this shouldn’t surprise anyone, making the ads harder to distinguish has been a Google trend. Google goes dark theme and passage ranking sees the light: Friday’s daily brief |Barry Schwartz |February 12, 2021 |Search Engine Land 

If searchers have a harder time distinguishing between the paid and free listings, it might cause confusion on what listing the searcher clicks on. Google Search dark theme mode expands but search ads are hard to distinguish |Barry Schwartz |February 11, 2021 |Search Engine Land 

Science fiction has long been distinguished by these dual impulses—leaving home and returning—when it’s not marked by the way that home leaves us, or deceives us when it’s no longer the place we recognize once we’re back. In Science Fiction, We Are Never Home - Issue 95: Escape |Steve Erickson |February 10, 2021 |Nautilus 

In Voice, Police Officer Kang Kwon-Joo has a heightened ability to distinguish sounds, allowing her to solve crime cases as a voice profiler. The 15 Most Anticipated Korean Dramas of 2021 |Kat Moon |January 29, 2021 |Time 

Filter Link Extensions are another way to distinguish your ads in SERPs. Microsoft Advertising now offers Filter Link Extensions |Carolyn Lyden |January 26, 2021 |Search Engine Land 

After years at the head of a parochial school classroom, he could no longer distinguish one blond Irish Catholic kid from another. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

The line between being careful about what you eat and being obsessive is difficult to distinguish. Orthorexia: When Healthy Eating Becomes an Obsession |DailyBurn |October 25, 2014 |DAILY BEAST 

Food business groups argue that a gram of sugar, natural or added, is a gram of sugar—so why distinguish it? Guess Who Doesn’t Want You to Know How Much Added Sugar Is in Your Food |Tim Mak |July 19, 2014 |DAILY BEAST 

Then there was the attempt to distinguish her tax status from those who are “truly well off.” Hillary’s Doomed if She Can’t Learn to Talk About Her Privilege |Keli Goff |June 27, 2014 |DAILY BEAST 

Behind-the-scenes technological differences do not distinguish Aereo's system from cable systems, which do perform publicly. What the Aereo Decision Means for You |Kyle Chayka |June 25, 2014 |DAILY BEAST 

In sorting notes it is necessary to be able readily to distinguish between notes of this bank and notes of other reserve banks. Readings in Money and Banking |Chester Arthur Phillips 

This is a feature by means of which it is always possible to distinguish the Great Horsetail from any other species. How to Know the Ferns |S. Leonard Bastin 

In the darkness and confusion I did not distinguish the face of the man who rendered me this assistance. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

I had no idea who they were, as the Grand Duke was in morning costume, and had no star or decoration to distinguish him. Music-Study in Germany |Amy Fay 

It reappears during a relapse, and thus helps to distinguish between a relapse and a complication, in which it does not reappear. A Manual of Clinical Diagnosis |James Campbell Todd