Their three-day scientific outing was paid for by Epstein and was big success. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

Three on-the-record stories from a family: a mother and her daughters who came from Phoenix. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

One of the other cops fired three times and those who were still able to give chase did. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

So I was looking back at the years, and that really popped out at me, those three years. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

Of the three nominated, Webster did the best, receiving 12 votes, Gohmert and Yoho received three and two votes, respectively. Democrats Accidentally Save Boehner From Republican Coup |Ben Jacobs, Jackie Kucinich |January 6, 2015 |DAILY BEAST 

It is thinner than that of chronic bronchitis, and upon standing separates into three layers of pus, mucus, and frothy serum. A Manual of Clinical Diagnosis |James Campbell Todd 

I waited three months more, in great impatience, then sent him back to the same post, to see if there might be a reply. The Boarded-Up House |Augusta Huiell Seaman 

There are three things a wise man will not trust: the wind, the sunshine of an April day, and woman's plighted faith. Pearls of Thought |Maturin M. Ballou 

In cross-section the burrows varied from round (three inches in diameter) to oval (three inches high and four inches wide). Summer Birds From the Yucatan Peninsula |Erwin E. Klaas 

It separates into three layers upon standing—a brown deposit, a clear fluid, and a frothy layer. A Manual of Clinical Diagnosis |James Campbell Todd