The company’s revenue grew 90 percent from April through September and has continued on a similar rate since, he said. Online wine sales continue to grow, but can they — or should they — replace local shops? |Dave McIntyre |February 26, 2021 |Washington Post 

If trends continue, Northam said, the state plans to remove the 1,000-person cap for outdoor venues by April. D.C.’s pandemic toll surpasses 1,000 as Bowser announces her sister’s death |Rebecca Tan, Jenna Portnoy |February 24, 2021 |Washington Post 

Writing in Newsweek in April, he argued that telemedicine, not the emergency room, should be the front line of pandemic care, since it could serve people who might be infected while protecting nurses and doctors from exposure. The doctor will Zoom you now |Katie McLean |February 24, 2021 |MIT Technology Review 

By sales, Zara and H&M are still bigger, but while their share prices have fluctuated through the global turmoil brought about by Covid-19, Fast Retailing’s stock has been on a steady rise since April. How Uniqlo became the world’s most valuable clothing company |Marc Bain |February 22, 2021 |Quartz 

This could all be interesting come April, when the NCAA’s selection committee will be tasked with comparing apples to ham sandwiches. James Madison’s football team is set for its first of two 2021 seasons. Just roll with it. |Barry Svrluga |February 19, 2021 |Washington Post 

Her latest book, Heretic: The Case for a Muslim Reformation, will be published in April by HarperCollins. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

Reached Tuesday, a Sitrick Co. rep confirmed they parted ways with Epstein in April 2011. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

It was known as the feast of Akitu, and it was celebrated in April. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

“Scratch a liberal, find a fascist every time,” Woods tweeted in April. How James Woods Became Obama’s Biggest Twitter Troll |Asawin Suebsaeng |December 31, 2014 |DAILY BEAST 

In response, the April Revolution protests erupted in much of the country. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

There are three things a wise man will not trust: the wind, the sunshine of an April day, and woman's plighted faith. Pearls of Thought |Maturin M. Ballou 

One evening in the month of April, a slim, straight-backed girl stood in the veranda of a bungalow at Meerut. The Red Year |Louis Tracy 

One day in April the thermometer suddenly rose to eighteen above the freezing-point of Fahrenheit. The Giant of the North |R.M. Ballantyne 

A clock was put above the spot where the fountain stood, in April, 1852, which cost £60. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

The south tunnel in New Street was blocked April 18, 1877, by a locomotive turning over. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell