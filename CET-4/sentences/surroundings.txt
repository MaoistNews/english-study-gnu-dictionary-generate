George had to go inside to take a phone call so I sat feasting my eyes on the beauty of the surroundings. When Gary Wright Met George Harrison: Dream Weaver, John and Yoko, and More |Gary Wright |September 29, 2014 |DAILY BEAST 

But Peggy is not tempted in the least to change her shop to fit in with the new(er) surroundings. New York's Sexiest Kosher Corsets |Emily Shire |August 20, 2014 |DAILY BEAST 

People are locked in texting, or whatever it is on their screens, and detached from their physical surroundings. The End of New York: How One Blog Tracks the Disappearance of a Vibrant City |Tim Teeman |August 6, 2014 |DAILY BEAST 

Strangers often believe, incorrectly, that he is unaware of his surroundings. Disney World Means Everything to a Special Needs Mom |Elizabeth Picciuto |July 17, 2014 |DAILY BEAST 

If the officials involved with the chemical weapons shipment are worried about the criminal surroundings, they are not showing it. Syria’s Chemical Weapons Arrive in Notorious Mafia Port |Barbie Latza Nadeau, Christopher Dickey |July 1, 2014 |DAILY BEAST 

Here began indeed, in the drab surroundings of the workshop, in the silent mystery of the laboratory, the magic of the new age. The Unsolved Riddle of Social Justice |Stephen Leacock 

And our surroundings at that particular moment were not the most favorable to coherent thought or plausible theory-building. Raw Gold |Bertrand W. Sinclair 

When his eyes grew accustomed to his surroundings he made out the shape of a native boat moored beneath the wall. The Red Year |Louis Tracy 

They would have made a good study for an artist, had an artist been at hand to sketch them and their surroundings. The World Before Them |Susanna Moodie 

Meanwhile Darry had explained his idea to the others, and they were all eager to view the surroundings of the Gandy stock farm. The Campfire Girls of Roselawn |Margaret Penrose