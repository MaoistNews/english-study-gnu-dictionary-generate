In some cases, it’s people that didn’t even know they were sick. Timeline: The 124 times Trump has downplayed the coronavirus threat |Aaron Blake, JM Rieger |September 17, 2020 |Washington Post 

The case stemmed from challenges to the state’s orders to close “non life-sustaining” businesses and to limit gatherings to 25 people indoors or 250 people outdoors. Courts may reconsider temporary coronavirus restrictions as pandemic drags on |Anne Gearan, Karin Brulliard |September 16, 2020 |Washington Post 

Redfield said he expected a vaccine to start being available in November or December, with the first people to receive it being those with health problems that make them most vulnerable to a severe case or death if they are infected. Biden questions whether a vaccine approved by Trump would be safe |Sean Sullivan |September 16, 2020 |Washington Post 

In the past, when I’ve discussed a case, unfortunately, that individual’s name was leaked to the media. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

In each case, Democratic presidential nominee Joe Biden leads — by a wide margin in Minnesota and a narrow one in Wisconsin. The key shifts in Minnesota and Wisconsin that have improved Biden’s chances of victory |Philip Bump |September 16, 2020 |Washington Post 

Certain features of its history suggest why this may be the case. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

And, in the case of fluoride, at least, that doubt might actually be justified. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Her latest book, Heretic: The Case for a Muslim Reformation, will be published in April by HarperCollins. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

Their friendship began when Krauss, who was chairman of the physics department at Case Western in Cleveland, sought out Epstein. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

A grand juror in the Ferguson case is suing to be able to explain exactly what went down in the courtroom. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

The case was an assault and battery that came off between two men named Brown and Henderson. The Book of Anecdotes and Budget of Fun; |Various 

In this case, I suspect, there was co-operant a strongly marked childish characteristic, the love of producing an effect. Children's Ways |James Sully 

Sometimes in the case of large plants, cones have been known to occur on the tips of the branches of the Marsh Horsetail. How to Know the Ferns |S. Leonard Bastin 

As a rule, however, even in the case of extreme varieties, a careful examination of the specimen will enable it to be identified. How to Know the Ferns |S. Leonard Bastin 

Thus was the man left entirely to the devil, not even his life being reserved, as in the case of Job. Solomon and Solomonic Literature |Moncure Daniel Conway