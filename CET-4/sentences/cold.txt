It’s the kind of place where the waiter prepares things tableside, like mixing a Manhattan so cold a layer of ice floats on its surface, or tossing a Caesar salad with silver tongs. We’re all fantasizing about post-covid dining now: “I just want someone to spill a beer on me” |Emily Heil |February 12, 2021 |Washington Post 

For past initiatives, like his $2 billion homelessness directive, members of Bezos’s team cold-called people they knew to figure out who to donate to, instead of opening up a public channel. Can Billionaires Really Save Us from Climate Disaster? |Heather Hansman |February 12, 2021 |Outside Online 

I use it every morning to loosen up my back, and if it’s too cold to run, I’ll go through a stretching routine or an online yoga session instead. 5 Pieces of Gear That Help Me Stay Active During Winter |Jakob Schiller |February 11, 2021 |Outside Online 

A cooler on the porch may be necessary to store meats and cold items. Hints From Heloise: Grocery delivery has its pros and cons |Heloise Heloise |February 11, 2021 |Washington Post 

It’s incredible how much comfort a nongreasy SPF moisturizer adds to your exposed skin on a cold ski day. Outdoorsy Gear Guy–Approved Valentine's Day Gifts |Joe Jackson |February 10, 2021 |Outside Online 

This is comedy based on a cold humor, detached, euphemistic, devoid of any generosity. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

We indulge in expensive cold-pressed juices and SoulCycle classes, justifying these purchases as investments in our health. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

Cold War fears could be manipulated through misleading art to attract readers to daunting material. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 

The vaccine is delivered through a “carrier virus” that causes a common cold in chimpanzees but does not affect humans. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

Accusing his opponents of being locked in a Cold War mind-set, it is Stone who is beholden to old orthodoxies. Oliver Stone’s Latest Dictator Suckup |James Kirchick |January 5, 2015 |DAILY BEAST 

Madame de Condillac stood watching him, her face composed, her glance cold. St. Martin's Summer |Rafael Sabatini 

Being quieted by the Captain with a draught of cold tea, and made to sit down, the examination of the book proceeded. The Giant of the North |R.M. Ballantyne 

When alone she sometimes picked it up and kissed the cold glass passionately. The Awakening and Selected Short Stories |Kate Chopin 

Such throats are trying, are they not?In case one catches cold; Ah, yes! Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

Turn we our backs to the cold gloomy north, to the wet windy west, to the dry parching east—on to the south! Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various