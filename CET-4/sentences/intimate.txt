We wanted it to be warm, we wanted it to be kind, we wanted it to be casual, and intimate. How Hallmark is handling pandemic Valentine’s Day |Luke Winkie |February 12, 2021 |Vox 

These are some of our favorite intimate encounters set in these close quarters. What floor? |Sophia Nguyen |February 12, 2021 |Washington Post 

The perpetrator had uploaded these non-intimate images—holiday and pregnancy photos and even pictures of her as a teenager—and encouraged other users to edit her face into violent pornographic photos. Deepfake porn is ruining women’s lives. Now the law may finally ban it |Karen Hao |February 12, 2021 |MIT Technology Review 

Legend love his fans too and to celebrate them, he’s hosting an intimate “Weekend Of Love” Valentine’s Day themed event on Instagram. John Legend Wants To Help You Get Romantic This Weekend |Charli Penn |February 11, 2021 |Essence.com 

The intimate story of Glaser’s subjects makes her book compelling, but the societal dots she’s able to connect make it important. For a mother forced to give up her child, decades of grief, shame and secrets |Ellen McCarthy |February 5, 2021 |Washington Post 

The Strange Social History of Our Most Intimate Institution. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

Literature in the 14th century, Strohm points out, was an intimate, interactive affair. A Year In The Life of The Canterbury Tales’ Storied Beginnings |Wendy Smith |December 25, 2014 |DAILY BEAST 

She suggested that Gregory stack newspapers on his desk to give the set an intimate, coffeehouse feel. David Gregory's 'Meet the Press' Eviction Exposed in Washingtonian Takedown |Lloyd Grove |December 23, 2014 |DAILY BEAST 

But my favorites, and by far the most intimate photos at the gallery, are by Jimmy Steinfeld. ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

It offers keen insights into Hitch's craft while painting an intimate and unsentimental picture of the man behind the camera. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

They were still talking of the poem and the music, exchanging intimate thoughts in the language he could not understand. The Wave |Algernon Blackwood 

Besides, these are only a few intimate friends who have assembled to celebrate my daughter's fte-day. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Your most intimate friend arrived in Paris, and you choose the next day to make a little tour! Confidence |Henry James 

When the service was over, they walked leisurely homeward, and their conversation became more intimate. The Homesteader |Oscar Micheaux 

Our friendship was close and intimate, such as is formed in the warmth of youth and which the grave alone dissolves. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow