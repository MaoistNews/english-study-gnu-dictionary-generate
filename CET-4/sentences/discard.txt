And in a way that was right, because he was keen to save the best bits of it and to discard the worst. Napoleon Was a Dynamite Dictator |J.P. O’Malley |November 7, 2014 |DAILY BEAST 

His past lives all display a remarkable bloodlust, one he continues to discard. ‘Game of Thrones’ Withdrawal? Watch Nickelodeon’s Fantasy Epic ‘The Legend of Korra’ |David Levesley |July 1, 2014 |DAILY BEAST 

So maybe what I've really got here is an Old Master discard being used – by me, the museum visitor – as a modern objet trouvé. Old Master as Ax Murderer? |Blake Gopnik |December 13, 2013 |DAILY BEAST 

Discard your alcohol and dispose of your pork “The Muslims Are Coming!” Why the Right Is Bashing My Muslim Comedy Movie |Dean Obeidallah |September 22, 2013 |DAILY BEAST 

Anyone walking down a Manhattan street on trash day knows that New Yorkers discard some spectacular things. One New York Sanitation Worker Has a New Idea for Recycling Trash … Turn It Into Art |Nina Strochlic |July 25, 2013 |DAILY BEAST 

William, indeed, was not the man to discard an old friend for a new one. The History of England from the Accession of James II. |Thomas Babington Macaulay 

The vote which required the King to discard them merely because they were what he himself was seemed to him a personal affront. The History of England from the Accession of James II. |Thomas Babington Macaulay 

He would discard any doctrine which, logically carried out, led to absurdity. Beacon Lights of History, Volume I |John Lord 

"Yes; she was too genuinely a lady to encourage his suit, then discard him at the last moment," he concluded, despondently. A Fortune Hunter; Or, The Old Stone Corral |John Dunloe Carteret 

Here you are, slouchin' around without a dressin' jacket er slippers en talkin' 'bout an ole song that's in the discard. David Lannarck, Midget |George S. Harney