It’s possible that immunity could be achieved at a level closer to 60 percent. The problem with Trump’s ‘herd mentality’ line isn’t the verbal flub. It’s the mass death. |Philip Bump |September 16, 2020 |Washington Post 

He blamed China for the pandemic and said he saved many lives by “closing up the country.” Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

Lowe’s is currently on a hot streak, but the gap is wide, and closing it requires a leader like Ellison with devoted employees, according to Wahba. Lowe’s teams up with Daymond John to diversify its suppliers with a virtual pitch competition |McKenna Moore |September 15, 2020 |Fortune 

In recent years, summer has brought a season of fear to California, with ever-worsening wildfires closing in. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

Big names like Tesla and Apple, which dragged the sector down last week, posted strong rallies, up 3% and 12% respectively as of Monday’s close. Tech sell off was a ‘head fake’ as stocks rebound, say analysts |Anne Sraders |September 14, 2020 |Fortune 

Not actual CIA agents, but U.S. government personnel who have worked very closely with the CIA, and who are fans of the show. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Those who have watched anti-gay groups closely suggest that there will be two major strategic shifts in their strategy. ‘Only God’ Can Stop Gay Marriage |Tim Mak |January 6, 2015 |DAILY BEAST 

Because I was living with this story, I watched closely as Hollywood considered making a film about Selma. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

We are looking forward to working closely with this champion of liberty. Vaclav Klaus, Libertarian Hero, Has His Wings Clipped by Cato Institute |James Kirchick |December 22, 2014 |DAILY BEAST 

There are also portraits, protests, reportage and Jim Marshall was there to shoot it all—or pretty closely. The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 

But it was necessary to take Silan, which the rebels hastened to strengthen, closely followed up by the Spaniards. The Philippine Islands |John Foreman 

We had now approached closely to the foot of the mountain-ranges, and their lofty summits were high above us in mid-air. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Their opportunities and earnings are relatively small, and in order to live they must figure closely. Readings in Money and Banking |Chester Arthur Phillips 

Keep closely covered with a bell glass and, in a few weeks, more or less, the baby Ferns will start to put in an appearance. How to Know the Ferns |S. Leonard Bastin 

Then she bent her glance to the writing, and studied it a moment, what time the man from Paris watched her closely. St. Martin's Summer |Rafael Sabatini