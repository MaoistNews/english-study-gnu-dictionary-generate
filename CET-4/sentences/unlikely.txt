Once approved, a vaccine is unlikely to be widely available for several months. 6 questions that must be answered in the race for a vaccine |jakemeth |September 15, 2020 |Fortune 

In such efforts, Russia is unlikely to encounter resistance from the Belarus security apparatus, which probably views its interests and future tied to maintenance of tight central control. Poison, Protest and the Power of Putin |Tracy Moran |September 4, 2020 |Ozy 

As Vox’s Umair Irfan reports, it’s still unlikely that a vaccine will be authorized for use by then. A third of Americans might refuse a Covid-19 vaccine. How screwed are we? |Brian Resnick |September 4, 2020 |Vox 

America is unlikely to know its presidential winner on election night due to the expected surge in mail-in votes. Podcast: Exploring the potential for election night nightmares |Axios |September 3, 2020 |Axios 

A survey conducted in late June found that 65% of Americans said they are “very unlikely” to return to theaters immediately after they reopen. Will ‘Tenet’ revive U.S. movie theaters as it’s finally released? |dzanemorris |September 3, 2020 |Fortune 

Harris is unlikely to see a challenge from Villaraigosa, either. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

At least 29 fellow Republicans must vote against Boehner for a second ballot to be reached, and that seems very unlikely. The YOLO Caucus' New Cry for Attention |Ben Jacobs |January 4, 2015 |DAILY BEAST 

Almost forty years later, it seems unlikely this will ever be the case. Juiciest ‘Star Wars: The Force Awakens’ Rumors (and Some Debunked Ones) |Rich Goldstein |January 3, 2015 |DAILY BEAST 

We tend to think not, but the rise of King, Kennedy, and Lincoln was unlikely, too. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

It was Orlando vs. Justin in an Ibiza melee with two highly unlikely opposing parties. The Bloom-Bieber Brawl We Didn’t Know We Needed |Kevin O’Keeffe |December 29, 2014 |DAILY BEAST 

The magistrate appeared to think such an occurrence not at all unlikely, as he committed him to prison for three months. The Book of Anecdotes and Budget of Fun; |Various 

The converse seems to me unlikely; however, they are not remarkable for originality. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Marius looks elsewhere for a wife—unless mademoiselle of her own free will should elect to wed him—a thing unlikely. St. Martin's Summer |Rafael Sabatini 

For your sake, old fellow, I hope she won't succeed, but I have known more unlikely things happen even than this. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Sara Lee might have fought the War Office single-handed and won out, but it is extremely unlikely. The Amazing Interlude |Mary Roberts Rinehart