Later, she won a new flock of fans as BFF Lilly in “The Princess Diaries” movies, as well. For Heather Matarazzo, ‘Equal’ is still a cause worth fighting for |John Paul King |October 20, 2020 |Washington Blade 

A flock of birds is kind of a canonical example of self-organization. Complexity Scientist Beats Traffic Jams Through Adaptation |Rodrigo Pérez Ortega |September 28, 2020 |Quanta Magazine 

On Monday, a flock of tech companies decided it was time to make their plans to go public, well, public, as tech stocks have soared during the pandemic. As VC payday nears, Sequoia sits in the middle of the IPO deluge |Lucinda Shen |August 25, 2020 |Fortune 

When the duo surveyed singles about the personality traits they were looking for in romantic partners, they found that a similarity model—the age-old idea that birds of a feather flock together—best described the data. Your Romantic Ideals Don’t Predict Who Your Future Partner Will Be - Issue 88: Love & Sex |Alice Fleerackers |August 5, 2020 |Nautilus 

One of the yeomen warders, known as the ravenmaster, is also responsible for looking after the tower’s resident flock of ravens. Guarding Britain’s crown jewels was a secure job for over 500 years. Not anymore |Jeremy Kahn |July 20, 2020 |Fortune 

Behind him stood a flock of fifth-grade boys—and two second-grade girls—all of them wearing the exact same yellow hat. Even Grade School Kids Are Protesting the Garner Killing Now |Caitlin Dickson |December 6, 2014 |DAILY BEAST 

Fans of the series will flock to see ‘Mockingjay’ this Thanksgiving weekend. Team Peeta or Team Gale: Why the ‘Hunger Games’ Love Triangle Ruins ‘Mockingjay – Part 1’ |Kevin Fallon |November 28, 2014 |DAILY BEAST 

And almost immediately, his fellow survivors flock to him, seeing his potential to be a great hero. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

Pressing the dodge button at the right time causes her to temporarily burst into an invincible flock of crows. Bayonetta Is Nintendo’s Graphic, Ass-Kicking Barbie |Alec Kubas-Meyer |October 24, 2014 |DAILY BEAST 

He is to be admired for his kindness and genuine pastoral concern for all the members of his flock. Pope Francis Pushes the Church Another Step Further on Gays |Gene Robinson |October 16, 2014 |DAILY BEAST 

A flock of weary sheep pattered along the road, barnward bound, heavy eyed and bleating softly. The Soldier of the Valley |Nelson Lloyd 

Once he suddenly found himself in the road driving a small flock of goats, whose he knew not, nor whence he got them. Ramona |Helen Hunt Jackson 

The faces of a flock of sheep are to a stranger all alike; to the shepherd, each has its personal individuality. Violins and Violin Makers |Joseph Pearce 

I have not been able to learn that the migratory flock above spoken of extended to any of the other Islands. Birds of Guernsey (1879) |Cecil Smith 

Gwynne accepted this act of sacrifice with a matter-of-fact nod, and it was but a moment later that they came upon another flock. Ancestors |Gertrude Atherton