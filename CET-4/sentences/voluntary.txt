With the country in the midst of a pandemic, policy makers and public health experts have proposed voluntary mail-in voting as one way for Americans to safely vote in November. Mandatory mail-in voting hurts neither Democratic nor Republican candidates |Sujata Gupta |August 26, 2020 |Science News 

Another 12,500 employees have retired and 11,000 are taking a voluntary leave of absence for the month of October—meaning that American expects to cut a total of 40,000 employees from a workforce that numbered 140,000 at the start of the pandemic. American Airlines announces plan to cut 19,000 jobs—unless Congress extends pandemic aid |Maria Aspan |August 25, 2020 |Fortune 

“They’re looking under the couch cushions for every megawatt hour in the West but aren’t willing to pay for someone’s voluntary action,” he says. Here’s how to keep California’s grid from buckling under the heat |James Temple |August 18, 2020 |MIT Technology Review 

This bill would require anyone who operates a contact tracing app to collaborate with public health officials, make app use voluntary and prevent commercial use of data collected by the apps. Readers ask about antibody tests, chimeras and public health and privacy |Science News Staff |July 26, 2020 |Science News 

Humans For Humanity is a non profit, voluntary organisation run by a 25 year old social worker, Anurag Chauhan who is working on a project called WASH – Women Sanitation Hygiene from 2015. Menstruation Comes With Innumerable Taboos In India |LGBTQ-Editor |May 29, 2020 |No Straight News 

Our world is in so many ways more based on voluntary exchange than ever before. Relax—Both Parties Are Going Extinct |Nick Gillespie |November 4, 2014 |DAILY BEAST 

“I see the celebs capitalizing on all these leaks, whether voluntary or not,” wrote Reddit user attacktei. ‘The Fappening’ Continues: Nudes of Kim Kardashian, Mary-Kate Olsen, and More Celebs Leak Online |Marlow Stern |September 20, 2014 |DAILY BEAST 

The Non-GMO Project, on the other hand, provides a voluntary way for manufacturers to declare themselves GMO free. Whole Foods' Anti-GMO Swindle |Michael Schulson |September 15, 2014 |DAILY BEAST 

Certification is voluntary only insofar as work as a physician is voluntary. Rand Paul and the Certification Racket |Russell Saunders |August 11, 2014 |DAILY BEAST 

UNRWA is funded by voluntary contributions, but Arab and predominantly Islamic nations are rarely among the top donors. War of Words Between Israel and UN Continues |Betsy Pisik |August 10, 2014 |DAILY BEAST 

This system had been in full operation in both districts prior to the general application of the voluntary system. Readings in Money and Banking |Chester Arthur Phillips 

It therefore took under favorable consideration the question of a voluntary clearing system. Readings in Money and Banking |Chester Arthur Phillips 

This is secured only by right objects of thought; it is impossible to reach it by voluntary mechanics. Expressive Voice Culture |Jessie Eldridge Southwick 

Our voluntary service regulars are the last descendants of those rulers of the ancient world, the Roman Legionaries. Gallipoli Diary, Volume I |Ian Hamilton 

A covenant is a mutual voluntary compact between two parties on given terms or conditions. The Ordinance of Covenanting |John Cunningham