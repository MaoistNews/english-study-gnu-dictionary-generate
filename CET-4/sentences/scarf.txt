Perfume bottles stand next to bright scarves, her jewelry box and the urn with her ashes — all sitting on an antique table that belonged to her parents. Tiffany Shackelford, 46, was known as a unique, fun ‘force’ to friends, family |Dana Hedgpeth |February 11, 2021 |Washington Post 

Traditionally known as Keffiyeh, Kufiya, Hatta or Shemagh scarf, it is offered in many different colorways. Stylish and sophisticated scarves that everyone will love |PopSci Commerce Team |January 13, 2021 |Popular-Science 

When it comes to accessories, scarves are an extremely practical and versatile item. Stylish and sophisticated scarves that everyone will love |PopSci Commerce Team |January 13, 2021 |Popular-Science 

The conversation-starter silk scarf also comes with Napier’s beloved facial cleansing wipes. 10 Beauty Gifts That Are Actually Still In Stock |cmurray |December 18, 2020 |Essence.com 

The company also sells solid, printed, and scarf masks, and you can mix and match if you’re buying more than one. Eight masks that make great stocking stuffers |John Kennedy |December 10, 2020 |Popular-Science 

Even for Arabic dance no one wears a long dress, just a scarf around the hips. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

The scarf was accompanied by a framed painting of Tanzanian birds and is valued at $595. Meditation Rugs, Swords, and Horse Head Fiddles: The Strangest Gifts Given to Government Bigwigs |Ben Jacobs |November 11, 2014 |DAILY BEAST 

Every day before leaving home, Sara stands before the mirror and tightens the knot on her scarf. Acid Attacks on Women Spread Terror in Iran |IranWire |October 18, 2014 |DAILY BEAST 

At least Jake Gyllenhaal shaved off his grotesque face scarf this year. Leo, the Beard Has to Go: When a Man’s Facial Hair Reaches Crisis Point |Tim Teeman |September 22, 2014 |DAILY BEAST 

Web users from across Iran gave their opinions, not of her work, but of her scarf. Iranian Math Genius Mirzakhani Unveiled by President Rouhani |IranWire |August 18, 2014 |DAILY BEAST 

She had laid aside the white shawl, but wore a red Indian scarf over her black gown. Ancestors |Gertrude Atherton 

Lamb could visualize him putting his coat on a hanger, carefully folding a scarf over it. Hooded Detective, Volume III No. 2, January, 1942 |Various 

He thought her a charming picture in her long white coat, with a lace scarf over her head, and her arms full of costly toys. Ancestors |Gertrude Atherton 

In the dance in Scene VI she used a long black gauze scarf and a white one. Fifty Contemporary One-Act Plays |Various 

She did so, and bound the Vizier's arms with a scarf, which so amused the Wazir that he laughed loud and long. Fifty Contemporary One-Act Plays |Various