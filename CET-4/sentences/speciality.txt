Choosing a city like Houston to focus on the energy sector, “makes sense and is a smart way” to “expand your market by a speciality,” and find areas of coverage that differentiate the FT from other publications covering that sector as well. The Financial Times plans to open 2 more U.S. bureaus to target ‘global Americans’ |Sara Guaglione |July 27, 2021 |Digiday 

The company, which has a 20-year history, said Fund V was “substantially oversubscribed” and will focus on the funds main specialities in software, fintech, marketplaces and the consumer internet. Europe’s DN Capital launches its new $350M fund after a knock-out year for its portfolio |Mike Butcher |July 9, 2021 |TechCrunch 

Bring on all the speciality desserts, tasty quiches, and perfectly grilled steak. Unique pan designs for a creative cooking experience |PopSci Commerce Team |October 28, 2020 |Popular-Science 

The DoD order requires services to develop "gender-neutral" standards for each speciality. Women in Combat: Standards Meet Equality |David Frum |January 31, 2013 |DAILY BEAST 

The house speciality is a shot-ski—a wooden ski holding a dozen shot glasses. Where Will Harry Party Back in London, And Who Will He Be Kissing? |Tom Sykes |January 24, 2013 |DAILY BEAST 

The speciality was mutton tagine, softly braised in the tagine pot with peas, vegetables, and spices. The World’s Five Best Cheap Meals |Jodi Ettenberg |November 14, 2012 |DAILY BEAST 

Thus the scale of sorrow from sad melancholy to painful suffering became the speciality of the Dsseldorf school. The History of Modern Painting, Volume 1 (of 4) |Richard Muther 

She had a speciality for making coffee, and really it was quite drinkable. Castellinaria |Henry Festing Jones 

Never before in the annals of that great school had a Speciality been known to tell a story of another girl. Betty Vivian |L. T. Meade 

Later, his speciality was the taking off of foreigners—a sorry means of inciting to laughter for a man of intellect. The English Stage |Augustin Filon 

On the morning of that day Fanny made another effort to induce Betty to renounce the idea of becoming a Speciality. Betty Vivian |L. T. Meade