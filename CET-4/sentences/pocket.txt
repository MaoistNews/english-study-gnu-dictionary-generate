Place your properly folded mask inside the bag and slip the bag into a pocket while you take a quick mask break. Why you shouldn’t ever wear your mask around your neck |Erin Fennessy |February 11, 2021 |Popular-Science 

Worn down and chipped from frequent handling, these figurines look like they might have been carried around in pockets or bags. What archaeologists got wrong about female statues, goddesses, and fertility |Annalee Newitz |February 10, 2021 |Popular-Science 

It’s great fuel, and I can cram it in a ski-jacket pocket and eat it on the lift. These Are Our Editors' Go-To Ski Lunches |Ula Chrobak |February 9, 2021 |Outside Online 

Why can’t this be a superpower that every developer has in their pocket. nextmv raises $8M Series A to increase accessibility to its automation optimization tech |Jordan Crook |February 9, 2021 |TechCrunch 

On every trip that involves wildlife, I have a secret urge to pocket the animals and take them home. In ‘The Whispering Land,’ a British naturalist collects travel tales — and animals with tails — in Argentina |Andrea Sachs |February 4, 2021 |Washington Post 

I was already over forty, had hardly a nickel in my pocket and this was the biggest break in my life. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

It had a wide brim and a tall crown, which created an insulated pocket of air and could also be used to carry water. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

This leaves thousands of women at companies across the United States left to pay out of pocket for their birth control. The 26 Next Hobby Lobbys |Abby Haglage |December 17, 2014 |DAILY BEAST 

One African American woman brandished a pocket-sized copy of the Constitution while marching. Sharpton Recalls Civil Rights Struggle in DC March Against Police Violence |Ben Jacobs |December 13, 2014 |DAILY BEAST 

Being dapper is all about attention to detail, like sporting a perfectly tucked handkerchief in your suit pocket. The Daily Beast’s 2014 Holiday Gift Guide: For the Don Draper in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

The young man smiled at the girl, as he crushed up the notes and stuffed them into his pocket. Rosemary in Search of a Father |C. N. Williamson 

Down in his galleries and chambers where it was dark as a pocket Grandfather Mole enjoyed himself thoroughly. The Tale of Grandfather Mole |Arthur Scott Bailey 

Absently his hands wandered through the pockets, and found his purse and the money in an outside pocket. The Homesteader |Oscar Micheaux 

“Of course we know it, sir,” rejoined Fogg, slapping his pocket—perhaps by accident. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Thinking it was a request for employment which he could not offer, Malcolm stuffed it carelessly into a pocket. The Red Year |Louis Tracy