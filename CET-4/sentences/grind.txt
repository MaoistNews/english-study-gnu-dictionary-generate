This can be super helpful for fine tuning your grind for espresso machines. Gear to make every day feel like National Coffee Day |PopSci Commerce Team |September 29, 2020 |Popular-Science 

The grind tray snaps out and lets you dump or repurpose excess or spilled coffee. Gear to make every day feel like National Coffee Day |PopSci Commerce Team |September 29, 2020 |Popular-Science 

The Toddy requires 12 ounces of grinds and seven cups of water. Gear to make every day feel like National Coffee Day |PopSci Commerce Team |September 29, 2020 |Popular-Science 

You can see it in the dives on the ground, the exhaustion, the grind. The NBA Finals Burst the Analytics Bubble |Joshua Eferighe |September 29, 2020 |Ozy 

Use a broad angle and grind for wood processing or other brute-force tasks and a narrow edge and concave grind for meat processing or other slicing duties. Three Questions to Ask Yourself Before Buying a Knife |Wes Siler |September 3, 2020 |Outside Online 

It was promoted on what might be called not-quite-mainstream or, indeed, axe-to-grind media. Dutch Biker Gangs Vs. ISIS |Nadette De Visser, Christopher Dickey |December 9, 2014 |DAILY BEAST 

But what if this war does eventually involve ground troops, and what if it does just grind on for years? Can America Still Win Wars? |Michael Tomasky |October 4, 2014 |DAILY BEAST 

The daily grind of child-rearing and the stress of sharing responsibility seem to be a big part of it. Pope Francis Is Wrong About My Child-Free Life |Amanda Marcotte |June 6, 2014 |DAILY BEAST 

Those who propagate it are considered paranoids or activists with an axe to grind. Did Putin Blow Up the Whole Polish Government in 2010? A Second Look. |Will Cathcart |April 11, 2014 |DAILY BEAST 

And I think it will slowly grind you down until you have the utmost respect for it, which I now have. Prince Harry: "Antarctica Jumped Up And Bit Me On The Ass." |Tom Sykes |March 19, 2014 |DAILY BEAST 

Take a millstone and grind meal: uncover thy shame, strip thy shoulder, make bare thy legs, pass over the rivers. The Bible, Douay-Rheims Version |Various 

The poorest people reduce it to powder by manual labour, in the same way as they grind corn preparatory to baking it into cakes. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

He delivered his repeated phrases with an unctuous indulgent roll that made Gwynne long to grind his teeth. Ancestors |Gertrude Atherton 

He turned over slowly, feeling the hard canes grind into his body as he moved. Insidekick |Jesse Franklin Bone 

Its that Im just sick of the office and the grind every week and no change!nothing new, nothing happening. Chains |Elizabeth Baker