PBCs have an affirmative duty to be good corporate citizens and to treat all stakeholders with respect. 50 years later, Milton Friedman’s shareholder doctrine is dead |jakemeth |September 13, 2020 |Fortune 

He is known to lower his eyes in Putin’s presence, out of respect. Spy Wars: The Hidden Foe America Must Defeat to Save Its Democracy |Charu Kasturi |September 13, 2020 |Ozy 

Anthony Mallott said his father continued to express respect for Potts up until his death. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

In many respects, this new bill seems to seek not to combat fake news but, rather, to control users. Brazil’s “fake news” bill won’t solve its misinformation problem |Amy Nordrum |September 10, 2020 |MIT Technology Review 

So in that respect, what the aforementioned companies are doing is nothing new. ‘There wasn’t a huge shift’: TV upfront market did not undergo expected overhaul this year |Tim Peterson |September 9, 2020 |Digiday 

If anything the work the two cops and the maintenance guy were doing deserves more respect and probably helped a lot more people. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

With all due respect to his athletic skill, Gronkowski is not high on the list of NFL players that elicit carnal thoughts. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

Yazbek tells The Daily Beast that the traffickers guarantee their service, and they treat the Syrian refugees with respect. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

They stood in a single row, united by solemn respect as the Liu family remained inside. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

You expect soldiers of all ranks to understand the need to respect the chain of command, regardless of personal feelings. We Need Our Police to Be Better Than This |Nick Gillespie |December 31, 2014 |DAILY BEAST 

It is, however, true, that in this respect the German hexametrist has a considerable advantage over the English. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Very charmingly is this respect for rule exhibited in all dealings with animals, also dolls and other pets. Children's Ways |James Sully 

Great had been her indignation at the want of respect shown to the Reverend John Dodd's cloth. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The house was but a single story high, but in this respect the king's palace itself was no better. Our Little Korean Cousin |H. Lee M. Pike 

The French Railroads are better in this respect, and the American cannot be worse, though the fault is not unknown there. Glances at Europe |Horace Greeley