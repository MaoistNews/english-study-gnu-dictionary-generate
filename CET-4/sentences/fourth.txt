Robinson began making his own weather observations as a fourth-grader. As Earth warms, will snowstorms be a thing of the past? |Haben Kelati |January 26, 2021 |Washington Post 

Henne executed perhaps the most surprising play call of the postseason so far when he hit Tyreek Hill on fourth and inches at the 50 with a little more than a minute left in last weekend’s game. Chad Henne is in the pantheon of NFL backup QB heroes, whether he gets another shot or not |Cindy Boren |January 21, 2021 |Washington Post 

The site’s prediction model projects the Capitals to finish fourth in the East with 63 points and gives the team a 53 percent chance to make the playoffs. What you may have missed in the Caps’ offseason, and what experts predict for this season |Scott Allen |January 14, 2021 |Washington Post 

I want to be out there every fourth day instead of every fifth. Reds’ Trevor Bauer, Indians’ Shane Bieber capture Cy Young awards |Jesse Dougherty |November 12, 2020 |Washington Post 

He signed with the Lakers as a free agent in 2018 and recently earned fourth NBA title of his career, including two with the Miami Heat. LeBron James seeking answers in death of sister of Cavaliers executive, a close friend |Des Bieler |November 5, 2020 |Washington Post 

A fourth suspect, a 26-year-old woman named Hayat Boumeddiene, remains at large. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

But in more middle-class and working-class neighborhoods, sessions are typically a fourth of that price. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

He won re-election twice as governor of New York, and had the hubris to run for a fourth term before being defeated in 1994. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 

He lost his bid for a fourth term to George Pataki that year. President Cuomo Would’ve Been a Lion |Jonathan Alter |January 2, 2015 |DAILY BEAST 

They constantly break the fourth wall, yelling and complaining to the cameramen. James Franco and Seth Rogen Get ‘Naked and Afraid’… And It’s Hilarious |Marlow Stern |December 8, 2014 |DAILY BEAST 

In treble, second and fourth, the first change is a dodge behind; and the second time the treble leads, there's a double Bob. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

Persistent glycosuria has been noted in brain injuries involving the floor of the fourth ventricle. A Manual of Clinical Diagnosis |James Campbell Todd 

Upon this he went to bed again, fell asleep, and dreamed a fourth time as before. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

He was rather silent, they observed; but the young clergyman, who made the fourth at the table, was voluble by nature. Elster's Folly |Mrs. Henry Wood 

Ordinarily the diazo appears a little earlier than the Widal reaction—about the fourth or fifth day—but it may be delayed. A Manual of Clinical Diagnosis |James Campbell Todd