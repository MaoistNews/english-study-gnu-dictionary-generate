Prime Hook is just one example of a restored coastal wetland. Soggy coastal soils? Here’s why ecologists love them |Alison Pearce Stevens |September 17, 2020 |Science News For Students 

For Nicholas Howley alone, that recovery restored hundreds of millions of dollars to his fortune. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 

The hope was, and still is, that a mass vaccination campaign can restore the world to normal. A third of Americans might refuse a Covid-19 vaccine. How screwed are we? |Brian Resnick |September 4, 2020 |Vox 

The purpose of the system is to drive us toward behaviors aimed at maintaining or restoring our natural state of balance. Why do you feel lonely? Neuroscience is starting to find answers. |Amy Nordrum |September 4, 2020 |MIT Technology Review 

A few weeks ago, we learned about the Twisted NAS-E, a restored and modified Land Rover Defender that now runs on electrons instead of gasoline or diesel. Now you can get a Land Rover restomod with a Tesla electric powertrain |Jonathan M. Gitlin |September 3, 2020 |Ars Technica 

Faal told the FBI that his group was trying “restore democracy to The Gambia and improve the lives of its people.” The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

The charismatic bearded revolucionario dressed in a dark olive uniform promised to restore order and hold elections. Cuba Is A Kleptocracy, Not Communist |Romina Ruiz-Goiriena |December 19, 2014 |DAILY BEAST 

This would restore overtime rights to workers earning up to around $50,000 a year, which is roughly the current median. It’s Always Black Friday for Clerks |Michael Tomasky |November 28, 2014 |DAILY BEAST 

The Onna church is expected to take up to 12 years to restore. Madonna, Carla Bruni & Obama Abandoned Pledges To Rebuild L'Aquila After The Quake |Barbie Latza Nadeau |November 18, 2014 |DAILY BEAST 

Much of the money meant to restore the center and rebuild the houses has gone instead to relocate the residents. Madonna, Carla Bruni & Obama Abandoned Pledges To Rebuild L'Aquila After The Quake |Barbie Latza Nadeau |November 18, 2014 |DAILY BEAST 

But one thing remained for Felipe now, If Ramona lived, he would find her, and restore to her this her rightful property. Ramona |Helen Hunt Jackson 

Insult and outrage seemed to have given that bodily vigour to Ripperda, which medicine and surgery had taken no pains to restore. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

And he wished also to restore her to her natural setting, with the greatest degree of historic accuracy. Bastien Lepage |Fr. Crastre 

This was "assault" in truth, and oddly enough seemed to restore the victim to perfect coolness. Dorothy at Skyrie |Evelyn Raymond 

The new general had orders to arrest certain officers and restore discipline. Napoleon's Marshals |R. P. Dunn-Pattison