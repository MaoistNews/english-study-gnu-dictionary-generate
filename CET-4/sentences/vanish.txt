Previously unknown papyri crop up only to vanish into private collections and out of the sight of scholars forever. Dismembering History: The Shady Online Trade in Ancient Texts |Candida Moss |November 23, 2014 |DAILY BEAST 

There he claims he saw a luminous object change colors several times then vanish into the night sky. 1980: America’s First Extraterrestrial Election | |October 15, 2014 |DAILY BEAST 

After losing out on the WFP backing, “I know Andrew Cuomo just thought I would vanish,” Teachout said. Can New York Democrat Zephyr Teachout Stop Governor Andrew Cuomo? |David Freedlander |August 18, 2014 |DAILY BEAST 

This teacher says that the wretched Soviet regime will vanish and life will return to normal. Inside ‘Maidan’: Sergei Loznitsa on His Ukrainian Uprising Doc and Putin’s ‘Fascist’ Regime |Richard Porton |May 24, 2014 |DAILY BEAST 

But what made The Beatles and The Beach Boys so spectacular vocally was that they could vanish into each other with their voices. The Band’s ‘Rock of Ages’ Is the Greatest Live Album Ever |Andrew Romano |October 14, 2013 |DAILY BEAST 

And then he hung up, left the station to vanish into the murk of the rain swept night. Hooded Detective, Volume III No. 2, January, 1942 |Various 

But indeed, it was useless to rub her eyes, the dwarfs did not vanish, and so she was obliged to believe that they were real. Honey-Bee |Anatole France 

He remained on his feet just long enough to see his Time Observatory dim and vanish. The Man from Time |Frank Belknap Long 

I would have cried like a child who sees the castle he has been dreaming about vanish away as he awakens from sleep. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

And by the same token the good Duchess has seen her hair grow white and her gaiety vanish. Honey-Bee |Anatole France