All the downsides of popcorn but none of the good butter grease. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

Company-provided data show that while travelers are booking almost twice as many remote stays as last year, home rentals in urban markets—Airbnb’s bread and butter—are still struggling. Airbnb CEO: The pandemic will force us to see more of the world, not less |Verne Kopytoff |September 7, 2020 |Fortune 

A 19th-century Pennsylvania Dutch doctor’s manual instructs its reader to inscribe the square in butter smeared on a piece of bread and eat it as a cure for rabies. The ancient palindrome that explains Christopher Nolan’s Tenet |Alissa Wilkinson |September 4, 2020 |Vox 

The consumer piece was easier to fix—sell, don’t store, the butter. Land O’Lakes CEO Beth Ford explains why farmers need broadband |Ellen McGirt |August 18, 2020 |Fortune 

It is the large quantities of salt and the sodium in the butters that are used to season them that can lead to high blood pressure. Is It Time to Put Down Soul Food? |Tyler Brady |November 27, 2017 |TruthBeTold.news 

In the bowl of an electric mixer, cream the butter and brown sugar until light and fluffy. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

While the beans are cooling and drying, melt the butter in a saute pan over medium heat. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 

“Butter has always been a healthy part of the diet in almost every culture; butter is a traditional food,” Asprey says. Bulletproof Coffee and the Case for Butter as a Health Food |DailyBurn |December 27, 2014 |DAILY BEAST 

Now, his new book “The Bulletproof Diet,” claims to offer a weight loss solution that lets you have your butter, and eat it too. Bulletproof Coffee and the Case for Butter as a Health Food |DailyBurn |December 27, 2014 |DAILY BEAST 

By Amanda Woerner for Life by DailyBurn Butter is making a comeback—and it has nothing to do with Paula Deen. Bulletproof Coffee and the Case for Butter as a Health Food |DailyBurn |December 27, 2014 |DAILY BEAST 

The sailors sometimes use it to fry their meat, for want of butter, and find it agreeable enough. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

You see, they always butter their chairs so that they won't stick fast when they sit down. Davy and The Goblin |Charles E. Carryl 

The former, in its frozen state, somewhat resembled hard butter. The Giant of the North |R.M. Ballantyne 

He shall eat butter and honey, that he may know to refuse the evil, and to choose the good. The Bible, Douay-Rheims Version |Various 

Your electro-plated butter-dish, or whatever it's going to be, will be simply flung back at you. First Plays |A. A. Milne