Do be cautious, as a splatter of oil or a little too much sugar so close to the heating element can go up in flames — in which case it’s time to turn off the broiler and open a few windows. Don’t Fear the Broiler |Elazar Sontag |February 11, 2021 |Eater 

In a medium skillet over medium heat, heat the oil until shimmering. Chili-tinged sweet potatoes are loaded in all the right ways - flavor, nutrition and satisfaction |Ellie Krieger |February 11, 2021 |Washington Post 

When it did, I’d rub its skin with salt, cracked spices, plenty of oil, and lemon zest. Love, Loneliness, and the Chicken in My Freezer |Elazar Sontag |February 9, 2021 |Eater 

The last time the price of oil was this high, the US was about to confirm its first Covid-19 case. The price of oil is back to pre-pandemic levels |Tim McDonnell |February 8, 2021 |Quartz 

When the time comes to hop back on the saddle, all the necessary moving parts are already tuned and oiled. Why you never forget how to ride a bike |Hannah Seo |February 8, 2021 |Popular-Science 

The State Department found that with high oil prices, the tar sands would be mined for oil, pipeline or no. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

Pulling oil from the tar sands is costly, even more so when you tack transportation costs on top. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

The focus here was on how fast oil would come out of the Canadian fields. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

Total oil production figures include crude oil, natural gas liquids, and other liquid energy products. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

On top of oil, the United States produces significantly more natural gas than Saudi Arabia. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

In pursuing his alchemical researches, he discovered Prussian blue, and the animal oil which bears his name. The Every Day Book of History and Chronology |Joel Munsell 

Movement to know that she was attired in appropriate costume—short frock, biped continuations and a mannish oil-skin hat. Glances at Europe |Horace Greeley 

A very small amount may be present after ingestion of large quantities of cod-liver oil or other fats. A Manual of Clinical Diagnosis |James Campbell Todd 

Go carefully over the film with an oil-immersion lens, using a mechanical stage if available. A Manual of Clinical Diagnosis |James Campbell Todd 

The unindustrious pupil imagines that “p” represents 8, and not “f” or “v,” and translates 1845 into “To pour oil” . Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)