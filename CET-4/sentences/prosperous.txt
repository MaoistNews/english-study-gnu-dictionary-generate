Without a prosperous community around me, this is not going to be a prosperous company. Why Mastercard isn’t a credit card company, according to its outgoing CEO Ajay Banga |cleaf2013 |December 3, 2020 |Fortune 

Over the decades, it has charted the fortunes of Hong Kong, reflecting its ups and downs through prosperous and uncertain times alike. Hong Kong’s Hang Seng index is catching all of China’s FAANGs |Mary Hui |December 2, 2020 |Quartz 

They are wasting valuable rebuilding time pretending, with their hopes and forecasts and strains to envision a prosperous future if they do everything just right. The Wizards’ reboot needs to start with trading both John Wall and Bradley Beal |Jerry Brewer |November 23, 2020 |Washington Post 

Now is our opportunity to embrace pay equity fully and lay the foundation for a more prosperous, inclusive version of capitalism. American capitalism needs equal-pay legislation. Canada is showing us how to make it work |matthewheimer |October 12, 2020 |Fortune 

Both highlight the need for more education, training and awareness of our own actions at work and online to foster more successful, creative and prosperous companies, agencies and individuals in the search industry. SEOs say bias, discrimination are bigger problem within the industry than their companies [Report] |Ginny Marvin |October 8, 2020 |Search Engine Land 

Prosperous farms came into being in the meadows where the antelope had pastured. Where the Trail Divides |Will Lillibridge 

Prosperous until the advent of the pioneer, he little by little became poorer, and finally withdrew from business. Sixty Years in Southern California 1853-1913 |Harris Newmark 

Prosperous and adverse days appeared to visit the two communities almost in the same alternation. History of the Jews, Vol. I (of 6) |Heinrich Graetz 

Prosperous and well-governed States were ever in peril of invasion by barbarous peoples. Myths of Babylonia and Assyria |Donald A. Mackenzie 

Prosperous traders foreclosed them, the spirit of the times defeated them, young Liberals succeeded them in office. The Imperialist |(a.k.a. Mrs. Everard Cotes) Sara Jeannette Duncan