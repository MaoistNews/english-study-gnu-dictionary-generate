If acupuncture was provided after an LPS shock, then the mice showed even higher levels of inflammation. We Need New, Safer Ways to Treat Pain. Could Electroacupuncture Be One? |Shelly Fan |August 18, 2020 |Singularity Hub 

Oatley has demonstrated his technique in mice but faces challenges with livestock. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

Wang, who reported those results June 15 in Cell Research, says his team is now testing the compound in mice. How two coronavirus drugs for cats might help humans fight COVID-19 |Erin Garcia de Jesus |August 11, 2020 |Science News 

Further experiments found that disrupting the vCA1 cells when giving mice a shock disrupted the entire network—that is, the waves broke down and the mice forgot their fear. Towards ‘Eternal Sunshine’? New Links Found Between Memory and Emotion |Shelly Fan |July 28, 2020 |Singularity Hub 

Of course, it’s hard to ask a mouse if it actually smelled something, so the team took a roundabout approach. A Highway to Smell: How Scientists Used Light to Incept Smell in Mice |Shelly Fan |July 1, 2020 |Singularity Hub 

He said he spent his time doing “Mickey Mouse make-work,” digging though old records for long-abandoned well sites. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

So Western governments are caught in a cat-and-mouse game and at times it is unclear who is the cat and who the mouse. ISIS Has a Message. Do We? |Jamie Dettmer |December 8, 2014 |DAILY BEAST 

Even the original score to the song labels the singing parts, “Mouse” (the woman) and “Wolf” (the man). The Most WTF Covers of ‘Baby, It’s Cold Outside,’ Everyone’s Favorite Date-Rape Holiday Classic |Kevin Fallon |November 19, 2014 |DAILY BEAST 

The episode was titled “Cat and Mouse” and it follows in the pattern of classic Serlingesque plot twists. How a War-Weary Vet Created ‘The Twilight Zone’ |Rich Goldstein |November 13, 2014 |DAILY BEAST 

In the event, in the long cat and mouse game that Stalin played with him, the cat did not pounce. When Stalin Met Lady Macbeth |Brian Moynahan |November 9, 2014 |DAILY BEAST 

And Mr. Meadow Mouse often remarked that it had more halls than any other dwelling he had ever seen. The Tale of Grandfather Mole |Arthur Scott Bailey 

Some of those halls that Mr. Meadow Mouse mentioned ran right out beneath the surface of the garden. The Tale of Grandfather Mole |Arthur Scott Bailey 

Mr. Meadow Mouse repeated, as if he wanted to be sure there was no misunderstanding about it. The Tale of Grandfather Mole |Arthur Scott Bailey 

Grandfather Mole demanded of Mr. Meadow Mouse, almost as soon as he had stepped just outside the shade of the toadstool. The Tale of Grandfather Mole |Arthur Scott Bailey 

And when he had made his promise to Mr. Meadow Mouse he had had no idea that it was going to rain so soon. The Tale of Grandfather Mole |Arthur Scott Bailey