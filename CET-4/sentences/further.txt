Also, she was tall and thin, too, further adding to the ways she met the physical beauty conventions. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Her post-crown fame, though, only further begs the question: Why has there not been another Jewish Miss America since 1945? Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

In Uganda, legislators are considering further criminalization of LGBT advocacy and same-sex relationships. ‘Only God’ Can Stop Gay Marriage |Tim Mak |January 6, 2015 |DAILY BEAST 

Further, there are maintenance crews who have to fix those drones. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

After that speech, many of the students gathered around to talk further. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

Off went the officers again, some distance to the front, and then back again to their men, and got them on a little further. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The improvement of transport still further swelled the volume of production. The Unsolved Riddle of Social Justice |Stephen Leacock 

Harry had no further adventures in reaching Fulton, and at once reported to Captain Duffield, who was in command of the post. The Courier of the Ozarks |Byron A. Dunn 

He, therefore, did as he said; made no further observation, but conducted himself to his young friend with grave distance. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

As the next verse is the last you needn't trouble yourself to make any further observations. Davy and The Goblin |Charles E. Carryl