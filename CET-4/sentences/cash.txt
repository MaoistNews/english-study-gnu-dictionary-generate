The campaign must report its cash on hand for the end of August by Sunday’s regulatory deadline. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

For a little extra cash up front, it significantly increases the protection for your machine. Six ways to make your ATV even more rugged |By Tyler Freel/Outdoor Life |September 17, 2020 |Popular-Science 

In theory, CBD could be a cash cow for publishers’ commerce operations, thanks to exploding consumer interest in the category and audiences of stressed out people who have spent much of 2020 stuck in their homes. ‘An educational stance’: Publishers mull CBD’s alluring – and complex – commerce opportunities |Max Willens |September 17, 2020 |Digiday 

The company is not the only one that wants to parlay a cash transfer app into a banking empire—PayPal is rushing to do the same with its Venmo app. ‘Square is a beast’ |Jeff |September 16, 2020 |Fortune 

Today, more than half of the world’s 193 countries will trade citizenship or residency for cash. Covid-19 is shaking up the citizenship by investment industry |Annabelle Timsit |September 16, 2020 |Quartz 

One that they cannot cash in at the bank to pay for their flats. One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

Crain posted a cash bond of $102.50 apiece shortly before 1:30 P.M., and they returned to the Castle Hotel. How Martin Luther King Jr. Influenced Sam Cooke’s ‘A Change Is Gonna Come’ |Peter Guralnick |December 28, 2014 |DAILY BEAST 

Mister Ham in need of cash: That is something a lot of people will not believe. The Stacks: Sell the Overcoat, Keep the Dignity |Paul Hemphill |December 22, 2014 |DAILY BEAST 

Hitchcock was quick to add he didn't see the cash exchange hands, but he doesn't doubt it happened. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He pointed a pistol at a grocery checkout woman and swiped whatever cash he could into his pockets before booking it. Post Office Robbers More Wanted Than ISIS |M.L. Nestel |December 13, 2014 |DAILY BEAST 

Were you ever arrested, having in your custody another man's cash, and would rather go to gaol, than break it? The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

The “Compañia General de Tabacos” lost about ₱30,000 in cash in addition to the damage done to their offices and property. The Philippine Islands |John Foreman 

The promoters went his security and put up the cash into the bargain, and he went back to the publishing house victorious. The Homesteader |Oscar Micheaux 

His wife had sold her relinquishment on the claim that he had spent thirty-five hundred dollars cash for. The Homesteader |Oscar Micheaux 

It was very annoying—more than ever—to the Elder when he was required to put up twenty-five dollars in cash as a retainer. The Homesteader |Oscar Micheaux