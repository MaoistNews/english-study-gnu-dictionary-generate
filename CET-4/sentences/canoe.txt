Complimentary for guests, the Fairmont Kea Lani also offers outings on an outrigger canoe, captained by two locals. A Maui vacation in three acts |Alex Pulaski |February 12, 2021 |Washington Post 

Experience the park the way explorers did 250 years ago by renting a canoe and reserving one of the park’s many frontcountry or backcountry sites. Staring Down Fear in Voyageurs National Park |Emily Pennington |January 28, 2021 |Outside Online 

They can see how people migrated across islands and pinpoint when technologies like canoes emerged by tracking the emergence of seafaring terms. Local languages are dying out and taking invaluable knowledge with them |Hannah Seo |January 25, 2021 |Popular-Science 

Lower yourself into a long wooden canoe and glide past cascading waterfalls to the start of a dirt trail. Journey to the Center of the Earth |David Kushner |December 28, 2020 |Outside Online 

They didn’t have access to tetanus shots or antibiotics, and getting to the nearest hospital entailed a day’s journey by dugout canoe, followed by another on a motorboat and another in a car. The fight to stop the next pandemic starts in the jungles of Borneo |Brian Barth |December 2, 2020 |Popular-Science 

As a boy, by the way, Pierre had set out from Florida in an unsuccessful canoe trip to Cuba! Canada ♥ Cuba Just Got Complicated |Shinan Govani |December 22, 2014 |DAILY BEAST 

Our foreign policy canoe is filled to the gunnels with catch-and-release trout armed with AK-47s. My Coffee Klatch With Rand Paul |P. J. O’Rourke |September 27, 2014 |DAILY BEAST 

My partner Brandon and I awake at the crack of dawn for a canoe ride on the milky blue glacial waters of Lake Louise. Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 

So I go out in a canoe and repeat verses over and over and try and learn poems. ‘The Good Wife’s Christine Baranski on Life After Will Gardner’s Death |Jason Lynch |April 21, 2014 |DAILY BEAST 

That canoe he paddles in Parks, that episode where Ron is a canoe, Nick Offerman built that canoe, for real! Parks and Recreation’s Aziz Ansari Is 30 Years Old and Writing a Book About Modern Love |Abby Haglage |November 12, 2013 |DAILY BEAST 

A primitive savage makes a bow and arrow in a day: it takes him a fortnight to make a bark canoe. The Unsolved Riddle of Social Justice |Stephen Leacock 

The roomy canoe, if not built for great speed, certainly was built for as much comfort as could be expected in such a craft. The Campfire Girls of Roselawn |Margaret Penrose 

The canoe touched the grassy bank at the edge of the old Carter place at the far end of the lake just before noon. The Campfire Girls of Roselawn |Margaret Penrose 

But suddenly Jessie drove her paddle deep into the water and sent the canoe in a dash to the landing. The Campfire Girls of Roselawn |Margaret Penrose 

Her chum came leaping up the hill behind her, having moored the canoe with one hitch. The Campfire Girls of Roselawn |Margaret Penrose