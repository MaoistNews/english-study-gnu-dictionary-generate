Amazon on Thursday announced the first companies to receive money from a $2 billion venture capital fund it formed to help combat climate change. Podcast: Amazon exec on the company's Climate Pledge Fund |Dan Primack |September 17, 2020 |Axios 

That’s been the pattern for other recent high-profile, venture-capital backed IPOs. Investors in Snowflake’s IPO should prepare for a second-day slump |Oliver Staley |September 17, 2020 |Quartz 

This capital has helped to de-risk the fund’s investments, allowing it the flexibility to invest in firms that banks might turn away. Opportunity Zones haven’t fully reached their potential, but don’t write them off yet |jakemeth |September 16, 2020 |Fortune 

So the 11% drop in the valuation would leave investors with a 1% annual capital gain. Will tech stocks stumble or slide? What the fundamentals tell us |Shawn Tully |September 16, 2020 |Fortune 

Those arrangements offer businesses more flexibility with their capital. ‘Square is a beast’ |Jeff |September 16, 2020 |Fortune 

Gunshots rang out in Paris this morning on a second day of deadly violence that has stunned the French capital. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

The mistletoe must have been hanging right across the aisle on Capital Hill. Congress’ Gift That Keeps on Giving |P. J. O’Rourke |December 20, 2014 |DAILY BEAST 

The second major split between the capital and the court occurred over oral care. The French Court’s Royal Ban on Smiles |William O’Connor |December 14, 2014 |DAILY BEAST 

Ah, gay Paree—the French capital has practically announced its own LGBT friendliness since the Belle Époque. The Ultimate LGBT Travel Bucket List | |December 12, 2014 |DAILY BEAST 

And the capital city is a veritable utopia of acceptance and integration. The Ultimate LGBT Travel Bucket List | |December 12, 2014 |DAILY BEAST 

"Capital, capital," his lordship would remark with great alacrity, when there was no other way of escape. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

When we speak against one capital vice, we ought to speak against its opposite; the middle betwixt both is the point for virtue. Pearls of Thought |Maturin M. Ballou 

The moon rose on a terrified mob trudging or riding the forty miles of road between Meerut and the Mogul capital. The Red Year |Louis Tracy 

It is the quiet chef lieu of the Allier, and was once the capital of the Bourbonnais. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

All possibility of a general increase of wages depended on the relation of available capital to the numbers of the working men. The Unsolved Riddle of Social Justice |Stephen Leacock