It will be less remembered as a year featuring a diverse roster of scientific anniversaries, ranging from the 1300th birthday of a prolific writer to the 25th birthday of a celebrity sheep. Top 10 science anniversaries to celebrate in 2021 |Tom Siegfried |February 3, 2021 |Science News 

Merino wool is made from a variety of sheep known for its fine, soft fibers. The Kora Xenolith Is My Secret Weapon Against the Cold |Wes Siler |February 2, 2021 |Outside Online 

Around 3,000 years ago a drought in central China drove Mongol herders a thousand miles north into the steppes of Khakassia, in Siberia, where they remained raising horses and sheep for centuries. The Big Thaw: How Russia Could Dominate a Warming World |by Abrahm Lustgarten, photography by Sergey Ponomarev |December 16, 2020 |ProPublica 

And, also like Orwell, he diverged from his social set, avoiding college for a career as a sheep farmer in South Africa … even though all he ever wanted was to be a writer. The Mild-Mannered Poet Who Championed James Bond |Fiona Zublin |November 30, 2020 |Ozy 

Merino sheep, to be precise, and the wool that is shorn from them in New Zealand that ends up in the comfy sneaker-slippers made by Allbirds. A woolly story about how Allbirds makes its shoes |Adam Lashinsky |September 22, 2020 |Fortune 

You will find winding pasture for sheep and highland cattle. Ester Elchies, The Estate Built By Whiskey | |December 10, 2014 |DAILY BEAST 

I had to pause for sheep crossing the road, which is a common occurrence when driving through the Highlands of Scotland. A Whisky Connoisseur Remembers That First Sip of The Macallan | |December 10, 2014 |DAILY BEAST 

Farmers, fearing ISIS attacks, have left the city with their sheep. Remembering Kobani Before The Siege |Mustafa Abdi, Movements.Org, Advancing Human Rights |November 8, 2014 |DAILY BEAST 

He notes that on Naxos sheep have very large gallbladders, but on Euboea they do not. Why Aristotle Deserves A Posthumous Nobel |Nick Romeo |October 18, 2014 |DAILY BEAST 

The Ivy League and other top schools are producing no more than ‘excellent sheep,’ says William Deresiewicz. The Elite American College Pile-On |Michael S. Roth |September 15, 2014 |DAILY BEAST 

A flock of weary sheep pattered along the road, barnward bound, heavy eyed and bleating softly. The Soldier of the Valley |Nelson Lloyd 

Harry took his bed into the spare-room, and Black Sheep lay down to die. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

The Griffin carried off one sheep a day from a German village, till a man came with a "falchion" and split the Griffin open. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

A little corral for the sheep, and a rough shed for the pony, and the home was complete: far the prettiest home they had ever had. Ramona |Helen Hunt Jackson 

And with these, and the society of Jane on board-wages, Black Sheep was left alone for a month. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling