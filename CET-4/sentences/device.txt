Rothamer and colleagues tested devices called mask fitters or mask braces — rubber or plastic frames that fit over the mask molding it more closely to the face. Making masks fit better can reduce coronavirus exposure by 96 percent |Tina Hesman Saey |February 12, 2021 |Science News 

Today he knows that magic was simply the diversity of a forest left to its own devices. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

That’s not surprising, after all, 85% of all internet users in the United States watched online video content monthly on any of their devices. A comprehensive guide on using Google Trends for keyword research |Aayush Gupta |February 12, 2021 |Search Engine Watch 

In others, we get impatient and set off explosive charges or use a powerful sound-making device. Using whale songs to image beneath the ocean’s floor |John Timmer |February 11, 2021 |Ars Technica 

New platforms will include slip-resistant tiles, LED lighting, larger digital displays with arrival information and service alerts, stainless-steel shelters, improved surveillance systems and charging ports for riders’ digital devices. Two Blue Line Metro stations to close through late May for platform work |Justin George |February 11, 2021 |Washington Post 

I still do find it a tremendously useful device to invent a character and have the character sing the song. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

Employees strap a device to their heads and power a helicopter drone with their minds. Use Your Brain—Control a Drone |The Daily Beast Video |January 5, 2015 |DAILY BEAST 

The extending out of one syllable is a great songwriting device. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

They double down on the plot device of a lone visionary opposed by conventional hierarchies. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

So filmmakers usually resort to a plot device to compensate for this absence. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

This takes at first the crude device of a couple of vertical lines attached to the head (see Fig. 4). Children's Ways |James Sully 

That was not the device of a woman who loved—it savored rather of the cool state-craft of a Lucrezia Borgia. The Red Year |Louis Tracy 

This would be a device for helping him to revive this hitherto unrecallable name. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

By a device resorted to in each separate case to help make a more vivid First Impression. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

It is the fashion there to regard it merely as a device to help an incompetent organist. The Recent Revolution in Organ Building |George Laing Miller