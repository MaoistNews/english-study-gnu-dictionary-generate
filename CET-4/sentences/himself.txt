On Thursday, Garcetti ruled himself out of the race to succeed Boxer. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

In the first episode, an officer is shown video of himself shooting and killing a man. 'Babylon' Review: The Dumb Lives of Trigger-Happy Cops |Melissa Leon |January 9, 2015 |DAILY BEAST 

He sees himself as the first Muslim president of all Europe. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Houellebecq shows himself to be perfectly disgusted with humanity. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

The third suspect, an 18-year-old named Hamyd Mourad, who turned himself in, is part of the same extended family. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

And he was gone, and out of sight on the swift galloping Benito, before Father Gaspara bethought himself. Ramona |Helen Hunt Jackson 

He distinguished himself in several campaigns, especially in the Peninsular war, and was raised to the rank of field marshal. The Every Day Book of History and Chronology |Joel Munsell 

In the meantime, the outlaw, having observed how much more cordially the tyrant is received than himself, has made his exit. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

After all, may not even John Burns be human; may not Mr. Chamberlain himself have a heart that can feel for another? God and my Neighbour |Robert Blatchford 

Aristide washed and powdered Jean himself, the landlord lounging by, pipe in mouth, administering suggestions. The Joyous Adventures of Aristide Pujol |William J. Locke