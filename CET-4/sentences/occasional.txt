While occasional conflicts do arise between coyotes and pets, it’s rare for there to be an issue with humans. Four wild animals that are thriving in cities |By Ryan Chelius/Outdoor Life |February 9, 2021 |Popular-Science 

Huge protests led to occasional violence and a renewed attention on systemic societal problems. A brutal, isolating year leads to baffling battles between good and evil |Philip Bump |February 4, 2021 |Washington Post 

The second in an occasional series about the books that spurred our love of travel. In ‘The Whispering Land,’ a British naturalist collects travel tales — and animals with tails — in Argentina |Andrea Sachs |February 4, 2021 |Washington Post 

Micro spikes are great if most of the run will be icy but are overkill if I’m just traversing the occasional slick patch. The Gear Our Editors Loved in January |The Editors |February 3, 2021 |Outside Online 

The Helix would include an artists-in-residence program and on occasional weekends would open its doors to the public. Amazon unveils Helix building as heart of campus in Arlington |Fredrick Kunkle |February 2, 2021 |Washington Post 

By which I mean, there are some Jews and the occasional Hindu or Muslim. In Florida, ’Tis The Season for Satan |Jay Michaelson |December 7, 2014 |DAILY BEAST 

Contrary to what you may assume about me, I actually enjoy the occasional trip to the mall. It’s Always Black Friday for Clerks |Michael Tomasky |November 28, 2014 |DAILY BEAST 

Occasional, more traditional efforts have met with success—the Big Apple Circus and lingering smaller acts. We’re All Carnies Now: Why We Can’t Quit the Circus |Anthony Paletta |November 27, 2014 |DAILY BEAST 

These days they are occasional meteorological irruptions, white river mists, not dense and toxic industrial pea-soupers. Sherlock Holmes Vs. Jack the Ripper |Clive Irving |November 16, 2014 |DAILY BEAST 

And, again, I now confess, I had an occasional Bourbon and stoogie on the cuff. Up to a Point: In Defense of Lobbyists |P. J. O’Rourke |October 25, 2014 |DAILY BEAST 

Feeling secure regarding their happiness and welfare, she did not miss them except with an occasional intense longing. The Awakening and Selected Short Stories |Kate Chopin 

The sediment contains a few hyaline and finely granular casts and an occasional red blood-cell. A Manual of Clinical Diagnosis |James Campbell Todd 

In that part, it was little better than a morass, from the occasional overflowing of the waters at the rainy seasons. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

American vessels made occasional trips outside the Bay, and brought in captive sailing-vessels. The Philippine Islands |John Foreman 

Long before we got there the deep-throated thunder was growling over us, and the clouds spat occasional flurries of rain. Raw Gold |Bertrand W. Sinclair