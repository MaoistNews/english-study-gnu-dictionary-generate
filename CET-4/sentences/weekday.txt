I know I shouldn’t sleep with my phone, but I do, and because it’s right there, the very first thing I do every weekday is read the Quartz Daily Brief. Introducing new emails for Quartz members |Katherine Bell |August 1, 2021 |Quartz 

Every weekday, she would set off with him strapped in the front row of the bus. What Philadelphia Reveals About America’s Homicide Surge |by Alec MacGillis, photography by Hannah Price/Magnum Photos, special to ProPublica |July 30, 2021 |ProPublica 

The Live Concert Series on Wilson Plaza, which runs through the week before Labor Day, features entertainers every weekday at noon. Where to find free outdoor concerts in the D.C. area |Fritz Hahn |July 23, 2021 |Washington Post 

By the time my now husband and I tried to reschedule, every summer weekend and most summer weekdays were gone. 2021 Could Be the Biggest Wedding Year Ever. But Are Guests Ready to Gather? |Eliana Dockterman |May 29, 2021 |Time 

On a recent weekday, many local residents were leery of talking about their town and the situation with police. Hurting in Windsor: How a viral police pepper-spray incident has illuminated racial tensions in a small Virginia town |Gregory S. Schneider |April 15, 2021 |Washington Post 

A couple years ago, I was walking one weekday morning down Court Street in Cobble Hill, Brooklyn and came upon Robin Williams. When I Met Robin Williams in Afghanistan |Matthew Kaminski |August 20, 2014 |DAILY BEAST 

In Washington D.C., he could ride an 80 bus to the Capitol, early on a weekday morning. What Paul Ryan Gets Wrong About ‘Inner-City’ Poverty |Jamelle Bouie |March 12, 2014 |DAILY BEAST 

To beat the crowds, booking a ticket for a weekday afternoon is the best bet. Digital Nomad Andrew Evans’s Six Top Travel Tips |Nina Strochlic |March 7, 2013 |DAILY BEAST 

Every weekday morning at 7:45 a.m., I make my 11-year-old daughter's school lunch. The Global School Lunch Project |David Frum |December 18, 2012 |DAILY BEAST 

Every weekday morning we will ask readers of the blog to weigh in on our Daily Poll on Facebook. Introducing the Daily Poll |Ryan Prior |February 27, 2012 |DAILY BEAST 

These adornments were of course for Sunday wear; no weekday clothes were worn on Sundays then. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The old gentleman, her father, I have heard, used to atone for his weekday sins with his Sunday devotions. The Awakening and Selected Short Stories |Kate Chopin 

It was an immense luxury to everybody, this lying in bed just beyond the ordinary time on a weekday. Sons and Lovers |David Herbert Lawrence 

He had said he would, but had dawdled skillfully and was still unfitly in bare feet and the shabby garments of a weekday. The Wrong Twin |Harry Leon Wilson 

Religion thus becomes a Sunday business, and Sunday business is kept separate from weekday business. Herein is Love |Reuel L. Howe