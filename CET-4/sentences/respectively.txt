The units offer 400 and 1,700 inch-pounds of torque, respectively, which translates to all the power you’ll need for every household task big and small. The best, most practical Valentine’s Day gifts for any kind of partner |PopSci Commerce Team |February 8, 2021 |Popular-Science 

Shortly after, Del Taco and Burger King added Beyond and Impossible products to their menus, respectively. Is Fast Food Healthier When It's Plant-Based? |Christine Byrne |February 3, 2021 |Outside Online 

All-Pro defenders Aaron Donald and Jalen Ramsey turn 30 and 27 this year, respectively. The Rams And Lions Both Got What They Wanted |Ty Schalter |February 1, 2021 |FiveThirtyEight 

The Associated Press reported the two men who are 27- and 29-years-old respectively were each caned 77 times in the province’s capital of Banda Aceh under Shariah law. Two men in Indonesia’s Aceh province caned for having sex |Michael K. Lavers |January 30, 2021 |Washington Blade 

Yoo Jae-Myung and Yang Kyung-Won, who had standout performances in last year’s Itaewon Class and Crash Landing on You, respectively, also star in this series. The 15 Most Anticipated Korean Dramas of 2021 |Kat Moon |January 29, 2021 |Time 

Of the three nominated, Webster did the best, receiving 12 votes, Gohmert and Yoho received three and two votes, respectively. Democrats Accidentally Save Boehner From Republican Coup |Ben Jacobs, Jackie Kucinich |January 6, 2015 |DAILY BEAST 

In 2008 and 2012, Huckabee and Santorum, respectively won the Iowa Caucus, but did not make it to the finish line. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

Buffalo ranked tenth in the nation, while Detroit and Pittsburgh ranked twelth and thirteenth, respectively. The Rustbelt Roars Back From the Dead |Joel Kotkin, Richey Piiparinen |December 7, 2014 |DAILY BEAST 

The difference is nonetheless drastic: $220,119 and $44,453, respectively, on average per person annually. Medicaid Will Give You Money for At-Home Care, but You Might Wait Years |Elizabeth Picciuto |December 2, 2014 |DAILY BEAST 

Jolly and Creten, who are both married to Monster Jam drivers (Neil Elliott and Jimmy Creten, respectively), have kids. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

This organ comprised no less than ninety-five ranks of Mixture, including two stops of twenty-one and twenty ranks, respectively. The Recent Revolution in Organ Building |George Laing Miller 

The original documents (in duplicate) are drawn up in Spanish and in English respectively. The Philippine Islands |John Foreman 

These divisions represent, respectively: "Foundation," "wood wind," "string" and "brass." The Recent Revolution in Organ Building |George Laing Miller 

This road was joined at Canterbury by two others, proceeding respectively from Lympne and Reculver. The Towns of Roman Britain |James Oliver Bevan 

The larger revenues of the primates, he said, were to be reduced respectively to the amounts of 10,000 and 8000 per annum. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan