That statue — with its inscribed tribute to Confederate soldiers “who, in defence of rights they believed sacred, took up arms against the invaders of Virginia” — gave him a chill every time he passed it. The Confederacy’s final resting place |Marc Fisher |May 30, 2021 |Washington Post 

Dennis recorded a single with John Lennon to raise money for a legal defence fund. Felix Dennis, Counter-Cultural Icon and Publisher Who Spent $100m on Fast Living, Dies |Tom Sykes |June 23, 2014 |DAILY BEAST 

In Estonia, Prince Harry will thank the Estonian Defence Forces for their support of British troops in Afghanistan. Cressida Wedding Summit |Tom Sykes |April 4, 2014 |DAILY BEAST 

Well, the defence team of Britain's most famous publicist Max Clifford came out - swinging? This Just In: Max Clifford's Penis Is "Certainly Not Enormous" |Tom Sykes |March 26, 2014 |DAILY BEAST 

Similar attacks then followed against the Latvian Ministry of Defence and Defence Forces sites. The Return of Russian Hard Power? |Michael Weiss |November 23, 2013 |DAILY BEAST 

Hats off the Guardian, Britain's most thoughtful left-wing newspaper, for running a piece today in defence of Prince Charles. Guardian Writer Defends Prince Charles |Tom Sykes |September 5, 2013 |DAILY BEAST 

During this defence, the Empress frequently shook her head; and when it was finished, she rose from her chair. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

She possessed also a great and philosophic mind, and wrote an able defence of Locke. The Every Day Book of History and Chronology |Joel Munsell 

This of course must be laid to the credit of the local supporters of "the noble art of self-defence," the Brummagem bruisers. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

No more admirable illustration can be found of the truth that the essence of defence lies in a vigorous local offence. Napoleon's Marshals |R. P. Dunn-Pattison 

The Turks fired a mine under Quinn's Post and then rushed a section of the defence isolated by the explosion. Gallipoli Diary, Volume I |Ian Hamilton