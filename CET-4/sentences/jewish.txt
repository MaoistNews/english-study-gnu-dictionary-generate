Soon after, Lewis’ mother packed up the family and moved to the predominantly Jewish and immigrant Maxwell Street area on the Near West Side. The Murder Chicago Didn’t Want to Solve |by Mick Dumke |February 25, 2021 |ProPublica 

In her personal training business, Lawson had Jewish clients and a Jewish girlfriend. Making Reconstructing Judaism work for all |Kathi Wolfe |February 18, 2021 |Washington Blade 

It would be a game-changer for a huge segment of the Northern Virginia Jewish community. Faith leaders criticize Fairfax school board for delaying vote on adding four religious holidays to calendar |Hannah Natanson |February 10, 2021 |Washington Post 

For 10 days, she went about her normal life, including attending a Jewish shiva gathering at a Rockville retirement community. 900,000 infected. Nearly 15,000 dead. How the coronavirus tore through D.C., Maryland and Virginia. |Rebecca Tan, Antonio Olivo, John D. Harden |February 5, 2021 |Washington Post 

In other words, the long-term problem isn’t that Greene had once claimed that a laser controlled by a Jewish cabal had caused forest fires in California to build a high-speed rail system. The Republican Party divide is not about politics. It’s about culture. |Philip Bump |February 4, 2021 |Washington Post 

“I have to think her body type played a role,” said Rachel Greenblatt, a Lecturer in Jewish Studies at Harvard University. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Then came Bess Myerson, a daughter of Russian-Jewish immigrants who was raised in the Sholem Aleichem Houses in the Bronx. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Even the hot Jewish women I mentioned above did something a bit more “intellectual” than pageantry: acting. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

It marked a groundbreaking moment in how the country viewed Jews, especially Jewish women. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Like most Jewish mothers, Myerson thought her daughter could do better. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

No Jewish historian nor scientist mentioned the rending of the veil of the temple, nor the rising of the saints from the dead. God and my Neighbour |Robert Blatchford 

God invites all to keep his commandments: the Gentiles that keep them shall be the people of God: the Jewish pastors are reproved. The Bible, Douay-Rheims Version |Various 

Had this stupendous miracle no effect upon the Jewish priests who had crucified Christ as an impostor? God and my Neighbour |Robert Blatchford 

Bonaparte issued a decree calling an assembly of Jewish deputies, for the purpose of forming a Sanhedrim. The Every Day Book of History and Chronology |Joel Munsell 

Special classes have been opened at the gymnasium for the religious instruction of Jewish pupils. Prison Memoirs of an Anarchist |Alexander Berkman