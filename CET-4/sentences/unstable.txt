Only Colbert can work through a “Your internet connection is unstable” warning popping up during a Zoom interview. One Good Thing: Stephen Colbert is looser, funnier, and angrier in quarantine |Emily VanDerWerff |September 4, 2020 |Vox 

There’s a long history of rural areas relying on one unstable source of income and on the desirability of natural resources. The Recreation Economy Isn't As Resilient As We Thought |Heather Hansman |August 29, 2020 |Outside Online 

After that, spring would bring unstable ice, rising waters, and unsuitable conditions to trap fish owls. The quest to snare—and save—the world’s largest owl |Jonathan Slaght |August 28, 2020 |Popular-Science 

If the universe turns out to be fundamentally unstable, a tiny bubble of the cosmos could convert to a more stable state. ‘The End of Everything’ explores the ways the universe could perish |Emily Conover |August 4, 2020 |Science News 

If the voltage goes too high or too low, the water itself becomes unstable. Batteries should not burst into flames |Carolyn Wilke |April 16, 2020 |Science News For Students 

Most of the think tanks thought that the situation in eastern regions of Ukraine would remain unstable for a long time. Putin and Poroshenko Agree to… Think About a Ukraine Ceasefire |Anna Nemtsova |September 3, 2014 |DAILY BEAST 

However, as they enter the current, the boat becomes unstable. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

You lethargic, unfocused, unstable, lazy, hazy, crazy time of year. Our Doomed Love Affair with Summer |P. J. O’Rourke |August 30, 2014 |DAILY BEAST 

Several people from the neighborhood told The Daily Beast the man was mentally unstable. Missouri Braces for a New Michael Brown |Justin Glawe |August 19, 2014 |DAILY BEAST 

The late 1960s proved to be the most civically unstable since the 1860s. A Brief History of Wingnuts in America; From George Washington to Woodstock |John Avlon |August 17, 2014 |DAILY BEAST 

Hence Napoleon was driven more and more to trust to the advice of the rash, unstable King of Naples. Napoleon's Marshals |R. P. Dunn-Pattison 

Industrial society is therefore mobile, elastic, standing at any moment in a temporary and unstable equilibrium. The Unsolved Riddle of Social Justice |Stephen Leacock 

The bliss of lovers is so unstable, that in every case lovers have more woes than the moon has changes. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

To me it is simply a medium, an unstable, oscillating medium of impetuous spiritual energies. The Creators |May Sinclair 

As a jailer he was in close touch with facts and knew by experience how unstable in these days was any man's power. The Light That Lures |Percy Brebner