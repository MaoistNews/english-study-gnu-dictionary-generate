One of these stones is decorated with a boar lying on its back. An Ancient Site with Human Skulls on Display - Issue 89: The Dark Side |Jo Marchant |September 2, 2020 |Nautilus 

Stonehenge today includes 63 complete stones, including 17 standing sarsen stones in the outer circle. Stonehenge enhanced sounds like voices or music for people inside the monument |Bruce Bower |August 31, 2020 |Science News 

We used to sit around his stone fireplace, chatting about everything under the sun, wine flowing and Leonard Cohen playing somewhere in the background. Investors ride the Big Tech rally even as COVID cases and unemployment spike |Bernhard Warner |August 21, 2020 |Fortune 

Instead, the focus is on medical conditions that astronauts have a fairly high chance of developing, like rashes or kidney stones. What will astronauts need to survive the dangerous journey to Mars? |Maria Temming |July 15, 2020 |Science News 

The stones help squish food so a creature doesn’t have to grind up everything in its mouth. Fossil stomach reveals a dinosaur’s last meal |Carolyn Wilke |July 7, 2020 |Science News For Students 

My body used for his hard pleasure; a stone god gripping me in his hands. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

“The US cannot tolerate the idea of any rival economic entity,” Stone writes. Oliver Stone’s Latest Dictator Suckup |James Kirchick |January 5, 2015 |DAILY BEAST 

Accusing his opponents of being locked in a Cold War mind-set, it is Stone who is beholden to old orthodoxies. Oliver Stone’s Latest Dictator Suckup |James Kirchick |January 5, 2015 |DAILY BEAST 

That Stone would slander the democratic, pro-Western, EuroMaidan revolution as a CIA coup is no surprise. Oliver Stone’s Latest Dictator Suckup |James Kirchick |January 5, 2015 |DAILY BEAST 

Mullins quotes Stewart from an interview with Rolling Stone. David Gregory's 'Meet the Press' Eviction Exposed in Washingtonian Takedown |Lloyd Grove |December 23, 2014 |DAILY BEAST 

That evening in the gondola, with one old and two newer friends, is marked with a white stone in my recollection. Glances at Europe |Horace Greeley 

Monsieur,” growls the baron, “stone walls have ears, you say if only they had tongues; what tales these could tell! Checkmate |Joseph Sheridan Le Fanu 

A colossal steam "traveller" had ceaselessly carried great blocks of stone and long steel girders from point to point. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The clink of the stone-masons' chisels had resounded year after year from morning till night. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

A few small rocks of some soft stone may be added, and in between these the Ferns are planted. How to Know the Ferns |S. Leonard Bastin