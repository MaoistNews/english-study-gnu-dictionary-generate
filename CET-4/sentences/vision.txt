In a conversation hosted by the Carnegie Endowment Thursday, Oversight Board co-chair and former Prime Minister of Denmark Helle Thorning-Schmidt painted a more expansive vision for the group that could go beyond making policy decisions for Facebook. Facebook Oversight Board says other social networks ‘welcome to join’ if project succeeds |Taylor Hatmaker |February 11, 2021 |TechCrunch 

It’s not a flying car, but it is a tasty vision of the future. Best toaster: Get perfectly golden slices every time |PopSci Commerce Team |February 10, 2021 |Popular-Science 

In other words, it called for a new vision of community safety that included “defunding” the police. What ‘defund the police’ really means |Simon Balto |February 9, 2021 |Washington Post 

Female founders are also more likely to get detailed questions focused on early-stage issues, such as breakeven points, or daily users, while men are more likely to get questions about their vision or opportunities. Bumble gave women more power in dating. Now the app is giving women power in the boardroom. |Jena McGregor |February 5, 2021 |Washington Post 

According to former Doubleday editor Patrick LoBrutto, he and Tevis worked and tirelessly reworked this dystopian vision of drugs and television as escapes from real life. ‘The Queen’s Gambit’ is a bestseller, but its author, Walter Tevis, was hardly a one-hit wonder |Michael Dirda |February 3, 2021 |Washington Post 

And in order for them to realize their vision, they are willing to use any means. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

Seeing what they were doing, I was inspired to add my vision to their technique. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

Another member of the plot took care of the ammo along with black uniforms, night-vision equipment, and body armor. The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

Mr. Bachner said it had been hard to introduce his work ethic and share his vision with the locals and his team. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

“One of the challenges is to get the weavers to see my vision,” Mr. Bachner said. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

The vision—it had been an instantaneous flash after all and nothing more—had left his mind completely for the time. The Wave |Algernon Blackwood 

The vision of dreams is the resemblance of one thing to another: as when a man's likeness is before the face of a man. The Bible, Douay-Rheims Version |Various 

Alford speaks of this as remarkable; but vision is the especial promise of Wisdom, therefore of Solomon, son of David. Solomon and Solomonic Literature |Moncure Daniel Conway 

A quick vision of death smote her soul, and for a second of time appalled and enfeebled her senses. The Awakening and Selected Short Stories |Kate Chopin 

It was Ezechiel that saw the glorious vision, which was shewn him upon the chariot of cherubims. The Bible, Douay-Rheims Version |Various