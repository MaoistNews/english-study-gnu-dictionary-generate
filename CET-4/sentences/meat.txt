Indeed, data suggests many young consumers are already incorporating plant-based meats into their meals. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

They didn’t necessarily have milk or meat or a bakery in-house. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

Efforts to curb wild meat consumption in response to disease outbreaks can also harm local populations who rely on wild meats for proteins and decrease trust in public health, which happened in the 2015 Ebola outbreak. To prevent the next pandemic, we might need to cut down fewer trees |Jonathan Lambert |July 23, 2020 |Science News 

In terms of the food supply, the FDA has detected varying levels of antibiotic-resistant bacteria in meat since monitoring began in 1996. 50 years ago, scientists first investigated antibiotic resistance in livestock |Jonathan Lambert |June 26, 2020 |Science News 

There, as people got more money, some of them may have more cleared land for cattle to raise for meat, Ferraro says. How giving cash to poor families may also save trees in Indonesia |Megan Sever |June 12, 2020 |Science News 

The smell of grilled meat mixes with the exotic wafts of cinnamon tea served with a mush of sweet brown dessert. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

Limbaugh makes comments like this because his right-wing fans require a non–stop diet of race-baiting red meat. Rush Limbaugh’s Fear of a Black James Bond |Dean Obeidallah |December 29, 2014 |DAILY BEAST 

Chickens require significantly less land, water, and energy than all other meat options except farmed salmon. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

In March, police arrested a group of wealthy businessmen and government officials who were about to dine on illegal tiger meat. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

Those raised for their meat (200 million tons of it a year) can barely walk because their breasts have been so enlarged. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

The sailors sometimes use it to fry their meat, for want of butter, and find it agreeable enough. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

The camp grew still, except for the rough and ready cook pottering about the fire, boiling buffalo-meat and mixing biscuit-dough. Raw Gold |Bertrand W. Sinclair 

A few years back it was partly turned into a depot for American meat, but is now simply used for warehouses. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

You know the fable about the dog who dropped his meat in the water, trying to snap at its reflection? Rosemary in Search of a Father |C. N. Williamson 

While this was being done, Ramona would dry the beef which would be their supply of meat for many months. Ramona |Helen Hunt Jackson