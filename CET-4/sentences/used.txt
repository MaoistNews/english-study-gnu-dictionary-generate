That strategy has been used in some cases to help determine GMO policy. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Like many trans users, Transartist often gets used as a source of information more than anything else. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

The new information consisted of Internet protocol addresses that Comey said are “exclusively used” by North Korea. Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 

My body used for his hard pleasure; a stone god gripping me in his hands. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

Morris struggled to straighten his back, which involved stiffening a spine rarely used. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

He alludes to it as one of their evil customs and used by them to produce insensibility. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Then there was Wee Wo,—he was a little Chinese chap, and we used to send him down the chimneys to open front doors for us. Davy and The Goblin |Charles E. Carryl 

Other things being equal, the volume of voice used measures the value that the mind puts upon the thought. Expressive Voice Culture |Jessie Eldridge Southwick 

The badness of the gunpowder used by the Mexicans, was again of great service to us. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

It was afterwards used as a schoolroom in connection with Winfield's factory. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell