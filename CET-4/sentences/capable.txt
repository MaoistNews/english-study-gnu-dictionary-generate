Family stories are funny things, though, capable of morphing and shifting over the years until the unknowns outnumber the knowns. He knew his grandfather was a mob boss. But was that the whole story? |Joe Heim |February 12, 2021 |Washington Post 

Technology capable of changing your brain’s activity with just a subtle nudge, however, brings “manipulation risks to the next level,” Ienca says. New technology can get inside your head. Are you ready? |Laura Sanders |February 11, 2021 |Science News For Students 

Technology capable of changing your brain’s activity with just a subtle nudge, however, “brings current manipulation risks to the next level,” Ienca says. Can privacy coexist with technology that reads and changes brain activity? |Laura Sanders |February 11, 2021 |Science News 

It’s also become evident that children are capable of transmitting the virus to some extent. Fauci: Vaccines for Kids as Young as First Graders Could Be Authorized by September |by Caroline Chen |February 11, 2021 |ProPublica 

That meant populous Fairfax County, which with the Inova hospital system had set up a network of sites capable of delivering at least 30,000 doses weekly, was positioned to scoop up more than half of Virginia’s weekly vaccine allocation. Confusion and chaos: Inside the vaccine rollout in D.C., Maryland and Virginia |Julie Zauzmer, Gregory S. Schneider, Erin Cox |February 9, 2021 |Washington Post 

At that point, who knows what they could have been capable of. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 

Hollywood might possibly fear North Korean sleeper cells capable of blowing up theaters that screen anti-Nork films. Pyongyang Shuffle: Hollywood In Dead Panic Over Sony Hack |James Poulos |December 19, 2014 |DAILY BEAST 

Because especially my sister is not capable of doing the stuff that he is accusing her of doing. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

Researchers in subsequent decades have indeed documented the violence, sexual and otherwise, that these birds are capable of. Lovable ‘Madagascar’ Penguins Are Known to Rape and Torture in Real Life |Asawin Suebsaeng |November 26, 2014 |DAILY BEAST 

Designed for “special missions,” the privately owned company is capable of transporting precious cargo anywhere in the world. The American Ebola Rescue Plan Hinges on One Company. Meet Phoenix. |Abby Haglage |November 22, 2014 |DAILY BEAST 

Nowhere can be found a region capable of supporting a larger population to the square mile than Lombardy. Glances at Europe |Horace Greeley 

I should pay a capable secretary like you—knowing several languages and all that—say forty dollars a week. Rosemary in Search of a Father |C. N. Williamson 

She did not realize that a passion for a business enterprise, as for a woman, is capable of destroying the balance of any man. Hilda Lessways |Arnold Bennett 

Aunt Ri gazed at her with a sentiment as near to veneration as her dry, humorous, practical nature was capable of feeling. Ramona |Helen Hunt Jackson 

He is shining black, and as he tosses his head one can see the wicked horns, capable of doing such terrible injury. Alila, Our Little Philippine Cousin |Mary Hazelton Wade