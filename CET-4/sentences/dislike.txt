What’s more, Cape had a passionate dislike for fictional thrillers, which Plomer enjoyed. The Mild-Mannered Poet Who Championed James Bond |Fiona Zublin |November 30, 2020 |Ozy 

Like Sam-I-Am and his dislike of green eggs and ham, dunning him for an entire lifetime to stick something, anything, in your rear end is an unseemly chore. Sex That You Will Never Have Under ANY Conditions |Eugene Robinson |November 29, 2020 |Ozy 

Tatsiana Maskalevich, a data scientist at clothing retailer Stitch Fix, calls the information its clients give the company—their size, age, likes and dislikes, and so on—“a data scientist’s goldmine.” “A.I. is a living and breathing engine” |Adam Lashinsky |September 24, 2020 |Fortune 

Many on the right dislike stakeholder capitalism because they fear it undercuts the economic discipline of shareholder governance. Stakeholder capitalism isn’t a choice |Alan Murray |August 24, 2020 |Fortune 

On June 24, US Department of Justice whistleblower John Elias testified that attorney general William Barr ordered investigations of 10 cannabis mergers in the 2019 fiscal year based on personal dislike of the industry. Does the cannabis industry have a monopoly problem? |Jenni Avins |July 5, 2020 |Quartz 

The Republicans he believes are driven at least in part by their intense dislike of Obama. Repubs Should Take It From Kucinich: Impeachment Isn’t Worth It |Eleanor Clift |December 5, 2014 |DAILY BEAST 

The more you try to force your beliefs on others, the more people dislike you. Dear Evangelicals: You’re Being Had |Jay Michaelson |November 30, 2014 |DAILY BEAST 

Happily, the people at Atari dislike lurkers almost as much as I do. ‘Asteroids’ & The Dawn of the Gamer Age |David Owen |November 29, 2014 |DAILY BEAST 

They dislike cell phones and they are, for various practical reasons, somewhat secretive. The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 

How could anyone think that their dislike of the Bee Gees made anything about Disco Demolition Night acceptable? Of Gamers, Gates, and Disco Demolition: The Roots of Reactionary Rage |Arthur Chu |October 16, 2014 |DAILY BEAST 

Without any known cause of offence, a tacit acknowledgement of mutual dislike was shewn by Louis and de Patinos. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Her own dislike (it was indeed no less than dislike) of the living lord, her lasting love for the dead one. Elster's Folly |Mrs. Henry Wood 

I have taken a violent dislike to more than one clever American man merely because he trailed his voice through his nose. Ancestors |Gertrude Atherton 

An incident of the campaign of 1806 gave the Marshal's enemies an excellent opening for showing their dislike. Napoleon's Marshals |R. P. Dunn-Pattison 

We resided at Derby in a terrace on the outskirt of the town, much to my dislike, for monotonous rows of houses I have ever hated. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow