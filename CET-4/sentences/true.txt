If they accept you — if Music accept you — then you learn true power. The tale of a bass player, sonic epiphanies and a quest to save ‘real music’ |Ben Ratliff |February 12, 2021 |Washington Post 

It is like my dream is coming true, and it’s been special for me. Matthew Hoppe was a little-known American soccer player — until he reached the Bundesliga |Steven Goff |February 11, 2021 |Washington Post 

It was not a true accurate description of what it does – so now Google is calling it passage ranking. Google passage ranking now live in US English search results |Barry Schwartz |February 11, 2021 |Search Engine Land 

This is especially true for people you once were close to or looked up to, who may now be engaging in behaviors that conflict with your values. Politics and conspiracy theories are fracturing relationships. Here’s how to grieve those broken bonds. |Jeff Schrum |February 11, 2021 |Washington Post 

It is also unclear whether the same is true for variants of the virus that appear to be more contagious. Chicago reaches deal with teachers to reopen school buildings |Moriah Balingit |February 10, 2021 |Washington Post 

What happened to true love knows no boundaries and all that? Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

At 1:42 a.m., a commenter bluntly asked: “Jeff, Is it true you are a convicted sex offender?” Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

True, this may not be what James Madison had in mind when he was writing the Bill of Rights. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

Like his old man, he keeps it reined in, but when talking about fishing, a true regret seeps out. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

None of that is true for those brands who have booked black faces. One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

She is quite true, but not wise, and your left hand must not know what your right hand is doing. Checkmate |Joseph Sheridan Le Fanu 

Each religion claims that its own Bible is the direct revelation of God, and is the only true Bible teaching the only true faith. God and my Neighbour |Robert Blatchford 

Each picture bore a label, giving a true description of the once-honoured gem. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

It seems to be a true instinct which comes before education and makes education possible. Children's Ways |James Sully 

But if what I told him were true, he was still at a loss how a kingdom could run out of its estate like a private person. Gulliver's Travels |Jonathan Swift