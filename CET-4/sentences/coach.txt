An assistant coach had to restrain Hill from going after the ref. A rare disease, a covid diagnosis, a painful decision: The death of basketball coach Lew Hill |Dave Sheinin |February 11, 2021 |Washington Post 

Shaw’s experience of being around Bryant, both as teammate and coach, earns him a receptive ear from his inquisitive youngsters. An NBA experiment lets draft prospects skip college, stay home and get paid to play |Michael Lee |February 11, 2021 |Washington Post 

A referee and two champion coaches showed that a woman’s place is on a Super Bowl fieldEntering the game, Reid said he expected Cheffers and umpire Fred Bryan to take a hands-off approach. Referee already had a history with the Chiefs. The Super Bowl was the latest chapter. |Matt Bonesteel |February 8, 2021 |Washington Post 

Lori Locust, a defensive line assistant, and Maral Javadifar, an assistant strength and conditioning coach, have been working on Bruce Arians’s staff the past two seasons. A referee and two champion coaches showed that a woman’s place is on a Super Bowl field |Cindy Boren |February 8, 2021 |Washington Post 

The Chiefs have an innovative coach, an all-time quarterback who is only 25 and loads of offensive and defensive weapons. What went wrong for the Chiefs and Patrick Mahomes in a brutal Super Bowl defeat |Adam Kilgore |February 8, 2021 |Washington Post 

With a .700 career winning percentage as a coach in college and the NFL, Harbaugh is a winner. Is Any College Football Coach Worth $60 Million? Jim Harbaugh Is |Jesse Lawrence |December 20, 2014 |DAILY BEAST 

At Michigan, he would be a formidable recruiter, able to evoke the tradition of his former iconic coach, Bo Schembechler. Is Any College Football Coach Worth $60 Million? Jim Harbaugh Is |Jesse Lawrence |December 20, 2014 |DAILY BEAST 

Michigan supposedly offered 49ers coach Jim Harbaugh a $42 million contract, which would him the highest-paid coach in the NCAA. Is Any College Football Coach Worth $60 Million? Jim Harbaugh Is |Jesse Lawrence |December 20, 2014 |DAILY BEAST 

Having just crossed the country in coach, I needed instant spiritual repair. Beer Countries vs. Wine Countries |Clive Irving |December 7, 2014 |DAILY BEAST 

“Roughly a third” of the tuition goes to instructors, according to one former coach who asked not to be named. The Secret World of Pickup Artist Julien Blanc |Brandy Zadrozny |December 1, 2014 |DAILY BEAST 

When she arrived she made a regular entry into the city in a coach all gold and glass, drawn by eight superb plumed horses. Music-Study in Germany |Amy Fay 

The father of Mr. Stacy Marks predestined him for the coach-building business. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

On the state-coach went, down the steep, driving the mules madly before it. The Soldier of the Valley |Nelson Lloyd 

To quote Mrs. Kaye, 'A Liberal peer is as useful as a fifth wheel to a coach, and as ornamental as whitewash.' Ancestors |Gertrude Atherton 

A celebrated Coach in Anatomy says that no one can learn Anatomy until he has learned and forgotten it from three to seven times! Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)