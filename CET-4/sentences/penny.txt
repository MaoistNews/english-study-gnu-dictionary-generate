In today’s essay, I look at the bubbly trade in penny and loss-making stocks, including the crazy surge in GameStop. What the savvy investor can learn from the bonkers rally in GameStop shares |Bernhard Warner |January 25, 2021 |Fortune 

Phil Knight, who with his wife, Penny, made the second- and third-largest donations last year according to the Chronicle, increased his wealth by about 77% over the same March-to-December period. Jeff Bezos made the single-largest charitable donation of 2020, toward climate change |lbelanger225 |January 5, 2021 |Fortune 

While most prison jobs pay pennies on the hour, a few prison jobs, such as making furniture, pay $1 an hour, Bryant said. A high school student needed help with tuition, so an unlikely group stepped up: Prison inmates |Kellie B. Gormly |January 1, 2021 |Washington Post 

In this case, fixing just the worst fridges would cut vaccine damage in half and save lives for pennies on the dollar. Once a COVID-19 vaccine is discovered, the hard part begins |jakemeth |September 17, 2020 |Fortune 

Now, Watson can answer 75% of the questions people ask, and Otsego County has started paying for the service, which Pokorny says costs “pennies” per conversation. Millions of Americans Have Lost Jobs in the Pandemic—And Robots and AI Are Replacing Them Faster Than Ever |Alana Semuels |August 6, 2020 |Time 

They deserve every penny and more: booking a four week tour is a huge job. How Much Money Does a Band Really Make on Tour? |Jack Conte |December 8, 2014 |DAILY BEAST 

“Every single witness is inadmissible, hearsay, triple-hearsay,” said assistant state attorney Penny Brill in court yesterday. Did Pablo Escobar Frame a Millionaire for Murdering Banana-Shipping Money Launderers? |Jacqui Goddard |November 11, 2014 |DAILY BEAST 

She regressed to the mental state of a toddler lost in a J.C. Penny department store. How Aidy Bryant Stealthily Became Your Favorite ‘Saturday Night Live’ Star |Kevin Fallon |October 31, 2014 |DAILY BEAST 

“I have not been paid and will not be paid a single penny,” he declared. Greenwald, Assange, and Snowden Join Forces with Kim Dotcom in New Zealand Election |Lennox Samuels |September 17, 2014 |DAILY BEAST 

While there I am, getting mad at my wife for sending me cards all the time because I know she needs every penny right now. Deep Thoughts from War Machine's Sexist, Racist Prison Blog |Melissa Leon |August 21, 2014 |DAILY BEAST 

In 1603 it was ordered that one quart of best ale, or two of small, should be sold for one penny. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Since the end of 1847, not a penny has come into his own pocket either through piano-playing and conducting, or through teaching. Music-Study in Germany |Amy Fay 

A system of supplying school-children with penny dinners is the latest philanthropic movement. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

In the same year he also manufactured two million penny tokens for our soldiers in Spain, which were not forbidden. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

The penny at that time was equal to a shilling of the present day, and would, relatively, purchase as much. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell