A third dog, Asia, escaped and has been recovered safely, authorities say. Lady Gaga offers half a million dollars for her stolen French bulldogs after walker was ambushed |Paulina Villegas |February 26, 2021 |Washington Post 

Sandy Ong reports on the remarkable success of online learning in Asia and the spread of telemedicine in Africa. 10 Breakthrough Technologies 2021 |David Rotman |February 24, 2021 |MIT Technology Review 

Here we look at developments in Asia and Africa that could set an example for the rest of the world. Some parts of remote living are here to stay |Katie McLean |February 24, 2021 |MIT Technology Review 

Since the service has a historical focus on China and Asia, however, BeiDou’s regional users can often get better location information, close to one meter in precision. Hyper-accurate positioning is rolling out worldwide |Katie McLean |February 24, 2021 |MIT Technology Review 

That he is capable of such an action regardless of the consequences, he explains, is a result of whatever happened to him during his time in Asia, as he is a changed person, capable of darkness and unafraid of death. Chang-rae Lee’s ‘My Year Abroad,’ is a sweeping, twisty tale of love, family and hope |Frances Cha |February 18, 2021 |Washington Post 

Asia Bibi, as she is known, was arrested and sentenced to death. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

The accident rate in Asia has marred what was in 2014 a banner year for aviation safety. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Like him, they identified the Airbus A320 as an airplane extremely well fitted to low cost airline operations in Asia. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Malaysian-based entrepreneur Tony Fernandes has turned AirAsia into the most successful low cost airline in southeast Asia. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Launched just 13 years ago, it quickly became a serious rival to MAS and a rising juggernaut in Asia. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

We must make more—much more—elbow room before the Turks get help from Asia or Constantinople. Gallipoli Diary, Volume I |Ian Hamilton 

Some writers have concluded that the plant served as a narcotic in some parts of Asia. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The shells came from Asia and Achi Baba:—in a fiery shower, they fell upon the lines of our front trenches. Gallipoli Diary, Volume I |Ian Hamilton 

These form one of the many island groups that hang like a fringe or festoon on the skirt of the continent of Asia. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

The Tchuktchi of north-eastern Asia are devoted worshipers of tobacco, and is one of the chief articles of trade with them. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.