Some activists work on both causes, and others see their work as ideologically sympathetic. How “abolish ICE” helped bring abolitionist ideas into the mainstream |Nicole Narea |July 9, 2020 |Vox 

The word comes from the grief that these players cause for others. Words From Minecraft: What Are Your Kids Saying? |Minrose Straussman |July 8, 2020 |Everything After Z 

She offers, “Equity is about getting to the root causes of the patterns that you’re seeing and not stopping short” at what companies can see through analysis, “but rather, why is the context what it is.” 3 ways companies can combat the major public health issue of racism right now |Anne Sraders |July 7, 2020 |Fortune 

A seriously wobbly wheel may have compromised bearings in the hub, for instance, which causes it to spin irregularly. Bring your old bike back to life with these pro restoration tips |Stan Horaczek |July 7, 2020 |Popular-Science 

Malaria is an infectious disease caused by a parasite, but it is not contagious because you don’t get it just by being around or coming into contact with an infected person. “Contagious” vs. “Infectious”: The Difference Can Be Important |John Kelly |July 5, 2020 |Everything After Z 

But Cosby Truthers are applying their principles to the wrong cause. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

So we know that boring down to the bedrock and pumping it full of fluid can cause earthquakes. 26 Earthquakes Later, Fracking’s Smoking Gun Is in Texas |James Joiner |January 7, 2015 |DAILY BEAST 

These days weather should never cause a commercial airliner to crash. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

If Dudesmash were to be something we continued doing, this would be an important year to do it, ‘cause we didn’t do one last year. Deer Tick's John McCauley on Ten Years in Rock and Roll |James Joiner |January 2, 2015 |DAILY BEAST 

Michelle Obama tweeting a hashtag is somehow cause for outrage. Political Memes That Absolutely Must Die in 2015 |Asawin Suebsaeng |January 1, 2015 |DAILY BEAST 

Whether they had ever, at different times, pleaded for or against the same cause, and cited precedents to prove contrary opinions? Gulliver's Travels |Jonathan Swift 

Without any known cause of offence, a tacit acknowledgement of mutual dislike was shewn by Louis and de Patinos. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

There is cause for alarm when they bring one hundred and ten ships into these seas without any means of resistance on our part. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

If we are to have a real education along lines of expression we must begin with the "content," or cause, of expression. Expressive Voice Culture |Jessie Eldridge Southwick 

Rapidity of action and a self-confidence which on the battlefield never felt itself beaten were the cause of Murat's success. Napoleon's Marshals |R. P. Dunn-Pattison