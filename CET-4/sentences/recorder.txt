Rather than cursing myself for not sitting down and writing for an hour after a 12-mile hike, I got an audio recorder and recorded voice notes all day long. These Gear Hacks Got Me Through a 30,000-Mile Road Trip |agintzler |July 27, 2021 |Outside Online 

They don’t use helicopters unless it’s a life or death situation, and when I was doing my studies, I had to camouflage my recorders so people wouldn’t notice them. The Military’s ‘Garbage Disposal’ Jets Are Ruining One of America’s Quietest Parks |Daniel Modlin |June 7, 2021 |The Daily Beast 

The source for the database was more than 2,700 assessment contracts that PACE districts filed with local recorders of deeds between November 2016 and February 2021 to secure property as collateral for their loans. State-Supported “Clean Energy” Loans Are Putting Borrowers At Risk of Losing Their Homes |by Jeremy Kohler and Haru Coryne |April 23, 2021 |ProPublica 

Los Angeles-based startup Kernel has devised two technologies that serve as non-invasive brain recorders — inventions that attracted $53 million in funding last year. Matrix 2021: Is Neurotech ‘the One’ That’ll Save Us? |Sean Culligan |April 4, 2021 |Ozy 

These sequences are incorporated into the recorder’s “DNA ticker tape” to document the signal. New Research Could Enable Direct Data Transfer From Computers to Living Cells |Edd Gent |January 11, 2021 |Singularity Hub 

I stood with a tape recorder, listening to men denounce the liberal media controlled by Jews. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

Her arrest came at a checkpoint in Damascus in June 2013, when soldiers spotted an audio recorder in her bag. Escaping Assad’s Rape Prisons: A Survivor Tells Her Story |Jamie Dettmer |October 28, 2014 |DAILY BEAST 

But sitting in a room with a guy and tape recorder asking those questions had to have been frustrating at the time, right? Choose Your Own Neil Patrick Harris: The Star on ‘Doogie,’ ‘Gone Girl,’ Gay Sex and More |Kevin Fallon |October 10, 2014 |DAILY BEAST 

Griffin is herself a character in the novel, the invisible hand on the other end of the tape recorder in all the interviews. A YA Novel About a Warhol Girl With Banksy Talent |Hugh Ryan |August 14, 2014 |DAILY BEAST 

Had Richard III been able to install a tape recorder in his palaces the ranting might well have been identical. Three Dicks: Cheney, Nixon, Richard III and the Art of Reputation Rehab |Clive Irving |July 27, 2014 |DAILY BEAST 

The safe rule is to leave the deed with the recorder as soon as possible after receiving it. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

As soon as the deed has been delivered, it should be taken to the recorder's office to be recorded. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

But these were trifles compared with the devastation committed at Bristol, when its recorder. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

The Sunday after I was brought to the same place again, before the lieutenant and recorder of London, and they examined me. Fox's Book of Martyrs |John Foxe 

The recorder was forbidden to practice at the bar except in cases which concerned himself or the town or Colony. A short history of Rhode Island |George Washington Greene