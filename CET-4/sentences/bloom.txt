Cyanobacterial blooms occur when still, warm water—like a pond—has so much nitrogen and phosphorus that the algae are able to breed and grow at an extremely fast rate, spreading throughout the pond. More than 350 elephants died in Botswana, and we may finally know why |Kat Eschner |September 24, 2020 |Popular-Science 

It’s a condition where an overgrowth of algae, known as a bloom, removes a lot of oxygen from the water. A dirty and growing problem: Too few toilets |Stephanie Parker |September 24, 2020 |Science News For Students 

“The ability to tie back to business outcomes is always going to be attractive,” Bloom added. ‘The more culture you own’: Condé Nast pursues more revenue growth with new brand-strength metric |Max Willens |September 18, 2020 |Digiday 

If you look closely, part of the bloom resembles an elephant’s trunk. How a Wasteland Shrub Is Becoming the Next Big Thing in Fashion |Daniel Malloy |August 28, 2020 |Ozy 

Margaret Mead and Ruth Benedict were among them—and the source of the problem, as Bloom saw it. Gender Is What You Make of It - Issue 88: Love & Sex |Charles King |August 5, 2020 |Nautilus 

Good news: so is this grainy TMZ footage of a Bieber/Bloom Ibiza brawl. Solange Smacks Jay Z, Legolas Slaps Bieber, and the Biggest Celebrity Feuds of the Year |Amy Zimmerman |December 24, 2014 |DAILY BEAST 

With women put in front of the public to justify staying with bad men, we see these justifications in full bloom. Why Didn’t Camille Dump Bill Cosby? |Amanda Marcotte |December 17, 2014 |DAILY BEAST 

About Blood Meridian, Bloom has said, “The violence is the book.” Compliments Are Nice, but Enough With the Cormac McCarthy Comparisons |William Giraldi |October 21, 2014 |DAILY BEAST 

“She follows the war and makes it very much into her business,” noted the critic Harold Bloom. Brecht's Mercenary Mother Courage Turns 75 |Katie Baker |September 10, 2014 |DAILY BEAST 

If you've ever questioned Justin Bieber's masculinity, he was almost knocked out by ORLANDO BLOOM. 13 Celebrities Who Dissed Justin Bieber |Kevin Fallon |August 7, 2014 |DAILY BEAST 

Let the thought of self pass in, and the beauty of great action is gone, like the bloom from a soiled flower. Pearls of Thought |Maturin M. Ballou 

The late roses and white lilies were in full bloom, the latter filling the air with a sweet odor and making a lovely background. The Cromptons |Mary J. Holmes 

In many of the parks, the rhododendrons were in full bloom, and their rich masses of color wonderfully enlivened the scenery. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Vases of flowers diffused their fragrance and expanded their beauty where flowers were never seen to bloom before. Madame Roland, Makers of History |John S. C. Abbott 

The day lilies were in bloom, and that meant August; it meant also that her book was written, rewritten, and ready to be copied. Tessa Wadsworth's Discipline |Jennie M. Drinkwater