There’s been a lot of quarterback movement around the league — and a lot of curiosity specifically about what the change at QB will mean for New England and Tampa Bay — but those moves haven’t affected our predictions too much. We Previewed Both Kinds Of Football For You |Sarah Shachat |September 8, 2020 |FiveThirtyEight 

By showing genuine curiosity in the families’ experience, Martinez said, the team is often able to get them to open up. Border Report: The Lingering Trauma of Family Separation |Maya Srikrishnan |August 31, 2020 |Voice of San Diego 

If for no other reason than curiosity, investigate Bitcoin and digital assets and see what everyone is talking about. Why 2020 might be the year cryptocurrency goes mainstream |jakemeth |August 24, 2020 |Fortune 

Nevertheless, the Ising model survived as a mathematical curiosity. The Cartoon Picture of Magnets That Has Transformed Science |Charlie Wood |June 24, 2020 |Quanta Magazine 

Consider, for example, the dodecahedron, a favorite object in many mathematical cabinets of curiosities. The Two Forms of Mathematical Beauty |Robbert Dijkgraaf |June 16, 2020 |Quanta Magazine 

I noticed a picture of her daughter, who was my classmate, and out of curiosity visited her page. 50 Shades of Iran: The Mullahs’ Kinky Fantasies about Sex in the West |IranWire, Shima Sharabi |January 1, 2015 |DAILY BEAST 

In fact, I publicly vowed to abstain from The Ball in 2012, but professional responsibilities and curiosity got the better of me. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

I remember being appalled that he killed off Little Nell in The Old Curiosity Shop. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

However, several probes—most recently the Curiosity rover—have measured methane in the Martian atmosphere. Methane on Mars: Life or Just Gas? |Matthew R. Francis |December 17, 2014 |DAILY BEAST 

“Curiosity cabinets are really a 16th century thing of trying to understand the world,” Wynd says. Dodo Bones and Kylie’s Poo: Inside London’s Strangest New Museum |Liza Foreman |November 11, 2014 |DAILY BEAST 

Sam sat opposite him in perfect silence, waiting, with eager curiosity, for the termination of the scene. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

She had never had this curiosity in relation to George Cannon--she had only wondered about his affairs with other women. Hilda Lessways |Arnold Bennett 

Miss Thangue sat forward with the frank curiosity of the Englishwoman when inspecting a foreign specimen. Ancestors |Gertrude Atherton 

Her directness had made all possible 'buts' seem ridiculous and futile, and had made the expression of curiosity seem offensive. Hilda Lessways |Arnold Bennett 

She had done with little things, and Isabel, with young curiosity, wondered in what convulsion the last of them had gone down. Ancestors |Gertrude Atherton