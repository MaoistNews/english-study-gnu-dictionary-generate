In powder form, it has a mild, nutty flavor and is best used like a protein boost, sprinkled over porridge, stirred into a vegetarian chili or folded into banana bread batter. They're Healthy. They're Sustainable. So Why Don't Humans Eat More Bugs? |Aryn Baker |February 26, 2021 |Time 

I tried out the book’s recipe for turkey jook, a rice porridge made with turkey stock, which I’d never made before but had eyed over the years. 20 Days of Turkey |Meghan McCarron |December 23, 2020 |Eater 

Many of their traditional foods, such as tortillas and pancake-like chorreadas, were adapted by the wider population, while the use of porridges and drinks made from maíz pujagua, or purple corn, are more isolated. Everything You Need to Know About Costa Rican Food |Nicholas Gill |November 11, 2020 |Eater 

Almost everyone who has won has used steel-cut oats and soaked the porridge overnight. In Pursuit of the Perfect Bowl of Porridge |Clarissa Wei |September 11, 2020 |Eater 

Per Carlsson of Sweden snagged the 2017 specialty win with a cloudberry-liqueur porridge brulee. In Pursuit of the Perfect Bowl of Porridge |Clarissa Wei |September 11, 2020 |Eater 

I went to get a bag of horse manure and I make it liquid, like a porridge, and then ... bam! Pensioner Planned Royal Attack With Horse Faeces |Tom Sykes |November 13, 2012 |DAILY BEAST 

There, they were presented with 40 crockpots of porridge; stacks of raisins, and piles of spoons. Live From Art Basel |Isabel Wilkinson |December 4, 2010 |DAILY BEAST 

Two years later, a repentant Uma wore skin-tight Versace in a meek porridge hue. The 10 Most Horrendous Oscar Gowns in History |Dale Hrabi |February 21, 2009 |DAILY BEAST 

"Then I'll catch it," I said, laughing at his discomfiture, for I knew he loathed stirring porridge. Three More John Silence Stories |Algernon Blackwood 

Porridge is a food which satisfies and strengthens, and which, it seems, is rich in bone-forming matter. Friend Mac Donald |Max O'Rell 

The room was full of healthy-looking workmen, tidily dressed and busily doing honour to the porridge and other items on the menu. Friend Mac Donald |Max O'Rell 

Robert Burns, who has sung of the haggis and the whisky of his native land, has only made indirect mention of porridge. Friend Mac Donald |Max O'Rell 

A very small bit of this chestnut grated into a kettle would make a potful of porridge. Stories the Iroquois Tell Their Children |Mabel Powers