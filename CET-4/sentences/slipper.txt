First, we can wear shoes — especially when carrying or moving something — or slippers. ‘A pandemic of broken toes’: How life at home has been painful for feet |Elizabeth Chang |January 12, 2021 |Washington Post 

Tasha Zemke, copy editor Every winter I treat myself to a new pair of slippers. The Gear Our Editors Loved in November |The Editors |December 4, 2020 |Outside Online 

Their versatile build “sits at the intersection between a runner, a travel slipper, a water shoe, and a low-cut hiking boot,” he wrote. Outside's 12 Days of Deals |The Editors |December 1, 2020 |Outside Online 

Ariella Gintzler, associate editorI’ve been looking for some quality slippers since March, and I’m happy to say I’ve finally found them. The Gear Our Editors Loved in October |The Editors |November 5, 2020 |Outside Online 

The leather slippers are intended for all-day wear, year-round. Gift Guide: What to wear when working from home |Rachel King |November 2, 2020 |Fortune 

“The prince is not going to come along with a glass slipper,” Thomas says. Marlo Thomas Says Girls Should Feel Free to Be Like Hannah Horvath |Emily Shire |April 24, 2014 |DAILY BEAST 

Displayed in a main hall lined with transparent cases, each is like a glass slipper menagerie. Shoes Fit For A Museum: Roger Vivier’s Virigule Show Opens at Palais De Tokyo |Sarah Moroz |October 2, 2013 |DAILY BEAST 

Three types of footwear -- including a brogue, a plimsoll, and an evening slipper -- established the basis for Katrantzou's theme. Queen of Prints, Mary Katrantzou |Erin Cunningham |September 16, 2013 |DAILY BEAST 

As if Cinderella showed up at the ball with a little pumpkin schmutz on her glass slipper to wreck the fantasy. French President Francois Hollande’s Inability to Tie a Necktie Earns France’s Scorn |Tracy McNicoll |November 10, 2012 |DAILY BEAST 

Snooki, meanwhile, has added a new line of platform flip-flops to her slipper line. Italy’s ‘Jersey Shore’ Spinoff |Barbie Latza Nadeau |June 17, 2011 |DAILY BEAST 

Her foot, thrust into a heel-less Tunisian slipper of blue velvet embroidered with seed pearls, beat the floor impatiently. The Pit Town Coronet, Volume III (of 3) |Charles James Wills 

Wilder flung a slipper across the room that missed Tootles head and clattered among the paint-brushes. The Woman Gives |Owen Johnson 

She went to the trunk and shut it with a bang, placing a red-heeled slipper on it, with a neat flash of blue-silk ankle above. The Woman Gives |Owen Johnson 

We all laughed, but I took the slipper and beat him with it, while Peter appeared on the verge of tears. The Idyl of Twin Fires |Walter Prichard Eaton 

Peter resentfully deposited the pup on the porch, and took my slipper back upstairs. The Idyl of Twin Fires |Walter Prichard Eaton