Indeed, every teacher is expected to be a Muslim by birth or conversion. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Indeed, as an almost purely advisory firm, Lazard is (appropriately) barely affected by the Dodd-Frank reforms. Antonio Weiss Is Not Part of the Problem |Steve Rattner |January 7, 2015 |DAILY BEAST 

Indeed, Lion Air, with 45 percent of the domestic Indonesian airline market, has swallowed the Fernandes formula whole. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Indeed, study after study affirms the benefits of involved fatherhood for women and children. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

Indeed, some of those troops who ran away from defending Mosul were already American-trained. Pentagon Insider on New Plan to Fight ISIS: ‘Of Course It’s Not Enough’ |Nancy A. Youssef |January 6, 2015 |DAILY BEAST 

Here began indeed, in the drab surroundings of the workshop, in the silent mystery of the laboratory, the magic of the new age. The Unsolved Riddle of Social Justice |Stephen Leacock 

Dean Swift was indeed a misanthrope by theory, however he may have made exception to private life. Gulliver's Travels |Jonathan Swift 

And all over the world each language would be taught with the same accent and quantities and idioms—a very desirable thing indeed. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Indeed, a score of bodies lying there had not been seen by Malcolm during his first frenzied examination of the house. The Red Year |Louis Tracy 

The Spaniards, indeed, feigned to regard them only as a remnant of the rebels who had joined the pre-existing brigand bands. The Philippine Islands |John Foreman