The top four assist leaders are too, prompting a look into why Canadian teams are surging in the scoring department. The NHL’s Canadian teams are scoring a lot. Like, a lot, a lot. |Neil Greenberg |February 12, 2021 |Washington Post 

Regardless of what kind of tool they’re selling, AI hiring vendors generally promise that these technologies will find better-qualified and more diverse candidates at lower cost and in less time than traditional HR departments. Auditors are testing hiring algorithms for bias, but there’s no easy fix |Amy Nordrum |February 11, 2021 |MIT Technology Review 

Hospitals have conducted the most vaccinations, followed by local health departments and long-term care facilities. Leaders in Washington region ask FEMA for help in vaccinating federal workers |Julie Zauzmer, Rachel Chason, Rebecca Tan |February 11, 2021 |Washington Post 

A department spokesman said he could not be more specific about when the CPUC received the payments, so it’s not clear if that happened before or after Stebbins raised her concerns. Utility Companies Owe Millions to This State Regulatory Agency. The Problem? The Agency Can’t Track What It’s Owed. |by Scott Morris, Bay City News Foundation |February 10, 2021 |ProPublica 

In an email to department employees the following morning, he said he was “honored and delighted to be officially on board.” Buttigieg to quarantine for 14 days after security agent tests positive for coronavirus |Michael Laris |February 8, 2021 |Washington Post 

Their friendship began when Krauss, who was chairman of the physics department at Case Western in Cleveland, sought out Epstein. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

The “doctorate” Duke claims is from an anti-Semitic Ukranian “diploma mill” as described by the State Department. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

Seventy-two adults between the ages of 18 and 50 are participating in the trial, led by the pediatrics department at Oxford. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

The State Department found that with high oil prices, the tar sands would be mined for oil, pipeline or no. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

The EPA felt that the State Department had not looked carefully enough at the impact of the pipeline if oil prices fell. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

At the latter date all artists were obliged to vacate the Sorbonne ateliers to make room for some new department of instruction. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

There is one other department of children's art which clearly does deserve to be studied with some care—their drawing. Children's Ways |James Sully 

The percentage of reserves to deposits, which marks the safety line for England, refers to the items in the banking department. Readings in Money and Banking |Chester Arthur Phillips 

There are many articles in the American department of which I would gladly speak, that have attracted no public notice. Glances at Europe |Horace Greeley 

That's the crushing kind of repertoire he gives his pupils—so exhaustive and complete in every department. Music-Study in Germany |Amy Fay