The “industrial scale” of the plant’s gene theft is also impressive, he says. A reeking, parasitic plant lost its body and much of its genetic blueprint |Jake Buehler |February 10, 2021 |Science News 

Crews will remove and replace the 120-by-70-foot sign, one of the last, most visible vestiges of Baltimore’s once-mighty industrial past. Domino Sugar is replacing its massive neon landmark in Baltimore — and hopes no one will notice a difference |Colin Campbell |February 7, 2021 |Washington Post 

In its analysis, that period stands in for the time before the industrial age, which began in the mid-1700s. Analyze This: 2020 ties with 2016 for hottest year on record |Carolyn Wilke |February 3, 2021 |Science News For Students 

The new upgrades are already rolling out, so if you’re in charge of a giant industrial company, it’s time to get your orders in. Boston Dynamics gave its dog-like robot a charging dock and an arm on its head |Stan Horaczek |February 2, 2021 |Popular-Science 

There’s something difficult to reconcile watching Spot walk up a flight of stairs in some industrial setting. Soon Boston Dynamics’ Spot will be remotely opening doors anywhere |Brian Heater |February 2, 2021 |TechCrunch 

The Industrial Revolution and Victorian practically erased the holiday in England. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

The folk memory of medieval community life had been wiped out by the industrial revolution. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

Fracking, in this regard, is no different from gypsum mining, or some kinds of industrial agriculture. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

Moreover, trucks, dust, and boomtown stress are the effects of any large-scale industrial activity. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

But they are serious: what large-scale fracking does is change small farm towns into industrial sites. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

A considerable proportion of the industrial and commercial news is now written to an end. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

It offers, to those who see it aright, the most perplexing industrial paradox ever presented in the history of mankind. The Unsolved Riddle of Social Justice |Stephen Leacock 

Industrial society, they say, must be reorganized from top to bottom; private industry must cease. The Unsolved Riddle of Social Justice |Stephen Leacock 

But closely allied to this subject, and not inferior to it in importance, stands that of Industrial Training. Glances at Europe |Horace Greeley 

The legal framework of the State and of obedience to the law in which industrial society is set threatens to break asunder. The Unsolved Riddle of Social Justice |Stephen Leacock