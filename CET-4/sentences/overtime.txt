Teams get two points in the standings for a win of any kind while an overtime loss, including the shootout, earns a team one point, also known as the “loser point.” ‘Loser points’ could produce some unlikely winners in 2021 NHL season |Neil Greenberg |February 5, 2021 |Washington Post 

Unionization could give these Alabama warehouse employees the opportunity to collectively fight for regular pay raises and have bargaining power when it comes to obligations like mandated overtime shifts. Why Amazon’s $62 Million FTC Labor Settlement Is a Bigger Deal Than the Bezos News |Abby Vesoulis |February 4, 2021 |Time 

It’s hard, because people that work here, they’ve been working full-time, no vacations, overtime, in order to care for their patients. Kansas City Chiefs Lineman Laurent Duvernay-Tardif Stayed On The COVID-19 Front Lines. Now He's Missing The Super Bowl. |Sean Gregory |February 4, 2021 |Time 

We should work weekends and overtime, ruthlessly pursuing wealth and status, to demonstrate our commitment to family. We Need More Feminist Dads - Issue 95: Escape |Jordan Shapiro |February 3, 2021 |Nautilus 

Mandatory overtime, Knox said, is communicated to workers no later than their lunch break the previous day. Amazon’s anti-union blitz stalks Alabama warehouse workers everywhere, even the bathroom |Jay Greene |February 2, 2021 |Washington Post 

Clinkscales is still a cop, and made more than $100,000 in salary and overtime last year. Chicago’s Cops Don’t Even Get Investigated for Shooting People in the Back |Justin Glawe |December 5, 2014 |DAILY BEAST 

All of these people, and millions more like them, deserve a little overtime. It’s Always Black Friday for Clerks |Michael Tomasky |November 28, 2014 |DAILY BEAST 

This would restore overtime rights to workers earning up to around $50,000 a year, which is roughly the current median. It’s Always Black Friday for Clerks |Michael Tomasky |November 28, 2014 |DAILY BEAST 

The CAP paper estimates that if current trends continue unabated, overtime pay will disappear entirely by 2026. It’s Always Black Friday for Clerks |Michael Tomasky |November 28, 2014 |DAILY BEAST 

But they are paid less, and the vanishing overtime pay is a big part of why. It’s Always Black Friday for Clerks |Michael Tomasky |November 28, 2014 |DAILY BEAST 

Before I'd gone five miles the hoodoo that had been working overtime on my behalf got busy again. Raw Gold |Bertrand W. Sinclair 

My father's time was hired out and as he knew a trade he had by working overtime saved up a considerable amount of money. Slave Narratives: a Folk History of Slavery in the United States |Various 

Joe was working overtime, covering the drills, while his father was doing the stable work. The Idyl of Twin Fires |Walter Prichard Eaton 

Mr. Van Dyke and his clerks, assisted by boy scouts, were working overtime to gratify all these demands. Ways of War and Peace |Delia Austrian 

One special cause of offense was the keeping back of overtime money to buy a new jacket. The Leaven in a Great City |Lillian William Betts