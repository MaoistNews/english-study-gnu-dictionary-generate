Sanders has done research that shows that college students tend to learn better when they have access to videos of lectures. Healthy screen time is one challenge of distance learning |Kathryn Hulick |September 11, 2020 |Science News For Students 

He and Dung Bui, then also at Washington University, had students listen to a lecture on car brakes and pumps. Top 10 tips on how to study smarter, not longer |Kathiann Kowalski |September 9, 2020 |Science News For Students 

Like thousands of US colleges and universities this spring, Simmons University in Boston had to adjust to Covid-19 on the fly, closing lecture halls and moving classes online. History will look back on 2020 as a turning point for US universities |Oliver Staley |August 23, 2020 |Quartz 

Nor did it touch you as a student seated in the wood-paneled lecture halls of law school. The first murder |Katie McLean |August 19, 2020 |MIT Technology Review 

Concerts, theatrical performances, award shows, conventions, lecture tours – every large in-person event across the country was either cancelled or postponed for the foreseeable future. GLAAD Media Awards will go on – virtually, of course |John Paul King |July 23, 2020 |Washington Blade 

Nobody has to lecture me about how Sharpton has played racial politics in New York. Steve Scalise and the Right’s Ridiculous Racial Blame Game |Michael Tomasky |January 2, 2015 |DAILY BEAST 

I, and many fellow men, know this because women say so—they write it, they lecture on it, they write books about it. Hey, Creeps, ‘Compliments’ Are Harassment, Too |Tauriq Moosa |November 5, 2014 |DAILY BEAST 

She hated sharing Georgie with his admirers, particularly on lecture tours in in North America. Borges Had A Genius For Literature But Not Love Or Much Else |Allen Barra |October 24, 2014 |DAILY BEAST 

The closing lecture also presents questions that Chomsky never answers—mainly one of alternatives. Noam Chomsky—Infuriating and Necessary |David Masciotra |September 28, 2014 |DAILY BEAST 

He carried a chair onto the stage, sat down and repeated the lecture he uses when­ever he hires an old-time musician. Stanley Booth on the Life and Hard Times of Blues Genius Furry Lewis |Stanley Booth |June 7, 2014 |DAILY BEAST 

I told her, when I wrote last, how I felt; and you never read such a lecture as she gave me in return. Elster's Folly |Mrs. Henry Wood 

However, he arrived in Aberdeen radiant, gave his lecture, and at the end was presented by Donald with a cheque for twenty pounds. Friend Mac Donald |Max O'Rell 

Lectures—Two ladies may attend a lecture, unaccompanied by a gentleman, without attracting attention. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

In a room, a few miles out of London, I had just given a lecture to the members of a literary Society. Friend Mac Donald |Max O'Rell 

I have often had the pleasure of hearing Mme. de Mirbel lecture her and it was very comical. Charles Baudelaire, His Life |Thophile Gautier