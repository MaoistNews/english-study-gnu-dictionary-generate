They’re likely banking on the fact that voters don’t remember or care that Republicans waited months to start negotiating. Why House Democrats have good reason to be anxious about no coronavirus relief deal |Amber Phillips |September 17, 2020 |Washington Post 

Achieving herd immunity is, in fact, one goal in this pandemic. The problem with Trump’s ‘herd mentality’ line isn’t the verbal flub. It’s the mass death. |Philip Bump |September 16, 2020 |Washington Post 

This story has been updated to reflect the fact that Red Ventures has made four acquisitions this year, not three. ‘Helping people discover information’: How Red Ventures grew into a giant |Max Willens |September 16, 2020 |Digiday 

Many publishers have struggled with the fact that virtual event sponsorships fetch smaller amounts of money than in-person ones. ‘Eager to explore more’: Business Insider’s virtual events strategy lifts volume and plants evergreen on-demand feature |Max Willens |September 16, 2020 |Digiday 

“The fact is, we created the greatest economy in the history of the world.” Trump’s ABC News town hall: Four Pinocchios, over and over again |Glenn Kessler |September 16, 2020 |Washington Post 

He loves the fact that, like on Grindr, users can identify as transgender. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

In fact, in a recent study of their users internationally, it was the lowest priority for most. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

Despite the strong language, however, the neither the JPO nor Lockheed could dispute a single fact in either Daily Beast report. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

In fact, according to F-35 program sources, the next software upgrades are not yet fully defined nor are they fully funded. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

In its attempt to discredit the story, the JPO inadvertently confirmed that fact. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

Many British Ferns evidence a marked tendency to “sport,” and this is a fact which the beginner should always bear in mind. How to Know the Ferns |S. Leonard Bastin 

In fact, except for Ramona's help, it would have been a question whether even Alessandro could have made Baba work in harness. Ramona |Helen Hunt Jackson 

Recognition of the pneumococcus depends upon its morphology, the fact that it is Gram-staining, and the presence of a capsule. A Manual of Clinical Diagnosis |James Campbell Todd 

The fact that the day following the punishment parade was a Sunday brought about a certain relaxation from discipline. The Red Year |Louis Tracy 

I claim that it contains many errors of fact, and the Higher Criticism supports the claim; as we shall see. God and my Neighbour |Robert Blatchford