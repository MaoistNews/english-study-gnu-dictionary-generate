The polling place, a convention center, offered multiple locations to cast ballots, and transportation by bus there was free. For Election Administrators, Death Threats Have Become Part of the Job |by Jessica Huseman |August 21, 2020 |ProPublica 

We’re personally reaching out to riders and drivers to share more about why this is happening, what you can do about it, and to provide some transportation alternatives. Lyft plans to suspend California rides at midnight, barring a court decision |Danielle Abril |August 20, 2020 |Fortune 

I needed to get home but realized that public transportation had been shut down, so I started walking to get a cab. Misery in Minsk |Eugene Robinson |August 18, 2020 |Ozy 

Councilwoman Barbara Bry, however, has put some distance between herself and SANDAG executive director Hasan Ikhrata’s proposal to make alternative forms of transportation just as competitive as driving. Morning Report: SDPD Says It Will Stop Seditious Language Tickets |Voice of San Diego |August 17, 2020 |Voice of San Diego 

When I think of a progressive view toward sustainability, toward energy, toward transportation, toward housing, we Americans think of you — maybe not Amsterdam per se but your part of the world certainly, as much more of a model than us. Is Economic Growth the Wrong Goal? (Ep. 429) |Stephen J. Dubner |August 13, 2020 |Freakonomics 

Pulling oil from the tar sands is costly, even more so when you tack transportation costs on top. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

But with the pipeline, transportation costs drop and production would be higher. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

The transportation service—and others like it—epitomize what the sharing economy is all about. One of a Kind Gifts Are Only a Neighbor Away |Lawrence Ferber |December 8, 2014 |DAILY BEAST 

Sure, the Red Coats had the upper hand in terms of transportation, supplies and training. The British Royals Reinvade Brooklyn: William and Kate Come Watch Basketball on Historic Battle Site |Justin Jones |December 6, 2014 |DAILY BEAST 

By early evening on Tuesday, the inter-city transportation system of shared taxis was shut down. The Radicals Who Slaughtered a Synagogue |Creede Newton |November 19, 2014 |DAILY BEAST 

Railroad tracks and bridges had been demolished; transportation facilities in some areas were almost non-existent. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

The sixth largest employer of workers engaged in manufacturing is the transportation equipment industry. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Half of the workers included in this category consist of railroad and water transportation workers. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

The economic activity of any region depends greatly upon its transportation facilities. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

He was stepping out upon the broad surface of the Transportation Building. Astounding Stories, May, 1931 |Various