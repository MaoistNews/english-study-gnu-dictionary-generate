Unless something changes in the original agreement, the financial responsibility for making those repairs will continue to fall on taxpayers. The Deal Before the 101 Ash St. Debacle Helps Explain How We Got Here |Lisa Halverstadt and Jesse Marx |August 24, 2020 |Voice of San Diego 

Those include auto repair, movers, photography, salons, pet groomers, realtors, child care, home services, professional services and contractors, among others. Yelp’s updated ‘Request a Quote’ and new ‘Nearby Jobs’ provide lead-gen for SMBs |Greg Sterling |August 18, 2020 |Search Engine Land 

A consultant hired by the city now estimates the building will need about $115 million in repairs that could take up to four years to complete. How the City Came to Lease a Lemon |Lisa Halverstadt and Jesse Marx |August 10, 2020 |Voice of San Diego 

Councilman Scott Sherman, for instance, said he wants to use it in neighborhoods for beautification of utility boxes or sidewalk repair. The City Is Walking a Fine Line in Demanding Millions From Its Next Power Provider |MacKenzie Elmer |August 7, 2020 |Voice of San Diego 

It’s that pot of money Council members are hopeful could be used to pay for things like electric vehicle charging stations, utility box beautification, sidewalk repair and tree trimming. What Power San Diego Has Over Its Power Company |MacKenzie Elmer |August 4, 2020 |Voice of San Diego 

Having just crossed the country in coach, I needed instant spiritual repair. Beer Countries vs. Wine Countries |Clive Irving |December 7, 2014 |DAILY BEAST 

This meant helping to change those insanely large tires and working to repair the vehicles. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

Today the church is wrapped in scaffolding and metal ribbons are holding its façade in place until someone pays to repair it. Madonna, Carla Bruni & Obama Abandoned Pledges To Rebuild L'Aquila After The Quake |Barbie Latza Nadeau |November 18, 2014 |DAILY BEAST 

Without the proper equipment to repair and operate the Mohajer-4 it may be more of a photo prop than a piece of weaponry. ISIS: We Nabbed an Iranian Drone |Jacob Siegel |November 17, 2014 |DAILY BEAST 

An exuberant game of football takes place, then the sound of shells is heard, and both sides repair back to their enemy positions. How Monty The Penguin Won Christmas: Britain’s Epic, Emotional Commercials |Tim Teeman |November 16, 2014 |DAILY BEAST 

A lineman was sent out to repair it under escort of civil guards, who were forced by the rebels to retire. The Philippine Islands |John Foreman 

Gourges fitted out three vessels and 150 soldiers at his own expense to revenge their death, and repair the honor of his nation. The Every Day Book of History and Chronology |Joel Munsell 

The third boat and kite had been damaged beyond repair, but the two left were sufficient. The Giant of the North |R.M. Ballantyne 

If mistakes are made they happen rarely and the resources of the accepting houses are easily able to repair the damage. Readings in Money and Banking |Chester Arthur Phillips 

Several of the organs constructed by his firm are in use to-day and are in a good state of repair. The Recent Revolution in Organ Building |George Laing Miller