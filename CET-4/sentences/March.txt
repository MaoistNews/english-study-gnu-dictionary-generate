Then its Work from Home Challenge was created once the pandemic took effect in March. ‘The second wave’: Publishers see the value of providing education through newsletter courses |Kayleigh Barber |August 27, 2020 |Digiday 

Blake’s father will speak at Sharpton’s March on Washington commemoration on Friday, Noerdlinger said. Protests continue in Wisconsin after police shooting of Jacob Blake |kdunn6 |August 25, 2020 |Fortune 

Just last week we were cheering a weekly jobless claims number that came in below 1 million for the first time since March. Investors ride the Big Tech rally even as COVID cases and unemployment spike |Bernhard Warner |August 21, 2020 |Fortune 

Then came the big selloff that sank the S&P by around 35% in March. The champ’s big comeback: Why beaten-down value stocks are poised to thrive |Shawn Tully |August 18, 2020 |Fortune 

By Jeanniey Walden, Chief Innovation & Marketing Officer, DailyPay In March, everything changed in an instant. Research: Only 25 percent of professionals expect to be working from home long-term |DailyPay |August 17, 2020 |Digiday 

I was pregnant, uncomfortably so, for the first time and with twins, due the following March. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

Last March they gave Airbus a huge piece of new business, ordering 169 A320s and 65 of the slightly larger A321. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Her focus would be on the three months, January through March 1965, that gave birth to the Voting Rights Act. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

The NYPD Emerald Society pipes and drums struck up a slow march and the procession began the journey to the cemetery. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

In March, police arrested a group of wealthy businessmen and government officials who were about to dine on illegal tiger meat. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

In less than ten minutes, the bivouac was broken up, and our little army on the march. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Nothing remarkable occurred in our march through this country. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Genoa has but recently and partially felt the new impulse, yet even here the march of improvement is visible. Glances at Europe |Horace Greeley 

Then with your victorious legions you can march south and help drive the Yankee invaders from the land. The Courier of the Ozarks |Byron A. Dunn 

While they were doing this, he assembled the officers around him, and the meaning of our night march was explained to us. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various