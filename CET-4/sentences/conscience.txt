You’ve focused a lot on the soul and conscience of Mediabrands. Media Buying Briefing: Mediabrands CEO Daryl Lee discusses the soul of the media agency business |jim cooper |February 8, 2021 |Digiday 

He wonders whether next season he will be able to win his job back or whether the state of the virus will have improved enough for him to return with a clear conscience. His team is going to the Super Bowl. He’s staying on the coronavirus front lines. |Adam Kilgore |February 1, 2021 |Washington Post 

If Republicans have genuinely relocated their fiscal consciences, they’ll listen. Right on schedule, Republicans pretend to care about deficits again |Catherine Rampell |January 21, 2021 |Washington Post 

She is a rapper with a conscience, and she’s not going away. 25 Rising Stars to Track in 2021 |Daniel Malloy |December 20, 2020 |Ozy 

Don’t know if this is the answer you were looking for, but it’s the only one I can give in good conscience. The Outré Art of Pegging |Eugene Robinson |December 14, 2020 |Ozy 

Instead, straighten your civic backbone and push back in clear conscience. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

Better to be a beggar in freedom,” he cried out, “than to be forced into compromises against my conscience. The Catholic Philosopher Who Took on Hitler |John Henry Crosby |December 26, 2014 |DAILY BEAST 

Martin Luther King Jr., Nelson Mandela, Oskar Schindler—these names come readily to mind when we think of heroes of conscience. The Catholic Philosopher Who Took on Hitler |John Henry Crosby |December 26, 2014 |DAILY BEAST 

As you put it, “letting some business owners exercise their conscience would cause no harm to gays.” Do LGBTs Owe Christians an Olive Branch? Try The Other Way Around |Jay Michaelson |December 14, 2014 |DAILY BEAST 

“Nothing in this country of good conscience has ever happened without protest,” he said. Eric Garner Protesters Have a Direct Line to City Hall |Jacob Siegel |December 11, 2014 |DAILY BEAST 

She reached forward to it in ecstasy; but she might not enjoy it, save at the price which her conscience exacted. Hilda Lessways |Arnold Bennett 

In this way it will be managed with less offense and with more ease to the conscience than now. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

My conscience importuned me to tell her bluntly that they would only come into Walsh feet first. Raw Gold |Bertrand W. Sinclair 

That he might lose his head and 'introduce an element of sex' was conscience confessing that it had been already introduced. The Wave |Algernon Blackwood 

But the conscience of Louis was at rest; and he soon found that "man does not live by bread alone!" The Pastor's Fire-side Vol. 3 of 4 |Jane Porter