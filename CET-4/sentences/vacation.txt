Watching Barb and Star Go to Vista Del Mar was the closest I’ve come in a long time to feeling like I was on vacation. The fabulous Barb and Star Go to Vista Del Mar recalls a bygone age of wackadoodle comedy |Alissa Wilkinson |February 12, 2021 |Vox 

Corado was in El Salvador on vacation and she did not hesitate to attend the event to support Menjívar once she found out about it. Ruby Corado backs transgender Central American Parliament candidate |Michael K. Lavers |February 11, 2021 |Washington Blade 

The compromise she set with the hotel union and one of the platforms would cap the number of permits the city gives for vacation rentals at about 6,500, including more than 1,000 just for Mission Beach. What’s Behind the Effort to Recall Council President Jen Campbell |Scott Lewis |February 9, 2021 |Voice of San Diego 

All three taste like being on vacation, a thing people used to do. From the Cut: 33 Valentine’s Day Gifts for the Foodie in Your Life |The Cut Staff |February 8, 2021 |Eater 

We also want to take care of all of the folks who love to print on the go, whether that be pictures from a recent vacation or driving directions for off-road adventures. Best all-in-one printer: Upgrade your home office with these multitasking machines |Carsen Joenk |February 8, 2021 |Popular-Science 

The rapid rise of the sharing economy is changing the way people around the world commute, shop, vacation, and borrow. Why Do ‘Progressives’ Want to Ban Uber and AirBnB? |Adam Thierer, Christopher Koopman |December 30, 2014 |DAILY BEAST 

But Sanders, a representative of the Northeastern vacation state of Vermont, also opposes fossil fuel development. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

Christmas Vacation was the last production from the original company that started with Animal House. Dinner at Nitehawk Cinema: ‘Christmas Vacation’ and a Beer in a Pear Tree |Rich Goldstein |December 12, 2014 |DAILY BEAST 

Her father split by the time she was 11—he moved out while the rest of the family was on vacation. A First Lady of Punk Rock Talks |Justin Jones |December 9, 2014 |DAILY BEAST 

Prepare for takeoff, because quality vacation time will certainly boost your mood. 9 Ways to Cope With Seasonal Affective Disorder |DailyBurn |December 5, 2014 |DAILY BEAST 

There are three ways in which a tourist may obtain a good idea of Britain during a summer's vacation of three or four months. British Highways And Byways From A Motor Car |Thomas D. Murphy 

It made us feel that one ought to have two or three years to explore Britain instead of a single summer's vacation. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Sent Alfaretta around to tell us that 'he'd overdone hisself and was obliged to take a vacation.' Dorothy at Skyrie |Evelyn Raymond 

Jess and her mother moved during 115 the short holiday vacation. The Girls of Central High on the Stage |Gertrude W. Morrison 

As it was vacation week, she let Jess go right ahead to settle things while she stuck to the typewriter. The Girls of Central High on the Stage |Gertrude W. Morrison