Social media accounts like Lincoln’s exist to help parents and teenagers navigate misinformation. A feminine wash for teens? Angry parents and gynecologists are on a social media crusade. |Abigail Higgins |February 12, 2021 |Washington Post 

By its third installment, the once-clever “To All the Boys” franchise can only hope to pique the interest of bored teenagers. It was time for the ‘To All the Boys’ franchise to end |Sonia Rao |February 12, 2021 |Washington Post 

In Loudoun County, the number of infections among people under 30 more than doubled after dozens of teenagers and young adults attended a massive “beach week” celebration in South Carolina. 900,000 infected. Nearly 15,000 dead. How the coronavirus tore through D.C., Maryland and Virginia. |Rebecca Tan, Antonio Olivo, John D. Harden |February 5, 2021 |Washington Post 

“My goal was not to challenge his thought process or ideology but to get to a point where he could do it on his own,” said Buckley, who worked with the teenager. After Capitol riots, desperate families turn to groups that ‘deprogram’ extremists |Paulina Villegas, Hannah Knowles |February 5, 2021 |Washington Post 

Even in households with older kids and teenagers, the same imbalance remains. We Need More Feminist Dads - Issue 95: Escape |Jordan Shapiro |February 3, 2021 |Nautilus 

“I have a lot of memories as a teenager that helped,” she smiles. ‘Zero Motivation’: the Funny Side of the IDF |Melissa Leon |December 8, 2014 |DAILY BEAST 

But when the announcement came, there was no surprise: Officer Darren Wilson will not stand trial for killing a teenager. Raging Protesters Set Ferguson on Fire |Justin Glawe |November 25, 2014 |DAILY BEAST 

The teenager was shaken by the incident, and his father remembers having to console him for hours that day. In Jerusalem Home Demolitions, the Biblical Justice of Revenge |Creede Newton |November 25, 2014 |DAILY BEAST 

“As a teenager, I tried to convince myself I had imagined it,” wrote Bowman. Bill Cosby’s Long List of Accusers (So Far): 18 Alleged Sexual Assault Victims Between 1965-2004 |Marlow Stern |November 24, 2014 |DAILY BEAST 

On Labor Day weekend, the teenager accompanied Shattuck, her three children, and several of their friends to Bethany beach. From Baltimore Ravens Cheerleader to Mrs. Robinson |Brandy Zadrozny |November 6, 2014 |DAILY BEAST 

Since the age of nineteen, when a normal teenager would have been in school, preparing himself for life. Revolution |Dallas McCord Reynolds 

I say he was a little boy because I am sure he was an early teenager. Warren Commission (8 of 26): Hearings Vol. VIII (of 15) |The President's Commission on the Assassination of President Kennedy 

Would you say he was more spoiled than the average teenager? Warren Commission (8 of 26): Hearings Vol. VIII (of 15) |The President's Commission on the Assassination of President Kennedy 

Ben flipped the car registration slot to "stand-by" and went back to the teenager's car. Code Three |Rick Raphael 

A freckled-faced teenager in a dinner jacket was in the driver's seat and had the blister window open. Code Three |Rick Raphael