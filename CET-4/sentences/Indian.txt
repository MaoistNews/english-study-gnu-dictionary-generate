The state also elected Republican Nikki Haley, an Indian-American, as governor. Steve Scalise Shows There’s a Fine Line Between Confederate & Southern |Lloyd Green |January 2, 2015 |DAILY BEAST 

They castigated the captain, a 48-year-old Indonesian, and his rookie copilot, a 24-year-old Indian. Who Will Get AsiaAir 8501’s Black Boxes? |Clive Irving |December 30, 2014 |DAILY BEAST 

The feisty airline is the brainchild of entrepreneur Tony Fernandes, a Malaysian of Indian descent who also is a British citizen. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

Poolaw spent most of his life (1906—84) documenting Indian subjects. The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 

Eid, who teaches Indian law at two law schools and works as an attorney himself, had no idea that it was coming. Tribes to U.S. Government: Take Your Weed and Shove It |Abby Haglage |December 13, 2014 |DAILY BEAST 

Alessandro turned a grateful look on Ramona as he translated this speech, so in unison with Indian modes of thought and feeling. Ramona |Helen Hunt Jackson 

The doors (Indian bungalows have hardly any windows, each door being half glass) were open front and back. The Red Year |Louis Tracy 

Had not this Indian plant been discovered, the whole history of some portions of America would have been far different. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The Indian turned his head, and spoke to some one behind; one after another a score of figures rose. Ramona |Helen Hunt Jackson 

As he was toiling slowly up a narrow, rocky pass, he suddenly saw an Indian's head peering over the ledge. Ramona |Helen Hunt Jackson