So for the first time we were offered a reprieve and we were so grateful when we found out Poles are moving into a neighborhood. Tan France Goes Deep on Racism and When He Almost Quit ‘Queer Eye’ |Eugene Robinson |September 3, 2020 |Ozy 

The benefit is tons of comments all the time from people about how grateful they are to have an opportunity to be outdoors, to see the kids smiling. Pick Your Poison |Nick Mancall-Bitel |September 3, 2020 |Eater 

We’re just grateful to receive so much organic visibility since we’ve never worked with influencers in a paid capacity. The founders of skin care startup Starface on normalizing uncomfortable conversations |Rachel King |August 31, 2020 |Fortune 

Despite the lack of mobility, Edmondson is grateful for the affordability and flexibility living in her van has allowed during so much uncertainty. The New Camper Companies Redefining Road Travel |Alex Temblador |August 27, 2020 |Outside Online 

By Saturday, Mullenweg tweeted he was “very grateful that folks at Apple re-reviewed” the WordPress app. Why Apple let WordPress walk but continues to fight Fortnite’s Epic Games |rhhackettfortune |August 25, 2020 |Fortune 

As I am in most restaurants, I was grateful for any modifications that could be made. I Ate Potato Pancakes Til I Plotzed |Emily Shire |December 17, 2014 |DAILY BEAST 

They provide their own assistance and we are grateful to them for this. Ukraine Militias Warn of Anti-Kiev Coup |Jamie Dettmer |November 28, 2014 |DAILY BEAST 

She realized that turkeys have, as she puts it, “nothing to be grateful for” each year on the last Thursday of November. A Turkey's View of Thanksgiving |The Daily Beast Video |November 26, 2014 |DAILY BEAST 

After walking museum hallways my entire life, I am grateful for the opportunity to be on the other side of the wall. Blurred Lines at NY Sketchbook Museum |Daniel Genis |November 1, 2014 |DAILY BEAST 

They can only be grateful to be equipped and trained with full body hazmat suits complete with hooded face masks. Ebola Nurses Are As Brave As Soldiers |Michael Daly |October 17, 2014 |DAILY BEAST 

Alessandro turned a grateful look on Ramona as he translated this speech, so in unison with Indian modes of thought and feeling. Ramona |Helen Hunt Jackson 

He turned to the gentle accents of his sweet Alice, breathed in a letter which had been wet with her grateful tears. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

At sight of this generous enemy, this faithful friend, how could he restrain the grateful impulse to fling himself into his arms! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

It was a life full of freedom, and I shall never cease to be grateful for it, but I must go home soon and look after my affairs. Ancestors |Gertrude Atherton 

Are you sufficiently grateful to me for having quelled your matrimonial ardour of two months ago? St. Martin's Summer |Rafael Sabatini