In summer, the tiny camp café hosts an all-you-can-eat pancake breakfast every morning. Travel Back in Time at Mesa Verde |Emily Pennington |September 27, 2020 |Outside Online 

The café reintroduces the idea that the landscape there belongs to the people. A pop-up winery to pair with your pops |Evan Caplan |September 19, 2020 |Washington Blade 

Pre-pandemic, they were set to open a café inside, but as a permanent indoor restaurant no longer became viable, Hilton says that the parties agreed to imagine an environment in plein air to enjoy sips and bites overlooking the elegant Potomac. A pop-up winery to pair with your pops |Evan Caplan |September 19, 2020 |Washington Blade 

Jennifer Windbeck, senior vice president and head of branches, cafés, and private banking at Capital One, says women no longer have to present as “male” to make it in the field. The Best Workplaces for Women are expanding the turf for women in finance |lbelanger225 |September 18, 2020 |Fortune 

We parents hung back on the sidewalk and happily waved them off before grabbing an obligatory coffee at the nearby café. A flurry of M&A deals lifts global stocks. Yes, even tech stocks |Bernhard Warner |September 14, 2020 |Fortune 

As a cafe in Sydney, Australia came under siege by a hostage-taking gunman on Monday, those nearby attempted to flee the area. In Defense of Uber’s Awful Sydney Surge Pricing |Olivia Nuzzi |December 16, 2014 |DAILY BEAST 

America already has a cereal cafe, Cereality, which has a store in Virginia and at the Dallas Fort Worth airport. Cereal Cafe’s Big Bowl of Hate |David Levesley |December 14, 2014 |DAILY BEAST 

But in October 2010, Palestinian security forces stormed into an Internet cafe and arrested me. What It’s Like to Be an Atheist in Palestine |Waleed al-Husseini, Movements.Org |December 8, 2014 |DAILY BEAST 

Boyfriend, a New Orleans-based rapper who prefers not to reveal her real name, gets up from her decaf cafe au lait. From Church of Christ to Pansexual Rapper |Tyler Gillespie |November 28, 2014 |DAILY BEAST 

In January, an attack on a Lebanese cafe popular with expats left 21 people dead. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

They take in a couple of French papers at this cafe, and the same number of Belgian journals. Little Travels and Roadside Sketches |William Makepeace Thackeray 

Their generosity did not suffice for his dissipations, his cafe bills and his unbridled taste for billiards. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

No long endless tables and big red velvet divans, as in a cafe! A Summary History of the Palazzo Dandolo |Anonymous 

At Tarascon, in a cafe, an hour ago; fifteen men attacked me, and I seized a knife to defend myself. File No. 113 |Emile Gaboriau 

I tore off the wrappings and spread out the diamonds on the cafe table; I could not believe they were real. In the Fog |Richard Harding Davis