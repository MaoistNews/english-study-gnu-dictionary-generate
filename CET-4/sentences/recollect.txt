By the end, I could probably recollect every blade of grass. James Cross, British diplomat kidnapped by Quebec separatists, dies at 99 of covid-19 |Emily Langer |January 21, 2021 |Washington Post 

Once these memories are formed, a partial prompt can easily have you recollect them. Person, Woman, Man, Camera, TV - Issue 93: Forerunners |Adithya Rajagopalan |December 2, 2020 |Nautilus 

Without it, we couldn’t form and recollect distinct and meaningful memories since many aspects of any two memories would otherwise overlap. Person, Woman, Man, Camera, TV - Issue 93: Forerunners |Adithya Rajagopalan |December 2, 2020 |Nautilus 

Within it, she recollected her early years, spinning the dispersed wool of her babyhood into the tangled threads of her childhood. Five Scientists on the Heroes Who Changed Their Lives - Issue 93: Forerunners |Alan Lightman, Hope Jahren, Robert Sapolsky, |December 2, 2020 |Nautilus 

However, in some cases, memory can become pathologically persistent, as when the memory of a traumatic experience—like an assault or a soldier’s time in combat—is recollected incessantly. You can’t completely trust your memories |David Linden |September 30, 2020 |Popular-Science 

I stooped down and asked him how he felt himself, but he made no answer, and evidently did not recollect me. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

They buried her body in the Recollect convent, with the greatest pomp possible. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

He does not recollect the duty the engine performed with the cylindrical boilers. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

We recollect sharing in the despondency, and even despair, which paralysed our party. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Hartledon sent his thoughts back, endeavouring to recollect what could have given rise to this charge. Elster's Folly |Mrs. Henry Wood