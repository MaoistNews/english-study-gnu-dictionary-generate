And more I cannot explain/but you, from what I did not say/will infer what I do not say. Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 

Compston managed all this without the say-so of the Courtauld, which sent him a stinging letter. Joshua Compston Was Once the Wunderkind of the British Art World…and Now He’s Been Practically Forgotten |Anthony Haden-Guest |January 17, 2014 |DAILY BEAST 

Marchman takes it as a given that MLB basing its case on the say-so of a sketchy Floridian drug-dealer is preposterous. Major League Baseball Is Right to Punish the Biogenesis Cheats |Michael Brendan Dougherty |June 6, 2013 |DAILY BEAST 

How does Romney give that answer on national television and force his campaign into what-he-really-meant-to-say mode? Mitt Romney Muddles His Message on Health Care |Howard Kurtz |September 11, 2012 |DAILY BEAST 

Matthew Yglesias on how the right's just-say-no game helped bring the left together. How the GOP Made It Happen |Matthew Yglesias |March 21, 2010 |DAILY BEAST 

Or ne say-je quelle estoit sa maladie; si elle venoit seulement par intervalles, ou non, je n'en say rien: tant y a que le 2. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

ITo heaven cry aloud, and to the world:“Who hath reduced her to this pass?Say, say!” The Poems of Giacomo Leopardi |Giacomo Leopardi 

Maybe Marie would wish then that she had thought twice about quitting him just on her mother's say-so. Cabin Fever |B. M. Bower 

And hast thou fixed my doom, kind master, say?And wilt thou kill thy servant, old and poor? Stories about Animals: with Pictures to Match |Francis C. Woodworth 

And hast thou fixed my doom, sweet master, say?And wilt thou kill thy servant, old and poor? Stories about Animals: with Pictures to Match |Francis C. Woodworth