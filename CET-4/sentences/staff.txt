The litigation highlights what some said is the firm’s extreme approach to departing staff. The losses continue to pile up for hedge fund king Ray Dalio |Bernhard Warner |September 15, 2020 |Fortune 

In 2019, we surveyed our staff, and the results tell us that we have fallen well short of the goal we set. The 2019 TechCrunch Include Report |Henry Pickavet |September 11, 2020 |TechCrunch 

We also distributed a form to staff, which was also volunteer and anonymous. The 2019 TechCrunch Include Report |Henry Pickavet |September 11, 2020 |TechCrunch 

Within days, the Metropolitan Museum of Art announced that it anticipated staying closed through July and laying off much of its staff. How the coronavirus outbreak is roiling the film and entertainment industries |Alissa Wilkinson |September 11, 2020 |Vox 

There are also 12 active staff cases and one staff member who has recovered from the coronavirus. Federal Jail Downtown Now Has One of the Country’s Worst COVID Outbreaks |Maya Srikrishnan |September 10, 2020 |Voice of San Diego 

They took cover inside a print works to the north east of Paris, where they held a member of staff as a hostage. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

Although the blood-spattered offices will be off-limits, staff have vowed to continue producing the magazine. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

You get these high-profile people that go into prison, and the staff abuse their authority. How a ‘Real Housewife’ Survives Prison: ‘I Don’t See [Teresa Giudice] Having a Cakewalk Here’ |Michael Howard |January 6, 2015 |DAILY BEAST 

He would talk to Mecallari and the staff about what was of paramount importance to him, his two sons. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

Grimm has even been actively trying hire staff members for his office in recent weeks after several former aides deserted him. The Felon Who Wouldn’t Leave Congress |Ben Jacobs, David Freedlander |December 23, 2014 |DAILY BEAST 

With each division, in addition to the divisional staff, there were officers detached from the headquarters staff. Napoleon's Marshals |R. P. Dunn-Pattison 

As Felipe approached, the old man's face beamed with pleasure, and he came forward totteringly, leaning on a staff in each hand. Ramona |Helen Hunt Jackson 

By the end of the campaign of 1796 he had proved that he was as great a chief of the staff as Bonaparte was a great commander. Napoleon's Marshals |R. P. Dunn-Pattison 

The commander-in-chief still kept him attached to the headquarter staff, and constantly employed him on special service. Napoleon's Marshals |R. P. Dunn-Pattison 

So it was all arranged, and Lawrence went to see his uncle and tell him of his new position on Blair's staff. The Courier of the Ozarks |Byron A. Dunn