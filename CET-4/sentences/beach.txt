Meyers spent much of his childhood on beaches in and around Los Angeles, becoming a lifeguard after one of his brothers drowned. Bruce Meyers, creator of the first fiberglass dune buggy, dies at 94 |Harrison Smith |February 25, 2021 |Washington Post 

I have a restaurant and it’s in the middle of Florida, but my family moves to the beach every summer. Am I Boring You? (Ep. 225 Rebroadcast) |Stephen J. Dubner |February 25, 2021 |Freakonomics 

A theater like Clear Space adds to the vibrancy of Rehoboth and its role as a beach resort town, Combs said. Rehoboth theater supporters hopeful for approval of new buildings |Lou Chibbaro Jr. |February 23, 2021 |Washington Blade 

Clouds dissolve into Côte d’Azur sun, warming bikini-clad women lounging in beach chairs. In a gray, empty Paris, this corner shop’s colorful posters transport you wherever you want to go |Lily Radziemski |February 19, 2021 |Washington Post 

The beach, apparently, is not as distracting as the office Keurig machine. Was a three-week trip to New Orleans for work or vacation? Both. |Andrea Sachs |February 19, 2021 |Washington Post 

Dinner was a baroque affair, on the beach, a warm breeze gently blowing. Canada ♥ Cuba Just Got Complicated |Shinan Govani |December 22, 2014 |DAILY BEAST 

The city protests that a beach is not a suitable place to feed the hungry. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

One of the three, Ralph Goodwin, is said to have drowned while swimming at a beach outside Havana. Cuba Protects America’s Most Wanted |Michael Daly |December 18, 2014 |DAILY BEAST 

Inside the Miami Beach Convention Center, there is a lot of good art—but more pretty art. Sneer and Clothing in Miami: Inside The $3 Billion Woodstock of Contemporary Art |Jay Michaelson |December 6, 2014 |DAILY BEAST 

She had enrolled at Maimonides University in North Miami Beach in order to work towards a degree in clinical sexology. Whip It: Secrets of a Dominatrix |Justin Jones |November 25, 2014 |DAILY BEAST 

If the "Y" Beach lot press their advantage they may cut off the enemy troops on the toe of the Peninsula. Gallipoli Diary, Volume I |Ian Hamilton 

The two women had no intention of bathing; they had just strolled down to the beach for a walk and to be alone and near the water. The Awakening and Selected Short Stories |Kate Chopin 

The little crowd and the boats on the beach were right under them and no one paid any attention or seemed to be in a hurry. Gallipoli Diary, Volume I |Ian Hamilton 

My thought was to keep pushing in troops from "W" Beach until the enemy had fallen back to save themselves from being cut off. Gallipoli Diary, Volume I |Ian Hamilton 

Going back, found that the lighter loads of wounded already taken off have by no means cleared the beach. Gallipoli Diary, Volume I |Ian Hamilton