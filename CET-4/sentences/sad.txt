“I’m personally sad that two parents would be out to ruin a program, ruin people’s livelihoods because their children didn’t work hard enough or made mistakes,” Clark said. School Sports Became ‘Clubs’ Amid the Pandemic – Now Two Coaches Are Out |Ashly McGlone |September 17, 2020 |Voice of San Diego 

The sad part is that when talking about this topic, I noticed how hard it actually is for Googlers to get the SEO community to change our ways. How to earn your place in Google’s index in 2020 |Bartosz Góralewicz |September 14, 2020 |Search Engine Land 

In a blog post, Amazon simply says that “positivity” measures how happy or sad a voice sounds. How Amazon’s new health tracker will use AI to monitor your tone of voice |Nicolás Rivero |August 28, 2020 |Quartz 

“Rather than do a watered-down, sad version of our television show with one person in a 2,000-square-foot set with nobody in the background, let’s just shift it at home,” said Bishop. ‘A new way of working’: Publishers’ test kitchens return to studios with new safety procedures in the mix |Kayleigh Barber |August 14, 2020 |Digiday 

This is especially interesting when comparing it to unemployment searches, which is a very sad side effect of the economy shut down. Five Google Trends charts that show the impact of COVID-19 |Jason Tabeling |July 9, 2020 |Search Engine Watch 

Yet this, in the end, is a book from which one emerges sad, gloomy, disenchanted, at least if we agree to take it seriously. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Haha, what a sad thing to be great at, but yeah, I guess I am. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

A sad-faced orange Star of David flashed across the iPhone screen as we swiped left on “James” (not his real name). My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

Lady Edith is so sad that her sadness nearly set the whole damned house on fire. ‘Downton Abbey’ Review: A Fire, Some Sex, and Sad, Sad Edith |Kevin Fallon |January 5, 2015 |DAILY BEAST 

The trio formed the Sad Boys collective, with Sherm and Gud on production and Lean manning the mic. The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

The sad end of the mission to King M'Bongo has been narrated in the body of this work. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

All through the sad duties of the next four days Felipe was conscious of the undercurrent of this premonition. Ramona |Helen Hunt Jackson 

She looked up at him with sad and eloquent eyes, which softened his heart in spite of himself. Rosemary in Search of a Father |C. N. Williamson 

It was a sad day for Ramona and Alessandro when the kindly Hyers pulled up their tent-stakes and left the valley. Ramona |Helen Hunt Jackson 

Somehow it made me feel sad to hear it, and a sense of the transitoriness of things came over me. Music-Study in Germany |Amy Fay