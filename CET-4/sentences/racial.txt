That’s really what’s most necessary as a way forward particularly in the moment of racial justice that we’re in as a country. Despite some gains in the past year, Hollywood still has inclusion problems, study says |radmarya |September 10, 2020 |Fortune 

He might have been better off sticking with the pandemic — cases appear to be flattening out ever so slightly, though thousands of new cases are reported daily — than with the red meat of racial politics. Why Trump Might Be Scaring Off Older Voters |Clare Malone (clare.malone@fivethirtyeight.com) |September 10, 2020 |FiveThirtyEight 

That’s because racism and racial inequity are more than just public policy issues. To fight systemic racism, the investment industry needs to look at its whiteness first |jakemeth |September 10, 2020 |Fortune 

As societal attention remains fixed on racial justice, decision-makers are “finding they can do much better” in building businesses that reflect the communities they serve, Heidrick and Struggles’ report says. Nearly half of open board seats went to women in 2019. Only 23% were filled by people of color |ehinchliffe |September 10, 2020 |Fortune 

Given this summer’s call for social change, the first episode feels especially timely, as Atticus road-trips from Chicago to Massachusetts with the threat of deadly racial violence lurking at every pit stop. Everything Our Editors Loved in August |The Editors |September 10, 2020 |Outside Online 

The attempt to “breed back” the Auroch of Teutonic legend was of a piece with the Nazi obsession with racial purity and eugenics. ‘Nazi Cows’ Tried to Kill British Farmer |Tom Sykes |January 6, 2015 |DAILY BEAST 

It demands an end to poverty and racial injustice, to which we are totally committed in our time. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

The state was in a deep recession as Duke galvanized a racial backlash. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

This was later repurposed in Europe as an explanation for racial superiority, and the term “Aryan” came to define a white race. The Himalayas’ Hidden Aryans |Nina Strochlic |January 3, 2015 |DAILY BEAST 

He first rose to prominence as a lawyer in Queens, who settled a boiling racial dispute over public housing in Forest Hills. Mario Cuomo: An OK Governor, but a Far Better Person |Michael Tomasky |January 2, 2015 |DAILY BEAST 

Like all racial beauties, bred by selection, she needed the arts of dress and furnishings to frame her. Ancestors |Gertrude Atherton 

Sin Sin Wa performed a curious shrugging movement, peculiarly racial. Dope |Sax Rohmer 

Racial segregation in the public schools of Virginia was provided for in the Constitution of 1902. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Racial segregation in the public schools of Virginia was constitutionally established in the Underwood Constitution of 1902. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

It was the clutch of something racial and inherited—a something which the Northerner hardly knows. Marriage la mode |Mrs. Humphry Ward