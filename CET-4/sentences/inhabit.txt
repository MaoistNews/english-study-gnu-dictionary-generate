About a million years ago there was one known species of mammoth inhabiting Siberia, the steppe mammoth. We Sequenced the Oldest Ever DNA From Million-Year-Old Mammoths |David Díez-del-Molino |February 21, 2021 |Singularity Hub 

Caught at an impasse, “So They Say,” is an offering from his as-yet-to-be announced second project, and allows Paige to express his gut feelings about the world he inhabits. Tarriona “Tank” Ball’s Playlist is Pure Friend Goals |Brande Victorian |February 19, 2021 |Essence.com 

What’s more, some hairy behemoths that inhabited North America may have been a hybrid mix between the woolly mammoth and a previously unknown mammoth species, researchers report February 17 in Nature. The oldest animal DNA ever recovered reveals mammoths’ evolution |Erin Garcia de Jesus |February 17, 2021 |Science News 

Police work, she argues, exposes what a “complicated” moral universe we inhabit. A Georgetown professor trades her classroom for a police beat |Ronald S. Sullivan Jr. |February 12, 2021 |Washington Post 

As someone who grew up 30 minutes outside the city, I never thought wild game would inhabit any part of the Five Boroughs. Four wild animals that are thriving in cities |By Ryan Chelius/Outdoor Life |February 9, 2021 |Popular-Science 

Actors can inhabit the person through the sheer force of their assimilation. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

Jarrett is the first person to fully inhabit this newly dominant role. Valerie Jarrett, Obama Consigliere—and Democracy Killer |James Poulos |November 12, 2014 |DAILY BEAST 

The Universe we inhabit seems to be four-dimensional: the three dimensions of height, length, and depth, along with time. Is the Cosmos Just a Big Hologram? |Matthew R. Francis |August 31, 2014 |DAILY BEAST 

There is no sign of the beefy NHL players who usually inhabit the place. The Most Depressing Show on Earth: Amongst the Clowns of Newark |Lizzie Crocker |March 16, 2014 |DAILY BEAST 

To try to do your best to inhabit a character, you judge them to the extent that you judge yourself. Scandal’s Most Scandalous Character: Jeff Perry on Playing Cyrus |Kevin Fallon |February 28, 2014 |DAILY BEAST 

Meeting the thirsty bring him water, you that inhabit the land of the south, meet with bread him that fleeth. The Bible, Douay-Rheims Version |Various 

What New France is, the nature of the country, what tribes inhabit it, and their customs. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

And they shall build houses, and inhabit them; and they shall plant vineyards, and eat the fruits of them. The Bible, Douay-Rheims Version |Various 

They inhabit the small intestine, usually in great numbers, and commonly produce a severe and often fatal anemia. A Manual of Clinical Diagnosis |James Campbell Todd 

And Asor shall be a habitation for dragons, desolate for ever: no man shall abide there, nor son of man inhabit it. The Bible, Douay-Rheims Version |Various