Richardson said she stayed late at work at the Atwood Building and drafted a resignation letter for Mallott, unsure if he wanted to use it. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

Here is what happened according to Potts, as well as interviews with her daughter and father, Walker, chiefs of staff for both Walker and Mallott, Mallott’s son Anthony and others who watched the resignation unfold. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

Since Rapoport’s resignation, which many of Bon Appétit’s most prominent staff endorsed, the food publication has faced a near-constant stream of resignations and call-outs from current staff. Bon Appétit Announces Dawn Davis, Publishing Heavyweight, as New Editor-in-Chief |Elazar Sontag |August 27, 2020 |Eater 

I had gotten a resignation letter and I knew I was going to pull it out one day. Full Transcript: Sean Spicer on ‘The Carlos Watson Show’ |Daniel Malloy |August 26, 2020 |Ozy 

Cybele Thompson, who has served as the city’s real estate chief since 2014, submitted a brief resignation letter to Chief Operating Officer Kris Michell on Monday. City’s Real Estate Assets Director Resigns Amid Scrutiny Over Ash Street Deal |Lisa Halverstadt and Jesse Marx |August 4, 2020 |Voice of San Diego 

In response, Greenberg offered the Times his resignation, which was refused. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

Residents of Rome even called for his resignation for defying city ordinances. The Mayor Who Took Down the Mafia That Ruined Rome |Barbie Latza Nadeau |December 6, 2014 |DAILY BEAST 

They seized key government buildings and forced the resignation of then Prime Minister Mohammed Basindwa. Yemen’s a Model All Right—For Disaster |Michael Shank , Casey Harrity |November 14, 2014 |DAILY BEAST 

Just days before his resignation, Driscoll had been confronted with the results of the investigation by church elders. Megachurch Mars Hill To close Doors: What Does the Future Hold Now? |Warren Throckmorton |November 2, 2014 |DAILY BEAST 

Because when it comes to domestic violence, that kind of resignation can be lethal. The Worst Question for Abuse Victims |Emily Shire |October 20, 2014 |DAILY BEAST 

His attitude was one of hopeless resignation as he looked toward a distant bird winging its flight away from him. The Awakening and Selected Short Stories |Kate Chopin 

If he fall, God has his wise purpose in the judgement, and I shall find resignation. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

With resignation he made the necessary preparations for the trip, and taking Orlean with him, went to the small town. The Homesteader |Oscar Micheaux 

Tom took his passage home; he also told Sir William that his resignation, whether the Board accepted it or not, was final. The Wave |Algernon Blackwood 

The resignation of Sunderland had put many honest gentlemen in good humour. The History of England from the Accession of James II. |Thomas Babington Macaulay