Thanks to ranching techniques used by The Perennial’s suppliers, one pound of beef is offset by 45 pounds of carbon sequestered in the soil. This restaurant duo want a zero-carbon food system. Can it happen? |Bobbie Johnson |September 24, 2020 |MIT Technology Review 

Everything from lumber, coins, ammo, beef, to medical supplies has been affected. Looking to a buy a car? A national shortage means you’ll probably pay more |Lance Lambert |September 20, 2020 |Fortune 

This is the biggest issue with lab-grown meat, whether beef, pork, or fish—it’s difficult and costly to scale up its production. This Startup Is Growing Sushi-Grade Salmon From Cells in a Lab |Vanessa Bates Ramirez |September 16, 2020 |Singularity Hub 

McGunagle will jump out of the plane carrying supplies that include drinking water, food like beef jerky or peanut butter, solution for his contact lenses, and a pound of coffee. How aerial firefighters battle blazes from the skies |Rob Verger |August 27, 2020 |Popular-Science 

Other biotech techniques hope to improve the beef industry without displacing it. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

They became so brown and shriveled that they looked like walking beef jerky with New York accents. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

Her first celebrity beef began when Snoop Dogg started Instagramming some pretty vile things about her, seemingly unprovoked. Solange Smacks Jay Z, Legolas Slaps Bieber, and the Biggest Celebrity Feuds of the Year |Amy Zimmerman |December 24, 2014 |DAILY BEAST 

Champagne, which is also acidic, offers a nice complement to anything from tuna tartare to beef bourguignon. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

Veselka layered its latke with pork goulash, and Toloache added beef short rib chorizo. I Ate Potato Pancakes Til I Plotzed |Emily Shire |December 17, 2014 |DAILY BEAST 

The freezer is filled with meat, sides of beef and large pieces of lamb. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

While this was being done, Ramona would dry the beef which would be their supply of meat for many months. Ramona |Helen Hunt Jackson 

At last, a sirloin of beef was set before him, on which his empty stomach made him feed voraciously. The Book of Anecdotes and Budget of Fun; |Various 

She's too old for beef, or the butcher would; and she makes out to get her livin' without botherin' nobody much. Dorothy at Skyrie |Evelyn Raymond 

The ship's company were supplied daily with fresh beef and vegetables. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Stuffing their crackers and dried beef into their pockets, they ran out of the house and to their machines. Motor Matt's "Century" Run |Stanley R. Matthews