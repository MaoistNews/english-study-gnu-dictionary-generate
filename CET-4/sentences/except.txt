Except the Braves did not win 14 straight pennants (they did win 14 straight division titles), and Smoltz is a also Republican. Conservative Curt Says His Politics, Not His Pitching, Kept Him Out of the Hall of Fame |Ben Jacobs |January 9, 2015 |DAILY BEAST 

I always wanted to have a career like his—except for the stopping work thing. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

In fact, Mexico buys and sells more US goods than any other country on the planet except for Canada. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

Except, Nomadness currently has 3,000 pending membership requests. ‘We Out Here’: Inside the New Black Travel Movement |Charlise Ferguson |January 4, 2015 |DAILY BEAST 

Author J.K. Rowling says all religions are present at her beloved wizard school—except Wiccans. Harry Potter and the Torah of Terror |Candida Moss, Joel Baden |January 4, 2015 |DAILY BEAST 

In fact, except for Ramona's help, it would have been a question whether even Alessandro could have made Baba work in harness. Ramona |Helen Hunt Jackson 

I haven't much time for seeing any one, except my patients, and the people I meet in society. Bella Donna |Robert Hichens 

The patriarchal decree of the government was a good deal of a joke on the plains, anyway—except when you were caught defying it! Raw Gold |Bertrand W. Sinclair 

The camp grew still, except for the rough and ready cook pottering about the fire, boiling buffalo-meat and mixing biscuit-dough. Raw Gold |Bertrand W. Sinclair 

Who he could not make out, except that it was a Kirton: and it prayed him to hasten down immediately. Elster's Folly |Mrs. Henry Wood