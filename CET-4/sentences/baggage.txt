The decision is part of the company’s aim to refocus on its core brand, an effort that has included ditching excess baggage like its Christmas Tree Shops stores and fixing its finances. Why Adidas is finally putting Reebok up for sale |Phil Wahba |December 14, 2020 |Fortune 

Pilots, flight attendants, baggage handlers and other workers got notices Thursday that they may be furloughed, Southwest said. Southwest Airlines warns nearly 13% of workforce at risk of layoffs after punishing year for travel |kdunn6 |December 4, 2020 |Fortune 

When firefighters arrived, he said, they found that an Amtrak employee was in a golf cart “used to transport baggage” and was struck by a train. Amtrak employee in golf cart struck by train at Union Station |Dana Hedgpeth |December 3, 2020 |Washington Post 

To be clear, Westbrook brings his own baggage and his own massive contract to Washington. Wizards’ deal of John Wall for Russell Westbrook is the right move, but packed with risk |Ben Golliver |December 3, 2020 |Washington Post 

Air travel requires strict adherence to baggage weight limitations, and backpacks and laptop bags used for going between work and home also have a finite capacity even before the toll of excess weight on your body is taken into account. Best wireless earbuds: Five things to consider |PopSci Commerce Team |October 30, 2020 |Popular-Science 

Other officials told reporters that searchers also spotted a life vest and baggage in the water. Wreckage, Bodies of AirAsia Crash Found |Lennox Samuels |December 30, 2014 |DAILY BEAST 

They take home mental baggage unlike anything carried in almost every other job. Any Outrage Out There for Ramos and Liu, Protesters? |Mike Barnicle |December 22, 2014 |DAILY BEAST 

“My parents split up around the time I started writing, which brought in a lot of emotional baggage,” Kiesza says. The Making of Kiesza: From Navy Sharpshooter to Beauty Queen to Pop Diva |Marlow Stern |October 20, 2014 |DAILY BEAST 

There are all sorts of baggage around being a man, and being a woman. Joseph Gordon-Levitt on Why He’s Exploring the Word ‘Feminism’ and Online Misogyny |Marlow Stern |September 22, 2014 |DAILY BEAST 

Much of the baggage that Charles carries is universally known. Imagining Prince Charles as King Makes All of Britain Wish They Could Leave Like Scotland |Clive Irving |September 17, 2014 |DAILY BEAST 

Well, if you do either lose baggage or want to buy a trunk already marked, deuced if I ain't the man to call on. The Book of Anecdotes and Budget of Fun; |Various 

Although the army was greatly demoralised during the retreat through Portugal, he never lost a single gun or baggage wagon. Napoleon's Marshals |R. P. Dunn-Pattison 

Soult's corps arrived without cannon or baggage, a mere armed rabble, and Ney's men jeered at the disorganised battalions. Napoleon's Marshals |R. P. Dunn-Pattison 

After her baggage is on the carriage, drive immediately to the house, and be certain all is ready there for her comfort. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Robert occupied the abbeys of Biland and Rievaulx and divided the spoils of the English camp and the king's baggage. King Robert the Bruce |A. F. Murison