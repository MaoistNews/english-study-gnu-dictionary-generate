Therefore, cancellation of the Pride events due to the pandemic has created a difficult and unique challenge, while providing unconventional opportunities and new possibilities. Time for a new chapter of Capital Pride |Capital Pride Alliance |October 1, 2020 |Washington Blade 

She took great pride in her Jewish heritage but broke with most traditional Jewish practices. In Death, As In Life, Ruth Bader Ginsburg Balanced Being American And Jewish |LGBTQ-Editor |September 25, 2020 |No Straight News 

Police officers regularly argue — not always as a point of pride — that they’re held to a higher standard than the general public. Sheriff’s Department Made Excuses for Captain Who Pleaded Guilty to Arms Dealing |Jesse Marx |September 23, 2020 |Voice of San Diego 

I have started one or two small new businesses, but this is my pride and joy and passion, because I know that doing this saves lives, babies’ lives, children’s lives. ‘We’re racing against the clock’: The CEO of the Serum Institute of India on his company’s COVID-19 vaccine campaign |Erika Fry |September 21, 2020 |Fortune 

She had more than 15 years’ experience in law enforcement and took pride in interviewing children who had been put in terrible situations, she said. Her Stepfather Admitted to Sexually Abusing Her. That Wasn’t Enough to Keep Her Safe. |by Nadia Sussman |September 18, 2020 |ProPublica 

And when we had Pride, we put up signs and some people would take them down. How A Company’s Support of Gay Employees Helped One of Them To Come Out | |December 24, 2014 |DAILY BEAST 

But such an approach works against the traditional pride in self-sufficiency espoused by many in the American middle class. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

To many of us, that smacks of censorship, the highest offense to our pride in self-publicity. On Torture, Chuck Johnson & Sondheim |James Poulos |December 13, 2014 |DAILY BEAST 

The event saw well over 100,000 attendees last year making it one of the best-attended pride events in the country. The Ultimate LGBT Travel Bucket List | |December 12, 2014 |DAILY BEAST 

So I was happy to see that the European theory of terroir was in action, promoting with pride the qualities of a specific region. Beer Countries vs. Wine Countries |Clive Irving |December 7, 2014 |DAILY BEAST 

Liszt gazed at "his Hans," as he calls him, with the fondest pride, and seemed perfectly happy over his arrival. Music-Study in Germany |Amy Fay 

E was an Esquire, with pride on his brow; F was a Farmer, and followed the plough. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

Thou fell spirit of pride, prejudice, ignorance, and mauvaise honte! Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Man's enthusiasm in praise of a fellow mortal, is soon damped by the original sin of his nature—rebellious pride! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

And he replied shortly, and with a slight charming affectation of pride: "I did without." Hilda Lessways |Arnold Bennett