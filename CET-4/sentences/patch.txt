In higher-latitude regions, such as the Tasman Sea, relief tended to be much closer, within a few tens of kilometers of the overheated patch, the researchers found. Species may swim thousands of kilometers to escape ocean heat waves |Carolyn Gramling |August 10, 2020 |Science News 

The officers’ uniforms bear a large patch that says “police,” but they aren’t police. What Happened In Portland Shows Just How Fragile Our Democracy Is |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |August 5, 2020 |FiveThirtyEight 

Microraptor’s shorter feathers appear in just a small patch on one of the dinosaur’s four wings — suggesting that the dinosaur molted sequentially, too, bird ecologist Yosef Kiat at the University of Haifa in Israel and colleagues report. This dinosaur may have shed its feathers like modern songbirds |Carolyn Gramling |July 16, 2020 |Science News 

Other times, they arranged patches of spikes in different directions. Shape-shifting cuts give shoes a better grip |Carolyn Wilke |July 14, 2020 |Science News For Students 

Broken pipes and pumps are fixed in patches when money comes through from both federal governments or via the North American Development Bank. Local Groups Pause Tijuana Sewage Lawsuits, But Solutions Are Still Far Off |MacKenzie Elmer |July 8, 2020 |Voice of San Diego 

Kocurek now works 12-hour shifts as a night watchman guarding the entrance to a drilling patch. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

Annie Leibovitz had hit a pretty rough patch in life by 2009. Annie Leibovitz Talks About ‘Pilgrimage,’ Susan Sontag, Vogue & More |Justin Jones |November 20, 2014 |DAILY BEAST 

But the illusions of peace and tranquility soon crumble around them like a patch of freshly laid snow. ‘Force Majeure’ and the Swedish Family Vacation From Hell |Alex Suskind |October 27, 2014 |DAILY BEAST 

There was a patch of congealed blood behind his head: “Except for the blood…the dead man looked immaculate.” How to Get Away With a Hollywood Murder |Tim Teeman |October 10, 2014 |DAILY BEAST 

A powder-blue blazer with a patch reading “All-Time All-American” hung in a clear plastic bag from the closet doorknob. Football Great Bob Suffridge Wanders Through the End Zone of Life |Paul Hemphill |September 6, 2014 |DAILY BEAST 

A few, very few, little dots had run back over that green patch—the others had passed down into the world of darkness. Gallipoli Diary, Volume I |Ian Hamilton 

In a minute Bruce was back with his hat full of water from the creek that whimpered just beyond the willow patch. Raw Gold |Bertrand W. Sinclair 

The patch of soft green that I knew for the cottonwoods Rutter had spoken of drew my roving gaze whether I would or no. Raw Gold |Bertrand W. Sinclair 

A patch of light fell clear on the side of the trap, and on Longcluse's ungloved hand as he leaned on it. Checkmate |Joseph Sheridan Le Fanu 

The male Black Redstart has also a white patch on the wing caused by the pale, nearly white, margins of the feathers. Birds of Guernsey (1879) |Cecil Smith