Healthe concedes its entry gate may not be as critical as its 222 nm products focused on air decontamination, but says it a valuable part of its layered approach to disinfection. Can far-UVC light reduce the spread of COVID-19 indoors? |Erika Fry |August 31, 2020 |Fortune 

One of the first major tech companies out of the gate with a questionably useful product is LG. LG’s battery-powered face mask will “make breathing effortless” |Ron Amadeo |August 27, 2020 |Ars Technica 

FinFET maintained fine control of current by surrounding the channel with a gate on three sides. Moore’s Law Lives: Intel Says Chips Will Pack 50 Times More Transistors |Jason Dorrier |August 23, 2020 |Singularity Hub 

A transistor is “on” when the gate allows current to flow, and it’s off when no current flows. Moore’s Law Lives: Intel Says Chips Will Pack 50 Times More Transistors |Jason Dorrier |August 23, 2020 |Singularity Hub 

If women’s soccer became popular and could attract those kinds of gates, that would take away from the men’s professional team. Should America (and FIFA) Pay Reparations? (Ep. 426) |Stephen J. Dubner |July 16, 2020 |Freakonomics 

The Florida GOP senator stormed out of the gate Wednesday in the highest of dudgeons. Rubio’s Embargo Anger Plays to the Past |Michael Tomasky |December 19, 2014 |DAILY BEAST 

Said it was like speed dating because he was late after hitting every wrong gate on the lot. Exclusive: Sony Emails Slam Leonardo DiCaprio, Willow and Jaden Smith, Gush Over Ryan Gosling |William Boot |December 13, 2014 |DAILY BEAST 

They dumped his body at the gate of a black cemetery—his head and right arm were gone. Greil Marcus Talks About Trying to Unlock Rock and Roll in 10 Songs |Allen Barra |November 17, 2014 |DAILY BEAST 

“We shoot at Sunset Gower Studios, and you can see the street through the gate,” he says. Jeff Daniels Defends Aaron Sorkin and the ‘Dumb and Dumber’ Toilet Scene |Kevin Fallon |November 7, 2014 |DAILY BEAST 

In earlier centuries academies existed to decide what was art, while today we have gallerists and critics at the gate. Blurred Lines at NY Sketchbook Museum |Daniel Genis |November 1, 2014 |DAILY BEAST 

Yet he feared to meet her eyes, and was glad of a saluting sepoy who swaggered jauntily past the open gate. The Red Year |Louis Tracy 

At once the sepoys at the Kashmir Gate fired a volley at the nearest officers, of whom three fell dead. The Red Year |Louis Tracy 

First through the gate came a company of Korean foot-soldiers, in blue uniforms. Our Little Korean Cousin |H. Lee M. Pike 

Bride and bridegroom, accompanied by the weeping crowd, proceeded to the castle gate. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

A rough track led to the gate, and Frank knocked loudly on an iron-studded door. The Red Year |Louis Tracy