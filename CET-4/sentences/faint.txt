You can still see them going by, but barely—they’re very faint. Satellite mega-constellations risk ruining astronomy forever |Neel Patel |September 2, 2020 |MIT Technology Review 

On the sides, a lean premixed flame appears as a faint wisp. Four types of flames join forces to make this eerie ‘blue whirl’ |Emily Conover |August 12, 2020 |Science News 

Researchers have started to make rough Faraday rotation measurements using LOFAR, but the telescope has trouble picking out the extremely faint signal. The Hidden Magnetic Universe Begins to Come Into View |Natalie Wolchover |July 2, 2020 |Quanta Magazine 

By training computers to recognize such faint rumbles, the scientists were able not only to identify the probable culprit behind the quakes, but also to track how such mysterious swarms can spread through complex fault networks in space and time. Machine learning helped demystify a California earthquake swarm |Carolyn Gramling |June 18, 2020 |Science News 

When that faint light hits the other side, it’s already been split apart into its colors. Explainer: Rainbows, fogbows and their eerie cousins |Matthew Cappucci |May 1, 2020 |Science News For Students 

Her voice was raspy and after answering questions she paused, as if about to faint. Jeopardy! Champion Julia Collins’s Brain Feels Like Mush |Sujay Kumar |November 20, 2014 |DAILY BEAST 

I saw a faint, sweet glimmer of the ferocious protector he once was. No One Ever Loses to Cancer |Dushka Zapata |October 8, 2014 |DAILY BEAST 

But this time I can plainly hear, through the rush of words, the faint rattle of hysteria that bespeaks a screw loose somewhere. The Stacks: Grateful Dead I Have Known |Ed McClanahan |August 30, 2014 |DAILY BEAST 

After about an hour, he hears a faint tapping sound from inside the freezer and opens the door. Robin Williams, Hollywood’s Grand Jester, Is Dead at 63 |Marlow Stern |August 12, 2014 |DAILY BEAST 

A fine book all around, but not a book for the faint of heart. The Gestapo Still Sets the Bar for Evil |James A. Warren |July 13, 2014 |DAILY BEAST 

The faint candle-light glimmered on a ponderous gilded cornice, which had also sustained violence. Checkmate |Joseph Sheridan Le Fanu 

It was difficult to describe—a little sterner, a little wilder, a faint emphasis of the barbaric peering through it. The Wave |Algernon Blackwood 

He was looking at me with eyebrows arched, curiously, and there was a faint suggestion of hostility in the set of his mouth. The Soldier of the Valley |Nelson Lloyd 

The cytoplasm of lymphocytes is generally robin's-egg blue; that of the large mononuclears may have a faint bluish tinge. A Manual of Clinical Diagnosis |James Campbell Todd 

Wright's stain gives such cells a faint bluish tinge when the condition is mild, and a rather deep blue when severe. A Manual of Clinical Diagnosis |James Campbell Todd