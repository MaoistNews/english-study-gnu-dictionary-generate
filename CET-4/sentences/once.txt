Once discovered, this maneuver did not endear the councilors to their constituents. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

My doctor insisted that once I filed this piece I lie down on my bed and not get out. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

Every once in a while, they act swiftly and acknowledge the problem. Today’s GOP: Still Cool With Racist Pandering? |Michael Tomasky |January 7, 2015 |DAILY BEAST 

Once I began reading, I realized A Gronking to Remember was a masturbatory tribute to the New England Patriots. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

Where the U.S. once depended on its own forces to determine who was military material, this time the Iraqis will decide. Pentagon Insider on New Plan to Fight ISIS: ‘Of Course It’s Not Enough’ |Nancy A. Youssef |January 6, 2015 |DAILY BEAST 

He held it, but it was without pressure; without recognizance of the delight with which he once grasped it. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

When his lordship retired early, as was his custom, the other men adjourned once more to the billiard-room. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

As Spain, however, has fallen from the high place she once held, her colonial system has also gone down. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Each picture bore a label, giving a true description of the once-honoured gem. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

And once more, she found herself desiring to be like Janet--not only in appearance, but in soft manner and tone. Hilda Lessways |Arnold Bennett