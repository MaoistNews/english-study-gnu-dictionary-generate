His defeat six months later was virtually assured in that moment. The case for Jimmy Carter as a ‘consequential’ president |Russell L. Riley |December 4, 2020 |Washington Post 

It’s a trade opponents are willing to make, because treating Hill like any other receiver, even any other very good receiver, is inviting defeat. What to know from NFL Week 12: Tyreek Hill and Derrick Henry took over as the coronavirus loomed |Adam Kilgore |November 30, 2020 |Washington Post 

They’ll win the Pac-12 North with defeats of California and Washington the next two weeks, and probably go to the Fiesta Bowl if they win the league. College football winners and losers: Pac-12 playoff hopes disappear with Oregon’s loss |Patrick Stevens |November 29, 2020 |Washington Post 

Philadelphia responded to that defeat by firing Coach Brett Brown and replacing him with Doc Rivers, who was let go by the Los Angeles Clippers. Daryl Morey in advanced talks to lead 76ers’ front office |Ben Golliver |October 28, 2020 |Washington Post 

Many classified the incident as a catastrophic defeat for the government, but the analysis from the Mexico Violence Resource Project suggests a more nuanced interpretation of the impact of those events. Border Report: The Asylum Process Is in Limbo |Maya Srikrishnan |October 19, 2020 |Voice of San Diego 

He rebuffed calls to institute the death penalty, and his last term as governor ended in his defeat. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 

After the defeat of ISIS in Sinjar, most other locals have been left wondering who might rule the city in the near future. Has the Kurdish Victory at Sinjar Turned the Tide of ISIS War? |Niqash |December 27, 2014 |DAILY BEAST 

That defeat was driven largely by Romney losing women voters by an insurmountable 11 points. Surprise! The GOP Closed the Gender Gap |Patricia Murphy |December 10, 2014 |DAILY BEAST 

In recent days, there has been a subtle feeling of defeat permeating through the camp. The Monuments Men of Occupy Hong Kong |Brendon Hong |December 4, 2014 |DAILY BEAST 

But it certainly contributed, and purposely so, to the defeat of the tough Likud hardliner Yitzhak Shamir in 1992. The Inside Story of U.S. Meddling in Israel’s Elections |Aaron David Miller |December 4, 2014 |DAILY BEAST 

He saw Gen. Braddock as he passed on to his defeat, and could give a succinct account of that sanguinary action. The Every Day Book of History and Chronology |Joel Munsell 

The friars were exceedingly wroth, and combined to defeat the Generalʼs efforts to come to an understanding with the rebels. The Philippine Islands |John Foreman 

He will tell you about the success he had in America; it quite makes up for the defeat of the British army in the Revolution. Confidence |Henry James 

But after the defeat at Leipzig King Joachim asked and obtained leave to return to his own dominions. Napoleon's Marshals |R. P. Dunn-Pattison 

But she had experienced an hour of mixed emotions in which a confused and wondering sense of defeat was paramount. Ancestors |Gertrude Atherton