On one of their trips, Shrem recalled, the two ventured off a trail and Hedaya had a close encounter with an ox. The Surfside debris has been cleared, but one victim is still missing. Her loved ones want answers. |Brittany Shammas, Meryl Kornfield, Paulina Firozi |July 23, 2021 |Washington Post 

His company, Livestock Wealth, helps anyone with a smartphone invest in a free-range ox or pregnant cow and enjoy annual growth of up to 14 percent. The New Face of Entrepreneurship? |Joshua Eferighe |February 21, 2021 |Ozy 

Google said, “Take this year by the horns—here’s to this next lunar cycle being as strong as an ox!” Diversity and inclusion in search marketing and advertising; Monday’s daily brief |Carolyn Lyden |February 15, 2021 |Search Engine Land 

There's no Nicely-Nicely Johnson hanging out in Times Square these days, no Harry the Horse, no Angie the Ox. New York’s Greatest Show Or How They Did Not Screw Up ‘Guys and Dolls’ |Ross Wetzsteon |April 6, 2014 |DAILY BEAST 

The doctors said it was as strong as an ox, considering he was so sedentary. Gore Vidal’s Tragic Final Decade |Tim Teeman |November 8, 2013 |DAILY BEAST 

But generally speaking, unless their personal ox is being gored, few young women seem willing to make waves. ‘The Feminine Mystique’ at 50, Part 2: Three Feminists on What It Means Today |Jessica Bennett, Letty Cottin Pogrebin, Alisa Solomon |February 12, 2013 |DAILY BEAST 

Whoever's ox Obama chooses to gore will probably be a considerably less enthusiastic coalition member come 2016. Is Demography Destiny? |Megan McArdle |November 7, 2012 |DAILY BEAST 

They want candidates willing to do whatever it takes—no matter whose ideological ox is gored—to make the economic pain stop. Mitt Romney’s Problem With Conservatives: He’s Not Selling What They Want |Peter Beinart |February 13, 2012 |DAILY BEAST 

The ox knoweth his owner, and the ass his master's crib: but Israel hath not known me, and my people hath not understood. The Bible, Douay-Rheims Version |Various 

The calf and the bear shall feed: their young ones shall rest together: and the lion shall eat straw like the ox. The Bible, Douay-Rheims Version |Various 

Blessed are ye that sow upon all waters, sending thither the foot of the ox and the ass. The Bible, Douay-Rheims Version |Various 

"Sing," said the Bull, as the stiff, muddy ox-bow creaked and strained. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

It was the custom to sacrifice an ox to him, and to write any treaty made with a neighboring people upon the skin. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire)