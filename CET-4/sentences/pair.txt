Some people are adamant that chocolate does not pair well with wine, period, no how, no way. The new wine rules: Drink what you like with what you want to eat |Dave McIntyre |February 12, 2021 |Washington Post 

Vancouver’s Jordie Benn and Quinn Hughes are the second-worst defensive pair in regard to shot quality allowed. The NHL’s Canadian teams are scoring a lot. Like, a lot, a lot. |Neil Greenberg |February 12, 2021 |Washington Post 

“One pair of dry socks lives in my sleeping bag,” Oram says. How to stay warm while sleeping in the frigid outdoors |Alisha McDarris |February 12, 2021 |Popular-Science 

With the pair of games next week, Maryland will be on track to finish the season with 20 conference games as planned, despite the schedule disruptions the Big Ten has navigated this season because of the pandemic. In schedule shuffle, Maryland will host Nebraska on back-to-back days next week |Emily Giambalvo |February 12, 2021 |Washington Post 

The second experiment, still using MTurk, added a pair of wrinkles. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

Oh, and the first press image they released was a pair of black dudes in tracksuits as a troll of sorts to NME. The 14 Best Songs of 2014: Bobby Shmurda, Future Islands, Drake, and More |Marlow Stern |December 31, 2014 |DAILY BEAST 

It will still carry a pair of Raytheon AIM-120 AMRAAM long-range air-to-air missiles and a pair of bombs. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

Gurley was gunned down on Nov. 20, when a pair of cops was patrolling the rough housing project. Protesters Demand Justice For Gurley As Gap Grows Between Cops and NYC |M.L. Nestel |December 28, 2014 |DAILY BEAST 

One of them had not been given time to get dressed; he was perp-walked wearing only a pair of boxers. Bobby Shmurda and Rap’s Ultimate Hoop Dream |Rawiya Kameir |December 23, 2014 |DAILY BEAST 

They waved down a pair of responding cops who followed the alleged cop killer into the subway. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

He stood, with the air of a hero, both arms extended towards the amazed pair of lovers. The Joyous Adventures of Aristide Pujol |William J. Locke 

Under the long lashes of low lids a pair of eyes black and insolent set off the haughty lines of her scarlet lips. St. Martin's Summer |Rafael Sabatini 

Here the pair reached the "Dun Cow" and retired to their respective quarters. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Since words have different meanings, we may sometimes find that a pair of words exemplify all three Laws, as plough and sword. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

The opposite of these two methods of rote learning is my method, which injects an active process between each pair of words. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)