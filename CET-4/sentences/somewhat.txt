“You’ll get warmer faster and stay that way for longer if you’re already somewhat warm before you get in your sleeping bag,” Oram points out. How to stay warm while sleeping in the frigid outdoors |Alisha McDarris |February 12, 2021 |Popular-Science 

What I think I hear you saying is it could be a little earlier or it could be somewhat later. Metro board expresses wariness over increased debt but gives tentative approval |Justin George |February 11, 2021 |Washington Post 

Theme park losses are slowing somewhat, going from $2 billion in the spring to $1 billion in the summer to $120 million this quarter, thanks both to reopenings and, of course, cost-cutting. Disney took in nearly $5 billion less in revenue over the pandemic-riddled holidays |Steven Zeitchik |February 11, 2021 |Washington Post 

The Virginia law is somewhat modeled on California's landmark Consumer Privacy Act, which was signed into law in 2018 and took effect on January 1, 2020. Virginia is about to get a major California-style data privacy law |Kate Cox |February 11, 2021 |Ars Technica 

I was sick in March of last year, and I got to tell you, I feel like this is the first month that I feel somewhat back to normal. Actress, Mother, Activist Alyssa Milano on Life as a Triple Threat |Esabelle Lee |February 11, 2021 |Ozy 

Yet for a vivid decade or so, sleaze was, somewhat paradoxically, a force for literacy and empowerment. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 

The rebels though seemed somewhat chastened by the result despite more than doubling the anti-Boehner votes from two years ago. Democrats Accidentally Save Boehner From Republican Coup |Ben Jacobs, Jackie Kucinich |January 6, 2015 |DAILY BEAST 

Given the somewhat macabre origins of the feast, many of the celebrations were designed to placate the gods. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

As with much of this investigation our information is somewhat limited. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

Extra dry, for example, is actually sweeter than brut, which is drier than demi-sec, which is somewhat sweet. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

I called it a spinet, because it somewhat resembled that instrument, and was played upon in the same manner. Gulliver's Travels |Jonathan Swift 

The order of meals at Lane End was somewhat peculiar even then, and would now be almost unique. Hilda Lessways |Arnold Bennett 

This was somewhat tiresome; and, after a rather feeble attempt at a third laugh, Davy said, "I don't feel like it any more." Davy and The Goblin |Charles E. Carryl 

The view from the sea-side may be somewhat better, but not much—not comparable to that of Genoa from the Mediterranean. Glances at Europe |Horace Greeley 

This Captain Kirton was really the best of the Kirton bunch: a quiet, unassuming young man, somewhat delicate in health. Elster's Folly |Mrs. Henry Wood