Sadler recommends mixing a paste of baking soda and water, then smearing it on in a thick layer and leaving it for 20 minutes to work on softening the deposits. How can I get the brown crud off my oven’s glass door? |Jeanne Huber |February 1, 2021 |Washington Post 

Prag recommends doctoring your recipes with ingredients that add rise, like eggs or baking soda, as well as add-ins that can help lend some structure, like bananas. A Curious Eater's Guide to Alternative Flours |Christine Byrne |December 15, 2020 |Outside Online 

Look for large boxes of baking soda in the laundry aisle, for even more savings. Hints From Heloise: Skip the powder for carpets |Heloise Heloise |December 10, 2020 |Washington Post 

Coca-Cola and Pepsi, finding themselves behind the ball, scrambled to come up with their own diet soda offerings. The Rise And Fall Of Tab – After Surviving The Sweetener Scares, The Iconic Diet Soda Gets Canned |LGBTQ-Editor |November 29, 2020 |No Straight News 

We’re trying to encourage people to have something close at hand to smother the fire, like a lid, or baking soda. More burn injuries are occurring in kitchens during pandemic. Thanksgiving may see a rise in these accidents. |Katherine Ellison |November 22, 2020 |Washington Post 

He would shake a chilled Coke, and then spray the soda into a cold glass of milk. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

Place the flour, baking soda, baking powder, and salt on parchment or wax paper. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

He speaks while sipping a soda in the restaurant of the Residence Victoria in downtown Kisangani. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

Meanwhile, sift the flour, baking soda, and salt into a medium bowl. Make These Barefoot Contessa Salty Oatmeal Chocolate Chunk Cookies |Ina Garten |November 28, 2014 |DAILY BEAST 

Derika Moses hefted a case of 2-liter soda bottles while setting up a grocery store display in 2007. Patients Screwed in Spine Surgery ‘Scam’ |The Center for Investigative Reporting |November 3, 2014 |DAILY BEAST 

Amorphous urates are readily soluble in caustic soda solutions. A Manual of Clinical Diagnosis |James Campbell Todd 

Crystals of calcium oxalate are insoluble in acetic acid or caustic soda. A Manual of Clinical Diagnosis |James Campbell Todd 

He walked over to the table and mixed two tumblers of whiskey-and-soda, wondering why he had not thought of it before. Ancestors |Gertrude Atherton 

He paused, then mixed and drank another whiskey-and-soda, lit a cigarette, and resumed. Ancestors |Gertrude Atherton 

Is spring water fit for washing the iodized paper; if it contains either sulphate or bicarbonate of lime or muriate of soda? Notes and Queries, Number 177, March 19, 1853 |Various