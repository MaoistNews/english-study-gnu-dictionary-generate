Canix caught the eye of several critical investors in its short life. Canix aims to ease cannabis cultivators’ regulatory bookkeeping |Matt Burns |September 17, 2020 |TechCrunch 

How to vote in your stateOn the pandemic, fears of becoming infected persist, with more than 6 in 10 Minnesota voters “very” or “somewhat” worried about an immediate family member catching the novel coronavirus. Post-ABC poll and others suggest Minnesota has shifted since 2016, but by how much? |Scott Clement, Dan Balz |September 16, 2020 |Washington Post 

To find out more about how his company is navigating through current challenges and what the future holds for business travel, Fortune recently caught up with Cohen—several weeks after the CEO made his first work trip in months. Are you ready to start traveling for work again? TripActions’ CEO is banking on it |Michal Lev-Ram, writer |September 15, 2020 |Fortune 

The engineers are working hard to meet their deadlines, and this reporter even caught Gil pulling aside some engineers to tell them to remember to take time off. IBM plans a huge leap in superfast quantum computing by 2023 |rhhackettfortune |September 15, 2020 |Fortune 

It didn’t take me long to realize that catching star-nosed moles did not include hopping from stone to stone along clear mountain streams like a wood elf. How to hunt for star-nosed moles (and their holes) |Kenneth Catania |September 15, 2020 |Popular-Science 

They all immediately dashed out to their car to catch the bad guys. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

“The government just wanted to catch the big fish [in the Juarez cartel] and they ignored everything in between,” Lozoya said. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

From a lyrical standpoint, there are precious few that can catch Kendrick. The 14 Best Songs of 2014: Bobby Shmurda, Future Islands, Drake, and More |Marlow Stern |December 31, 2014 |DAILY BEAST 

With Rick, I think the culture just lags behind great artists much of the time, and it takes time for it to catch up. Coffee Talk with Ethan Hawke: On ‘Boyhood,’ Jennifer Lawrence, and Bill Clinton’s Urinal Exchange |Marlow Stern |December 27, 2014 |DAILY BEAST 

Phone lines would catch fire from the velocity and ferocity of his words. David Garth, the Consultant Who Talked Up to Voters |Jeff Greenfield |December 15, 2014 |DAILY BEAST 

While you were admiring the long roll of the wave, a sudden spray would be dashed over you, and make you catch your breath! Music-Study in Germany |Amy Fay 

If I could catch Laura's eye—but I suppose it would hardly be decent to go just yet. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

Then Squinty would toss the apple up in the air, off his nose, and catch it as it came down. Squinty the Comical Pig |Richard Barnum 

But what if I catch the fish by using a hired boat and a hired net, or by buying worms as bait from some one who has dug them? The Unsolved Riddle of Social Justice |Stephen Leacock 

We nearly played our horses out galloping around looking for you—after we'd gone a mile or so, and you didn't catch up. Raw Gold |Bertrand W. Sinclair