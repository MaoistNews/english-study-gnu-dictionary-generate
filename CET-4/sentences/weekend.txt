When he got back, they made plans for him to cook dinner for her that weekend. Date Lab: One of our setups is still going strong two years later. We caught up with Willie and Renee for an update. |Marin Cogan |February 11, 2021 |Washington Post 

This criticism of gaming also seems weak in an era where bingeing TV shows on Netflix is considered a normal weekend night. Video games deserve better than blanket, parachute coverage from reporters who don’t get it |Gene Park |January 27, 2021 |Washington Post 

Regardless of whether I’m taking a weekend road trip or a two-week car adventure, these six pieces of gear go with me. The Gear I Carry as a Female Road-Tripper |Alex Temblador |January 24, 2021 |Outside Online 

I felt like the whole weekend was full of those types of terrible punts. Lamar Jackson, The Browns And Slime Won The NFL’s Wild-Card Weekend |Sara Ziegler (sara.ziegler@fivethirtyeight.com) |January 11, 2021 |FiveThirtyEight 

The nonprofit supplies research-based curriculum to guides and course providers across the country, who then offer various training weekends—priced between $250 and $500—throughout winter. A Beginner’s Guide to Splitboarding |Amelia Arvesen |December 26, 2020 |Outside Online 

And when BET Weekend came to Atlanta two months ago, Brinsley thought he could become a major party promoter. Alleged Cop Killer’s Blood-Soaked Screenplay |M.L. Nestel |December 24, 2014 |DAILY BEAST 

Williams was released on his own recognizance over the weekend and is expected back before a judge on Feb. 9, 2015. Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 

In its opening weekend the movie Heaven Is For Real (budget: $12 million) doubled its gross. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

On Christmas weekend, a North Korean tyrant has decided what American teenagers will see on the silver screen. The Sony Hack and America’s Craven Capitulation To Terror |David Keyes |December 19, 2014 |DAILY BEAST 

Are future historians going to look back on the past weekend as the one in which Elizabeth Warren took over the Democratic Party? The Most Powerful Democrat in America |Michael Tomasky |December 15, 2014 |DAILY BEAST 

I have promised to lead a folk dance conference on the weekend. Warren Commission (3 of 26): Hearings Vol. III (of 15) |The President's Commission on the Assassination of President Kennedy 

And I have no recollection of ever thinking he was not going to come that weekend. Warren Commission (3 of 26): Hearings Vol. III (of 15) |The President's Commission on the Assassination of President Kennedy 

Out of curiosity I accepted an invitation for a weekend amid what is called the "hunting set" of Long Island. The Book of Life: Vol. I Mind and Body; Vol. II Love and Society |Upton Sinclair 

You probably go to concerts on the weekend, and you probably check music out of the library too. Little Brother |Cory Doctorow 

I hold in my hands actual literature on offer at the so-called concert in Dolores Park last weekend. Little Brother |Cory Doctorow