The hope is Garrison can help Austin improve these and other issues, as the Pentagon needs to lift qualified minorities to more positions of power and keep extremists out of uniform. The Pentagon is taking a major step to deal with its diversity problems |Alex Ward |February 12, 2021 |Vox 

The minority of Republicans who think otherwise might survive the next round of primaries, but they show no ability to move the 88 to 95 percent of the party out. Stop trying to save the GOP. It’s hopeless. |Jennifer Rubin |February 12, 2021 |Washington Post 

While I think that is not wrong, I also think that the line for a small minority of the GOP has gotten brighter. How Has The Nation Changed Since The Insurrection At The Capitol? |Micah Cohen (micah.cohen@fivethirtyeight.com) |February 11, 2021 |FiveThirtyEight 

Although it is still too early to know whether these steps will improve lives in minority communities, they represent one effort to tackle a long-standing problem around the globe. The problem of environmental racism in Mexico today is rooted in history |Jayson Porter |February 11, 2021 |Washington Post 

This means that far fewer older adults from under-resourced racial and ethnic minority communities have been able to make appointments. One big hiccup in US efforts to distribute COVID-19 vaccines? Poor internet access. |By Tamra Burns Loeb, Et Al./The Conversation |February 10, 2021 |Popular-Science 

Latinos, the fastest growing minority group in America, are even more underrepresented in Congress. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

For my friend, a small minority of JSwipe matches materialized into conversations, and none have materialized into dates. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

The two major complaints minority communities have against the police seem at first glance paradoxical. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

Veterans are a small minority of the population, as well, serving the greater whole. A Veteran’s View: NYC Cold War Between Cops and City Hall |Matt Gallagher |December 29, 2014 |DAILY BEAST 

There, many minority parents supported Tom Torklarson, who favored the education reform agenda. How Public Sector Unions Divide the Democrats |Daniel DiSalvo |December 29, 2014 |DAILY BEAST 

While the majority pulled in one way there was an active minority that wished the Nana to set up an independent kingdom. The Red Year |Louis Tracy 

Though that party may be a minority of the faithful few, the members are enough to continue the organization. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

In regard to this question of economic condition the Minority Report took a more modest view. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The Americans had some great British leaders on their side, but they were definitely in the minority. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

But the members who were disposed to let him have even half as many troops as he thought necessary were a minority. The History of England from the Accession of James II. |Thomas Babington Macaulay