Baking also doubles as a great way to get your kids involved in making Valentine’s Day at home special—just be sure to have lots of pink, red and purple sprinkles and toppers ideas on hand, of course. How You’re Making Valentine’s Day Weekend Special At Home |Charli Penn |February 12, 2021 |Essence.com 

We then doubled back and followed the Yellow Diamond Trail toward the largest natural bridge at Shawnee. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

That’s about double the proposed IPO valuation that Reuters reported in December, citing people familiar with the discussions. Robinhood shares are soaring just like the stocks that trade on Robinhood |John Detrixhe |February 11, 2021 |Quartz 

Like many teens, she knows social media is a double-edged sword — one that seemed to become both more indispensable and more injurious during the pandemic. The loneliness of an interrupted adolescence |Ellen McCarthy |February 11, 2021 |Washington Post 

On the Rocket League subreddit, one player shared a helpful tutorial for the mode that doubled as an exemplar of a perfect touchdown pass, coordinated by two teammates. Keep the football mode in ‘Rocket League,’ you cowards |Mikhail Klimentov |February 8, 2021 |Washington Post 

And Ollie says, ‘Oh, I see, well, let me have two double vodka martinis.’ The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

A few weeks after returning from England, I was trolling the dairy section and came across the Cotswold Double Gloucester. Biking With the Bard |Kara Cutruzzula |December 28, 2014 |DAILY BEAST 

He went on to say that even such double horrors had never kept cops from continuing on. Two Cops ‘Assassinated’ in Brooklyn |Michael Daly |December 21, 2014 |DAILY BEAST 

Faced with the loss of middle class voters, the administration seems determined to double down on its current coalition. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

Albuquerque Economic Development, a private non-profit, estimates the five year growth rate at almost double the U.S. in general. After The Fall: Introducing The Anti-Villain |Rich Goldstein |December 21, 2014 |DAILY BEAST 

Under the one-sixth they appear as slender, highly refractive fibers with double contour and, often, curled or split ends. A Manual of Clinical Diagnosis |James Campbell Todd 

In treble, second and fourth, the first change is a dodge behind; and the second time the treble leads, there's a double Bob. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

All things are double, one against another, and he hath made nothing defective. The Bible, Douay-Rheims Version |Various 

The way was under a double row of tall trees, which met at the top and formed a green arch over our heads. Music-Study in Germany |Amy Fay 

The wretched young man persistently exercises his right of crying "Banco," and so practically going double or quits each time. The Pit Town Coronet, Volume I (of 3) |Charles James Wills