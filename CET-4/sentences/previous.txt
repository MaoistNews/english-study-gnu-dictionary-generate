Already, previous deepfake bills introduced in the US Congress have received significant pushback for being too broad. Deepfake porn is ruining women’s lives. Now the law may finally ban it |Karen Hao |February 12, 2021 |MIT Technology Review 

Adams, an executive at a software company, said his previous job — which he left in 2018 — spurred an unhealthy lifestyle. The ‘garbage guy’ walks 12 miles a day around D.C. picking up trash: ‘I’ll pick up pretty much anything.’ |Sydney Page |February 11, 2021 |Washington Post 

They reported torture during a previous detention, and we worry they may face additional abuse. Brothers who fled Chechnya arrested, returned to homeland |Michael K. Lavers |February 10, 2021 |Washington Blade 

Hope will orbit Mars at a higher altitude than any previous Mars mission, allowing scientists to see half of the planet no matter where the orbiter is. The UAE’s Hope probe is about to arrive at Mars in a historic first |Neel Patel |February 9, 2021 |MIT Technology Review 

The game marked the first time the winning quarterbacks from the previous two Super Bowls had squared off, and it had all the trappings of a classic, perhaps becoming one of the best quarterback duels in football history. That really was one of the least enjoyable Super Bowls of all time |Neil Greenberg |February 9, 2021 |Washington Post 

This is even more striking in Submission than in his previous books. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

And their fundraising collapsed by 50 percent from the previous year. ‘Only God’ Can Stop Gay Marriage |Tim Mak |January 6, 2015 |DAILY BEAST 

Millennials also thinks about our public personas so much more than previous generations. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 

The legislation strengthens and updates a previous version of the bill that expired in 2011. Nazis, Sunscreen, and Sea Gull Eggs: Congress in 2014 Was Hella Productive |Ben Jacobs |December 29, 2014 |DAILY BEAST 

There have been previous waves of people moving to Texas, and we are now experiencing the latest wave. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

Prima facie, the verdict here is less favorable than in the previous case. Readings in Money and Banking |Chester Arthur Phillips 

The evening previous to his death he was walking about the farm, in the full possession of all his faculties of mind and body. The Every Day Book of History and Chronology |Joel Munsell 

On joining the earl, father and son met as if they had parted only the previous day. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

In 1205 wheat was worth 12 pence per bushel, which was cheap, as there had been some years of famine previous thereto. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

He was a bookbinder previous to going upon the stage; and acquired a high degree of reputation as an actor. The Every Day Book of History and Chronology |Joel Munsell