A vapor chamber—an enclosed cooling system that relies on liquid evaporating and condensing to cool the air around it—employs another fan to help vent hot air out through the top. Nvidia’s monstrous new graphics cards crank up the power while dropping their prices |Stan Horaczek |September 9, 2020 |Popular-Science 

This interview has been lightly edited and condensed for clarity. ‘We don’t have the burden of traditional media’: Confessions of an upstart agency holding group MD |Seb Joseph |September 7, 2020 |Digiday 

This interview is based on phone and email interviews and has been condensed and edited for clarity. Conducting the Mathematical Orchestra From the Middle |Rachel Crowell |September 2, 2020 |Quanta Magazine 

Automated tools that help condense and summarize and extract information from written text are becoming more and more essential. Can A.I. understand poetry? |Jeremy Kahn |August 18, 2020 |Fortune 

Appel talked about how the university has condensed it’s “how to teach online” courses for teachers into week-long immersions, or shorter, that are now free for teachers and parents to meet the unprecedented demand. Morning Report: How the City Came to Lease a Lemon |Voice of San Diego |August 11, 2020 |Voice of San Diego 

Those vapors condense into a liquid later in the process and that “condensate” is collected in a storage area, called a sump. Oil Tankers Leaking into Seattle’s Water |Bill Conroy |October 13, 2014 |DAILY BEAST 

She speaks in thick paragraphs that her staffers probably wish they could condense and sharpen at times. Could a Pro-Pot Lesbian Become the Next Governor of Maryland? |Jim Newell |March 11, 2014 |DAILY BEAST 

I cannot condense the horror of either the Bosnian war or the Rwandan genocide in the length of this column. Pity Boston, Ignore Nigeria: The Limits of Compassion |Janine di Giovanni |April 28, 2013 |DAILY BEAST 

They get an actor on the schedule at their budgets where they try to condense roles. Billy Zane Opens Up About ‘Titanic,’ ‘Zoolander,’ and the Lost Decade |Marlow Stern |April 4, 2012 |DAILY BEAST 

Should the vapor not condense well, the test-tube may be immersed in a glass of cold water. A Manual of Clinical Diagnosis |James Campbell Todd 

"I wonder if she has ever tried to condense rudeness into an epigram," said Isabel viciously, pausing in her narrative. Ancestors |Gertrude Atherton 

I tried to condense the steam by the cold sides of the condenser, without using injection-water. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

After a while it became cool enough to permit the water to condense on the surface and so the ocean began to be formed. Birds and All Nature, Vol. VI, No. 3, October 1899 |Various 

Heat and light come and go, as vapors of water condense into rain and dissolve into vapor to return again to the atmosphere. Etidorhpa or the End of Earth. |John Uri Lloyd