Last year The Vegan Society published an employer handbook providing guidance on how to make an office space and canteen more vegan-friendly — including offering vegans their own shelf in the fridge. Veganism in the workplace: How businesses are embracing eco-conscious workers |Jessica Davies |May 18, 2021 |Digiday 

Nestor focuses on delivering lunch for office workers in ParisFoodles raises another $10 million for its cloud canteen Elior acquires food delivery startup Nestor |Romain Dillet |April 9, 2021 |TechCrunch 

While Elior is working with big companies in glass towers, it has been quite hard to convince small and medium companies to open a canteen in the office. Elior acquires food delivery startup Nestor |Romain Dillet |April 9, 2021 |TechCrunch 

For instance, companies can switch to Nestor for their canteens. Elior acquires food delivery startup Nestor |Romain Dillet |April 9, 2021 |TechCrunch 

Government guidelines promoting plant-based proteins for factory canteens and school cafeterias would play an enormous part in reducing costs and raising public awareness. How China Could Change the World by Taking Meat Off the Menu |Charlie Campbell/Shanghai |January 22, 2021 |Time 

I innocently wrote a letter home to my mother telling her how great the French chef was in the headquarters canteen. I Saw Nuclear Armageddon Sitting on My Desk |Clive Irving |November 10, 2014 |DAILY BEAST 

“The profit from the canteen goes directly back into the facility,” says Van Wickler. ‘Progressive Jail’ Is a 21st-Century Hell, Inmates Complain |Sarah Shourd |September 29, 2014 |DAILY BEAST 

She spent 12-hour days at the Red Cross canteen, doing whatever was needed. Eleanor Roosevelt: Feminist Icon |Eleanor Clift |September 2, 2014 |DAILY BEAST 

Dick van Hoff x Thomas Eyck Canteen and Lunch Box Set, $299 at GSelect  2. The Daily Beast's 2013 Ultimate Holiday Gift Guide |The Daily Beast |November 27, 2013 |DAILY BEAST 

“Being popular online is like sitting at the cool table in the canteen…In a mental hospital,” he wrote. Amnesty International U.K. Board Chairman Resigns Over Crude Jokes |Nico Hines |August 14, 2013 |DAILY BEAST 

As Matt stepped back from the motor-car and finished screwing the cap on the canteen, a man jumped out into the road. Motor Matt's "Century" Run |Stanley R. Matthews 

Still hanging to Clip's canteen, he jerked the motor-cycle away from the bushes, got into the saddle, and started the pedals. Motor Matt's "Century" Run |Stanley R. Matthews 

So the name of a street has become the much-berated canteen of the sutler and the much needed canteen of the soldier. English: Composition and Literature |W. F. (William Franklin) Webster 

Now, after three months without change of diet, the first canteen ship is about due. Gallipoli Diary, Volume 2 |Ian Hamilton 

The hunter handed over a small bag of food and a large canteen full of water. Average Jones |Samuel Hopkins Adams