To protect delicate hardwood, look for a model that lets you turn off the brush or has specially designed soft bristles. Stick it to dirt and dust with the best vacuum on the market |Florie Korani |July 28, 2021 |Popular-Science 

Those tiny wire bristles can break off, stick onto the grill, and wind up in your food, which winds up in your body, which winds up in the hospital. Best grill accessories you need to become a grill master |Florie Korani |July 23, 2021 |Popular-Science 

Scrub the outside of the shoes thoroughly, taking care to get the bristles into fabric sections to clear dirt out of the mesh. How to Clean Your Cycling Gear |agintzler |July 17, 2021 |Outside Online 

Her bristles about the press and her image don’t always lack merit. Cindy McCain Opens Up — On Her Terms |Philip Elliott |April 27, 2021 |Time 

This Bissell carpet cleaner has a powerbrush that has ten rows of bristles designed to loosen dirt and odors in combination with a cleaning solution, before sucking them into a waste tank. The best carpet cleaner machines to keep your home spotless |Charlotte Marcus |March 19, 2021 |Popular-Science 

Americans tend to bristle even at self-censorship; we are reluctant to declare that we simply are not going to look at something. From ISIS Videos to JLaw Nudes, When Is Looking Abetting Evil? |Michael Daly |September 3, 2014 |DAILY BEAST 

And they bristle too at the notion that they had some kind of personal enmity toward the president. These Clinton Haters Can’t Quit the Crazy |David Freedlander |May 22, 2014 |DAILY BEAST 

She continued to bristle at being associated with the Mafia because of her father. A True Tough Guy: The Mafia, Gays, and Michael Sam’s Boyfriend |Michael Daly |May 15, 2014 |DAILY BEAST 

Her woefully neglected novels still bristle with wit and insight. The Neglected Penelope Mortimer Was a Novelist Ahead of Her Time |Jessica Ferri |March 25, 2014 |DAILY BEAST 

While a normal person would bristle at such obvious red flags, our girls ran like bulls towards love. Juan Pablo Chooses the Girl He ‘Likes,’ Proposes They Go Get a Burger |Brandy Zadrozny |March 11, 2014 |DAILY BEAST 

Even the roofs of the houses bristle with pigeon-lofts and artful-looking structures for the capture of wandering birds. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

To whom the prophetess, seeing his neck now bristle with horrid snakes, flings a soporific cake of honey and medicated grain. The Fatal Dowry |Philip Massinger 

De time wuz w'en folks had a mighty slim chance fer ter git bristle, en dey aint no tellin' w'en dat time gwine come ag'in. Nights With Uncle Remus |Joel Chandler Harris 

His mask is brown, cut off above the upper lip, over which a pair of short moustachios bristle. The Memoirs of Count Carlo Gozzi; Volume the first |Count Carlo Gozzi 

In crescent formation the dense black cloud swept on—in dead silence—a phalanx of shields, a perfect bristle of assegais. Forging the Blades |Bertram Mitford