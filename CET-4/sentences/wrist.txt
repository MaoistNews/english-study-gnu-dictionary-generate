Second baseman Starlin Castro is out until October with a broken wrist. The Nationals Are Running Out Of Time |Neil Paine (neil.paine@fivethirtyeight.com) |September 11, 2020 |FiveThirtyEight 

Six months later, it’s time to focus on our hurting necks, wrists, forearms, shoulders, backs, eyesight, and so on. You’re not sitting correctly at your home office |Adam Lashinsky |September 10, 2020 |Fortune 

It has a five-day battery life, and the low-profile wrist band is waterproof and comfortable. The Most Futuristic Workout Gear of 2020 |Hayden Carpenter |September 5, 2020 |Outside Online 

Many people already fear that smart assistants like Amazon’s own Alexa are already listening to everything they say, which makes the prospect of strapping a dedicated listening device to your wrist all day seem dubious. Amazon’s new fitness tracker listens to your voice to figure out your mood |Stan Horaczek |September 2, 2020 |Popular-Science 

You can adjust the size if you want to use them as wrist weights, allowing for a more heavy duty arm workout. The best ankle weights for a sculpted lower body |PopSci Commerce Team |September 1, 2020 |Popular-Science 

Clad in a blue, striped button-down, a silver watch adorning his left wrist, Huckabee beams on the cover. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

Rather than downing a handful of pills, I planned to take my life by opening a vein in each wrist. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

While that might just seem like a slap on the wrist compared to the cost of insurance, the penalty increases every year. Think You’re Invincible? Here’s Why Open Enrollment Matters |DailyBurn |November 16, 2014 |DAILY BEAST 

A scar marks her right wrist where the bullet hit her eight months ago. ‘There was no food, no more water lilies’ |Crystal Wells |October 31, 2014 |DAILY BEAST 

He also was diagnosed with “head, neck, wrist and back injuries – and the biggest was TBI, or traumatic brain injury.” When the War Comes Home |Sara Stewart |October 16, 2014 |DAILY BEAST 

Just try it once, and you'll see how ingenious it is—only one must be careful not to throw out the elbow in turning out the wrist. Music-Study in Germany |Amy Fay 

At present she is throwing her whole weight upon my wrist, which I hope will get limber under it! Music-Study in Germany |Amy Fay 

Phœbe Chiffinch, breathless, is holding Alice Arden's wrist with a firm grasp. Checkmate |Joseph Sheridan Le Fanu 

Like lightning he turned and seized by the wrist a man who had already opened the bag and laid hold of some of its contents. The Garret and the Garden |R.M. Ballantyne 

He wears a real pearl stud and a good signet ring; also a gold wrist watch, face broken and hands stopped at seven-fifteen. Dope |Sax Rohmer