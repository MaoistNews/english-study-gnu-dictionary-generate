It is the summit of human happiness: the surrender of man to God, of woman to man, of several women to the same man. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Several times, either because they forgot or they had a technical problem, they connected directly, and we could see them. Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 

Several Muslim sites in France, including mosques have been attacked or vandalized since the Charlie Hebdo massacre. Europe’s Islam Haters Say We Told You So |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

Julianne Moore, Still Alice Julianne Moore should have several Oscars by now. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 

Several Republicans won primaries in 2014 by running as ideologically pure conservatives who wanted new leadership in the House. Kamikaze Congress Prepares to Strike Boehner |Ben Jacobs |January 6, 2015 |DAILY BEAST 

He distinguished himself in several campaigns, especially in the Peninsular war, and was raised to the rank of field marshal. The Every Day Book of History and Chronology |Joel Munsell 

After we had passed over this desert, we found several garisons to defend the caravans from the violence of the Tartars. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

I called out several times, as loud as I could raise my voice, but all to no purpose. Gulliver's Travels |Jonathan Swift 

For several months he remained under a political cloud, charged with incompetency to quell the Philippine Rebellion. The Philippine Islands |John Foreman 

Several pioneers familiar with the facts of the tragedy at the time of its occurrence were also present. Among the Sioux |R. J. Creswell