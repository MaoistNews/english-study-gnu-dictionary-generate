These can be a bit sturdier than an individual, adjustable guide comb, and often work better for longer beards. The best beard trimmer: Shape your facial hair with ease |Carsen Joenk |January 19, 2021 |Popular-Science 

So a frequency comb in visible wavelengths might be composed of light with a wavelength of 500 nanometers, 510nm, 520nm, and so on. One piece of optical hardware performs massively parallel AI calculations |John Timmer |January 7, 2021 |Ars Technica 

Just be careful not to mix and match the cutters and combs when you put them back together since they are arranged in matching pairs by the manufacturer. Best electric shaver: Get a smooth shave with our picks |Jeremy Helligar |December 18, 2020 |Popular-Science 

At the same time, larval comb jellies also began to disappear. When prey get scarce, these jellies become cannibals |Erin Garcia de Jesus |June 1, 2020 |Science News For Students 

Javidpour and her team collected comb jellies, both adults and larvae, from Kiel Fjord. When prey get scarce, these jellies become cannibals |Erin Garcia de Jesus |June 1, 2020 |Science News For Students 

When she first launched paridust, Ehsan would comb her own closet for her artistic stylings. She's Got the Look: How Pari Ehsan Marries Fashion and Art |Allison McNearney |August 26, 2014 |DAILY BEAST 

Upon landing, he was reportedly spotted wandering the tarmac with only a comb in his pocket. How to Hitchhike a Plane—and Survive |Kent Sepkowitz |April 22, 2014 |DAILY BEAST 

I ask Cuco how The Verne Club gets illegal, over-proof alcohol like absinthe through the fine-toothed comb of Argentine customs. The Absinthe-Minded Porteños of Buenos Aires |Jeff Campagna |March 10, 2014 |DAILY BEAST 

Impending indictment for felony comb-over rumored to have played a part in the decision. Up to a Point: PJ on Sochi Stray Dog Stew and 1-800-F*CKYOU |P. J. O’Rourke |February 21, 2014 |DAILY BEAST 

In the script, David wrote that he wanted the character Christian Bale played to have a comb-over. The ‘American Hustle’ Style Guide |Erin Cunningham |February 14, 2014 |DAILY BEAST 

He leaves orders that these fellers behind are t' comb the country till he calls 'em off. Raw Gold |Bertrand W. Sinclair 

His hair relieved this somewhat, for it was white and always stood gaily on end, defying brush and comb. The Soldier of the Valley |Nelson Lloyd 

The red comb on the top of his head has teeth like a carpenter's saw, and is so large it will not stand up straight. Seven O'Clock Stories |Robert Gordon Anderson 

The poor beast has no ears left and his mane is all notched like an old broken comb; but Roger loves him. Child Life In Town And Country |Anatole France 

The objects in the engraving are probably the shears, comb, ladle, and an unknown instrument used for cleansing wool. The Catacombs of Rome |William Henry Withrow