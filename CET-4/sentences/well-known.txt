The best comparison here for an American audience is, well, Internet stuff. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

Then add in all bored people, as well as people whose job it is to report on celebrities. Sia and Shia LaBeouf’s Pedophilia Nontroversy Over ‘Elastic Heart’ |Marlow Stern |January 9, 2015 |DAILY BEAST 

Asia Bibi, as she is known, was arrested and sentenced to death. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

The cartoonist, better known as Charb, was shot dead Wednesday. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

He hits bottom at Rocamadour, a sanctuary in the Dordogne known as a citadel of faith devoted to Mary. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Mrs. Wurzel was quite right; they had been supplied, regardless of cost, from Messrs. Rochet and Stole's well-known establishment. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The big room at King's Warren Parsonage was already fairly well filled. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The country is well inhabited, for it contains fifty-one cities, near a hundred walled towns, and a great number of villages. Gulliver's Travels |Jonathan Swift 

Before he could finish the sentence the Hole-keeper said snappishly, "Well, drop out again—quick!" Davy and The Goblin |Charles E. Carryl 

Old Mrs. Wurzel and the buxom but not too well-favoured heiress of the house of Grains were at the head of the table. The Pit Town Coronet, Volume I (of 3) |Charles James Wills