We can look back at flu pandemics — you usually have one or two years of circulation before enough immunity builds up to the virus. How coronavirus variants may drive reinfection and shape vaccination efforts |Erin Garcia de Jesus |February 5, 2021 |Science News 

Depending on the answer, the US may require regular vaccine campaigns against the coronavirus in the future, similar to the flu. The case for optimism on Covid-19, in 2 charts |German Lopez |February 5, 2021 |Vox 

Because mRNA is an information molecule, the difference between our covid vaccine, Zika vaccine, and flu vaccine is only the order of the nucleotides. The next act for messenger RNA could be bigger than covid vaccines |David Rotman |February 5, 2021 |MIT Technology Review 

After all, most people who get sick with the flu won’t even bother to go get tested for it. How COVID-19 Ended Flu Season Before It Started |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |February 4, 2021 |FiveThirtyEight 

It could be because these viruses, unlike flu and RSV, aren’t as thoroughly destroyed by handwashing and using sanitizers. COVID-19 precautions may be reducing cases of flu and other respiratory infections |Jennifer Welsh |February 2, 2021 |Science News 

I am fortunate that I have never been deathly ill, but whenever I have the stomach flu, I most certainly feel like I am dying. Why My Norovirus Panic Makes Me Sick |Lizzie Crocker |January 5, 2015 |DAILY BEAST 

The Italian health ministry said it could not prove a direct correlation between the flu vaccine and the deaths. Did This Flu Vaccine Kill 13? |Barbie Latza Nadeau |December 2, 2014 |DAILY BEAST 

The benefits are obvious, says 83-year-old Mariacristina Poggio, who sums it up succinctly: “I could die from the flu without it.” Did This Flu Vaccine Kill 13? |Barbie Latza Nadeau |December 2, 2014 |DAILY BEAST 

Blister rust is like having the flu; the pine beetle is like fast acting leukemia. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

Suffering from flu symptoms one day, she decided to call in sick. I Warned You About Bill Cosby in 2007 |Mark Ebner |November 20, 2014 |DAILY BEAST 

Mr. Pope was a bachelor, and his valet inconsiderately took the “flu.” Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

This condition caused many "flu" patients to develop pneumonia and to die. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Grisel explained that it was pneumonia following on "flu," and he heard her blow her poor little nose. Happy House |Betsey Riddle, Freifrau von Hutten zum Stolzenberg 

I wish some of those fellows with the flu could have you to look after them. Quin |Alice Hegan Rice 

Lee all but died, and Nielsen, afterward, told me he would rather die than have the "flu" again. Tales of lonely trails |Zane Grey