Thus, more time is spent organization and obtaining ones free of failings. Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 

Could the (thus far) timid trembling give way to a full-on, grand mal seizure? 26 Earthquakes Later, Fracking’s Smoking Gun Is in Texas |James Joiner |January 7, 2015 |DAILY BEAST 

Thus begins an episode of The Mindy Project centered around a guy trying to have butt sex with his girlfriend. Year of the Butt: How the Booty Changed the World in 2014 |Kevin Fallon |December 30, 2014 |DAILY BEAST 

Thus it attracted a wave of cowboy operators to fly passengers and cargo between cities. Who Will Get AsiaAir 8501’s Black Boxes? |Clive Irving |December 30, 2014 |DAILY BEAST 

He was not selling “loosies” that day, no cigarettes were found on his person, and thus there was no probable cause in play. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 

We resolved to do our best to merit the good opinion which we thus supposed them to entertain of us. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

I take the Extream Bells, and set down the six Changes on them thus. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

Thus far Boston banks have received more benefits from this bank than have the other banks in this district. Readings in Money and Banking |Chester Arthur Phillips 

Anselme, thus enjoined, lent an unwonted alacrity to his movements, waddling grotesquely like a hastening waterfowl. St. Martin's Summer |Rafael Sabatini 

The manifest annoyance of her household was thus easily accounted for, but he marveled at the strength of her bodyguard. The Red Year |Louis Tracy