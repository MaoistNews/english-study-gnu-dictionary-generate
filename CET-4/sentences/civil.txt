Malcolm was incarcerated for several years and went on to mobilize thousands on issues of both civil and human rights. How Laurence Fishburne Gave Voice To ‘The Autobiography Of Malcolm X’ |Joi-Marie McKenzie |September 17, 2020 |Essence.com 

To create lasting community change, meaningful reforms should also be a component of civil settlements. The policing reforms in the Breonna Taylor settlement, explained |Fabiola Cineas |September 17, 2020 |Vox 

I would say that with civil disturbance type operations, we’re reluctant because there are many things that can go wrong. Mobilizing the National Guard Doesn’t Mean Your State Is Under Martial Law. Usually. |by Logan Jaffe |September 17, 2020 |ProPublica 

I've had long conversations with the civil rights community here to say that I recognize that he would have been a great assistant attorney general for civil rights. The Trailer: The First State goes last |David Weigel |September 15, 2020 |Washington Post 

The city and the district attorney listed multiple “Does” on each injunction upon the initial filing, so that law enforcement could add people in the future without having to file another civil injunction. While We’re Rethinking Policing, It’s Time to End Gang Injunctions |Jamie Wilson |September 15, 2020 |Voice of San Diego 

They are, to say the least, preparing for civil war (the polling stations are stormed by armed gangs). Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

But what is there more irresponsible than playing with the fire of an imagined civil war in the France of today? Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Compared with neighbors Myanmar, Vietnam, and Laos, Cambodia appears to have a blossoming civil society. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 

Strangio is at his best when exposing what appears to be a flourishing civil society in Cambodia. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 

Rashad was there to celebrate the release of the Civil Rights drama Selma. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

His 6,000 native auxiliaries (as it proved later on) could not be relied upon in a civil war. The Philippine Islands |John Foreman 

A lineman was sent out to repair it under escort of civil guards, who were forced by the rebels to retire. The Philippine Islands |John Foreman 

In the good old days of yore there was little trouble in obtaining admission to the Civil Service. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

He held various civil offices, was a justice of the peace about 60 years, and for many years a member of the state legislature. The Every Day Book of History and Chronology |Joel Munsell 

Alessandro had hard work to give civil answers to the men who wished to buy Benito and the wagon for quarter of their value. Ramona |Helen Hunt Jackson