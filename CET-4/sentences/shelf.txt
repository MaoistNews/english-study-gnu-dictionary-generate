The 10-inch-deep shelf up top is a great place for placing plants, picture frames, keys, lamps, or any other odds and ends the space might require, too. Garment racks for maximizing space in every type of room |PopSci Commerce Team |September 9, 2020 |Popular-Science 

The empty supermarket shelves and shortages of medical supplies of recent months have driven home the fact that supply chain stability is not just a business issue. COVID-19 and climate change expose the dangers of unstable supply chains |matthewheimer |August 27, 2020 |Fortune 

Even when you’re anxious, you’re delivering those packages, stocking those shelves, and doing all that essential work so that all of us can keep moving forward. ‘He is clearly in over his head’: Read Michelle Obama’s full speech denouncing Donald Trump |kdunn6 |August 18, 2020 |Fortune 

It’s not the first product to be torn from shelves, physical or otherwise. Environment Report: One Way to Force Companies to Emit Less Carbon |MacKenzie Elmer |August 10, 2020 |Voice of San Diego 

You can add robe hooks to the back of the door and install a hotel towel shelf above the commode where towels are out of the way but easily accessible. Living large in small spaces |Valerie Blake |August 8, 2020 |Washington Blade 

But by far the most interesting object, which held enormous fascination for me, sat high up on the top shelf. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

She reassumed slave posture while Couple grabbed two magnetized clamps and a collection of circular magnets from a nearby shelf. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

And much of it, unlike Pappy, is right there on the shelf, humbly, quietly waiting to be tried. The Cult of Pappy van Winkle |Eric Felten |December 3, 2014 |DAILY BEAST 

The men preside over three display cases, each with three shelves, seven comic books per shelf. The Holy Grail of Comic Books Hid in Plain Site at New York Comic Con |Sujay Kumar |October 14, 2014 |DAILY BEAST 

On the top shelf, sandwiched between Detective Comics No.27 and Superman No.1, are three issues of Actions Comics No.1. The Holy Grail of Comic Books Hid in Plain Site at New York Comic Con |Sujay Kumar |October 14, 2014 |DAILY BEAST 

On a shelf near one of these windows stood the little Madonna, again wreathed with vines as in San Pasquale. Ramona |Helen Hunt Jackson 

The walls were painted blue, the skirting almost a third of the height, and so wide at the top as to form a narrow shelf. The Daisy Chain |Charlotte Yonge 

On a shelf above the divan, however, were many books, and Gwynne ran his eye over them. Ancestors |Gertrude Atherton 

Kerry crossed the room, laid his oilskin and cane upon a chair, and from the shelf where it reposed took a squat volume. Dope |Sax Rohmer 

Moodily he stood there, one hand on the high mantel shelf, one foot upon an andiron, his eyes upon the flames. St. Martin's Summer |Rafael Sabatini