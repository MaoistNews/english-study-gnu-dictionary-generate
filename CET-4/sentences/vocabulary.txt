They have oddly expansive vocabularies and eminently quotable catchphrases, one of which — “Be Excellent to Each Other” — was posted on the marquee on my local movie theater when the pandemic shut it down in March. The essential kindness of Bill and Ted |Alissa Wilkinson |August 28, 2020 |Vox 

Instead, the teen wrote tens of thousands of articles in English with a put-on Scottish accent, ignoring actual Scots grammar and vocabulary. How a Scots Wikipedia scandal highlighted AI’s data problem |Nicolás Rivero |August 27, 2020 |Quartz 

You speak in the voice and register that belongs to your old self—well-enunciated, resonant, its statements infiltrated by a formal, lawyerly vocabulary. The first murder |Katie McLean |August 19, 2020 |MIT Technology Review 

You know, so much of learning vocabulary is just word-definition, word-definition, word-definition. Can You Guess These Words From Their Definitions? |Candice Bradley |July 23, 2020 |Everything After Z 

The slightly modified version of Gödel’s scheme presented by Ernest Nagel and James Newman in their 1958 book, Gödel’s Proof, begins with 12 elementary symbols that serve as the vocabulary for expressing a set of basic axioms. How Gödel’s Proof Works |Natalie Wolchover |July 14, 2020 |Quanta Magazine 

My Arabic is limited to a vocabulary of my favorite foods, such as “I love chicken and rice.” Middle East Goes Monty Python on ISIS |Dean Obeidallah |October 29, 2014 |DAILY BEAST 

In an uncanny way, that describes the precise definition of the hipster, when the term first appeared in the American vocabulary. Why Do We Hate Hipsters So F'ing Much? |Ted Gioia |July 13, 2014 |DAILY BEAST 

Here, the vocabulary of fast food for many young Brazilians is temaki (hand rolls) instead of burgers and fries. Meet the Chef Fighting to Ensure That Brazilians Will Never Be as Fat as Americans |Brandon Presser |June 25, 2014 |DAILY BEAST 

Without the freedom to act on moral values, there is not even a vocabulary for public virtue. Government Has Made America Inept |Philip K. Howard |May 4, 2014 |DAILY BEAST 

Off camera, Rooney was growing up fast, ditching school and developing an impressive vocabulary of curse words. Mickey Rooney Was Hollywood’s Golden Age Showman |Lorenza Muñoz |April 7, 2014 |DAILY BEAST 

I hope you are able to bear the brunt of the battle, for my vocabulary will scarcely carry me through ten words. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

This word, as well as the one last-named, is very expressive in the vocabulary of the vulgar. Notes and Queries, Number 194, July 16, 1853 |Various 

The strength, the originality, the true raison d'être of the Provençal speech resides in its rich vocabulary. Frdric Mistral |Charles Alfred Downer 

We remark elsewhere the lack of independence in the dialect of Avignon, that its vocabulary alone gives it life. Frdric Mistral |Charles Alfred Downer 

But in Scotland you will hear the people using numbers of modern French words, which are no part of the English vocabulary. Friend Mac Donald |Max O'Rell