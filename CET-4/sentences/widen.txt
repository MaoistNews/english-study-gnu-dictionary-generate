Government policy has failed to narrow the rich-and-poor gap that widened following the virus outbreak. Haves and Have-Nots: Pandemic Recovery Explodes China’s Wealth Gap |Daniel Malloy |August 19, 2020 |Ozy 

Conservatives have said they’re not necessarily opposed to transit but want the agency to meet its prior commitments to voters by continuing to widen freeways. One Race Could Make or Break Plans to Overhaul the Region’s Transportation System |Jesse Marx |August 13, 2020 |Voice of San Diego 

We persist even though the pay gap actually widens for women at higher education levels. COVID-19 Highlighted Pay Disparities; November’s Election Can Help Fix Them |Shirley Weber |August 13, 2020 |Voice of San Diego 

She saw the widening hole in the ozone layer, and other environmental problems. Is Economic Growth the Wrong Goal? (Ep. 429) |Stephen J. Dubner |August 13, 2020 |Freakonomics 

First, it was only a 5- or 6-percentage-point gap, but since the middle of June, that margin has widened to anywhere from 8 to 9 points, according to FiveThirtyEight’s national polling average. Voters Are Starting To Doubt Trump’s Reelection Chances |Geoffrey Skelley (geoffrey.skelley@abc.com) |July 28, 2020 |FiveThirtyEight 

And these new aficionados of global cool are poised to widen their impact. From Public Enemy to Power Broker: Hip-Hop’s the New Global Pop Culture |Lauren DeLisa Coleman |September 27, 2014 |DAILY BEAST 

Now widen the circle to religious organizations: denominations, for example. Why Progressives Just Woke Up and Killed ENDA |Jay Michaelson |July 9, 2014 |DAILY BEAST 

“Administration officials favorite phrase these days is that, ‘you have to widen the aperture,’” says Bockenfeld. Obama To Cut Middle East Democracy Programs |Jamie Dettmer |January 2, 2014 |DAILY BEAST 

A historic settlement will grant Netanyahu prestige and widen Israeli leverage for a possible preemptive strike. A Very Israeli Linkage: Iran's Bomb and Peace With the Palestinians |Nadav Eyal |September 10, 2013 |DAILY BEAST 

A historic settlement would grant Netanyahu prestige and widen Israeli leverage for a possible preemptive strike. A Very Israeli Linkage: Iran's Nuclear Bomb and Peace With Palestine |Nadav Eyal |September 10, 2013 |DAILY BEAST 

We readily note that these lights appear to close in behind us, and widen their intervals in the direction in which we journey. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Net five rows, then take a mesh a very little larger, and widen by netting two stitches in every stitch. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

He sought to widen the domain of pleasure and narrow that of pain, and regarded a passionless state of life as the highest. Beacon Lights of History, Volume I |John Lord 

But you must know, she said, looking at him with her sea-blue eyes, that in moments of tense emotion seemed to widen and darken. The Woman Gives |Owen Johnson 

Similar arguments are frequently urged against the desire to raise the standard and widen the avenues of the "higher education." Lippincott's Magazine of Popular Literature and Science |Various