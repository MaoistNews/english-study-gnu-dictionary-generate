Cruce operates the Iron Hill Campground on the other side of the highway. The 7-Year-Old Plane Crash Survivor’s Brutal Journey Through the Woods |James Higdon |January 7, 2015 |DAILY BEAST 

So in that sense we have gotten close to the families that have lost loved ones, be it from one side or the other. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

Andy Serkis, Dawn of the Planet of the Apes Do you want to be on the wrong side of history, Academy? Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 

What could be more important, to make sure that side of things is right before we tie ourselves to someone forever? ‘Downton Abbey’ Review: A Fire, Some Sex, and Sad, Sad Edith |Kevin Fallon |January 5, 2015 |DAILY BEAST 

Within minutes, it seems, of the disclosures of these tragic events, large numbers of people chose a side and stuck to it. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

She skilfully manages the side-lights, and by this means produces strong effects. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

This city stands upon almost two equal parts on each side the river that passes through. Gulliver's Travels |Jonathan Swift 

This is one of the most striking manifestations of the better side of child-nature and deserves a chapter to itself. Children's Ways |James Sully 

When the women came, he was preparing to go to the west side for his daily visit with Mrs. Pruitt. The Homesteader |Oscar Micheaux 

Her heart fluttered violently with fear as she saw that he stepped out after her, and walked by her side toward the house. Checkmate |Joseph Sheridan Le Fanu