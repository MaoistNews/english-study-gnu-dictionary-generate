Americans who are indifferent to the risk posed by the virus would be unlikely to embrace such a reintroduction. The administration hoped its new mask guidance would stoke vaccinations. It doesn’t seem to have worked. |Philip Bump |July 22, 2021 |Washington Post 

If he’s that indifferent to that perilous dynamic right after Justice Ginsburg’s disastrous choice to not retire, presidential arm-twisting probably won’t do much good. SCOTUS Watchers Freak Out Over Breyer’s ‘Selfish’ and ‘Dangerous’ Decision to Stay Put |Scott Bixby |July 9, 2021 |The Daily Beast 

Others may be disillusioned, frustrated and even afraid, but nobody should feel indifferent. Locked up in the Land of Liberty: Part I |Yariel Valdés González |July 7, 2021 |Washington Blade 

After years of being mostly indifferent to podcasts, the world’s tech platforms — as well as the largest terrestrial radio broadcasters — have grown interested in them as they battle on every front of the digital ad market. Cheat Sheet: Why Amazon bought Art19 |Max Willens |June 28, 2021 |Digiday 

I, on the other hand, am an indifferent cook whose dinner parties have become legendary for how awful they were. Miss Manners: Aunt feels left out of distant event |Judith Martin, Nicholas Martin, Jacobina Martin |June 25, 2021 |Washington Post 

However, as she feared, The Bell Jar appeared to indifferent notices and the launch—which Ted attended—was rather low-key. Ted Hughes’s Brother on Losing Sylvia Plath |Gerald Hughes |December 2, 2014 |DAILY BEAST 

The immigrants can stay, because they are victims of indifferent authorities just like we are. In Rome’s Riots, Cries for Mussolini and Attacks on Refugees |Barbie Latza Nadeau |November 14, 2014 |DAILY BEAST 

Because we have so little skin in the game, it seems that the public is indifferent. McCain’s 13 Favorite Soldiers |Sandra McElwaine |November 11, 2014 |DAILY BEAST 

Communist-era clerks were famously rude and indifferent, because they had no motive to make people happy. Why Your Waiter Hates You |Jedediah Purdy |October 26, 2014 |DAILY BEAST 

Yet, according to the complaint, Berger was “deliberately indifferent” to these allegations against Gibney. Is UMass-Amherst Biased Against Male Students in Title IX Assault Cases? |Emily Shire |August 18, 2014 |DAILY BEAST 

The reformers of the earlier period were not indifferent to the need for centralized organization in the banking system. Readings in Money and Banking |Chester Arthur Phillips 

She stabbed him, noting the effect upon him with a detached interest that seemed indifferent to his pain. The Wave |Algernon Blackwood 

Thus arrayed I fixed myself on the porch, to be smoking my pipe in a careless, indifferent way when she came. The Soldier of the Valley |Nelson Lloyd 

Indifferent health, for he was delicate too, was one of the bonds between us. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

I have elsewhere spoken of the indifferent figure made by most Englishmen at public speaking. Glances at Europe |Horace Greeley