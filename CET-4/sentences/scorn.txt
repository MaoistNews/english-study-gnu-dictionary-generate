That Microsoft was about to stop supporting a PC it is currently selling and for which it controls everything from the firmware to the drivers earned the company some well-deserved scorn from users and the press. Microsoft will support some, but not most, 7th-gen Intel Core CPUs in Windows 11 |Andrew Cunningham |August 27, 2021 |Ars Technica 

In the theater, there was a kind of scorn, if I may say, for sitcoms. Christopher Lloyd is still playing characters who are unhinged — and larger than life |Karen Heller |August 26, 2021 |Washington Post 

Younger generations typically don’t approach fast food with the same amount of scorn, and sandwich releases now come with celebrity endorsements and the same level of anticipation as sneaker drops. The Arch Deluxe Was a Hell of a Burger. It Was Also McDonald’s Most Expensive Flop. |Jeremy Glass |July 23, 2021 |Eater 

The outsize power of celebrity billionaires and influencers to steer the market has drawn scorn from committed investors and from regulators worried about manipulation. Cryptocurrencies crash in brutal sell-off, with bitcoin down nearly 10 percent |Hamza Shaban |May 19, 2021 |Washington Post 

Plus, social media scorn also takes place after the fact—when harm to the animal has already been done. The Ethics of Wildlife Photography |Mike Koshmrl |April 14, 2021 |Outside Online 

Ricky Gervais, the sultan of scorn, uttered that cheeky bit while emceeing the Golden Globes ceremony a few years back. The Golden Globes Sobers Up (Sort Of): Years of Ridicule and Bribery Rumors Scares HFPA Straight |Marlow Stern |December 11, 2014 |DAILY BEAST 

Hanauer has been making the same case for years, drawing heaps of both praise and scorn. The Big, Long, 30-Year Conservative Lie |Monica Potts |August 8, 2014 |DAILY BEAST 

Heap praise, not scorn, on physicians who are brave and caring enough to recommend cannabis when appropriate. I Got a Weed License in Minutes |Daniela Drake |June 24, 2014 |DAILY BEAST 

Nutrition nannies scorn hot dogs, but there are plenty of happy eaters who adore them. The Jersey Shore’s Biggest Weiners Are at Jimmy Buff’s |Jane & Michael Stern |June 15, 2014 |DAILY BEAST 

This idea fell out of favor in the last century—and was looked on with scorn as “unscientific.” New Research Shows Poorly Understood “Leaky Gut Syndrome” Is Real, May Be the Cause of Several Diseases |Daniela Drake |March 27, 2014 |DAILY BEAST 

Then she put her anger from her; put from her, too, the insolence and scorn with which so lavishly she had addressed him hitherto. St. Martin's Summer |Rafael Sabatini 

But scorn is far more volcanic than glacial and a poor barrier between sex and judgment. Ancestors |Gertrude Atherton 

"Mr. Capt don't demean himself to chambermaids, Miss Lucy," retorted the abigail with angry scorn. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

For all his vaunted scorn of being a butcher at a price, now that he heard the price he seemed not half so scornful. St. Martin's Summer |Rafael Sabatini 

His face was ash-coloured and his black eyebrows quivered as though the blaze of her scorn had blinded him. Summer |Edith Wharton