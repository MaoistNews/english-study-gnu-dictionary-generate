The Englishman has made four consecutive birdies to get to even par for the tournament, good for a tie for 20th. With confidence and calm, Hideki Matsuyama ends Japan’s long wait for a Masters title |Chuck Culpepper, Scott Allen, Des Bieler |April 11, 2021 |Washington Post 

The Englishman, who led outright after each of the first two rounds, is still in fine position to make another run at his first green jacket. Hideki Matsuyama’s sublime day at the Masters gives him a four-shot lead and a shot at history |Chuck Culpepper, Scott Allen |April 11, 2021 |Washington Post 

The 40-year-old Englishman, a two-time runner-up at Augusta, birdied six of his final nine holes Thursday to finish four strokes clear of the rest of the field, tied for the largest 18-hole lead in any major over the last 75 years. The Masters 2021 live updates: Justin Rose regains lead |Scott Allen |April 9, 2021 |Washington Post 

The hirsute Englishman drained his tee shot at the par-3 16th for a hole-in-one. Justin Rose, with a back nine out of a dream, races to a four-shot lead at the Masters |Chuck Culpepper, Scott Allen, Des Bieler |April 9, 2021 |Washington Post 

A few years earlier, Thomas Hunt, an Englishman who had come to the area to explore and trade, had attacked and captured 20 Nauset and Patuxet and intended to sell them into slavery. How the Mayflower Story Fits Into Native American History |David Treuer |December 8, 2020 |Time 

She was not wearing hejab but more surprising that that, is married to an Englishman. 50 Shades of Iran: The Mullahs’ Kinky Fantasies about Sex in the West |IranWire, Shima Sharabi |January 1, 2015 |DAILY BEAST 

Even after he became a citizen in 1955, he regarded himself as an Englishman abroad. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

She was not wearing hejab, but more surprising that that, is married to an Englishman. The Iranian Islamic Fundamentalist’s Handbook on Sex in the West |IranWire |July 25, 2014 |DAILY BEAST 

The actor who plays Bassam—bearded, blue-eyed Englishman Adam Rayner—does what he can with the material, but it's not much. Generic and Superficial ‘Tyrant’ Amerisplains the Middle East |Andrew Romano |June 25, 2014 |DAILY BEAST 

The second monster was an Englishman, Richard Vyse, who on a visit to Egypt in 1835 became fascinated by the pyramids of Giza. The Nile: Where Ancient and Modern Meet |William O’Connor |June 21, 2014 |DAILY BEAST 

The commander of this fleet was an Englishman, according to the agreement between them. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The young Englishman's hair, pale in color and very smooth, was worn longer than the fashion, the ends lilting. Ancestors |Gertrude Atherton 

A few words explained his errand; but the brave Englishman would hardly hear it to the end. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

You see it for yourself, no Englishman ever shall suspect me, when we shall converse, of being other than a Briton. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He was a pretty bright sort, that same Goodell, quick-witted, nimble of tongue above the average Englishman. Raw Gold |Bertrand W. Sinclair