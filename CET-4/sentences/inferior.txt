I was not inadequate or inferior, but trying to fit into spaces that had not been created with people like me in mind. A front-row seat to #BlackLivesMatter |Katie McLean |October 20, 2020 |MIT Technology Review 

The nub of her argument, whether acting for men or women plaintiffs, was that treating men and women differently under the law helped tokeep woman in her place, a place inferior to that occupied by men in our society. Ruth Bader Ginsburg Forged A New Place For Women In The Law And Society |LGBTQ-Editor |September 23, 2020 |No Straight News 

To some degree the AMP initiative is part of that, making sure that the mobile web experience isn’t completely inferior to the app experience. Is Apple getting real about search and about to take on Google? |Greg Sterling |August 27, 2020 |Search Engine Land 

After the census office used inferior paper cards that left fibers in the mercury, Hollerith required his customers to purchase his own high-quality cards. Punching in |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Just because you live alone doesn’t mean that you’re somehow living an inferior life. Is There Really a “Loneliness Epidemic”? (Ep. 407) |Stephen J. Dubner |February 27, 2020 |Freakonomics 

Some were blatantly inferior, he said, at times with metal shavings and burrs in the threads. Patients Screwed in Spine Surgery ‘Scam’ |The Center for Investigative Reporting |November 3, 2014 |DAILY BEAST 

Think about this: music is the only branch of the entertainment world to embrace progressively inferior technologies. Five Lessons the Faltering Music Industry Could Learn From TV |Ted Gioia |August 3, 2014 |DAILY BEAST 

While it came with multiple different preset control styles, it was still an inferior system. Video Games Go Wild for Reboots |Alec Kubas-Meyer |July 6, 2014 |DAILY BEAST 

People listen to it on tinny cellphone speakers that are entirely inferior to what they had in lo-tech times of yore. Comedy Is His Calling: The Brilliance of Billy Crystal |Tom Shales |April 18, 2014 |DAILY BEAST 

Adderall is an inferior drug, asserts Hart, because of its side effects. Why We Need Medical Meth + Cocaine |Valerie Vande Panne |February 22, 2014 |DAILY BEAST 

Then we are again overgrown boys, beings of inferior race and incapable of being civilized. The Philippine Islands |John Foreman 

But closely allied to this subject, and not inferior to it in importance, stands that of Industrial Training. Glances at Europe |Horace Greeley 

Dearer than the cigarritos are the cigars, which are not inferior to the best Havanna. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He is thought to be little inferior to Racine in the merit of his dramatic compositions. The Every Day Book of History and Chronology |Joel Munsell 

But, glad as was the schemer, his delight and sense of freedom were much inferior to those of his misguided and unlucky partner. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various