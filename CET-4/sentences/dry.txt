Conventional wisdom says sweeter, or off-dry wines pair best with hot and spicy dishes. The new wine rules: Drink what you like with what you want to eat |Dave McIntyre |February 12, 2021 |Washington Post 

GREAT VALUEZeni FeF Collection, Corvina Veronese 2018Lively and bright, with savory flavors dried fig and wild herbs, this delicious red is a pick-me-up for your palate. This $14 Italian red is a gem that invites a pairing with a pot roast or pasta |Dave McIntyre |February 12, 2021 |Washington Post 

Now, as we walk along a pathway winding between shrubs and dry grasses, Cook makes the case that the Watch has ushered in a new era of fitness tracking, and not just for dedicated athletes. Tim Cook Pivots to Fitness |Michael Roberts |February 10, 2021 |Outside Online 

Another winter storm is possible over the weekend before a cold and dry Presidents’ Day. D.C.-area forecast: Cloudy and milder today before winter storm threats arrive |Matt Rogers |February 9, 2021 |Washington Post 

Last year proved to be one of the most challenging years on record for the media industry with ad revenue drying up in the second quarter, but for Group Nine, it was magnified by its entry into its first full year following the merger with PopSugar. ‘Proactive is the path’: Group Nine’s Geoff Schiller on his selling strategy |Kayleigh Barber |February 9, 2021 |Digiday 

My understanding was that according to most Christian beliefs, being trans or gay was a sin, cut and dry. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

Fold the parchment paper with the dry ingredients in half and pour into the stand mixer. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

Allow beans to cool completely then remove to a paper towel-lined plate to dry. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 

Even when he opens up, the sentences are wooden, the scenes sucked dry of emotion. The Story of the World’s Greatest Cricket Player |William O’Connor |December 24, 2014 |DAILY BEAST 

Extra dry, for example, is actually sweeter than brut, which is drier than demi-sec, which is somewhat sweet. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

The tears came so fast to Mrs. Pontellier's eyes that the damp sleeve of her peignoir no longer served to dry them. The Awakening and Selected Short Stories |Kate Chopin 

But Polavieja started his campaign with the immense advantage of having the whole of the dry season before him. The Philippine Islands |John Foreman 

Their method of curing the leaves was to air-dry them and then packing them until wanted for use. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The smoke from her kitchen fire rose white as she put in dry sumac to give it a start. The Bondboy |George W. (George Washington) Ogden 

Turn we our backs to the cold gloomy north, to the wet windy west, to the dry parching east—on to the south! Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various