A lot of households that were struggling, then a lot of the jobs that were furloughed have become permanent layoffs. ‘Can’t eat a gift card’: Rural food banks fight to put turkeys on the table |Kyle Swenson |November 20, 2020 |Washington Post 

If left untreated, it can cause serious complications and perhaps permanent heart damage. Teen athletes with even mild COVID-19 can develop heart problems |Partho Sengupta |November 18, 2020 |Science News For Students 

The combo of the immediacy of Stories and the staying power of Highlights will let you create a permanent album. Harness the power of integrated CRO and social media |Ricky Wang |November 17, 2020 |Search Engine Watch 

Shop and Reels tabs are now permanent fixtures on Instagram’s home screen, the company announced Thursday. Social Shorts: Instagram pegs Reels and Shop, Pinterest’s engagement metric, TikTok’s size and more |Ginny Marvin |November 16, 2020 |Search Engine Land 

With wide asymptomatic spread, there will be more mutations and more risk that the virus will become permanent. If we don’t vaccinate the world quickly, all our COVID efforts will be a waste |jakemeth |November 12, 2020 |Fortune 

Haringey Council told The Daily Beast that the children had not been taken permanently into state care. Britain May Spy on Preschoolers Searching for Potential Jihadis |Nico Hines |January 7, 2015 |DAILY BEAST 

Hitchcock was our mountains and our rivers, curled permanently into our brainpans. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He survived, Risner says, but was left permanently injured by a bullet to his spine. Drug Smuggler Sues U.S. Over Dog Bite |Caitlin Dickson |December 10, 2014 |DAILY BEAST 

And not all illegal immigrants remain in the country permanently. The Case for More Low-Skill Immigration |Veronique de Rugy |December 7, 2014 |DAILY BEAST 

Is he the type of character who would ever join the group permanently, or is he more of a drift-in, drift-out kind of guy? ‘Walking Dead’ Showrunner Scott Gimple Teases ‘Darker, Weirder’ Times Ahead |Melissa Leon |December 2, 2014 |DAILY BEAST 

The box B is permanently supplied with air under pressure from the bellows. The Recent Revolution in Organ Building |George Laing Miller 

It is less troublesome, as well as less expensive, than a hot supper, and the custom will be a good one to adopt permanently. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

However, one-third of the area of the state—which had become West Virginia—was permanently separated. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Her nominee was unable to hold his ground in Kerry, nor was the Papal Bishop permanently resident. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell 

About half this great counter was permanently immersed, and when a boat laid over, almost the whole of it came into bearing. Yachting Vol. 2 |Various.