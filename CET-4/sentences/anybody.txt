Also, use hand sanitizer before you knock on doors and ring bells to protect anybody who may come after you. Five ways to help democracy even if you can’t vote |Sandra Gutierrez G. |October 21, 2020 |Popular-Science 

I don’t know anybody who really believes in dirty air or dirty water. Why America’s volunteer spirit could save the election |jakemeth |October 19, 2020 |Fortune 

His ability to play one through five, guard anybody on the floor, take the challenge, not only guard on the perimeter, and continue to protect the paint. Anthony Davis Was Key For The Lakers’ Title Run. He’s Also The Key To Their Future. |James L. Jackson |October 14, 2020 |FiveThirtyEight 

He’s decided to stifle his beliefs so as not to make waves with anybody in the condominium, he said, adjusting his mask to better cover his mouth and nose. Why earnings season could be the next big lift for stocks |Bernhard Warner |October 12, 2020 |Fortune 

Bear in mind that you can share these links as many times and with as many people as you want, but the link is public and anybody who has it can access your playlist. The best way to share playlists on every major platform |David Nield |October 8, 2020 |Popular-Science 

I remember all our music appeared on Spotify overnight, without anybody asking us. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

As anybody who has seen his now famous rant on Parks and Recreation knows, Patton Oswalt can get a little obsessed. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

“Any time you put a foreign substance into anybody you have the potential for an adverse event,” Geisbert reminds. Uh Oh: Ebola Vaccine Trials Stop |Leigh Cowart |December 19, 2014 |DAILY BEAST 

“He has one of the most unabashedly pro-life records as anybody in the field,” said Mackowiak. How A Jeb Bush Candidacy Would Hurt Chris Christie And Shake Up The 2016 GOP Field |David Freedlander |December 16, 2014 |DAILY BEAST 

I harbor a rock ‘n’ roll fantasy, just like anybody, and I welcomed the challenge. Michael C. Hall on Going Drag for ‘Hedwig and the Angry Inch’ and Exorcising ‘Dexter’ |Marlow Stern |December 4, 2014 |DAILY BEAST 

She would never forget it; but realizing its gravity, she decided thereupon never to tell it—the dream—to anybody. The Homesteader |Oscar Micheaux 

Mr. Crow was rocking back and forth on his perch, for a joke—on anybody except himself—always delighted him. The Tale of Grandfather Mole |Arthur Scott Bailey 

It's quite true the land can't run away, but there are always rows and revolutions and smashes going on; you can't trust anybody. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Why, 'pon my word, I'm bound to say that I'm just as much in the dark as anybody else, if it comes to that! Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

Never did I feel leaving anybody or any place so much, and Berlin seems to me like a great roaring wilderness. Music-Study in Germany |Amy Fay