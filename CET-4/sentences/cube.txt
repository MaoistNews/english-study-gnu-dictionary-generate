The water will freeze when it hits the cube, and then stack up — forming a tower of ice. Build ice towers with bottled water and ice |Bethany Brookshire |September 16, 2020 |Science News For Students 

One net for the cube, for example, is a T shape made of six squares. Mathematicians Report New Discovery About the Dodecahedron |Erica Klarreich |August 31, 2020 |Quanta Magazine 

When I stagger back into my apartment an hour later, those pink, fleshy cubes will have a light rime. The Sublime Agony of Hot-Weather Running |Martin Fritz Huber |August 27, 2020 |Outside Online 

On camping trips that have a multi-sport agenda, I pack the cubes based on activity—my trail running kit in one cube, mountain biking clothes in another, and camp pajamas in another. The Matador SEG42 Offers Unrivaled Gear Organization |Graham Averill |August 25, 2020 |Outside Online 

The integrated packing cubes are the obvious standout feature. The Matador SEG42 Offers Unrivaled Gear Organization |Graham Averill |August 25, 2020 |Outside Online 

Geometrically, it is a cube being forced into a rhomboid by the fault. Silicon Valley Mansions, Swallowed Alive |Geoff Manaugh |November 8, 2014 |DAILY BEAST 

“Ice Cube and Schoolboy Q.” Osorio seems dumfounded by the decision. It Was All a Dream: Drama, Bullshit, and the Rebirth of The Source Magazine |Alex Suskind |October 14, 2014 |DAILY BEAST 

Wiz Khalifa and Young Jeezy were on the cover; April's issue had Schoolboy Q and Ice Cube. It Was All a Dream: Drama, Bullshit, and the Rebirth of The Source Magazine |Alex Suskind |October 14, 2014 |DAILY BEAST 

The shooting Ice Cube was referring to was the accidental killing of Karen Toshima by a stray bullet. A Brief History of the Phrase 'F*ck the Police' |Rich Goldstein |August 23, 2014 |DAILY BEAST 

[Laughs] The fact that Ice Cube even knows who I am is still awesome to me. Channing Tatum and Jonah Hill on ‘22 Jump Street,’ Penis Kissing, and Julie Andrews’s Boobs |Kevin Fallon |June 10, 2014 |DAILY BEAST 

Ethel's mind was in too irritated and tumultuous a state for her to derive her usual solace from Cube Root. The Daisy Chain |Charlotte Yonge 

It consists of vaulted terraces, raised one above another, and resting upon cube-shaped pillars. The Geography of Strabo, Volume III (of 3) |Strabo 

Ya notice how that funny-looking cube inside there gets bigger every time you look at it? Vanishing Point |C.C. Beck 

It sometimes assumes twelve-sided shapes, or is merely a cube, or yet again variations of these figures. Stories about Famous Precious Stones |Mrs Goddard Orpen 

Its measurement is roughly a cube of eight feet, it must weigh nearly twenty tons and it rests on three large slabs of stone. Guatemala, the country of the future |Charles M. Pepper