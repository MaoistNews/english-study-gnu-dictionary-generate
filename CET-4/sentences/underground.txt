Italy may owe some of its seismic activity to carbon dioxide bubbling up from deep underground. Carbon dioxide from Earth’s mantle may trigger some Italian earthquakes |Maria Temming |August 26, 2020 |Science News 

So the group of refuseniks — one of many underground cells plotting freedom — decided to hijack a plane. The Failed Hijacking That Remade the Soviet Union |Eromo Egbejule |August 24, 2020 |Ozy 

Archaeologists had known since 1916 that some holes lurked underground. Underground mega-monument found near Stonehenge |Avery Elizabeth Hurt |August 11, 2020 |Science News For Students 

Data from NASA’s Dawn orbiter, the study suggests, show signs that it may be harboring an ocean deep underground. The dwarf planet Ceres might be home to an underground ocean of water |Neel Patel |August 11, 2020 |MIT Technology Review 

For example, the team found that a seismometer placed 380 meters underground near Auckland, New Zealand, was not only registering human activity, but saw that activity halved during the lockdowns. COVID-19 lockdowns dramatically reduced seismic noise from humans |Carolyn Gramling |July 23, 2020 |Science News 

But underground classes have Persians getting with the beat. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

Atefeh says the participants in the underground classes she attends are mainly young women. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

Youssef said the jailings are not only driving the community underground but pushing many to move abroad. Sisi Is Persecuting, Prosecuting, and Publicly Shaming Egypt’s Gays |Bel Trew |December 30, 2014 |DAILY BEAST 

“He literally went underground to hold services,” Moscow-based dissident and journalist Victor Davidoff said in an email. Remembering the Russian Priest Who Fought the Orthodox Church |Cathy Young |December 28, 2014 |DAILY BEAST 

Unfortunately, the underground tunnels that were used to transport booze and, if necessary, escaping patrons, are off-limits. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

All over the world the just claims of organized labor are intermingled with the underground conspiracy of social revolution. The Unsolved Riddle of Social Justice |Stephen Leacock 

One thing was certain: Grandfather Mole could travel much faster through the water than he could underground. The Tale of Grandfather Mole |Arthur Scott Bailey 

And when he took an underground stroll he was almost sure to find a few angleworms, which furnished most of his meals. The Tale of Grandfather Mole |Arthur Scott Bailey 

When a besieged city suspects a mine, do not the inhabitants dig underground, and meet their enemy at his work? The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

There was a look in his eyes almost as of one coming back from a long and dark journey underground into the light of day. Bella Donna |Robert Hichens