That low-oxygen setting almost certainly slowed tissue decay, the scientists say, giving it time to be preserved. How fossilization preserved a 310-million-year-old horseshoe crab’s brain |Rebecca Dzombak |August 20, 2021 |Science News 

In the decay of the Clubhouse model, we sense that the creator economy is all hat and not enough cowboy. Gillmor Gang: Social climbing |Steve Gillmor |July 30, 2021 |TechCrunch 

These fossils preserve an incredible amount of anatomical detail, as well as behaviors, mainly because very little decay takes place after the organism is rapidly trapped in the resin. Ancient Brains: Inside the Extraordinary Preservation of a 310-Million-Year-Old Nervous System |John Paterson |July 30, 2021 |Singularity Hub 

Her sincerity is most evident when she captures signs of decay. D.C. is a city of grand monuments and federal buildings. These photos capture the often unseen poetic details. |Kelsey Ables |July 30, 2021 |Washington Post 

Also, their penchant for tunneling creates a naturally aerated environment for the decay to happen. Best compost bins: These eco-friendly products help you to do your part for the environment |Florie Korani |July 20, 2021 |Popular-Science 

Before anti-vaxxers, there were anti-fluoriders: a group who spread fear about the anti-tooth decay agent added to drinking water. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

As a means of preventing tooth decay in those cities that do fluoridate, the practice certainly looks like a success. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Their decay proceeded without a ready supply of oxygen, producing hydrocarbons like methane instead of oxygen-bearing molecules. Methane on Mars: Life or Just Gas? |Matthew R. Francis |December 17, 2014 |DAILY BEAST 

Political Order and Political Decay Francis Fukuyama (Farrar, Straus, and Giroux) How are strong democratic states created? The Best Big Ideas Of 2014 |William O’Connor |December 11, 2014 |DAILY BEAST 

A drawing of what was deemed a “deer pig” was also sent through the uranium decay ringer. The Oldest Cave Art May Not Be in Europe |Justin Jones |October 9, 2014 |DAILY BEAST 

The General in command of the station was a feeble old man, suffering from senile decay. The Red Year |Louis Tracy 

It will hold tenaciously there, the last of its race, days after the decay of its greener and more healthy-looking mates. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The decay and ruin of nearly all the "old families" in Ireland are among the penalties of disregarding it. Glances at Europe |Horace Greeley 

The old dining-hall had shared in the general decay, and been shorn of all its ancient honours. The World Before Them |Susanna Moodie 

It is likewise formed daring the decay of animal and vegetable matters, and is consequently evolved from dung and compost heaps. Elements of Agricultural Chemistry |Thomas Anderson