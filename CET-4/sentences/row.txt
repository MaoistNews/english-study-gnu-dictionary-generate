While just about everyone has been affected by misinformation in the digital age, Jain has had more of a front row view than most. He’s Fighting QAnon With Sunlight |Nick Fouriezos |September 6, 2020 |Ozy 

The Bucks had the top defensive regular-season rating in the league for the second year in a row, and Antetokounmpo earned the Defensive Player of Year award. Giannis Is Doing More Work In Less Time |Andres Waters |September 4, 2020 |FiveThirtyEight 

Yet we also think the AFC East — which New England has won 11 times in a row — is very much up for grabs. Newton Can Replace Brady, But Can The Pats Replace Half Of Their Defense? |Neil Paine (neil.paine@fivethirtyeight.com) |September 3, 2020 |FiveThirtyEight 

The chip is made completely of silicon and has rows of triangular holes. 6G Will Be 100 Times Faster Than 5G—and Now There’s a Chip for It |Vanessa Bates Ramirez |August 21, 2020 |Singularity Hub 

As a result, China’s retail sales have fallen for five months in a row since the pandemic began to spread nationally in February. Haves and Have-Nots: Pandemic Recovery Explodes China’s Wealth Gap |Daniel Malloy |August 19, 2020 |Ozy 

They stood in a single row, united by solemn respect as the Liu family remained inside. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

Last September, the "designer" duo got booed at Lanvin's Paris fashion show after they arrived late to their front row seats. Kanye West and Kim Kardashian’s Balmain Campaign: High Fashion Meets Low Culture |Amy Zimmerman |December 23, 2014 |DAILY BEAST 

Brown had been serving a life sentence; McCollum had been on Death Row. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

Detainees forced to stand on broken legs, or go 180 hours in a row without sleep. The Most Gruesome Moments in the CIA ‘Torture Report’ |Shane Harris, Tim Mak |December 9, 2014 |DAILY BEAST 

Later that day he made a call from the row of phones in the yard and reached his wife for the first time in six months. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

Of course, my first row was a long one, quite through the city from west to east, including innumerable turnings and windings. Glances at Europe |Horace Greeley 

The way was under a double row of tall trees, which met at the top and formed a green arch over our heads. Music-Study in Germany |Amy Fay 

Chumru, though no fighting-man, realized that he was expected to make a row and uttered a bloodcurdling yell. The Red Year |Louis Tracy 

I would have examined the poor man, but the friends kicked up a great row and shoved me off. Hunting the Lions |R.M. Ballantyne 

Now they are approaching the long row of noble beeches that line the boundary of Mortlake. Checkmate |Joseph Sheridan Le Fanu