I read The Autobiography of Malcolm X as told to by Roots author Alex Haley for the first time in college. How Laurence Fishburne Gave Voice To ‘The Autobiography Of Malcolm X’ |Joi-Marie McKenzie |September 17, 2020 |Essence.com 

Sadly, most of these innovations never took hold in our schools and colleges, and remote learners today are left with edtech that feels like it is still trapped in the 90s. Why hasn’t digital learning lived up to its promise? |Walter Thompson |September 17, 2020 |TechCrunch 

InsideSherpa, a Y Combinator graduate, hosts virtual work experience programs for college students all around the world. Forage, formerly InsideSherpa, raises $9.3 million Series A for virtual work experiences |Natasha Mascarenhas |September 17, 2020 |TechCrunch 

The best laid plans of colleges and universities are no assurance during a public health crisis. Even the most cautious schools are seeing outbreaks |Sy Mukherjee |September 17, 2020 |Fortune 

Still, Mulligan’s analysis considered Sanders’s proposals for universal health care, “free public college, free child care … a full transformation of the energy sector” and more. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

This is the Mexico that U.S. college students would be wise to steer clear of on spring break. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

Under the current president and his predecessor, Jett notes, the ambassadorship of Belize has gone to college roommates. U.S. Embassies Have Always Been for Sale |William O’Connor |January 2, 2015 |DAILY BEAST 

If the oft-talked-about college “hook-up culture” could be embodied by a place, it would be Shooters. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 

In my four years of college, I know exactly one woman who has asked a man out on a date. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 

This was also the year Duke University student Belle Knox put college girls on the map. Porn Stars on the Year in Porn: Drone Erotica, Belle Knox, and Wild Sex |Aurora Snow |December 27, 2014 |DAILY BEAST 

He was converted and baptized, and was the first Hebrew instructor at Harvard college. The Every Day Book of History and Chronology |Joel Munsell 

They are very urgent questions; our sons and daughters will have to begin to deal with them from the moment they leave college. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

But you will find most colleges and most college societies bar religious instruction and discussion. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

In practice we find a good deal of technical study comes into the college stage. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

In Scotland and America that is distinguished and thought of clearly as the college stage. The Salvaging Of Civilisation |H. G. (Herbert George) Wells