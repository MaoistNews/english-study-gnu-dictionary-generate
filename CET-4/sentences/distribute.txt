Biden also called for any potential vaccine to be equitably distributed. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

Today, StarTech’s being managed a lot more conservatively than in 2015, as shown by the share of earnings it’s distributing instead of re-investing. Will tech stocks stumble or slide? What the fundamentals tell us |Shawn Tully |September 16, 2020 |Fortune 

This lightweight vest is made from comfy neoprene material and has an ergonomic design, so the weights are distributed equally. The best weighted vests for your next tough workout |PopSci Commerce Team |September 16, 2020 |Popular-Science 

States and local communities will need to devise precise plans for receiving and locally distributing vaccines, some of which will require special handling such as refrigeration or freezing. U.S. outlines sweeping plan to provide free COVID-19 vaccines |Rachel Schallom |September 16, 2020 |Fortune 

“Claims of having a vaccine ready to distribute before you do testing, I think, is problematic, at best,” Fauci said. China’s controversial emergency-use program for COVID vaccines is going global |Grady McGregor |September 16, 2020 |Fortune 

Surenos are told when to workout, who to associate with and how to distribute any funds they make from illegal activity. The Mexican Mafia Is the Daddy of All Street Gangs |Seth Ferranti |December 11, 2014 |DAILY BEAST 

Pinch it with your fingers until it makes large crumbles and distribute it on the berries (it will not cover them entirely). The Barefoot Contessa Knows How To Make Us Crumble |Ina Garten |November 30, 2014 |DAILY BEAST 

To be sure, it had plenty of big newspaper plants, but could we bring our papers back to Baltimore in time to distribute them? The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 

Rather than focus on building large centralized plants, why not distribute solar power across a bunch of rooftops? Panel Discussion |The Daily Beast |September 8, 2014 |DAILY BEAST 

He blanched when it came time to plunge, invest the money, press, and distribute. The Stacks: How Leonard Chess Helped Make Muddy Waters |Alex Belth |August 2, 2014 |DAILY BEAST 

In the time when thou shalt end the days of thy life, and in the time of thy decease, distribute thy inheritance. The Bible, Douay-Rheims Version |Various 

To the greatest extent of the capacities of all, it is dutiful for them to obtain and distribute copies of the blessed word. The Ordinance of Covenanting |John Cunningham 

It would endanger me to distribute such a fiery appeal, my friend remonstrated. Prison Memoirs of an Anarchist |Alexander Berkman 

You would like us to print the Testament of Jean Meslier and distribute four or five thousand copies. Superstition In All Ages (1732) |Jean Meslier 

Even we publishers have our ideals—and our purest is to distribute through the world the works of a man of genius. Jaffery |William J. Locke