高管们还认为，如果你知道自己可以通过Zoom完成某件事，那么在全国范围内飞行的速度和面对面会议的数量仍将保持不变，现在可能会被视为效率低下、成本过高“我们有能力”：冠状病毒危机如何加速广告向敏捷的转变|克里斯蒂娜·蒙略斯| 2020年9月7日|迪吉德
国际能源署指出，现有的最佳技术的效率是全世界实际使用的平均水平的两倍多，是市场上效率最低的产品的三倍多。空调技术是应对气候变化的伟大机遇|詹姆斯·坦普尔| 2020年9月1日|麻省理工技术评论
在这次抓捕之前，我们一直把套索地毯放在河岸边我们希望猫头鹰能够降落的地方，这样做效率很低。捕捉和拯救世界上最大猫头鹰的探索|乔纳森·斯莱特| 2020年8月28日|科普
在那些不太可能在第四次工业革命的结构性变革中生存下来的公司中保留工作岗位在经济上是低效的我们将共同努力：到2021年底，德国将继续补贴工人工资|大卫·迈耶| 2020年8月26日|财富|
事实上，在某种程度上，习近平本人之所以被选中是因为该党担心，在非典过后的一段时间里，它会失去对部分社会的控制，因为它被视为腐败、低效、无能。新冠病毒-19是否会引发与中国的冷战（或更糟）？（第414页）|斯蒂芬·J·杜布纳| 2020年4月23日|畸形经济学
听到她的故事后，优步为“效率低下的路线”道歉，并部分退还了车费。Uber十大最糟糕恐怖故事《奥利维亚·努齐》（2014年11月19日）《每日野兽》
一名女子声称优步司机绑架了她，优步声称“路线低效”。优步十大最糟糕恐怖故事《奥利维亚·努齐》2014年11月19日《每日野兽》
这种福利支出阻碍了工作，增加了税收，并作为对低工资企业的隐性低效补贴。为了让胜利持久，共和党必须确定最低工资| Dmitri Mehlhorn | 2014年11月6日| DAILY BEAST
官僚主义是低效和不诚实的，也许不是故意的。但是因为运动部件太多了。迈克·利奇（Mike Leach）与动机杀人凶手杰罗尼莫（Geronimo）|詹姆斯·A·沃伦| 2014年8月17日|每日野兽
一架航班的消失和另一架航班的击落将这艘效率低下的航母推向了崩溃的边缘。马来西亚航空公司即将倒闭|克莱夫·欧文| 2014年8月1日|每日野兽
因此，使用水泥浆是工艺低效的标志，不应在良好工作中予以支持。大英百科全书，第11版，第4卷，第3部分
在公共警察效率低下的情况下，他们继续为社区提供一些服务。英国Gilds | Francis Aiden Hibbert的影响与发展
上一任州长很善良，但效率低下，几个月前被派往西印度群岛，在那里他被正式埋葬。在非洲| John T.McCutcheon
我们的献身精神是不是被这种新学说所造就的可怜的、低效的、笨拙的发明所付出的？美国自由的关键注释|各种
拿破仑的国务委员会对犯错误的妇女十分温柔，效率很低。Honorine | Honore de Balzac