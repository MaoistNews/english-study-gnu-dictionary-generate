福特工程师对全轮驱动系统进行了调整，使Mach-E有一种活泼的后驱感，无论天气好还是坏，都能显示出来。福特的电动野马Mach-E是迈向未来的重要飞跃|丹·卡尼| 2021年2月12日|科普
它生动地讲述了勇敢、背叛和复仇的扣人心弦的故事。娜塔莉·海恩斯的《一千艘船》精彩地再现了特洛伊战争|卡罗尔·门莫特| 2021年2月8日|华盛顿邮报|
尽管有周期性的叙事迂回，这本书还是以生动的节奏前进。决心行医的两姐妹违抗惯例|珍妮特·戈尔登| 2021年2月5日|华盛顿邮报|
Silverman随后与Sacha Baron Cohen和记者Sheera Frenkel就社交媒体的弊病展开了热烈的讨论。莎拉·西尔弗曼只是想把事情做好|杰夫·埃德格斯| 2021年2月4日|华盛顿邮报
由地下水分痕迹驱动的RSL将使火星更加活跃，但可能不是一个有生命的火星。火星上的睫毛膏状条纹可能是由泥泞和山体滑坡引起的。|查理·伍德| 2021年2月3日|科普
乐队转过身来，奏出一支活泼的曲子，表示生活将继续。纽约警察局工会老板迈克尔·戴利2015年1月5日《每日野兽》的葬礼抗议太多了
它的总部设在斯坦利维尔，在一个高大的角落建筑，仍然站在破旧，但活跃，市中心。刚果被遗忘的殖民地逃亡|尼娜·斯特罗奇利奇| 2014年12月18日|每日野兽
她是一个30岁左右的红发女孩，活泼的蓝眼睛，身材娇小，很有胆量。盖伊·塔利斯在查理·曼森牧场的家中|盖伊·塔利斯| 2014年10月31日|每日野兽
生动的讨论、鼓舞人心的故事和德克萨斯州的热情好客！世界女性德克萨斯偷窥|辛西娅·阿勒姆| 2014年10月20日|每日野兽
并不是每个人都同意我们的观点，但这会引发一场生动的对话。天花板漏水、猫叫声和未封住的蟒蛇：在纽约最差的地铁上4小时|凯文·扎瓦基| 2014年8月8日|每日野兽
杂志给我们写了一篇生动活泼的文章，报纸一步一步地给我们写了一篇小插曲，一次皇家巡演。布莱克伍德的爱丁堡杂志，第CCCXXXIX号。1844年1月。第LV卷|各种各样的
诚然，她对她哥哥的所有策展人都很感兴趣，但这始终是一种职业兴趣，纯粹是柏拉图式的。《Pit Town Coronet》，第一卷（共3卷）|查尔斯·詹姆斯·威尔斯
维尼弗雷德天生是一个精神饱满、活泼的女孩，很快就从那致命的星期天晚上的恐惧中恢复过来。红色的一年|路易斯·特蕾西
他喜欢可怜的人，但幽默最能打动他，无论我们走到哪里，他生动活泼的礼物都受到欢迎。英格兰、苏格兰和爱尔兰50年的铁路生活|约瑟夫·塔洛
“我的名字叫斯里科，”活泼的小松鼠女孩一边跳一边回答。滑稽猪眯眼|理查德·巴纳姆