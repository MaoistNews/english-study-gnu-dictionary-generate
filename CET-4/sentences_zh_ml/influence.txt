他不知道小组中还有谁，也不知道小组会对整个过程产生多大影响。Oceanside正在重新考虑其警察局长的招聘流程，原因是社区的担忧| Kayla Jimenez | 2020年9月14日|圣地亚哥之声|
在黄光裕的领导下，英伟达在市场价值和影响力方面迅速崛起，跻身科技公司行列。Nvidia正在收购软银的Arm芯片部门，这是有史以来最大的半导体交易《财富》杂志记者Claire Zillman，2020年9月14日
通过广泛的观测，他们得以重建被三颗恒星撕裂的圆盘的三维结构。一个奇怪的尘土飞扬的圆盘可能将一颗行星隐藏在三星之间|保拉·罗莎·阿基诺| 2020年9月11日|科普
考虑到风向对飓风的速度和轨迹有多大的影响，气候变化可能会通过重塑大规模的风向模式来影响失速。缓慢蜿蜒的飓风通常更为危险，而且越来越常见。| Greta Moran | 2020年9月9日|科普
综上所述，政府对医疗创新具有重大影响。当政府愿意时，冲突时代可能导致医疗创新|杰弗里·克莱门斯/对话| 2020年9月9日|科普
例如，警察有工会，这些工会影响其文职领导人的选举。《老兵的观点：纽约警察和市政厅之间的冷战》|马特·加拉赫| 2014年12月29日|每日野兽报
使他与许多同时代人不同的是，他罕见地免受主流思想的影响。与希特勒较量的天主教哲学家|约翰·亨利·克罗斯比| 2014年12月26日|每日野兽
E街乐队吉他手史蒂文·范·赞特（Steven Van Zandt）曾盛赞道：“他把雷·查尔斯（Ray Charles）带到了混音中，对摇滚乐产生了影响。”。Joe Cocker的深度现场切割| Asawin Suebsaeng | 2014年12月22日|每日野兽
这些妇女将圣徒的尸体埋葬在自己的土地上，偶尔也会影响教皇的政治。第一位英国圣公会女主教回归基督教根基| Candida Moss | 2014年12月18日| DAILY BEAST
主要竞争对手印度有钱，伊朗和俄罗斯也在该地区施加影响。巴基斯坦与恐怖分子的舞蹈适得其反，导致132名儿童死亡| Chris Allbritton | 2014年12月17日| DAILY BEAST
在与尤金·马内结婚后，她受到了他著名的哥哥埃杜阿德的影响。从公元前七世纪到公元二十世纪的女性美术作品|克拉拉·厄斯金·克莱门特
她的方法是如此聪明，毫无疑问，她对他的艺术记忆的持久性产生了巨大的影响。从公元前七世纪到公元二十世纪的女性美术作品|克拉拉·厄斯金·克莱门特
梅耶少校很可能早在1802年就受到普鲁德洪的影响，可能是在那之前。从公元前七世纪到公元二十世纪的女性美术作品|克拉拉·厄斯金·克莱门特
声音是表达最有力的影响力，是灵魂与灵魂之间的有翼信使。富有表现力的声音文化|杰西·埃尔德里奇·索斯威克
然而，在美丽的抚慰下，牧师忘记了自己的不幸。《Pit Town Coronet》，第一卷（共3卷）|查尔斯·詹姆斯·威尔斯