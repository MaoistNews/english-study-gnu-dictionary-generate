此外，她说，“我确实担心疫苗犹豫不决的群体。”如果我们广泛分享，冠状病毒疫苗将拯救更多的生命| Kat Eschner | 2020年9月17日|科普
然而，营销人员的内部模型通常不涉及个别媒体渠道的细节。NBCUniversal测试新的测量程序，以证明它可以推动广告商的产品销售|蒂姆·彼得森| 2020年9月17日| Digiday
在一些州，如内布拉斯加州，在没有全州指令的情况下，个别县正在扩大投票时的邮件访问。邮寄投票：哪些州允许缺席投票|凯特·拉比诺维茨，布列塔尼·梅耶斯| 2020年9月17日|华盛顿邮报
它是否会作为一项单独的立法被采纳，还有待观察。为什么立法者可能会为第二轮新冠病毒刺激计划选择更具针对性的方法| Aric Jenkins | 2020年9月16日|财富|
在过去的十年中，洛杉矶和加利福尼亚州其他各县已经停止使用帮派禁令，因为研究证明，帮派禁令对家庭、社区、个人乃至社会都有害。当我们重新思考治安时，是时候结束帮派禁令了|杰米·威尔逊| 2020年9月15日|圣地亚哥之声|
每个种族都涉及到研究人员、制造商和公共卫生实体之间不同寻常的合作。埃博拉疫苗竞赛| Abby Haglage | 2015年1月7日| DAILY BEAST
他们能否确定公民个人不应享有宪法规定的权利？佛罗里达州为阻止同性婚姻而进行的暗斗终于结束了|杰伊·迈克尔森| 2015年1月5日|每日野兽|
任何事情，包括法律本身，都必须有足够的空间让个人承担责任。繁文缛节扼杀了善良的撒玛利亚人|菲利普·K·霍华德| 2014年12月27日|每日野兽报
由于讨论人事问题的敏感性，这两个人都不会被记录在案。自由主义英雄瓦茨拉夫·克劳斯（Vaclav Klaus）被卡托研究所（James Kirchick）2014年12月22日《每日野兽》（DAILY BEAST）剪掉了翅膀
槲寄生感染可以杀死个别树木和林分，大多数槲寄生物种攻击特定的树种。槲寄生是植物的吸血鬼|海伦·汤普森| 2014年12月21日|每日野兽
个别样品可能呈微碱性，尤其是在饱餐后。临床诊断手册|詹姆斯·坎贝尔·托德
单个棱柱体通常很细，有一个斜面，楔形端部，但有时是针状的。临床诊断手册|詹姆斯·坎贝尔·托德
作为纯经济学的一部分，雇主个人对此毫无兴趣。社会正义的未解之谜|斯蒂芬·利科克
一天十个小时，从一般意义上讲，不考虑个别例外，可能比一天十二个小时更有效率。社会正义的未解之谜|斯蒂芬·利科克
他们以部落的身份发动战争，因为他们对个人犯下了错误。《耶稣会关系与同盟文献》，第二卷：阿卡迪亚，1612-1614