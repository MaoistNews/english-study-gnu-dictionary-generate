然后，在2018年，它启动了该项目的新阶段，将一个数据中心淹没在苏格兰奥克尼群岛附近的海床上。微软对其海底数据中心实验的成功表示欢迎，并表示这可能对旱地也有影响
当我把它们放在小溪边时，我很惊讶它们像鱼一样游走了，消失在对面的水下植被中。如何寻找星鼻鼹鼠（及其洞）|肯尼斯·卡塔尼亚| 2020年9月15日|科普
豪尔预计，总共有1300万美国人将被迫离开淹没的海岸线。气候变化将迫使新的美国移民——Abram Lustgarten，Meridith Kohut摄影——2020年9月15日——ProPublica
玩具船沿着悬停液体的底部摆动，因为就像漂浮在顶部的船一样，玩具部分被淹没。玩具船在一层悬浮液体下倒置漂浮| Maria Temming | 2020年9月2日|科学新闻
一艘部分淹没、倒置的船也会经历同样的向上拉力。玩具船在一层悬浮液体下倒置漂浮| Maria Temming | 2020年9月2日|科学新闻
韭菜、草本植物和绿叶植物是在水培系统中生长的，水培系统将植物根系完全浸没在水中，而不使用土壤。美国的垂直室内农场正在成长|米兰达格林| 2013年5月7日|每日野兽
从市中心到布鲁克林炮台隧道，观看飓风桑迪淹没城市。七个令人震惊的纽约市洪水视频| Ben Teitelbaum | 2012年10月30日| DAILY BEAST
将苹果片浸入糖浆中，让它们“休息”几个小时，或者更好的是，过夜。新鲜精选| Alex Guarnaschelli | 2011年9月15日|每日野兽
他们不愿意为了赢得选举而掩饰自己的愤怒。共和党正在搞砸它|彼得·贝纳特| 2010年6月9日|每日野兽
将其乘以所需次数，以完全淹没火鸡。简单烤火鸡、香菇肉汁|每日野兽| 2008年11月25日|每日野兽
她试图把这个想法推出去，把它淹没在一阵嘲笑之下；但是没有用。黎明勇士|霍华德·卡尔顿·布朗
如果我们要把祖父号淹没，我就得用潜望镜给卡西迪下达命令。马特汽车的危险，或者说，在巴哈马被抛弃|斯坦利·R·马修斯
然后他的成功开始淹没了他：他在厚厚的信件雨中喘着粗气。人与鬼的故事|伊迪丝·沃顿
他没有等主人，就潜入水中，发现水深得几乎要把他淹没。巴拿马英雄| F.S.布雷顿
现在，她内心产生了强烈的好奇心，似乎暂时淹没了她的不安和恐惧。十二月之爱|罗伯特·希金斯