它还为众议院共和党议员提供了借口，说他们投票支持格林纯粹是为了防止民主党滥用他们作为多数党的权力。Marjorie Taylor Greene委员会罢免投票，解释为| Gabby Birenbaum | 2021年2月5日| Vox
当你认为内容是纯粹的获取时，你会被获取的药物蒙蔽了双眼。将您的SEO内容带到收购之外| Mordy Oberstein | 2021年2月2日|搜索引擎观察
这并不是说美国会将人工智能用于纯粹的道德目的。中国想成为世界人工智能超级大国。它有它所需要的吗|凡妮莎·贝茨·拉米雷斯| 2021年1月17日|奇点中心
当然，最好的会议仍然依靠出色的演讲者和可采取行动的外卖而获胜，但遵循在线活动的亲身体验并不能充分利用纯数字媒体所提供的优势。为什么我们要为SMX创造新的内容体验| Henry Powderly | 2021年1月13日|搜索引擎领域
特斯拉首席执行官同意不收取工资或奖金，以换取纯粹基于绩效的薪酬计划。特斯拉创始人埃隆·马斯克（Elon Musk）超越杰夫·贝佐斯（Jeff Bezos）成为世界首富迈克尔·科伦（Michael J.Coren）（2021年1月7日）
事实上，作为一家几乎纯粹的咨询公司，拉扎德（恰当地）几乎没有受到多德-弗兰克改革的影响。安东尼奥·韦斯不是问题的一部分|史蒂夫·拉特纳| 2015年1月7日|每日野兽
传统上，通俗历史几乎完全由叙事驱动。感谢国会，而不是LBJ《伟大社会》|朱利安·泽利泽，斯科特·波奇| 2015年1月4日|每日野兽报
撒玛利亚指南是围绕自杀是一种纯粹的非理性行为这一假设编写的，这种行为是由疾病引起的。掩盖和关注巨魔：事实上，这是关于自杀新闻中的道德问题| Arthur Chu | 2015年1月3日| DAILY BEAST
纯属偶然，安娜·科伦在巧克力店围攻开始之际降落在悉尼。CNN《一夜悉尼之星》|劳埃德·格罗夫| 2014年12月16日|每日野兽报
首先，从一个纯粹实用的、全副武装的位置出发，我说如果你能做这项工作，你就应该保留这项工作。金斯伯格法官还不应该辞职|凯文·布莱耶| 2014年12月1日|每日野兽
在这里，正如在许多这些幼稚的赞美中一样，我们不需要纯粹的感觉。儿童的方式|詹姆斯·萨利
在前一章中，我们考察了机械生产时代纯机械的一面。社会正义的未解之谜|斯蒂芬·利科克
诚然，她对她哥哥的所有策展人都很感兴趣，但这始终是一种职业兴趣，纯粹是柏拉图式的。《Pit Town Coronet》，第一卷（共3卷）|查尔斯·詹姆斯·威尔斯
他的野心纯粹是自私的，而我的野心显然是仁慈的。《Pit Town Coronet》，第一卷（共3卷）|查尔斯·詹姆斯·威尔斯
我们现有的贫困纯粹是人类努力方向和分配方面的问题。社会正义的未解之谜|斯蒂芬·利科克