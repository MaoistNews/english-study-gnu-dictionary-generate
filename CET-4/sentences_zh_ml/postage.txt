她通过了改善年轻人投票机会的立法，取消了邮寄选票的邮费和其他许多费用。政治报道：雪莉·韦伯的鞋子将填补|斯科特·刘易斯| 2020年12月26日|圣地亚哥之声
电子投票要求选民提供自己的信封并贴上邮资。马里兰投票指南：提前投票的相关知识，邮寄选票|艾琳·考克斯| 2020年10月29日|华盛顿邮报
在一份声明中，该部列出了它在法律规定的最低限度之外为增加访问量而采取的其他几项措施，包括为返回邮寄选票提供预付邮资。宾夕法尼亚州新的邮寄投票法扩大了除穷人以外的所有人的投票权，作者：乔纳森·赖、萨曼莎·梅拉米德和迈克尔·邦德，《费城询问者》于2020年10月22日出版
其中，科罗拉多州和犹他州要求选民支付邮寄选票的邮费。免费邮寄投票是提高选民投票率的一种快捷方式| Annalisa Merelli | 2020年9月23日|石英
如果没有，请联系您当地的邮局，询问其选票邮寄政策。报告方法：如何通过邮件报告投票|由Rachel Glickhouse和Jessica Huseman | 2020年9月2日| ProPublica
新闻稿中写道：“至少可以说，在美国邮票上纪念捕食者哈维·米尔克令人不安。”。边缘因素：让哈维·米尔克远离我们的邮件|Caitlin Dickson | 2014年6月1日|每日野兽
鲍勃·拉森（Bob Larson）在内布拉斯加州的麦考克（McCook）长大，这是一个拥有约7000人和23座教堂的集邮小镇。我的295美元Skype驱魔|斯科特·比克斯比| 2014年2月6日|每日野兽
她坚持不懈地努力让我父亲获得美国邮票的荣誉。贝蒂·沙巴兹在丈夫马尔科姆X被杀后如何坚持|伊利亚萨·沙巴兹| 2013年2月2日|每日野兽
投票者可以发送的邮件数量没有限制，候选人在邮费上也没有特别折扣。超级太平洋委员会对2012年总统选举的影响是否被夸大了|Ben Jacobs | 2012年2月16日|每日野兽
然后哈里斯走到邮票大小的舞台上，脸上闪现出选美皇后的微笑，并敦促她的观众们蹲下来。加利福尼亚州咬指甲比赛|吉娜·皮卡洛| 2010年11月3日|每日野兽
当时，来自该地区的信件邮资很高，有时高达五、六十美分，甚至一美元。封闭的房子| Augusta Huiell Seaman
偏远地区的邮票通常需要比税收更高的邮资才能获得。革命前夕|卡尔·贝克尔
当保单寄出并预付邮资，以便及时交付给被保险人时，合同即告完成。普特南为门外汉准备的简易法律书|阿尔伯特·西德尼·博尔斯
在信封的右上角贴上邮票。女士礼仪手册和礼貌手册|弗洛伦斯·哈特利
如查询只涉及你个人感兴趣的事项，请附上一张邮票作为答复。女士礼仪手册和礼貌手册|弗洛伦斯·哈特利