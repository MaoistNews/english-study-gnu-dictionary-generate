只要有一只脚在门上，就很容易忽略不愉快或不安全的行为，尤其是当没有其他人发出警报时。餐厅厨房的毒性正是我从未报告虐待行为的原因| Lindsey Danis | 2021年2月4日| Eater
这是一项工作要求，特别是在他近15年的欧洲外交官生涯中，但他显然认为这是一项不愉快的义务。并非所有总统的舞蹈技巧都是平等的|邦尼·伯克维茨、乔安妮·李| 2021年1月21日|华盛顿邮报|
这是一次非常不愉快的经历，但我能看出哪里出错了。奥运会跨栏金牌选手因兴奋剂案被暂时停赛|格林A.希尔| 2021年1月14日|华盛顿邮报
随着流感大流行的结束，全世界数百万人可能会面临更多令人不快的惊喜。技术可以帮助我们养活世界，如果我们超越利润|凯蒂·麦克莱恩| 2020年12月18日|麻省理工技术评论
这完全是令人不快的，想到接下来会有什么新习惯，我不寒而栗。礼仪小姐：丈夫粗鲁的饮食习惯让妻子怒不可遏|朱迪思·马丁、尼古拉斯·马丁、雅各比娜·马丁| 2020年12月18日|华盛顿邮报
两者都是陈腐和无聊的，无论你最终拥有哪一个，都是令人不快的。Peeta团队或Gale团队：为什么“饥饿游戏”三角恋会毁掉“模仿杰伊-第1部分”|凯文·法伦| 2014年11月28日|每日野兽
这是一个令人不快的提醒，对许多人来说，科学仍然被认为是一个男孩俱乐部。地球人，我们降落在彗星上| Matthew R.Francis | 2014年11月12日| DAILY BEAST
对于广受赞誉的布拉顿来说，这一定是一个令人不快的打击。2014年11月11日，《每日野兽》美国司法峰会上，比尔·布拉顿斥责咯咯笑的观众
它不仅仅是发现音乐令人不快，它还引用了合法性的修辞。《玩家、盖茨和迪斯科的毁灭：反动愤怒的根源》|朱棣文| 2014年10月16日|每日野兽
这是一个令人不快和混乱的场景：一个模拟劫持人质、精神避难所和疯狂的梦境的场景。性、血和尖叫：停电的黑暗恐惧| Tim Teeman | 2014年10月7日|每日野兽
这将给我们时间来扭转局面，并为类似的令人不快的伤亡做好准备。布莱克伍德的爱丁堡杂志，第CCCXXXIX号。1844年1月。第LV卷|各种各样的
没有人能比我们自己更好地理解那些盯着我们看的令人不快的可能性。原金|伯特兰·W·辛克莱
试图对一位经验丰富的法官进行模仿总是会产生令人不快的结果。小提琴和小提琴制作者|约瑟夫·皮尔斯
杰西从小就习惯了这些不愉快的场合。舞台上的中央高中女生|格特鲁德·W·莫里森
不要对你交谈的人重复你可能听到的关于她的任何不愉快的话。女士礼仪手册和礼貌手册|弗洛伦斯·哈特利