在它的18公升耐磨尼龙里装满了一天徒步旅行所需的零食和蛋卷，一架照相机和一件在城里跑的夹克，或者一堆杂货。我们最喜欢的旅行装备| Janna Irons | 2020年9月8日|户外在线
在冬季系列中，Infantium Victoria推出了一款连帽衫、婴儿夹克和披风的Weganool。《荒地灌木如何成为时尚界的下一件大事》| Daniel Malloy | 2020年8月28日| Ozy
当然，那是以前的时代，当时尖肩夹克和合身裤子是日常工作服。《财富》杂志记者克莱尔·齐尔曼（Claire Zillman）2020年8月21日报道，女性终于重新找回了“家装”
他告诉我他是如何在红木城的“坏邻居”周围走来走去的，夹克口袋里装着一把上了膛的手枪。当耶稣怪胎变坏时|尤金·罗宾逊| 2020年8月6日|奥兹
然后，当忧郁的音乐响起时，我们看到年轻人穿着夹克离开了家。老年护理工作需要彻底改名|丽拉·麦克莱伦| 2020年7月28日|石英
奥蒂斯说，他当时穿着一件与目击者描述的类似的棕褐色夹克。他40年后出狱的第一天：适应户外生活| Justin Rohrlich | 2015年1月3日| DAILY BEAST
巴索西拉·博塔拉穿着一件蓝色防雨夹克，尽管天气酷热。刚果被遗忘的殖民地逃亡|尼娜·斯特罗奇利奇| 2014年12月18日|每日野兽
我买了一件毛衣和夹克，在公文包里塞了一条领带，以防万一结果是舞会。阿尔弗雷德·希区柯克的《褪色为黑色：伟大导演的最后日子》|大卫·弗里曼| 2014年12月13日|每日野兽报
迈卡10岁了，他有一件适合这个季节的外套，一件带风帽的巴塔哥尼亚冬季夹克。纽约市为埃里克·加纳（Eric Garner）|迈克·巴尼科尔（Mike Barnicle）| 2014年12月8日|每日野兽（DAILY BEAST）|举行了极其和平、人性化、近乎无聊、最终意义重大的抗议活动
爱丽丝穿着一件黑色尼龙防雨夹克，看起来似乎没有做好应对即将到来的寒冷的准备。纽约市为埃里克·加纳（Eric Garner）|迈克·巴尼科尔（Mike Barnicle）| 2014年12月8日|每日野兽（DAILY BEAST）|举行了极其和平、人性化、近乎无聊、最终意义重大的抗议活动
在这段对话中，哈利的右手搁在夹克下面，抓着左轮手枪的枪托。奥扎克一家的信使拜伦·A·邓恩
他是一个狂热的民主党派，也是克伦威尔的支持者，当局经常让他穿上一件紧身夹克。《每日历史与年表》乔尔·蒙塞尔
农夫告诉他那是六英里；“但是，”他补充道，“你必须骑得很快，否则在你到达之前你会得到一件湿夹克。”|各种各样的
“先生，”卢西尼伯爵尊严地说，把赢来的钱塞进夹克口袋。阿里斯蒂德·普约尔|威廉·J·洛克的欢乐冒险
她的母亲穿着一件沾了墨水的夹克，正忙着坐在书桌旁，笔在一张张大的便笺纸上划来划去。舞台上的中央高中女生|格特鲁德·W·莫里森