在唐人街，她敦促人们采取预防措施并保持警惕。特朗普的ABC新闻市政厅：四只皮诺奇，一遍又一遍|格伦·凯斯勒| 2020年9月16日|华盛顿邮报
采取这项措施的部分原因是为了预防《危险》节目主持人亚历克斯·特雷贝克（Alex Trebek）的健康，他患有4期胰腺癌。冠状病毒疫情如何席卷电影和娱乐业| Alissa Wilkinson | 2020年9月11日| Vox
他推测，消费者在很长一段时间内都不会做好登机的准备，让旅行计划更多地取决于安全预防措施，而不是奇思妙想。Airbnb首席执行官：流感大流行将迫使我们看到更多的世界，而不是更少的《凡尔纳·科皮托夫》《财富》杂志（2020年9月7日）
总的来说，企业高管们说，以透明的方式召开面对面会议，并为一旦面对面将采取的各种预防措施做好准备，这是关键安全与健康：随着面对面会议的恢复，企业高管们正在进行冠状病毒“谈话”|克里斯蒂娜·蒙略斯| 2020年9月4日|迪吉德
她说，如果有机会检测并发现无症状接触呈阳性，那就更可取了，因为人们更有可能采取预防措施并适当隔离，特别是在家人周围。美国没有一个连贯的无症状检测策略。它需要一个|Caroline Chen | 2020年9月1日|出版
为了防止可能发生的骚乱，渡轮由一艘警艇护航，蓝色灯光闪烁我不能呼吸了我不能呼吸了对警察文化的道德控诉|迈克尔·戴利| 2014年12月4日|每日野兽
夹克和手套是一种预防措施，以防小鹰惊慌失措，但当他把网撒在她身上时，并没有引起什么骚动。他因营救小鹰而面临牢狱之灾|迈克尔·戴利| 2014年11月2日|每日野兽报
作为预防措施，他们已经接受了为期21天的发烧观察。CDC称埃博拉爆发为“森林大火”| Abby Haglage | 2014年7月28日| DAILY BEAST
但是，在薄壁营房的上层，预防措施是平淡无奇的。塔利班战斗季节开始时在阿富汗躲避火箭弹| Nick Willard | 2014年5月14日| DAILY BEAST
DePree说，Marble Collegiate有时会采取另一种流感预防措施。在圣餐会上你会感冒吗|Keli Goff | 2013年12月13日|每日野兽
为什么他在调查隐蔽的火灾这样可疑的事情时不采取更多的预防措施呢？原金|伯特兰·W·辛克莱
吉特上校采取了一切预防措施，以防夜间袭击，但他的小部队被允许安息。奥扎克一家的信使拜伦·A·邓恩
我已经采取了预防措施，聘请了城里最好的法律顾问。九个人及时|诺埃尔·米勒·卢米斯
一个处境轻松的人，只要采取上述简单的预防措施，就会省去许多烦恼。《小旅行》和《路边素描》|威廉·马克皮斯·萨克雷
事实上，你不会像我被捕时那样采取预防措施，搜查我寻找枪支。皮姆利科的医生威廉·勒奎克斯