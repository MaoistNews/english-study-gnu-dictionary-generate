今天，我们有幸拥有比以往更多的选择。最佳加热毛毯：与这些电毯捆绑在一起| PopSci商业团队| 2021年2月11日|科普
2019年11月，我有幸参加了在山景城谷歌总部举行的谷歌网站管理员会议。寻求更大的覆盖率：从谷歌搜索控制台的覆盖率报告（Glenn Gabe）| 2021年2月9日|搜索引擎土地）中获得更大的出口
作为拉巴斯一位将军四个孩子中最小的一个，她因从家里偷运食物和衣服给不幸的邻居而陷入麻烦。布兰卡·克林（Blanca Kling）是拉丁美洲社区的一名支柱，曾帮助过数千名犯罪受害者，她死于新冠病毒-19的并发症|卢兹·拉佐| 2021年2月5日|华盛顿邮报|
美国很幸运有皮特作为他们的运输部长。在LGBTQ历史上的第一次，Buttigieg被确认为内阁级任命者| Chris Johnson | 2021年2月2日| Washington Blade
Wachter和一些刚刚接种过疫苗的美国老年人告诉华盛顿邮报，他们认为自己很幸运，已经掌握了疫苗，并意识到疫苗接种后生活的不确定性是一个很好的问题。许多接受过冠状病毒疫苗的人都想知道：我能安全地做什么|劳里·麦金利，伦尼·伯恩斯坦| 2021年2月1日|华盛顿邮报
我很幸运，我从未患过致命的疾病，但每当我得了胃流感时，我肯定会觉得自己快死了。为什么我的诺沃克病毒恐慌让我生病|莉齐·克罗克| 2015年1月5日|每日野兽
大卫·柯克帕特里克（David Kirkpatrick），一本关于该网站的书的作者，告诉《泰晤士报》：“他很幸运，他发现自己在同一个房间里。”。克里斯·休斯和肖恩·埃尔德里奇的兴衰，美国最差的同性恋权力夫妻|詹姆斯·基奇克| 2014年12月9日|每日野兽
我很幸运，我的演员、DP和布景设计师都是顶级的。乔恩·斯图尔特谈论“罗斯沃特”和“奇肯斯-t”民主党中期大屠杀|马洛·斯特恩| 2014年11月9日|每日野兽报
她意识到，无论是在个人还是在职业上，她都是幸运的。喀麦隆妇女与旅游业的性别歧视作斗争| Abena Agyeman Fisher | 2014年11月7日|每日野兽
有幸逃脱的居民在8月初开始返回被摧毁的村庄。加沙的幽灵：以色列士兵自杀|克里德·牛顿| 2014年10月28日|每日野兽
他有着巨大的食欲，幸运的是，他是一位善于寻找角虫的专家。鼹鼠爷爷的故事|亚瑟·斯科特·贝利
于是，他诱骗了他们，这对幸运的夫妇得以在黑暗的掩护下到达英国防线。红色的一年|路易斯·特蕾西
这次幸运捕获成功的事件是众所周知的，不需要非常简短的重述。布莱克伍德的爱丁堡杂志，第CCCXXXIX号。1844年1月。第LV卷|各种各样的
除了作为一个幸运的朋友的客人，我从来不敢冒险过去。祖先|格特鲁德·阿瑟顿
草甸鼠先生毫不犹豫地使用了它，他是那些在任何地方都很自在的幸运儿之一。鼹鼠爷爷的故事|亚瑟·斯科特·贝利