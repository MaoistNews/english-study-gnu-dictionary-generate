五百枚贝壳是手工制作的，设计成闪烁的金光和垂柳般的效果。还被就职典礼的表演弄得眼花缭乱吗？下面是它是如何结合在一起的，以及为什么汤姆·汉克斯看起来如此冷漠|Maura Judkis | 2021年1月22日|华盛顿邮报
指甲油整理器是一个展示你的闪光和金色的好方法，同时也能让你的眼睛看起来很舒服。指甲油组织者检查您的收藏| PopSci商业团队| 2021年1月20日|科普
不在长街，这座城市的著名部分，有着所有的大酒店、赌场和闪闪发光的东西。回放（2015）：下一个布鲁克林会是……拉斯维加斯吗？！（第205页）| Stephen J.Dubner | 2020年12月6日|畸形经济学
这一年没有太多的闪光或炫目，但你可以改变这一点。《2020年酷儿送礼终极指南》|米奇·洛克斯| 2020年12月4日|华盛顿刀片
所有这些都凸显出为什么黄金开采业最近所见证的辉煌不会很快消失。2020年，一切闪亮的东西都是金子|查鲁·卡斯图里| 2020年9月8日|奥齐
尼尔·帕特里克·哈里斯（Neil Patrick Harris）、海德薇（Hedwig）和尼尔·帕特里克·哈里斯（Neil Patrick Harris）在《鱼网》、《高跟鞋》和《闪闪发光》（Glight）中的愤怒形象可能是一个很好的噱头。海德薇、休和迈克尔·塞拉：2014年12月31日《贾妮斯·卡普兰》《每日野兽》12场大型剧场演出
我在我的产品上缝、粘、闪、切、绑许多东西，以完成最终的创作。埃西改变了政策。那又怎样|Sara Stroman | 2013年11月8日|每日野兽
还是有一次玛丽亚·凯莉漫谈了四分钟关于蝴蝶的事，然后把闪光的东西吹到了风中？詹妮弗·洛佩兹、基思·厄本和小哈利·康尼克将拯救《美国偶像》|凯文·法伦| 2013年9月3日|每日野兽
碧昂斯只用闪光笔为《炫耀》杂志封面：如果你喜欢，那么你应该在上面涂上闪光笔。CFDA/Vogue时尚基金入围名单公布；碧昂丝只穿闪闪发光的衣服|时尚野兽队| 2013年7月11日|每日野兽
碧昂斯在最新一期的《炫耀》杂志上登了封面，除了闪闪发光的金光，她什么也没穿。CFDA/Vogue时尚基金入围名单公布；碧昂丝只穿闪闪发光的衣服|时尚野兽队| 2013年7月11日|每日野兽
我只看到一个墨西哥人在帮我站起来的时候，把刺刀刺进了他的肩膀，刺刀闪闪发光。布莱克伍德的爱丁堡杂志，第CCCXXXIX号。1844年1月。第LV卷|各种各样的
伯爵闪闪发光的眼睛使他着迷，在他们的魔力下，他看到的和伯爵看到的一样，感觉到的和他感觉到的一样。永恒的武器|约瑟夫·霍金
她向他伸出紧握的双手，手指上的蛇戒指似乎以其不可思议的闪光嘲笑他。他们看起来很可爱|亚历克斯·麦克维·米勒夫人
他们无法得到金钱，无法得到黄金的光芒，也无法听到痛苦的呼喊。用锋利的工具|亨利·塞顿·梅里曼
马具极其鲜艳，漆成各种颜色，红色、蓝色和黄色，并由一些金属丝和闪光组成。环游美好世界| G.E.米顿