#百度通用翻译API,不包含词典、tts语音合成等资源，如有相关需求请联系translate_api@baidu.com
# coding=utf-8

import http.client
import hashlib
import urllib
import random
import json
import requests as r
from base64 import b64encode as e
import sys,os,threading,time,re,gc,json
import translators as ts

def query(q):
    global toLang
    if not q:return{'trans_result':[]}
    return {'trans_content':q,'trans_result':ts.youdao(q,sleep_seconds=5,timeout=None,proxies=None,from_language='en', to_language='zh')}

def scan_cycle(text,n):return scan(text,n)
def scan(text,n):
    n+=1
    if n>5:return None#People’s War
    r=query(text) #强制学习
    print(r)
    if r:
        if'trans_result'in r:return r
    return scan_cycle(text,n)
class thread_rec(threading.Thread):
    def __init__(self,d,n):threading.Thread.__init__(self);self.d,self.n=d,n
    def run(self):rec(self.d,self.n)
def rec(d,n):
    global li,m,k,toLang;li.append(n)
    pa='sentences_zh_dict/%s.dict'%d[0].split('.txt')[0]
    if not os.path.exists(pa):
        print('正在识别%d'%(int((n-1)/2)+1))
        print(pa)
        nz=0
        r2=scan(d[1],nz)
        if r2==None:pass
        else:
            print(len(d[1]))
            print(pa)
            if not os.path.exists(zn:='/'.join(pa.split('/')[:-1])):os.makedirs(zn)
            f=open(pa,'w+');f.write(repr(r2));f.close()
    li.remove(n)
toLang='en'
def run(path,lang):
    print("\033c", end="")
    global li,cl,toLang
    toLang=lang
    c=[]
    for x,y,z in os.walk('%s/sentences'%path):
        for w in z:
            f=open('%s/%s'%(x,w),'rb');c.append([w,f.read().decode(errors='ignore')]);f.close()
    n,li=-1,[]
    cl=len(c)
    while(True):
        if(threading.active_count()<2):
            n+=2
            print(int((n+1)/2),cl)
            if(int((n+1)/2)<=cl):thread_rec(c[int((n-1)/2)],n).start()
            elif(threading.active_count()<=1):break
run(sys.path[0],'zh')
