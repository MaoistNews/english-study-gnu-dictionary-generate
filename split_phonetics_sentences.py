from html.parser import HTMLParser
from re import sub
from sys import stderr
from traceback import print_exc
class _DeHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.__text = []

    def handle_data(self, data):
        text = data.strip()
        if len(text) > 0:
            text = sub('[ \t\r\n]+', ' ', text)
            self.__text.append(text + ' ')

    def handle_starttag(self, tag, attrs):
        if tag == 'p':
            self.__text.append('\n\n')
        elif tag == 'br':
            self.__text.append('\n')

    def handle_startendtag(self, tag, attrs):
        if tag == 'br':
            self.__text.append('\n\n')

    def text(self):
        return ''.join(self.__text).strip()
def dehtml(text):
    try:
        parser = _DeHTMLParser()
        parser.feed(text)
        parser.close()
        return parser.text()
    except:
        print_exc(file=stderr)
        return text
def replac(t):
    l=[['whenstressed','ws. '],
       ['adjective','adj. '],
       ['preposition','prep. '],
       ['verb','v. '],
       ['noun','n. '],
       ['for11-14',' f.11-14'],
       ['for10',' f.10'],
       ['unstressed','uns. '],
       ['or,especiallyBritish,','UK. '],
       ['or,especiallyafterahomorganicconsonant,','或者，特别是在同臂辅音之后，'],
       ['interjection','inter. '],
       ['British','UK. '],
       ['Nautical','Na. '],
       ['stressed','s. '],
       ['before a consonant','在辅音之前，'],
       ['for 1-4, 6',' f.1-4,6'],
       ['for 5',' f.5'],
       ['for 1-3',' f.1-3'],
       ['for 4',' f.4'],
       ['or,forpasttenseformof9',' 或者，过去时形式为9'],
       ['OlderUseandLiterary','旧的使用和提交'],
       ['Britishalso','UK. '],
       ['or,especiallybeforeconsonants','或者，特别是在辅音之前'],
       ['beforeaconsonant','在辅音之前'],
       ['beforeavowel','在元音之前'],
       ['usually','us. '],
       ['also',' 也为 '],
       [';','; '],
       [',',', '],
       ['，',', '],
       ['for',' f.']]
    for a in l:t=t.replace(a[0],a[1])
    return t
f=open('TEM-8 words.txt','r');l,he=[[a]for a in eval(f.read())],'dictionary.com';f.close()
for a in range(len(l)):
    print(l[a][0])
    f=open('%s/%s.htm'%(he,l[a][0]),'r');ps=f.read();f.close()
    l[a].append(replac(dehtml('%s /'%'>'.join(ps.split(st)[1].split('>')[1:]).split(' /')[0]if ps.find(st:='<span class="pron-ipa-content')!=-1 else'/ %s /'%l[a][0]).replace('/ ','/|').replace(' /','|/').replace(' ','').replace('/|','/ ').replace('|/',' /')))
l.sort(key=lambda x:x[0])
f=open('TEM-8 words_with_phonetics.txt','w+');f.write('\n'.join(['::'.join(a)for a in l]));f.close()
